class AddTagsToPrintTemplateLabels < ActiveRecord::Migration
  def change
    add_column :print_template_labels, :title_tag, :string
    add_column :print_template_labels, :content_tag, :string
  end
end
