class AddInfoToMyprintOrders < ActiveRecord::Migration
  def change
  	add_column :myprint_orders, :reference_number, :string
    add_column :myprint_orders, :address, :string
    add_column :myprint_orders, :address_complement, :string
    add_column :myprint_orders, :picture_url, :string
    add_column :myprint_orders, :surface, :string
  	add_column :myprint_orders, :type_de_bien, :string
    add_column :myprint_orders, :r_first_name, :string
  	add_column :myprint_orders, :r_last_name, :string
  	add_column :myprint_orders, :r_tel, :string
  	add_column :myprint_orders, :r_email, :string
  	add_column :myprint_orders, :r_company_name, :string
  	add_column :myprint_orders, :r_company_email, :string
  	add_column :myprint_orders, :r_notes, :text
  end
end
