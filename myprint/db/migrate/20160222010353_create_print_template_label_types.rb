class CreatePrintTemplateLabelTypes < ActiveRecord::Migration
  def change
    create_table :print_template_label_types do |t|
      t.string :slug
      t.string :description

      t.timestamps null: false
    end
  end
end
