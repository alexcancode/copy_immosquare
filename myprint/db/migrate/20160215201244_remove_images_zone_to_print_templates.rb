class RemoveImagesZoneToPrintTemplates < ActiveRecord::Migration
  def change
    remove_column :print_templates, :image_zones, :integer
  end
end
