class RemoveDescriptionToPrintTemplateLabel < ActiveRecord::Migration
  def change
    remove_column :print_template_labels , :description
  end
end
