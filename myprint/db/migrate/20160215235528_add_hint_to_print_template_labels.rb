class AddHintToPrintTemplateLabels < ActiveRecord::Migration
  def change
    add_column :print_template_labels, :hint, :string
    add_column :print_template_labels, :is_visible, :integer, :default => 0
  end
end
