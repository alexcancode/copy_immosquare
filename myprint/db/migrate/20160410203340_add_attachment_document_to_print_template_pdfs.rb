class AddAttachmentDocumentToPrintTemplatePdfs < ActiveRecord::Migration
  def self.up
    change_table :print_template_pdfs do |t|
      t.attachment :document
    end
  end

  def self.down
    remove_attachment :print_template_pdfs, :document
  end
end
