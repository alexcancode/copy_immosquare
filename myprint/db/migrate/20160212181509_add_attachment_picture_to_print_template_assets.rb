class AddAttachmentPictureToPrintTemplateAssets < ActiveRecord::Migration
  def self.up
    change_table :print_template_assets do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :print_template_assets, :picture
  end
end
