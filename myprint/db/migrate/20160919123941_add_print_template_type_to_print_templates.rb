class AddPrintTemplateTypeToPrintTemplates < ActiveRecord::Migration
  def change
    add_reference :print_templates, :print_template_type, index: true, foreign_key: false,:after => :id
  end
end
