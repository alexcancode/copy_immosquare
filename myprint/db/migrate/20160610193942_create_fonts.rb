class CreateFonts < ActiveRecord::Migration
  def change
    create_table :fonts do |t|
      t.integer :draft, :default => 1

      t.timestamps null: false
    end
  end
end
