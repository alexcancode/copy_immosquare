class AddPrintTemplatePdfIdToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :print_template_pdf, index: true, foreign_key: false,:after=>:print_template_id
  end
end
