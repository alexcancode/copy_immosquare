class AddNameIsVisibleToPrintLabels < ActiveRecord::Migration
  def change
    remove_column :print_template_labels , :is_visible
    add_column :print_template_labels, :name_is_visible, :integer, :default => 1
  end
end
