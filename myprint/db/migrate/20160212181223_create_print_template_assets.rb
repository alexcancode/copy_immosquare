class CreatePrintTemplateAssets < ActiveRecord::Migration
  def change
    create_table :print_template_assets do |t|
      t.references :print_template, index: true, foreign_key: false
      t.integer :position

      t.timestamps null: false
    end
  end
end
