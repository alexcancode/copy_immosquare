class AddPrintTemplateSizeToPrintTemplate < ActiveRecord::Migration
  def change
    add_reference :print_templates, :print_template_size, index: true, foreign_key: false, :after => :print_template_type_id
  end
end
