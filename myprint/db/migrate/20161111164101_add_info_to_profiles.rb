class AddInfoToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :phone, :string
    add_column :profiles, :email, :string
    add_column :profiles, :company_name, :string
    add_column :profiles, :company_email, :string
    add_column :profiles, :company_phone, :string
    add_column :profiles, :company_address, :string
    add_column :profiles, :company_address2, :string
    add_column :profiles, :company_country, :string
    add_column :profiles, :company_city, :string
    add_column :profiles, :company_province, :string
    add_column :profiles, :company_zipcode, :string
  end
end
