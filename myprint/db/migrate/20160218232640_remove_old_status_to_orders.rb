class RemoveOldStatusToOrders < ActiveRecord::Migration
  def change
    add_column    :orders, :status, :integer, :default => 0, :after => :print_template_id
    change_column :orders, :status_as_text, :string, :after => :status
    remove_column :orders, :preview_status, :string
    remove_column :orders, :print_pdf_status, :integer
    remove_column :orders, :send_to_printer_status, :integer
    remove_column :orders, :printer_have_print_status, :integer
    remove_column :orders, :print_send_status, :integer
  end
end
