class AddDraftToPrintTemplateZones < ActiveRecord::Migration
  def change
    add_column :print_template_zones, :draft, :integer, :after => :id , :default => 0
  end
end
