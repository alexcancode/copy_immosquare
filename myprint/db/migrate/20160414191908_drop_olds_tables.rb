class DropOldsTables < ActiveRecord::Migration
  def change
   drop_table :print_template_labels
   drop_table :print_template_label_types
   drop_table :print_template_label_translations
   drop_table :print_template_areas
   drop_table :print_template_zones
   drop_table :print_template_zone_types
   drop_table :print_template_zone_translations
 end
end
