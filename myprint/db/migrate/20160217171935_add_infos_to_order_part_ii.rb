class AddInfosToOrderPartIi < ActiveRecord::Migration
  def change
    remove_column :orders, :template_id
    remove_column :orders, :order_info
    add_reference :orders, :print_template, index: true, foreign_key: false, :after => :uid
    add_column :orders, :json_template, :text, :after => :print_template_id
    add_column :orders, :status_as_text, :string, :after => :env
  end
end
