class ChangeTypeForAllDeliveries < ActiveRecord::Migration
  def change

  	change_column :postcanada_sectors, :shipping_mode_id, :integer

  	change_column :postcanada_sectors, :delivery_spot_shops, :integer
  	change_column :postcanada_sectors, :delivery_spot_domiciles, :integer
  	change_column :postcanada_sectors, :delivery_spot_farms, :integer

  	change_column :postcanada_sectors, :consumer_choice_appartments, :integer
  	change_column :postcanada_sectors, :consumer_choice_shops, :integer
  	change_column :postcanada_sectors, :consumer_choice_domiciles, :integer
  	change_column :postcanada_sectors, :consumer_choice_farms, :integer

  end
end
