class RemoveTypeZoneToPrintTemplateZone < ActiveRecord::Migration
  def change
    remove_column :print_template_zones, :type_zone, :string
  end
end
