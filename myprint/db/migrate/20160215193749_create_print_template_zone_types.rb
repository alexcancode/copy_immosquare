class CreatePrintTemplateZoneTypes < ActiveRecord::Migration
  def change
    create_table :print_template_zone_types do |t|
      t.string :name
      t.string :description

      t.timestamps null: false
    end
  end
end
