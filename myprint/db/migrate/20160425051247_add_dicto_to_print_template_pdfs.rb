class AddDictoToPrintTemplatePdfs < ActiveRecord::Migration
  def change
    add_column :print_template_pdfs, :dictionary, :text
  end
end
