class CreateApiProviders < ActiveRecord::Migration
  def change
    create_table :api_providers do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
