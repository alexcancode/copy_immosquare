class CreateApiApps < ActiveRecord::Migration
  def change
    create_table :api_apps do |t|
      t.references :api_provider, index: true, foreign_key: false
      t.string :key
      t.string :token
      t.string :note
      t.timestamps null: false
    end
  end
end
