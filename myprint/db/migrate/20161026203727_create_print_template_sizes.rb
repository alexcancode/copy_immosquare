class CreatePrintTemplateSizes < ActiveRecord::Migration
  def change
    create_table :print_template_sizes do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
