class AddLabelIsVisibleToPrintTemplateZone < ActiveRecord::Migration
  def change
    add_column :print_template_zones, :name_is_visible, :integer, :default => 1
  end
end
