class CreatePrintTemplateZones < ActiveRecord::Migration
  def change
    create_table :print_template_zones do |t|
      t.references :print_template, index: true, foreign_key: false
      t.integer :position
      t.string :type_zone, :default => "A"
      t.string :name


      t.timestamps null: false
    end
  end
end
