class AddDraftToPrintTemplateSize < ActiveRecord::Migration
  def change
    add_column :print_template_sizes, :draft, :integer, :default => 1, :after => :id
    add_column :print_template_sizes, :width, :float, :after => :name
    add_column :print_template_sizes, :height, :float, :after => :width
    add_column :print_template_sizes, :unit_id, :integer, :after => :height
  end
end
