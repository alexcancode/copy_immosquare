class AddContentTitleSeperatorToPrintTemplateLabels < ActiveRecord::Migration
  def change
    add_column :print_template_labels, :content_title_separator, :string, :default => " : "
  end
end
