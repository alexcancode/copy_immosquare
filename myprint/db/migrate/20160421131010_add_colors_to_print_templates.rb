class AddColorsToPrintTemplates < ActiveRecord::Migration
  def change
    add_column :print_templates, :color_c, :integer, :default => 50
    add_column :print_templates, :color_m, :integer, :default => 50
    add_column :print_templates, :color_y, :integer, :default => 50
    add_column :print_templates, :color_k, :integer, :default => 50
    add_column :print_templates, :color_editable, :integer, :default => 0
  end
end
