class ChangeTypesForDeliveryAppartments < ActiveRecord::Migration
  def change
  	change_column :postcanada_sectors, :delivery_spot_appartments,  :integer
  end
end
