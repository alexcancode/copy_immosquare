class AddProviderHostToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :provider_host, :string, :after => :provider
  end
end
