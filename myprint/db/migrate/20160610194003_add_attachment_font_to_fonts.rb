class AddAttachmentFontToFonts < ActiveRecord::Migration
  def self.up
    change_table :fonts do |t|
      t.attachment :font
    end
  end

  def self.down
    remove_attachment :fonts, :font
  end
end
