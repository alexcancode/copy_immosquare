class RemoveAttToTempAss < ActiveRecord::Migration
  def change
    remove_attachment :print_template_assets, :picture
    add_attachment :print_template_assets, :picture
  end
end
