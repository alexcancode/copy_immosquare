class AddOrderStatusIdToOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :status
    remove_column :orders, :status_as_text
    add_reference :orders, :order_status, index: true, foreign_key: false, :after => :print_template_id


  end
end
