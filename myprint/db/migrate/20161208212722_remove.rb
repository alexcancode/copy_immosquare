class Remove < ActiveRecord::Migration
  def change
    drop_table :api_providers
    drop_table :api_apps
    drop_table :myprint_orders
    drop_table :print_template_assets
  end
end
