class CreatePrintTemplatePdfs < ActiveRecord::Migration
  def change
    create_table :print_template_pdfs do |t|
      t.references :print_template, index: true, foreign_key: false
      t.string :locale

      t.timestamps null: false
    end
  end
end
