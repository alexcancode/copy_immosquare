class AddPropertyCountToPrintTemplatePdf < ActiveRecord::Migration
  def change
    add_column :print_template_pdfs, :property_count, :integer, :default => 1
  end
end
