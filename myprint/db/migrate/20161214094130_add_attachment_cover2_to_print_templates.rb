class AddAttachmentCover2ToPrintTemplates < ActiveRecord::Migration

  def change
    remove_attachment :print_templates, :cover
  end
end
