class RemovePrintTemplatePageToPrintTemplateLabels < ActiveRecord::Migration
  def change
    remove_reference :print_template_labels, :print_template_page, index: true, foreign_key: false
  end
end
