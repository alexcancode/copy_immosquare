class AddCountryToPrintTemplates < ActiveRecord::Migration
  def change
    add_column :print_templates, :country, :string
  end
end
