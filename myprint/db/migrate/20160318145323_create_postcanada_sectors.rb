class CreatePostcanadaSectors < ActiveRecord::Migration
	def change
		create_table :postcanada_sectors do |t|

			t.string :service_office_ZIP_code, :limit => 6
			t.string :service_office_shipping_sector_name, :limit => 30
			t.string :service_office_shipping_installation_type_description, :limit => 5
			t.string :service_office_shipping_installation_qualification, :limit => 15
			t.string :service_office_short_name, :limit => 15
			t.string :service_office_state_code, :limit => 1

			t.string :shipping_mode_type, :limit => 2
			t.string :shipping_mode_id, :limit => 4
			t.string :delivery_spot_appartments, :limit => 5
			t.string :delivery_spot_shops, :limit => 5
			t.string :delivery_spot_domiciles, :limit => 5
			t.string :delivery_spot_farms, :limit => 5
			t.string :consumer_choice_appartments, :limit => 5
			t.string :consumer_choice_shops, :limit => 5
			t.string :consumer_choice_domiciles, :limit => 5
			t.string :consumer_choice_farms, :limit => 5
			t.string :ZIP_code, :limit => 6

			t.string :UDL_type, :limit => 2
			t.string :supervisor_office_ZIP_code, :limit => 6
			t.string :supervisor_office_shipping_sector_name, :limit => 30
			t.string :supervisor_office_shipping_installation_shipping_sector_name, :limit => 30
			t.string :supervisor_office_shipping_installation_type_description, :limit => 5
			t.string :supervisor_office_shipping_installation_qualification, :limit => 15
			t.string :supervisor_office_local_address_number, :limit => 6

			t.string :supervisor_office_local_address_number_suffix, :limit => 1
			t.string :supervisor_office_unit_number, :limit => 6
			t.string :supervisor_office_street_name, :limit => 30
			t.string :supervisor_office_street_type, :limit => 6
			t.string :supervisor_office_cardinal_point, :limit => 2
			t.string :supervisor_office_province_indicator, :limit => 2
			t.string :supervisor_office_state_code, :limit => 1
			t.string :RTA, :limit => 3
			t.string :itinerary_sollicitation_indicator, :limit => 2

		end
	end
end
