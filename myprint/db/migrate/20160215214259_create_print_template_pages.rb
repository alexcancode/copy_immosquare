class CreatePrintTemplatePages < ActiveRecord::Migration
  def change
    create_table :print_template_pages do |t|
      t.references :print_template, index: true, foreign_key: false
      t.integer :draft, :default => 0
      t.integer :position
      t.timestamps null: false
    end
  end
end
