class RemovePrintTemplatePageToPrintTemplateZones < ActiveRecord::Migration
  def change
    remove_reference :print_template_zones, :print_template_page, index: true, foreign_key: false
  end
end
