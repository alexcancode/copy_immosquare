class RemoveInfosToPrintTemplates < ActiveRecord::Migration
  def change
    remove_column :print_templates, :description, :text
    remove_column :print_templates, :content, :text
    remove_column :print_templates, :country, :string
  end
end
