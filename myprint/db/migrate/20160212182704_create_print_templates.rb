class CreatePrintTemplates < ActiveRecord::Migration
  def change
    create_table :print_templates do |t|
      t.integer :draft
      t.string :name
      t.integer :image_zones
      t.string :description
      t.text :content

      t.timestamps null: false
    end
  end
end
