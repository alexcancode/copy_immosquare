class RemovePrintTemplatePageToPrintTemplateAreas < ActiveRecord::Migration
  def change
    remove_reference :print_template_areas, :print_template_page, index: true, foreign_key: false
  end
end
