class AddInfosToPrintTemplateZones < ActiveRecord::Migration
  def change
    remove_attachment :print_template_assets, :cover
    add_reference :print_template_zones, :print_template_zone_type, index: true, foreign_key: false
  end
end
