class AddPreviewCountToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :preview_count, :integer, :default => 0, :after => :order_status_id
  end
end
