class AddModelToPrintTemplatePdfs < ActiveRecord::Migration
  def up
    add_attachment :print_template_pdfs, :model
  end

  def down
    remove_attachment :print_template_pdfs, :model
  end
end