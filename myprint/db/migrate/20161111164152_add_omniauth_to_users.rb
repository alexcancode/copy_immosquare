class AddOmniauthToUsers < ActiveRecord::Migration
  def change
    add_column :users, :uid, :string, :after => :id
    add_column :users, :provider, :string, :after => :uid

  end
end
