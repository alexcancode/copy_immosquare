class CreatePrintTemplateLabelsTranslations < ActiveRecord::Migration
  def self.up
    PrintTemplateLabel.create_translation_table!({
      :name => :string,
    }, {
      :migrate_data => false
    })
  end

  def self.down
    PrintTemplateLabel.drop_translation_table! :migrate_data => true
  end
end



