class AddActiveToPdfPrintTemplatePdfs < ActiveRecord::Migration
  def change
    add_column :print_template_pdfs, :active, :boolean, :after => :print_template_id, :default => true
  end
end
