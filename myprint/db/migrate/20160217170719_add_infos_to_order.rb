class AddInfosToOrder < ActiveRecord::Migration
  def change
    add_reference :orders, :api_provider, index: true, foreign_key: false, :after => :id
    add_column :orders, :uid, :string, :after => :api_provider_id
    add_column :orders, :env, :string, :after => :uid
    add_column :orders, :channel, :string, :after => :env

  end
end
