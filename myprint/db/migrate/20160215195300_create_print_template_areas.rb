class CreatePrintTemplateAreas < ActiveRecord::Migration
  def change
    create_table :print_template_areas do |t|
      t.references :print_template, index: true, foreign_key: false
      t.integer :draft, :default => 0
      t.integer :position
      t.integer :x_min
      t.integer :y_min
      t.timestamps null: false
    end
  end
end
