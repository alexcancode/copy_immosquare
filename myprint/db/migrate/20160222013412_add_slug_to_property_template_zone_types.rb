class AddSlugToPropertyTemplateZoneTypes < ActiveRecord::Migration
  def change
    rename_column :print_template_zone_types, :name, :slug
  end
end
