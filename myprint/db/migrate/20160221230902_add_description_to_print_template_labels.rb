class AddDescriptionToPrintTemplateLabels < ActiveRecord::Migration
  def change
    remove_column :print_template_labels , :hint
    add_column :print_template_labels, :description, :string
  end
end
