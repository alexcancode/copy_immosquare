class ChangeEnvToOrders < ActiveRecord::Migration
  def change
    change_column :orders, :env, :string, :after => :uid
  end
end
