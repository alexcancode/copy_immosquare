class CreatePrintTemplateLabels < ActiveRecord::Migration
  def change
    create_table :print_template_labels do |t|
      t.integer :draft
      t.references :print_template_page, index: true, foreign_key: false
      t.integer :position
      t.string :slug
      t.string :name

      t.timestamps null: false
    end
  end
end
