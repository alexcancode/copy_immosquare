class AddPrintTemplatePageToPrintTemplateZone < ActiveRecord::Migration
  def change
    remove_reference :print_template_zones, :print_template
    remove_reference :print_template_areas, :print_template
    add_reference :print_template_zones, :print_template_page, index: true, foreign_key: false, :after => :draft
    add_reference :print_template_areas, :print_template_page, index: true, foreign_key: false, :after => :draft
  end
end
