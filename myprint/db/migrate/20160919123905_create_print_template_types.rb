class CreatePrintTemplateTypes < ActiveRecord::Migration
  def change
    create_table :print_template_types do |t|
      t.string :name
      t.string :slug

      t.timestamps null: false
    end
  end
end
