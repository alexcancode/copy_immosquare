class RenameJsonTemplateToOrders < ActiveRecord::Migration
  def change
    rename_column :orders, :json_template, :dictionary
  end
end
