class AddInfoToOrder < ActiveRecord::Migration
  def change
  	add_column :orders, :user_id, :integer, after: :id
  	add_column :orders, :picture_url, :string, after: :user_id
  	add_column :orders, :address, :string, after: :picture_url
  end
end
