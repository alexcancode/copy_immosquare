class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :template
      t.text :order_info
      t.integer :preview_status
      t.integer :print_pdf_status
      t.integer :send_to_printer_status
      t.integer :printer_have_print_status
      t.integer :print_send_status
      t.timestamps null: false
    end
  end
end
