class AddDraftToPrintTemplatePdfs < ActiveRecord::Migration
  def change
    add_column :print_template_pdfs, :draft, :integer, :after => :active, :default => 0
  end
end
