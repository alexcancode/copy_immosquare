class AddReferencesToPrintTemplateLabel < ActiveRecord::Migration
  def change
    add_reference :print_template_labels, :print_template_label_type, index: true, foreign_key: false, :after => :slug
    remove_column :print_template_labels , :slug
  end
end
