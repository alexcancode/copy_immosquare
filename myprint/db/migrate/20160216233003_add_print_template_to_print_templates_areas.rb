class AddPrintTemplateToPrintTemplatesAreas < ActiveRecord::Migration
  def change
    add_reference :print_template_areas, :print_template, index: true, foreign_key: false, :after => :draft
    add_reference :print_template_zones, :print_template, index: true, foreign_key: false, :after => :draft
    add_reference :print_template_labels, :print_template, index: true, foreign_key: false, :after => :draft
  end
end
