# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161227211715) do

  create_table "fonts", force: :cascade do |t|
    t.integer  "draft",             limit: 4,   default: 1
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "font_file_name",    limit: 255
    t.string   "font_content_type", limit: 255
    t.integer  "font_file_size",    limit: 4
    t.datetime "font_updated_at"
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id",               limit: 4
    t.string   "picture_url",           limit: 255
    t.string   "address",               limit: 255
    t.integer  "api_provider_id",       limit: 4
    t.string   "uid",                   limit: 255
    t.string   "env",                   limit: 255
    t.integer  "print_template_id",     limit: 4
    t.integer  "print_template_pdf_id", limit: 4
    t.integer  "order_status_id",       limit: 4
    t.integer  "preview_count",         limit: 4,     default: 0
    t.text     "dictionary",            limit: 65535
    t.string   "channel",               limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "orders", ["api_provider_id"], name: "index_orders_on_api_provider_id", using: :btree
  add_index "orders", ["order_status_id"], name: "index_orders_on_order_status_id", using: :btree
  add_index "orders", ["print_template_id"], name: "index_orders_on_print_template_id", using: :btree
  add_index "orders", ["print_template_pdf_id"], name: "index_orders_on_print_template_pdf_id", using: :btree

  create_table "print_template_pdfs", force: :cascade do |t|
    t.integer  "print_template_id",     limit: 4
    t.boolean  "active",                              default: true
    t.integer  "draft",                 limit: 4,     default: 0
    t.string   "locale",                limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "document_file_name",    limit: 255
    t.string   "document_content_type", limit: 255
    t.integer  "document_file_size",    limit: 4
    t.datetime "document_updated_at"
    t.text     "dictionary",            limit: 65535
    t.integer  "property_count",        limit: 4,     default: 1
    t.string   "description",           limit: 255
    t.string   "model_file_name",       limit: 255
    t.string   "model_content_type",    limit: 255
    t.integer  "model_file_size",       limit: 4
    t.datetime "model_updated_at"
  end

  add_index "print_template_pdfs", ["print_template_id"], name: "index_print_template_pdfs_on_print_template_id", using: :btree

  create_table "print_template_sizes", force: :cascade do |t|
    t.integer  "draft",      limit: 4,   default: 1
    t.string   "name",       limit: 255
    t.float    "width",      limit: 24
    t.float    "height",     limit: 24
    t.integer  "unit_id",    limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "print_template_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "slug",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "print_templates", force: :cascade do |t|
    t.integer  "print_template_type_id", limit: 4
    t.integer  "print_template_size_id", limit: 4
    t.integer  "draft",                  limit: 4
    t.string   "name",                   limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "color_c",                limit: 4,   default: 50
    t.integer  "color_m",                limit: 4,   default: 50
    t.integer  "color_y",                limit: 4,   default: 50
    t.integer  "color_k",                limit: 4,   default: 50
    t.integer  "color_editable",         limit: 4,   default: 0
  end

  add_index "print_templates", ["print_template_size_id"], name: "index_print_templates_on_print_template_size_id", using: :btree
  add_index "print_templates", ["print_template_type_id"], name: "index_print_templates_on_print_template_type_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "uid",                    limit: 255
    t.string   "provider",               limit: 255
    t.string   "provider_host",          limit: 255
    t.string   "oauth_token",            limit: 255
    t.string   "oauth_refresh_token",    limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
