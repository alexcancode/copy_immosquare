class DictionaryPopulater

  include HtmlToPlainTextHelper

  def initialize(args)
    args = args.deep_symbolize_keys
    @pdf                      = args[:pdf]
    I18n.locale               = @pdf.locale
    @dictionary               = @pdf.dictionary.deep_symbolize_keys
    @properties               = args[:properties]
    @property                 = nil
    @image                    = nil
    @user                     = args[:user]
  end


  ##============================================================##
  ## Send a Populated dictionary
  ## Il faut utiliser .clone sinon on change text[:name]
  ## dans decompose_definition avec slice!
  ##============================================================##
  def populate_dictionary
    if @dictionary[:document].present?
      @dictionary[:document][:primary_color] = [@pdf.print_template.color_c, @pdf.print_template.color_m, @pdf.print_template.color_y, @pdf.print_template.color_k]
    end
    @dictionary[:blocks].each do |b|
      case b[:type]
      when "text"
        b[:content] = decompose_definition(b[:name].clone) unless b[:content].present?
      when "image"
        b[:url] = decompose_image(b[:name].clone) if b[:editable] == "false"
      end
    end if @dictionary[:blocks].present?
    @dictionary
  end

  ##============================================================##
  ## This method calls the right dictionary method using the name of the block (for texts)
  ##============================================================##
  def decompose_definition(name)
    # indicates if we want a comma at the end
    with_slash    = name.slice!('_with_slash').present?

    # indicates if we want a comma at the end
    with_comma    = name.slice!('_with_comma').present?

    # indicates if we want a new line at the end
    with_newline  = name.slice!('_with_newline').present?

    # get all methods we want to call here (separated by "_and_")
    methods       = name.split('_and_')

    # for each method, we're gonna call it and collect the result
    methods.collect! do |meth|

      # here we check if we have a property number in the block name (format : "#i")
      meth = meth.split(".")[0]
      index = meth.slice!(/(#[0-9]+)/)
      index.slice!("#") if index.present?
      @property = index.present? && index.to_i <= @properties.length ? @properties[index.to_i - 1] : @properties[0]

      # and we call the method
      self.send(meth)
    end

    # Here we add a comma at the end if needed
    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth}, "
      end
    end if with_comma

     # Here we add a slash at the end if needed
    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth} / "
      end
    end if with_slash

    # Here we add a newline at the end if needed
    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth}\n"
      end
    end if with_newline

    # Finally, we concatenate everything
    final = methods.join((with_comma || with_newline)  ? "" : " ")
    return final
  end

  ##============================================================##
  ## This method calls the right dictionary method using the name of the block (for images)
  ##============================================================##
  def decompose_image(name)
    # here we check if we have a property number in the block name (format : "#i")
    property_number = name.slice!(/(#[0-9]+)/)
    property_number.slice!("#") if property_number.present?
    @property = property_number.present? && property_number.to_i <= @properties.length ? @properties[property_number.to_i - 1] : @properties[0]

    # here we check if we have an image number in the block name (format : "_i")
    image_number = name.slice!(/(_[0-9]+)/)
    image_number.slice!("_") if image_number.present?
    @image = image_number.present? && image_number.to_i <= @property[:pictures].length ? @property[:pictures][image_number.to_i - 1] : @property[:cover_url]

    # and we call the method
    self.send(name)
  end




  ##============================================================##
  ## When Missing property is called
  ##============================================================##
  def method_missing(sym, *args, &block)
    # JulesLogger.info "method missing => #{sym}"
    return nil
  end






  ##============================================================##
  ## DICTONNARY BELOW
  ##============================================================##

  ##============================================================##
  ## B
  ##============================================================##
  def broker_bio
    convert_to_text(@user[:bio][I18n.locale])
  end

  def broker_certificate
    @user[:presentation_text][I18n.locale]
  end

  def broker_photo
    @user[:picture_url]
  end

  def broker_logo
    @user[:company_logo_url]
  end

  def broker_agency_name
    @user[:company_name]
  end

  def broker_agency_address
    add = []
    add << @user[:company_address]                                                                                  if @user[:company_address].present?
    add << "#{@user[:company_city]}#{@user[:company_zipcode].present? ? ", #{@user[:company_zipcode]}" : nil }"     if(@user[:company_city].present?)
    add.join("\n")
  end

  def broker_email
    @user[:email]
  end

  def broker_job
    @user[:presentation_text][I18n.locale]
  end

  def broker_name
    "#{@user[:first_name]} #{@user[:last_name]}"
  end

  def broker_phone
    @user[:phone]
  end

  def broker_phone_plus_label
    "#{I18n.t('dico.phone')} : #{@user[:phone]}" if @user[:phone].present?
  end

  def broker_phonemobile_plus_label
    "#{I18n.t('dico.mobile')} : #{@user[:phone_sms]}" if @user[:phone_sms].present?
  end


  def broker_website
    @user[:website]
  end


  ##============================================================##
  ## I
  ##============================================================##
  def image
    @image.is_a?(String) ? @image : @image[:large]
  end


  ##============================================================##
  ## M
  ##============================================================##
  def mls_id
    @complement.canada_mls.present? ? @complement.canada_mls : @property.uid
  end


  ##============================================================##
  ## P
  ##============================================================##
  def property_address1
    "#{@property[:address][:street_number]} #{@property[:address][:street_name]}"
  end


  def property_address_2_lines
    line_1 = "#{@property[:address][:street_number]} #{@property[:address][:street_name]}"
    line_2 = "#{@property[:address][:zipcode]} #{@property[:address][:locality]}"
    "#{line_1.present? ? "#{line_1}\n" : nil }#{line_2}"
  end

  def property_title
    @property[:title][I18n.locale]
  end

  def property_map
    "https://api.mapbox.com/v4/mapbox.streets/pin-m-marker+e3a21d(#{@property[:address][:longitude]},#{@property[:address][:latitude]})/#{@property[:address][:longitude]},#{@property[:address][:latitude]},14/500x500@2x.png?access_token=#{ENV['MAPBOX_API_TOKEN']}"
  end

  def property_floor
    @property[:level]
  end

  def property_cover
    @property[:cover_url]
  end

  def property_address
    "#{@property[:address][:street_number]} #{@property[:address][:street_name]} #{@property[:address][:zipcode]} #{@property[:address][:locality]}"
  end

  def property_price
    @property[:financial][:price_formated]
  end

  def property_price_if_not_sold
    if @property[:flag_id] == 5
      @property[:financial][:price_formated]
    else
      nil
    end
  end

  def property_classification_listing
    "#{@property[:classification][I18n.locale]} #{@property[:listing][I18n.locale]}"
  end

  def property_classification
    @property[:classification][I18n.locale]
  end

  def property_listing
    @property[:listing][I18n.locale]
  end

  def property_status
    @property[:status][I18n.locale]
  end

  def property_area_living
    @property.dig(:area, :formated, :living).present? ? @property.dig(:area, :formated, :living).html_safe : "-"
  end

  def property_area_balcony
    @property.dig(:area, :formated, :balcony).present? ? @property.dig(:area, :formated, :balcony).html_safe : "-"
  end

  def property_area_land
    @property.dig(:area, :formated, :land).present? ? @property.dig(:area, :formated, :land).html_safe : "-"
  end

  def property_bathrooms
    @property[:rooms][:bathrooms].present? ? @property[:rooms][:bathrooms] : "-"
  end

  def property_bedrooms
    @property[:rooms][:bedrooms].present? ? @property[:rooms][:bedrooms] : "-"
  end

  def property_rooms
    @property[:rooms][:rooms].present? ? @property[:rooms][:rooms] : "-"
  end

  def property_mensual_fees
    @property[:fees].present? ? @property[:formated][:fees] : "-"
  end

  def property_reference
    @property[:uid].present? ? "##{@property[:uid]}" : "##{@property[:id]}"
  end

  def property_available_date
    "#{I18n.t('app.availability')} : #{@property[:availability_date].present? ? I18n.localize(DateTime.parse(@property[:availability_date]), :format=>("%d %B %Y")) : I18n.t('app.immediately')}"
  end

  def property_link
    @property[:url].present? && @property[:url][I18n.locale].present? ? @property[:url][I18n.locale] : "https://www.shareimmo.com/ad/#{@property[:id]}"
  end

  def property_description
    convert_to_text(@property[:description][I18n.locale])
  end

  def property_description_short
    convert_to_text(@property[:description_short][I18n.locale])
  end

  def property_cover_url
    @property[:cover_url]
  end

  def property_france_dpe_indice
    @property[:france].present? && @property[:france][:dpe_indice].present? ?  @property[:france][:dpe_indice] : "-"
  end

  def property_france_dpe_value
    @property[:france].present? && @property[:france][:dpe_value].present? ?  @property[:france][:dpe_value] : "-"
  end

  def property_france_ges_indice
    @property[:france].present? && @property[:france][:ges_indice].present? ?  @property[:france][:ges_indice] : "-"
  end

  def property_france_ges_value
    @property[:france].present? && @property[:france][:ges_value].present? ?  @property[:france][:ges_value] : "-"
  end

  def property_france_alur_is_condo
    @property[:france].present? && @property[:france][:alur_is_condo] == 1 ?  I18n.t('yes') : I18n.t('no')
  end

  def property_france_alur_units
    @property[:france].present? && @property[:france][:alur_units].present? ?  @property[:france][:alur_units] : "-"
  end

  def property_france_alur_uninhabitables
    @property[:france].present? && @property[:france][:alur_uninhabitables].present? ?  @property[:france][:alur_uninhabitables] : "-"
  end

  def property_france_alur_spending
    @property[:france].present? && @property[:france][:alur_spending].present? ?  @property[:france][:alur_spending] : "-"
  end

  def property_france_alur_legal_action
    @property[:france].present? && @property[:france][:alur_legal_action] == 1 ?  I18n.t('yes') : I18n.t('no')
  end

  def property_mandat_exclusif
    @property[:france].present? && @property[:france][:exclusive_mandat] == 1 ? I18n.t('yes') : I18n.t('no')
  end

  def property_furnished
    @property.dig(:other, :inclusion, :furnished).present? ? (@property[:other][:inclusion][:furnished] == 1 ? I18n.t('yes') : I18n.t('no')) : "-"
  end

  def property_parking
    @property.dig(:other, :detail, :inside_parking).present? or @property.dig(:other, :detail, :outside_parking).present? ? ((@property.dig(:other, :detail, :inside_parking) == 1 or @property.dig(:other, :detail, :outside_parking) == 1) ?  I18n.t('yes') : I18n.t('no')) : "-"
  end

  def property_terrace
    @property.dig(:other, :exterior, :terrace).present? ? (@property.dig(:other, :exterior, :terrace) == 1 ? I18n.t('yes') : I18n.t('no')) : "-"
  end

  def property_garden
    @property.dig(:other, :exterior, :garden).present? ? (@property.dig(:other, :exterior, :garden) == 1 ? I18n.t('yes') : I18n.t('no')) : "-"
  end

  def property_pool
    @property.dig(:other, :detail, :inside_pool).present? or @property.dig(:other, :detail, :outside_pool).present? ? ((@property.dig(:other, :detail, :inside_pool) == 1 or @property.dig(:other, :detail, :outside_pool) == 1) ? I18n.t('yes') : I18n.t('no')) : "-"
  end

  def property_locality
    @property[:address][:locality]
  end

  def property_sublocality
    @property[:address][:sublocality]
  end

  def property_equipments
    text = ""
    if @property[:other][:inclusion].present?
      [:air_conditioning,:hot_water,:heated,:electricity,:furnished,:fridge,:cooker,:dishwasher,:washer,:dryer,:microwave].each do |i|
        text += "#{I18n.t("inclusion.#{i}")}\n" if @property[:other][:inclusion][i] == 1
      end
    end
    return text
  end


end
