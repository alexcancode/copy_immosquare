json.order_id   @order.id
json.status_id  @order.order_status_id
json.status     @order.order_status.name
json.title      "On met un titre"
json.preview    "#{root_url}/previews/orders/#{@order.id}/1.png"
