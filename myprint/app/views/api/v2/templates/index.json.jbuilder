json.templates @templates do |t|
  json.id             t.id
  json.type           t.print_template_type.name
  json.name           t.name
  json.color_editable t.color_editable
end
