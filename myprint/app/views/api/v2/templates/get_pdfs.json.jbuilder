json.max_properites @pdfs.pluck(:property_count).max
json.pdfs @pdfs do |pdf|
  json.id             pdf.id
  json.name           pdf.description.present? ? pdf.description : pdf.document_file_name
  json.property_count pdf.property_count
  json.locale         pdf.locale
end
