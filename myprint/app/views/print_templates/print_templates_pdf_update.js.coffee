<% if @print_template_pdf.errors.any? %>
swal("Failed to upload picture: <%= j @print_template_pdf.errors.full_messages.join(', ').html_safe %>")
<% else %>
$("#myPdf_edit").modal('hide')
$("#pdfList").html("<%= j render :partial => 'print_templates/print_template_pdfs', locals: {pdfs: @print_template_pdf.print_template.print_template_pdfs, print_template: @print_template_pdf.print_template, new_pdf: @new_pdf} %>")
<% end %>