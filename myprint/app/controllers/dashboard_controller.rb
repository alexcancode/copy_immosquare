class DashboardController < ApplicationController

  include ClientAuthorized


  ##============================================================##
  ##
  ##============================================================##
  def index
    @is_authorized = @store_response.present? && @store_response["authorizations"]["plans"].length > 0 && @store_response["authorizations"]["plans"][0]["status"] == "active"
    if @is_authorized
      response   = HTTParty.get("#{current_user.provider_host}/api/v1/properties?access_token=#{current_user.oauth_token}")
      @response  = JSON.parse(response.body).deep_symbolize_keys! if response.code == 200
      @orders    = Order.where(user_id: current_user.id)
    end
  end


  def showcase
    @templates  = PrintTemplate.includes(:print_template_type).where(:print_template_types=>{:slug=>"showcase"})
    @uid        = params[:uid]
  end




  def history
    @orders  = Order.where(user_id: current_user.id).order(:created_at => "desc")
  end



  private

end
