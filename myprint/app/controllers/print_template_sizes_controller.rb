class PrintTemplateSizesController < ApplicationController

  before_action :authenticate_user!
  layout "admin"

  def index
    @printTemplateSizes = PrintTemplateSize.where(:draft =>0).order("name")
  end

  def new
    @printTemplateSize = PrintTemplateSize.where(:draft => 1).first_or_create
    redirect_to edit_print_template_size_path(@printTemplateSize)
  end

  def edit
    @printTemplateSize = PrintTemplateSize.find(params[:id])
  end


  def update
    @printTemplateSize = PrintTemplateSize.find(params[:id])
    if @printTemplateSize.update_attributes(sizes_params)
      redirect_to print_template_sizes_path
    else
      render 'edit'
    end
  end

  def destroy
    @printTemplateSize = PrintTemplateSize.find(params[:id])
    @printTemplateSize.delete
    redirect_to print_template_sizes_path
  end


  def sizes_params
    params.require(:print_template_size).permit!
  end


end
