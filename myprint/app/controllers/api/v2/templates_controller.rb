class Api::V2::TemplatesController < Api::V2::ApplicationController

  ##============================================================##
  ## /api/v2/templates/:type?ids=:ids
  ##============================================================##
  def index
    begin
      @templates = PrintTemplate.where(:draft => 0, :print_template_type_id => params[:type_id])
      @templates = @templates.where(:id => params[:ids].split(",")) if params[:ids].present?
    rescue Exception => e
      return render :json =>{:errors=>e.message}, :status=> 402
    end
  end

  ##============================================================##
  ## /api/v2/templates/:id/pdfs
  ##============================================================##
  def get_pdfs
    template  = PrintTemplate.find(params[:id])
    @pdfs     = template.print_template_pdfs.where(:active => true, :draft => 0)
  end


end
