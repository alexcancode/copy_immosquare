class Api::V2::OrdersController < Api::V2::ApplicationController




  ##============================================================##
  ## /api/v2/orders/:id
  ## Pour voir le status d'une commande
  ##============================================================##
  def show
    begin
      @order = Order.find(params[:id])
    rescue Exception => e
      return render :json =>{:errors=>e.message}, :status=> 402
    end
  end


end
