class PrintTemplatesController < ApplicationController

  before_action :authenticate_user!

  layout "admin"
  require "color_converter"

  ##============================================================##
  ## PrintTemplate
  ##============================================================##
  def print_templates
    @print_templates = PrintTemplate.all
  end


  def print_templates_new
    @print_template = PrintTemplate.where(:draft => 1).first_or_create
    redirect_to admin_print_templates_edit_path(@print_template)
  end


  def print_templates_edit
    @print_template = PrintTemplate.find(params[:id])
    @new_pdf        = PrintTemplatePdf.where(:draft => 1).first_or_create
    @color          = ColorConverter.hex(@print_template.color_c,@print_template.color_m,@print_template.color_y,@print_template.color_k)
  end


  def print_templates_update
    @print_template = PrintTemplate.find(params[:id])
    @print_template.update_attributes(templates_params)
    flash[:success] = "saved"
    redirect_to admin_print_templates_edit_path(@print_template)
  end

  def print_templates_delete
    @print_template = PrintTemplate.find(params[:id])
    @print_template.destroy
    redirect_to admin_print_templates_path
  end




  ##============================================================##
  ## PrintTemplate Pdf
  ##============================================================##
  def print_templates_pdf_edit
    @print_template_pdf = PrintTemplatePdf.find(params[:id])
    @print_template_id = params[:print_template_id]
  end

  def print_templates_pdf_dictionary
    @print_template_pdf = PrintTemplatePdf.find(params[:id])
  end

  def print_templates_pdf_update
    @print_template_pdf = PrintTemplatePdf.find(params[:id])
    @print_template_pdf.update_attributes(print_template_pdfs_params)

    @new_pdf = PrintTemplatePdf.where(:draft => 1).first_or_create
  end


  def print_templates_pdf_deactivate
    @pdf = PrintTemplatePdf.find(params[:id])
    @pdf.update_attribute(:active, false)
    redirect_to admin_print_templates_edit_path(@pdf.print_template)
  end

  def print_templates_pdf_reactivate
    @pdf = PrintTemplatePdf.find(params[:id])
    @pdf.update_attribute(:active, true)
    redirect_to admin_print_templates_edit_path(@pdf.print_template)
  end







  private

  def templates_params
    params.require(:print_template).permit!
  end

  def print_template_assets_params
    params.require(:print_template_asset).permit!
  end

  def print_template_pdfs_params
    params.require(:print_template_pdf).permit!
  end





end
