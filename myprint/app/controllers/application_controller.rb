class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_locale


  def set_locale
    I18n.locale = params[:locale]
  end

  ##============================================================##
  ## After Login
  ##============================================================##
  def after_sign_in_path_for(user)
    dashboard_path
  end

  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {locale: I18n.locale}
  end



end
