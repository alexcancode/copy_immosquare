class FontsController < ApplicationController

  before_action :authenticate_user!
  layout "admin"

  def index
    @fonts = Font.where(:draft =>0).order("font_file_name")
  end

  def new
    @font = Font.where(:draft => 1).first_or_create
    redirect_to edit_font_path(@font)
  end

  def edit
    @font = Font.find(params[:id])
  end

  def update
    JulesLogger.info params
    @font = Font.find(params[:id])
    if @font.update_attributes(fonts_params)
      redirect_to fonts_path
    else
      render 'edit'
    end
  end

  def destroy
    @font = Font.find(params[:id])
    @font.delete
    redirect_to fonts_path
  end


  def fonts_params
    params.require(:font).permit!
  end


end
