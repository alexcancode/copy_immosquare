class PreviewsController < ApplicationController

  require 'color_converter'
  require 'json'

  ##============================================================##
  ## Iframe to populate dictionary
  ##============================================================##
  def show
    begin

      ##============================================================##
      ## Download pdf Locally & Parse it to get dictionary
      ##============================================================##
      @pdf             = PrintTemplatePdf.find(params[:pdf])
      raise "PDF not set"           if @pdf.blank?
      raise "Dictionnary is empty"  if @pdf.dictionary.blank?

      @print_template  = @pdf.print_template
      raise "template not set"        if @print_template.blank?
      raise "uids not set"            if params[:uids].blank?

      @order = Order.where(:uid  => params[:uids], :print_template_id  => @print_template.id).where('order_status_id < ?',4).first_or_create

      ##============================================================##
      ## Update Order
      ##============================================================##
      @order.update_attributes(
        :order_status_id        => 1,
        :print_template_pdf_id  => @pdf.id,
        :channel                => SecureRandom.urlsafe_base64,
        :picture_url            => nil,
        :address                => nil,
        :user_id                => current_user.present? ? current_user.id : nil
        )

      ##============================================================#
      ## Go fetch property(ies)
      ##============================================================##
      host = Rails.env.development? ? "http://lvh.me:3000" : "https://shareimmo.com"

      @properties = []
      @pictures   = []
      i = 1

      if params[:uids].include?(",")
        uids = params[:uids].split(",")
        raise "Incorrect number of properties. Is #{uids.length} but should be #{@pdf.property_count}" if uids.length != @pdf.property_count
        uids.each do |uid|
          url  = "#{host}/api/v2/search/properties/#{uid}?apiKey=#{ENV['shareimmo_api_key']}&apiToken=#{ENV['shareimmo_api_token']}"
          call  = HTTParty.get(url)
          raise "api Error #1" if call.code != 200
          response = JSON.parse(call.body).deep_symbolize_keys
          @properties << response
        end
        @properties.each do |p|
          p[:pictures][0..([1, p[:pictures].length].min)].each do |picture|
            @pictures << {
              :uid      => i,
              :property => p[:id],
              :pictures => picture
            }
            i += 1
          end if p[:pictures].present?
        end
      else
        url   = "#{host}/api/v2/search/properties/#{params[:uids]}?apiKey=#{ENV['shareimmo_api_key']}&apiToken=#{ENV['shareimmo_api_token']}"
        call  = HTTParty.get(url)
        raise "api Error #2" if call.code != 200
        response = JSON.parse(call.body).deep_symbolize_keys
        @properties << response
        # get images
        @properties.each do |p|
          p[:pictures].each do |picture|
            @pictures << {
              :uid      => i,
              :property => p[:id],
              :pictures => picture
            }
            i += 1
          end
        end
      end

      ##============================================================#
      ## Go fetch other properties if needed
      ##============================================================##
      additional_pictures = []
      if params[:additional].present?
        # we want to fetch other properties infos
        uids = params[:additional].split(",")
        uids.each do |uid|
          url  = "#{host}/api/v2/search/properties/#{uid}?apiKey=#{ENV['shareimmo_api_key']}&apiToken=#{ENV['shareimmo_api_token']}"
          call  = HTTParty.get(url)
          raise "api Error #3" if call.code != 200
          response = JSON.parse(call.body).deep_symbolize_keys

          # get images
          [response].each do |p|
            p[:pictures].each do |picture|
              additional_pictures << {
                :uid      => i,
                :property => p[:id],
                :pictures => picture
              }
              i += 1
            end
          end
        end
      end


      @pictures.concat(additional_pictures)

      ##============================================================#
      ## Go fetch user
      ##============================================================##
      url  = "#{host}/api/v2/search/accounts/#{@properties.first[:account_id]}?apiKey=#{ENV['shareimmo_api_key']}&apiToken=#{ENV['shareimmo_api_token']}"
      call  = HTTParty.get(url)
      raise "api Error #4" if call.code != 200
      response_user = JSON.parse(call.body).deep_symbolize_keys
      @user = response_user

      ##============================================================#
      ## Populate Dic
      ##============================================================##
      @dictionary = DictionaryPopulater.new(
        :pdf          => @pdf,
        :properties   => @properties,
        :user         => @user
        ).populate_dictionary

      @dictionary.deep_symbolize_keys!

      # @dictionary = {:document=>{:page_count=>2, :pdf_version=>"1.6", :title=>nil, :page_height=>1063, :page_width=>2313, :primary_color=>[50, 50, 50, 50]}, :blocks=>[{:name=>"broker_job", :type=>"text", :description=>"Votre profession", :page=>1, :position=>{:width=>612, :height=>70, :top=>920, :left=>56}, :url=>nil, :textflow=>false, :content=>"professionnel de l'immobilier de luxe", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"broker_name", :type=>"text", :description=>"Votre nom", :page=>1, :position=>{:width=>612, :height=>77, :top=>837, :left=>56}, :url=>nil, :textflow=>false, :content=>"Cyrielle Bocquet", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"broker_phone_plus_label_and_broker_phonemobile_plus_label_with_slash", :type=>"text", :description=>"Vos coordonnées téléphoniques", :page=>1, :position=>{:width=>827, :height=>49, :top=>896, :left=>959}, :url=>nil, :textflow=>false, :content=>"Tél : 514 568-6780 /  Cell : 514 568-6780", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"broker_photo", :type=>"image", :description=>"Votre photo", :page=>1, :position=>{:width=>750, :height=>806, :top=>0, :left=>0}, :url=>"https://s3.amazonaws.com/shareimmo/development/brokers/2/picture_original.jpg", :textflow=>false, :content=>nil, :editable=>"false", :properties=>[]}, {:name=>"image#1", :type=>"image", :description=>nil, :page=>1, :position=>{:width=>893, :height=>577, :top=>97, :left=>1021}, :url=>nil, :textflow=>false, :content=>nil, :editable=>"true", :properties=>[{:name => "p9:opacity", :value => "0.4", :type=>"string"}, {:name=>"p9:editable", :value=>"true", :type=>"string"}, {:name=>"p9:layer", :value=>"2", :type=>"string"}]}, {:name=>"property#1_address_and_property#1_location_with_slash", :type=>"text", :description=>"Adresse Propriété #1", :page=>1, :position=>{:width=>907, :height=>60, :top=>691, :left=>1018}, :url=>nil, :textflow=>false, :content=>"1740 Rue Saint-Patrick H3K 2H2 Montréal ", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"1", :type=>"string"}]}, {:name=>"property#1_listing", :type=>"text", :description=>nil, :page=>1, :position=>{:width=>122, :height=>573, :top=>101, :left=>1796}, :url=>nil, :textflow=>false, :content=>"À Louer", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"4", :type=>"string"}]}, {:name=>"broker_address", :type=>"text", :description=>"Votre adresse", :page=>2, :position=>{:width=>678, :height=>46, :top=>861, :left=>726}, :url=>nil, :textflow=>false, :content=>"", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"broker_email", :type=>"text", :description=>"Votre courriel", :page=>2, :position=>{:width=>674, :height=>46, :top=>920, :left=>730}, :url=>nil, :textflow=>false, :content=>"contact@immosquare.com", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"broker_job", :type=>"text", :description=>"Votre profession", :page=>2, :position=>{:width=>511, :height=>46, :top=>955, :left=>42}, :url=>nil, :textflow=>false, :content=>"professionnel de l'immobilier de luxe", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"broker_name", :type=>"text", :description=>"Votre nom", :page=>2, :position=>{:width=>511, :height=>77, :top=>865, :left=>42}, :url=>nil, :textflow=>false, :content=>"Cyrielle Bocquet", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"broker_phone_plus_label_and_broker_phonemobile_plus_label_with_slash", :type=>"text", :description=>"Vos coordonnées téléphoniques", :page=>2, :position=>{:width=>674, :height=>39, :top=>976, :left=>730}, :url=>nil, :textflow=>false, :content=>"Tél : 514 568-6780 /  Cell : 514 568-6780", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"broker_slogan", :type=>"text", :description=>"Votre slogan", :page=>2, :position=>{:width=>1035, :height=>139, :top=>69, :left=>77}, :url=>nil, :textflow=>true, :content=>"Un service personnalisé et attentionné qui tient compte de vos impératifs !", :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"image#2", :type=>"image", :description=>"Image Annonce #2", :page=>2, :position=>{:width=>514, :height=>393, :top=>243, :left=>185}, :url=>nil, :textflow=>false, :content=>nil, :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"image#3", :type=>"image", :description=>nil, :page=>2, :position=>{:width=>511, :height=>393, :top=>243, :left=>865}, :url=>nil, :textflow=>false, :content=>nil, :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"image#4", :type=>"image", :description=>nil, :page=>2, :position=>{:width=>518, :height=>393, :top=>243, :left=>1546}, :url=>nil, :textflow=>false, :content=>nil, :editable=>"true", :properties=>[{:name=>"p9:editable", :value=>"true", :type=>"string"}]}, {:name=>"property#2_address_and_property#2_location_with_slash", :type=>"text", :description=>"Adresse propriété #2", :page=>2, :position=>{:width=>514, :height=>28, :top=>653, :left=>185}, :url=>nil, :textflow=>false, :content=>"30 Allée des Brises-du-Fleuve H4G3M7 Montréal ", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"1", :type=>"string"}]}, {:name=>"property#2_listing", :type=>"text", :description=>nil, :page=>2, :position=>{:width=>84, :height=>393, :top=>243, :left=>615}, :url=>nil, :textflow=>false, :content=>"À Vendre", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"3", :type=>"string"}]}, {:name=>"property#2_price_if_not_sold", :type=>"text", :description=>"Prix annonce #2", :page=>2, :position=>{:width=>514, :height=>28, :top=>691, :left=>185}, :url=>nil, :textflow=>false, :content=>"", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"1", :type=>"string"}]}, {:name=>"property#3_address_and_property#3_location_with_slash", :type=>"text", :description=>"Adresse Propriété #3", :page=>2, :position=>{:width=>511, :height=>28, :top=>653, :left=>865}, :url=>nil, :textflow=>false, :content=>"1342 Belvédèresud J1H4E3 Sherbrooke ", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"1", :type=>"string"}]}, {:name=>"property#3_listing", :type=>"text", :description=>nil, :page=>2, :position=>{:width=>84, :height=>393, :top=>243, :left=>1292}, :url=>nil, :textflow=>false, :content=>"À Louer", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"3", :type=>"string"}]}, {:name=>"property#3_price_if_not_sold", :type=>"text", :description=>"Prix annonce #3", :page=>2, :position=>{:width=>511, :height=>32, :top=>688, :left=>865}, :url=>nil, :textflow=>false, :content=>"", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"1", :type=>"string"}]}, {:name=>"property#4_address_and_property#4_location_with_slash", :type=>"text", :description=>"Adresse Propriété #4", :page=>2, :position=>{:width=>518, :height=>28, :top=>653, :left=>1546}, :url=>nil, :textflow=>false, :content=>"1740 Rue Saint-Patrick H3K 2H2 Montréal ", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"1", :type=>"string"}]}, {:name=>"property#4_listing", :type=>"text", :description=>nil, :page=>2, :position=>{:width=>84, :height=>393, :top=>243, :left=>1980}, :url=>nil, :textflow=>false, :content=>"À Vendre", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"3", :type=>"string"}]}, {:name=>"property#4_price_if_not_sold", :type=>"text", :description=>"Prix annonce #4", :page=>2, :position=>{:width=>518, :height=>32, :top=>688, :left=>1546}, :url=>nil, :textflow=>false, :content=>"123 €", :editable=>"false", :properties=>[{:name=>"p9:case", :value=>"uppercase", :type=>"string"}, {:name=>"p9:layer", :value=>"1", :type=>"string"}]}]}

      ##============================================================##
      ## Setup Variable
      ##============================================================##
      @order.update_attribute(:dictionary,@dictionary)

      @textlines = Hash.new
      @textflows = Hash.new
      (1..(@dictionary[:document][:page_count].to_i)).each do |page|
        @textlines[page]  = @dictionary[:blocks].select {|b| b[:type] == 'text' && b[:editable] == 'true' && b[:textflow] == false && b[:page] == page}  if @dictionary[:blocks].present?
        @textflows[page]  = @dictionary[:blocks].select {|b| b[:type] == 'text' && b[:editable] == 'true' && b[:textflow] == true  && b[:page] == page}  if @dictionary[:blocks].present?

        # @textlines[page] = @dictionary[:blocks].select {|b| b[:type] == 'text' && b[:editable] == 'false' && b[:textflow] == false && b[:page] == page}  if @dictionary[:blocks].present?
        # @textflows[page] = @dictionary[:blocks].select {|b| b[:type] == 'text' && b[:editable] == 'false' && b[:textflow] == false && b[:page] == page}   if @dictionary[:blocks].present?
      end

      # @print_template.color_editable = 1

    rescue Exception => e
      @error = "#{e.message}\n#{e.backtrace}"
      render 'error'
    end
  end

  ##============================================================##
  ## To producde pdf
  ##============================================================##
  def generate
    @order = Order.find(params[:id])
    @order.update_attributes(
      :dictionary       => JSON.parse(params[:dictionary]),
      :order_status_id  => 2,
      :preview_count    => @order.preview_count+1
      )
    PreviewWorker.perform_async(
      :order_id   => @order.id,
      :watermark  => nil
      )
    render nothing: true
  end


  ##============================================================##
  ## Download preview PDF
  ##============================================================##
  def download_preview_pdf
    begin
      @order = Order.find(params[:id])
      send_file(@order.pdf_result_path,:filename=>"myprint_#{@order.id}.pdf",:disposition => 'inline',:type => "application/pdf")
    rescue Exception => e
      flash[:notice] = "#{e.message}"
      redirect_to root_path
    end
  end



  private

  def fetch_multi_properties(ids)

  end

  def fetch_single_property(id)

  end

end
