class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController


  def gercopstore
    auth    = request.env['omniauth.auth'].to_hash.deep_symbolize_keys!
    store   = auth[:extra][:raw_info][:store]
    client  = auth[:extra][:raw_info][:client]
    company = auth[:extra][:raw_info][:company]


    ##============================================================##
    ## On create
    ##============================================================##
    @user = User.where(:provider => auth[:provider], :uid => auth[:uid]).first_or_create do |user|
      user.email                    = "#{auth[:uid]}@#{auth[:provider]}.com"
      user.password                 = Devise.friendly_token[0,20]
    end

    ##============================================================##
    ## On create/update
    ##============================================================##
    @user.update_attributes(
      :oauth_token      => auth[:credentials][:token],
      :provider_host    => store[:url]
      )


    ##============================================================##
    ## Sign in @user
    ##============================================================##
    if @user.persisted?
      sign_in_and_redirect(@user, :event => :authentication)
      set_flash_message(:notice, :success, :kind => auth[:provider]) if is_navigational_format?
    else
      session["devise.storeimmo_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end




  def failure
    flash[:message] = env["omniauth.error"]
    redirect_to root_path
  end
end
