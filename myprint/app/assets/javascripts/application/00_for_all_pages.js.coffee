##============================================================##
## Spinner
##============================================================##
@spin = (on_off) =>
  if on_off
    $("#layer-full").show()
  else
    $("#layer-full").hide()

##============================================================##
## Delay
##============================================================##
@delay = (->
    timer = 0
    (callback, ms) ->
      clearTimeout timer
      timer = setTimeout(callback, ms)
      return
  )()





$(document).ready ->
  $('.bootstrapNumber').bootstrapNumber()
  $('.chosen-select').chosen()

  ##============================================================##
  ## Remove flash Notice
  ##============================================================##
  delay ->
    $('.flash-notice').fadeOut()
  ,8000


