$('body.print_templates.print_templates_edit').ready ->

  ##============================================================##
  ## Color CMYK preview
  ##============================================================##
  $("#print_template_color_c,#print_template_color_m,#print_template_color_y,#print_template_color_k").on 'blur', (e) ->
    cmyk = "cmyk(#{$("#print_template_color_c").val()}%,#{$("#print_template_color_m").val()}%,#{$("#print_template_color_y").val()}%,#{$("#print_template_color_k").val()}%)"
    $('#color_test').css
      "background-color" : w3color(cmyk).toRgbString()



