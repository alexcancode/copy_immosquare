$('body.previews.show').ready ->

  $('.tab-link').tab()

  ##============================================================##
  # update remaining characters and disable input if too many
  ##============================================================##
  _.each $('.textLimit'), (elem) ->
    $(elem).html $('.charCount[data-name="'+$(elem).data("name")+'"]').data("maxchar") - twttr.txt.getTweetLength($('.charCount[data-name="'+$(elem).data("name")+'"]').val())
  
  $('.charCount').keyup ->
    remainingCharacters = $(@).data("maxchar") - twttr.txt.getTweetLength($(@).val())
    $('.textLimit[data-name="'+$(@).data("name")+'"]').html remainingCharacters

  ##============================================================##
  ## Drag&Drop
  ##============================================================##
  interacting = false
  interact(".draggable")
    .draggable
      autoScroll: false
      inertia: false
      onmove: (event) ->
        interaction = event.interaction
        if interaction.pointerIsDown && !interacting && interaction.interacting()
          original  = event.target
          url       = $('img', original).attr('src')
          imgWidth  = $('img', original).innerWidth()
          imgHeight = $('img', original).innerHeight()
          imgTop = $('img', original).offset().top
          imgLeft = $('img', original).offset().left
          check     = $(original).data('check')
          clone = $("<div class='bg-cover draggableClone' style='z-index:10000000;-webkit-transform: translate(0px, 0px);transform: translate(0px, 0px);position:absolute;left:#{imgLeft}px;top:#{imgTop}px;width:#{imgWidth}px;height:#{imgHeight}px;background-image:url(#{url})' data-url=#{url} data-check=#{check}></div>")
          $("body").append(clone)
          interacting = true;
          interaction.start({ name: 'drag'},event.interactable,clone[0])

        target  = event.target
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy
        target.style.webkitTransform = target.style.transform = "translate(#{x}px,#{y}px)"
        target.setAttribute('data-x',x)
        target.setAttribute('data-y',y)

        targetClone  = $('.draggableClone')[0]
        x = (parseFloat(targetClone.getAttribute('data-x')) || 0) + event.dx
        y = (parseFloat(targetClone.getAttribute('data-y')) || 0) + event.dy
        targetClone.style.webkitTransform = targetClone.style.transform = "translate(#{x}px,#{y}px)"
        targetClone.setAttribute('data-x',x)
        targetClone.setAttribute('data-y',y)
      onend: (event) ->
        $('.draggableClone')[0].remove()
        interacting = false;
        target  = event.target
        target.style.webkitTransform = 'translate(' + 0 + 'px, ' + 0 + 'px)'
        target.style.transform = 'translate(' + 0 + 'px, ' + 0 + 'px)'
        target.setAttribute('data-x', 0)
        target.setAttribute('data-y', 0)




  interact('.dropzone').dropzone
    accept: '.draggable'
    overlap: 0.30

    ondragenter: (event) ->
      drag = event.relatedTarget
      drop = event.target
      $(drop).addClass('dragEnter')

    ondragleave: (event) ->
      drag = event.relatedTarget
      drop = event.target
      $(drop).removeClass('dragEnter')

    ondrop: (event) ->
      # set elements
      drag = event.relatedTarget
      drop = event.target

      # update background image
      url  = $(drag).data('image')
      $(drop).css
        backgroundImage: "url('#{url}')"
      $(drop).removeClass('dragEnter')

      # show bin to delete image
      bin = $(drop).parent().find("div.delete_image[data-name='"+$(drop).data('name')+"']")
      bin.show()

      # update image block in dictionary
      block = _.first(_.where(dictionary.blocks,{name: $(drop).data('name')}))
      block.url = url

      # uncheck previous image and check new dropped image (we keep the id in the data to retreive it later)
      $('#'+bin.data("check")).hide()
      check = $('#'+drag.getAttribute('data-check'))
      check.show()
      bin.data('check', check[0].id)


  # Delete image when clicking on little bin
  $("div.delete_image").on "click", (e) ->
    div = $("div[data-name='"+$(@).data('name')+"']")
    $('#'+div.data('check')).hide()
    div.css("backgroundImage", "none")
    $(@).hide()





  ##============================================================##
  ## Slider
  ##============================================================##
  $('#owl-slider').owlCarousel
    loop:false
    margin:10
    nav:true
    navText: ["Précédent","Suivant"]
    dots: true
    items:1
    responsiveClass: true
    center: true


  ##============================================================##
  ## Intreaction du formulaire
  ##============================================================##
  if typeof dictionary isnt 'undefined'
    ##============================================================##
    ## Submit Generation with Javascript Dictionary
    ##============================================================##
    $('a#startGeneration').on 'click', (e) ->
      $("#appView").hide()
      $('#appViewResult').show()
      spin(true)
      $.post
        url: $(@).data('action')
        dataType: 'json'
        data:
          dictionary: JSON.stringify(dictionary)

    ##============================================================##
    ## Back to first page
    ##============================================================##
    $('a#backToStep1').on 'click', (e) ->
      $("#appView").show()
      $('#appViewResult, #owl-slider, #appViewResultActions').hide()


    ##============================================================##
    ## Pusher Management
    ##============================================================##
    pusher = new Pusher pusher_key,
      encrypted: true
    pusher.subscribe(channel).bind 'my_event', (data) ->
      if(typeof data.pdf_is_ready isnt 'undefined')
        spin(false)
        _.each data.magazine, (page_url,key) ->
          $("#magazine_page_#{key+1} img").attr('src',page_url)
        $('#owl-slider, #appViewResultActions').show()
        $('#backtostore').attr('href',"#{store_url}?order=#{data.order_id}#prices_container")


      if(data.status is false)
        $('#stepName').html("Error<br>"+data.error)
        spin(false)
      else
        $('#stepName').html(data.step)



    ##============================================================##
    ## Texts Reset
    ##============================================================##
    $('span.textPrintDescriptionReset').on 'click', ->
      textarea = $("textarea[data-name='#{$(@).data('name')}']")
      textarea.val($(@).data('reset'))
      textarea.trigger("keyup")

    ##============================================================##
    ## Cette fonction sert à attendre avant de lancer un
    ## callback
    ##============================================================##
    typewatch = do ->
      timer = 0
      (callback, ms) ->
        clearTimeout timer
        timer = setTimeout(callback, ms)
        return
    $('input,textarea').on 'keyup', ->
      # update dictionary when writing texts
      typewatch =>
        current_text          = _.first(_.where(dictionary.blocks,{name: $(@).data('name'), page: $(@).data('page')}))
        current_text.content  = $(@).val()
      , 100
