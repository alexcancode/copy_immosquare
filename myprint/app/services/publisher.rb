class Publisher

  ##============================================================##
  ## Bunny Connection
  ##============================================================##
  def self.connection
    @connection ||= Bunny.new(:hostname => ENV["amqp_server"], :username => ENV["amqp_username"], :password => ENV["amqp_password"], :vhost => ENV["amqp_vhost"]).tap do |c|
      c.start
    end
  end

  ##============================================================##
  ## Init Channel
  ##============================================================##
  def self.channel
    @channel ||= connection.create_channel
  end


  ##============================================================##
  ## In order to publish message we need a exchange name.
  ## Note that RabbitMQ does not care about the payload -
  ## we will be using JSON-encoded strings
  ##============================================================##
  def self.publish(queue, message = {})
    x = channel.default_exchange
    x.publish(message.to_json,:persistent => true, :routing_key =>queue)
  end


end
