class NotifierMailer < ApplicationMailer

  default :template_path => "mailers/notifier_mailer"

  def test_pdf_mail(order)
    @order = order
    @title = "New pdf"
    mail(
      :to => "jules@immosquare.com",
      :from => set_my_from("no-reply"),
      :subject => set_my_subject(@title)
      )
  end


end
