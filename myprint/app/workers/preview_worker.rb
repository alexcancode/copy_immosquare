class PreviewWorker
  include Sidekiq::Worker

  require 'color_converter'
  require 'open-uri'
  require 'net/https'


  def perform(args)
    begin
      args        = args.deep_symbolize_keys
      @order      = Order.find(args[:order_id])
      @watermark  = args[:watermark]
      raise "This order have already been completed" if @order.order_status_id > 3
      @dictionary = @order.dictionary.deep_symbolize_keys

      JulesLogger.info "start Preview Worker order ##{@order.id} (#{@order.env})"

      ##============================================================##
      ## Step 1 : On définie les chemins
      ##============================================================##
      JulesLogger.info "Start step 1"
      Pusher.trigger(@order.channel,'my_event',{:step => I18n.t('print.step_1')})
      original  = @order.print_template_pdf
      raise "Source Pdf is missing" if original.blank?
      original_path  = original.pdf_original_path
      FileUtils.mkdir_p(File.dirname(original_path))  unless File.exists?(File.dirname(original_path))
      unless File.exists?(original_path)
        open(original_path, 'wb') do |file|
          file.write HTTParty.get(original.document.url).parsed_response
        end
      end
      result_path           = @order.pdf_result_path
      result_watermark_path = @order.pdf_result_watermark_path
      preview_folder_path = "#{Rails.root}/public/previews/orders/#{@order.id}"
      temp_folder_path    = "#{Rails.root}/tmp/print/orders/#{@order.id}"
      FileUtils.rm_rf(Dir.glob(preview_folder_path))
      FileUtils.rm_rf(Dir.glob(temp_folder_path))
      FileUtils.mkdir_p(File.dirname(result_path))            unless File.directory?(File.dirname(result_path))
      FileUtils.mkdir_p(File.dirname(result_watermark_path))  unless File.directory?(File.dirname(result_watermark_path))
      FileUtils.mkdir_p(preview_folder_path)                  unless File.directory?(preview_folder_path)
      FileUtils.mkdir_p(temp_folder_path)                     unless File.directory?(temp_folder_path)


      ##============================================================##
      ## Step 2 : On récupère les images du template localement
      ##============================================================##
      JulesLogger.info "Start step 2"
      images = @dictionary[:blocks].select{|b| b[:type] == "image"}
      pics_count  = images.size
      images.each_with_index do |image,x|
        # image[:url] = "http://placehold.it/#{image[:position][:width]}x#{image[:position][:height]}?text=picture+#{x}+(#{image[:position][:width]}x#{image[:position][:height]}).png" if !remote_file_exists?(image[:url])
        image[:url] = "http://placehold.it/#{image[:position][:width]}x#{image[:position][:height]}/ffffff?text=+" if !remote_file_exists?(image[:url])
        new_pic_nane = "#{temp_folder_path}/#{File.basename(image[:url])}"
        open(new_pic_nane, 'wb') do |file|
          file.write HTTParty.get(image[:url]).parsed_response
          file.close
        end unless File.exists?(new_pic_nane)
        image[:content] = new_pic_nane
        Pusher.trigger(@order.channel, 'my_event',{:step => I18n.t('print.step_2',:number =>x+1,:count =>pics_count)})
      end



      ##============================================================##
      ## Step 3 : PDFLIB Writter
      ##============================================================##
      JulesLogger.info "Start step 3"
      Pusher.trigger(@order.channel, 'my_event',{:step => I18n.t('print.step_3')})
      pdf = Mypdflib::PdfWritter.new(:original => original_path,:result => result_path, :dictionary =>@dictionary).produce
      raise pdf if pdf != true

      ##============================================================##
      ## Step3 bis : Create Watermarked PDF
      ##============================================================##
      if @watermark.present?
        pdf = Mypdflib::PdfWatermark.new(:original => result_path,:result => result_watermark_path, :watermark =>@watermark).produce
        raise pdf if pdf != true
      end


      ##============================================================##
      ## Step 4 : Create images
      ##============================================================##
      JulesLogger.info "Start step 4"
      Pusher.trigger(@order.channel, 'my_event',{:step => I18n.t('print.step_4')})
      pdf_to_png    = "gs -dUseTrimBox -dMaxBitmap=2147483647 -sDEVICE=png16m -r600 -dDownScaleFactor=3 -o #{preview_folder_path}/%d.png #{@watermark.present? ? result_watermark_path : result_path }"
      system(pdf_to_png)


      ##============================================================##
      ## Step 5 : Clean
      ## We have to clean because there were bugs when editing a pdf file
      ##============================================================##
      JulesLogger.info "Start step 5"
      File.delete(original_path)

      ##============================================================##
      ## Step 6 : Final Response
      ##============================================================##
      JulesLogger.info "Start step 6"
      @order.update_attribute(:order_status_id,3)
      magazine = Array.new
      (1..@dictionary[:document][:page_count].to_i).each do |i|
        magazine.push("#{preview_folder_path.split("public").last}/#{i}.png?#{rand(20000)}")
      end
      Pusher.trigger(@order.channel, 'my_event',{:step => nil,:pdf_is_ready => true,:order_id => @order.id,:magazine => magazine})
      JulesLogger.info "DONE"

    rescue Exception => e
      JulesLogger.info "Error : #{e.message}"
      JulesLogger.info e.backtrace.join("\n")
      Pusher.trigger(@order.channel, 'my_event',{:status => false, :error =>"#{e.message}"})
    end
  end


  def remote_file_exists?(url)
    begin
      url = URI.parse(url)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = (url.scheme == "https")
      return [200].include?(http.head(url.request_uri).code.to_i)
    rescue Exception => e
      return false
    end
  end





end
