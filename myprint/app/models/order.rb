class Order < ApplicationRecord

  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :dictionary, JSON

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :print_template
  belongs_to :print_template_pdf
  belongs_to :api_provider
  belongs_to :order_status


  def pdf_result_path
    "#{Rails.root}/pdfs/results/#{self.id}/result_#{self.id}.pdf"
  end

  def pdf_result_watermark_path
    "#{Rails.root}/pdfs/results/#{self.id}/result_#{self.id}_watermarked.pdf"
  end

end
