class PrintTemplatePdf < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :print_template


  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :dictionary, JSON


  after_update :set_dictionary_and_images

  def set_dictionary_and_images
    ##============================================================##
    ## On telecharge le pdf localement puis on le parse
    ## pour récupérer son dictionnaire
    ##============================================================##
    if self.document_updated_at_changed?
      dictionary = Mypdflib::PdfParser.new(:pdf=>self.document.queued_for_write[:original].path).parse
      self.update_column(:dictionary, dictionary)
    end

    ##============================================================##
    ## On découpe le model pour créer des images
    ##============================================================##
    if self.model_updated_at_changed?
      locale_model = self.model.queued_for_write[:original].path

      # découpe des images
      locale_images = File.dirname(locale_model)
      pdf_to_png = "gs -dMaxBitmap=2147483647 -sDEVICE=png16m -r600 -dDownScaleFactor=3 -o #{locale_images}/%d.png #{locale_model}"
      system(pdf_to_png)

      # envoi des images sur amazon
      Dir.glob("#{locale_images}/*.png").each do |image|
        cmd_white_filter = "convert #{image} -fill white -colorize 50% #{image}"
        system(cmd_white_filter)
        s3 = Aws::S3::Resource.new
        obj = s3.bucket(ENV["aws_bucket"]).object("#{Rails.env.production? ? Rails.env : "development"}/print_templates/#{self.print_template_id}/pdfs/#{self.id}_#{File.basename(image)}")
        obj.upload_file(image, acl:'public-read')
      end
    end
  end



  def pdf_original_path
    "#{Rails.root}/pdfs/originals/#{self.print_template_id}/#{self.id}/#{File.basename(self.document.url)}"
  end



  ##============================================================##
  ##
  ##============================================================##
  Paperclip.interpolates :print_template_id do |attachment, style|
    attachment.instance.print_template_id
  end

  #============================================================##
  ## Paperclip : Document
  ##============================================================##
  has_attached_file :document,
  :path => "#{Rails.env.production? ? Rails.env : "development"}/print_templates/:print_template_id/pdfs/:id.:extension",
  :use_timestamp => false
  validates_attachment_content_type :document, :content_type => ['application/pdf']


  #============================================================##
  ## Paperclip : Model
  ##============================================================##
  has_attached_file :model,
  :path => "#{Rails.env.production? ? Rails.env : "development"}/print_templates/:print_template_id/pdfs/:id_model.:extension",
  :use_timestamp => false
  validates_attachment_content_type :model, :content_type => ['application/pdf']

end
