class User < ActiveRecord::Base


  ##============================================================##
  ## Association
  ##============================================================##
  rolify
  has_many :myprint_orders
  ##============================================================##
  ## Devise
  ##============================================================##
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable,:omniauthable, :omniauth_providers => [:storeimmo,:gercopstore]


  def check_role?
    return true if self.has_any_role? :admin, :manager
  end


end
