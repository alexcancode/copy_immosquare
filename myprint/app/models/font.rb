class Font < ApplicationRecord


  #============================================================##
  ## Paperclip : Document
  ##============================================================##
  has_attached_file :font,
  :storage => :filesystem,
  :path => "#{Mypdflib.stuff_folder}/:filename"

  validates_attachment_file_name :font, :matches => [/ttf\Z/, /otf\Z/]

end
