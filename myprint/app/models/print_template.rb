class PrintTemplate < ApplicationRecord


  ##============================================================##
  ## Association
  ##============================================================##
  has_many :print_template_pdfs, dependent: :destroy
  belongs_to :print_template_type
  belongs_to :print_template_size


  private



end
