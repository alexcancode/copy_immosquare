set :application, "myprint"
set :repo_url, "git@bitbucket.org:wgroupe/myprint.git"
set :bundle_roles, :all
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle  public/previews pdfs}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

