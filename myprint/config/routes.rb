require 'sidekiq/web'

Rails.application.routes.draw do

  ##============================================================##
  ## API
  ##============================================================##
  namespace :api, defaults: {format: 'json'} do
    namespace :v2 do

      ##============================================================##
      ## Orders
      ##============================================================##
      match "orders/:id"    => 'orders#show',           :via => :get
      ##============================================================##
      ## Templates
      ##============================================================##
      match "templates/(:type_id)"    => 'templates#index',        :via => :get
      match "templates/:id/pdfs"    => 'templates#get_pdfs',     :via => :get

    end
  end


  devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}


  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do
    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users, skip: :omniauth_callbacks

    ##============================================================##
    ## Dashboard
    ##============================================================##
    root to: "pages#home"
    match "dashboard"       => "dashboard#index",           :via => [:get], :as => :dashboard
    match 'showcase'        => 'dashboard#showcase',        :via => [:get], :as => :dashboard_showcase
    match 'history'         => "dashboard#history",         :via => :get,   :as => :dashboard_history


    authenticate :user, lambda { |u| u.check_role? } do
      mount Sidekiq::Web => '/sidekiq'
    end




    scope "app" do
      match 'preview'         => 'previews#show',                 :via => [:get],   :as => "app_preview"
      match 'preview/g/:id'   => 'previews#generate',             :via => [:post],  :as => "app_preview_generate", :format => :json
      match 'download/:id'    => 'previews#download_preview_pdf', :via => [:get],   :as => "app_preview_download"
      match 'preview/error'   => 'previews#error',                :via => [:get],   :as => "app_preview_error"
    end



    ##============================================================##
    ## Admin
    ##============================================================##
    scope "admin" do
      ##============================================================##
      ## admin
      ##============================================================##
      ##============================================================##
      ## Print Template
      ##============================================================##
      match 'print-templates'                   => 'print_templates#print_templates',               via: [:get],    :as => "admin_print_templates"
      match 'print-templates/new'               => 'print_templates#print_templates_new',           via: [:get],    :as => "admin_print_templates_new"
      match 'print-templates/:id'               => 'print_templates#print_templates_edit',          via: [:get],    :as => "admin_print_templates_edit"
      match 'print-templates/:id'               => 'print_templates#print_templates_update',        via: [:patch],  :as => "admin_print_templates_update"
      match 'print-templates/:id'               => 'print_templates#print_templates_delete',        via: [:delete], :as => "admin_print_templates_delete"
      ##============================================================##
      ## Print Template Pdfs
      ##============================================================##
      match 'print-templates/pdfs/:id'              => 'print_templates#print_templates_pdf_edit',        via: [:get],        :as => 'admin_print_template_pdf_edit'
      match 'print-templates/pdfs/:id/dictionary'   => 'print_templates#print_templates_pdf_dictionary',  via: [:get],        :as => 'admin_print_template_pdf_dictionary'
      match 'print-templates/pdfs/:id/deactivate'   => 'print_templates#print_templates_pdf_deactivate',  via: [:post],       :as => 'admin_print_template_pdf_deactivate'
      match 'print-templates/pdfs/:id/reactivate'   => 'print_templates#print_templates_pdf_reactivate',  via: [:post],       :as => 'admin_print_template_pdf_reactivate'
      match 'print-templates/pdfs/:id'              => 'print_templates#print_templates_pdf_update',      via: [:patch],      :as => 'admin_print_template_pdf_update'
      resources :fonts
      resources :print_template_sizes
    end



  end
end
