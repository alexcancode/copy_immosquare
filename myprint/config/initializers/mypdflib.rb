Mypdflib.setup do |config|
  config.license        = OS.mac? ? nil : ENV['pdflib_license']
  config.stuff_folder   = "#{Rails.root}/pdfs/fonts"
  config.ppi            = 250
end