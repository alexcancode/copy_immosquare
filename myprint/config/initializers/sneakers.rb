opts = {
  :amqp               => "amqp://#{ENV['amqp_username']}:#{ENV['amqp_password']}@#{ENV['amqp_server']}:#{ENV['amqp_port']}",
  :vhost              => ENV["amqp_vhost"],
  :workers            => 4,
  :threads            => 8,
  :pid_path           => "tmp/pids/sneakers.pid",
  :log                => ActiveSupport::Logger.new("#{Rails.root}/log/sneakers.log"),
}

Sneakers.configure(opts)

Sneakers.logger.level = Logger::INFO
Sneakers.logger.formatter = Sneakers::Support::ProductionFormatter
