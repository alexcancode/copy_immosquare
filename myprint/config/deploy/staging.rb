set :stage, :staging
server 'web03.immosquare.com', {:user=>'root', :roles => %w{web app db}}
set :rvm_ruby_version, "2.3.3@myprint-staging"
set :deploy_to, "/srv/apps/myprint-staging"