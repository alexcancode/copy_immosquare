require "color_converter"
namespace :samcon do


  task :pdf_1 => :environment do
    original = "/Users/juleswelsch/Dropbox/03_print_templates/samcon/samcon-test2.pdf"
    result  = "/Users/juleswelsch/Downloads/samcon-test2_result.pdf"

    dictionary = Mypdflib::PdfParser.new(:pdf =>original).parse

    params = Hash.new
    params["samcon"] = Hash.new
    params["samcon"]["csv_file"] = [
      [{:type => "string", :content => 206},
        {:type => "string", :content => 2},
        {:type => "string", :content => "Le Windsor"},
        {:type => "string", :content => "4 1/2"},
        {:type => "string", :content => 2},
        {:type => "string", :content => "730 sq. ft."},
        {:type => "string", :content => "12 to 24 months"},
        {:type => "string", :content => "Available"}],
      [{:type => "string", :content => 210},
        {:type => "string", :content => 2},
        {:type => "string", :content => "Le Victoria-B"},
        {:type => "string", :content => "4 1/2"},
        {:type => "string", :content => 2},
        {:type => "string", :content => "770 sq. ft."},
        {:type => "string", :content => "12 to 24 months"},
        {:type => "string", :content => "Available"}],
      [{:type => "string", :content => 306},
        {:type => "string", :content => 3},
        {:type => "string", :content => "Le Windsor"},
        {:type => "string", :content => "4 1/2"},
        {:type => "string", :content => 2},
        {:type => "string", :content => "730 sq. ft."},
        {:type => "string", :content => "12 to 24 months"},
        {:type => "string", :content => "Available"}]
      ]
          dico = SamconPopulater.new(:dictionary => dictionary,:infos => params["samcon"]).populate_dictionary
          pdf = Mypdflib::PdfWritter.new(:original => original,:result => result,:dictionary => dico)
          pdf.produce
        end





      end
