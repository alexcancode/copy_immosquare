# encoding: utf-8

require '/Users/juleswelsch/Sites/print/demo_pdflib/bind/ruby/ruby22/PDFlib.bundle'


##============================================================##
## Define  Pdfs
##============================================================##
searchpath  = "/Users/juleswelsch/Sites/print/demo_pdflib/bind/data"
infile      = "/Users/juleswelsch/Desktop/original_2.pdf"
outfile     = "/Users/juleswelsch/Desktop/result.pdf"

my_data = Hash.new
my_data[:agent_certificate_and_firm_office_address_only_with_newline] = "test 1"
my_data[:agent_name] = "test 2"
my_data[:agent_phone] = "test 3"

begin
  p = PDFlib.new
  p.set_option("SearchPath={{#{searchpath}}}")
  p.set_option("errorpolicy=return")
  p.set_option("stringformat=utf8")

  ##============================================================##
  ## On créé un document finale vide et on lui set certaines
  ## informations
  ##============================================================##
  final_document = p.begin_document(outfile, "destination={type=fitwindow} pagelayout=singlepage")
  raise "Error: #{p.get_errmsg()}" if final_document == -1
  p.set_info("Creator", "PDFlib starter sample")
  p.set_info("Title", "starter_block")


  ##============================================================##
  ## On ouvre le pdfs de travail
  ##============================================================##
  indoc  = p.open_pdi_document(infile, "")
  raise "Error: #{p.get_errmsg()}"  if indoc == -1

  @number_of_pages  = p.pcos_get_number(indoc, "length:pages").to_i
  puts "Number of pages : #{@number_of_pages}"


  ##============================================================##
  ## Pour chaque pages du document
  ##============================================================##
  (1..@number_of_pages).each do |i|
    page_number = i-1
    inpage = p.open_pdi_page(indoc,i, "cloneboxes")
    raise "Error: #{p.get_errmsg()}"  if (inpage == -1)
    p.begin_page_ext(10, 10, "")
    p.fit_pdi_page(inpage, 0, 0, "cloneboxes")


    ##============================================================##
    ## Create blocks Array
    ##============================================================##
    blocks      = Array.new
    blockcount  = p.pcos_get_number(indoc,"length:pages[#{page_number}]/blocks").to_i
    puts "Page ##{page_number+1} => #{blockcount} block(s)"
    (0..blockcount-1).each do |block_number|
      name = p.pcos_get_string(indoc, "pages[#{page_number}]/blocks[#{block_number}]/Name")
      type = p.pcos_get_string(indoc, "pages[#{page_number}]/blocks/#{name}/Subtype")
      blocks << {:name =>name, :type=>type,:value=> my_data.has_key?(name.to_sym) ? my_data[name.to_sym] : nil }
    end


    ##============================================================##
    ## Each blocks
    ##============================================================##
    blocks.each_with_index do |block,x|
      puts "Block #{x+1} #{block[:name]}(#{block[:type]}) => #{block[:value]}"
      begin
        case block[:type]
        when "Text"
          if p.fill_textblock(inpage,block[:name],block[:value],"encoding=unicode embedding") == -1
            puts "Warning: #{p.get_errmsg()}"
          end
        when "Image"
          image = p.load_image("auto","new.jpg", "")
          p.fill_imageblock(inpage, "icon", image, "")
          p.close_image(image)
        else
          puts "autre type de bloc pas encore géré..."
        end
      rescue  Exception => e
        puts "Error block placing => #{e.message}"
      end
    end


    ##============================================================##
    ## close page
    ##============================================================##
    p.close_pdi_page(inpage)
    p.end_page_ext("")
    puts "========="
  end

  ##============================================================##
  ## On ferme le document
  ##============================================================##
  p.close_pdi_document(indoc)
  p.end_document("")
rescue  PDFlibException => pe
  puts "[#{pe.get_errnum.to_s}] #{pe.get_apiname} : #{pe.get_errmsg}"
rescue  Exception => e
  puts "#{e.backtrace.join("\n")}\n#{e.to_s}"
ensure
  p.delete() if p
end

