set :application, "pixis"
set :repo_url, "git@bitbucket.org:wgroupe/pixis.git"
set :rvm_ruby_version, "2.3.3@pixis"
set :deploy_to, "/srv/apps/pixis"
set :bundle_roles, :all
set :linked_dirs, %w{log bin tmp/pids tmp/sockets}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "pixis_#{fetch(:stage)}" }
