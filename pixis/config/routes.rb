require 'sidekiq/web'

Rails.application.routes.draw do

  ##============================================================##
  ## Devise does not support scoping OmniAuth callbacks under a dynamic segment
  ##============================================================##
  devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}

  ##============================================================##
  ## Sidekiq
  ##============================================================##
  mount Sidekiq::Web => '/sidekiq'

  ##============================================================##
  ## API
  ##============================================================##
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      ##============================================================##
      ## Emmobilier
      ##============================================================##
      match 'emmobilier/orders/:id'     => "emmobilier#orders",    :via => :get
    end
  end

  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do
    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users, skip: :omniauth_callbacks

    ##============================================================##
    ## OAuth
    ##============================================================##
    match 'users/auth/:provider/callback'   => 'authentifications#create',   :via => [:get]
    match 'users/auth/failure'              => 'authentifications#failure',  :via => [:get]
    match 'users/auth/:id'                  => 'authentifications#destroy',  :via => [:delete] ,:as => 'dashboard_delete_app'

    ##============================================================##
    ## Dashboard
    ##============================================================##
    root to: "pages#home"
    match "dashboard"                 => "dashboard#index",           :via => [:get],   :as => :dashboard
    match "history"                   => "dashboard#history",         :via => [:get],   :as => :history
    match "get_pictures/:order_id"    => "dashboard#get_pictures",    :via => [:get],   :as => :get_pictures

    ##============================================================##
    ## pixisOrder
    ##============================================================##
    match "pixis/:property_id"  => "pixis_orders#new",            :via => [:get],     :as => :pixis_new
    match "pixis/create"        => "pixis_orders#create",         :via => [:post],    :as => :pixis_create
    match "confirmation/:id"    => "pixis_orders#confirmation",   :via => [:get],     :as => :confirmation
  end

end