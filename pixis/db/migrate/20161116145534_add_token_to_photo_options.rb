class AddTokenToPhotoOptions < ActiveRecord::Migration[5.0]
  def change
  	add_column :photo_options, :token, :integer, after: :option
  	add_column :pixis_orders, :token, :integer, after: :user_uid

  end
end
