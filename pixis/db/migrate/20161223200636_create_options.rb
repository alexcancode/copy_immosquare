class CreateOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :options do |t|
      t.string :name
      t.integer :token

      t.timestamps
    end

    remove_column :photo_options, :option, :string
    rename_column :photo_options, :token, :option_id
  end
end
