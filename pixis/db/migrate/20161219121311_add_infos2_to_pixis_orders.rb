class AddInfos2ToPixisOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :pixis_orders, :emmobilider_response_code, :integer, :after => :emmobilier_data
    add_column :pixis_orders, :emmobilier_response_data, :text, :after => :emmobilider_response_code
  end
end
