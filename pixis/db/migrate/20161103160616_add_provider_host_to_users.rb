class AddProviderHostToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :provider_host, :string, :after => :provider
  end
end
