class AddStatusToPixisOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :pixis_orders, :status, :integer, :after => :token, :default => 0
  end
end
