class DropTableProfile < ActiveRecord::Migration[5.0]
  def change
    drop_table :profiles

    remove_column :pixis_orders, :address, :string
    add_column :pixis_orders, :property_id, :integer, :after => :user_uid
  end
end
