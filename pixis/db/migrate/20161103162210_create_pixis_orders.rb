class CreatePixisOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :pixis_orders do |t|
      t.references :user, foreign_key: false
      t.string :user_uid
      t.string :picture_url

      t.timestamps
    end
  end
end
