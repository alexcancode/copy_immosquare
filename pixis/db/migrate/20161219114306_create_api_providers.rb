class CreateApiProviders < ActiveRecord::Migration[5.0]
  def change
    create_table :api_providers do |t|
      t.string :name

      t.timestamps
    end
  end
end
