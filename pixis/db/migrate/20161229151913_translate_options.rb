class TranslateOptions < ActiveRecord::Migration[5.0]
  def change

    remove_column :pixis_orders, :user_uid, :string

    reversible do |dir|
      dir.up do
        Option.create_translation_table! :name => :string
      end

      dir.down do 
        Option.drop_translation_table!
      end
    end

  end
end
