class AddInfos3ToPixisOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :pixis_orders, :emmobilier_response_final_data, :text
    rename_column :pixis_orders, :emmobilider_order_id, :emmobilier_order_id
    rename_column :pixis_orders, :emmobilider_response_code, :emmobilier_response_code
  end
end
