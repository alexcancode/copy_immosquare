class AddInfosToPixisOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :pixis_orders, :emmobilider_order_id, :integer, :after => :token
    add_column :pixis_orders, :emmobilier_data, :text, :after => :emmobilider_order_id
  end
end
