class AddInfoToPixisOrders < ActiveRecord::Migration[5.0]
  def change
  	add_column :pixis_orders, :reference_number, :string
  	add_column :pixis_orders, :address, :string
  end
end

