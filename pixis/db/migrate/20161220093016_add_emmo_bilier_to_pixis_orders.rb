class AddEmmoBilierToPixisOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :pixis_orders, :emmobilier_response_final_code, :integer, :after => :emmobilier_response_data
  end
end
