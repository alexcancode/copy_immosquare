class CreateApiApps < ActiveRecord::Migration[5.0]
  def change
    create_table :api_apps do |t|
      t.string :key
      t.string :token
      t.references :api_provider, foreign_key: false
      t.string :internal_note

      t.timestamps
    end
  end
end
