class CreatePhotoOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :photo_options do |t|
			t.references :pixis_order, foreign_key: false
      t.string :picture_url
      t.string :option
      t.timestamps
    end
  end
end
