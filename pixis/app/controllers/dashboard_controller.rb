class DashboardController < ApplicationController

  include ClientAuthorized

  before_filter :is_authorized

  def index
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties?access_token=#{current_user.oauth_token}")
    @response = JSON.parse(response.body).deep_symbolize_keys! if response.code == 200
    @pixis_orders = PixisOrder.where(user_id: current_user.id)
  end

  def history
    @pixis_orders = PixisOrder.where(user_id: current_user.id)
    ids =  @pixis_orders.pluck(:property_id).uniq.join(",")
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties?access_token=#{current_user.oauth_token}&id=#{ids}")
    @response = JSON.parse(response.body).deep_symbolize_keys! if response.code == 200
  end

  def get_pictures
    @pixis_order = PixisOrder.find(params[:order_id])
    @images = [{:old_url => "", :new_url => ""}]
  end

end