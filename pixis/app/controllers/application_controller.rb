class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale

  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {locale: I18n.locale}
  end

  def set_locale
    I18n.locale  = params[:locale]
  end

  def after_sign_in_path_for(user)
    dashboard_path
  end

  protected
  def is_authorized
    @is_authorized = @store_api.present? && @store_api["authorizations"]["tokens"].present? && @store_api["authorizations"]["tokens"].length > 0 && @store_api["authorizations"]["tokens"].first["tokens"].to_i > 0
  end
  
end
