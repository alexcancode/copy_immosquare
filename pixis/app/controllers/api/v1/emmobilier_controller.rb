class Api::V1::EmmobilierController < Api::V1::ApplicationController

  def orders
    begin
      @order = PixisOrder.where(:emmobilier_order_id => params[:id]).last
      raise t("api.errors.no_order") if @order.blank?
      emmobilier_get = HTTParty.get("#{ENV['emmobilier_url']}/api/getfinishedimages?contactAPIkey=#{ENV['emmobilier_apikey']}&contactEmail=#{ENV['emmobilier_email']}&id=#{params[:id]}")
      @order.update_attributes(
        :emmobilier_response_final_code => emmobilier_get.code,
        :emmobilier_response_final_data => JSON.parse(emmobilier_get.body),
        :status => 1
      )
    rescue Exception => e
      return render :json =>{:errors=>e.message}, :status => 402
    end
  end

end

