module Api
  module V1
    class ApplicationController < ActionController::Base

      ##============================================================##
      ## Before & After
      ##============================================================##
      before_filter :check_credentials_in_header_or_url, :except => [:sign_in]

      def check_credentials_in_header_or_url
        begin
          if request.headers["apiKey"].blank? or request.headers["apiToken"].blank?
            raise t("api.errors.no_credentials") if(params["apiKey"].blank? or params["apiToken"].blank?)
            @api_app = ApiApp.where(:key=>params["apiKey"],:token=>params["apiToken"]).first
            @api_app.flow_name = params["flowName"] if params.has_key?("flowName")
            raise t("api.errors.not_authorized") if @api_app.blank?
          else
            @api_app = ApiApp.where(:key=>request.headers["apiKey"],:token=>request.headers["apiToken"]).first
            @api_app.flow_name = request.headers["flowName"] if request.headers["flowName"].present?
            raise t("api.errors.not_authorized") if @api_app.blank?
          end
        rescue Exception => e
          return render :json =>{:errors=>e.message}, :status=> 402
        end
      end

    end
  end
end