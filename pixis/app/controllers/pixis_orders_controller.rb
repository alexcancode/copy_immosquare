class PixisOrdersController < ApplicationController

  include ClientAuthorized

  before_filter :is_authorized

  def new
    if @is_authorized
      @property_id = params["property_id"]
      response      = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{@property_id}?access_token=#{current_user.oauth_token}")
      @response     = JSON.parse(response.body) if response.code == 200
      @pixis_order  = PixisOrder.new
      @pixis_order.photo_options.build

      @options = Option.all.order(:token)
      @options_map = @options.map{|o| ["#{o.name}: #{t("app.tokens_c", :count => o.token)}", o.id, :data => {:tokens => o.token}]}
    else
      flash[:danger] = t("app.no_auth")
      return redirect_to dashboard_path
    end
  end

  def create
    begin
      raise t("app.no_auth") if !@is_authorized

      @pixis_order = PixisOrder.new(pixis_order_params)
      token_debited = @pixis_order.cost

      debit_url = @store_api["authorizations"]["tokens"].first["debit_url"]

      raise t("pixis.no_tokens_error") if token_debited > @store_api["authorizations"]["tokens"].first["tokens"].to_i

      debit_post = HTTParty.post("#{debit_url}/#{token_debited}?access_token=#{current_user.oauth_token}&api_key=#{ENV["storeimmo_api_key"]}&api_token=#{ENV["storeimmo_api_token"]}")

      raise "Debit error" if debit_post.code != 200
      @pixis_order.save
      @pixis_order.photo_options.each{|po| po.option.token == 0 ? po.destroy : nil}

      photo_3credit = @pixis_order.photo_options.includes(:option).where(:options => {:token => 3}).pluck(:picture_url)
      photo_1credit = @pixis_order.photo_options.includes(:option).where(:options => {:token => 1}).pluck(:picture_url)
      images_array = photo_3credit + photo_1credit

      ##============================================================##
      ## Send infos to Emmobilier
      ##============================================================##
      emmobilier_body = {
        :imagesUrls         => images_array,
        :contactEmail       => ENV['emmobilier_email'],
        :contactAPIkey      => ENV['emmobilier_apikey'],
        :templateIds        => ["83"],
        :nbFilesPertinence  => photo_3credit.length,
        :nbFilesEquilibrage => photo_1credit.length,
        :commandeType       => "2",
        :status             => 1
      }

      emmobilier_post = HTTParty.post("#{ENV['emmobilier_url']}/api/commandes", :body => emmobilier_body)
      data = JSON.parse(emmobilier_post.body)
      @pixis_order.update_attributes(:emmobilier_data => emmobilier_body, :emmobilier_response_code => emmobilier_post.code, :emmobilier_response_data => data, :emmobilier_order_id => emmobilier_post.code == 200 ? data["id_commande"] : nil)

      # send email
      NotifierMailer.confirmation_order(@pixis_order.id, @store_api["email"]).deliver_later

      flash[:success] = t("pixis.success_message")
      redirect_to confirmation_path(@pixis_order)
    rescue Exception => e
      flash[:danger] = e
      redirect_to :back
    end
  end

  def confirmation
    @pixis_order = PixisOrder.find(params[:id])
    call = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{@pixis_order.property_id}?access_token=#{current_user.oauth_token}")
    @property = JSON.parse(call.body) if call.code == 200
  end

  private

  def pixis_order_params
    params.require(:pixis_order).permit(:user_id, :property_id, :token, :picture_url, :reference_number, photo_options_attributes: [:id, :picture_url, :option_id])
  end

end