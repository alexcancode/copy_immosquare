class PixisOrder < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  has_many :photo_options
  accepts_nested_attributes_for :photo_options

  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :emmobilier_data, JSON
  serialize :emmobilier_response_data, JSON
  serialize :emmobilier_response_final_data, JSON

  def cost
    sum = 0
    self.photo_options.each do |po|
      sum += po.option.token
    end
    self.update_attribute(:token, sum)
    return sum
  end

end
