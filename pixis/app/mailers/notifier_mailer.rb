class NotifierMailer < ApplicationMailer
 
  def confirmation_order(pixis_order, email)
    @pixis_order = PixisOrder.find(pixis_order)
    mail(from: 'no-reply@immosquare.com', to: email, bcc: "quentin@immosquare.com", subject: 'Pixis: Thank you for your purchase')
  end

end
