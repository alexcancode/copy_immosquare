$(document).ready ->

  $("#chosen-select_louer").chosen()

  $('.sync_btn')
    .bind "ajax:beforeSend", (evt, xhr, settings) ->
      $(@).addClass('disabled')
    .bind 'ajax:send' , ->
      $('#syncDebugger').html('<i class="fa fa-spinner fa-spin fa-2x"></i>')
    .bind 'ajax:error', (evt, xhr, status, error) ->
      $('#syncDebugger').append(xhr.responseText)
      $(@).removeClass('disabled')
    .bind 'ajax:complete',(xhr, status) ->
      $(@).removeClass('disabled')

    $('#zipcode--section').on 'change','.ajax_update_status', (event) ->
      $($(@).closest('form')).submit()


  $('.datepicker').pickadate()


  $('#addNote').on 'click',(event) ->
    event.preventDefault()
    old_value =  $('#my_broker_note').val()
    new_value = "#{$(@).data('date')} : \n \n" + old_value
    $('#my_broker_note').val(new_value)
    $('#my_broker_note').focus()



  ##============================================================##
  ## ZipCode Edit
  ##============================================================##
  neighborhoods = $('#zipcode_centris_neighborhood_id').html()
  $('#zipcode_centris_city_id').change ->
    city = $('#zipcode_centris_city_id :selected').text()
    escaped_city = city.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = "<option value=''>--</option>"
    options += $(neighborhoods).filter("optgroup[label='#{escaped_city}']").html()
    $('#zipcode_centris_neighborhood_id').html(options)

