class ApiMailer < ApplicationMailer

  default :template_path => "mailers/api_mailer"

  require 'open-uri'
  OpenURI::Buffer.send :remove_const, 'StringMax' if OpenURI::Buffer.const_defined?('StringMax')
  OpenURI::Buffer.const_set 'StringMax', 0

  def error_monitoring(error_locatisation,error_message)
    @error_message = error_message
    @title = "API ERROR #{error_locatisation}"
    mail(
     :to => "error@shareimmo.com",
     :from => "api@shareimmo.com",
     :subject => "Error monitoring"
     )
  end


end
