class ApplicationMailer < ActionMailer::Base

  layout 'mailer'

  def default_url_options(options = {})
    {
      :locale => I18n.locale,
      :host => @my_provider[:link_host]
    }
  end



end

