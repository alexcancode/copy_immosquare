json.louercom_location do
  json.array! @zips.group_by(&:louercom_location) do |louercom_location,zips|  
    json.set! louercom_location.id, zips.map(&:zipcode) unless louercom_location.nil?
  end
end

json.louercom_region do
  json.array! @zips.group_by(&:louercom_location) do |louercom_location,zips|  
    json.set! louercom_location.region_id, zips.map(&:zipcode) unless louercom_location.nil?
  end
end
