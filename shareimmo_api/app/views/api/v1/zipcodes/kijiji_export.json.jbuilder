json.kijiji_location do
  json.array! @zips.group_by(&:kijiji_location) do |kijiji_location,zips|
    json.set! kijiji_location.name, zips.map(&:zipcode)
  end
end

json.best_sector do
  json.array! @zips do |zip|
    json.set! zip.zipcode, zip.best_sector
  end
end


