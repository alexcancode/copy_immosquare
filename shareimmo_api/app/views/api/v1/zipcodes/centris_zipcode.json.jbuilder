if @zip.present?
  json.status 1
  json.zipcode @zip.zipcode
  json.city do
    json.name @zip.centris_city.description
    json.code @zip.centris_city.code
    json.region_code @zip.centris_city.region_code
  end
  if @zip.centris_neighborhood_id.present?
    json.neighborhood do
      json.name @zip.centris_neighborhood.description_francaise
      json.num_code @zip.centris_neighborhood.mun_code.to_i
      json.code @zip.centris_neighborhood.code
    end
  end
else
  json.status 0
end
