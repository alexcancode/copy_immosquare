class PagesimmoSyncWorker
  include Sidekiq::Worker

  def perform(args)
    puts "start"
    soap = Savon.client(:wsdl => "http://wsimmosquare.pagesimmo.com/storeimmo.asmx?wsdl")

    args = args.deep_symbolize_keys
    call = HTTParty.get("#{ENV['store_url']}/api/v1/gateways/pagesimmo?apiKey=#{ENV['store_api_key']}&apiToken=#{ENV['store_api_token']}&client_id=#{args[:client_id]}")
    call = JSON.parse(call.body)

    call["gateways"].each do |gateway|
      gateway["activations"].each do |activation|
        puts "start client #{activation["username"]}"

        ##============================================================##
        ## Authentification
        ##============================================================##
        begin
          call1 = soap.call(:authentification,
            :message => {
              "_agence_Login" => activation["username"],
              "_agence_Pwd"   => activation["password"],
              "_editeur_Key"  => ENV['gercop_editor_key']
              })

          token = call1.body[:authentification_response][:authentification_result]
          raise "no token" if token.blank?
        rescue Exception => e
          puts "error when getting token : #{e}"
          next
        end

        ##============================================================##
        ## Liste des annonces
        ##============================================================##
        begin
          call2 = soap.call(:get_liste_annonces,
            :message => {
              "_auth_Token"  => token,
              "_auth_Login"  => activation["username"]
              })
          JulesLogger.info call2.body
          call2     = Nokogiri::XML(call2.body[:get_liste_annonces_response][:get_liste_annonces_result])
          editor    = call2.css("Editeur").first
          crytml    = call2.css("CryptML").first
          raise "no content CryptML" if crytml.blank?
          crytml    = crytml.content
          # File.open("#{Rails.root}/tmp/gercop_#{activation["username"]}.xml", 'w') { |file| file.write(call2)}
        rescue Exception => e
          puts "error when getting xml content : #{e}"
          next
        end

        ##============================================================##
        ## Ruby Parser
        ##============================================================##
        begin
          nb_errors = 0
          nb_success = 0
          crytml_rb = Hash.from_xml(crytml).deep_symbolize_keys
          crytml_rb[:ROOT][:DESTINATAIRE][:AGENCE][:BIEN].each do |bien|
            puts "importing property ##{bien[:REFDISTANTE]}"
            body = generate_property_body(bien, activation["client_id"], activation["username"])

            property_post = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",{:headers => {"ApiKey" => activation["shareimmo_api_key"], "ApiToken" => activation["shareimmo_api_token"]}, :body => body})
            if property_post.code == 200 
              nb_success += 1
              puts "imported successfuly"
            else
              nb_errors += 1
              puts "error during importation"
            end
          end

          puts "track importation in store"
          activation_post = HTTParty.post("#{ENV['store_url']}/api/v1/gateways/track/#{activation["id"]}",{:headers => {"apiKey" => ENV['store_api_key'], "apiToken" => ENV['store_api_token']}, :body => {:nb_errors => nb_errors, :nb_success => nb_success}})
          activation_post = JSON.parse(activation_post.body)
        rescue Exception => e
          puts "error when parsing : #{e}"
          next
        end
        
      end
    end

    puts "end"
  end




  def generate_property_body(bien, account_uid, uid)
    infos = bien[:LOCATION] if bien.dig(:LOCATION)
    infos = bien[:SAISONNIER] if bien.dig(:SAISONNIER)
    infos = bien[:VENTE] if bien.dig(:VENTE)

    maison = bien[:AUTRE] if bien.dig(:AUTRE)
    maison = bien[:DEMEURE_CHATEAU] if bien.dig(:DEMEURE_CHATEAU)
    maison = bien[:TERRAIN] if bien.dig(:TERRAIN)
    maison = bien[:PARKING_GARAGE] if bien.dig(:PARKING_GARAGE)
    maison = bien[:IMMEUBLE] if bien.dig(:IMMEUBLE)
    maison = bien[:AGRICOLE_VITICOLE] if bien.dig(:AGRICOLE_VITICOLE)
    maison = bien[:FORET] if bien.dig(:FORET)
    maison = bien[:LOCAL_COMMERCIAL] if bien.dig(:LOCAL_COMMERCIAL)
    maison = bien[:LOCAL_INDUSTRIEL] if bien.dig(:LOCAL_INDUSTRIEL)
    maison = bien[:LOCAL_PROFESSIONNEL] if bien.dig(:LOCAL_PROFESSIONNEL)
    maison = bien[:FONDS_COMMERCE] if bien.dig(:FONDS_COMMERCE)
    maison = bien[:APPARTEMENT] if bien.dig(:APPARTEMENT)
    maison = bien[:MAISON] if bien.dig(:MAISON)

    body                      = Hash.new
    body[:uid]                = "#{uid}_#{bien[:REFDISTANTE]}"
    body[:account_uid]        = account_uid
    body[:online]             = 1

    classification_id = 101

    if bien.dig(:AUTRE)
      classification_id = 101
    end

    if bien.dig(:DEMEURE_CHATEAU)
      classification_id = case maison[:CATEGORIE_DEMEURE]
      when 1 # Château
        111
      when 2 # Demeure ancienne
        101
      when 3 # Demeure contemporaine
        101
      when 4 # Demeure traditionnelle
        101
      when 5 # Manoir
        104
      when 6 # Maison de maître
        104
      when 7 # Mas
        109
      when 8 # Villa
        109
      when 9 # Propriété
        117
      else
        101
      end
    end

    if bien.dig(:TERRAIN)
      classification_id = case maison[:CATEGORIE_TERRAIN]
      when 1 # Terrain à aménager
        402
      when 2 # Terrain à bâtir
        402
      when 3 # Terrain industriel et artisanal
        405
      when 4 # Terrain non constructible
        403
      else
        401
      end
    end

    if bien.dig(:PARKING_GARAGE)
      classification_id = case maison[:CATEGORIE_PARKING]
      when 1 # Parking
        302
      when 2 # Parking couvert
        303
      when 3 # Garage
        301
      else
        302
      end
    end

    if bien.dig(:IMMEUBLE)
      classification_id = case maison[:CATEGORIE_IMMEUBLE]
      when 1 # Immeuble loué
        501
      when 2 # Immeuble vide
        101
      else
        101
      end
    end

    if bien.dig(:AGRICOLE_VITICOLE)
      classification_id = case maison[:CATEGORIE_AGRICOLE]
      when 1 # Agricole
        403
      when 2 # Viticole
        403
      when 3 # Agricole et viticole
        403
      else
        403
      end
    end

    if bien.dig(:FORET)
      classification_id = 401
    end

    if bien.dig(:LOCAL_COMMERCIAL)
      classification_id = 801
    end

    if bien.dig(:LOCAL_INDUSTRIEL)
      classification_id = case maison[:CATEGORIE_LOCAL_INDUSTRIEL]
      when 1 # Local industriel / d'activités
        811
      when 2 # Entrepôt
        806
      else
        811
      end
    end

    if bien.dig(:LOCAL_PROFESSIONNEL)
      classification_id = case maison[:CATEGORIE_LOCAL_PROFESSIONNEL]
      when 1 # Bureaux
        802
      when 2 # Locaux professionnels
        801
      else
        801
      end
    end

    if bien.dig(:FONDS_COMMERCE)
      classification_id = 801
    end

    if bien.dig(:APPARTEMENT)
      classification_id = case maison[:CATEGORIE_APPARTEMENT]
      when 1 # Appartement ancien
        201
      when 2 # Appartement bourgeois
        201
      when 3 # Appartement moderne
        114
      when 4 # Loft
        207
      when 5 # Habitation de loisirs
        201
      when 6 # Chambre de bonne
        202
      when 7 # Autre
        201
      when 8 # Non spécifié
        201
      else
        201
      end
    end

    if bien.dig(:MAISON)
      classification_id = case maison[:CATEGORIE_MAISON]
      when 1 # Maison de ville
        114
      when 2 # Maison de village
        115
      when 3 # Maison de campagne
        115
      when 4 # Hotel particulier
        109
      when 5 # Maison individuelle en lotissement
        109
      when 6 # Pavillon en lotissement
        701
      when 7 # Fermette
        108
      when 8 # Habitation de loisirs
        101
      when 9 # Villa
        109
      when 10 # Mas
        107
      when 11 # Chalet
        110
      when 12 # Autre
        119
      when 13 # Non spécifié
        119
      else
        101
      end
    end

    body[:classification_id]  = classification_id
    body[:status_id]          = bien.dig(:ANCIENNETE) && bien[:ANCIENNETE] == "1" ? 1 : 2
    body[:listing_id]         = bien.dig(:VENTE) ? 1 : 2
    body[:year_of_built]      = maison.present? ? maison[:ANNEE_CONSTRUCTION] : nil
    body[:building_uid]       = nil
    body[:unit]               = nil
    body[:level]              = maison.present? ? maison[:ETAGE] : nil
    body[:cadastre]           = nil
    body[:flag_id]            = nil
    body[:availability_date]  = maison.present? ? maison[:DISPONIBLE_LE] : nil
    body[:is_luxurious]       = nil

    body[:address]                                = Hash.new
    body[:address][:address_visibility]           = 1
    body[:address][:address_formatted]            = bien[:LOCALISATION][:ADRESSE1] if bien.dig(:LOCALISATION,:ADRESSE1)
    body[:address][:street_number]                = bien[:LOCALISATION][:NUM_RUE] if bien.dig(:LOCALISATION,:NUM_RUE)
    body[:address][:street_name]                  = bien[:LOCALISATION][:NOM_RUE] if bien.dig(:LOCALISATION,:NOM_RUE)
    body[:address][:zipcode]                      = bien[:LOCALISATION][:CP] if bien.dig(:LOCALISATION,:CP)
    body[:address][:locality]                     = bien[:LOCALISATION][:VILLE] if bien.dig(:LOCALISATION,:VILLE)
    body[:address][:sublocality]                  = "#{bien[:LOCALISATION][:SECTEUR]} #{bien[:LOCALISATION][:QUARTIER]}"  if (bien.dig(:LOCALISATION,:QUARTIER) || bien.dig(:LOCALISATION,:SECTEUR))
    body[:address][:administrative_area_level1]   = nil
    body[:address][:administrative_area_level2]   = nil
    body[:address][:country]                      = ISO3166::Country.find_country_by_name(bien[:LOCALISATION][:PAYS]).alpha2 if (bien.dig(:LOCALISATION,:PAYS) && ISO3166::Country.find_country_by_name(bien[:LOCALISATION][:PAYS]).present?)
    body[:address][:latitude]                     = bien[:LOCALISATION][:LATITUDE] if bien.dig(:LOCALISATION,:LATITUDE)
    body[:address][:longitude]                    = bien[:LOCALISATION][:LONGITUDE] if bien.dig(:LOCALISATION,:LONGITUDE)

    body[:area]             = Hash.new
    body[:area][:living]    = maison.present? ? maison[:SURFACE_HABITABLE] : nil
    body[:area][:land]      = maison.present? ? maison[:SURFACE_TOTALE] : nil
    body[:area][:balcony]   = maison.present? ? maison[:NB_BALCONS] : nil
    body[:area][:unit_id]   = 2

    body[:rooms]                = Hash.new
    body[:rooms][:rooms]        = maison.present? ? maison[:NB_PIECES] : nil
    body[:rooms][:bathrooms]    = maison.present? ? maison[:NB_SDB] : nil
    body[:rooms][:bedrooms]     = maison.present? ? maison[:NB_CHAMBRES] : nil
    body[:rooms][:showerrooms]  = maison.present? ? maison[:NB_SDE] : nil
    body[:rooms][:toilets]      = nil

    body[:title]      = Hash.new
    body[:title][:fr] = infos.dig(:TEXTES, :TITRE_FR) ? infos[:TEXTES][:TITRE_FR] : nil
    body[:title][:en] = infos.dig(:TEXTES, :TITRE_EN) ? infos[:TEXTES][:TITRE_EN] : nil
    body[:title][:de] = infos.dig(:TEXTES, :AUTRE_LANGUE, :TITRE) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "de" ? infos[:TEXTES][:AUTRE_LANGUE][:TITRE] : nil
    body[:title][:nl] = infos.dig(:TEXTES, :AUTRE_LANGUE, :TITRE) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "nl" ? infos[:TEXTES][:AUTRE_LANGUE][:TITRE] : nil
    body[:title][:it] = infos.dig(:TEXTES, :AUTRE_LANGUE, :TITRE) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "it" ? infos[:TEXTES][:AUTRE_LANGUE][:TITRE] : nil
    body[:title][:es] = infos.dig(:TEXTES, :AUTRE_LANGUE, :TITRE) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "es" ? infos[:TEXTES][:AUTRE_LANGUE][:TITRE] : nil
    body[:title][:pt] = infos.dig(:TEXTES, :AUTRE_LANGUE, :TITRE) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "pt" ? infos[:TEXTES][:AUTRE_LANGUE][:TITRE] : nil

    body[:description_short]      = Hash.new
    body[:description_short][:fr] = nil
    body[:description_short][:en] = nil
    body[:description_short][:de] = nil
    body[:description_short][:nl] = nil
    body[:description_short][:it] = nil
    body[:description_short][:es] = nil
    body[:description_short][:pt] = nil

    body[:description]      = Hash.new
    body[:description][:fr] = infos.dig(:TEXTES, :DESCRIPTION_FR) ? infos[:TEXTES][:DESCRIPTION_FR] : nil
    body[:description][:en] = infos.dig(:TEXTES, :DESCRIPTION_EN) ? infos[:TEXTES][:DESCRIPTION_EN] : nil
    body[:description][:de] = infos.dig(:TEXTES, :AUTRE_LANGUE, :DESCRIPTION) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "de" ? infos[:TEXTES][:AUTRE_LANGUE][:DESCRIPTION] : nil
    body[:description][:nl] = infos.dig(:TEXTES, :AUTRE_LANGUE, :DESCRIPTION) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "nl" ? infos[:TEXTES][:AUTRE_LANGUE][:DESCRIPTION] : nil
    body[:description][:it] = infos.dig(:TEXTES, :AUTRE_LANGUE, :DESCRIPTION) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "it" ? infos[:TEXTES][:AUTRE_LANGUE][:DESCRIPTION] : nil
    body[:description][:es] = infos.dig(:TEXTES, :AUTRE_LANGUE, :DESCRIPTION) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "es" ? infos[:TEXTES][:AUTRE_LANGUE][:DESCRIPTION] : nil
    body[:description][:pt] = infos.dig(:TEXTES, :AUTRE_LANGUE, :DESCRIPTION) && infos[:TEXTES][:AUTRE_LANGUE][:CULTURE] == "pt" ? infos[:TEXTES][:AUTRE_LANGUE][:DESCRIPTION] : nil

    body[:url]         = Hash.new
    body[:url][:fr]    = nil
    body[:url][:en]    = nil
    body[:url][:de]    = nil
    body[:url][:nl]    = nil
    body[:url][:it]    = nil
    body[:url][:es]    = nil
    body[:url][:pt]    = nil

    deposit = nil
    bien[:DONNEE].each do |d|
      if d[:LIBELLE] == "Dépôt Garantie"
        deposit = d[:VALEUR]
        break
      end
    end if bien.dig(:DONNEE)

    body[:financial] = Hash.new
    body[:financial][:price]          = infos[:PRIX] if body[:listing_id] == 1
    body[:financial][:rent]           = infos[:LOYER_MENSUEL_TTC] if body[:listing_id] == 2
    body[:financial][:currency]       = "EUR"
    body[:financial][:price_with_tax] = 1
    body[:financial][:fees]           = infos.dig(:CHARGES_MENSUELLES) ? infos[:CHARGES_MENSUELLES].to_i*12 : nil
    body[:financial][:rent_frequency] = infos[:PERIODE_LOYER]
    body[:financial][:tax1_annual]    = infos[:TAXE_HABITATION]
    body[:financial][:tax2_annual]    = infos[:TAXE_FONCIERE]
    body[:financial][:income_annual]  = nil
    body[:financial][:deposit]        = deposit

    body[:canada]       = Hash.new
    body[:canada][:mls] = nil

    body[:france]                         = Hash.new
    body[:france][:dpe_indice]            = bien.dig(:DIAGNOSTICS, :DPE_CONSOMMATIONS_ENERGIE, :DONNEES, :LETTRE) ? bien[:DIAGNOSTICS][:DPE_CONSOMMATIONS_ENERGIE][:DONNEES][:LETTRE] : nil
    body[:france][:dpe_value]             = bien.dig(:DIAGNOSTICS, :DPE_CONSOMMATIONS_ENERGIE, :DONNEES, :VALEUR) ? bien[:DIAGNOSTICS][:DPE_CONSOMMATIONS_ENERGIE][:DONNEES][:VALEUR] : nil
    body[:france][:dpe_id]                = nil
    body[:france][:ges_indice]            = bien.dig(:DIAGNOSTICS, :DPE_EMISSIONS_GES, :DONNEES, :LETTRE) ? bien[:DIAGNOSTICS][:DPE_EMISSIONS_GES][:DONNEES][:LETTRE] : nil
    body[:france][:ges_value]             = bien.dig(:DIAGNOSTICS, :DPE_EMISSIONS_GES, :DONNEES, :VALEUR) ? bien[:DIAGNOSTICS][:DPE_EMISSIONS_GES][:DONNEES][:VALEUR] : nil
    body[:france][:alur_is_condo]         = nil
    body[:france][:alur_units]            = nil
    body[:france][:alur_uninhabitables]   = nil
    body[:france][:alur_spending]         = nil
    body[:france][:alur_legal_action]     = nil
    body[:france][:rent_honorary]         = infos[:FRAIS_AGENCE]
    body[:france][:mandat_exclusif]       = infos.dig(:MANDAT_VENTE) && infos[:MANDAT_VENTE][:TYPE_MANDAT] == 2 ? 1 : (infos.dig(:MANDAT_LOCATION) && infos[:MANDAT_LOCATION][:TYPE_MANDAT] == 2 ? 1 : 0)

    body[:belgium]                          = Hash.new
    body[:belgium][:peb_indice]             = nil
    body[:belgium][:peb_value]              = nil
    body[:belgium][:peb_id]                 = nil
    body[:belgium][:building_permit]        = nil
    body[:belgium][:done_assignments]       = nil
    body[:belgium][:preemption_property]    = nil
    body[:belgium][:subdivision_permits]    = nil
    body[:belgium][:sensitive_flood_area]   = nil
    body[:belgium][:delimited_flood_zone]   = nil
    body[:belgium][:risk_area]              = nil

    body[:other] = Hash.new
    body[:other][:orientation]          = Hash.new
    body[:other][:orientation][:north]  = nil
    body[:other][:orientation][:south]  = nil
    body[:other][:orientation][:east]   = nil
    body[:other][:orientation][:west]   = nil

    body[:other][:inclusion]                      = Hash.new
    body[:other][:inclusion][:air_conditioning]   = nil
    body[:other][:inclusion][:hot_water]          = nil
    body[:other][:inclusion][:heated]             = nil
    body[:other][:inclusion][:electricity]        = nil
    body[:other][:inclusion][:furnished]          = maison.present? ? maison[:MEUBLE] : nil
    body[:other][:inclusion][:fridge]             = nil
    body[:other][:inclusion][:cooker]             = nil
    body[:other][:inclusion][:dishwasher]         = nil
    body[:other][:inclusion][:dryer]              = nil
    body[:other][:inclusion][:washer]             = nil
    body[:other][:inclusion][:microwave]          = nil

    body[:other][:detail]                       = Hash.new
    body[:other][:detail][:elevator]            = maison.present? ? maison[:ASCENSEUR] : nil
    body[:other][:detail][:laundry]             = nil
    body[:other][:detail][:garbage_chute]       = nil
    body[:other][:detail][:common_space]        = nil
    body[:other][:detail][:janitor]             = nil
    body[:other][:detail][:gym]                 = nil
    body[:other][:detail][:golf]                = nil
    body[:other][:detail][:tennis]              = nil
    body[:other][:detail][:sauna]               = nil
    body[:other][:detail][:spa]                 = nil
    body[:other][:detail][:inside_pool]         = maison.present? ? maison[:PISCINE] : nil
    body[:other][:detail][:outside_pool]        = nil
    body[:other][:detail][:inside_parking]      = maison.present? && maison[:NB_PARKINGS].to_i > 0 ? 1 : 0
    body[:other][:detail][:outside_parking]     = nil
    body[:other][:detail][:parking_on_street]   = nil
    body[:other][:detail][:garagebox]           = maison.present? && maison[:NB_GARAGES].to_i > 0 ? 1 : 0

    body[:other][:half_furnished]                 = Hash.new
    body[:other][:half_furnished][:living_room]   = nil
    body[:other][:half_furnished][:bedrooms]      = nil
    body[:other][:half_furnished][:kitchen]       = maison.present? ? maison[:CUISINE] : nil
    body[:other][:half_furnished][:other]         = nil

    body[:other][:indoor]                         = Hash.new
    body[:other][:indoor][:attic]                 = nil
    body[:other][:indoor][:attic_convertible]     = nil
    body[:other][:indoor][:attic_converted]       = nil
    body[:other][:indoor][:cellar]                = maison.present? && maison[:NB_CAVES].to_i > 0 ? 1 : 0
    body[:other][:indoor][:central_vacuum]        = nil
    body[:other][:indoor][:entries_washer_dryer]  = nil
    body[:other][:indoor][:entries_dishwasher]    = nil
    body[:other][:indoor][:fireplace]             = nil
    body[:other][:indoor][:storage]               = nil
    body[:other][:indoor][:no_smoking]            = nil
    body[:other][:indoor][:double_glazing]        = nil
    body[:other][:indoor][:triple_glazing]        = nil

    body[:other][:floor]              = Hash.new
    body[:other][:floor][:carpet]     = nil
    body[:other][:floor][:wood]       = nil
    body[:other][:floor][:floating]   = nil
    body[:other][:floor][:ceramic]    = nil
    body[:other][:floor][:parquet]    = nil
    body[:other][:floor][:cushion]    = nil
    body[:other][:floor][:vinyle]     = nil
    body[:other][:floor][:lino]       = nil
    body[:other][:floor][:marble]     = nil

    body[:other][:exterior]                     = Hash.new
    body[:other][:exterior][:land_access]       = nil
    body[:other][:exterior][:back_balcony]      = maison.present? && maison[:NB_BALCONS].to_i > 0 ? 1 : 0
    body[:other][:exterior][:front_balcony]     = nil
    body[:other][:exterior][:private_patio]     = nil
    body[:other][:exterior][:storage]           = nil
    body[:other][:exterior][:terrace]           = maison.present? && maison[:NB_TERRASSES].to_i > 0 ? 1 : 0
    body[:other][:exterior][:veranda]           = nil
    body[:other][:exterior][:garden]            = nil
    body[:other][:exterior][:sea_view]          = nil
    body[:other][:exterior][:mountain_view]     = nil

    body[:other][:accessibility]                      = Hash.new
    body[:other][:accessibility][:elevator]           = maison.present? ? maison[:ASCENSEUR] : nil
    body[:other][:accessibility][:balcony]            = nil
    body[:other][:accessibility][:grab_bar]           = nil
    body[:other][:accessibility][:wider_corridors]    = nil
    body[:other][:accessibility][:lowered_switches]   = nil
    body[:other][:accessibility][:ramp]               = nil

    body[:other][:senior]                         = Hash.new
    body[:other][:senior][:autonomy]              = nil
    body[:other][:senior][:half_autonomy]         = nil
    body[:other][:senior][:no_autonomy]           = nil
    body[:other][:senior][:meal]                  = nil
    body[:other][:senior][:nursery]               = nil
    body[:other][:senior][:domestic_help]         = nil
    body[:other][:senior][:community]             = nil
    body[:other][:senior][:activities]            = nil
    body[:other][:senior][:validated_residence]   = nil
    body[:other][:senior][:housing_cooperative]   = nil

    body[:other][:pet]                    = Hash.new
    body[:other][:pet][:allow]            = nil
    body[:other][:pet][:cat]              = nil
    body[:other][:pet][:dog]              = nil
    body[:other][:pet][:little_dog]       = nil
    body[:other][:pet][:cage_aquarium]    = nil
    body[:other][:pet][:not_allow]        = nil

    body[:other][:service]              = Hash.new
    body[:other][:service][:internet]   = nil
    body[:other][:service][:tv]         = nil
    body[:other][:service][:tv_sat]     = nil
    body[:other][:service][:phone]      = nil

    body[:other][:composition]                    = Hash.new
    body[:other][:composition][:bar]              = nil
    body[:other][:composition][:living]           = nil
    body[:other][:composition][:dining]           = nil
    body[:other][:composition][:separate_toilet]  = nil
    body[:other][:composition][:open_kitchen]     = nil

    body[:other][:heating]                  = Hash.new
    body[:other][:heating][:electric]       = maison.present? && maison.dig(:CHAUFFAGE, :ENERGIE) && maison[:CHAUFFAGE][:ENERGIE] == 1 ? 3 : 0
    body[:other][:heating][:solar]          = maison.present? && maison.dig(:CHAUFFAGE, :ENERGIE) && maison[:CHAUFFAGE][:ENERGIE] == 1 ? 5 : 0
    body[:other][:heating][:gaz]            = maison.present? && maison.dig(:CHAUFFAGE, :ENERGIE) && maison[:CHAUFFAGE][:ENERGIE] == 1 ? 1 : 0
    body[:other][:heating][:condensation]   = nil
    body[:other][:heating][:fuel]           = maison.present? && maison.dig(:CHAUFFAGE, :ENERGIE) && maison[:CHAUFFAGE][:ENERGIE] == 1 ? 2 : 0
    body[:other][:heating][:heat_pump]      = maison.present? && maison.dig(:CHAUFFAGE, :ENERGIE) && maison[:CHAUFFAGE][:ENERGIE] == 1 ? 6 : 0
    body[:other][:heating][:floor_heating]  = nil

    body[:other][:transport]                      = Hash.new
    body[:other][:transport][:bicycle_path]       = nil
    body[:other][:transport][:public_transport]   = nil
    body[:other][:transport][:public_bike]        = nil
    body[:other][:transport][:subway]             = nil
    body[:other][:transport][:train_station]      = nil

    body[:other][:security]                     = Hash.new
    body[:other][:security][:guardian]          = nil
    body[:other][:security][:alarm]             = nil
    body[:other][:security][:intercom]          = nil
    body[:other][:security][:camera]            = nil
    body[:other][:security][:smoke_dectector]   = nil

    pictures = []
    fichiers = bien[:FICHIER].class.to_s == "Hash" ? [bien[:FICHIER]] : bien[:FICHIER]
    fichiers.each do |f|
      pictures << {:name => (f.dig(:TEXTES, :TITRE_FR) ? f[:TEXTES][:TITRE_FR] : ""), :url => f[:FICHIER_JOINT][:NOM_FICHIER]} if f.class.to_s == "Hash" && f.dig(:FICHIER_JOINT, :NOM_FICHIER) && f.dig(:FICHIER_JOINT, :TYPE_MIME) && f[:FICHIER_JOINT][:TYPE_MIME].include?("image") 
    end if bien.dig(:FICHIER)

    body[:pictures] = pictures

    return body
  end





end