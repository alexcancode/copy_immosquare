module ApplicationHelper

  def view_is_devise?
    devise_controller = [ "sessions", "registrations", "confirmations", "unlocks","passwords" ];
    if devise_controller.include?(controller_name)
      unless controller.controller_name=="registrations" and (controller.action_name=="edit" or controller.action_name=="update")
        return true
      end
    end
  end

  def nl2br(s)
    s.gsub(/\n/, '<br>')
  end


  def pluralize(count, singular, plural = nil)
    "#{count || 0} " + ((count.to_i <= 1) ? singular : (plural || singular.pluralize))
  end

  def gravatar_url(email, size=50,  secure=false, img_type='png')
    default_img ="mm"
    allow_rating="pg"
    gravatar_id = Digest::MD5.hexdigest(email)
    "http"+ (!!secure ? 's://secure.' : '://') +"gravatar.com/avatar/#{gravatar_id}.#{img_type}?s=#{size}&r=#{allow_rating}&d=#{default_img}"
  end

end
