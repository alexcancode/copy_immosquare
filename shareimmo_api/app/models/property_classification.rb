class PropertyClassification < ActiveRecord::Base

  translates :name
  has_many :magex_units

end
