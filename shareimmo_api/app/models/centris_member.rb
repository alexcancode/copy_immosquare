class CentrisMember < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :centris_office, primary_key: "code", foreign_key: "bur_code"

end
