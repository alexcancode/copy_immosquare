class User < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  rolify
  has_many :authentifications, dependent: :destroy

  ##============================================================##
  ## Before & After
  ##============================================================##
  after_create :assign_default_role


  ##============================================================##
  ## Devise
  ## ======
  ## Include default devise modules. Others available are:
  ## :confirmable, :lockable, :timeoutable and :omniauthable
  ##============================================================##
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable



  def assign_default_role
    add_role(:user)
  end


  def check_role?
    if self.has_role? :admin
      return true
    end
  end


end
