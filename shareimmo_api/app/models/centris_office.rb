class CentrisOffice < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :centris_firm, primary_key: "code", foreign_key: "firme_code"

end
