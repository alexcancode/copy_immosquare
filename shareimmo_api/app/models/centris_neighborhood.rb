class CentrisNeighborhood < ActiveRecord::Base

  belongs_to :centris_city, primary_key: "code", foreign_key: "mun_code"
end
