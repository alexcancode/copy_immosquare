class MagexUnit < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :magex_account
  belongs_to :magex_property
  belongs_to :property_classification
end
