class Zipcode < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :kijiji_location
  belongs_to :louercom_location
  belongs_to :centris_city
  belongs_to :centris_neighborhood


  ##============================================================##
  ## Next
  ##============================================================##
  def next
    current = Zipcode.where(:zipcode =>self.zipcode).first
    Zipcode.where("id > ?", current.id).first

  end

  def prev
    current = Zipcode.where(:zipcode =>self.zipcode).first
    Zipcode.where("id < ?", current.id).last
  end


end
