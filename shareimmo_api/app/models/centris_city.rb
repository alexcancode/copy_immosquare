class CentrisCity < ActiveRecord::Base

  self.primary_key = "code"
  ##============================================================##
  ## Association
  ##============================================================##
  has_many :centris_neighborhoods, primary_key: "code", foreign_key: "mun_code"
  has_one :centris_region, :foreign_key => :code, :primary_key => :region_code
end
