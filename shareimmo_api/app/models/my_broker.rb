class MyBroker < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :flag

  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters: [
      :sorted_by,
      :by_flag,
      :search_query
    ]
  )


  scope :sorted_by, lambda { |sort_key|
    direction = (sort_key =~ /desc$/) ? 'desc' : 'asc'
    case sort_key.to_s
    when /^id_/
      order("my_brokers.id #{ direction }")
    when /^city_/
      order("my_brokers.city #{ direction }")
    when /^next_step_date_/
      where.not('my_brokers.next_step_date' => nil).order("my_brokers.next_step_date #{ direction }")
    else
      order("my_brokers.id #{ direction }")
    end
  }



  scope :search_query, lambda { |query|
    # Searches the students table on the 'first_name' and 'last_name' columns.
    # Matches using LIKE, automatically appends '%' to each term.
    # LIKE is case INsensitive with MySQL, however it is case
    # sensitive with PostGreSQL. To make it work in both worlds,
    # we downcase everything.
    return nil  if query.blank?

    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)

    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      (e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }

    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conds = 4
    where(
      terms.map { |term|
        "(LOWER(my_brokers.first_name) LIKE ? OR LOWER(my_brokers.last_name) LIKE ?  OR LOWER(my_brokers.city) LIKE ? OR LOWER(my_brokers.zipcode) LIKE ?)"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conds }.flatten
    )
  }



  scope :by_flag, lambda { |property_flags_ids|
    where(:flag_id => [*property_flags_ids])
  }






end
