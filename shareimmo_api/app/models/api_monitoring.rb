class ApiMonitoring < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :api_provider


  ##============================================================##
  ## Filterrific
  ##============================================================##
  filterrific(
    default_filter_params: { sorted_by: 'id_asc' },
    available_filters: [
      :by_api,
      :sorted_by,
    ]
    )

  scope :by_api, lambda { |api_ids|
    where(:api_provider_id => [*api_ids])
  }



  scope :sorted_by, lambda { |sort_option|
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id_/
      order("api_monitorings.id #{ direction }")
    else
      order("api_monitorings.id #{ direction }")
    end
  }



end
