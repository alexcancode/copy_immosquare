class CentrisInscription < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  has_many :centris_pictures, :foreign_key => :no_inscription, :primary_key => :no_inscription
  has_many :centris_remarks, :foreign_key => :no_inscription, :primary_key => :no_inscription
  has_many :centris_addendas, :foreign_key => :no_inscription, :primary_key => :no_inscription
  has_many :centris_costs, :foreign_key => :no_inscription, :primary_key => :no_inscription
  has_one :centris_city, :foreign_key => :code, :primary_key => :mun_code
  has_one :centris_region, through: :centris_city

  ##============================================================##
  ##
  ##============================================================##
  belongs_to :broker_1, :class_name => "CentrisMember", :foreign_key => "courtier_inscripteur_1", :primary_key => :code
  belongs_to :broker_2, :class_name => "CentrisMember", :foreign_key => "courtier_inscripteur_2", :primary_key => :code
  belongs_to :broker_3, :class_name => "CentrisMember", :foreign_key => "courtier_inscripteur_3", :primary_key => :code
  belongs_to :broker_4, :class_name => "CentrisMember", :foreign_key => "courtier_inscripteur_4", :primary_key => :code

end
