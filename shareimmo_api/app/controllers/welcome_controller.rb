class WelcomeController < ApplicationController

  before_filter :authenticate_user!,:except=>[:home]

  require 'nokogiri'

  def home
  end

  def dashboard
    @filterrific = initialize_filterrific(ApiMonitoring,params[:filterrific]) or return
    @scraps = ApiMonitoring.filterrific_find(@filterrific).reverse_order

    builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml['rdf'].RDF({:xmlns=>"http://purl.org/rss/1.0/","xmlns:rdf" => "http://www.w3.org/1999/02/22-rdf-syntax-ns#","xmlns:cl"=>"http://www.craigslist.org/about/cl-bulk-ns/1.0"}) do
        xml.channel do
          xml.items do
            xml['rdf'].li({"rdf:resource"=>"NYCBrokerHousingSample1"})
          end
          xml['cl'].auth({"username"=>"craigslist@shareimmo.com","password"=>"Immo460Immo","accountID"=>"14"})
        end
        xml.item( "rdf:about"=>"NYCBrokerHousingSample1") do
          xml['cl'].category "fee"
          xml['cl'].area "nyc"
          xml['cl'].subarea "mnh"
          xml['cl'].neighborhood "Upper West Side"
          xml['cl'].housingInfo({"price"=>"1450","bedrooms"=>"0","sqft"=>"600"})
          xml['cl'].replyEmail({"privacy"=>"C"}){xml.text"bulkuser@bulkposterz.net"}
          xml['cl'].brokerInfo({"companyName"=>"Joe Sample and Associates","feeDisclosure"=>"fee di/sclosure here"})
          xml.title "Spacious Sunny Studio in Upper West Side"
          xml.description{xml.cdata("posting body goes here")}
        end
      end
    end
    @new_xml = builder.to_xml

    options = {:body =>@new_xml,:headers => {"Content-Type" => "application/x-www-form-urlencoded"}}
    @httparty = HTTParty.post("https://post.craigslist.org/bulk-rss/validate",options)


    respond_to do |format|
      format.html
      format.js
    end
  end

  def craiglist_api

  end

end

