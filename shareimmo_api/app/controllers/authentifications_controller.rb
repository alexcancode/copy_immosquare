class AuthentificationsController < ApplicationController

  def create
    auth = request.env['omniauth.auth']
    provider = ApiProvider.where(:name=>auth["provider"]).first
    user = current_user.authentifications.find_or_create_by(:api_provider_id=>provider[:id],:uid=>auth["uid"]).tap do |u|
      if auth["provider"] == 'google_oauth2'
        u.update_attributes(
          :name => auth["info"]["name"],
          :email => auth["info"]["email"],
          :first_name => auth["info"]["first_name"],
          :last_name => auth["info"]["last_name"],
          :token => auth["credentials"]["token"],
          :refresh_token => auth["credentials"]["refresh_token"],
          :expires_at=>auth["credentials"]["expires_at"],
          :expires=>auth["credentials"]["expires"],
          )
      end
    end
    redirect_to dashboard_youtube_path
  end

  def fail
    flash[:notice] = "Auth Failed #{params[:message]}"
    redirect_to dashboard_youtube_path
  end



end
