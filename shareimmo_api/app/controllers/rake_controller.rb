require 'rake'

##============================================================##
## Necessary to avoid tasks being loaded several times in dev mode
##============================================================##
Rake::Task.clear                     if Rails.env.development?
ShareimmoApi::Application.load_tasks if Rails.env.development?



class RakeController < ApplicationController

  layout false

  def run_poliris
    Rake::Task["test:import"].reenable
    Rake::Task["test:import"].invoke
  end

end
