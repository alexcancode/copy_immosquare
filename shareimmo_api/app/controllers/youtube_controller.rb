class YoutubeController < ApplicationController

  before_filter :authenticate_user!

  def index
    @videos = Wemoov.all
    @googles = Authentification.joins(:api_provider).where(:user_id=>current_user.id,:api_providers => { :name =>'google_oauth2'})
  end


end
