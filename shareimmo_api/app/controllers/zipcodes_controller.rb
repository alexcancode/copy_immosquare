class ZipcodesController < ApplicationController

  before_filter :authenticate_user!


  def index
    @zips = Zipcode.all
  end

  def update
    @zip = Zipcode.find(params[:id])
    @zip.update_attributes(clean_params)
     render nothing: true
  end

  def edit
    @zip = Zipcode.where(:zipcode => params[:id]).first
    @louer_com_locations = LouercomLocation.all
    @kijiji_locations = KijijiLocation.all
    @centris_cities = CentrisCity.all
    @centris_neighborhoods = CentrisNeighborhood.all
  end

  def debbuger
    @zip = Zipcode.find(params[:id])
    @geocoder = Geocoder.search("#{@zip.zipcode}, QC, Canada").first
    @hash = Gmaps4rails.build_markers(@geocoder) do |geocoder, marker|
      marker.lat geocoder.data["lat"]
      marker.lng geocoder.data["lon"]
    end
  end



  private

  def clean_params
    params.require(:zipcode).permit(
      :kijiji_location_id,
      :louercom_location_id,
      :best_sector,
      :centris_city_id,
      :centris_neighborhood_id
    )
  end

end
