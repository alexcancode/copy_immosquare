class CentrisController < ApplicationController

  before_filter :authenticate_user!
  authorize_resource :class => false




  def ftp
    @import_stats = CentrisImportStat.all.reverse_order
  end

  def banners
    @banners = CentrisBanner.all
  end

  def firms
    @firms = CentrisFirm.all
  end

  def offices
    @offices = CentrisOffice.all
  end

  def brokers
    @brokers = CentrisMember.all
  end

  def inscriptions
    @inscriptions = CentrisInscription.select('id,is_active,no_inscription,courtier_inscripteur_1,bureau_inscripteur_1,prix_demande,no_civique_debut,nom_rue_complet').all
  end

  def neighborhoods
    @neighborhoods = CentrisNeighborhood.all
  end

  def cities
    @cities = CentrisCity.all
  end

  def regions
    @regions = CentrisRegion.all
  end



end


