# encoding: utf-8

class MagexController < ApplicationController

  ##============================================================##
  ## Before Filter
  ##============================================================##
  before_filter :authenticate_user!
  authorize_resource :class => false

  require 'net/ftp'


  def ftp
    @stats = MagexImportStat.all.reverse_order
  end

  def accounts
    @accounts = MagexAccount.all
  end

  def buildings
    @buildings = MagexProperty.all
  end

  def units
    @units = MagexUnit.all
  end


  def ftp_sync
    FileUtils.rm_rf(Dir.glob(File.join(Rails.root,'import','magex','*')))
    folder_destination = File.join(Rails.root,'import','magex',"#{Time.now.strftime("%y-%m-%d")}")
    FileUtils.mkdir_p(folder_destination)
    magex = ImportUser.where(:status=>1,:import_provider=>'magex').first

    #============================================================##
    # Connexion au FTP -> on récupère tout les comptes actifs
    # -> puis on les transferts vers notre dossier temporaire
    #============================================================##
    Net::FTP.open(magex.ftp_domain) do |ftp|
      ftp.login
      files = ftp.chdir('ProprioExpert/ShareImmo/')
      files = ftp.nlst("*.xml")
      unless files.blank?
        files.each do |magex_client_xml|
          ftp.getbinaryfile(magex_client_xml,File.join(folder_destination, File.basename(magex_client_xml)))
        end
      end
      ftp.close
    end

    ##============================================================##
    ## Parse XML
    ##============================================================##
    staying_accounts = Array.new
    staying_units = Array.new
    Dir.foreach(folder_destination) do |file|
      next if file == '.' or file == '..'
      f = File.open(File.join(folder_destination,file))
      @xml = Nokogiri::XML(f)
      f.close

      magex_account = MagexAccount.where(:uid=>@xml.css("ID").first.content).first_or_create
      staying_accounts << magex_account.id
      if @xml.css('Active').first.content == "true"
        magex_account[:is_active] = 1
      else
        magex_account[:is_active] = 0
      end
      magex_account[:first_name] = @xml.css('FirstName').first.content
      magex_account[:last_name] = @xml.css('LastName').first.content
      magex_account[:email] = @xml.css('Email').first.content
      language = @xml.css('Language').first.content
      unless language.blank?
        if language == "English"
          magex_account[:language] = "en"
        else
          magex_account[:language] = "fr"
        end
      end
      magex_account[:phone] = @xml.css('TelephoneNumber Number').first.content unless @xml.css('TelephoneNumber Number').blank?
      magex_account[:published_at] = @xml.css('PublishedOn').first.content.split('T').first
      magex_account[:secure_token] = @xml.css('SecurityCode').first.content
      if magex_account.changed?
        magex_account.save({:validate=>false})
      end


      properties = @xml.css("Property")
      properties.each do |property|
        magex_property = MagexProperty.where(:uid=>"#{1000000000+magex_account.uid}#{property.css('BaseID').first.content.to_i}").first_or_create
        magex_property[:magex_account_id] = magex_account.id
        magex_property[:identification] = property.css('Identification').first.content
        magex_property[:address] = "#{property.css('Address Street').first.content}, #{property.css('Address City').first.content}, #{property.css('Address PostalCode').first.content}, #{property.css('Address Province').first.content}  #{property.css('Address Country').first.content}"
        magex_property[:city] = property.css('Address City').first.content
        magex_property[:year_of_built] = property.css('Yearbuilt').first.content unless property.css('Yearbuilt').first.content == "0"
        magex_property[:number_of_units] = property.css('NumberOfUnits').first.content
        magex_property[:ready_for_rent_date] = property.css('ReadyForRentDate').first.content.split('T').first unless property.css('ReadyForRentDate').first.content.blank?
        magex_property[:purchase_price] = property.css('PurchasePrice').first.content unless property.css('PurchasePrice').first.content == "0"
        magex_property[:building_evaluation] = property.css('BuildingEvaluation').first.content unless property.css('BuildingEvaluation').first.content == "0"
        magex_property[:building_area] = property.css('BuildingArea').first.content unless property.css('BuildingArea').first.content == "0"
        magex_property[:building_area_type] = property.css('BuildingAreaType').first.content unless property.css('BuildingArea').first.content == "0"
        magex_property[:land_area] = property.css('LandArea').first.content unless property.css('LandArea').first.content == "0"
        magex_property[:land_area_type] = property.css('LandAreaType').first.content unless property.css('LandArea').first.content == "0"
        magex_property[:taxes_municipal] = property.css('TaxesMunicipal').first.content unless property.css('TaxesMunicipal').first.content == "0"
        magex_property[:taxes_school] = property.css('TaxesSchool').first.content unless property.css('TaxesSchool').first.content == "0"
        magex_property[:taxes_water] = property.css('TaxesWater').first.content unless property.css('TaxesWater').first.content == "0"
        magex_property[:taxes_other] = property.css('TaxesOther').first.content unless property.css('TaxesOther').first.content == "0"
        if magex_property.changed?
          magex_property.save(:validate=>false)
        end
        unless property.css('Units').children.count == 0
          units = property.css("Unit")
          units.each do |unit|
            magex_unit = MagexUnit.where(:uid=>"#{1000000000+magex_account.uid}#{unit.css('BaseID').first.content}").first_or_create
            staying_units << magex_unit.id
            magex_unit[:magex_account_id] = magex_account.id
            magex_unit[:magex_property_id] = magex_property.id
            magex_unit[:is_active] = 1
            if unit.css('Leases').children.count == 0
              magex_unit[:is_vacant] = 1
            end
            magex_unit[:identification] = unit.css('Identification').first.content
            magex_unit[:unit_type] = unit.css('Type').first.content
            if property_classifier = PropertyClassifier.where(:name=>magex_unit[:unit_type]).first
              magex_unit[:property_classification_id] = property_classifier.property_classification_id
            end
            magex_unit[:address] = "#{unit.css('Address Street').first.content}, #{unit.css('Address City').first.content}, #{unit.css('Address PostalCode').first.content}, #{unit.css('Address Province').first.content}  #{unit.css('Address Country').first.content}"
            magex_unit[:city] = unit.css('Address City').first.content
            magex_unit[:notes] = unit.css('Notes').first.content
            magex_unit[:number_of_rooms] = unit.css('NumberOfRooms').first.content unless unit.css('NumberOfRooms').first.content == "0"
            magex_unit[:number_of_bedrooms] = unit.css('NumberOfBedrooms').first.content unless unit.css('NumberOfBedrooms').first.content == "0"
            magex_unit[:area] = unit.css('Area').first.content unless unit.css('Area').first.content == "0"
            magex_unit[:area_type] = unit.css('AreaType').first.content unless unit.css('Area').first.content == "0"
            magex_unit[:commercial] = unit.css('Commercial').first.content
            magex_unit[:service_stove] = unit.css('ServiceStove').first.content
            magex_unit[:service_stove_detail] = unit.css('ServiceStoveDetail').first.content
            magex_unit[:service_washer_dryer] = unit.css('ServiceWasherDryerHookup').first.content
            magex_unit[:service_washer_dryer_detail] = unit.css('ServiceWasherDryerHookupDetail').first.content
            magex_unit[:service_furnished] = unit.css('ServiceFurnished').first.content
            magex_unit[:service_furnished_detail] = unit.css('ServiceFurnishedDetail').first.content
            magex_unit[:service_semi_furnished] = unit.css('ServiceSemiFurnished').first.content
            magex_unit[:service_semi_furnished_detail] = unit.css('ServiceSemiFurnishedDetail').first.content
            magex_unit[:service_heating] = unit.css('ServiceHeating').first.content
            magex_unit[:service_heating_detail] = unit.css('ServiceHeatingDetail').first.content
            magex_unit[:service_electricity] = unit.css('ServiceElectricity').first.content
            magex_unit[:service_electricity_detail] = unit.css('ServiceElectricityDetail').first.content
            magex_unit[:service_hot_water] = unit.css('ServiceHotWater').first.content
            magex_unit[:service_hot_water_detail] = unit.css('ServiceHotWaterDetail').first.content
            magex_unit[:lease_next_payment_interval] = unit.css('LeaseNextPaymentInterval').first.content
            magex_unit[:lease_next_duration] = unit.css('LeaseNextDuration').first.content
            magex_unit[:lease_next_duration_frequency] = unit.css('LeaseNextDurationFrequency').first.content
            magex_unit[:lease_next_dwelling_price] = unit.css('LeaseNextDwellingPrice').first.content
            if magex_unit.changed?
              magex_unit.save({:validate=>false})
            end
          end
        end
      end
    end

    MagexAccount.where.not(:id=>staying_accounts).each do |delete_item|
      delete_item.is_active = 0
      delete_item.save
    end

    MagexUnit.where.not(:id=>staying_units).each do |delete_item|
      delete_item.is_active = 0
      delete_item.save
    end

    ##============================================================##
    ## Statistique
    ##============================================================##
    @stat = MagexImportStat.new
    @stat.accounts_count = MagexAccount.count
    @stat.properties_count = MagexProperty.count
    @stat.units_count = MagexUnit.count
    @stat.save


    ##============================================================##
    ## Users
    ##============================================================##
    accounts = Array.new
    MagexAccount.where(:is_active=>1).each do |user|
      accounts << {
        :provider => 'magex',
        :uid => user.uid,
        :info =>{
          :nickname => "magex#{user.uid}",
          :email => user.email,
          :name => "#{user.first_name} #{user.last_name}",
          :first_name => user.first_name,
          :last_name => user.last_name,
          :phone=> user.phone
        },
        :credentials => {
          :token => user.secure_token
        },
        :extras => {
          :country => "",
          :is_a_pro_user => 0,
          :spoken_languages => user.language,
          :website => "",
          :picture => "",
          :agency_name => "",
          :agency_address => "",
          :agency_city => "",
          :agency_zipcode => "",
          :agency_province => "",
          :logo => ""
        }
      }
    end

    ##============================================================##
    ## Properties
    ##============================================================##
    properties = Array.new
    MagexUnit.all.each.all.each do |unit|
    area_unit =  unit.area_type == "SquareFeets" ? 1 : 2
    rooms = unit.number_of_rooms.include?(" ½") ? unit.number_of_rooms.split(" ½").first.to_i : rooms = unit.number_of_rooms.to_i
    properties << {
      :uid=>unit.uid,
      :status => unit.is_vacant,
      :account_uid => unit.magex_account.uid,
      :address => unit.magex_property.address,
      :title_fr => "#{unit.magex_property.city} - #{I18n.with_locale('fr-ca'){unit.property_classification.name}} ##{unit.uid}",
      :title_en => "#{unit.magex_property.city} - #{I18n.with_locale('en-ca'){unit.property_classification.name}} ##{unit.uid}",
      :description_fr => unit.notes,
      :unit=> unit.identification,
      :property_classification=> unit.property_classification_id,
      :property_status => 1,
      :property_listing => 2,
      :area_unit => area_unit,
      :area => unit.area,
      :rooms=> rooms,
      :bedrooms=> unit.number_of_bedrooms,
      :rent=> unit.lease_next_dwelling_price,
      :currency=> "CAD",
      }
    end

    ##============================================================##
    ## Sync
    ##============================================================##
    options = {:body => {:provider => 'magex',:accounts => accounts}}
    httparty = HTTParty.post("#{ENV['api_base']}/registrations",options)
    @response= JSON.parse(httparty.body)


    respond_to do |format|
      format.js
    end
  end


end


