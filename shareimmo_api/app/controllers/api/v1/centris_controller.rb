class Api::V1::CentrisController < Api::ApiController

  def brokers
    if params[:offices_ids].present?
      firms     = CentrisFirm.where(:no_certificat  => params[:offices_ids])
      offices   = CentrisOffice.where(:firme_code   => firms.pluck(:code)).pluck(:code)
      @brokers  = CentrisMember.where(:bur_code  => offices) + CentrisMember.where(:no_certificat=> params[:brokers_ids])
    else
      @brokers  = CentrisMember.where(:no_certificat=> params[:brokers_ids])
    end
  end


end
