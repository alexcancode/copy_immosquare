class Api::V1::PagesimmoController < Api::ApiController

  def sync
    PagesimmoSyncWorker.new.perform({:client_id => params[:client_id]})
    return render :json =>{:message=> "OK"}, :status=> 200
  end

end