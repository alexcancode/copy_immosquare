class MyBrokersController < ApplicationController

  before_filter :authenticate_user!


  def index
    @filterrific = initialize_filterrific(MyBroker,params[:filterrific]) or return
    @my_brokers = @filterrific.find.page(params[:page])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @my_broker = MyBroker.new
  end


  def create
    @my_broker = MyBroker.new(clean_params)
    @my_broker.save
    redirect_to my_brokers_path
  end


  def edit
    @my_broker = MyBroker.find(params[:id])
  end

  def update
    @my_broker = MyBroker.find(params[:id])
    @my_broker.update_attributes(clean_params)
    redirect_to my_brokers_path
  end


  private

  def clean_params
    params.require(:my_broker).permit(
      :first_name,
      :last_name,
      :phone,
      :phone2,
      :email,
      :agency_name,
      :street_number,
      :street_name,
      :city,
      :province,
      :zipcode,
      :flag_id,
      :note,
      :next_step_date,
      :next_step_id,
      :website,
      :website_techno_id,
      :phoner_id
    )
  end

end
