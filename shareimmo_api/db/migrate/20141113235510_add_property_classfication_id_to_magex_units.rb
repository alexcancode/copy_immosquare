class AddPropertyClassficationIdToMagexUnits < ActiveRecord::Migration
  def change
    add_reference :magex_units, :property_classification, index: true, :default=>1, :after=> :identification
  end
end
