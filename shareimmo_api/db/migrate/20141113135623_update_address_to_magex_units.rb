class UpdateAddressToMagexUnits < ActiveRecord::Migration
  def change
    add_column :magex_units, :address, :string, :after=>:type
    remove_column :magex_units, :AddressType
    remove_column :magex_units, :AddressCountry
    remove_column :magex_units, :AddressProvince
    remove_column :magex_units, :AddressCity
    remove_column :magex_units, :AddressStreet
    remove_column :magex_units, :AddressUnitNumber
    remove_column :magex_units, :AddressPostalCode
    remove_column :magex_units, :AddressCivicNumber
    remove_column :magex_units, :Reserved
    remove_column :magex_units, :Floor
    remove_column :magex_units, :FloorDetail
    remove_column :magex_units, :EntryAccessKey
    remove_column :magex_units, :MeterElectric
    remove_column :magex_units, :MeterGaz
    rename_column :magex_units, :Notes, :notes
    rename_column :magex_units, :NumberOfRooms, :number_of_rooms
    rename_column :magex_units, :NumberOfBedrooms, :number_of_bedrooms
    rename_column :magex_units, :Area, :area
    rename_column :magex_units, :AreaType, :area_type
    rename_column :magex_units, :Commercial, :commercial
    rename_column :magex_units, :ServiceHotWater, :service_hot_water
    rename_column :magex_units, :ServiceHotWaterDetail, :service_hot_water_detail
    rename_column :magex_units, :ServiceStove, :service_stove
    rename_column :magex_units, :ServiceStoveDetail, :service_stove_detail
    rename_column :magex_units, :ServiceWasherDryerHookup, :service_washer_dryer
    rename_column :magex_units, :ServiceWasherDryerHookupDetail, :service_washer_dryer_detail
    rename_column :magex_units, :ServiceFurnished, :service_furnished
    rename_column :magex_units, :ServiceFurnishedDetail, :service_furnished_detail
    rename_column :magex_units, :ServiceSemiFurnished, :service_semi_furnished
    rename_column :magex_units, :ServiceSemiFurnishedDetail, :service_semi_furnished_detail
    rename_column :magex_units, :ServiceHeating, :service_heating
    rename_column :magex_units, :ServiceHeatingDetail, :service_heating_detail
    rename_column :magex_units, :ServiceElectricity, :service_electricity
    rename_column :magex_units, :ServiceElectricityDetail, :service_electricity_detail
  end
end
