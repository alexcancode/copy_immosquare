class CreateAuthentifications < ActiveRecord::Migration
  def change
    create_table :authentifications do |t|
      t.references :user, index: true
      t.string :uid
      t.references :api_provider, index: true
      t.string :token
      t.integer :expires_at
      t.string :token_secret
      t.integer :expires
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :nickname
      t.string :image
      t.string :location
      t.string :link
      t.string :name

      t.timestamps
    end
  end
end
