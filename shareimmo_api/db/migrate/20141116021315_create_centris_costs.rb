class CreateCentrisCosts < ActiveRecord::Migration
  def change
    create_table :centris_costs do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.string :tdep_code, :limit => 10
      t.integer :montant_depense
      t.integer :annee
      t.integer :annee_expiration
      t.string :frequence, :limit => 10
      t.string :part_depense, :limit => 10
    end
    add_index :centris_costs, [:is_active]
    add_index :centris_costs, [:no_inscription]
    add_index :centris_costs, [:tdep_code]
    add_index :centris_costs, [:part_depense]
  end
end
