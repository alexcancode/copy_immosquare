class AddIsActiveToMagexProperties < ActiveRecord::Migration
  def change
    add_column :magex_properties, :is_active, :integer, :default=>1, :after=>:magex_account_id, :index=>true
  end
end
