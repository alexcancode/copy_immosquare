class CreateMagexAccounts < ActiveRecord::Migration
  def change
    create_table :magex_accounts do |t|
      t.references :magex, index: true
      t.string :active
      t.date :published_at
      t.string :secure_token

      t.timestamps
    end
  end
end
