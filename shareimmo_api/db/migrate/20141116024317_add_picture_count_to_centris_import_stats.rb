class AddPictureCountToCentrisImportStats < ActiveRecord::Migration
  def change
    add_column :centris_import_stats, :pictures_count, :integer
  end
end
