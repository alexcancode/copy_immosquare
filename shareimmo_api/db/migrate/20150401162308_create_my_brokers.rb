class CreateMyBrokers < ActiveRecord::Migration
  def change
    create_table :my_brokers do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :phone2
      t.string :email
      t.string :agency_name
      t.string :street_number
      t.string :street_name
      t.string :city
      t.string :province
      t.string :zipcode
      t.references :flag, index: true

      t.timestamps
    end
  end
end
