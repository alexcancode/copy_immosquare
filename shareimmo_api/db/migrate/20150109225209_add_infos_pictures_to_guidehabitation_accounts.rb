class AddInfosPicturesToGuidehabitationAccounts < ActiveRecord::Migration
  def change
    add_column :guidehabitation_accounts, :picture_rewrite, :integer, :default=>1, :after =>:picture
    add_column :guidehabitation_accounts, :logo_rewrite, :integer, :default=>1, :after => :logo
  end
end
