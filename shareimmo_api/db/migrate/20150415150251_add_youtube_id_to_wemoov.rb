class AddYoutubeIdToWemoov < ActiveRecord::Migration
  def change
    add_column :wemoovs, :youtube_id, :string, :after => :uid
  end
end
