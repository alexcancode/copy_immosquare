class CreateImportStat < ActiveRecord::Migration
  def change
    create_table :import_stats do |t|
      t.integer :properties_count, :default =>0
      t.integer :properties_actived_count, :default => 0
      t.integer :properties_deleted_count, :default => 0
      t.text :modif_properties
      t.timestamps
    end
  end
end
