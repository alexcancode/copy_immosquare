class AddModelCentrisVisitesLibre < ActiveRecord::Migration
   create_table :centris_visites_libres do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription
      t.integer :seq
      t.string :date_debut
      t.string :date_fin
      t.string :commentaires_f
      t.string :commentaires_a
    end
    add_index :centris_visites_libres, [:no_inscription]
end


