class AddDeleteOrRemoveoldAdvetrsToGuidehabitationAccounts < ActiveRecord::Migration
  def change
    add_column :guidehabitation_accounts, :delete_or_inactive_old_adverts, :string, :default => "delete"
  end
end
