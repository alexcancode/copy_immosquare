class CreateCentrisOffices < ActiveRecord::Migration
  def change
    create_table :centris_offices do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :code, :limit => 12
      t.string :firme_code, :limit => 12
      t.string :nom_legal, :limit => 40
      t.string :no_civique, :limit => 10
      t.string :nom_rue, :limit => 60
      t.string :bureau, :limit => 10
      t.string :municipalite, :limit => 50
      t.string :province, :limit => 10
      t.string :code_postal, :limit => 6
      t.string :telephone_1, :limit => 20
      t.integer :poste_1
      t.string :telephone_2, :limit => 20
      t.integer :poste_2
      t.string :telephone_fax, :limit => 20
      t.string :courriel, :limit => 150
      t.string :site_web, :limit => 150
      t.string :directeur_code, :limit => 10
    end
    add_index :centris_offices, [:is_active]
    add_index :centris_offices, [:code]
  end
end

