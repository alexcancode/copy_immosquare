class CreateCentrisPictures < ActiveRecord::Migration
  def change
    create_table :centris_pictures do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.integer :seq
      t.string :nom_fichier_photo, :limit => 20
      t.string :code_description_photo, :limit => 10
      t.string :description_francaise, :limit => 60
      t.string :description_anglaise, :limit => 60
      t.string :photourl
      t.string :no_version
      t.datetime :date_modification
    end
    add_index :centris_pictures, [:is_active]
    add_index :centris_pictures, [:no_inscription]
    add_index :centris_pictures, [:seq]
  end
end
