class AddCityToMagexUnits < ActiveRecord::Migration
  def change
    add_column :magex_units, :city, :string, :after=>:address
    add_column :magex_properties, :city, :string, :after=>:address
  end
end
