class CreateGuidehabitationProperties < ActiveRecord::Migration
  def change
    create_table :guidehabitation_properties do |t|
      t.integer :uid, :index=>true
      t.integer :account_uid, index: true
      t.references :guidehabitation_account, index: true
      t.integer :is_active, :index =>true, :default =>1
      t.string :address
      t.float :latitude
      t.float :longitude
      t.string :title
      t.text :description
      t.string :unit
      t.integer :mls
      t.references :property_classification ,:index=>true
      t.references :property_status ,:index=>true
      t.references :property_listing ,:index=>true
      t.integer :area_unit
      t.integer :area
      t.integer :year_of_build
      t.integer :rooms
      t.integer :bedrooms
      t.integer :bathrooms
      t.integer :price
      t.integer :price
      t.integer :tax_1
      t.integer :tax_2
      t.integer :condo_fees
      t.string :currency
      t.integer :international_posting
      t.integer :video_posting
      t.timestamps
    end
  end
end
