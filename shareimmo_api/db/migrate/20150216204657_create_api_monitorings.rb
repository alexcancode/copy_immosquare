class CreateApiMonitorings < ActiveRecord::Migration
  def change
    create_table :api_monitorings do |t|
      t.references :api_provider, index: true
      t.integer :ads_count

      t.timestamps
    end
  end
end
