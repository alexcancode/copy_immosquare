class CreateGuidehabitationAccounts < ActiveRecord::Migration
  def change
    create_table :guidehabitation_accounts do |t|
      t.integer :uid, :index=>true
      t.integer :is_active, :index=> true, :default =>1
      t.string :email
      t.string :username
      t.string :country
      t.integer :is_a_pro_user
      t.string :spoken_languages
      t.string :token
      t.string :public_name
      t.string :phone
      t.string :website
      t.string :picture
      t.string :agency_name
      t.string :agency_address
      t.string :agency_city
      t.string :agency_zipcode
      t.string :agency_province
      t.string :logo

      t.timestamps
    end
  end
end
