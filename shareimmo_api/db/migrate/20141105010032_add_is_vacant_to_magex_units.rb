class AddIsVacantToMagexUnits < ActiveRecord::Migration
  def change
    add_column :magex_units, :is_vacant, :integer,:default=>0
  end
end
