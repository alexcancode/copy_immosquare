class CreateCentrisIncomes < ActiveRecord::Migration
  def change
    create_table :centris_incomes do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.integer :seq
      t.string :type_espace, :limit => 10
      t.integer :nb_unites_totales
      t.integer :nb_unites_vacantes
      t.string :description_francaise, :limit => 60
      t.string :description_anglaise, :limit => 60
      t.string :champ_inutilise_1, :limit => 10
      t.integer :superficie_totale, :limit=>8
      t.string :um_superficie_totale, :limit => 10
      t.integer :champ_inutilise_2, :limit=>8
      t.string :champ_inutilise_3, :limit => 10
      t.integer :champ_inutilise_4
      t.string :champ_inutilise_5, :limit => 10
      t.datetime :champ_inutilise_6
      t.integer :champ_inutilise_7, :limit=>8
      t.string :champ_inutilise_8, :limit => 10
    end
    add_index :centris_incomes, [:is_active]
    add_index :centris_incomes, [:no_inscription]
    add_index :centris_incomes, [:seq]
  end
end
