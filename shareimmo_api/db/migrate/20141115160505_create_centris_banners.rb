class CreateCentrisBanners < ActiveRecord::Migration
  def change
    create_table :centris_banners do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :code, :limit => 10
      t.string :nom_francais, :limit => 60
      t.string :nom_anglais, :limit => 60
      t.string :nom_abrege_francais, :limit => 15
      t.string :nom_abrege_anglais, :limit => 15
    end
    add_index :centris_banners, [:is_active]
    add_index :centris_banners, [:code]
  end
end
