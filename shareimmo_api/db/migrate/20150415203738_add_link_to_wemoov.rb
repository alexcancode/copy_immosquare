class AddLinkToWemoov < ActiveRecord::Migration
  def change
    add_column :wemoovs, :link, :string, :after => :youtube_id
  end
end
