class FixDataSizeCentris < ActiveRecord::Migration
  def change
    change_column :centris_inscriptions, :courtier1_type_divul_interet, :string, :limit => 255
    change_column :centris_inscriptions, :courtier2_type_divul_interet, :string, :limit => 255
    change_column :centris_inscriptions, :courtier3_type_divul_interet, :string, :limit => 255
    change_column :centris_inscriptions, :courtier4_type_divul_interet, :string, :limit => 255
  end
end
