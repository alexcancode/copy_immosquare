class RemoveIsActiveToCentris2 < ActiveRecord::Migration
  def change
    remove_column :centris_inscriptions, :is_active, :integer
    remove_column :centris_members, :is_active, :integer
  end
end
