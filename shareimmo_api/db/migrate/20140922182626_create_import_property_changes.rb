class CreateImportPropertyChanges < ActiveRecord::Migration
  def change
    create_table :import_property_changes do |t|
      t.references :centris_property, index: true
      t.string :field_modified
      t.string :old_value
      t.string :new_value

      t.timestamps
    end
  end
end
