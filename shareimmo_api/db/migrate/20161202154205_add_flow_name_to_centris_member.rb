class AddFlowNameToCentrisMember < ActiveRecord::Migration
  def change
    add_column :centris_members, :flow_name, :string, :after => :id
  end
end
