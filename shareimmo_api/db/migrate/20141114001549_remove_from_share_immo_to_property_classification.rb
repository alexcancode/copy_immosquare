class RemoveFromShareImmoToPropertyClassification < ActiveRecord::Migration
  def change
    remove_column :property_classifications, :from_shareimmo, :integer
  end
end
