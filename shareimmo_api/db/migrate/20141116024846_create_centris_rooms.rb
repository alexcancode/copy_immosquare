class CreateCentrisRooms < ActiveRecord::Migration
  def change
    create_table :centris_rooms do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.integer :seq
      t.integer :ordre_affichage
      t.string :piece_code, :limit => 10
      t.integer :champ_inutilise_1, :limit=>8
      t.integer :champ_inutilise_2, :limit=>8
      t.string :dimensions, :limit => 15
      t.string :ind_irregulier, :limit => 1
      t.string :etage, :limit => 10
      t.string :couvre_plancher_code, :limit => 10
      t.string :informations_francaises, :limit => 30
      t.string :informations_anglaises, :limit => 30
      t.string :champ_inutilise_3, :limit => 1
    end
    add_index :centris_rooms, [:is_active]
    add_index :centris_rooms, [:no_inscription]
    add_index :centris_rooms, [:seq]
  end
end
