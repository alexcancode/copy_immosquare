class CreateCentrisRegions < ActiveRecord::Migration
  def change
    create_table :centris_regions do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :code, :limit => 3
      t.string :description_francaise, :limit => 60
      t.string :description_anglaise, :limit => 60
    end
    add_index :centris_regions, [:is_active]
    add_index :centris_regions, [:code]
  end
end
