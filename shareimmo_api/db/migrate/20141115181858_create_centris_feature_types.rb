class CreateCentrisFeatureTypes < ActiveRecord::Migration
  def change
    create_table :centris_feature_types do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :code, :limit => 15
      t.string :description_abregee_francaise, :limit => 15
      t.string :description_francaise, :limit => 60
      t.string :description_abregee_anglaise, :limit => 15
      t.string :description_anglaise, :limit => 60
      t.string :ind_plusieurs_criteres, :limit => 1
    end
    add_index :centris_feature_types, [:is_active]
    add_index :centris_feature_types, [:code]
  end
end
