class AddPropertiesModifedNoteToImportStat < ActiveRecord::Migration
  def change
    add_column :import_stats, :properties_modified_note, :text
  end
end
