class RemoveLastEmails < ActiveRecord::Migration
  def change
    remove_column :magex_accounts, :last_emails
  end
end
