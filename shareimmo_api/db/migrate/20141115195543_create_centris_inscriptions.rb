class CreateCentrisInscriptions < ActiveRecord::Migration
  def change
    create_table :centris_inscriptions do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.string :champ_inutilise_1, :limit => 10
      t.string :courtier_inscripteur_1, :limit => 10
      t.string :bureau_inscripteur_1, :limit => 12
      t.string :courtier_inscripteur_2, :limit => 10
      t.string :bureau_inscripteur_2, :limit => 12
      t.integer :prix_demande,:limit=>8
      t.string :um_prix_demande, :limit => 10
      t.string :devise_prix_demande, :limit => 10
      t.integer :prix_location_demande,:limit=>8
      t.string :um_prix_location_demande, :limit => 10
      t.string :devise_prix_location_demande, :limit => 10
      t.string :ind_clause_restrictive, :limit => 1
      t.string :code_declaration_vendeur, :limit => 10
      t.string :ind_reprise_finance, :limit => 1
      t.string :ind_internet, :limit => 1
      t.string :ind_echange_possible, :limit => 1
      t.string :clef, :limit => 10
      t.string :champ_inutilise_3, :limit => 30
      t.string :champ_inutilise_4, :limit => 10
      t.datetime :date_inscription
      t.datetime :date_expiration
      t.string :mun_code, :limit => 6
      t.string :quartr_code, :limit => 4
      t.string :pres_de, :limit => 60
      t.string :no_civique_debut, :limit => 10
      t.string :no_civique_fin, :limit => 10
      t.string :nom_rue_complet, :limit => 60
      t.string :appartement, :limit => 8
      t.string :code_postal, :limit => 6
      t.string :cadastre, :limit => 40
      t.string :ind_certificat_localisation, :limit => 1
      t.integer :annee_certificat_localisation
      t.string :champ_inutilise_5, :limit => 60
      t.string :champ_inutilise_6, :limit => 80
      t.string :champ_inutilise_7, :limit => 20
      t.string :champ_inutilise_8, :limit => 20
      t.integer :champ_inutilise_9
      t.string :champ_inutilise_10, :limit => 60
      t.string :champ_inutilise_11, :limit => 80
      t.string :champ_inutilise_12, :limit => 20
      t.string :champ_inutilise_13, :limit => 20
      t.integer :champ_inutilise_14
      t.datetime :date_occupation
      t.string :delai_occupation_francais, :limit => 15
      t.string :delai_occupation_anglais, :limit => 15
      t.datetime :date_acte_vente
      t.string :delai_acte_vente_francais, :limit => 15
      t.string :delai_acte_vente_anglais, :limit => 15
      t.datetime :date_fin_bail
      t.string :tel_rendez_vous
      t.string :champ_inutilise_15, :limit => 50
      t.string :code_rendez_vous, :limit => 10
      t.string :categorie_propriete, :limit => 10
      t.string :genre_propriete, :limit => 3
      t.string :type_batiment, :limit => 10
      t.string :type_copropriete, :limit => 10
      t.string :niveau, :limit => 10
      t.integer :nb_etages
      t.integer :annee_contruction
      t.string :code_annee_construction, :limit => 10
      t.string :champ_inutilise_16, :limit => 20
      t.integer :facade_batiment,:limit=>8
      t.integer :profondeur_batiment,:limit=>8
      t.string :ind_irregulier_batiment, :limit => 1
      t.string :um_dimension_batiment, :limit => 10
      t.integer :superficie_batiment,:limit=>8
      t.string :um_superficie_batiment, :limit => 10
      t.integer :superficie_habitable,:limit=>8
      t.string :um_superficie_habitable, :limit => 10
      t.string :champ_inutilise_17, :limit => 20
      t.integer :facade_terrain,:limit=>8
      t.integer :profondeur_terrain,:limit=>8
      t.string :ind_irregulier_terrain, :limit => 1
      t.string :um_dimension_terrain, :limit => 10
      t.integer :superficie_terrain,:limit=>8
      t.string :um_superficie_terrain, :limit => 10
      t.string :matricule, :limit => 24
      t.integer :annee_evaluation
      t.integer :evaluation_municipale_terrain
      t.integer :evaluation_municipale_batiment
      t.integer :nb_pieces
      t.integer :nb_chambres
      t.integer :nb_chambres_sous_sol
      t.integer :nb_chambres_hors_sol
      t.integer :nb_salles_bains
      t.integer :nb_salles_eau
      t.string :nom_fichier_photo,:limit=>20
      t.string :no_contrat_courtage,:limit=>15
      t.integer :champ_inutilise_18
      t.integer :champ_inutilise_19
      t.integer :champ_inutilise_20
      t.integer :champ_inutilise_21
      t.integer :depenses_tot_exploitation
      t.integer :champ_inutilise_22
      t.integer :champ_inutilise_23
      t.string :nom_plan_eau,:limit=>50
      t.integer :champ_inutilise_24, :limit=>8
      t.integer :champ_inutilise_25
      t.integer :nb_chauffe_eau_loue
      t.text :inclus_francais
      t.text :inclus_anglais
      t.text :exclus_francais
      t.text :exclus_anglais
      t.integer :nb_unites_total
      t.string :champ_inutilise_26, :limit=>10
      t.string :champ_inutilise_27, :limit=>1
      t.integer :champ_inutilise_28
      t.string :champ_inutilise_29, :limit=>1
      t.string :champ_inutilise_30, :limit=>40
      t.string :champ_inutilise_31, :limit=>10
      t.string :champ_inutilise_32, :limit=>10
      t.string :ind_adresse_sur_internet, :limit=>1
      t.datetime :date_modif
      t.string :frequence_prix_location, :limit=>10
      t.string :code_statut, :limit=>10
      t.integer :pourc_quote_part, :limit=>8
      t.string :utilisation_commerciale, :limit=>10
      t.string :champ_inutilise_2, :limit=>10
      t.string :nom_du_parc, :limit=>30
      t.datetime :date_promesse_achat_cond
      t.string :ind_promesse_achat_cond, :limit=>1
      t.string :raison_sociale, :limit=>40
      t.string :en_oper_depuis, :limit=>6
      t.string :ind_franchise, :limit=>1
      t.string :champ_inutilise_33, :limit=>40
      t.string :champ_inutilise_34, :limit=>10
      t.string :champ_inutilise_35, :limit=>10
      t.string :ind_opt_renouv_bail, :limit=>1
      t.string :annee_mois_echeance_bail, :limit=>6
      t.string :url_visite_virtuelle_francais, :limit=>150
      t.string :url_visite_virtuelle_anglais, :limit=>150
      t.string :url_desc_detaillee, :limit=>180
      t.string :ind_taxes_prix_demande, :limit=>1
      t.string :ind_taxes_prix_location_demande, :limit=>1
      t.string :courtier_inscripteur_3
      t.string :bureau_inscripteur_3
      t.string :courtier_inscripteur_4
      t.string :bureau_inscripteur_4
      t.string :courtier1_type_divul_interet,:limit=>10
      t.string :courtier2_type_divul_interet,:limit=>10
      t.string :courtier3_type_divul_interet,:limit=>10
      t.string :courtier4_type_divul_interet,:limit=>10
      t.string :ind_vente_sans_garantie_legale, :limit=>1
      t.integer :latitude
      t.integer :longitude
      t.string :type_superficie_habitable
      t.integer :rev_pot_brut_res
      t.integer :rev_pot_brut_comm
      t.integer :rev_pot_brut_stat
      t.integer :rev_pot_brut_au
      t.datetime :date_rev_brut_pot
    end
    add_index :centris_inscriptions, [:is_active]
    add_index :centris_inscriptions, [:no_inscription]
  end
end









