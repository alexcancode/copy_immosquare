class AddTitleEnToGuidehabitationProperty < ActiveRecord::Migration
  def change
    rename_column :guidehabitation_properties, :title, :title_fr
    rename_column :guidehabitation_properties, :description, :description_fr
    add_column :guidehabitation_properties, :title_en, :string, :after => :title_fr
    add_column :guidehabitation_properties, :description_en, :text, :after=> :description_fr
    add_column :guidehabitation_properties, :property_url_fr, :string, :after=> :description_en
    add_column :guidehabitation_properties, :property_url_en, :string, :after=> :property_url_fr
  end
end
