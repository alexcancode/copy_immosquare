class UpdateDateToCentrisInscriptions < ActiveRecord::Migration
  def change
    change_column :centris_inscriptions, :date_modif, :datetime
  end
end
