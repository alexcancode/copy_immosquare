class RemoveMagexUnitIdToMagexUnits < ActiveRecord::Migration
  def change
    rename_column :magex_units, :Identification, :identification
    add_column :magex_units, :uid, :integer, :after=>:id, :index=>true
    remove_reference :magex_units, :magex_unit, index: true
    remove_column :magex_units, :active_item
    add_column :magex_units, :is_active, :integer, :after=>:magex_property_id, :index=>true,:default=>0
  end
end
