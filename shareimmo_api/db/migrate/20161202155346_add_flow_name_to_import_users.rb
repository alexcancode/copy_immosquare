class AddFlowNameToImportUsers < ActiveRecord::Migration
  def change
    add_column :import_users, :flow_name, :string, :after => :status
  end
end
