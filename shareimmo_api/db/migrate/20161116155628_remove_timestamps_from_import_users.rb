class RemoveTimestampsFromImportUsers < ActiveRecord::Migration
  def change
    remove_column :import_users, :created_at, :string
    remove_column :import_users, :updated_at, :string
  end
end
