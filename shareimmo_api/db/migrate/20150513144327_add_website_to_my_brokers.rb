class AddWebsiteToMyBrokers < ActiveRecord::Migration
  def change
    add_column :my_brokers, :website, :string
    add_reference :my_brokers, :website_techno, index: true
  end
end
