class CreateMagexUnits < ActiveRecord::Migration
  def change
    create_table :magex_units do |t|
      t.references :magex_account, index: true
      t.references :magex_property, index: true
      t.references :magex_unit, index: true
      t.integer :active_item, :default => 1
      t.string :Type
      t.string :Identification
      t.string :NumberOfRooms
      t.integer :NumberOfBedrooms
      t.integer :Floor
      t.string :FloorDetail
      t.integer :Area
      t.string :AreaType
      t.string :Commercial
      t.string :Reserved
      t.string :EntryAccessKey
      t.string :MeterElectric
      t.string :MeterGaz
      t.string :ServiceStove
      t.string :ServiceStoveDetail
      t.string :ServiceWasherDryerHookup
      t.string :ServiceWasherDryerHookupDetail
      t.string :ServiceFurnished
      t.string :ServiceFurnishedDetail
      t.string :ServiceSemiFurnished
      t.string :ServiceSemiFurnishedDetail
      t.string :ServiceHeating
      t.string :ServiceHeatingDetail
      t.string :ServiceElectricity
      t.string :ServiceElectricityDetail
      t.string :ServiceHotWater
      t.string :ServiceHotWaterDetail
      t.string :LeaseNextPaymentInterval
      t.integer :LeaseNextDuration
      t.string :LeaseNextDurationFrequency
      t.integer :LeaseNextDwellingPrice
      t.text :Notes
      t.string :AddressType
      t.string :AddressCountry
      t.string :AddressProvince
      t.string :AddressCity
      t.string :AddressStreet
      t.string :AddressCivicNumber
      t.string :AddressUnitNumber
      t.string :AddressPostalCode

      t.timestamps
    end
    add_index :magex_units, [:active_item]
  end
end
