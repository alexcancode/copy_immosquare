class AddVisibilityToGuidehabitationProperties < ActiveRecord::Migration
  def change
    remove_column :guidehabitation_properties, :international_posting
    remove_column :guidehabitation_properties, :video_posting
    add_column :guidehabitation_properties, :mini_site, :integer, :after =>:currency
    add_column :guidehabitation_properties, :international, :integer, :after =>:mini_site
    add_column :guidehabitation_properties, :youtube, :integer, :after =>:international
    add_column :guidehabitation_properties, :kijiji, :integer, :after =>:youtube
    add_column :guidehabitation_properties, :solihome, :integer, :after =>:kijiji
  end
end
