class CreateMagexProperties < ActiveRecord::Migration
  def change
    create_table :magex_properties do |t|
      t.references :magex_account, index: true
      t.references :magex_property, index: true
      t.string :Identification
      t.string :Yearbuilt
      t.integer :NumberOfUnits
      t.date :ReadyForRentDate
      t.string :PurchasePrice
      t.string :BuildingEvaluation
      t.integer :BuildingArea
      t.string :BuildingAreaType
      t.string :LandArea
      t.string :LandAreaType
      t.integer :TaxesMunicipal
      t.integer :TaxesSchool
      t.integer :TaxesWater
      t.integer :TaxesOther
      t.string :AddressType
      t.string :AddressCountry
      t.string :AddressProvince
      t.string :AddressCity
      t.string :AddressStreet
      t.string :AddressCivicNumber
      t.string :AddressUnitNumber
      t.string :AddressPostalCode

      t.timestamps
    end
  end
end
