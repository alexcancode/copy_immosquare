class CreateCentrisMembres < ActiveRecord::Migration
  def change
    create_table :centris_membres do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :code, :limit => 10
      t.string :bur_code, :limit => 12
      t.string :no_certificat, :limit => 15
      t.string :type_certificat, :limit => 10
      t.string :nom, :limit => 40
      t.string :prenom, :limit => 40
      t.string :titre_professionnel, :limit => 40
      t.string :champ_inutilise_1, :limit => 10
      t.string :telephone_1, :limit => 20
      t.string :telephone_2, :limit => 20
      t.string :telephone_fax, :limit => 20
      t.string :courriel, :limit => 150
      t.string :site_web, :limit => 150
      t.string :nom_fichier_photo, :limit => 20
      t.string :code_langue, :limit => 1
      t.string :photo_url
      t.string :date_modification
      t.string :nom_societe, :limit => 180
      t.string :type_societe_desc_f, :limit => 180
      t.string :type_societe_desc_a, :limit => 180
    end
    add_index :centris_membres, [:is_active]
    add_index :centris_membres, [:code]
  end
end
