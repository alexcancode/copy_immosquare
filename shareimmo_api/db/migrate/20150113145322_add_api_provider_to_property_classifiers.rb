class AddApiProviderToPropertyClassifiers < ActiveRecord::Migration
  def change
    add_column :property_classifiers, :provider, :string, :after => :name
  end
end
