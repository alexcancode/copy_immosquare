class CreatePropertyClassifications < ActiveRecord::Migration
  def change
    create_table :property_classifications do |t|
      t.string :name
      t.references :shareimmo, index: true

      t.timestamps
    end
  end
end
