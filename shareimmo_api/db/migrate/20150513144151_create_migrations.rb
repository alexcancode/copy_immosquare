class CreateMigrations < ActiveRecord::Migration
  def change
    create_table :migrations do |t|
      t.string :AddWebsiteToMyBrokers
      t.string :website
      t.references :website_techno, index: true

      t.timestamps
    end
  end
end
