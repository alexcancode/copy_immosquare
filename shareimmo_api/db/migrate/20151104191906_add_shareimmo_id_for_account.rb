class AddShareimmoIdForAccount < ActiveRecord::Migration
  def change
    add_column :magex_accounts, :shareimmo_id, :integer, :after => :uid, :default => 0
    add_column :magex_properties, :shareimmo_id, :integer, :after => :uid, :default => 0
    add_column :magex_units, :shareimmo_id, :integer, :after => :uid, :default => 0
  end
end
