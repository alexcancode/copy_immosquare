class RemoveInfosPropToMagexImportStats < ActiveRecord::Migration
  def change
    remove_column :magex_import_stats, :accounts_active_count, :integer
    remove_column :magex_import_stats, :units_active_count, :integer
  end
end
