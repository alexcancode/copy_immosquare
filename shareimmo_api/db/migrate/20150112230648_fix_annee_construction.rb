class FixAnneeConstruction < ActiveRecord::Migration
  def change
    rename_column :centris_inscriptions, :annee_contruction, :annee_construction
  end
end
