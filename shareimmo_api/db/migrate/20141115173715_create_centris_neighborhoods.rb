class CreateCentrisNeighborhoods < ActiveRecord::Migration
  def change
    create_table :centris_neighborhoods do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :mun_code, :limit => 6
      t.string :code, :limit => 4
      t.string :description_francaise, :limit => 60
      t.string :description_anglaise, :limit => 60
    end
    add_index :centris_neighborhoods, [:is_active]
    add_index :centris_neighborhoods, [:mun_code]
    add_index :centris_neighborhoods, [:code]
  end
end

