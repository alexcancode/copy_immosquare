class AddDescriptionToWemoov < ActiveRecord::Migration
  def change
    add_column :wemoovs, :youtube_description, :text
  end
end
