class AddInfosToCentrisVisitesLibres < ActiveRecord::Migration
  def change
    add_column :centris_visites_libres, :heure_debut, :string, :after => :date_fin
    add_column :centris_visites_libres, :heure_fin, :string, :after => :heure_debut

    ##============================================================##
    ## Décembre 2016 Changements
    ##============================================================##
    add_column :centris_inscriptions, :particularite_construction, :string
    add_column :centris_offices, :url_logo_bureau, :string

  end
end
