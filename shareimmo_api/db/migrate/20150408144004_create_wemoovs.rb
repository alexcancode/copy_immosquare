class CreateWemoovs < ActiveRecord::Migration
  def change
    create_table :wemoovs do |t|
      t.integer :uid
      t.string :backlink

      t.timestamps
    end
  end
end
