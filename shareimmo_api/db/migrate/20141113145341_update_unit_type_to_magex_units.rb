class UpdateUnitTypeToMagexUnits < ActiveRecord::Migration
  def change
    rename_column :magex_units, :type, :unit_type
  end
end
