class AddLouerComLocations < ActiveRecord::Migration
  def change
    create_table :louercom_locations do |t|
      t.string :name
      t.integer :region_id
      t.timestamps
    end
    add_reference :zipcodes, :louercom_location, :after => :kijiji_location_id
  end
end
