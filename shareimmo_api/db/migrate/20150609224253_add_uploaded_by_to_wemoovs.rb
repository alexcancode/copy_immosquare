class AddUploadedByToWemoovs < ActiveRecord::Migration
  def change
    add_column :wemoovs, :uploaded_by, :string, :after => :youtube_id
  end
end
