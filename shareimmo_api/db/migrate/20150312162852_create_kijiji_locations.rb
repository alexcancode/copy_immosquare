class CreateKijijiLocations < ActiveRecord::Migration
  def change
    create_table :kijiji_locations do |t|
      t.string :name

      t.timestamps
    end
  end
end
