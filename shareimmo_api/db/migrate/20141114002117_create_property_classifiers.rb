class CreatePropertyClassifiers < ActiveRecord::Migration
  def change
    create_table :property_classifiers do |t|
      t.string :name
      t.references :property_classification, index: true

      t.timestamps
    end
  end
end
