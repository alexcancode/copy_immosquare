class AddPieceIntergene < ActiveRecord::Migration
  def change
    create_table :centris_piece_intergenerations do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.integer :seq
      t.string :piece_code
      t.string :dimensions
      t.string :ind_irregulier
      t.string :etage
      t.string :couvre_plancher_code
      t.string :informations_francaises
      t.string :informations_anglaises
    end
    add_index :centris_piece_intergenerations, [:no_inscription]


  end
end
