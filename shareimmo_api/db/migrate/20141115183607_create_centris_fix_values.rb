class CreateCentrisFixValues < ActiveRecord::Migration
  def change
    create_table :centris_fix_values do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :domaine, :limit => 70
      t.string :valeur, :limit => 70
      t.string :description_abregee_francaise, :limit => 15
      t.string :description_abregee_anglaise, :limit => 15
      t.string :description_francaise, :limit => 150
      t.string :description_anglaise, :limit => 150
    end
    add_index :centris_fix_values, [:is_active]
    add_index :centris_fix_values, [:domaine]
    add_index :centris_fix_values, [:valeur]
  end
end
