class CreateCentrisFirms < ActiveRecord::Migration
  def change
    create_table :centris_firms do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :code, :limit => 12
      t.string :nom_legal, :limit => 80
      t.string :no_certificat, :limit => 10
      t.string :type_certificat, :limit => 10
      t.string :banniere_code, :limit => 10
      t.string :firme_principale, :limit => 12
      t.string :courtier_code, :limit => 10
    end
    add_index :centris_firms, [:is_active]
    add_index :centris_firms, [:code]
  end
end

