class UpdateTypeToMagexUnits < ActiveRecord::Migration
  def change
    rename_column :magex_units, :Type, :type
    change_column :magex_units, :type,:string, :after=> :identification
    change_column :magex_units, :is_vacant, :integer, :after=> :is_active
  end
end
