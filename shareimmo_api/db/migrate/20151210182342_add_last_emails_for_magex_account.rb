class AddLastEmailsForMagexAccount < ActiveRecord::Migration
  def change
    add_column :magex_accounts, :last_emails, :text
  end
end
