class RemoveRewriteLogoToGuidehabitationAccounts < ActiveRecord::Migration
  def change
    remove_column :guidehabitation_accounts, :logo_rewrite, :string
    remove_column :guidehabitation_accounts, :picture_rewrite, :string
  end
end
