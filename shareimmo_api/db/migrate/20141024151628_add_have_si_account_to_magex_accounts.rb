class AddHaveSiAccountToMagexAccounts < ActiveRecord::Migration
  def change
    add_column :magex_accounts, :have_si_account, :integer, :default => 0
  end
end
