class RemoveTimestampsFromTable < ActiveRecord::Migration
  def change
    remove_column :centris_addendas, :created_at, :string
    remove_column :centris_addendas, :updated_at, :string

    remove_column :centris_banners, :created_at, :string
    remove_column :centris_banners, :updated_at, :string

    remove_column :centris_cities, :created_at, :string
    remove_column :centris_cities, :updated_at, :string

    remove_column :centris_costs, :created_at, :string
    remove_column :centris_costs, :updated_at, :string

    remove_column :centris_feature_subtypes, :created_at, :string
    remove_column :centris_feature_subtypes, :updated_at, :string

    remove_column :centris_feature_types, :created_at, :string
    remove_column :centris_feature_types, :updated_at, :string

    remove_column :centris_firms, :created_at, :string
    remove_column :centris_firms, :updated_at, :string

    remove_column :centris_fix_values, :created_at, :string
    remove_column :centris_fix_values, :updated_at, :string


    remove_column :centris_incomes, :created_at, :string
    remove_column :centris_incomes, :updated_at, :string

    remove_column :centris_inscriptions, :created_at, :string
    remove_column :centris_inscriptions, :updated_at, :string

    remove_column :centris_members, :created_at, :string
    remove_column :centris_members, :updated_at, :string

    remove_column :centris_neighborhoods, :created_at, :string
    remove_column :centris_neighborhoods, :updated_at, :string

    remove_column :centris_offices, :created_at, :string
    remove_column :centris_offices, :updated_at, :string

    remove_column :centris_pictures, :created_at, :string
    remove_column :centris_pictures, :updated_at, :string

    remove_column :centris_piece_intergenerations, :created_at, :string
    remove_column :centris_piece_intergenerations, :updated_at, :string

    remove_column :centris_property_classifications, :created_at, :string
    remove_column :centris_property_classifications, :updated_at, :string

    remove_column :centris_regions, :created_at, :string
    remove_column :centris_regions, :updated_at, :string

    remove_column :centris_remarks, :created_at, :string
    remove_column :centris_remarks, :updated_at, :string

    remove_column :centris_renovations, :created_at, :string
    remove_column :centris_renovations, :updated_at, :string

    remove_column :centris_rooms, :created_at, :string
    remove_column :centris_rooms, :updated_at, :string

    remove_column :centris_specifications, :created_at, :string
    remove_column :centris_specifications, :updated_at, :string

    remove_column :centris_visites_libres, :created_at, :string
    remove_column :centris_visites_libres, :updated_at, :string



  end
end
