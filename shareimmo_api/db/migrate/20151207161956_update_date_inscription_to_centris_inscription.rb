class UpdateDateInscriptionToCentrisInscription < ActiveRecord::Migration
  def change

	rename_column :centris_inscriptions, :date_inscription, :date_mise_en_vigueur

  end
end
