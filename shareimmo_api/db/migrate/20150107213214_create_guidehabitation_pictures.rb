class CreateGuidehabitationPictures < ActiveRecord::Migration
  def change
    create_table :guidehabitation_pictures do |t|
      t.references :guidehabitation_property, index: true
      t.string :small_url
      t.string :medium_url
      t.string :large_url
      t.timestamps
    end
  end
end
