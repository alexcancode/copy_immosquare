class ChangeAddressToMagexProperties < ActiveRecord::Migration
  def change
    add_column :magex_properties, :address, :string, :after=>:identification
    remove_column :magex_properties, :AddressType
    remove_column :magex_properties, :AddressCountry
    remove_column :magex_properties, :AddressProvince
    remove_column :magex_properties, :AddressCity
    remove_column :magex_properties, :AddressStreet
    remove_column :magex_properties, :AddressUnitNumber
    remove_column :magex_properties, :AddressPostalCode
    rename_column :magex_properties, :Yearbuilt, :year_of_built
    rename_column :magex_properties, :NumberOfUnits, :number_of_units
    rename_column :magex_properties, :ReadyForRentDate, :ready_for_rent_date
    rename_column :magex_properties, :PurchasePrice, :purchase_price
    rename_column :magex_properties, :BuildingEvaluation, :building_evaluation
    rename_column :magex_properties, :BuildingArea, :building_area
    rename_column :magex_properties, :BuildingAreaType, :building_area_type
    rename_column :magex_properties, :LandArea, :land_area
    rename_column :magex_properties, :LandAreaType, :land_area_type
    rename_column :magex_properties, :TaxesMunicipal, :taxes_municipal
    rename_column :magex_properties, :TaxesSchool, :taxes_school
    rename_column :magex_properties, :TaxesWater, :taxes_water
    rename_column :magex_properties, :TaxesOther, :taxes_other
  end
end
