class UpdateCentris2015 < ActiveRecord::Migration
  def change
    rename_column :centris_addendas, :ind_internet, :champ_inutilise_1
    rename_column :centris_addendas, :ind_sia, :champ_inutilise_2
    rename_column :centris_remarks, :ind_internet, :champ_inutilise_1
    rename_column :centris_remarks, :ind_sia, :champ_inutilise_2
    rename_column :centris_members, :nom_fichier_photo, :champ_inutilise_2

  end
end
