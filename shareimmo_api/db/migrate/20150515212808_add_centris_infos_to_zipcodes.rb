class AddCentrisInfosToZipcodes < ActiveRecord::Migration
  def change
    add_reference :zipcodes, :centris_city, index: true
    add_reference :zipcodes, :centris_neighborhood, index: true
  end
end
