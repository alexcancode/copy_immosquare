class ChangeUidFormatToMagexUnits < ActiveRecord::Migration
  def change
    change_column :magex_properties, :uid, :integer, :limit=>8
    change_column :magex_units, :uid, :integer, :limit=>8
  end
end
