class AddNextStepToMyBrokers < ActiveRecord::Migration
  def change
    add_column :my_brokers, :next_step_date, :datetime
    add_reference :my_brokers, :next_step, index: true
  end
end
