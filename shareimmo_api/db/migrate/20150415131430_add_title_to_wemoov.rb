class AddTitleToWemoov < ActiveRecord::Migration
  def change
    add_column :wemoovs, :title, :string, :after => :uid
  end
end
