class CreateCentrisImportStats < ActiveRecord::Migration
  def change
    create_table :centris_import_stats do |t|
      t.integer :members_count, :default=>0
      t.integer :offices_count, :default=>0
      t.integer :properties_count,:default=>0
      t.timestamps
    end

    drop_table :import_property_changes
    drop_table :import_stats

  end
end
