class CreateCentrisAddendas < ActiveRecord::Migration
  def change
    create_table :centris_addendas do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.integer :no_addenda
      t.string :code_langue, :limit => 14
      t.integer :ordre_affichage
      t.string :ind_internet, :limit => 1
      t.string :ind_sia, :limit => 1
      t.string :texte, :limit => 60
    end
    add_index :centris_addendas, [:is_active]
    add_index :centris_addendas, [:no_inscription]
    add_index :centris_addendas, [:no_addenda]
    add_index :centris_addendas, [:ordre_affichage]
  end
end
