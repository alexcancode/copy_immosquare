class RemoveSmallUrlToGuidehabitationPictures < ActiveRecord::Migration
  def change
    add_column :guidehabitation_pictures, :url, :string, :after => :guidehabitation_property_id
    remove_column :guidehabitation_pictures, :small_url, :string
    remove_column :guidehabitation_pictures, :medium_url, :string
    remove_column :guidehabitation_pictures, :large_url, :string
  end
end
