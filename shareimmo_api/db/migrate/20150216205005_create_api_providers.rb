class CreateApiProviders < ActiveRecord::Migration
  def change
    create_table :api_providers do |t|
      t.string :name
      t.string :commercial_name

      t.timestamps
    end
  end
end
