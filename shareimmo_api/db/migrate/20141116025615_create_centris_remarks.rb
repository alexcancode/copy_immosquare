class CreateCentrisRemarks < ActiveRecord::Migration
  def change
    create_table :centris_remarks do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.integer :no_remarque
      t.string :code_langue, :limit => 10
      t.integer :ordre_affichage
      t.string :ind_internet, :limit => 1
      t.string :ind_sia, :limit => 1
      t.text :texte
    end
    add_index :centris_remarks, [:is_active]
    add_index :centris_remarks, [:no_inscription]
    add_index :centris_remarks, [:no_remarque]
    add_index :centris_remarks, [:ordre_affichage]
  end
end
