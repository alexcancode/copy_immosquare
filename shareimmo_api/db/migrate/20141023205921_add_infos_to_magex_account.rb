class AddInfosToMagexAccount < ActiveRecord::Migration
  def change
    add_column :magex_accounts, :email, :string
    add_column :magex_accounts, :first_name, :string
    add_column :magex_accounts, :last_name, :string
    add_column :magex_accounts, :phone, :string
  end
end
