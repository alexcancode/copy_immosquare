class FixDataSizeCentris4 < ActiveRecord::Migration
  def change
    change_column :centris_addendas, :texte, :string, :limit => 255
  end
end
