class AddEnviToWeemov < ActiveRecord::Migration
  def change
    add_column :wemoovs, :environment, :string, :after => :uid
  end
end
