class AddRefreshTokenToAuthentifications < ActiveRecord::Migration
  def change
    add_column :authentifications, :refresh_token, :string, :after=>:token
  end
end
