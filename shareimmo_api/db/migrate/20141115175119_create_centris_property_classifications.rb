class CreateCentrisPropertyClassifications < ActiveRecord::Migration
  def change
    create_table :centris_property_classifications do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :categorie_propriete, :limit => 10
      t.string :genre_propriete, :limit => 3
      t.string :description_abregee_francaise, :limit => 15
      t.string :description_francaise, :limit => 60
      t.string :description_abregee_anglaise, :limit => 15
      t.string :description_anglaise, :limit => 60
    end
    add_index :centris_property_classifications, [:is_active]
    add_index :centris_property_classifications, [:categorie_propriete]
    add_index :centris_property_classifications, [:genre_propriete]
  end
end
