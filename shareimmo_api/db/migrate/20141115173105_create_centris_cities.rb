class CreateCentrisCities < ActiveRecord::Migration
  def change
    create_table :centris_cities do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :code, :limit => 6
      t.string :description, :limit => 60
      t.string :region_code, :limit => 3
    end
    add_index :centris_cities, [:is_active]
    add_index :centris_cities, [:code]
  end
end
