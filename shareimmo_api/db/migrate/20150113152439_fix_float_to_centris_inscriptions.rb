class FixFloatToCentrisInscriptions < ActiveRecord::Migration
  def change
    change_column :centris_inscriptions, :latitude, :string
    change_column :centris_inscriptions, :longitude, :string
  end
end
