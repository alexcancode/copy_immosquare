class AddFromshareimmoToPropertyClassifications < ActiveRecord::Migration
  def change
    add_column :property_classifications, :from_shareimmo, :integer, :default=>0,:index=>true,:after=>:shareimmo_id
  end
end
