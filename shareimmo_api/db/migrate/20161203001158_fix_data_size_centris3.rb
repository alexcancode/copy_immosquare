class FixDataSizeCentris3 < ActiveRecord::Migration
  def change
    change_column :centris_offices, :province, :string, :limit => 255
  end
end
