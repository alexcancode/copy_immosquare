class CreateCentrisSpecifications < ActiveRecord::Migration
  def change
    create_table :centris_specifications do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.string :tcar_code, :limit => 10
      t.string :scarac_code, :limit => 4
      t.integer :nombre
      t.string :informations_francaises, :limit => 60
      t.string :informations_anglaises, :limit => 60
    end
    add_index :centris_specifications, [:is_active]
    add_index :centris_specifications, [:no_inscription]
    add_index :centris_specifications, [:tcar_code]
    add_index :centris_specifications, [:scarac_code]
  end
end
