class CreateCentrisRenovations < ActiveRecord::Migration
  def change
    create_table :centris_renovations do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :no_inscription, :limit => 14
      t.integer :seq
      t.string :renovation_type, :limit => 10
      t.integer :annee
      t.integer :champ_inutilise_1
      t.string :informations_francaises, :limit => 30
      t.string :informations_anglaises, :limit => 30
    end
    add_index :centris_renovations, [:is_active]
    add_index :centris_renovations, [:no_inscription]
    add_index :centris_renovations, [:seq]
  end
end
