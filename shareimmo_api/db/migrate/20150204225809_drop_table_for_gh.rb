class DropTableForGh < ActiveRecord::Migration
  def change
    drop_table :guidehabitation_pictures
    drop_table :guidehabitation_properties
    drop_table :guidehabitation_accounts
  end
end
