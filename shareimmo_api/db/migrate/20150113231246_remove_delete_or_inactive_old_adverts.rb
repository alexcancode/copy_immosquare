class RemoveDeleteOrInactiveOldAdverts < ActiveRecord::Migration
  def change
    remove_column :guidehabitation_accounts, :delete_or_inactive_old_adverts
  end
end
