class UpdateLeasesToMagexUnits < ActiveRecord::Migration
  def change
    rename_column :magex_units, :LeaseNextPaymentInterval, :lease_next_payment_interval
    rename_column :magex_units, :LeaseNextDuration, :lease_next_duration
    rename_column :magex_units, :LeaseNextDurationFrequency, :lease_next_duration_frequency
    rename_column :magex_units, :LeaseNextDwellingPrice, :lease_next_dwelling_price
    change_column :magex_units, :notes, :text, :after=> :address
  end
end
