class ChangeUidToWemoov < ActiveRecord::Migration
  def change
    change_column :wemoovs, :uid, :integer, :limit => 8
  end
end
