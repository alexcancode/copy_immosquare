class AddCentrisVisits < ActiveRecord::Migration
  def change
    create_table :centris_visits do |t|

      t.timestamps
      t.string :no_inscription, :limit => 14

      t.integer :seq

      t.date :date_debut
      t.date :date_fin
      t.time :heure_debut
      t.time :heure_fin

      t.string :commentaires_f
      t.string :commentaires_a

      t.timestamps

    end
    add_index :centris_visits, [:no_inscription]

  end
end
