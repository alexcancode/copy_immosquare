class AddPropertiesModifiedCountToImportStat < ActiveRecord::Migration
  def change
    add_column :import_stats, :properties_modified_count, :integer, :default => 0
  end
end
