class FixTableMemberName < ActiveRecord::Migration
  def change
    rename_table :centris_membres, :centris_members
  end
end
