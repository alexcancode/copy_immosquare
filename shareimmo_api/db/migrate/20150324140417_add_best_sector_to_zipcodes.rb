class AddBestSectorToZipcodes < ActiveRecord::Migration
  def change
    add_column :zipcodes, :best_sector, :string, :after => :zipcode
  end
end
