class CreateDezrezs < ActiveRecord::Migration
  def change
    create_table :dezrezs do |t|
      t.integer :uid
      t.string :visibility
      t.integer :eaid
      t.integer :bid
      t.integer :sale
      t.integer :category
      t.string :price
      t.float :priceVal
      t.integer :poa
      t.string :currency
      t.integer :rentalperiod
      t.integer :sold
      t.string :statusFlag
      t.integer :bedrooms
      t.integer :bathrooms
      t.integer :receptions
      t.integer :garages
      t.integer :gardens
      t.integer :parkingSpaces
      t.integer :otherrooms
      t.integer :specials
      t.integer :leaseType
      t.integer :metropix
      t.integer :propertyType
      t.string :period
      t.integer :branchRef
      t.integer :portalID
      t.float :latitude
      t.float :longitude
      t.float :yaw
      t.float :pitch
      t.float :zoom
      t.integer :smallPicWidth
      t.integer :largePicWidth
      t.string :contact_name
      t.string :contact_tel
      t.string :contact_email
      t.string :contact_address
      t.string :contact_city
      t.string :contact_county
      t.string :contact_country
      t.string :contact_zipcode
      t.string :property_street_number
      t.string :property_street_name
      t.string :property_town
      t.string :property_city
      t.string :property_county
      t.string :property_country
      t.string :property_zipcode
      t.string :property_useAddress
      t.text :description
      t.string :charges
      t.string :pricetext
      t.text :areas
      t.timestamps
    end
  end
end
