class RemoveIsActiveToCentris < ActiveRecord::Migration
  def change
    remove_column :centris_addendas, :is_active, :integer
    remove_column :centris_banners, :is_active, :integer
    remove_column :centris_cities, :is_active, :integer
    remove_column :centris_costs, :is_active, :integer
    remove_column :centris_feature_subtypes, :is_active, :integer
    remove_column :centris_feature_types, :is_active, :integer
    remove_column :centris_firms, :is_active, :integer
    remove_column :centris_fix_values, :is_active, :integer
    remove_column :centris_incomes, :is_active, :integer
    remove_column :centris_neighborhoods, :is_active, :integer
    remove_column :centris_offices, :is_active, :integer
    remove_column :centris_pictures, :is_active, :integer
    remove_column :centris_piece_intergenerations, :is_active, :integer
    remove_column :centris_property_classifications, :is_active, :integer
    remove_column :centris_regions, :is_active, :integer
    remove_column :centris_remarks, :is_active, :integer
    remove_column :centris_renovations, :is_active, :integer
    remove_column :centris_rooms, :is_active, :integer
    remove_column :centris_specifications, :is_active, :integer
    remove_column :centris_visites_libres, :is_active, :integer

  end
end
