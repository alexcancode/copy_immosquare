class UpdateCentris20152 < ActiveRecord::Migration
  def change
    rename_column :centris_inscriptions, :ind_clause_restrictive, :champ_inutilise_36
    rename_column :centris_inscriptions, :clef, :champ_inutilise_37
    rename_column :centris_inscriptions, :date_expiration, :champ_inutilise_38
    rename_column :centris_inscriptions, :cadastre, :champ_inutilise_39
    rename_column :centris_inscriptions, :ind_certificat_localisation, :champ_inutilise_40
    rename_column :centris_inscriptions, :annee_certificat_localisation, :champ_inutilise_41
    rename_column :centris_inscriptions, :date_acte_vente, :champ_inutilise_42
    rename_column :centris_inscriptions, :delai_acte_vente_francais, :champ_inutilise_43
    rename_column :centris_inscriptions, :delai_acte_vente_anglais, :champ_inutilise_44
    rename_column :centris_inscriptions, :tel_rendez_vous, :champ_inutilise_52
    rename_column :centris_inscriptions, :code_rendez_vous, :champ_inutilise_45
    rename_column :centris_inscriptions, :matricule, :champ_inutilise_46
    rename_column :centris_inscriptions, :nom_fichier_photo, :champ_inutilise_47
    rename_column :centris_inscriptions, :no_contrat_courtage, :champ_inutilise_48
    rename_column :centris_inscriptions, :ind_adresse_sur_internet, :champ_inutilise_49
    rename_column :centris_inscriptions, :date_promesse_achat_cond, :champ_inutilise_50
    rename_column :centris_inscriptions, :ind_promesse_achat_cond, :champ_inutilise_51
  end
end
