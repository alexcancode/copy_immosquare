class AddIsActiveToMagexAccounts < ActiveRecord::Migration
  def change
    remove_column :magex_accounts, :active
    remove_column :magex_accounts, :magex_id
    add_column :magex_accounts, :uid, :integer, :after=>:id, :index=>true
    add_column :magex_accounts, :is_active, :integer, :default=>1, :after=>:uid, :index=>true
    add_column :magex_accounts, :language, :string, :after=> 'is_active'
    change_column :magex_accounts, :first_name, :string, :after=> :language
    change_column :magex_accounts, :last_name, :string, :after=> :first_name
    change_column :magex_accounts, :email, :string, :after=> :last_name
    change_column :magex_accounts, :phone, :string, :after=> :email
  end
end
