class CreateImportUsers < ActiveRecord::Migration
  def change
    create_table :import_users do |t|
      t.integer :status,:default => 1
      t.string :import_provider
      t.string :ftp_user
      t.string :ftp_password

      t.timestamps
    end
  end
end
