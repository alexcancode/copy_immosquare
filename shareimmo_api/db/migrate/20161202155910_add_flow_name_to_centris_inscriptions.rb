class AddFlowNameToCentrisInscriptions < ActiveRecord::Migration
  def change
    add_column :centris_inscriptions, :flow_name, :string, :after => :id
  end
end
