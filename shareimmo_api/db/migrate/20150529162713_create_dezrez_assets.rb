class CreateDezrezAssets < ActiveRecord::Migration
  def change
    create_table :dezrez_assets do |t|
      t.references :dezrez, index: true
      t.integer :uid
      t.string :caption
      t.string :url

      t.timestamps
    end
  end
end
