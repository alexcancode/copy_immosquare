class ChangeInfosToMagexProperties < ActiveRecord::Migration
  def change
    add_column :magex_properties, :uid, :integer,:after=>:id
    rename_column :magex_properties, :Identification, :identification
    remove_column :magex_properties, :magex_property_id
  end
end
