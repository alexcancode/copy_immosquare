class FixDateToCentrisInscriptions < ActiveRecord::Migration
  def change
    change_column :centris_inscriptions, :date_inscription,  :string , :limit => 20
    change_column :centris_inscriptions, :date_expiration,  :string , :limit => 20
    change_column :centris_inscriptions, :date_occupation,  :string , :limit => 20
    change_column :centris_inscriptions, :date_acte_vente,  :string , :limit => 20
    change_column :centris_inscriptions, :date_fin_bail,  :string , :limit => 20
    change_column :centris_inscriptions, :date_modif,  :string , :limit => 20
    change_column :centris_inscriptions, :date_promesse_achat_cond,  :string , :limit => 20
    change_column :centris_inscriptions, :date_rev_brut_pot,  :string , :limit => 20
  end
end
