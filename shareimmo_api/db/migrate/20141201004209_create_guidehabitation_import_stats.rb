class CreateGuidehabitationImportStats < ActiveRecord::Migration
  def change
    create_table :guidehabitation_import_stats do |t|
      t.integer :accounts_count
      t.integer :properties_count

      t.timestamps
    end
  end
end
