class CreateCentrisFeatureSubtypes < ActiveRecord::Migration
  def change
    create_table :centris_feature_subtypes do |t|
      t.integer :is_active, :default => 1
      t.timestamps
      t.string :tcar_code, :limit => 4
      t.string :code, :limit => 4
      t.string :code, :limit => 4
      t.string :description_abregee_francaise, :limit => 15
      t.string :description_francaise, :limit => 60
      t.string :description_abregee_anglaise, :limit => 15
      t.string :description_anglaise, :limit => 60
    end
    add_index :centris_feature_subtypes, [:is_active]
    add_index :centris_feature_subtypes, [:code]
    add_index :centris_feature_subtypes, [:tcar_code]
  end
end
