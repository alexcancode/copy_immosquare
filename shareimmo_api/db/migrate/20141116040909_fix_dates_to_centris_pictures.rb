class FixDatesToCentrisPictures < ActiveRecord::Migration
  def change
    change_column :centris_pictures, :date_modification,  :string , :limit => 50
  end
end
