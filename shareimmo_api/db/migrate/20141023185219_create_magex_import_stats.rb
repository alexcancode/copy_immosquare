class CreateMagexImportStats < ActiveRecord::Migration
  def change
    create_table :magex_import_stats do |t|
      t.integer :accounts_count, :default=>0
      t.integer :accounts_active_count, :default=>0
      t.integer :properties_count, :default=>0
      t.integer :units_count, :default=>0
      t.integer :units_active_count, :default=>0

      t.timestamps
    end
  end
end
