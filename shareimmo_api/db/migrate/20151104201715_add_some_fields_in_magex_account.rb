class AddSomeFieldsInMagexAccount < ActiveRecord::Migration
  def change
    add_column :magex_accounts, :country_id, :string, :after => :shareimmo_id
    add_column :magex_accounts, :language_id, :string, :after => :country_id
    add_column :magex_accounts, :company_name, :string, :after => :language_id
    add_column :magex_accounts, :web_site, :string, :after => :company_name
  end
end
