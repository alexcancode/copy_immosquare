class CreateZipcodes < ActiveRecord::Migration
  def change
    create_table :zipcodes do |t|
      t.string :zipcode
      t.string :country
      t.string :province
      t.references :kijiji_location, index: true

      t.timestamps
    end
  end
end
