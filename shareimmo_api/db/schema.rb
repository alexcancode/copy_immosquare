# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161203002931) do

  create_table "api_apps", force: :cascade do |t|
    t.string   "key",             limit: 255
    t.string   "token",           limit: 255
    t.integer  "api_provider_id", limit: 8
    t.string   "note_interal",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "api_apps", ["api_provider_id"], name: "index_api_apps_on_api_provider_id", using: :btree

  create_table "api_monitorings", force: :cascade do |t|
    t.integer  "api_provider_id", limit: 8
    t.integer  "ads_count",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "api_monitorings", ["api_provider_id"], name: "index_api_monitorings_on_api_provider_id", using: :btree

  create_table "api_providers", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "commercial_name", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authentifications", force: :cascade do |t|
    t.integer  "user_id",         limit: 8
    t.string   "uid",             limit: 255
    t.integer  "api_provider_id", limit: 8
    t.string   "token",           limit: 255
    t.string   "refresh_token",   limit: 255
    t.integer  "expires_at",      limit: 4
    t.string   "token_secret",    limit: 255
    t.integer  "expires",         limit: 4
    t.string   "email",           limit: 255
    t.string   "first_name",      limit: 255
    t.string   "last_name",       limit: 255
    t.string   "nickname",        limit: 255
    t.string   "image",           limit: 255
    t.string   "location",        limit: 255
    t.string   "link",            limit: 255
    t.string   "name",            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authentifications", ["api_provider_id"], name: "index_authentifications_on_api_provider_id", using: :btree
  add_index "authentifications", ["user_id"], name: "index_authentifications_on_user_id", using: :btree

  create_table "centris_addendas", force: :cascade do |t|
    t.string  "no_inscription",    limit: 14
    t.integer "no_addenda",        limit: 4
    t.string  "code_langue",       limit: 14
    t.integer "ordre_affichage",   limit: 4
    t.string  "champ_inutilise_1", limit: 1
    t.string  "champ_inutilise_2", limit: 1
    t.text    "texte",             limit: 65535
  end

  add_index "centris_addendas", ["no_addenda"], name: "index_centris_addendas_on_no_addenda", using: :btree
  add_index "centris_addendas", ["no_inscription"], name: "index_centris_addendas_on_no_inscription", using: :btree
  add_index "centris_addendas", ["ordre_affichage"], name: "index_centris_addendas_on_ordre_affichage", using: :btree

  create_table "centris_banners", force: :cascade do |t|
    t.string "code",                limit: 10
    t.string "nom_francais",        limit: 60
    t.string "nom_anglais",         limit: 60
    t.string "nom_abrege_francais", limit: 15
    t.string "nom_abrege_anglais",  limit: 15
  end

  add_index "centris_banners", ["code"], name: "index_centris_banners_on_code", using: :btree

  create_table "centris_cities", force: :cascade do |t|
    t.integer "code",        limit: 4
    t.string  "description", limit: 60
    t.string  "region_code", limit: 3
  end

  add_index "centris_cities", ["code"], name: "index_centris_cities_on_code", using: :btree

  create_table "centris_costs", force: :cascade do |t|
    t.string  "no_inscription",   limit: 14
    t.string  "tdep_code",        limit: 10
    t.integer "montant_depense",  limit: 4
    t.integer "annee",            limit: 4
    t.integer "annee_expiration", limit: 4
    t.string  "frequence",        limit: 10
    t.string  "part_depense",     limit: 10
  end

  add_index "centris_costs", ["no_inscription"], name: "index_centris_costs_on_no_inscription", using: :btree
  add_index "centris_costs", ["part_depense"], name: "index_centris_costs_on_part_depense", using: :btree
  add_index "centris_costs", ["tdep_code"], name: "index_centris_costs_on_tdep_code", using: :btree

  create_table "centris_feature_subtypes", force: :cascade do |t|
    t.string "tcar_code",                     limit: 4
    t.string "code",                          limit: 4
    t.string "description_abregee_francaise", limit: 15
    t.string "description_francaise",         limit: 60
    t.string "description_abregee_anglaise",  limit: 15
    t.string "description_anglaise",          limit: 60
  end

  add_index "centris_feature_subtypes", ["code"], name: "index_centris_feature_subtypes_on_code", using: :btree
  add_index "centris_feature_subtypes", ["tcar_code"], name: "index_centris_feature_subtypes_on_tcar_code", using: :btree

  create_table "centris_feature_types", force: :cascade do |t|
    t.string "code",                          limit: 15
    t.string "description_abregee_francaise", limit: 15
    t.string "description_francaise",         limit: 60
    t.string "description_abregee_anglaise",  limit: 15
    t.string "description_anglaise",          limit: 60
    t.string "ind_plusieurs_criteres",        limit: 1
  end

  add_index "centris_feature_types", ["code"], name: "index_centris_feature_types_on_code", using: :btree

  create_table "centris_firms", force: :cascade do |t|
    t.string "code",             limit: 12
    t.string "nom_legal",        limit: 80
    t.string "no_certificat",    limit: 10
    t.string "type_certificat",  limit: 10
    t.string "banniere_code",    limit: 10
    t.string "firme_principale", limit: 12
    t.string "courtier_code",    limit: 10
  end

  add_index "centris_firms", ["code"], name: "index_centris_firms_on_code", using: :btree

  create_table "centris_fix_values", force: :cascade do |t|
    t.string "domaine",                       limit: 70
    t.string "valeur",                        limit: 70
    t.string "description_abregee_francaise", limit: 15
    t.string "description_abregee_anglaise",  limit: 15
    t.string "description_francaise",         limit: 150
    t.string "description_anglaise",          limit: 150
  end

  add_index "centris_fix_values", ["domaine"], name: "index_centris_fix_values_on_domaine", using: :btree
  add_index "centris_fix_values", ["valeur"], name: "index_centris_fix_values_on_valeur", using: :btree

  create_table "centris_import_stats", force: :cascade do |t|
    t.integer  "members_count",    limit: 4, default: 0
    t.integer  "offices_count",    limit: 4, default: 0
    t.integer  "properties_count", limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pictures_count",   limit: 4
  end

  create_table "centris_incomes", force: :cascade do |t|
    t.string   "no_inscription",        limit: 14
    t.integer  "seq",                   limit: 4
    t.string   "type_espace",           limit: 10
    t.integer  "nb_unites_totales",     limit: 4
    t.integer  "nb_unites_vacantes",    limit: 4
    t.string   "description_francaise", limit: 60
    t.string   "description_anglaise",  limit: 60
    t.string   "champ_inutilise_1",     limit: 10
    t.integer  "superficie_totale",     limit: 8
    t.string   "um_superficie_totale",  limit: 10
    t.integer  "champ_inutilise_2",     limit: 8
    t.string   "champ_inutilise_3",     limit: 10
    t.integer  "champ_inutilise_4",     limit: 4
    t.string   "champ_inutilise_5",     limit: 10
    t.datetime "champ_inutilise_6"
    t.integer  "champ_inutilise_7",     limit: 8
    t.string   "champ_inutilise_8",     limit: 10
  end

  add_index "centris_incomes", ["no_inscription"], name: "index_centris_incomes_on_no_inscription", using: :btree
  add_index "centris_incomes", ["seq"], name: "index_centris_incomes_on_seq", using: :btree

  create_table "centris_inscriptions", force: :cascade do |t|
    t.string   "flow_name",                       limit: 255
    t.string   "no_inscription",                  limit: 14
    t.string   "champ_inutilise_1",               limit: 10
    t.string   "courtier_inscripteur_1",          limit: 10
    t.string   "bureau_inscripteur_1",            limit: 12
    t.string   "courtier_inscripteur_2",          limit: 10
    t.string   "bureau_inscripteur_2",            limit: 12
    t.integer  "prix_demande",                    limit: 8
    t.string   "um_prix_demande",                 limit: 10
    t.string   "devise_prix_demande",             limit: 10
    t.integer  "prix_location_demande",           limit: 8
    t.string   "um_prix_location_demande",        limit: 10
    t.string   "devise_prix_location_demande",    limit: 10
    t.string   "champ_inutilise_36",              limit: 1
    t.string   "code_declaration_vendeur",        limit: 10
    t.string   "ind_reprise_finance",             limit: 1
    t.string   "ind_internet",                    limit: 1
    t.string   "ind_echange_possible",            limit: 1
    t.string   "champ_inutilise_37",              limit: 10
    t.string   "champ_inutilise_3",               limit: 30
    t.string   "champ_inutilise_4",               limit: 10
    t.string   "date_mise_en_vigueur",            limit: 20
    t.string   "champ_inutilise_38",              limit: 20
    t.string   "mun_code",                        limit: 6
    t.string   "quartr_code",                     limit: 4
    t.string   "pres_de",                         limit: 60
    t.string   "no_civique_debut",                limit: 10
    t.string   "no_civique_fin",                  limit: 10
    t.string   "nom_rue_complet",                 limit: 60
    t.string   "appartement",                     limit: 8
    t.string   "code_postal",                     limit: 6
    t.string   "champ_inutilise_39",              limit: 40
    t.string   "champ_inutilise_40",              limit: 1
    t.integer  "champ_inutilise_41",              limit: 4
    t.string   "champ_inutilise_5",               limit: 60
    t.string   "champ_inutilise_6",               limit: 80
    t.string   "champ_inutilise_7",               limit: 20
    t.string   "champ_inutilise_8",               limit: 20
    t.integer  "champ_inutilise_9",               limit: 4
    t.string   "champ_inutilise_10",              limit: 60
    t.string   "champ_inutilise_11",              limit: 80
    t.string   "champ_inutilise_12",              limit: 20
    t.string   "champ_inutilise_13",              limit: 20
    t.integer  "champ_inutilise_14",              limit: 4
    t.string   "date_occupation",                 limit: 20
    t.string   "delai_occupation_francais",       limit: 15
    t.string   "delai_occupation_anglais",        limit: 15
    t.string   "champ_inutilise_42",              limit: 20
    t.string   "champ_inutilise_43",              limit: 15
    t.string   "champ_inutilise_44",              limit: 15
    t.string   "date_fin_bail",                   limit: 20
    t.string   "champ_inutilise_52",              limit: 255
    t.string   "champ_inutilise_15",              limit: 50
    t.string   "champ_inutilise_45",              limit: 10
    t.string   "categorie_propriete",             limit: 10
    t.string   "genre_propriete",                 limit: 3
    t.string   "type_batiment",                   limit: 10
    t.string   "type_copropriete",                limit: 10
    t.string   "niveau",                          limit: 10
    t.integer  "nb_etages",                       limit: 4
    t.integer  "annee_construction",              limit: 4
    t.string   "code_annee_construction",         limit: 10
    t.string   "champ_inutilise_16",              limit: 20
    t.integer  "facade_batiment",                 limit: 8
    t.integer  "profondeur_batiment",             limit: 8
    t.string   "ind_irregulier_batiment",         limit: 1
    t.string   "um_dimension_batiment",           limit: 10
    t.integer  "superficie_batiment",             limit: 8
    t.string   "um_superficie_batiment",          limit: 10
    t.integer  "superficie_habitable",            limit: 8
    t.string   "um_superficie_habitable",         limit: 10
    t.string   "champ_inutilise_17",              limit: 20
    t.integer  "facade_terrain",                  limit: 8
    t.integer  "profondeur_terrain",              limit: 8
    t.string   "ind_irregulier_terrain",          limit: 1
    t.string   "um_dimension_terrain",            limit: 10
    t.integer  "superficie_terrain",              limit: 8
    t.string   "um_superficie_terrain",           limit: 10
    t.string   "champ_inutilise_46",              limit: 24
    t.integer  "annee_evaluation",                limit: 4
    t.integer  "evaluation_municipale_terrain",   limit: 4
    t.integer  "evaluation_municipale_batiment",  limit: 4
    t.integer  "nb_pieces",                       limit: 4
    t.integer  "nb_chambres",                     limit: 4
    t.integer  "nb_chambres_sous_sol",            limit: 4
    t.integer  "nb_chambres_hors_sol",            limit: 4
    t.integer  "nb_salles_bains",                 limit: 4
    t.integer  "nb_salles_eau",                   limit: 4
    t.string   "champ_inutilise_47",              limit: 20
    t.string   "champ_inutilise_48",              limit: 15
    t.integer  "champ_inutilise_18",              limit: 4
    t.integer  "champ_inutilise_19",              limit: 4
    t.integer  "champ_inutilise_20",              limit: 4
    t.integer  "champ_inutilise_21",              limit: 4
    t.integer  "depenses_tot_exploitation",       limit: 4
    t.integer  "champ_inutilise_22",              limit: 4
    t.integer  "champ_inutilise_23",              limit: 4
    t.string   "nom_plan_eau",                    limit: 50
    t.integer  "champ_inutilise_24",              limit: 8
    t.integer  "champ_inutilise_25",              limit: 4
    t.integer  "nb_chauffe_eau_loue",             limit: 4
    t.text     "inclus_francais",                 limit: 65535
    t.text     "inclus_anglais",                  limit: 65535
    t.text     "exclus_francais",                 limit: 65535
    t.text     "exclus_anglais",                  limit: 65535
    t.integer  "nb_unites_total",                 limit: 4
    t.string   "champ_inutilise_26",              limit: 10
    t.string   "champ_inutilise_27",              limit: 1
    t.integer  "champ_inutilise_28",              limit: 4
    t.string   "champ_inutilise_29",              limit: 1
    t.string   "champ_inutilise_30",              limit: 40
    t.string   "champ_inutilise_31",              limit: 10
    t.string   "champ_inutilise_32",              limit: 10
    t.string   "champ_inutilise_49",              limit: 1
    t.datetime "date_modif"
    t.string   "frequence_prix_location",         limit: 10
    t.string   "code_statut",                     limit: 10
    t.string   "pourc_quote_part",                limit: 50
    t.string   "utilisation_commerciale",         limit: 10
    t.string   "champ_inutilise_2",               limit: 10
    t.string   "nom_du_parc",                     limit: 30
    t.string   "champ_inutilise_50",              limit: 20
    t.string   "champ_inutilise_51",              limit: 1
    t.string   "raison_sociale",                  limit: 40
    t.string   "en_oper_depuis",                  limit: 6
    t.string   "ind_franchise",                   limit: 1
    t.string   "champ_inutilise_33",              limit: 40
    t.string   "champ_inutilise_34",              limit: 10
    t.string   "champ_inutilise_35",              limit: 10
    t.string   "ind_opt_renouv_bail",             limit: 1
    t.string   "annee_mois_echeance_bail",        limit: 6
    t.string   "url_visite_virtuelle_francais",   limit: 150
    t.string   "url_visite_virtuelle_anglais",    limit: 150
    t.string   "url_desc_detaillee",              limit: 180
    t.string   "ind_taxes_prix_demande",          limit: 1
    t.string   "ind_taxes_prix_location_demande", limit: 1
    t.string   "courtier_inscripteur_3",          limit: 255
    t.string   "bureau_inscripteur_3",            limit: 255
    t.string   "courtier_inscripteur_4",          limit: 255
    t.string   "bureau_inscripteur_4",            limit: 255
    t.string   "courtier1_type_divul_interet",    limit: 255
    t.string   "courtier2_type_divul_interet",    limit: 255
    t.string   "courtier3_type_divul_interet",    limit: 255
    t.string   "courtier4_type_divul_interet",    limit: 255
    t.string   "ind_vente_sans_garantie_legale",  limit: 255
    t.string   "latitude",                        limit: 255
    t.string   "longitude",                       limit: 255
    t.string   "type_superficie_habitable",       limit: 255
    t.integer  "rev_pot_brut_res",                limit: 4
    t.integer  "rev_pot_brut_comm",               limit: 4
    t.integer  "rev_pot_brut_stat",               limit: 4
    t.integer  "rev_pot_brut_au",                 limit: 4
    t.string   "date_rev_brut_pot",               limit: 20
    t.string   "particularite_construction",      limit: 255
  end

  add_index "centris_inscriptions", ["no_inscription"], name: "index_centris_inscriptions_on_no_inscription", using: :btree

  create_table "centris_members", force: :cascade do |t|
    t.string "flow_name",           limit: 255
    t.string "code",                limit: 10
    t.string "bur_code",            limit: 12
    t.string "no_certificat",       limit: 15
    t.string "type_certificat",     limit: 10
    t.string "nom",                 limit: 40
    t.string "prenom",              limit: 40
    t.string "titre_professionnel", limit: 40
    t.string "champ_inutilise_1",   limit: 10
    t.string "telephone_1",         limit: 20
    t.string "telephone_2",         limit: 20
    t.string "telephone_fax",       limit: 20
    t.string "courriel",            limit: 150
    t.string "site_web",            limit: 150
    t.string "champ_inutilise_2",   limit: 20
    t.string "code_langue",         limit: 1
    t.string "photo_url",           limit: 255
    t.string "date_modification",   limit: 255
    t.string "nom_societe",         limit: 180
    t.string "type_societe_desc_f", limit: 180
    t.string "type_societe_desc_a", limit: 180
  end

  add_index "centris_members", ["code"], name: "index_centris_members_on_code", using: :btree

  create_table "centris_neighborhoods", force: :cascade do |t|
    t.string "mun_code",              limit: 6
    t.string "code",                  limit: 4
    t.string "description_francaise", limit: 60
    t.string "description_anglaise",  limit: 60
  end

  add_index "centris_neighborhoods", ["code"], name: "index_centris_neighborhoods_on_code", using: :btree
  add_index "centris_neighborhoods", ["mun_code"], name: "index_centris_neighborhoods_on_mun_code", using: :btree

  create_table "centris_offices", force: :cascade do |t|
    t.string  "code",            limit: 12
    t.string  "firme_code",      limit: 12
    t.string  "nom_legal",       limit: 40
    t.string  "no_civique",      limit: 10
    t.string  "nom_rue",         limit: 60
    t.string  "bureau",          limit: 10
    t.string  "municipalite",    limit: 50
    t.string  "province",        limit: 255
    t.string  "code_postal",     limit: 6
    t.string  "telephone_1",     limit: 20
    t.integer "poste_1",         limit: 4
    t.string  "telephone_2",     limit: 20
    t.integer "poste_2",         limit: 4
    t.string  "telephone_fax",   limit: 20
    t.string  "courriel",        limit: 150
    t.string  "site_web",        limit: 150
    t.string  "directeur_code",  limit: 10
    t.string  "url_logo_bureau", limit: 255
  end

  add_index "centris_offices", ["code"], name: "index_centris_offices_on_code", using: :btree

  create_table "centris_pictures", force: :cascade do |t|
    t.string  "no_inscription",         limit: 14
    t.integer "seq",                    limit: 4
    t.string  "nom_fichier_photo",      limit: 20
    t.string  "code_description_photo", limit: 10
    t.string  "description_francaise",  limit: 60
    t.string  "description_anglaise",   limit: 60
    t.string  "photourl",               limit: 255
    t.string  "no_version",             limit: 255
    t.string  "date_modification",      limit: 50
  end

  add_index "centris_pictures", ["no_inscription"], name: "index_centris_pictures_on_no_inscription", using: :btree
  add_index "centris_pictures", ["seq"], name: "index_centris_pictures_on_seq", using: :btree

  create_table "centris_piece_intergenerations", force: :cascade do |t|
    t.string  "no_inscription",          limit: 14
    t.integer "seq",                     limit: 4
    t.string  "piece_code",              limit: 255
    t.string  "dimensions",              limit: 255
    t.string  "ind_irregulier",          limit: 255
    t.string  "etage",                   limit: 255
    t.string  "couvre_plancher_code",    limit: 255
    t.string  "informations_francaises", limit: 255
    t.string  "informations_anglaises",  limit: 255
  end

  add_index "centris_piece_intergenerations", ["no_inscription"], name: "index_centris_piece_intergenerations_on_no_inscription", using: :btree

  create_table "centris_property_classifications", force: :cascade do |t|
    t.string "categorie_propriete",           limit: 10
    t.string "genre_propriete",               limit: 3
    t.string "description_abregee_francaise", limit: 15
    t.string "description_francaise",         limit: 60
    t.string "description_abregee_anglaise",  limit: 15
    t.string "description_anglaise",          limit: 60
  end

  add_index "centris_property_classifications", ["categorie_propriete"], name: "index_centris_property_classifications_on_categorie_propriete", using: :btree
  add_index "centris_property_classifications", ["genre_propriete"], name: "index_centris_property_classifications_on_genre_propriete", using: :btree

  create_table "centris_regions", force: :cascade do |t|
    t.string "code",                  limit: 3
    t.string "description_francaise", limit: 60
    t.string "description_anglaise",  limit: 60
  end

  add_index "centris_regions", ["code"], name: "index_centris_regions_on_code", using: :btree

  create_table "centris_remarks", force: :cascade do |t|
    t.string  "no_inscription",    limit: 14
    t.integer "no_remarque",       limit: 4
    t.string  "code_langue",       limit: 10
    t.integer "ordre_affichage",   limit: 4
    t.string  "champ_inutilise_1", limit: 1
    t.string  "champ_inutilise_2", limit: 1
    t.text    "texte",             limit: 65535
  end

  add_index "centris_remarks", ["no_inscription"], name: "index_centris_remarks_on_no_inscription", using: :btree
  add_index "centris_remarks", ["no_remarque"], name: "index_centris_remarks_on_no_remarque", using: :btree
  add_index "centris_remarks", ["ordre_affichage"], name: "index_centris_remarks_on_ordre_affichage", using: :btree

  create_table "centris_renovations", force: :cascade do |t|
    t.string  "no_inscription",          limit: 14
    t.integer "seq",                     limit: 4
    t.string  "renovation_type",         limit: 10
    t.integer "annee",                   limit: 4
    t.integer "champ_inutilise_1",       limit: 4
    t.string  "informations_francaises", limit: 30
    t.string  "informations_anglaises",  limit: 30
  end

  add_index "centris_renovations", ["no_inscription"], name: "index_centris_renovations_on_no_inscription", using: :btree
  add_index "centris_renovations", ["seq"], name: "index_centris_renovations_on_seq", using: :btree

  create_table "centris_rooms", force: :cascade do |t|
    t.string  "no_inscription",          limit: 14
    t.integer "seq",                     limit: 4
    t.integer "ordre_affichage",         limit: 4
    t.string  "piece_code",              limit: 10
    t.integer "champ_inutilise_1",       limit: 8
    t.integer "champ_inutilise_2",       limit: 8
    t.string  "dimensions",              limit: 15
    t.string  "ind_irregulier",          limit: 1
    t.string  "etage",                   limit: 10
    t.string  "couvre_plancher_code",    limit: 10
    t.string  "informations_francaises", limit: 30
    t.string  "informations_anglaises",  limit: 30
    t.string  "champ_inutilise_3",       limit: 1
  end

  add_index "centris_rooms", ["no_inscription"], name: "index_centris_rooms_on_no_inscription", using: :btree
  add_index "centris_rooms", ["seq"], name: "index_centris_rooms_on_seq", using: :btree

  create_table "centris_specifications", force: :cascade do |t|
    t.string  "no_inscription",          limit: 14
    t.string  "tcar_code",               limit: 10
    t.string  "scarac_code",             limit: 4
    t.integer "nombre",                  limit: 4
    t.string  "informations_francaises", limit: 60
    t.string  "informations_anglaises",  limit: 60
  end

  add_index "centris_specifications", ["no_inscription"], name: "index_centris_specifications_on_no_inscription", using: :btree
  add_index "centris_specifications", ["scarac_code"], name: "index_centris_specifications_on_scarac_code", using: :btree
  add_index "centris_specifications", ["tcar_code"], name: "index_centris_specifications_on_tcar_code", using: :btree

  create_table "centris_visites_libres", force: :cascade do |t|
    t.string  "no_inscription", limit: 255
    t.integer "seq",            limit: 4
    t.string  "date_debut",     limit: 255
    t.string  "date_fin",       limit: 255
    t.string  "heure_debut",    limit: 255
    t.string  "heure_fin",      limit: 255
    t.string  "commentaires_f", limit: 255
    t.string  "commentaires_a", limit: 255
  end

  add_index "centris_visites_libres", ["no_inscription"], name: "index_centris_visites_libres_on_no_inscription", using: :btree

  create_table "dezrez_assets", force: :cascade do |t|
    t.integer  "dezrez_id",  limit: 8
    t.integer  "uid",        limit: 4
    t.string   "caption",    limit: 255
    t.string   "url",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dezrez_assets", ["dezrez_id"], name: "index_dezrez_assets_on_dezrez_id", using: :btree

  create_table "dezrezs", force: :cascade do |t|
    t.integer  "uid",                    limit: 4
    t.string   "visibility",             limit: 255
    t.integer  "eaid",                   limit: 4
    t.integer  "bid",                    limit: 4
    t.integer  "sale",                   limit: 4
    t.integer  "category",               limit: 4
    t.string   "price",                  limit: 255
    t.float    "priceVal",               limit: 24
    t.integer  "poa",                    limit: 4
    t.string   "currency",               limit: 255
    t.integer  "rentalperiod",           limit: 4
    t.integer  "sold",                   limit: 4
    t.string   "statusFlag",             limit: 255
    t.integer  "bedrooms",               limit: 4
    t.integer  "bathrooms",              limit: 4
    t.integer  "receptions",             limit: 4
    t.integer  "garages",                limit: 4
    t.integer  "gardens",                limit: 4
    t.integer  "parkingSpaces",          limit: 4
    t.integer  "otherrooms",             limit: 4
    t.integer  "specials",               limit: 4
    t.integer  "leaseType",              limit: 4
    t.integer  "metropix",               limit: 4
    t.integer  "propertyType",           limit: 4
    t.string   "period",                 limit: 255
    t.integer  "branchRef",              limit: 4
    t.integer  "portalID",               limit: 4
    t.float    "latitude",               limit: 24
    t.float    "longitude",              limit: 24
    t.float    "yaw",                    limit: 24
    t.float    "pitch",                  limit: 24
    t.float    "zoom",                   limit: 24
    t.integer  "smallPicWidth",          limit: 4
    t.integer  "largePicWidth",          limit: 4
    t.string   "contact_name",           limit: 255
    t.string   "contact_tel",            limit: 255
    t.string   "contact_email",          limit: 255
    t.string   "contact_address",        limit: 255
    t.string   "contact_city",           limit: 255
    t.string   "contact_county",         limit: 255
    t.string   "contact_country",        limit: 255
    t.string   "contact_zipcode",        limit: 255
    t.string   "property_street_number", limit: 255
    t.string   "property_street_name",   limit: 255
    t.string   "property_town",          limit: 255
    t.string   "property_city",          limit: 255
    t.string   "property_county",        limit: 255
    t.string   "property_country",       limit: 255
    t.string   "property_zipcode",       limit: 255
    t.string   "property_useAddress",    limit: 255
    t.text     "description",            limit: 65535
    t.string   "charges",                limit: 255
    t.string   "pricetext",              limit: 255
    t.text     "areas",                  limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flags", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "import_users", force: :cascade do |t|
    t.integer "status",          limit: 4,   default: 1
    t.string  "flow_name",       limit: 255
    t.string  "import_provider", limit: 255
    t.string  "ftp_user",        limit: 255
    t.string  "ftp_password",    limit: 255
    t.string  "ftp_domain",      limit: 255
  end

  create_table "kijiji_locations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "louercom_locations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "region_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "magex_accounts", force: :cascade do |t|
    t.integer  "uid",          limit: 4
    t.integer  "shareimmo_id", limit: 4,   default: 0
    t.string   "country_id",   limit: 255
    t.string   "language_id",  limit: 255
    t.string   "company_name", limit: 255
    t.string   "web_site",     limit: 255
    t.integer  "is_active",    limit: 4,   default: 1
    t.string   "language",     limit: 255
    t.string   "first_name",   limit: 255
    t.string   "last_name",    limit: 255
    t.string   "email",        limit: 255
    t.string   "phone",        limit: 255
    t.date     "published_at"
    t.string   "secure_token", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "magex_import_stats", force: :cascade do |t|
    t.integer  "accounts_count",   limit: 4, default: 0
    t.integer  "properties_count", limit: 4, default: 0
    t.integer  "units_count",      limit: 4, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "magex_properties", force: :cascade do |t|
    t.integer  "uid",                 limit: 8
    t.integer  "shareimmo_id",        limit: 4,   default: 0
    t.integer  "magex_account_id",    limit: 8
    t.integer  "is_active",           limit: 4,   default: 1
    t.string   "identification",      limit: 255
    t.string   "address",             limit: 255
    t.string   "city",                limit: 255
    t.string   "year_of_built",       limit: 255
    t.integer  "number_of_units",     limit: 4
    t.date     "ready_for_rent_date"
    t.string   "purchase_price",      limit: 255
    t.string   "building_evaluation", limit: 255
    t.integer  "building_area",       limit: 4
    t.string   "building_area_type",  limit: 255
    t.string   "land_area",           limit: 255
    t.string   "land_area_type",      limit: 255
    t.integer  "taxes_municipal",     limit: 4
    t.integer  "taxes_school",        limit: 4
    t.integer  "taxes_water",         limit: 4
    t.integer  "taxes_other",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "magex_properties", ["magex_account_id"], name: "index_magex_properties_on_magex_account_id", using: :btree

  create_table "magex_units", force: :cascade do |t|
    t.integer  "uid",                           limit: 8
    t.integer  "shareimmo_id",                  limit: 4,     default: 0
    t.integer  "magex_account_id",              limit: 8
    t.integer  "magex_property_id",             limit: 8
    t.integer  "is_active",                     limit: 4,     default: 0
    t.integer  "is_vacant",                     limit: 4,     default: 0
    t.string   "identification",                limit: 255
    t.integer  "property_classification_id",    limit: 8
    t.string   "unit_type",                     limit: 255
    t.string   "address",                       limit: 255
    t.string   "city",                          limit: 255
    t.text     "notes",                         limit: 65535
    t.string   "number_of_rooms",               limit: 255
    t.integer  "number_of_bedrooms",            limit: 4
    t.integer  "area",                          limit: 4
    t.string   "area_type",                     limit: 255
    t.string   "commercial",                    limit: 255
    t.string   "service_stove",                 limit: 255
    t.string   "service_stove_detail",          limit: 255
    t.string   "service_washer_dryer",          limit: 255
    t.string   "service_washer_dryer_detail",   limit: 255
    t.string   "service_furnished",             limit: 255
    t.string   "service_furnished_detail",      limit: 255
    t.string   "service_semi_furnished",        limit: 255
    t.string   "service_semi_furnished_detail", limit: 255
    t.string   "service_heating",               limit: 255
    t.string   "service_heating_detail",        limit: 255
    t.string   "service_electricity",           limit: 255
    t.string   "service_electricity_detail",    limit: 255
    t.string   "service_hot_water",             limit: 255
    t.string   "service_hot_water_detail",      limit: 255
    t.string   "lease_next_payment_interval",   limit: 255
    t.integer  "lease_next_duration",           limit: 4
    t.string   "lease_next_duration_frequency", limit: 255
    t.integer  "lease_next_dwelling_price",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "magex_units", ["magex_account_id"], name: "index_magex_units_on_magex_account_id", using: :btree
  add_index "magex_units", ["magex_property_id"], name: "index_magex_units_on_magex_property_id", using: :btree
  add_index "magex_units", ["property_classification_id"], name: "index_magex_units_on_property_classification_id", using: :btree

  create_table "migrations", force: :cascade do |t|
    t.string   "AddWebsiteToMyBrokers", limit: 255
    t.string   "website",               limit: 255
    t.integer  "website_techno_id",     limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "migrations", ["website_techno_id"], name: "index_migrations_on_website_techno_id", using: :btree

  create_table "my_brokers", force: :cascade do |t|
    t.string   "first_name",        limit: 255
    t.string   "last_name",         limit: 255
    t.string   "phone",             limit: 255
    t.string   "phone2",            limit: 255
    t.string   "email",             limit: 255
    t.string   "agency_name",       limit: 255
    t.string   "street_number",     limit: 255
    t.string   "street_name",       limit: 255
    t.string   "city",              limit: 255
    t.string   "province",          limit: 255
    t.string   "zipcode",           limit: 255
    t.integer  "flag_id",           limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "note",              limit: 65535
    t.datetime "next_step_date"
    t.integer  "next_step_id",      limit: 4
    t.string   "website",           limit: 255
    t.integer  "website_techno_id", limit: 4
    t.integer  "phoner_id",         limit: 4
  end

  add_index "my_brokers", ["flag_id"], name: "index_my_brokers_on_flag_id", using: :btree
  add_index "my_brokers", ["next_step_id"], name: "index_my_brokers_on_next_step_id", using: :btree
  add_index "my_brokers", ["website_techno_id"], name: "index_my_brokers_on_website_techno_id", using: :btree

  create_table "property_classification_translations", force: :cascade do |t|
    t.integer  "property_classification_id", limit: 4,   null: false
    t.string   "locale",                     limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                       limit: 255
  end

  add_index "property_classification_translations", ["locale"], name: "index_property_classification_translations_on_locale", using: :btree
  add_index "property_classification_translations", ["property_classification_id"], name: "index_fd9c8b5085a4691fa1d1ba1ff9acb0abc44ff800", using: :btree

  create_table "property_classifications", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.integer  "shareimmo_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "property_classifications", ["shareimmo_id"], name: "index_property_classifications_on_shareimmo_id", using: :btree

  create_table "property_classifiers", force: :cascade do |t|
    t.string   "name",                       limit: 255
    t.string   "provider",                   limit: 255
    t.integer  "property_classification_id", limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "property_classifiers", ["property_classification_id"], name: "index_property_classifiers_on_property_classification_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 8
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "wemoovs", force: :cascade do |t|
    t.integer  "uid",                 limit: 8
    t.string   "youtube_id",          limit: 255
    t.string   "uploaded_by",         limit: 255
    t.string   "link",                limit: 255
    t.string   "title",               limit: 255
    t.string   "environment",         limit: 255
    t.string   "backlink",            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "youtube_description", limit: 65535
  end

  create_table "zipcodes", force: :cascade do |t|
    t.string   "zipcode",                 limit: 255
    t.string   "best_sector",             limit: 255
    t.string   "country",                 limit: 255
    t.string   "province",                limit: 255
    t.integer  "kijiji_location_id",      limit: 8
    t.integer  "louercom_location_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "centris_city_id",         limit: 4
    t.integer  "centris_neighborhood_id", limit: 4
  end

  add_index "zipcodes", ["centris_city_id"], name: "index_zipcodes_on_centris_city_id", using: :btree
  add_index "zipcodes", ["centris_neighborhood_id"], name: "index_zipcodes_on_centris_neighborhood_id", using: :btree
  add_index "zipcodes", ["kijiji_location_id"], name: "index_zipcodes_on_kijiji_location_id", using: :btree

end
