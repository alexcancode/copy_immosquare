require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

Bundler.require(*Rails.groups)

module ShareimmoApi
  class Application < Rails::Application

    config.time_zone = 'Eastern Time (US & Canada)'

    ##============================================================##
    ## The default locale is :en and all translations
    ## from config/locales/*.rb,yml are auto loaded.
    ##============================================================##
    config.i18n.default_locale = 'fr'
    config.i18n.fallbacks = true


    # alternatively, you can preconfigure the client like so
    Twilio.configure do |config|
      config.account_sid = ENV['twilio_sid']
      config.auth_token = ENV['twilio_token']
    end



    ##============================================================##
    ## Config action_mailer
    ## ====================
    ##============================================================##
    config.action_mailer.default_url_options = { :host => ENV["host"] }
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :address        => "smtp-relay.sendinblue.com",
      :port           => 587,
      :authentication => :plain,
      :user_name      => ENV["SENDINBLUE_USERNAME"],
      :password       => ENV["SENDINBLUE_KEY"]
    }
  end
end
