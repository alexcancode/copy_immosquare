set :application, 'shareimmo_api'
set :rvm_ruby_version, "2.3.3@shareimmo_api"
set :repo_url, 'git@bitbucket.org:wgroupe/shareimmo_api.git'
set :bundle_roles, :all
set :deploy_to, "/srv/apps/shareimmo_api"
set :linked_dirs, %w{log tmp/pids tmp/sockets tmp/pdfs}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :keep_assets, 2
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

