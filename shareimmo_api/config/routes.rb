 require 'sidekiq/web'

 Rails.application.routes.draw do

  ##============================================================##
  ## API
  ##============================================================##
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      scope "centris" do
        match "brokers"  => "centris#brokers", :via => :post
      end
      scope "pagesimmo" do
        match "sync/:client_id"  => "pagesimmo#sync", :via => :get
      end
      scope "zipcodes" do
        match 'kijiji'            => 'zipcodes#kijiji_export', :via => :get
        match 'louer'             => 'zipcodes#louercom_export', :via => :get
        match 'centris/:id'       => 'zipcodes#centris_zipcode', :via => :get
      end
    end
  end



  root 'welcome#home'
  match 'dashboard' => 'welcome#dashboard', :via => [:get]

  ##============================================================##
  ##
  ##============================================================##
  match 'rake/poliris'  => 'rake#run_poliris', :via => [:get]


  ##============================================================##
  ## OAuth
  ##============================================================##
  get '/auth/:provider/callback', to: 'authentifications#create'
  get '/auth/failure', to: 'authentifications#fail'


  ##============================================================##
  ## Cette methode autorise l'url de sideKiq que pour les
  ## adminisatreur du site
  ##============================================================##
  authenticate :user, lambda { |u| u.check_role? } do
    mount Sidekiq::Web => '/sidekiq'
  end


  ##============================================================##
  ## Centris
  ##============================================================##
  match 'dashboard/centris'                 => 'centris#ftp', :via => [:get]
  match 'dashboard/centris/banners'         => 'centris#banners', :via => [:get]
  match 'dashboard/centris/firms'           => 'centris#firms', :via => [:get]
  match 'dashboard/centris/offices'         => 'centris#offices', :via => [:get]
  match 'dashboard/centris/brokers'         => 'centris#brokers', :via => [:get]
  match 'dashboard/centris/inscriptions'    => 'centris#inscriptions', :via => [:get]
  match 'dashboard/centris/neighborhoods'   => 'centris#neighborhoods', :via => [:get]
  match 'dashboard/centris/cities'          => 'centris#cities', :via => [:get]
  match 'dashboard/centris/regions'         => 'centris#regions', :via => [:get]

  match 'dashboard/centris/offices/sync'    => 'centris#offices_sync', :via => [:get]
  match 'dashboard/centris/brokers/sync'    => 'centris#brokers_sync', :via => [:get]

  match 'dashboard/zipcodes'                => 'zipcodes#index', :via => [:get], :as => 'dashboard_zipcode'
  match 'dashboard/zipcode/:id'             => 'zipcodes#edit', :via => [:get], :as => 'dashboard_zipcode_edit'
  match 'dashboard/zipcode/:id/debug'       => 'zipcodes#debbuger', :via => [:get], :as => 'dashboard_debug_zipcode'


  resources :zipcodes, :only =>[:update]



  match 'dashboard/youtube'                => 'youtube#index', :via => [:get], :as => 'dashboard_youtube'

  resources :my_brokers





  ##============================================================##
  ## Magex
  ##============================================================##
  match 'dashboard/magex'                  => 'magex#ftp', :via => [:get]
  match 'dashboard/magex/accounts'         => 'magex#accounts', :via => [:get]
  match 'dashboard/magex/buildings'        => 'magex#buildings', :via => [:get]
  match 'dashboard/magex/units'            => 'magex#units', :via => [:get]
  match 'dashboard/magex/ftp/sync'         => 'magex#ftp_sync',via: [:get]




  devise_for :users, :skip => [:sessions], :controllers => { :registrations => :registrations }
  as :user do
    get 'signin' => 'devise/sessions#new', :as => :new_user_session
    post 'signin' => 'devise/sessions#create', :as => :user_session
    delete 'signout' => 'devise/sessions#destroy', :as => :destroy_user_session
  end


end
