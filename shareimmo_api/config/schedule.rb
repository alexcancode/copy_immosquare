every 1.day, :at => '00:01 am' do
  rake "gercop:sync"
end

every 1.day, :at => '01:00 am' do
  rake "pagesimmo:import"
end

every 1.day, :at => '01:30 am' do
  rake "emulis:import"
  rake "poliris:import"
end

every 1.day, :at => '02:30 am' do
  rake "evosys:import"
end

every 1.day, :at => '06:30 am' do
  rake "magex:import"
end

every 1.day, :at => '07:00 am' do
  rake "poliris:import"
end

every 1.day, :at => '07:00 pm' do
  rake "centris:import"
end

every 1.day, :at => '08:00 pm' do
  rake "prestige:import"
end

every 1.day, :at => '08:30 pm' do
  rake "centris:export_users"
end

every 1.day, :at => '10:00 pm' do
  rake "centris:export_properties"
end

