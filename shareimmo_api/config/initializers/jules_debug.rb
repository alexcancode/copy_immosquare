log_path = File.join(Rails.root, 'log','jules_debug.log')
log_file = File.open(log_path, 'a')
log_file.sync = true
JulesLogger = Logger.new(log_file)

if Rails.env.development?
  MAX_LOG_SIZE = 20.megabytes
  logs = File.join(Rails.root,'log','*.log')
  if Dir[logs].any? {|log| File.size?(log).to_i > MAX_LOG_SIZE }
    $stdout.puts "Runing rake log:clear"
    'rake log:clear'
  end
end
