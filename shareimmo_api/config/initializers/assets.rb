# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(filterrific/filterrific-spinner.gif)



##============================================================##
## Fix Precompile
##============================================================##
Rails.application.config.assets.configure do |env|
  env.context_class.class_eval do
    def asset_path(path, options = {})
      "/assets/#{path}"
    end
  end
end
