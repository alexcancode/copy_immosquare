if Rails.env.production?
  Rails.application.config.session_store :cookie_store, key: 'shareimmo_api_session',:domain => :all
else
  Rails.application.config.session_store :cookie_store, :key => 'shareimmo_api_session', :domain => "lvh.me"
end
