require 'net/ftp'

namespace :immotop do

  task to_shareimmo: :environment do
    FileUtils.rm_rf(Dir.glob(File.join(Rails.root,'import','immotop','*')))
    folder_destination = File.join(Rails.root,'import','immotop',"#{Time.now.strftime("%y-%m-%d")}")
    FileUtils.mkdir_p(folder_destination)
    account = ImportUser.where(:status=>1,:import_provider=>'immotop').first
    #============================================================##
    # Connexion au FTP -> on récupère tout les comptes actifs
    # -> puis on les transferts vers notre dossier temporaire
    #============================================================##
    Net::FTP.open(account.ftp_domain,account.ftp_user,account.ftp_password) do |ftp|
      files = ftp.nlst("*.xml")
      unless files.blank?
        ftp.getbinaryfile(files.last,File.join(folder_destination, File.basename(files.last)))
      end
      ftp.close
    end

    Dir.foreach(folder_destination) do |file|
      next if file == '.' or file == '..'
      @user_id = file.split('.xml').map {|x| x[/\d+/]}.first
      f = File.open(File.join(folder_destination,file))
      @xml = Nokogiri::XML(f,nil, 'UTF-8')
      f.close
    end


    properties = Array.new
    @xml.css("RECORD").each_with_index do |property,n|
      pictures = Array.new

      properties << {
        :uid=> property.css('ID').first.content,
        :uid2 => nil,
        :accountUid => @user_id,
        :status => property.css('Status').first.content == "active" ? 1 : 0 ,
        :mls =>nil,
        :geo => {
          :geocoding =>{
            :address => property.css('address').first.content,
            :latitude =>  property.css('latitude').first.content.to_f,
            :longitude => property.css('longitude').first.content.to_f,
          },
          :addressVisibility  =>  1,
          :streetNumber       =>  nil,
          :streetName         =>  nil,
          :subLocality        =>  nil,
          :locality           =>  property.css('city').first.content,
          :zipcode            =>  nil,
          :province           =>  property.css('region1').first.content,
          :municipality       =>  property.css('region2').first.content,
          :country            =>  property.css('country').first.content,
          :cadastre => nil,
          :unit=> nil,
          :level => property.css('floor').first.content,
          :building => {
            :uid => nil,
            :name => nil,
            :NumberOfLevels => property.css('floors').first.content,
            :NumberOfUnits => nil,
            :streetNumber => nil,
            :streetName => nil,
            :zipcode => nil,
            :city =>nil,
            :subLocality => nil,
            :province => nil,
            :country => nil,
            :description => nil,
            :pictures => property.css('photos').first.content.split(',').each{ |p| pictures << {:url =>"http://static.immotop.lu/files/#{@user_id}/x/#{p}"}}
          }
        },
        :title =>{
          :fr => nil
        },
        :description =>{
          :fr => property.css("description_fr").first.content,
          :de => property.css("description_de").first.content,
          :pt => property.css("description_pt").first.content,
          :en => property.css("description_en").first.content,
        },
        :url =>{
          :fr => nil,
          :en => nil
        },
        "area" =>{
          :living => property.css("surface").first.content,
          :land  => property.css("surface_of_the_ground").first.content,
          :balcony => property.css("balcony_surface").first.content,
          :unit => "mc" ,
        },
        :rooms =>{
          :rooms=> property.css("rooms").first.content,
          :withHalf => nil,
          :bedrooms=> nil,
          :bathrooms=> property.css("bathrooms").first.content,
          :showerrooms => check_value(property.css('comfort').first.content,5),
        },
        :financial =>{
          :price => property.css("Type").first.content == "sale" ? property.css("price").first.content : nil ,
          :price_from => nil,
          :availableDate => nil,
          :rentInfos =>{
            :rent=> property.css("Type").first.content == "rent" ? property.css("price").first.content : nil ,
            :fees => property.css("charges").first.content,
            :frequency => 30
          },
          :annual =>{
            :tax_1 => nil,
            :tax_2 => nil,
            :income => nil
          },
          :currency => "eur",
        },
        :infos =>{
          :propertyListing => property.css("Type").first.content == "sale" ? 1 : 2,
          :propertyClassification=> property.css("Kind_ID").first.content,
          :propertyStatus => 1,
          :propertyFlag => property.css("Kind_ID").first.content == 1 ? 1 : nil,
          :yearOfBuild => property.css('year').first.content,
        },
        :pictures => pictures,
        :videos => nil,
        :inclusions => {
          :airConditioning => check_value(property.css('equipment').first.content,6),
          :hotWater => nil,
          :heated => nil,
          :electricity => nil,
          :furnished => check_value(property.css('comfort').first.content,3),
          :fridge => nil,
          :cooker => nil,
          :dishwasher => check_value(property.css('comfort').first.content,8),
          :dryer => nil,
          :washer => nil,
          :microwave=> nil
        },
        :partiallyFurnished => {
          :livingRoom => nil,
          :rooms => nil,
          :kitchen => check_value(property.css('kitchen').first.content,3),
          :other => 1,
        },
        :composition => {
          :bar => check_value(property.css('kitchen').first.content,6),
          :living => check_value(property.css('inside').first.content,2),
          :dining => check_value(property.css('inside').first.content,1),
          :separateToilet => check_value(property.css('comfort').first.content,6),
          :openKitchen => check_value(property.css('kitchen').first.content,2)
        },
        :heatingSystems => {
          :indexEnergy => property.css('energy_pass').first.content,
          :indexInsulation => property.css('isolation').first.content,
          :electric => check_value(property.css('heating').first.content,3),
          :solar => check_value(property.css('equipment').first.content,7),
          :gaz => check_value(property.css('heating').first.content,1),
          :condensation => check_value(property.css('heating').first.content,4),
          :fuel => check_value(property.css('heating').first.content,2),
          :heatPump => check_value(property.css('heating').first.content,6),
          :floorHeating => check_value(property.css('heating').first.content,5)
        },
        :details => {
          :elevator => check_value(property.css('comfort').first.content,1),
          :laundry => check_value(property.css('inside').first.content,5),
          :garbageChute => nil,
          :commonSpace => nil,
          :janitor => nil,
          :gym => nil,
          :sauna => check_value(property.css('comfort').first.content,7),
          :spa => check_value(property.css('comfort').first.content,2),
          :insidePool => check_value(property.css('outside').first.content,9),
          :outsidePool => check_value(property.css('outside').first.content,9),
          :insideParking => check_value(property.css('outside').first.content,3),
          :outsideParking => check_value(property.css('outside').first.content,2),
          :parkingOnStreet => check_value(property.css('outside').first.content,5),
          :garageBox => check_value(property.css('outside').first.content,4)
        },
        :indoorFeatures => {
          :attic => check_value(property.css('inside').first.content,8),
          :cellar => check_value(property.css('inside').first.content,6),
          :centralVacuum => nil,
          :entriesWasherDryer => nil,
          :entriesDishwasher =>nil,
          :fireplace => check_value(property.css('inside').first.content,4),
          :storage => check_value(property.css('inside').first.content,3),
          :walkIn => nil,
          :noSmoking => nil,
          :doubleGlazing => check_value(property.css('inside').first.content,9),
          :tripleGlazing => check_value(property.css('inside').first.content,10)
        },
        :flooring => {
          :carpet => check_value(property.css('flooring').first.content,2),
          :wood => nil,
          :floating => nil,
          :ceramic => check_value(property.css('flooring').first.content,6),
          :parquet => check_value(property.css('flooring').first.content,5),
          :cushion => nil,
          :vinyle => check_value(property.css('flooring').first.content,3),
          :lino => nil,
          :marble => check_value(property.css('flooring').first.content,1)
        },
        :outdoorFeatures => {
          :landAccess => nil,
          :backBalcony => check_value(property.css('outside').first.content,1),
          :frontBalcony => check_value(property.css('outside').first.content,1),
          :privatePatio => nil,
          :storage => nil,
          :terrace => check_value(property.css('outside').first.content,7),
          :veranda => check_value(property.css('outside').first.content,8),
          :garden => check_value(property.css('outside').first.content,6)
        },
        :accessibility => {
          :elevator => nil,
          :balcony => nil,
          :grabBar => nil,
          :widerCorridors => check_value(property.css('other').first.content,9),
          :loweredSwitches => nil,
          :ramp => nil
        },
        :senior => {
          :autonomy => nil,
          :halfAutonomy => nil,
          :noAutonomy => nil,
          :meal => nil,
          :nursery => nil,
          :domesticHelp => nil,
          :community => nil,
          :activities => nil,
          :validatedResidence => nil,
          :housingCooperative => nil
        },
        :pets => {
          :allow => check_value(property.css('other').first.content,6),
          :notAllow => nil,
          :cat => nil,
          :dog => nil,
          :littleDog => nil,
          :cageAquarium => nil
        },
        :transport => {
          :bicyclePath => nil,
          # :publicTransport => check_value(property.css('location').first.content,4),
          :publicBike => nil,
          :subway => nil,
          :trainStation => nil
        },
        :security => {
          :guardianJanitor => nil,
          :alarm => check_value(property.css('security').first.content,1),
          :interCom => check_value(property.css('security').first.content,2),
          :camera => check_value(property.css('security').first.content,4),
          :smokeDectector => check_value(property.css('safety').first.content,2)
        },
        :services => {
          :internet => check_value(property.css('equipment').first.content,5),
          :tv => nil,
          :tvSat => check_value(property.css('outside').first.content,10),
          :phone => nil
        },
        :visibility => {
          :start => Time.now,
          :until => Time.now+1.month,
          :international => nil,
          :youtube => nil,
          :canada => {
            :louer_com => nil,
            :craigslist => nil,
            :lespac => nil
          },
          :luxembourg => {
            :athome => 1,
            :immotop => 1,
            :wortimmo => 1,
            :habiter_lu => 1,
            :isquare => 1
          }
        }
      }
    end

    puts properties

    HTTParty.post("#{ENV['api_base']}/properties",{
      :body => {
        :credentials => {
          :apiKey => 'dd841945-ff63-42e6-ad2f-831cec3588a3',
          :apiToken => '8228a835-b08b-4c3b-b8f2-49ed3885b0cc'
        },
        :properties => properties
      }}
    )


      api_provider  = ApiProvider.where(:name=>"immotop").select(:id).first
      ApiMonitoring.create(:api_provider_id=>api_provider.id,:ads_count=>properties.size)

      puts "Sync OK => #{Time.now}"

    end


    def check_value(field1,field2)
      field1.split(",").map(&:to_i).include?(field2.to_i) ? 1 : 0
    end



  end
