require 'csv'
require 'json'


namespace :my_brokers do

  task :import => :environment do
    csv_folder = File.join(Rails.root,'import','my_brokers')
    csv_file = File.join(csv_folder,"liste-courtiers.csv")

    if File.exist?(csv_file)
      MyBroker.destroy_all
      puts "start import"
      CSV.foreach(csv_file,:headers => true,:col_sep=>";") do |row|
        broker = MyBroker.new(
          :first_name => row["first_name"],
          :last_name => row["last_name"],
          :phone => row['phone'],
          :phone2 => row['phone2'],
          :email => row['email'],
          :agency_name => row['agency_name'],
          :street_number => row['street_number'],
          :street_name => row['street_name'],
          :city => row['city'],
          :province => row['province'],
          :zipcode => row['zipcode'],
        )
        broker.save
      end
    end
  end


end
