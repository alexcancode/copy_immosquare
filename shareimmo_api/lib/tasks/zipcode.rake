namespace :zipcode do
  task import: :environment do
    File.open(File.join(Rails.root,'datas',"zipcodes.txt"),:encoding => 'utf-8') do |txt|
      txt.each do |line|
        puts line
        Zipcode.where(:zipcode=>line.squish,:country=>"CA",:province=>"QC").first_or_create
      end
    end
  end
end
