# encoding: utf-8

require 'net/ftp'
require 'rubygems'
require 'zip'
require 'csv'

namespace :pagesimmo do

  ##============================================================##
  ##
  ##============================================================##
  task :get_serverslist => :environment do
    puts "pagesimmo:get_serverlist - started"

    _auth_Login = "00Q03717"
    _auth_Pwd   = "shareimmo_00Q03717"

    puts ENV['pagesimmo_soap_host']
    client = Savon.client(wsdl: ENV['pagesimmo_soap_host'])

    response_authenticate = client.call(:authentification, message: { "_agence_Login" => ENV["pagesimmo_main_account_login"], "_agence_Pwd" => ENV["pagesimmo_main_account_password"], "_editeur_Key" => ENV["pagesimmo_soap_editor_key"] } )
    if response_authenticate.body[:authentification_response][:authentification_result].present?
      auth_Token = response_authenticate.body[:authentification_response][:authentification_result]
      puts response_authenticate.body[:authentification_response][:authentification_result]
      response_serverlists = client.call(:get_liste_serveurs, message: { "_auth_Token" => auth_Token , "_auth_Login" => ENV["pagesimmo_main_account_login"] })
      puts response_serverlists.body
      puts response_serverlists.body[:get_liste_serveurs_response][:get_ListeServeurs]
    end

    puts "pagesimmo:get_serverlist - ended"
  end


  ##============================================================##
  ##
  ##============================================================##
  task :import => :environment do
    puts "pagesimmo:import - started"
    folder_destination  = "#{Rails.root}/import/pagesimmo/#{Time.now.strftime("%y-%m-%d")}"
    # FileUtils.rm_rf(Dir.glob( "#{Rails.root}/import/pagesimmo/*"))
    # FileUtils.mkdir_p(folder_destination)


    ImportUser.where(:status=>1,:import_provider=>'crypto').each do |account|

      account_folder  = "#{folder_destination}/#{account.ftp_user}"
      zip_destination = "#{folder_destination}/#{account.ftp_user}.zip"

      # ##============================================================##
      # ## Download
      # ##============================================================##
      # Net::FTP.open(account.ftp_domain,account.ftp_user,account.ftp_password) do |ftp|
      #   files = ftp.nlst("*.zip")
      #   unless files.blank?
      #     FileUtils.mkdir_p(account_folder) unless File.directory?(account_folder)
      #     ftp.getbinaryfile(files.last,zip_destination)
      #     Zip::File.open(zip_destination) do |zip_file|
      #       zip_file.each do |entry|
      #         entry.extract("#{account_folder}/#{entry.name}")
      #       end
      #     end
      #     File.delete(zip_destination)
      #   end
      # end


      ##============================================================##
      ## Parse each .CrypML
      ##============================================================##
      Dir.glob("#{account_folder}/*.CryptML").each do |file|
        f  = File.open(file){ |f| Nokogiri::XML(f)}
        import_agency(Hash.from_xml(f.to_s).deep_symbolize_keys,account_folder)
      end

    end
    puts "pagesimmo:import - ended"
  end



  def import_agency(hash,folder_destination)
    api_errors = Array.new
    pictures_path = "public/tmp/pictures"
    pictures_url  = "#{ENV["host"]}/tmp/pictures"
    apiKeys       = {"ApiKey" => ENV['pagesimmo_api_key'],"ApiToken" => ENV['pagesimmo_api_token']}
    agency        = hash[:ROOT][:DESTINATAIRE][:AGENCE]
    account_uid   = agency[:REFERENCE]

    ##============================================================##
    ## Check if account already exists
    ##============================================================##
    httparty = HTTParty.get("#{ENV['api_shareimmo_base']}/accounts/#{account_uid}",{:headers => apiKeys})
    if httparty.code != 200
      body = {
        :uid      => account_uid,
        :email    => agency[:REFDISTANTE],
        :password => SecureRandom.urlsafe_base64
      }
      httparty  = HTTParty.post("#{ENV['api_shareimmo_base']}/accounts",{:headers => apiKeys,:body => body})
      raise "Error on Account Creation" if httparty.code != 200
    end

    ##============================================================##
    ## Update Account
    ##============================================================##
    country = nil
    if agency.dig(:COORDONNEES,:CP) && agency.dig(:COORDONNEES,:ADRESSE1)
      address = Geocoder.search("#{agency[:COORDONNEES][:ADRESSE1]}, #{agency[:COORDONNEES][:CP]}").first
      country = ISO3166::Country.find_country_by_name(address.country).alpha2 if address.present?
    end


    body                    = {}
    body[:country]          = country
    body[:public_email]     = agency[:COORDONNEES][:EMAIL]                if agency.dig(:COORDONNEES,:EMAIL)
    body[:first_name]       = nil
    body[:last_name]        = nil
    body[:phone]            = agency[:COORDONNEES][:TELEPHONE_BUREAU]     if agency.dig(:COORDONNEES,:TELEPHONE_BUREAU)
    body[:phone_sms]        = nil
    body[:phone_visibility] = nil
    body[:website]          = nil
    body[:spoken_languages] = nil
    body[:picture_url]      = nil
    body[:facebook_url]     = nil
    body[:twitter_url]      = nil
    body[:linkedin_url]     = nil
    body[:pinterest_url]    = nil
    body[:google_plus_url]  = nil
    body[:instagram_url]    = nil
    body[:company_name]     = agency[:COORDONNEES][:RAISON_SOCIALE] if agency.dig(:COORDONNEES,:RAISON_SOCIALE)
    body[:company_address]  = agency[:COORDONNEES][:ADRESSE1]       if agency.dig(:COORDONNEES,:ADRESSE1)
    body[:company_city]     = agency[:COORDONNEES][:VILLE]          if agency.dig(:COORDONNEES,:VILLE)
    body[:company_zipcode]  = agency[:COORDONNEES][:CP]             if agency.dig(:COORDONNEES,:CP)
    body[:company_administrative_area]  = nil
    body[:company_logo_url]             = nil
    body[:corporate_account]            = nil
    body[:color1]                       = nil
    body[:color2]                       = nil
    body[:presentation_text]            = {}
    body[:presentation_text][:fr]       = agency[:COORDONNEES][:RESUME_AGENCE]             if agency.dig(:COORDONNEES,:RESUME_AGENCE)
    body[:presentation_text][:en]       = nil
    body[:presentation_text][:de]       = nil
    body[:presentation_text][:nl]       = nil
    body[:presentation_text][:it]       = nil
    body[:presentation_text][:es]       = nil
    body[:presentation_text][:pt]       = nil
    body[:bio]            = {}
    body[:bio][:fr]       = nil
    body[:bio][:en]       = nil
    body[:bio][:de]       = nil
    body[:bio][:nl]       = nil
    body[:bio][:it]       = nil
    body[:bio][:es]       = nil
    body[:bio][:pt]       = nil
    body[:baseline]            = {}
    body[:baseline][:fr]       = nil
    body[:baseline][:en]       = nil
    body[:baseline][:de]       = nil
    body[:baseline][:nl]       = nil
    body[:baseline][:it]       = nil
    body[:baseline][:es]       = nil
    body[:baseline][:pt]       = nil

    httparty  = HTTParty.put("#{ENV['api_shareimmo_base']}/accounts/#{account_uid}",{:headers => apiKeys,:body => body})
    raise "Error on Account Update" if httparty.code != 200


    ##============================================================##
    ## Properties
    ##============================================================##
    agency[:BIEN].each do |bien|
      JulesLogger.info "==="*30
      body                      = {}
      body[:uid]                = bien[:REFDISTANTE] if bien.dig(:REFDISTANTE)
      body[:account_uid]        = account_uid
      body[:online]             = 1


      # If classification in appartment
      if bien.dig(:APPARTEMENT)
        appartement = bien[:APPARTEMENT]
        case appartement[:CATEGORIE_APPARTEMENT]
        when 1 # Appartement ancien
          classification_id = 201
        when 2  # Appartement bourgeois
          classification_id = 201
        when 3 # Appartement moderne
          classification_id = 114
        when 4  # Loft
          classification_id = 207
        when 5  # Habitation de loisirs
          classification_id = 201
        when 6 # Chambre de bonne
          classification_id = 202
        when 7  # Autre
          classification_id = 201
        when 8 # Non spécifié
          classification_id = 201
        else
          classification_id = 201
        end
        area_living = appartement[:SURFACE_HABITABLE] if appartement.dig(:SURFACE_HABITABLE)
        area_land   = appartement[:SURFACE_TOTALE]    if appartement.dig(:SURFACE_TOTALE)
        level       = appartement[:ETAGE]             if appartement.dig(:ETAGE)
        rooms       = appartement[:NB_PIECES]         if appartement.dig(:NB_PIECES)
        bedrooms    = appartement[:NB_CHAMBRES]       if appartement.dig(:NB_CHAMBRES)
        bathrooms   = appartement[:NB_SDB]            if appartement.dig(:NB_SDB)
        insidePool  = appartement[:PISCINE]           if appartement.dig(:PISCINE)
      end


      # If classification in maison
      if bien.dig(:MAISON)
        maison = bien[:MAISON]
        case maison[:CATEGORIE_MAISON]
        when 1 # Maison de ville
          classification_id = 114
        when 2 # Maison de village
          classification_id = 115
        when 3 # Maison de campagne
          classification_id = 115
        when 4 # Hotel particulier
          classification_id = 109
        when 5 # Maison individuelle en lotissement
          classification_id = 109
        when 6 # Pavillon en lotissement
          classification_id = 701
        when 7 # Fermette
          classification_id = 108
        when 8 # Habitation de loisirs
          classification_id = 101
        when 9 # Villa
          classification_id = 109
        when 10 # Mas
          classification_id = 107
        when 11 # Chalet
          classification_id = 110
        when 12 # Autre
          classification_id = 119
        when 13 # Non spécifié
          classification_id = 119
        else
          classification_id = 101
        end

        area_living = maison[:SURFACE_HABITABLE] if maison.dig(:SURFACE_HABITABLE)
        area_land   = maison[:SURFACE_TOTALE]    if maison.dig(:SURFACE_TOTALE)
        level       = maison[:ETAGE]             if maison.dig(:ETAGE)
        rooms       = maison[:NB_PIECES]         if maison.dig(:NB_PIECES)
        bedrooms    = maison[:NB_CHAMBRES]       if maison.dig(:NB_CHAMBRES)
        bathrooms   = maison[:NB_SDB]            if maison.dig(:NB_SDB)
        insidePool  = maison[:PISCINE]           if maison.dig(:PISCINE)
      end




      body[:classification_id]  = classification_id
      body[:status_id]          = bien.dig(:ANCIENNETE) && bien[:ANCIENNETE] == "1" ? 1 : 2
      body[:listing_id]         = bien.dig(:VENTE) ? 1 : 2
      body[:year_of_built]      = nil
      body[:building_uid]       = nil
      body[:unit]               = nil
      body[:level]              = nil
      body[:cadastre]           = nil
      body[:flag_id]            = nil
      body[:availability_date]  = nil
      body[:is_luxurious]       = nil



      body[:address]                                = Hash.new
      body[:address][:address_visibility]           = 1
      body[:address][:address_formatted]            = bien[:LOCALISATION][:ADRESSE1]   if bien.dig(:LOCALISATION,:ADRESSE1)
      body[:address][:street_number]                = nil
      body[:address][:street_name]                  = nil
      body[:address][:zipcode]                      = bien[:LOCALISATION][:CP]        if bien.dig(:LOCALISATION,:CP)
      body[:address][:locality]                     = bien[:LOCALISATION][:VILLE]     if bien.dig(:LOCALISATION,:VILLE)
      body[:address][:sublocality]                  = "#{bien[:LOCALISATION][:SECTEUR]} #{bien[:LOCALISATION][:QUARTIER]}"  if (bien.dig(:LOCALISATION,:QUARTIER) || bien.dig(:LOCALISATION,:SECTEUR))
      body[:address][:administrative_area_level1]   = nil
      body[:address][:administrative_area_level2]   = nil
      body[:address][:country]                      = ISO3166::Country.find_country_by_name(bien[:LOCALISATION][:PAYS]).alpha2 if (bien.dig(:LOCALISATION,:PAYS) && ISO3166::Country.find_country_by_name(bien[:LOCALISATION][:PAYS]).present?)
      body[:address][:latitude]                     = bien[:LOCALISATION][:LATITUDE]     if bien.dig(:LOCALISATION,:LATITUDE)
      body[:address][:longitude]                    = bien[:LOCALISATION][:LONGITUDE]    if bien.dig(:LOCALISATION,:LONGITUDE)


      body[:area]             = Hash.new
      body[:area][:living]    = area_living
      body[:area][:land]      = area_land
      body[:area][:unit_id]   = 2


      body[:rooms]                = Hash.new
      body[:rooms][:rooms]        = nil
      body[:rooms][:bathrooms]    = nil
      body[:rooms][:bedrooms]     = nil
      body[:rooms][:showerrooms]  = nil
      body[:rooms][:toilets]      = nil


      #   body[:title]         = Hash.new
      #   body[:title][:fr]    = item["freeTitle"]["FR"] if item.dig("freeTitle", "FR")
      #   body[:title][:en]    = item["freeTitle"]["EN"] if item.dig("freeTitle", "EN")
      #   body[:title][:de]    = item["freeTitle"]["DE"] if item.dig("freeTitle", "DE")
      #   body[:title][:nl]    = item["freeTitle"]["NL"] if item.dig("freeTitle", "NL")
      #   body[:title][:it]    = item["freeTitle"]["IT"] if item.dig("freeTitle", "IT")
      #   body[:title][:es]    = item["freeTitle"]["ES"] if item.dig("freeTitle", "ES")
      #   body[:title][:pt]    = item["freeTitle"]["PT"] if item.dig("freeTitle", "PT")


      #   body[:description_short]         = Hash.new
      #   body[:description_short][:fr]    = nil
      #   body[:description_short][:en]    = nil
      #   body[:description_short][:de]    = nil
      #   body[:description_short][:nl]    = nil
      #   body[:description_short][:it]    = nil
      #   body[:description_short][:es]    = nil
      #   body[:description_short][:pt]    = nil


      #   body[:description]         = Hash.new
      #   body[:description][:fr]    = item["freeDescription"]["FR"] if item.dig("freeDescription", "FR")
      #   body[:description][:en]    = item["freeDescription"]["EN"] if item.dig("freeDescription", "EN")
      #   body[:description][:de]    = item["freeDescription"]["DE"] if item.dig("freeDescription", "DE")
      #   body[:description][:nl]    = item["freeDescription"]["NL"] if item.dig("freeDescription", "NL")
      #   body[:description][:it]    = item["freeDescription"]["IT"] if item.dig("freeDescription", "IT")
      #   body[:description][:es]    = item["freeDescription"]["ES"] if item.dig("freeDescription", "ES")
      #   body[:description][:pt]    = item["freeDescription"]["PT"] if item.dig("freeDescription", "PT")


      #   body[:url]         = Hash.new
      #   body[:url][:fr]    = item["virtualVisitURL"]["FR"] if item.dig("virtualVisitURL", "FR")
      #   body[:url][:en]    = item["virtualVisitURL"]["EN"] if item.dig("virtualVisitURL", "EN")
      #   body[:url][:de]    = item["virtualVisitURL"]["DE"] if item.dig("virtualVisitURL", "DE")
      #   body[:url][:nl]    = item["virtualVisitURL"]["NL"] if item.dig("virtualVisitURL", "NL")
      #   body[:url][:it]    = item["virtualVisitURL"]["IT"] if item.dig("virtualVisitURL", "IT")
      #   body[:url][:es]    = item["virtualVisitURL"]["ES"] if item.dig("virtualVisitURL", "ES")
      #   body[:url][:pt]    = item["virtualVisitURL"]["PT"] if item.dig("virtualVisitURL", "PT")


      #   body[:financial] = {}
      #   body[:financial][:price]          = (body[:listing_id]  == 1 && item.dig("transaction", "price", "value")) ? item["transaction"]["price"]["value"] : nil
      #   body[:financial][:rent]           = (body[:listing_id]  == 2 && item.dig("transaction", "price", "value")) ? item["transaction"]["price"]["value"] : nil
      #   body[:financial][:currency]       = item["transaction"]["price"]["currency"] if item.dig("transaction", "price", "currency")
      #   body[:financial][:price_with_tax] = nil
      #   body[:financial][:fees]           = item["transaction"]["maintenanceCharges"]["value"] if item.dig("transaction", "maintenanceCharges", "value")
      #   body[:financial][:rent_frequency] = nil
      #   body[:financial][:tax1_annual]    = nil
      #   body[:financial][:tax2_annual]    = nil
      #   body[:financial][:income_annual]  = item["generalInformation"]["rentalIncome"] if item.dig("generalInformation", "rentalIncome")
      #   body[:financial][:deposit]        = nil


      #   body[:canada]       = Hash.new
      #   body[:canada][:mls] = nil

      #   body[:france]                         = Hash.new
      #   body[:france][:dpe_indice]            = nil
      #   body[:france][:dpe_value]             = nil
      #   body[:france][:dpe_id]                = nil
      #   body[:france][:ges_indice]            = nil
      #   body[:france][:ges_value]             = nil
      #   body[:france][:alur_is_condo]         = nil
      #   body[:france][:alur_units]            = nil
      #   body[:france][:alur_uninhabitables]   = nil
      #   body[:france][:alur_spending]         = nil
      #   body[:france][:alur_legal_action]     = nil
      #   body[:france][:rent_honorary]         = nil
      #   body[:france][:mandat_exclusif]       = nil


      #   body[:belgium]                          = Hash.new
      #   body[:belgium][:peb_indice]             = nil
      #   body[:belgium][:peb_value]              = item["buildingRequirements"]["epc"]["energyConsumption"] if item.dig("buildingRequirements", "epc", "energyConsumption")
      #   body[:belgium][:peb_id]                 = item["buildingRequirements"]["epc"]["epcReference"] if item.dig("buildingRequirements", "epc", "epcReference")
      #   body[:belgium][:building_permit]        = nil
      #   body[:belgium][:done_assignments]       = nil
      #   body[:belgium][:preemption_property]    = ( item["buildingRequirements"]["preemption"].to_i > 0 ? 1 : 0 ) if item.dig("buildingRequirements", "preemption")
      #   body[:belgium][:subdivision_permits]    = nil
      #   body[:belgium][:sensitive_flood_area]   = nil
      #   body[:belgium][:delimited_flood_zone]   = nil
      #   body[:belgium][:risk_area]              = nil


      #   body[:other] = Hash.new
      #   body[:other][:orientation]          = Hash.new
      #   body[:other][:orientation][:north]  = item["outsideDescription"]["orientation"]["north"] if item.dig("outsideDescription", "orientation", "north")
      #   body[:other][:orientation][:south]  = item["outsideDescription"]["orientation"]["south"] if item.dig("outsideDescription", "orientation", "south")
      #   body[:other][:orientation][:east]   = item["outsideDescription"]["orientation"]["east"] if item.dig("outsideDescription", "orientation", "east")
      #   body[:other][:orientation][:west]   = item["outsideDescription"]["orientation"]["west"] if item.dig("outsideDescription", "orientation", "west")


      #   body[:other][:inclusion]                      = Hash.new
      #   body[:other][:inclusion][:air_conditioning]   = ( item["goodEquipment"]["heating"]["airConditioning"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "heating", "airConditioning")
      #   body[:other][:inclusion][:hot_water]          = ( item["insideDescription"]["connections"]["water"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "water")
      #   body[:other][:inclusion][:heated]             = ( item["insideDescription"]["connections"]["gasSupply"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "gasSupply")
      #   body[:other][:inclusion][:electricity]        = ( item["insideDescription"]["connections"]["electricity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "electricity")
      #   body[:other][:inclusion][:furnished]          = ( item["insideDescription"]["furnished"]["yesno"].to_i > 0 ? 1 : 0 )  if item.dig("insideDescription", "furnished", "yesno")
      #   body[:other][:inclusion][:fridge]             = nil
      #   body[:other][:inclusion][:cooker]             = nil
      #   body[:other][:inclusion][:dishwasher]         = nil
      #   body[:other][:inclusion][:dryer]              = nil
      #   body[:other][:inclusion][:washer]             = ( item["insideDescription"]["rooms"]["washhouse"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "washhouse", "quantity")
      #   body[:other][:inclusion][:microwave]          = nil


      #   body[:other][:detail]                       = Hash.new
      #   body[:other][:detail][:elevator]            = ( item["goodEquipment"]["elevator"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "elevator")
      #   body[:other][:detail][:laundry]             = nil
      #   body[:other][:detail][:garbage_chute]       = nil
      #   body[:other][:detail][:common_space]        = nil
      #   body[:other][:detail][:janitor]             = nil
      #   body[:other][:detail][:gym]                 = nil
      #   body[:other][:detail][:golf]                = nil
      #   body[:other][:detail][:tennis]              = ( item["outsideDescription"]["tennisCourt"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "tennisCourt", "yesno")
      #   body[:other][:detail][:sauna]               = nil
      #   body[:other][:detail][:spa]                 = nil
      #   body[:other][:detail][:inside_pool]         = nil
      #   body[:other][:detail][:outside_pool]        = ( item["outsideDescription"]["swimmingPool"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "swimmingPool", "yesno")
      #   body[:other][:detail][:inside_parking]      = nil
      #   body[:other][:detail][:outside_parking]     = ( item["outsideDescription"]["numberOfOutdoorParking"].to_i > 0 ? 1 : 0 ) if item.dig("outsideDescription", "numberOfOutdoorParking")
      #   body[:other][:detail][:parking_on_street]   = nil
      #   body[:other][:detail][:garagebox]           = ( item["insideDescription"]["rooms"]["garage"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "garage", "quantity")



      #   body[:other][:half_furnished]                 = Hash.new
      #   body[:other][:half_furnished][:living_room]   = nil
      #   body[:other][:half_furnished][:bedrooms]      = nil
      #   body[:other][:half_furnished][:kitchen]       = nil
      #   body[:other][:half_furnished][:other]         = nil



      #   body[:other][:indoor]                         = Hash.new
      #   body[:other][:indoor][:attic]                 = ( item["insideDescription"]["rooms"]["attic"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "attic", "quantity")
      #   body[:other][:indoor][:attic_convertible]     = nil
      #   body[:other][:indoor][:attic_converted]       = nil
      #   body[:other][:indoor][:cellar]                = ( item["insideDescription"]["rooms"]["cellar"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "cellar", "quantity")
      #   body[:other][:indoor][:central_vacuum]        = nil
      #   body[:other][:indoor][:entries_washer_dryer]  = nil
      #   body[:other][:indoor][:entries_dishwasher]    = nil
      #   body[:other][:indoor][:fireplace]             = nil
      #   body[:other][:indoor][:storage]               = nil
      #   body[:other][:indoor][:no_smoking]            = nil
      #   body[:other][:indoor][:double_glazing]        = ( item["goodEquipment"]["doubleGlazing"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "doubleGlazing")
      #   body[:other][:indoor][:central_vacuum]        = nil
      #   body[:other][:indoor][:triple_glazing]        = nil



      #   body[:other][:floor]              = Hash.new
      #   body[:other][:floor][:carpet]     = nil
      #   body[:other][:floor][:wood]       = nil
      #   body[:other][:floor][:floating]   = nil
      #   body[:other][:floor][:ceramic]    = nil
      #   body[:other][:floor][:parquet]    = nil
      #   body[:other][:floor][:cushion]    = nil
      #   body[:other][:floor][:vinyle]     = nil
      #   body[:other][:floor][:lino]       = nil
      #   body[:other][:floor][:marble]     = nil



      #   body[:other][:exterior]                     = Hash.new
      #   body[:other][:exterior][:land_access]       = nil
      #   body[:other][:exterior][:back_balcony]      = nil
      #   body[:other][:exterior][:front_balcony]     = nil
      #   body[:other][:exterior][:private_patio]     = nil
      #   body[:other][:exterior][:storage]           = nil
      #   body[:other][:exterior][:terrace]           = ( item["outsideDescription"]["terrace"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "terrace", "yesno")
      #   body[:other][:exterior][:veranda]           = ( item["outsideDescription"]["veranda"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "veranda", "yesno")
      #   body[:other][:exterior][:garden]            = ( item["outsideDescription"]["garden"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "garden", "yesno")
      #   body[:other][:exterior][:sea_view]          = nil
      #   body[:other][:exterior][:mountain_view]     = nil


      #   body[:other][:accessibility]                      = Hash.new
      #   body[:other][:accessibility][:elevator]           = nil
      #   body[:other][:accessibility][:balcony]            = nil
      #   body[:other][:accessibility][:grab_bar]           = nil
      #   body[:other][:accessibility][:wider_corridors]    = nil
      #   body[:other][:accessibility][:lowered_switches]   = nil
      #   body[:other][:accessibility][:ramp]               = nil


      #   body[:other][:senior]                         = Hash.new
      #   body[:other][:senior][:autonomy]              = nil
      #   body[:other][:senior][:half_autonomy]         = nil
      #   body[:other][:senior][:no_autonomy]           = nil
      #   body[:other][:senior][:meal]                  = nil
      #   body[:other][:senior][:nursery]               = nil
      #   body[:other][:senior][:domestic_help]         = nil
      #   body[:other][:senior][:community]             = nil
      #   body[:other][:senior][:activities]            = nil
      #   body[:other][:senior][:validated_residence]   = nil
      #   body[:other][:senior][:housing_cooperative]   = nil



      #   body[:other][:pet]                    = Hash.new
      #   body[:other][:pet][:allow]            = item["generalInformation"]["petsAllowed"] if item.dig("generalInformation", "petsAllowed")
      #   body[:other][:pet][:cat]              = nil
      #   body[:other][:pet][:dog]              = nil
      #   body[:other][:pet][:little_dog]       = nil
      #   body[:other][:pet][:cage_aquarium]    = nil
      #   body[:other][:pet][:not_allow]        = nil



      #   body[:other][:service]              = Hash.new
      #   body[:other][:service][:internet]   = ( item["insideDescription"]["connections"]["internet"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "internet")
      #   body[:other][:service][:tv]         = ( item["insideDescription"]["connections"]["televisionCable"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "televisionCable")
      #   body[:other][:service][:tv_sat]     = ( item["insideDescription"]["connections"]["satelliteTV"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "satelliteTV")
      #   body[:other][:service][:phone]      = ( item["insideDescription"]["connections"]["phone"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "phone")


      #   body[:other][:composition]                    = Hash.new
      #   body[:other][:composition][:bar]              = nil
      #   body[:other][:composition][:living]           = ( item["insideDescription"]["rooms"]["livingRoom"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "livingRoom", "quantity")
      #   body[:other][:composition][:dining]           = nil
      #   body[:other][:composition][:separate_toilet]  = nil
      #   body[:other][:composition][:open_kitchen]     = ( item["insideDescription"]["rooms"]["kitchen"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "kitchen", "quantity")


      #   body[:other][:heating]                  = Hash.new
      #   body[:other][:heating][:electric]       = nil
      #   body[:other][:heating][:solar]          = nil
      #   body[:other][:heating][:gaz]            = nil
      #   body[:other][:heating][:condensation]   = nil
      #   body[:other][:heating][:fuel]           = ( item["goodEquipment"]["heating"]["heatingFuel"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "heating", "heatingFuel")
      #   body[:other][:heating][:heat_pump]      = nil
      #   body[:other][:heating][:floor_heating]  = nil


      #   body[:other][:transport]                      = Hash.new
      #   body[:other][:transport][:bicycle_path]       = nil
      #   body[:other][:transport][:public_transport]   = nil
      #   body[:other][:transport][:public_bike]        = nil
      #   body[:other][:transport][:subway]             = nil
      #   body[:other][:transport][:train_station]      = nil


      #   body[:other][:security]                     = Hash.new
      #   body[:other][:security][:guardian]          = nil
      #   body[:other][:security][:alarm]             = ( item["goodEquipment"]["alarm"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "alarm")
      #   body[:other][:security][:intercom]          = nil
      #   body[:other][:security][:camera]            = nil
      #   body[:other][:security][:smoke_dectector]   = nil

      JulesLogger.info body












    end


  end











    #         # If classification in maison
    #         if bien.css("PARKING_GARAGE").first.present?
    #           parking = bien.css("PARKING_GARAGE").first.content

    #           case parking.css("CATEGORIE_PARKING").first.content
    #                 # Parking
    #               when 1
    #                 my_unit[:outsideParking] = 1
    #                 # Parking couvert
    #               when 2
    #                 my_unit[:insideParking] = 1
    #                 # Garage
    #               when 3
    #                 my_unit[:outsideParking] = 1
    #                 # Non spécifié
    #               when 4
    #                 my_unit[:parkingOnStreet] = 1
    #               end if parking.css("CATEGORIE_PARKING").first.present?
    #             end












    #         if bien.css("DPE_ENERGIE").first.present?
    #           my_unit[:index_energy] = bien.css("DPE_ENERGIE").first.css("LETTRE").first.content if bien.css("DPE_ENERGIE").first.css("LETTRE").first.present?
    #         end

    #         my_unit[:financial_currency] = "EUR"

    #         # Vente
    #         if bien.css("VENTE").first.present?

    #           vente = bien.css("VENTE").first
    #           my_unit[:listing_id] = 1
    #             # my_unit[:flag_id] =
    #             # my_unit[:availability_date] =
    #             # my_unit[:index_insulation] =
    #             # my_unit[:building_uid] =
    #             # my_unit[:building_name] =
    #             # my_unit[:building_units] =
    #             # my_unit[:building_levels] =
    #             # my_unit[:sublocality] =
    #             # my_unit[:administrative_area_level2] =
    #             # my_unit[:description] =

    #             # my_unit[:address_visibility] =
    #             # my_unit[:balcony] =
    #             # my_unit[:rooms] =
    #             # my_unit[:half] =

    #             textes = vente.css("TEXTES").first

    #             if textes.present?
    #               my_unit[:title_fr] = textes.css("TITRE_FR").first.content if textes.css("TITRE_FR").first.present?
    #               my_unit[:description_fr] = textes.css("DESCRIPTION_FR").first.content if textes.css("DESCRIPTION_FR").first.present?
    #               my_unit[:title_en] = textes.css("TITRE_EN").first.content if textes.css("TITRE_EN").first.present?
    #               my_unit[:description_en] = textes.css("DESCRIPTION_EN").first.content if textes.css("DESCRIPTION_EN").first.present?

    #             end




    #             # my_unit[:url] =
    #             my_unit[:financial_price] = vente.css("PRIX").first.content if vente.css("PRIX").first.present?

    #             # my_unit[:fees] =
    #             my_unit[:tax1_annual] = vente.css("TAXE_HABITATION").first.content if vente.css("TAXE_HABITATION").first.present?
    #             my_unit[:tax2_annual] = vente.css("TAXE_FONCIERE").first.content if vente.css("TAXE_FONCIERE").first.present?

    #             my_unit[:pictures] = []

    #             Dir.glob("#{folder_destination}#{my_account[:uid]}-#{my_unit[:uid]}-*").each_with_index do | file, index |
    #               f = File.open(File.join(file))
    #               FileUtils.mv(f.path, pictures_path)
    #               my_unit[:pictures] << { :url => [pictures_url, File.basename(file)].join("/") }
    #               f.close
    #             end

    #         # Location
    #       elsif bien.css("LOCATION").first.present?
    #         rent = bien.css("LOCATION").first
    #         my_unit[:listing_id] = 2
    #         my_unit[:financial_rent] = rent.css("LOYER_MENSUEL_TTC").first.content if rent.css("LOYER_MENSUEL_TTC").first.present?
    #             # my_unit[:rent_frequency] =
    #



  end
