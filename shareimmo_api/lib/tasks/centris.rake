require 'net/ftp'
require 'rubygems'
require 'zip'
require 'csv'
include ActionView::Helpers::AssetUrlHelper


namespace :centris do

  task test: :environment do
    centris_txt = "#{Rails.root}/import/centris/16-11-30/viamag/RENOVATIONS.TXT"
    File.open(centris_txt, File::RDONLY,:encoding => 'ISO-8859-1') do |file|
      CSV.parse(file.read) do |row|
        JulesLogger.info row
      end
    end
  end



  ##============================================================##
  ## Import Alls datas
  ##============================================================##
  task import: :environment do
    begin
      puts "Start Import"
      puts "="*30
      start               = Time.now
      folder_destination  = "#{Rails.root}/import/centris/#{Time.now.strftime("%y-%m-%d")}"
      FileUtils.rm_rf(Dir.glob("#{Rails.root}/import/centris/*"))
      FileUtils.mkdir_p(folder_destination)


      ##============================================================##
      ## Connexion au FTP -> on récupère tous les comptes actifs
      ## -> on dézippe tous les fichiers dans un dossier puis on
      ## supprime les fichiers zip originaux
      ##============================================================##
      puts "Download Zip And Unzip"
      account_folders = Array.new
      ImportUser.where(:status=>1,:import_provider=> "centris").each do |account|
        Net::FTP.open(account.ftp_domain,account.ftp_user,account.ftp_password) do |ftp|
          files = ftp.nlst("*.zip")
          unless files.blank?
            account_folder = "#{folder_destination}/#{account.ftp_user}"
            account_folders << {
              :flow_name  => account.flow_name,
              :path       => account_folder
            }
            FileUtils.mkdir_p(account_folder) unless File.directory?(account_folder)
            zip_destination = "#{folder_destination}/#{account.ftp_user}.zip"
            ftp.getbinaryfile(files.last,zip_destination)
            Zip::File.open(zip_destination) do |zip_file|
              zip_file.each do |entry|
                entry.extract("#{folder_destination}/#{account.ftp_user}/#{entry.name}")
              end
            end
            File.delete(zip_destination)
          end
        end
      end
      t1 = Time.now
      puts "Durartion (#{t1-start} s)"
      puts "="*30



      ##======================================================================================================##
      ## Ces fichiers ne sont présent que lors du 1er envoies où lors qu'il y  a des modifications car ceci
      ## servent de "dictonnaires" et de références pour les autres tables. Ces fichiers sont les mêmes
      ## pour tout les courtiers centris, Donc si on trouve plusieurs fois ces fichiers on ne  les
      ## concatainent pas dans un fichier unique mais on écrasse le fichier
      ##======================================================================================================##
      centris_files = Array.new
      # centris_files << { :text_file => 'REGIONS.TXT',                      :concatenate => false, :model => CentrisRegion                }
      # centris_files << { :text_file => 'MUNICIPALITES.TXT',                :concatenate => false, :model => CentrisCity                  }
      # centris_files << { :text_file => 'QUARTIERS.TXT',                    :concatenate => false, :model => CentrisNeighborhood          }
      # centris_files << { :text_file => 'GENRES_PROPRIETES.TXT',            :concatenate => false, :model => CentrisPropertyClassification}
      # centris_files << { :text_file => 'TYPE_CARACTERISTIQUES.TXT',        :concatenate => false, :model => CentrisFeatureType           }
      # centris_files << { :text_file => 'SOUS_TYPE_CARACTERISTIQUES.TXT',   :concatenate => false, :model => CentrisFeatureSubtype        }
      # centris_files << { :text_file => 'TYPES_BANNIERES.TXT',              :concatenate => false, :model => CentrisBanner                }
      # centris_files << { :text_file => 'VALEURS_FIXES.TXT',                :concatenate => false, :model => CentrisFixValue              }
      ##======================================================================================================##
      ## Ces fichiers sont propre à chaque dépôt FTP
      ##======================================================================================================##
      centris_files << { :text_file => 'MEMBRES.TXT',                 :concatenate => true,  :model => CentrisMember              }
      centris_files << { :text_file => 'INSCRIPTIONS.TXT',            :concatenate => true,  :model => CentrisInscription         }
      centris_files << { :text_file => 'FIRMES.TXT',                  :concatenate => true,  :model => CentrisFirm                }
      centris_files << { :text_file => 'BUREAUX.TXT',                 :concatenate => true,  :model => CentrisOffice              }
      centris_files << { :text_file => 'ADDENDA.TXT',                 :concatenate => true,  :model => CentrisAddenda             }
      centris_files << { :text_file => 'CARACTERISTIQUES.TXT',        :concatenate => true,  :model => CentrisSpecification       }
      centris_files << { :text_file => 'DEPENSES.TXT',                :concatenate => true,  :model => CentrisCost                }
      centris_files << { :text_file => 'ESPACES_REVENUS.TXT',         :concatenate => true,  :model => CentrisIncome              }
      centris_files << { :text_file => 'PHOTOS.TXT',                  :concatenate => true,  :model => CentrisPicture             }
      centris_files << { :text_file => 'PIECES.TXT',                  :concatenate => true,  :model => CentrisRoom                }
      centris_files << { :text_file => 'REMARQUES.TXT',               :concatenate => true,  :model => CentrisRemark              }
      centris_files << { :text_file => 'RENOVATIONS.TXT',             :concatenate => true,  :model => CentrisRenovation          }
      centris_files << { :text_file => 'VISITES_LIBRES.TXT',          :concatenate => true,  :model => CentrisVisitesLibre        }
      centris_files << { :text_file => 'PIECES_INTERGENERATIONS.TXT', :concatenate => true,  :model => CentrisPieceIntergeneration}


      ##============================================================##
      ##
      ##============================================================##
      centris_files.each do |c|
        t2 = Time.now
        puts "#{c[:text_file]}"
        batch   = []
        model   = c[:model]
        keys    = model.attribute_names.drop(1)

        ##============================================================##
        ## Clean DB
        ##============================================================##
        ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
        ActiveRecord::Base.connection.execute("TRUNCATE #{model.table_name}")
        ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")

        ##============================================================##
        ##
        ##============================================================##
        account_folders.each_with_index do |folder,index|
          centris_txt = "#{folder[:path]}/#{c[:text_file]}"
          File.open(centris_txt, File::RDONLY,:encoding => 'ISO-8859-1') do |file|
            CSV.parse(file.read) do |row|
              row = row.unshift(folder[:flow_name]) if model == CentrisMember || model == CentrisInscription
              batch << model.new(Hash[keys.zip(row)])
            end
            if batch.size > 10000
              model.import(batch,:validate => false)
              batch = []
            end
          end if File.exist?(centris_txt)
        end


        model.import(batch,:validate => false)
        puts "Duration (#{(Time.now-t2)} s)"
        puts "="*30
      end

      puts "Sync OK => (#{(Time.now-start)} s)"
    rescue Exception => e
      JulesLogger.info e.message
      JulesLogger.info "==="
      JulesLogger.info e.backtrace
    end
  end


  ##============================================================##
  ## Exports Users
  ##============================================================##
  task export_users: :environment do
    begin
      start         = Time.now
      apiKeys       = {"ApiKey" => ENV['centris_api_key'],"ApiToken" => ENV['centris_api_token']}
      brokers       = CentrisMember.includes(centris_office: [:centris_firm]).where.not(:no_certificat => nil)
      brokers_count = brokers.count


      puts "Start Transfert #{brokers_count} brokers"
      brokers.in_groups_of(1000,false) do |brokers|
        # threads = []
        puts "===="*10
        brokers.each do |broker|
          # threads << Thread.new do
            centris_id = broker.no_certificat
            ##============================================================##
            ## AddInfos To ApiKeys
            ##============================================================##
            apiKeys[:flowName] = broker.flow_name

            ##============================================================##
            ## Step #1 : Check if account is existing
            ##============================================================##
            httparty  = HTTParty.get("#{ENV['api_shareimmo_base']}/accounts/#{centris_id}", :headers => apiKeys)
            response  =  JSON.parse(httparty.body)
            if response["uid"].present?
              puts "Account already created with id #{response["id"]} and username #{response["username"]}"
              need_update_account = response["sign_in_count"] == 0
            else
              puts "create account #{centris_id}"
              need_update_account = true
              ##============================================================##
              ## Create Account
              ##============================================================##
              body = {
                :uid      => centris_id,
                :email    => "centris_#{centris_id}@shareimmo.com",
                :password => SecureRandom.urlsafe_base64
              }
              httparty  = HTTParty.post("#{ENV['api_shareimmo_base']}/accounts",{:headers => apiKeys,:body => body})
              raise "Error on Account Creation #{centris_id}" if httparty.code != 200
            end

            ##============================================================##
            ## Step #2 : update account
            ##============================================================##
            if need_update_account == true
              office =  broker.centris_office
              firm   =  office.centris_firm
              case firm.banniere_code
              when "CAPI"
                color1 = "#01224B"
                color2 =  "#1AB7EA"
                logo   = 'centris/logo_via.png'
              when "CENT"
                color1 = "#000000"
                color2 = "#FDC94D"
                logo   = 'centris/logo_century.png'
              when "REM"
                color1 = "#D80034"
                color2 = "#2F4794"
                logo   = 'centris/logo_remax.jpeg'
              when "ROYLEP"
                color1 = "#E12120"
                color2 = "#000000"
                logo   = 'centris/logo_royal.png'
              when "SUTTON"
                color1 = "#E32A34"
                color2 = "#000000"
                logo   = 'centris/logo_sutton.png'
              else
                color1 = "#ED5565"
                color2 = "#3B5369"
                logo   = nil
              end

              body = Hash.new
              body[:country]                      = "CA"
              body[:public_email]                 = broker.courriel
              body[:first_name]                   = broker.prenom
              body[:last_name]                    = broker.nom
              body[:phone]                        = broker.telephone_1
              body[:phone_sms]                    = broker.telephone_2
              body[:phone_visibility]             = 1
              body[:website]                      = broker.site_web
              body[:spoken_languages]             = "fr,en"
              body[:picture_url]                  = broker.photo_url
              body[:facebook_url]                 = nil
              body[:twitter_url]                  = nil
              body[:linkedin_url]                 = nil
              body[:pinterest_url]                = nil
              body[:google_plus_url]              = nil
              body[:instagram_url]                = nil
              body[:company_name]                 = firm.nom_legal
              body[:company_address]              = "#{office.no_civique} #{office.nom_rue}"
              body[:company_city]                 = "#{office.municipalite}"
              body[:company_zipcode]              = "#{office.code_postal}"
              body[:company_administrative_area]  = office.code_postal
              body[:company_logo_url]             = office.url_logo_bureau.present? ?  office.url_logo_bureau : (logo.present? ? "#{ENV["host"]}/#{logo}" : nil)
              body[:corporate_account]            = 1
              body[:color1]                       = color1
              body[:color2]                       = color2

              body[:presentation_text]         = Hash.new
              body[:presentation_text][:fr]    = nil
              body[:presentation_text][:en]    = nil
              body[:presentation_text][:de]    = nil
              body[:presentation_text][:nl]    = nil
              body[:presentation_text][:it]    = nil
              body[:presentation_text][:es]    = nil
              body[:presentation_text][:pt]    = nil

              body[:bio]         = Hash.new
              body[:bio][:fr]    = nil
              body[:bio][:en]    = nil
              body[:bio][:de]    = nil
              body[:bio][:nl]    = nil
              body[:bio][:it]    = nil
              body[:bio][:es]    = nil
              body[:bio][:pt]    = nil

              body[:baseline]         = Hash.new
              body[:baseline][:fr]    = nil
              body[:baseline][:en]    = nil
              body[:baseline][:de]    = nil
              body[:baseline][:nl]    = nil
              body[:baseline][:it]    = nil
              body[:baseline][:es]    = nil
              body[:baseline][:pt]    = nil

              httparty = HTTParty.put("#{ENV['api_shareimmo_base']}/accounts/#{centris_id}",{:headers => apiKeys,:body => body})
            end
          # end
        end
        ##============================================================##
        ## After creating a few threads we wait for them all to finish consecutively.
        ##============================================================##
        # threads.each { |thr| thr.join }
        # sleep(0.1)
      end
      puts "Sync OK => (#{(Time.now-start).ceil} s)"
    rescue Exception => e
      JulesLogger.info e.message
      JulesLogger.info e.backtrace
    end
  end


  ##============================================================##
  ## Exports Properties
  ##============================================================##
  task export_properties: :environment do
    begin
      start             = Time.now
      apiKeys           = {"ApiKey" => ENV['centris_api_key'],"ApiToken" => ENV['centris_api_token']}
      properties        = CentrisInscription.all
      properties_count  = properties.count

      puts "Start Transfert #{properties_count} properties"
      properties.in_groups_of(100,false) do |inscriptions|
        puts "===="*10
        inscriptions.each do |inscription|
          if inscription.broker_1.present?
            body      = create_body(inscription,inscription.broker_1.no_certificat,1)
            httparty  = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",{:headers => apiKeys,:body => body})
            puts "broker 1 (#{inscription.broker_1.no_certificat}) => #{httparty.code}"
          end
          if inscription.broker_2.present?
            body      = create_body(inscription,inscription.broker_2.no_certificat,0)
            httparty  = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",{:headers => apiKeys,:body => body})
            puts "broker 2 (#{inscription.broker_2.no_certificat}) => #{httparty.code}"
          end
          if inscription.broker_3.present?
            body      = create_body(inscription,inscription.broker_3.no_certificat,0)
            httparty  = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",{:headers => apiKeys,:body => body})
            puts "broker 3 (#{inscription.broker_3.no_certificat}) => #{httparty.code}"
          end
          if inscription.broker_4.present?
            body      = create_body(inscription,inscription.broker_4.no_certificat,0)
            httparty  = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",{:headers => apiKeys,:body => body})
            puts "broker 4 (#{inscription.broker_4.no_certificat}) => #{httparty.code}"
          end
        end
      end
      puts "Sync OK => (#{(Time.now-start).ceil} s)"
    rescue Exception => e
      puts e.message
      puts e.backtrace
    end
  end




  def create_body(inscription,broker_uid,master)
    pictures                  = Array.new
    description_fr            = ""
    description_en            = ""
    category                  = CentrisPropertyClassification.where(:genre_propriete=> inscription.genre_propriete.present? ? inscription.genre_propriete : "AP").first
    shareimmo_classification  = PropertyClassifier.where(:name=>category.genre_propriete).first
    tax_1                     = inscription.centris_costs.where(:tdep_code=>"TAXMUN").first
    tax_2                     = inscription.centris_costs.where(:tdep_code=>"TAXSCO").first
    tax_3                     = inscription.centris_costs.where(:tdep_code=>"FCOP").first
    inscription.centris_pictures.each do |picture|
      pictures << { :url => picture.photourl, :position => picture.seq}
    end

    ##============================================================##
    ## Text #1 : Remarks
    ##============================================================##
    inscription.centris_remarks.order('ordre_affichage ASC').each do |remark|
      if remark.code_langue == "F"
        description_fr += "#{remark.texte}<br><br>"
      else
        description_en += "#{remark.texte}<br><br>"
      end
    end

    ##============================================================##
    ## Text #2
    ## Les Addendas Centris sont split sur une nouvelle ligne
    ## tout les 60 caractères...Si la ligne fini par un
    ## espace c'est quelle va avec la ligne du dessous
    ##============================================================##
    inscription.centris_addendas.order('ordre_affichage ASC').group_by(&:code_langue).each do |locale,addendas|
      if addendas.present?
        addendas.each do |a|
          if locale == "F"
            description_fr += a.texte.nil? ? "<br>" : "#{a.texte}#{ /\s+$/.match(a.texte).present? ? nil : "<br>"}"
          else
            description_en += a.texte.nil? ? "<br>" : "#{a.texte}#{ /\s+$/.match(a.texte).present? ? nil : "<br>"}"
          end
        end
      end
    end


    ##============================================================##
    ## Text #3 : Inclusions / Exclusions
    ## On ne doit pas ajouter les inclus/exclus si
    ## les remarks sont vides
    ##============================================================##
    description_fr += "<br><br><b>INCLUSIONS</b><br>#{inscription.inclus_francais.present? ? inscription.inclus_francais : "--"}<br><br><b>EXCLUSIONS</b><br>#{inscription.exclus_francais.present? ? "#{inscription.exclus_francais}<br><br>" :  "--<br><br>"}" if description_fr.present?
    description_en += "<br><br><b>INCLUSIONS</b><br>#{inscription.inclus_anglais.present?  ? inscription.inclus_anglais : "--"}<br><br><b>EXCLUSIONS</b><br>#{inscription.exclus_anglais.present? ? "#{inscription.exclus_anglais}<br><br>" : "--<br><br>"}"     if description_en.present?


    ##============================================================##
    ## Currency
    ##============================================================##
    currency = inscription.devise_prix_demande || inscription.devise_prix_location_demande
    currency = (currency == "CAN" ? "CAD" :  currency)


    ##============================================================##
    ## City
    ##============================================================##
    my_city         = ""
    my_sublocality  = ""

    if inscription.centris_city.present?
      city = inscription.centris_city.description.split("(")
        if city.length == 2
          my_sublocality = city[0]
          my_city =  city[1].split(')')[0]
        else
          my_sublocality = nil
          my_city = inscription.centris_city.description
        end
      end


    ##============================================================##
    ## body
    ##============================================================##
    body                      = Hash.new
    body[:uid]                = inscription.no_inscription
    body[:master]             = master
    body[:account_uid]        = broker_uid
    body[:building_uid]       = nil
    body[:online]             = 1
    body[:classification_id]  = shareimmo_classification.property_classification_id
    body[:status_id]          = 2
    body[:listing_id]         = inscription.frequence_prix_location.blank? ? 1 : 2
    body[:unit]               = inscription.appartement
    body[:level]              = inscription.niveau
    body[:cadastre]           = nil
    body[:flag_id]            = inscription.code_statut == "VE" ?  5 : nil
    body[:year_of_built]      = inscription.annee_construction
    body[:availability_date]  = inscription.date_fin_bail
    body[:is_luxurious]       = nil


    body[:address]                                = Hash.new
    body[:address][:address_visibility]           = 1
    body[:address][:address_formatted]            = "#{inscription.no_civique_debut} #{inscription.nom_rue_complet}, #{inscription.code_postal}, #{inscription.centris_city.present? ? inscription.centris_city.description : nil}, #{inscription.centris_region.present? ? inscription.centris_region.description_anglaise : nil }"
    body[:address][:street_number]                = inscription.no_civique_debut
    body[:address][:street_name]                  = inscription.nom_rue_complet
    body[:address][:zipcode]                      = inscription.code_postal
    body[:address][:locality]                     = my_city
    body[:address][:sublocality]                  = my_sublocality
    body[:address][:administrative_area_level1]   = "QC"
    body[:address][:administrative_area_level2]   = nil
    body[:address][:country]                      = "CA"
    body[:address][:latitude]                     = inscription.latitude.present? ? inscription.latitude.to_f : nil
    body[:address][:longitude]                    = inscription.longitude.present? ? inscription.longitude.to_f : nil


    body[:area]             = Hash.new
    body[:area][:living]    = inscription.superficie_habitable
    body[:area][:land]      = inscription.superficie_terrain
    body[:area][:unit_id]   = inscription.um_superficie_habitable == "PC" ? 2 : (inscription.um_superficie_habitable == "MC" ? 1 : nil)


    body[:rooms]                = Hash.new
    body[:rooms][:rooms]        = inscription.nb_pieces
    body[:rooms][:bathrooms]    = inscription.nb_salles_bains
    body[:rooms][:bedrooms]     = inscription.nb_chambres
    body[:rooms][:showerrooms]  = inscription.nb_salles_eau
    body[:rooms][:toilets]      = nil


    body[:title]         = Hash.new
    body[:title][:fr]    = "#{category.description_francaise} - #{inscription.no_civique_debut} #{inscription.nom_rue_complet}"
    body[:title][:en]    = "#{category.description_anglaise} - #{inscription.no_civique_debut} #{inscription.nom_rue_complet}"
    body[:title][:de]    = nil
    body[:title][:nl]    = nil
    body[:title][:it]    = nil
    body[:title][:es]    = nil
    body[:title][:pt]    = nil


    body[:description_short]         = Hash.new
    body[:description_short][:fr]    = nil
    body[:description_short][:en]    = nil
    body[:description_short][:de]    = nil
    body[:description_short][:nl]    = nil
    body[:description_short][:it]    = nil
    body[:description_short][:es]    = nil
    body[:description_short][:pt]    = nil


    body[:description]         = Hash.new
    body[:description][:fr]    = description_fr.present? ? description_fr : nil
    body[:description][:en]    = description_en.present? ? description_en : nil
    body[:description][:de]    = nil
    body[:description][:nl]    = nil
    body[:description][:it]    = nil
    body[:description][:es]    = nil
    body[:description][:pt]    = nil


    body[:url]         = Hash.new
    body[:url][:fr]    = inscription.url_desc_detaillee
    body[:url][:en]    = inscription.url_desc_detaillee
    body[:url][:de]    = nil
    body[:url][:nl]    = nil
    body[:url][:it]    = nil
    body[:url][:es]    = nil
    body[:url][:pt]    = nil


    body[:financial] = Hash.new
    body[:financial][:price]          = inscription.prix_demande
    body[:financial][:price_with_tax] = inscription.ind_taxes_prix_demande == "O" ? 1 : 0
    body[:financial][:rent]           = inscription.prix_location_demande
    body[:financial][:fees]           = tax_3.present? ? tax_3.montant_depense.to_f/12.00 : nil
    body[:financial][:rent_frequency] = inscription.frequence_prix_location == "M" ? 30 : 30
    body[:financial][:tax1_annual]    = tax_1.present? ? tax_1.montant_depense : nil
    body[:financial][:tax2_annual]    = tax_2.present? ? tax_2.montant_depense : nil
    body[:financial][:income_annual]  = inscription.rev_pot_brut_res
    body[:financial][:deposit]        = nil
    body[:financial][:currency]       = currency

    body[:pictures] = pictures

    body[:canada]       = Hash.new
    body[:canada][:mls] = inscription.no_inscription

    body[:france]                         = Hash.new
    body[:france][:dpe_indice]            = nil
    body[:france][:dpe_value]             = nil
    body[:france][:dpe_id]                = nil
    body[:france][:ges_indice]            = nil
    body[:france][:ges_value]             = nil
    body[:france][:alur_is_condo]         = nil
    body[:france][:alur_units]            = nil
    body[:france][:alur_uninhabitables]   = nil
    body[:france][:alur_spending]         = nil
    body[:france][:alur_legal_action]     = nil
    body[:france][:rent_honorary]         = nil
    body[:france][:mandat_exclusif]       = nil

    body[:belgium]                          = Hash.new
    body[:belgium][:peb_indice]             = nil
    body[:belgium][:peb_value]              = nil
    body[:belgium][:peb_id]                 = nil
    body[:belgium][:building_permit]        = nil
    body[:belgium][:done_assignments]       = nil
    body[:belgium][:preemption_property]    = nil
    body[:belgium][:subdivision_permits]    = nil
    body[:belgium][:sensitive_flood_area]   = nil
    body[:belgium][:delimited_flood_zone]   = nil
    body[:belgium][:risk_area]              = nil

    body[:other] = Hash.new
    body[:other][:orientation]          = Hash.new
    body[:other][:orientation][:north]  = nil
    body[:other][:orientation][:south]  = nil
    body[:other][:orientation][:east]   = nil
    body[:other][:orientation][:west]   = nil

    body[:other][:inclusion]                      = Hash.new
    body[:other][:inclusion][:air_conditioning]   = nil
    body[:other][:inclusion][:hot_water]          = nil
    body[:other][:inclusion][:heated]             = nil
    body[:other][:inclusion][:electricity]        = nil
    body[:other][:inclusion][:furnished]          = nil
    body[:other][:inclusion][:fridge]             = nil
    body[:other][:inclusion][:cooker]             = nil
    body[:other][:inclusion][:dishwasher]         = nil
    body[:other][:inclusion][:dryer]              = nil
    body[:other][:inclusion][:washer]             = nil
    body[:other][:inclusion][:microwave]          = nil

    body[:other][:detail]                       = Hash.new
    body[:other][:detail][:elevator]            = nil
    body[:other][:detail][:laundry]             = nil
    body[:other][:detail][:garbage_chute]       = nil
    body[:other][:detail][:common_space]        = nil
    body[:other][:detail][:janitor]             = nil
    body[:other][:detail][:gym]                 = nil
    body[:other][:detail][:golf]                = nil
    body[:other][:detail][:tennis]              = nil
    body[:other][:detail][:sauna]               = nil
    body[:other][:detail][:spa]                 = nil
    body[:other][:detail][:inside_pool]         = nil
    body[:other][:detail][:outside_pool]        = nil
    body[:other][:detail][:inside_parking]      = nil
    body[:other][:detail][:outside_parking]     = nil
    body[:other][:detail][:parking_on_street]   = nil
    body[:other][:detail][:garagebox]           = nil

    body[:other][:half_furnished]                 = Hash.new
    body[:other][:half_furnished][:living_room]   = nil
    body[:other][:half_furnished][:bedrooms]      = nil
    body[:other][:half_furnished][:kitchen]       = nil
    body[:other][:half_furnished][:other]         = nil

    body[:other][:indoor]                         = Hash.new
    body[:other][:indoor][:attic]                 = nil
    body[:other][:indoor][:attic_convertible]     = nil
    body[:other][:indoor][:attic_converted]       = nil
    body[:other][:indoor][:cellar]                = nil
    body[:other][:indoor][:central_vacuum]        = nil
    body[:other][:indoor][:entries_washer_dryer]  = nil
    body[:other][:indoor][:entries_dishwasher]    = nil
    body[:other][:indoor][:fireplace]             = nil
    body[:other][:indoor][:storage]               = nil
    body[:other][:indoor][:no_smoking]            = nil
    body[:other][:indoor][:double_glazing]        = nil
    body[:other][:indoor][:central_vacuum]        = nil
    body[:other][:indoor][:triple_glazing]        = nil

    body[:other][:floor]              = Hash.new
    body[:other][:floor][:carpet]     = nil
    body[:other][:floor][:wood]       = nil
    body[:other][:floor][:floating]   = nil
    body[:other][:floor][:ceramic]    = nil
    body[:other][:floor][:parquet]    = nil
    body[:other][:floor][:cushion]    = nil
    body[:other][:floor][:vinyle]     = nil
    body[:other][:floor][:lino]       = nil
    body[:other][:floor][:marble]     = nil

    body[:other][:exterior]                     = Hash.new
    body[:other][:exterior][:land_access]       = nil
    body[:other][:exterior][:back_balcony]      = nil
    body[:other][:exterior][:front_balcony]     = nil
    body[:other][:exterior][:private_patio]     = nil
    body[:other][:exterior][:storage]           = nil
    body[:other][:exterior][:terrace]           = nil
    body[:other][:exterior][:veranda]           = nil
    body[:other][:exterior][:garden]            = nil
    body[:other][:exterior][:sea_view]          = nil
    body[:other][:exterior][:mountain_view]     = nil

    body[:other][:accessibility]                      = Hash.new
    body[:other][:accessibility][:elevator]           = nil
    body[:other][:accessibility][:balcony]            = nil
    body[:other][:accessibility][:grab_bar]           = nil
    body[:other][:accessibility][:wider_corridors]    = nil
    body[:other][:accessibility][:lowered_switches]   = nil
    body[:other][:accessibility][:ramp]               = nil

    body[:other][:senior]                         = Hash.new
    body[:other][:senior][:autonomy]              = nil
    body[:other][:senior][:half_autonomy]         = nil
    body[:other][:senior][:no_autonomy]           = nil
    body[:other][:senior][:meal]                  = nil
    body[:other][:senior][:nursery]               = nil
    body[:other][:senior][:domestic_help]         = nil
    body[:other][:senior][:community]             = nil
    body[:other][:senior][:activities]            = nil
    body[:other][:senior][:validated_residence]   = nil
    body[:other][:senior][:housing_cooperative]   = nil

    body[:other][:pet]                    = Hash.new
    body[:other][:pet][:allow]            = nil
    body[:other][:pet][:cat]              = nil
    body[:other][:pet][:dog]              = nil
    body[:other][:pet][:little_dog]       = nil
    body[:other][:pet][:cage_aquarium]    = nil
    body[:other][:pet][:not_allow]        = nil

    body[:other][:service]              = Hash.new
    body[:other][:service][:internet]   = nil
    body[:other][:service][:tv]         = nil
    body[:other][:service][:tv_sat]     = nil
    body[:other][:service][:phone]      = nil

    body[:other][:composition]                    = Hash.new
    body[:other][:composition][:bar]              = nil
    body[:other][:composition][:living]           = nil
    body[:other][:composition][:dining]           = nil
    body[:other][:composition][:separate_toilet]  = nil
    body[:other][:composition][:open_kitchen]     = nil

    body[:other][:heating]                  = Hash.new
    body[:other][:heating][:electric]       = nil
    body[:other][:heating][:solar]          = nil
    body[:other][:heating][:gaz]            = nil
    body[:other][:heating][:condensation]   = nil
    body[:other][:heating][:fuel]           = nil
    body[:other][:heating][:heat_pump]      = nil
    body[:other][:heating][:floor_heating]  = nil

    body[:other][:transport]                      = Hash.new
    body[:other][:transport][:bicycle_path]       = nil
    body[:other][:transport][:public_transport]   = nil
    body[:other][:transport][:public_bike]        = nil
    body[:other][:transport][:subway]             = nil
    body[:other][:transport][:train_station]      = nil

    body[:other][:security]                     = Hash.new
    body[:other][:security][:guardian]          = nil
    body[:other][:security][:alarm]             = nil
    body[:other][:security][:intercom]          = nil
    body[:other][:security][:camera]            = nil
    body[:other][:security][:smoke_dectector]   = nil


    return body
  end



  ##============================================================##
  ## Centris update 08/12/2015
  ##============================================================##
  task update_2015_dec: :environment do
    puts "centris:update_20151208 - started"
    start = Time.now
    ##============================================================##
    ## Update Centris Property Classifications
    ##============================================================##
    puts "="*30
    puts "Start CentrisPropertyClassification"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_property_classifications")
    puts "CentrisPropertyClassification Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2015/GENRES_PROPRIETES.csv",:headers => true) do |row|
      c = CentrisPropertyClassification.new
      c.categorie_propriete           = row["categorie_propriete".upcase]
      c.genre_propriete               = row["genre_propriete".upcase]
      c.description_abregee_francaise = row["description_abregee_francaise".upcase]
      c.description_francaise         = row["description_francaise".upcase]
      c.description_abregee_anglaise  = row["description_abregee_anglaise".upcase]
      c.description_anglaise          = row["description_anglaise".upcase]
      batch << c
    end
    CentrisPropertyClassification.import(batch)
    puts "CentrisPropertyClassification OK => (#{(Time.now-start)} s)"


    ##============================================================##
    ## Update Centris Fix Values
    ##============================================================##
    puts "="*30
    puts "Start CentrisFixValue"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_fix_values")
    puts "CentrisFixValue Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2015/VALEURS_FIXES.csv",:headers => true) do |row|
      c = CentrisFixValue.new
      c.domaine                       = row["domaine".upcase]
      c.valeur                        = row["valeur".upcase]
      c.description_abregee_francaise = row["description_abregee_francaise".upcase]
      c.description_abregee_anglaise  = row["description_abregee_anglaise".upcase]
      c.description_francaise         = row["description_francaise".upcase]
      c.description_anglaise          = row["description_anglaise".upcase]
      batch << c
    end
    CentrisFixValue.import(batch)
    puts "CentrisFixValue OK => (#{(Time.now-start)} s)"

    ##============================================================##
    ## Update Centris Features SubTypes
    ##============================================================##
    puts "="*30
    puts "Start CentrisFeatureSubtype"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_feature_subtypes")
    puts "CentrisFeatureSubtype Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2015/SOUS_TYPE_CARACTERISTIQUES.csv",:headers => true) do |row|
      c = CentrisFeatureSubtype.new
      c.tcar_code                      = row["tcar_code".upcase]
      c.code                           = row["code".upcase]
      c.description_abregee_francaise  = row["description_abregee_francaise".upcase]
      c.description_francaise          = row["description_francaise".upcase]
      c.description_abregee_anglaise   = row["description_abregee_anglaise".upcase]
      c.description_anglaise           = row["description_anglaise".upcase]
      batch << c
    end
    CentrisFeatureSubtype.import(batch)
    puts "CentrisFeatureSubtype OK => (#{(Time.now-start)} s)"

    ##============================================================##
    ## Update Centris Cities
    ##============================================================##
    puts "="*30
    puts "Start CentrisCity"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_cities")
    puts "CentrisCity Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2015/MUNICIPALITES.csv",:headers => true) do |row|
      c = CentrisCity.new
      c.code          = row["code".upcase]
      c.description   = row["description".upcase]
      c.region_code   = row["region_code".upcase]
      batch << c
    end
    CentrisCity.import(batch)
    puts "CentrisCity OK => (#{(Time.now-start)} s)"

    ##============================================================##
    ## Update Centris Neighborhoods
    ##============================================================##
    puts "="*30
    puts "Start CentrisNeighborhood"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_neighborhoods")
    puts "CentrisNeighborhood Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2015/QUARTIERS.csv",:headers => true) do |row|
      c = CentrisNeighborhood.new
      c.mun_code              = row["mun_code".upcase]
      c.code                  = row["code".upcase]
      c.description_francaise = row["description_francaise".upcase]
      c.description_anglaise  = row["description_anglaise".upcase]
      batch << c
    end
    CentrisNeighborhood.import(batch)
    puts "CentrisNeighborhood OK => (#{(Time.now-start)} s)"
  end


  ##============================================================##
  ## Centris update 08/12/2016
  ##============================================================##
  task update_2016_dec: :environment do
    puts "centris:update_20151208 - started"
    start = Time.now

    ##============================================================##
    ## Update Centris Fix Values
    ##============================================================##
    puts "="*30
    puts "Start CentrisFixValue"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_fix_values")
    puts "CentrisFixValue Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2016/VALEURS_FIXES.csv",:headers => true) do |row|
      c = CentrisFixValue.new
      c.domaine                       = row["domaine".upcase]
      c.valeur                        = row["valeur".upcase]
      c.description_abregee_francaise = row["description_abregee_francaise".upcase]
      c.description_abregee_anglaise  = row["description_abregee_anglaise".upcase]
      c.description_francaise         = row["description_francaise".upcase]
      c.description_anglaise          = row["description_anglaise".upcase]
      batch << c
    end
    CentrisFixValue.import(batch)
    puts "CentrisFixValue OK => (#{(Time.now-start)} s)"

    ##============================================================##
    ## Update Centris Features SubTypes
    ##============================================================##
    puts "="*30
    puts "Start CentrisFeatureSubtype"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_feature_subtypes")
    puts "CentrisFeatureSubtype Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2016/SOUS_TYPE_CARACTERISTIQUES.csv",:headers => true) do |row|
      c = CentrisFeatureSubtype.new
      c.tcar_code                      = row["tcar_code".upcase]
      c.code                           = row["code".upcase]
      c.description_abregee_francaise  = row["description_abregee_francaise".upcase]
      c.description_francaise          = row["description_francaise".upcase]
      c.description_abregee_anglaise   = row["description_abregee_anglaise".upcase]
      c.description_anglaise           = row["description_anglaise".upcase]
      batch << c
    end
    CentrisFeatureSubtype.import(batch)
    puts "CentrisFeatureSubtype OK => (#{(Time.now-start)} s)"

    ##============================================================##
    ## Update Centris Cities
    ##============================================================##
    puts "="*30
    puts "Start CentrisCity"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_cities")
    puts "CentrisCity Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2016/MUNICIPALITES.csv",:headers => true) do |row|
      c = CentrisCity.new
      c.code          = row["code".upcase]
      c.description   = row["description".upcase]
      c.region_code   = row["region_code".upcase]
      batch << c
    end
    CentrisCity.import(batch)
    puts "CentrisCity OK => (#{(Time.now-start)} s)"

    ##============================================================##
    ## Update Centris Neighborhoods
    ##============================================================##
    puts "="*30
    puts "Start CentrisNeighborhood"
    batch = Array.new
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE centris_neighborhoods")
    puts "CentrisNeighborhood Deleted => (#{(Time.now-start)} s)"
    CSV.foreach("#{Rails.root}/datas/centris_dec_2016/QUARTIERS.csv",:headers => true) do |row|
      c = CentrisNeighborhood.new
      c.mun_code              = row["mun_code".upcase]
      c.code                  = row["code".upcase]
      c.description_francaise = row["description_francaise".upcase]
      c.description_anglaise  = row["description_anglaise".upcase]
      batch << c
    end
    CentrisNeighborhood.import(batch)
    puts "CentrisNeighborhood OK => (#{(Time.now-start)} s)"
  end

end
