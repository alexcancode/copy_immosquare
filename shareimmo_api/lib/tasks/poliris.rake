# encoding: utf-8

require 'zip'
require 'httparty'
require 'active_support/core_ext/hash/conversions'

namespace :poliris do


  task import: :environment do
    begin
      clients  = [{
        :id             => "heytienne",
        :shareimmo_id   => 6989
      }
    ]

    apiKeys           = {"ApiKey" => ENV['poliris_api_key'],"ApiToken" => ENV['poliris_api_token']}
    apiKeysShareimmo  = {"ApiKey" => ENV['shareimmo_api_key'],"ApiToken" => ENV['shareimmo_api_token']}


    clients.each do |client|
      FileUtils.rm_rf(Dir.glob("#{Rails.root}/import/poliris/#{client[:id]}/*"))
      folder_destination        = "#{Rails.root}/import/poliris/#{client[:id]}/#{Time.now.strftime("%y-%m-%d")}"
      account                   = ImportUser.where(:status=>1,:import_provider => client[:id]).first
      ftp_path                  = "pictures/poliris/#{client[:id]}"

        ##======================================================##
        ## CREATE DIRECTORY (import/poliris/heytienne/16-09-14)
        ##======================================================##
        FileUtils.mkdir_p(folder_destination)
        puts "Poliris - #{client[:id]} : started"



        ##======================================================##
        ## DOWNLOAD ZIP FILE (import/poliris/heytienne/16-09-14/heytienne_shareimmo.zip)
        ##======================================================##
        puts "Poliris - #{client[:id]} : Download Zip file"
        zipnames = []
        Net::FTP.open(account.ftp_domain, account.ftp_user, account.ftp_password) do |ftp|
          files = ftp.nlst("*.ZIP")
          files.each do |file|
            name =  File.basename(file)
            zipnames << name
            ftp.getbinaryfile(file,File.join(folder_destination,name))
          end if files.present?
          ftp.close
        end


        ##======================================================##
        ## UNZIP FILE (import/poliris/heytienne/16-09-14/unziphere)
        ##======================================================##
        puts "Poliris - #{client[:id]} : Unzip file"
        zipnames.each do |file|
          file = "#{folder_destination}/#{file}"
          Zip::File.open(file) do |zip_file|
            zip_file.each do |entry|
              entry.extract(File.join(folder_destination, entry.name))
            end
          end
          FileUtils.rm(file)
        end


        ##======================================================##
        ## CLEAR FTP FOLDER
        ## Il faut créer le dossier manuellement sur le FTP
        ## à améliorer...
        ##======================================================##
        puts "Poliris - #{client[:id]} : Clear FTP  ftp://#{ENV['apps_ftp_host']}/#{ftp_path}"
        Net::FTP.open(ENV['apps_ftp_host'],ENV['apps_ftp_username'],ENV['apps_ftp_password'] ) do |ftp|
          ftp.passive = true
          begin
            ftp.chdir(ftp_path)
            ftp.list("*").each do |ftpfile|
              file = ftpfile.split(/\s+/).last
              ftp.delete file
            end
          rescue
            # TODO
          end
        end


        ##======================================================##
        ## Upload Images to ftp
        ##======================================================##
        puts "Poliris - #{client[:id]} : Upload images to ftp://#{ENV['apps_ftp_host']}"
        pictures_tmp1 = {}
        xml_file      = nil
        picture_date  = Time.now.strftime("%y-%m-%d-%H-%M")
        Dir.foreach(folder_destination) do |file|
          next if file == '.' or file == '..'
          ext = File.extname(file)
          if ext.present? && [".jpg",".jpeg",".png"].include?(ext.downcase)
            new_filename = "#{picture_date}-#{file}"
            Net::FTP.open( ENV['apps_ftp_host'], ENV['apps_ftp_username'], ENV['apps_ftp_password'] ) do |ftp|
              ftp.passive = true
              ftp.chdir(ftp_path)
              ftp.putbinaryfile("#{folder_destination}/#{file}",new_filename)
            end
            file_arr = file.to_s.split("-")
            pictures_tmp1[file_arr[2]] = [] if pictures_tmp1[file_arr[2]].nil?
            pictures_tmp1[file_arr[2]] << new_filename
          end
          xml_file = file if ext.present? && [".xml"].include?(ext.downcase)
        end


        ##============================================================##
        ## Remove olds properties
        ##============================================================##
        properties_online     = []
        properties_to_keep    = []
        call = HTTParty.get("#{ENV['api_shareimmo_base']}/search/properties?&user=#{client[:shareimmo_id]}&per_page=300",{:headers => apiKeysShareimmo})
        raise call.body if call.code != 200
        call = JSON.parse(call.body)
        call["properties"].each do |p|
          properties_online << p["_source"]["uid"]
        end


        ##======================================================##
        ## Properties Management
        ##======================================================##
        xml_file      = File.open("#{folder_destination}/#{xml_file}") { |f| Nokogiri::XML(f) }
        hash          = Hash.from_xml(xml_file.to_s)

        hash["BIENS"]["BIEN"].each_with_index do |property|
          uid = "#{property["CODE_SOCIETE"]}-#{property["NO_ASP"]}"
          puts "="*30
          puts "Annonce ##{uid}"
          properties_to_keep << uid

          ##============================================================##
          ## Classification
          ##============================================================##
          case property["TYPE_OFFRE"]
          when "1"
            classification_id = 201
            listing_id        = 1
          when "11"
            classification_id = 201
            listing_id        = 2
          when "2"
            classification_id = 101
            listing_id        = 1
          when "12"
            classification_id = 101
            listing_id        = 2
          when "3"
            classification_id = 401
            listing_id        = 1
          when "4"
            classification_id = 501
            listing_id        = 1
          when "5"
            classification_id = 119
            listing_id        = 1
          when "6"
            classification_id = 801
            listing_id        = 1
          when "7"
            classification_id = 812
            listing_id        = 1
          when "12"
            classification_id = 101
            listing_id        = 2
          when "13"
            classification_id = 119
            listing_id        = 2
          when "14"
            classification_id = 812
            listing_id        = 2
          else
            classification_id = 101
            listing_id        = 1
          end

          ##============================================================##
          ## API
          ##============================================================##
          body                      = Hash.new
          body[:uid]                = uid
          body[:account_uid]        = client[:id]
          body[:building_uid]       = nil
          body[:online]             = 1
          body[:classification_id]  = classification_id
          body[:status_id]          = property["ANNEE_CONS"].to_i >= Time.now.year ? 1 : 2
          body[:listing_id]         = listing_id
          body[:unit]               = property["NO_DOSSIER"]
          body[:level]              = property["ETAGE"]
          body[:cadastre]           = nil
          body[:flag_id]            = property["COUP_DE_COEUR"] == "Oui" ? 4 : nil
          body[:year_of_built]      = property["ANNEE_CONS"]
          body[:availability_date]  = property["DATE_DISPO"]
          body[:is_luxurious]       = property["IMMEUBLEPRESTIGE"]

          body[:address]                                = Hash.new
          body[:address][:address_visibility]           = 0
          body[:address][:address_formatted]            = "#{property["ADRESSE1_OFFRE"]} #{property["ADRESSE2_OFFRE"]}, #{property["CP_OFFRE"]}, #{property["VILLE_OFFRE"]}"
          body[:address][:street_number]                = nil
          body[:address][:street_name]                  = property["ADRESSE1_OFFRE"]
          body[:address][:zipcode]                      = property["CP_OFFRE"]
          body[:address][:locality]                     = property["VILLE_OFFRE"]
          body[:address][:sublocality]                  = property["QUARTIER"] || property["SECTEUR"]
          body[:address][:administrative_area_level1]   = nil
          body[:address][:administrative_area_level2]   = nil
          body[:address][:country]                      = "FR"
          body[:address][:latitude]                     = nil
          body[:address][:longitude]                    = nil


          body[:area]             = Hash.new
          body[:area][:living]    = property["SURF_HAB"]
          body[:area][:balcony]   = nil
          body[:area][:land]      = property["SURF_TERRAIN"]
          body[:area][:unit_id]   = 1


          body[:rooms]                = Hash.new
          body[:rooms][:rooms]        = property["NB_PIECES"]
          body[:rooms][:bathrooms]    = property["NB_SDB"]
          body[:rooms][:bedrooms]     = property["NB_CHAMBRES"]
          body[:rooms][:showerrooms]  = property["NB_SE"]
          body[:rooms][:toilets]      = property["NB_WC"]


          body[:title]         = Hash.new
          body[:title][:fr]    = "#{listing_id == 1 ? "À vendre" : "À louer"} #{property["CATEGORIE"]} | #{property["VILLE_OFFRE"]}"
          body[:title][:en]    = nil
          body[:title][:de]    = nil
          body[:title][:nl]    = nil
          body[:title][:it]    = nil
          body[:title][:es]    = nil
          body[:title][:pt]    = nil


          body[:description_short]         = Hash.new
          body[:description_short][:fr]    = property["TEXTE_FR"]
          body[:description_short][:en]    = property["TEXTE_UK"]
          body[:description_short][:de]    = property["TEXTE_GER"]
          body[:description_short][:nl]    = nil
          body[:description_short][:it]    = nil
          body[:description_short][:es]    = property["TEXTE_SP"]
          body[:description_short][:pt]    = nil


          body[:description]         = Hash.new
          body[:description][:fr]    = property["TEXTE_FR"]
          body[:description][:en]    = property["TEXTE_UK"]
          body[:description][:de]    = property["TEXTE_GER"]
          body[:description][:nl]    = nil
          body[:description][:it]    = nil
          body[:description][:es]    = property["TEXTE_SP"]
          body[:description][:pt]    = nil


          body[:url]         = Hash.new
          body[:url][:fr]    = property["URL_VISITE"]
          body[:url][:en]    = nil
          body[:url][:de]    = nil
          body[:url][:nl]    = nil
          body[:url][:it]    = nil
          body[:url][:es]    = nil
          body[:url][:pt]    = nil


          body[:financial]                  = Hash.new
          body[:financial][:price]          = property["PRIX"]
          body[:financial][:price_with_tax] = 1
          body[:financial][:rent]           = property["PRIX"]
          body[:financial][:fees]           = property["CHARGES"]
          body[:financial][:rent_frequency] = nil
          body[:financial][:tax1_annual]    = property["TAXE_HABITATION"]
          body[:financial][:tax2_annual]    = property["TAXE_FONCIERE"]
          body[:financial][:income_annual]  = nil
          body[:financial][:deposit]        = property["DEPOT_GARANTIE"]
          body[:financial][:currency]       = "EUR"

          body[:pictures]  = []

          body[:canada]       = Hash.new
          body[:canada][:mls] = nil

          body[:france]                         = Hash.new
          body[:france][:dpe_indice]            = property["DPE_ETIQ1"]
          body[:france][:dpe_value]             = property["DPE_VAL1"]
          body[:france][:dpe_id]                = nil
          body[:france][:ges_indice]            = property["DPE_ETIQ2"]
          body[:france][:ges_value]             = property["DPE_VAL2"]
          body[:france][:alur_is_condo]         = property["COPROPRIETE"] == "Oui" ? 1 : nil
          body[:france][:alur_units]            = property["NB_LOTS_COPROPRIETE"]
          body[:france][:alur_uninhabitables]   = nil
          body[:france][:alur_spending]         = property["MONTANT_QUOTE_PART"]
          body[:france][:alur_legal_action]     = property["PROCEDURE_SYNDICAT"] == "Oui" ? 1 : nil
          body[:france][:rent_honorary]         = property["HONORAIRES"]
          body[:france][:mandat_exclusif]       = property["TYPE_MANDAT"] == "exclusif" ? 1 : nil


          body[:belgium]                          = Hash.new
          body[:belgium][:peb_indice]             = nil
          body[:belgium][:peb_value]              = nil
          body[:belgium][:peb_id]                 = nil
          body[:belgium][:building_permit]        = nil
          body[:belgium][:done_assignments]       = nil
          body[:belgium][:preemption_property]    = nil
          body[:belgium][:subdivision_permits]    = nil
          body[:belgium][:sensitive_flood_area]   = nil
          body[:belgium][:delimited_flood_zone]   = nil
          body[:belgium][:risk_area]              = nil


          body[:other] = Hash.new
          body[:other][:orientation]          = Hash.new
          body[:other][:orientation][:north]  = nil
          body[:other][:orientation][:south]  = nil
          body[:other][:orientation][:east]   = nil
          body[:other][:orientation][:west]   = nil


          body[:other][:inclusion]                      = Hash.new
          body[:other][:inclusion][:air_conditioning]   = property["CLIMATISATION"] == "Vrai" ? 1 : nil
          body[:other][:inclusion][:hot_water]          = nil
          body[:other][:inclusion][:heated]             = nil
          body[:other][:inclusion][:electricity]        = nil
          body[:other][:inclusion][:furnished]          = nil
          body[:other][:inclusion][:fridge]             = nil
          body[:other][:inclusion][:cooker]             = nil
          body[:other][:inclusion][:dishwasher]         = nil
          body[:other][:inclusion][:dryer]              = nil
          body[:other][:inclusion][:washer]             = nil
          body[:other][:inclusion][:microwave]          = nil


          body[:other][:detail]                       = Hash.new
          body[:other][:detail][:elevator]            = property["ASCENSEUR"] == "Oui" ?  1 : nil
          body[:other][:detail][:laundry]             = nil
          body[:other][:detail][:garbage_chute]       = nil
          body[:other][:detail][:common_space]        = nil
          body[:other][:detail][:janitor]             = nil
          body[:other][:detail][:gym]                 = nil
          body[:other][:detail][:golf]                = nil
          body[:other][:detail][:tennis]              = nil
          body[:other][:detail][:sauna]               = nil
          body[:other][:detail][:spa]                 = nil
          body[:other][:detail][:inside_pool]         = nil
          body[:other][:detail][:outside_pool]        = property["PISCINE"] == "Oui" ?  1 : nil
          body[:other][:detail][:inside_parking]      = (property["NB_PARK_INT"].present? and property["NB_PARK_INT"] != "0") ? 1 : nil
          body[:other][:detail][:outside_parking]     = (property["NB_PARK_EXT"].present? and property["NB_PARK_EXT"] != "0") ? 1 : nil
          body[:other][:detail][:parking_on_street]   = nil
          body[:other][:detail][:garagebox]           = (property["GARAGE_BOX"].present? and property["GARAGE_BOX"] != "0") ? 1 : nil



          body[:other][:half_furnished]                 = Hash.new
          body[:other][:half_furnished][:living_room]   = nil
          body[:other][:half_furnished][:bedrooms]      = nil
          body[:other][:half_furnished][:kitchen]       = nil
          body[:other][:half_furnished][:other]         = nil



          body[:other][:indoor]                         = Hash.new
          body[:other][:indoor][:attic]                 = nil
          body[:other][:indoor][:attic_convertible]     = nil
          body[:other][:indoor][:attic_converted]       = nil
          body[:other][:indoor][:cellar]                = property["SOUS_SOL"].present? ? 1 : nil
          body[:other][:indoor][:central_vacuum]        = nil
          body[:other][:indoor][:entries_washer_dryer]  = nil
          body[:other][:indoor][:entries_dishwasher]    = nil
          body[:other][:indoor][:fireplace]             = nil
          body[:other][:indoor][:storage]               = nil
          body[:other][:indoor][:no_smoking]            = nil
          body[:other][:indoor][:double_glazing]        = nil
          body[:other][:indoor][:central_vacuum]        = nil
          body[:other][:indoor][:triple_glazing]        = nil



          body[:other][:floor]              = Hash.new
          body[:other][:floor][:carpet]     = nil
          body[:other][:floor][:wood]       = nil
          body[:other][:floor][:floating]   = nil
          body[:other][:floor][:ceramic]    = nil
          body[:other][:floor][:parquet]    = nil
          body[:other][:floor][:cushion]    = nil
          body[:other][:floor][:vinyle]     = nil
          body[:other][:floor][:lino]       = nil
          body[:other][:floor][:marble]     = nil



          body[:other][:exterior]                     = Hash.new
          body[:other][:exterior][:land_access]       = nil
          body[:other][:exterior][:back_balcony]      = (property["BALCON"].present? and property["BALCON"] != "0") ? 1 : nil
          body[:other][:exterior][:front_balcony]     = (property["BALCON"].present? and property["BALCON"] != "0") ? 1 : nil
          body[:other][:exterior][:private_patio]     = nil
          body[:other][:exterior][:storage]           = nil
          body[:other][:exterior][:terrace]           = (property["TERRASSE"].present? and property["TERRASSE"] != "0") ? 1 : nil
          body[:other][:exterior][:veranda]           = nil
          body[:other][:exterior][:garden]            = nil
          body[:other][:exterior][:sea_view]          = nil
          body[:other][:exterior][:mountain_view]     = nil


          body[:other][:accessibility]                      = Hash.new
          body[:other][:accessibility][:elevator]           = property["ACCES_HANDI"] == "Oui" ? 1 : nil
          body[:other][:accessibility][:balcony]            = property["ACCES_HANDI"] == "Oui" ? 1 : nil
          body[:other][:accessibility][:grab_bar]           = property["ACCES_HANDI"] == "Oui" ? 1 : nil
          body[:other][:accessibility][:wider_corridors]    = property["ACCES_HANDI"] == "Oui" ? 1 : nil
          body[:other][:accessibility][:lowered_switches]   = property["ACCES_HANDI"] == "Oui" ? 1 : nil
          body[:other][:accessibility][:ramp]               = property["ACCES_HANDI"] == "Oui" ? 1 : nil


          body[:other][:senior]                         = Hash.new
          body[:other][:senior][:autonomy]              = nil
          body[:other][:senior][:half_autonomy]         = nil
          body[:other][:senior][:no_autonomy]           = nil
          body[:other][:senior][:meal]                  = nil
          body[:other][:senior][:nursery]               = nil
          body[:other][:senior][:domestic_help]         = nil
          body[:other][:senior][:community]             = nil
          body[:other][:senior][:activities]            = nil
          body[:other][:senior][:validated_residence]   = nil
          body[:other][:senior][:housing_cooperative]   = nil



          body[:other][:pet]                    = Hash.new
          body[:other][:pet][:allow]            = nil
          body[:other][:pet][:cat]              = nil
          body[:other][:pet][:dog]              = nil
          body[:other][:pet][:little_dog]       = nil
          body[:other][:pet][:cage_aquarium]    = nil
          body[:other][:pet][:not_allow]        = nil



          body[:other][:service]              = Hash.new
          body[:other][:service][:internet]   = nil
          body[:other][:service][:tv]         = nil
          body[:other][:service][:tv_sat]     = nil
          body[:other][:service][:phone]      = nil


          body[:other][:composition]                    = Hash.new
          body[:other][:composition][:bar]              = nil
          body[:other][:composition][:living]           = nil
          body[:other][:composition][:dining]           = nil
          body[:other][:composition][:separate_toilet]  = nil
          body[:other][:composition][:open_kitchen]     = nil


          body[:other][:heating]                  = Hash.new
          body[:other][:heating][:electric]       = nil
          body[:other][:heating][:solar]          = nil
          body[:other][:heating][:gaz]            = nil
          body[:other][:heating][:condensation]   = nil
          body[:other][:heating][:fuel]           = nil
          body[:other][:heating][:heat_pump]      = nil
          body[:other][:heating][:floor_heating]  = nil


          body[:other][:transport]                      = Hash.new
          body[:other][:transport][:bicycle_path]       = nil
          body[:other][:transport][:public_transport]   = nil
          body[:other][:transport][:public_bike]        = nil
          body[:other][:transport][:subway]             = nil
          body[:other][:transport][:train_station]      = nil


          body[:other][:security]                     = Hash.new
          body[:other][:security][:guardian]          = property["GARDIENNAGE"] == "Vrai" ?  1 : nil
          body[:other][:security][:alarm]             = nil
          body[:other][:security][:intercom]          = property["INTERPHONE"] == "Vrai" ?  1 : nil
          body[:other][:security][:camera]            = nil
          body[:other][:security][:smoke_dectector]   = nil


          pictures        = []
          pictures_url    = "http://apps.shareimmo.com/#{ftp_path}"
          pictures_tmp2   = pictures_tmp1[property["NO_ASP"]]
          pictures_tmp2.sort_by{|e|e}.each do |picture_file|
            pictures << { :url => "#{pictures_url}/#{picture_file}" }
          end if pictures_tmp2.present? && pictures_tmp2.count > 0

          puts pictures
          body[:pictures] = pictures

          property_post = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",{:headers => apiKeys ,:body => body})
          property_post = JSON.parse(property_post.body)
        end



        ##============================================================##
        ## Deactivate old properties not in feed
        ##============================================================##
        properties_to_remove = properties_online - properties_to_keep
        properties_to_remove.each do |uid|
          puts "Deactivate property #{uid}"
          body               = Hash.new
          body[:uid]         = uid
          body[:account_uid] = client[:id]
          body[:online]      = 0
          property_post = HTTParty.put("#{ENV['api_shareimmo_base']}/properties",{:headers => apiKeys ,:body => body})
          property_post = JSON.parse(property_post.body)
        end


      end

    rescue Exception => e
      JulesLogger.info e.message
      JulesLogger.info e.backtrace
    end

  end
end


