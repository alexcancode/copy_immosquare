namespace :youtube do


  task clean_video_folder: :environment do
    FileUtils.rm_rf(File.join("public","videos"))
  end

end
