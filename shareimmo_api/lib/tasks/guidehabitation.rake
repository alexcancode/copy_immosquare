require 'net/ftp'

namespace :guidehabitation do

  task to_shareimmo: :environment do
    FileUtils.rm_rf(Dir.glob(File.join(Rails.root,'import','guidehabitation','*')))
    folder_destination = File.join(Rails.root,'import','guidehabitation',"#{Time.now.strftime("%y-%m-%d")}")
    FileUtils.mkdir_p(folder_destination)
    account = ImportUser.where(:status=>1,:import_provider=>'guidehabitation').first
    #============================================================##
    # Connexion au FTP -> on récupère tout les comptes actifs
    # -> puis on les transferts vers notre dossier temporaire
    #============================================================##
    Net::FTP.open(account.ftp_domain,account.ftp_user,account.ftp_password) do |ftp|
      files = ftp.nlst("*.xml")
      unless files.blank?
        ftp.getbinaryfile(files.last,File.join(folder_destination, File.basename(files.last)))
      end
      ftp.close
    end

    Dir.foreach(folder_destination) do |file|
      next if file == '.' or file == '..'
      f = File.open(File.join(folder_destination,file))
      @xml = Nokogiri::XML(f,nil, 'UTF-8')
      f.close
    end

      accounts = Array.new
      @xml.css("account").each do |account|
        accounts << {
          :uid => account.css("uid").first.content,
          :username => account.css('username').first.content,
          :email => account.css("email").first.content,
          :infos =>{
            :fullName => account.css('public_name').first.content,
            :firstName => nil,
            :lastName => nil,
            :phone=> account.css('phone').first.content,
            :avatarUrl => account.css('picture').first.content,
            :spokenLanguages => account.css('spoken_languages').first.content,
            :job => nil,
            :bio => nil,
            :country => account.css('country').first.content,
            :socials => {
              :facebook => nil,
              :twitter => nil,
              :linkedin => nil,
              :pinterest => nil,
              :googlePlus => nil
            },
          },
          :offeredBy => account.css('is_a_pro_user').first.content == 1 ? "pro" : "owner",
          :proInfos => {
            :website => account.css('website').first.content,
            :logoUrl =>  account.css('logo').first.content,
            :companyName => account.css('agency_name').first.content,
            :companyAddress => account.css('agency_address').first.content,
            :companyCity => account.css('agency_city').first.content,
            :companyZipcode => account.css('agency_zipcode').first.content,
            :companyProvince => account.css('agency_province').first.content,
            :companyCountry => "CA"
          },
          :services =>{
            :miniWebSite => 0,
            :international => 1,
            :youtube => 1,
            :kijiji => 1,
            :solihome => 0
          }
        }
      end

      properties = Array.new
      @xml.css("property").each do |property|

        @geocoder = Geocoder.search("#{property.css('latitude').first.content.to_f},#{property.css('longitude').first.content.to_f}").first

        pictures = Array.new
        property.css("photo").each do |picture|
          pictures << {:url => picture.content}
        end
        properties << {
          :uid=>property.css('uid').first.content,
          :accountUid => property.css('account_uid').first.content,
          :status => 1,
          :mls => property.css('mls').first.content,
          :geo =>{
            :geocoding =>{
              :address => property.css('address').first.content,
              :latitude =>  property.css('latitude').first.content.to_f,
              :longitude => property.css('longitude').first.content.to_f,
            },
            :streetNumber =>  (@geocoder.present? and @geocoder.data.present?) == true ?  @geocoder.data["address"]["house_number"] : nil,
            :streetName => (@geocoder.present? and @geocoder.data.present?) ?  @geocoder.data["address"]["road"] : nil,
            :subLocality => (@geocoder.present? and  @geocoder.data.present?) ?  @geocoder.data["address"]["suburb"]: nil,
            :locality => (@geocoder.present? and  @geocoder.data.present?) ?  @geocoder.data["address"]["city"]: nil,
            :zipcode => (@geocoder.present? and  @geocoder.data.present?) ?  @geocoder.data["address"]["postcode"]: nil,
            :province => (@geocoder.present? and  @geocoder.data.present?) ?  @geocoder.data["address"]["state"]: nil,
            :country => (@geocoder.present? and  @geocoder.data.present?) ?  @geocoder.data["address"]["country_code"]: nil,
            :cadastre => nil,
            :unit=> property.css('unit').first.content,
            :level => nil,
          },
          :building => {
            :uid => nil,
            :name => nil,
            :NumberOfLevels => nil,
            :NumberOfUnits => nil,
            :streetNumber => nil,
            :streetName => nil,
            :zipcode => nil,
            :city =>nil,
            :province => nil,
            :country => nil
          },
          :title =>{
            :fr => property.css("titles title[language='fr']").first.content,
            :en => property.css("titles title[language='en']").first.content
          },
          :description =>{
            :fr => property.css("descriptions description[language='fr']").first.content,
            :en => property.css("descriptions description[language='en']").first.content
          },
          :url =>{
            :fr => property.css("property_urls property_url[language='fr']").first.content,
            :en => property.css("property_urls property_url[language='en']").first.content
          },
          "area" =>{
            :living => property.css('area').first.content,
            :land  => nil,
            :unit => property.css('area_unit').first.content == 1 ? "pc" : "mc" ,
          },
          :rooms =>{
            :rooms=> property.css('rooms').first.content,
            :withHalf => "yes",
            :bedrooms=> property.css('bedrooms').first.content,
            :bathrooms=> property.css('bathrooms').first.content,
            :showerrooms => nil,
          },
          :financial =>{
            :price => property.css('price_from').first.content,
            :price_from => property.css('price_from').first.content,
            :availableDate => nil,
            :rentInfos =>{
              :rent=> nil,
              :fees => property.css('condo_fees').first.content,
              :frequency => 30
            },
            :annual =>{
              :tax_1 => property.css('tax_1').first.content,
              :tax_2 => property.css('tax_2').first.content,
              :income => nil
            },
            :currency => property.css('currency').first.content,
          },
          :infos =>{
            :propertyListing => property.css('property_listing').first.content,
            :propertyClassification=> property.css('property_classification').first.content,
            :propertyStatus => property.css('property_status').first.content,
            :propertyFlag => nil,
            :yearOfConstruction => property.css('year_of_build').first.content,
          },
          :pictures => pictures,
          :videos => nil,
          :inclusions => {
            :airConditioning => nil,
            :hotWater => nil,
            :heated => nil,
            :electricity => nil,
            :furnished => nil,
            :fridge => nil,
            :cooker => nil,
            :dishwasher => nil,
            :dryer => nil,
            :washer => nil,
            :microwave=> nil
          },
          :details => {
            :elevator => nil,
            :laundry => nil,
            :garbageChute => nil,
            :commonSpace => nil,
            :janitor => nil,
            :gym => nil,
            :sauna => nil,
            :spa => nil,
            :insidePool => nil,
            :outsidePool => nil,
            :insideParking => nil,
            :outsideParking => nil,
            :parkingOnStreet => nil,
            :petsAllow => nil
          },
          :visibility =>{
            :international => property.css('visibility international').first.content,
            :youtube => property.css('visibility youtube').first.content,
            :canada =>{
              :kijiji => property.css('visibility kijiji').first.content,
              :kijijiBump => 0,
              :lespac => nil,
              :craigslist => nil,
              :louer_com => nil
            }
          }
        }
      end

      HTTParty.post("#{ENV['api_base']}/registrations",{
        :body => {
          :credentials => {
            :apiKey => '303432rfsmvsfgv342345fsmtwe57',
            :apiToken => 'rwe23z1287x231sdggb23as12dczx'
          },
          :accounts => accounts
        }}
      )

      HTTParty.post("#{ENV['api_base']}/properties",{
        :body => {
          :credentials => {
            :apiKey => '303432rfsmvsfgv342345fsmtwe57',
            :apiToken => 'rwe23z1287x231sdggb23as12dczx'
          },
          :properties => properties
        }}
      )


      api_provider  = ApiProvider.where(:name=>"guidehabitation").select(:id).first
      ApiMonitoring.create(:api_provider_id=>api_provider.id,:ads_count=>properties.size)

      puts "Sync OK => #{Time.now}"

  end
end
