namespace :evosys do


  task :import => :environment do
    puts "Start import Evosys"
    ##============================================================##
    ## Setup
    ##============================================================##
    start               = Time.now
    apiKeys             = {"ApiKey" => ENV['evosys_api_key'],"ApiToken" => ENV['evosys_api_token'], :flowName => "evosys"}
    folder_destination  = "#{Rails.root}/import/evosys/#{Time.now.strftime("%y-%m-%d")}"
    FileUtils.rm_rf(Dir.glob(File.join(Rails.root,'import',"evosys",'*')))
    FileUtils.mkdir_p(folder_destination)


    ##============================================================##
    ## Import files
    ##============================================================##
    puts "Download files from FTP"
    account = ImportUser.where(:status=>1,:import_provider=>"evosys").first
    Net::FTP.open(account.ftp_domain,account.ftp_user,account.ftp_password) do |ftp|
      files = ftp.nlst("*.xml")
      unless files.blank?
        files.each do |file|
          ftp.getbinaryfile(file,File.join(folder_destination, File.basename(file)))
        end
      end
      ftp.close
    end


    ##============================================================##
    ## For each XML File (each property)
    ##============================================================##
    puts "Start parse each File"
    Dir.foreach(folder_destination) do |file|
      begin
        next if file == '.' or file == '..'
        puts "="*30
        puts file
        f               = File.open("#{folder_destination}/#{file}"){ |f| Nokogiri::XML(f)}
        xml             = Hash.from_xml(f.to_s)
        account_uid     = xml["message"]["header"]["controlArea"]["from"]["subid"] if xml.dig("message", "header","controlArea","from","subid")


        raise "No Acccount Found" if account_uid.blank?
        raise "No Data found"     if !xml.dig("message","data","advertisement")


        ##============================================================##
        ## Check if user exist
        ##============================================================##
        puts "Check user #{account_uid}"
        httparty  = HTTParty.get("#{ENV['api_shareimmo_base']}/accounts/#{account_uid}",{:headers => apiKeys})
        if httparty.code != 200
          body = {
            :uid      => account_uid,
            :email    => "user_#{account_uid}@evosys.com",
            :password => SecureRandom.urlsafe_base64
          }
          httparty  = HTTParty.post("#{ENV['api_shareimmo_base']}/accounts",{:headers => apiKeys,:body => body})
          raise "Error on Account Creation" if httparty.code != 200
        end


        ##============================================================##
        ## Property
        ##============================================================##
        data = xml["message"]["data"]["advertisement"]
        item = data["item"]


        type    = item.dig("classification", "type") ?    item["classification"]["type"]   : ""
        subtype = item.dig("classification", "subType") ? item["classification"]["subType"] : ""
        shareimmoType = case type.to_s
        when "10"
          case subtype
          when "01"
            101
          when "09"
            101
          when "0D"
            101
          when "OK"
            101
          when "0O"
            101
          when "0S"
            114
          when "0W"
            114
          when "11"
            114
          when "15"
            109
          when "1W"
            110
          when "29"
            107
          when "2D"
            113
          when "2K"
            110
          when "2O"
            101
          when "49"
            209
          when "2S"
            111
          when "4S"
            209
          else
            101
          end
        when "20"
          case subtype
          when "2W"
            201
          when "35"
            201
          when "39"
            206
          when "3D"
            206
          when "3K"
            204
          when "3O"
            205
          when "3S"
            206
          when "3U"
            202
          when "3W"
            202
          when "3Y"
            201
          when 41
            207
          else
            201
          end
        when "30"
          case subtype
          when "45"
            209
          when "4O"
            811
          when "4Q"
            805
          when "4V"
            811
          when "51"
            802
          else
            811
          end
        when "40"
          case subtype
          when "4T"
            802
          when "4D"
            802
          when "55"
            801
          else
            801
          end
        when "50"
          case subtype
          when "4U"
            804
          when "4K"
            801
          else
            804
          end
        when "60"
          case subtype
          when "4W"
            301
          else
            301
          end
        when "70"
          case subtype
          when "61"
            402
          when "65"
            402
          when "69"
            402
          when "6D"
            402
          when "6H"
            403
          when "6K"
            604
          when "6O"
            401
          when "6S"
            401
          else
            401
          end
        when "100"
          case subtype
          when 99
            815
          else
            815
          end
        else
          119
        end

        ##============================================================##
        ## Body
        ##============================================================##
        body                      = Hash.new
        body[:uid]                = data["customerReference"] if data.dig("customerReference")
        body[:account_uid]        = account_uid
        body[:online]             = xml["message"]["header"]["request"]["action"]["code"] == "S" ? 0 : 1
        body[:classification_id]  = shareimmoType
        body[:status_id]          = 1
        body[:status_id]          = item["outsideDescription"]["condition"] == "0" ? 1 : 2  if item.dig("outsideDescription", "condition")
        body[:listing_id]         = 1
        body[:listing_id]         = 2 if item.dig("transaction", "type") && item["transaction"]["type"].to_i == 1
        body[:year_of_built]      = item["outsideDescription"]["buildYear"] if item.dig("outsideDescription", "buildYear")



        body[:building_uid]       = data["customerReference2"] if data.dig("customerReference2") #not sure
        body[:unit]               = nil
        body[:level]              = nil
        body[:cadastre]           = nil
        body[:flag_id]            = nil
        body[:availability_date]  = item["generalInformation"]["dateFromAvailability"] if item.dig("generalInformation", "dateFromAvailability")
        body[:is_luxurious]       = nil



        body[:address]                                = Hash.new
        body[:address][:address_visibility]           = item["location"]["address"]["displayAddress"] if item.dig("location", "address", "displayAddress")
        body[:address][:address_formatted]            = nil
        body[:address][:street_number]                = item["location"]["address"]["streetNumber"]   if item.dig("location", "address", "streetNumber")
        body[:address][:street_name]                  = item["location"]["address"]["street"]         if item.dig("location", "address", "street")
        body[:address][:zipcode]                      = item["location"]["address"]["zip"]            if item.dig("location", "address", "zip")
        body[:address][:locality]                     = item["location"]["address"]["city"]           if item.dig("location", "address", "city")
        body[:address][:sublocality]                  = nil
        body[:address][:administrative_area_level1]   = nil
        body[:address][:administrative_area_level2]   = nil
        body[:address][:country]                      = item["location"]["address"]["country"]        if item.dig("location", "address", "country")
        body[:address][:latitude]                     = nil
        body[:address][:longitude]                    = nil


        body[:area]             = Hash.new
        body[:area][:living]    = item["insideDescription"]["livableSurface"]           if item.dig("insideDescription", "livableSurface")
        body[:area][:land]      = item["outsideDescription"]["ground"]["groundSurface"] if item.dig("outsideDescription","ground","groundSurface")
        body[:area][:unit_id]   = 1


        body[:rooms]                = Hash.new
        body[:rooms][:rooms]        = item["outsideDescription"]["numberOfRooms"]    if item.dig("outsideDescription","numberOfRooms")
        body[:rooms][:bathrooms]    = item["insideDescription"]["rooms"]["bathRooms"]["quantity"] if item.dig("insideDescription", "rooms", "bathRooms", "quantity")
        body[:rooms][:bedrooms]     = item["insideDescription"]["rooms"]["bedrooms"]["quantity"] if item.dig("insideDescription", "rooms", "bedrooms", "quantity")
        body[:rooms][:showerrooms]  = item["insideDescription"]["rooms"]["showerRooms"]["quantity"] if item.dig("insideDescription", "rooms", "showerRooms", "quantity")
        body[:rooms][:toilets]      = ( item["insideDescription"]["rooms"]["Toilets"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "Toilets", "quantity")


        body[:title]         = Hash.new
        body[:title][:fr]    = item["freeTitle"]["FR"] if item.dig("freeTitle", "FR")
        body[:title][:en]    = item["freeTitle"]["EN"] if item.dig("freeTitle", "EN")
        body[:title][:de]    = item["freeTitle"]["DE"] if item.dig("freeTitle", "DE")
        body[:title][:nl]    = item["freeTitle"]["NL"] if item.dig("freeTitle", "NL")
        body[:title][:it]    = item["freeTitle"]["IT"] if item.dig("freeTitle", "IT")
        body[:title][:es]    = item["freeTitle"]["ES"] if item.dig("freeTitle", "ES")
        body[:title][:pt]    = item["freeTitle"]["PT"] if item.dig("freeTitle", "PT")


        body[:description_short]         = Hash.new
        body[:description_short][:fr]    = nil
        body[:description_short][:en]    = nil
        body[:description_short][:de]    = nil
        body[:description_short][:nl]    = nil
        body[:description_short][:it]    = nil
        body[:description_short][:es]    = nil
        body[:description_short][:pt]    = nil


        body[:description]         = Hash.new
        body[:description][:fr]    = item["freeDescription"]["FR"] if item.dig("freeDescription", "FR")
        body[:description][:en]    = item["freeDescription"]["EN"] if item.dig("freeDescription", "EN")
        body[:description][:de]    = item["freeDescription"]["DE"] if item.dig("freeDescription", "DE")
        body[:description][:nl]    = item["freeDescription"]["NL"] if item.dig("freeDescription", "NL")
        body[:description][:it]    = item["freeDescription"]["IT"] if item.dig("freeDescription", "IT")
        body[:description][:es]    = item["freeDescription"]["ES"] if item.dig("freeDescription", "ES")
        body[:description][:pt]    = item["freeDescription"]["PT"] if item.dig("freeDescription", "PT")


        body[:url]         = Hash.new
        body[:url][:fr]    = item["virtualVisitURL"]["FR"] if item.dig("virtualVisitURL", "FR")
        body[:url][:en]    = item["virtualVisitURL"]["EN"] if item.dig("virtualVisitURL", "EN")
        body[:url][:de]    = item["virtualVisitURL"]["DE"] if item.dig("virtualVisitURL", "DE")
        body[:url][:nl]    = item["virtualVisitURL"]["NL"] if item.dig("virtualVisitURL", "NL")
        body[:url][:it]    = item["virtualVisitURL"]["IT"] if item.dig("virtualVisitURL", "IT")
        body[:url][:es]    = item["virtualVisitURL"]["ES"] if item.dig("virtualVisitURL", "ES")
        body[:url][:pt]    = item["virtualVisitURL"]["PT"] if item.dig("virtualVisitURL", "PT")


        body[:financial] = Hash.new
        body[:financial][:price]          = (body[:listing_id]  == 1 && item.dig("transaction", "price", "value")) ? item["transaction"]["price"]["value"] : nil
        body[:financial][:rent]           = (body[:listing_id]  == 2 && item.dig("transaction", "price", "value")) ? item["transaction"]["price"]["value"] : nil
        body[:financial][:currency]       = item["transaction"]["price"]["currency"] if item.dig("transaction", "price", "currency")
        body[:financial][:price_with_tax] = nil
        body[:financial][:fees]           = item["transaction"]["maintenanceCharges"]["value"] if item.dig("transaction", "maintenanceCharges", "value")
        body[:financial][:rent_frequency] = nil
        body[:financial][:tax1_annual]    = nil
        body[:financial][:tax2_annual]    = nil
        body[:financial][:income_annual]  = item["generalInformation"]["rentalIncome"] if item.dig("generalInformation", "rentalIncome")
        body[:financial][:deposit]        = nil


        body[:canada]       = Hash.new
        body[:canada][:mls] = nil

        body[:france]                         = Hash.new
        body[:france][:dpe_indice]            = nil
        body[:france][:dpe_value]             = nil
        body[:france][:dpe_id]                = nil
        body[:france][:ges_indice]            = nil
        body[:france][:ges_value]             = nil
        body[:france][:alur_is_condo]         = nil
        body[:france][:alur_units]            = nil
        body[:france][:alur_uninhabitables]   = nil
        body[:france][:alur_spending]         = nil
        body[:france][:alur_legal_action]     = nil
        body[:france][:rent_honorary]         = nil
        body[:france][:mandat_exclusif]       = nil


        body[:belgium]                          = Hash.new
        body[:belgium][:peb_indice]             = nil
        body[:belgium][:peb_value]              = item["buildingRequirements"]["epc"]["energyConsumption"] if item.dig("buildingRequirements", "epc", "energyConsumption")
        body[:belgium][:peb_id]                 = item["buildingRequirements"]["epc"]["epcReference"] if item.dig("buildingRequirements", "epc", "epcReference")
        body[:belgium][:building_permit]        = nil
        body[:belgium][:done_assignments]       = nil
        body[:belgium][:preemption_property]    = ( item["buildingRequirements"]["preemption"].to_i > 0 ? 1 : 0 ) if item.dig("buildingRequirements", "preemption")
        body[:belgium][:subdivision_permits]    = nil
        body[:belgium][:sensitive_flood_area]   = nil
        body[:belgium][:delimited_flood_zone]   = nil
        body[:belgium][:risk_area]              = nil


        body[:other] = Hash.new
        body[:other][:orientation]          = Hash.new
        body[:other][:orientation][:north]  = item["outsideDescription"]["orientation"]["north"] if item.dig("outsideDescription", "orientation", "north")
        body[:other][:orientation][:south]  = item["outsideDescription"]["orientation"]["south"] if item.dig("outsideDescription", "orientation", "south")
        body[:other][:orientation][:east]   = item["outsideDescription"]["orientation"]["east"] if item.dig("outsideDescription", "orientation", "east")
        body[:other][:orientation][:west]   = item["outsideDescription"]["orientation"]["west"] if item.dig("outsideDescription", "orientation", "west")


        body[:other][:inclusion]                      = Hash.new
        body[:other][:inclusion][:air_conditioning]   = ( item["goodEquipment"]["heating"]["airConditioning"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "heating", "airConditioning")
        body[:other][:inclusion][:hot_water]          = ( item["insideDescription"]["connections"]["water"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "water")
        body[:other][:inclusion][:heated]             = ( item["insideDescription"]["connections"]["gasSupply"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "gasSupply")
        body[:other][:inclusion][:electricity]        = ( item["insideDescription"]["connections"]["electricity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "electricity")
        body[:other][:inclusion][:furnished]          = ( item["insideDescription"]["furnished"]["yesno"].to_i > 0 ? 1 : 0 )  if item.dig("insideDescription", "furnished", "yesno")
        body[:other][:inclusion][:fridge]             = nil
        body[:other][:inclusion][:cooker]             = nil
        body[:other][:inclusion][:dishwasher]         = nil
        body[:other][:inclusion][:dryer]              = nil
        body[:other][:inclusion][:washer]             = ( item["insideDescription"]["rooms"]["washhouse"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "washhouse", "quantity")
        body[:other][:inclusion][:microwave]          = nil


        body[:other][:detail]                       = Hash.new
        body[:other][:detail][:elevator]            = ( item["goodEquipment"]["elevator"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "elevator")
        body[:other][:detail][:laundry]             = nil
        body[:other][:detail][:garbage_chute]       = nil
        body[:other][:detail][:common_space]        = nil
        body[:other][:detail][:janitor]             = nil
        body[:other][:detail][:gym]                 = nil
        body[:other][:detail][:golf]                = nil
        body[:other][:detail][:tennis]              = ( item["outsideDescription"]["tennisCourt"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "tennisCourt", "yesno")
        body[:other][:detail][:sauna]               = nil
        body[:other][:detail][:spa]                 = nil
        body[:other][:detail][:inside_pool]         = nil
        body[:other][:detail][:outside_pool]        = ( item["outsideDescription"]["swimmingPool"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "swimmingPool", "yesno")
        body[:other][:detail][:inside_parking]      = nil
        body[:other][:detail][:outside_parking]     = ( item["outsideDescription"]["numberOfOutdoorParking"].to_i > 0 ? 1 : 0 ) if item.dig("outsideDescription", "numberOfOutdoorParking")
        body[:other][:detail][:parking_on_street]   = nil
        body[:other][:detail][:garagebox]           = ( item["insideDescription"]["rooms"]["garage"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "garage", "quantity")



        body[:other][:half_furnished]                 = Hash.new
        body[:other][:half_furnished][:living_room]   = nil
        body[:other][:half_furnished][:bedrooms]      = nil
        body[:other][:half_furnished][:kitchen]       = nil
        body[:other][:half_furnished][:other]         = nil



        body[:other][:indoor]                         = Hash.new
        body[:other][:indoor][:attic]                 = ( item["insideDescription"]["rooms"]["attic"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "attic", "quantity")
        body[:other][:indoor][:attic_convertible]     = nil
        body[:other][:indoor][:attic_converted]       = nil
        body[:other][:indoor][:cellar]                = ( item["insideDescription"]["rooms"]["cellar"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "cellar", "quantity")
        body[:other][:indoor][:central_vacuum]        = nil
        body[:other][:indoor][:entries_washer_dryer]  = nil
        body[:other][:indoor][:entries_dishwasher]    = nil
        body[:other][:indoor][:fireplace]             = nil
        body[:other][:indoor][:storage]               = nil
        body[:other][:indoor][:no_smoking]            = nil
        body[:other][:indoor][:double_glazing]        = ( item["goodEquipment"]["doubleGlazing"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "doubleGlazing")
        body[:other][:indoor][:central_vacuum]        = nil
        body[:other][:indoor][:triple_glazing]        = nil



        body[:other][:floor]              = Hash.new
        body[:other][:floor][:carpet]     = nil
        body[:other][:floor][:wood]       = nil
        body[:other][:floor][:floating]   = nil
        body[:other][:floor][:ceramic]    = nil
        body[:other][:floor][:parquet]    = nil
        body[:other][:floor][:cushion]    = nil
        body[:other][:floor][:vinyle]     = nil
        body[:other][:floor][:lino]       = nil
        body[:other][:floor][:marble]     = nil



        body[:other][:exterior]                     = Hash.new
        body[:other][:exterior][:land_access]       = nil
        body[:other][:exterior][:back_balcony]      = nil
        body[:other][:exterior][:front_balcony]     = nil
        body[:other][:exterior][:private_patio]     = nil
        body[:other][:exterior][:storage]           = nil
        body[:other][:exterior][:terrace]           = ( item["outsideDescription"]["terrace"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "terrace", "yesno")
        body[:other][:exterior][:veranda]           = ( item["outsideDescription"]["veranda"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "veranda", "yesno")
        body[:other][:exterior][:garden]            = ( item["outsideDescription"]["garden"]["yesno"].to_i > 0 ? 1 : 0 ) if item.dig("outisdeDescription", "garden", "yesno")
        body[:other][:exterior][:sea_view]          = nil
        body[:other][:exterior][:mountain_view]     = nil


        body[:other][:accessibility]                      = Hash.new
        body[:other][:accessibility][:elevator]           = nil
        body[:other][:accessibility][:balcony]            = nil
        body[:other][:accessibility][:grab_bar]           = nil
        body[:other][:accessibility][:wider_corridors]    = nil
        body[:other][:accessibility][:lowered_switches]   = nil
        body[:other][:accessibility][:ramp]               = nil


        body[:other][:senior]                         = Hash.new
        body[:other][:senior][:autonomy]              = nil
        body[:other][:senior][:half_autonomy]         = nil
        body[:other][:senior][:no_autonomy]           = nil
        body[:other][:senior][:meal]                  = nil
        body[:other][:senior][:nursery]               = nil
        body[:other][:senior][:domestic_help]         = nil
        body[:other][:senior][:community]             = nil
        body[:other][:senior][:activities]            = nil
        body[:other][:senior][:validated_residence]   = nil
        body[:other][:senior][:housing_cooperative]   = nil



        body[:other][:pet]                    = Hash.new
        body[:other][:pet][:allow]            = item["generalInformation"]["petsAllowed"] if item.dig("generalInformation", "petsAllowed")
        body[:other][:pet][:cat]              = nil
        body[:other][:pet][:dog]              = nil
        body[:other][:pet][:little_dog]       = nil
        body[:other][:pet][:cage_aquarium]    = nil
        body[:other][:pet][:not_allow]        = nil



        body[:other][:service]              = Hash.new
        body[:other][:service][:internet]   = ( item["insideDescription"]["connections"]["internet"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "internet")
        body[:other][:service][:tv]         = ( item["insideDescription"]["connections"]["televisionCable"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "televisionCable")
        body[:other][:service][:tv_sat]     = ( item["insideDescription"]["connections"]["satelliteTV"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "satelliteTV")
        body[:other][:service][:phone]      = ( item["insideDescription"]["connections"]["phone"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "connections", "phone")


        body[:other][:composition]                    = Hash.new
        body[:other][:composition][:bar]              = nil
        body[:other][:composition][:living]           = ( item["insideDescription"]["rooms"]["livingRoom"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "livingRoom", "quantity")
        body[:other][:composition][:dining]           = nil
        body[:other][:composition][:separate_toilet]  = nil
        body[:other][:composition][:open_kitchen]     = ( item["insideDescription"]["rooms"]["kitchen"]["quantity"].to_i > 0 ? 1 : 0 ) if item.dig("insideDescription", "rooms", "kitchen", "quantity")


        body[:other][:heating]                  = Hash.new
        body[:other][:heating][:electric]       = nil
        body[:other][:heating][:solar]          = nil
        body[:other][:heating][:gaz]            = nil
        body[:other][:heating][:condensation]   = nil
        body[:other][:heating][:fuel]           = ( item["goodEquipment"]["heating"]["heatingFuel"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "heating", "heatingFuel")
        body[:other][:heating][:heat_pump]      = nil
        body[:other][:heating][:floor_heating]  = nil


        body[:other][:transport]                      = Hash.new
        body[:other][:transport][:bicycle_path]       = nil
        body[:other][:transport][:public_transport]   = nil
        body[:other][:transport][:public_bike]        = nil
        body[:other][:transport][:subway]             = nil
        body[:other][:transport][:train_station]      = nil


        body[:other][:security]                     = Hash.new
        body[:other][:security][:guardian]          = nil
        body[:other][:security][:alarm]             = ( item["goodEquipment"]["alarm"].to_i > 0 ? 1 : 0 ) if item.dig("goodEquipment", "alarm")
        body[:other][:security][:intercom]          = nil
        body[:other][:security][:camera]            = nil
        body[:other][:security][:smoke_dectector]   = nil



        body[:pictures]  = []
        if item["pictures"].present?
          if item["pictures"]["picture"].kind_of?(Array)
            item["pictures"]["picture"].each do |picture|
              body[:pictures] << { :url => picture["url"]}
            end
          else
            body[:pictures] << { :url => item["pictures"]["picture"]["url"]}
          end
        end


        ##============================================================##
        ## Post to shareIMMO
        ##============================================================##
        httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/properties", {headers: apiKeys ,:body => body})
        response = JSON.parse(httparty.body)
        if httparty.code == 200
          puts "property #{body[:uid]} added with id #{response["id"]}"
        else
          puts "property #{body[:uid]} failed"
        end



      rescue Exception => e
        puts e.message
        puts e.backtrace
      end
    end
    puts "Sync OK => (#{(Time.now-start)} s)"
  end

end
