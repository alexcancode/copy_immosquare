# encoding: utf-8

require 'zip'
require 'csv'

namespace :emulis do

  task import: :environment do
    begin
      puts "Import started"

      ##============================================================##
      ## Get zip file
      ##============================================================##
      FileUtils.rm_rf(Dir.glob("#{Rails.root}/import/emulis/*"))
      folder_destination = "#{Rails.root}/import/emulis/#{Time.now.strftime("%y-%m-%d")}"
      FileUtils.mkdir_p(folder_destination)


      puts "Download from FTP"
      account = ImportUser.where(:status=>1,:import_provider=>'emulis').first
      ##============================================================##
      ## FTP connection -> get all files
      ## -> copy to tmp directory
      ##============================================================##
      Net::FTP.open(account.ftp_domain,account.ftp_user,account.ftp_password) do |ftp|
        files = ftp.nlst("*.zip")
        unless files.blank?
          files.each do |file|
            ftp.getbinaryfile(file,"#{folder_destination}/#{File.basename(file)}")
          end
        end
        ftp.close
      end

      ##============================================================##
      ## For each account
      ##============================================================##
      [
        # {:username => "sorec",:id => 5155,:flowname => "sorec"},
        {:username => "soreclocation",:id => 5404,:flowname => "soreclocation"}
      ].each do |account|
        puts "="*30
        puts "Process account => #{account[:username]}"
        apiKeys  = {"ApiKey" => ENV['emulis_api_key'],"ApiToken" => ENV['emulis_api_token'], :flowName => account[:flowname]}

        properties_to_remove = []
        call = HTTParty.get("http://shareimmo.com/api/v2/search/properties?apiKey=#{ENV['shareimmo_api_key']}&apiToken=#{ENV['shareimmo_api_token']}&user=#{account[:id]}&per_page=3000")
        call = JSON.parse(call.body)
        call["properties"].each do |p|
          properties_to_remove << p["_source"]["uid"]
        end
        puts "Properties Online => #{properties_to_remove.size}"

        ##============================================================##
        ## Create tmp directory and add files
        ##============================================================##
        account_path = "#{folder_destination}/#{account[:username]}"
        puts "Unzip #{account[:username]}.zip"
        FileUtils.mkdir_p(account_path)
        zip_destination = File.join(folder_destination,"#{account[:username]}.zip")
        Zip::File.open(zip_destination) do |zip_file|
          zip_file.each do |entry|
            entry.extract(File.join(account_path, entry.name))
          end
        end

        ##============================================================##
        ## Parse csv
        ##============================================================##
        CSV.foreach("#{account_path}/Annonces.csv", :col_sep=>"!#").each do |row|
          next if row[0].nil?
          puts "="*30
          puts "Parse ad #{row[seloger_index(2)]}"
          properties_to_remove.delete(row[seloger_index(2)])

          ##============================================================##
          ## Classification
          ##============================================================##
          classification_index =  case row[seloger_index(4)]
          when "Appartement"
            201
          when "bâtiment"
            601
          when "boutique"
            801
          when "château"
            111
          when "inconnu"
            201
          when "hôtel particulier"
            808
          when "immeuble"
            601
          when "local"
            305
          when "loft/atelier/surface"
            207
          when "maison/villa"
            101
          when "parking/box"
            812
          when "terrain"
            401
          else
            201
          end



          ##============================================================##
          ##
          ##============================================================##
          case row[seloger_index(33)]
          when 2048, 2176, 2304, 2432, 6144, 6272, 6400, 6528, 10240, 10368, 10496, 10624
            heating_electric = 1
          when 512, 640, 768, 896, 4608, 4736, 4864, 4992, 8704, 8832, 8960, 9088
            heating_gaz = 1
          when 1024, 1152, 1280, 1408, 5120, 5248, 5376, 5504, 9216, 9344, 9472, 9600
            heating_fuel = 1
          end


          ##============================================================##
          ##
          ##============================================================##
          case row[seloger_index(34)]
          when 2, 5, 8
            other_composition_open_kitchen = 1
          when 6, 8, 9
            halfFurnsihed_kitchen = 1
          end


          ##============================================================##
          ##
          ##============================================================##
          pictures = []
          pic_hash = {}
          pic_hash[84] = 93
          pic_hash[85] = 94
          pic_hash[86] = 95
          pic_hash[87] = 96
          pic_hash[88] = 97
          pic_hash[89] = 98
          pic_hash[90] = 99
          pic_hash[91] = 100
          pic_hash[92] = 101
          pic_hash.each do |x, y|
            picture = {}
            unless row[x].blank?
              picture[:url] = row[x]
              picture[:name] = row[y]
              pictures << picture
            end
          end




          ##============================================================##
          ## Body
          ##============================================================##
          body                      = Hash.new
          body[:uid]                = row[seloger_index(2)]
          body[:account_uid]        = account[:username]
          body[:online]             = 1
          body[:classification_id]  = classification_index
          body[:status_id]          = (row[seloger_index(28)] == "OUI" ? 1 : 2 )
          body[:listing_id]         = ['cession de bail','location','location_vacances'].include?(row[seloger_index(3)]) ? 2 : 1
          body[:year_of_built]      = row[seloger_index(27)]



          body[:building_uid]       = nil
          body[:unit]               = nil
          body[:level]              = row[seloger_index(24)]
          body[:cadastre]           = nil
          body[:flag_id]            = row[seloger_index(84)] == "OUI" ? 4 : nil
          body[:availability_date]  = row[seloger_index(22)]
          body[:is_luxurious]       = nil



          body[:address]                                = Hash.new
          body[:address][:address_visibility]           = nil
          body[:address][:address_formatted]            = "#{row[seloger_index(8)]}, #{row[seloger_index(5)]}, #{row[seloger_index(6)]}"
          body[:address][:street_number]                = row[seloger_index(8)][0..5].to_i
          body[:address][:street_name]                  = nil
          body[:address][:zipcode]                      = row[seloger_index(5)]
          body[:address][:locality]                     = row[seloger_index(6)]
          body[:address][:sublocality]                  = row[seloger_index(9)]
          body[:address][:administrative_area_level1]   = nil
          body[:address][:administrative_area_level2]   = nil
          body[:address][:country]                      = row[seloger_index(7)].present? ? ISO3166::Country.find_country_by_name(row[seloger_index(7)]).alpha2 : "FR"
          body[:address][:latitude]                     = nil
          body[:address][:longitude]                    = nil


          body[:area]             = Hash.new
          body[:area][:living]    = row[seloger_index(16)]
          body[:area][:land]      = row[seloger_index(17)]
          body[:area][:balcony]   = row[seloger_index(40)]
          body[:area][:unit_id]   = nil


          body[:rooms]                = Hash.new
          body[:rooms][:rooms]        = row[seloger_index(18)]
          body[:rooms][:bathrooms]    = row[seloger_index(29)]
          body[:rooms][:bedrooms]     = row[seloger_index(19)]
          body[:rooms][:showerrooms]  = row[seloger_index(30)]
          body[:rooms][:toilets]      = row[seloger_index(31)]


          body[:title]         = Hash.new
          body[:title][:fr]    = row[seloger_index(20)]
          body[:title][:en]    = nil
          body[:title][:de]    = nil
          body[:title][:nl]    = nil
          body[:title][:it]    = nil
          body[:title][:es]    = nil
          body[:title][:pt]    = nil


          body[:description_short]         = Hash.new
          body[:description_short][:fr]    = nil
          body[:description_short][:en]    = nil
          body[:description_short][:de]    = nil
          body[:description_short][:nl]    = nil
          body[:description_short][:it]    = nil
          body[:description_short][:es]    = nil
          body[:description_short][:pt]    = nil


          body[:description]         = Hash.new
          body[:description][:fr]    = row[seloger_index(21)]
          body[:description][:en]    = nil
          body[:description][:de]    = nil
          body[:description][:nl]    = nil
          body[:description][:it]    = nil
          body[:description][:es]    = nil
          body[:description][:pt]    = nil


          body[:url]         = Hash.new
          body[:url][:fr]    = nil
          body[:url][:en]    = nil
          body[:url][:de]    = nil
          body[:url][:nl]    = nil
          body[:url][:it]    = nil
          body[:url][:es]    = nil
          body[:url][:pt]    = nil


          body[:financial] = Hash.new
          body[:financial][:price]          = row[seloger_index(11)] if body[:listing_id] == 1
          body[:financial][:rent]           = row[seloger_index(11)] if body[:listing_id] == 2
          body[:financial][:currency]       = "EUR"
          body[:financial][:price_with_tax] = row[seloger_index(13)] == "OUI" ? 1 : 0
          body[:financial][:fees]           = row[seloger_index(23)].present? ? row[seloger_index(23)].to_i*12 : nil
          body[:financial][:rent_frequency] = nil
          body[:financial][:tax1_annual]    = nil
          body[:financial][:tax2_annual]    = nil
          body[:financial][:income_annual]  = nil
          body[:financial][:deposit]        = row[seloger_index(161)]


          body[:canada]       = Hash.new
          body[:canada][:mls] = nil

          body[:france]                         = Hash.new
          body[:france][:dpe_indice]            = row[seloger_index(177)]
          body[:france][:dpe_value]             = row[seloger_index(176)]
          body[:france][:dpe_id]                = nil
          body[:france][:ges_indice]            = row[seloger_index(179)]
          body[:france][:ges_value]             = row[seloger_index(178)]
          body[:france][:alur_is_condo]         = row[seloger_index(258)] == "OUI" ? 1 : 0
          body[:france][:alur_units]            = row[seloger_index(259)].to_i      if row[seloger_index(259)].present?
          body[:france][:alur_uninhabitables]   = nil
          body[:france][:alur_spending]         = row[seloger_index(260)].to_i   if row[seloger_index(260)].present?
          body[:france][:alur_legal_action]     = row[seloger_index(261)] == "OUI" ? 1 : 0
          body[:france][:rent_honorary]         = row[seloger_index(15)].to_i
          body[:france][:mandat_exclusif]       = row[seloger_index(83)] == "OUI" ? 1 : nil


          body[:belgium]                          = Hash.new
          body[:belgium][:peb_indice]             = nil
          body[:belgium][:peb_value]              = nil
          body[:belgium][:peb_id]                 = nil
          body[:belgium][:building_permit]        = nil
          body[:belgium][:done_assignments]       = nil
          body[:belgium][:preemption_property]    = nil
          body[:belgium][:subdivision_permits]    = nil
          body[:belgium][:sensitive_flood_area]   = nil
          body[:belgium][:delimited_flood_zone]   = nil
          body[:belgium][:risk_area]              = nil


          body[:other] = Hash.new
          body[:other][:orientation]          = Hash.new
          body[:other][:orientation][:north]  = row[seloger_index(38)] == "OUI" ? 1 : 0
          body[:other][:orientation][:south]  = row[seloger_index(35)] == "OUI" ? 1 : 0
          body[:other][:orientation][:east]   = row[seloger_index(36)] == "OUI" ? 1 : 0
          body[:other][:orientation][:west]   = row[seloger_index(37)] == "OUI" ? 1 : 0


          body[:other][:inclusion]                      = Hash.new
          body[:other][:inclusion][:air_conditioning]   = row[seloger_index(64)] == "OUI" ? 1 : 0
          body[:other][:inclusion][:hot_water]          = nil
          body[:other][:inclusion][:heated]             = nil
          body[:other][:inclusion][:electricity]        = nil
          body[:other][:inclusion][:furnished]          = row[seloger_index(26)] == "OUI" ? 1 : 0
          body[:other][:inclusion][:fridge]             = nil
          body[:other][:inclusion][:cooker]             = row[seloger_index(70)] == "OUI" ? 1 : 0
          body[:other][:inclusion][:dishwasher]         = row[seloger_index(71)] == "OUI" ? 1 : 0
          body[:other][:inclusion][:dryer]              = nil
          body[:other][:inclusion][:washer]             = nil
          body[:other][:inclusion][:microwave]          = row[seloger_index(72)] == "OUI" ? 1 : 0


          body[:other][:detail]                       = Hash.new
          body[:other][:detail][:elevator]            = row[seloger_index(41)] == "OUI" ? 1 : 0
          body[:other][:detail][:laundry]             = nil
          body[:other][:detail][:garbage_chute]       = nil
          body[:other][:detail][:common_space]        = nil
          body[:other][:detail][:janitor]             = nil
          body[:other][:detail][:gym]                 = nil
          body[:other][:detail][:golf]                = nil
          body[:other][:detail][:tennis]              = row[seloger_index(76)] == "OUI" ? 1 : 0
          body[:other][:detail][:sauna]               = nil
          body[:other][:detail][:spa]                 = nil
          body[:other][:detail][:inside_pool]         = row[seloger_index(65)] == "OUI" ? 1 : 0
          body[:other][:detail][:outside_pool]        = row[seloger_index(65)] == "OUI" ? 1 : 0
          body[:other][:detail][:inside_parking]      = (row[seloger_index(43)].to_i > 0 ? 1 : 0 ) if row[seloger_index(43)].present?
          body[:other][:detail][:outside_parking]     = (row[seloger_index(43)].to_i > 0 ? 1 : 0 ) if row[seloger_index(43)].present?
          body[:other][:detail][:parking_on_street]   = (row[seloger_index(43)].to_i > 0 ? 1 : 0 ) if row[43].present?
          body[:other][:detail][:garagebox]           = (row[seloger_index(44)].to_i > 0 ? 1 : 0 ) if row[seloger_index(44)].present?



          body[:other][:half_furnished]                 = Hash.new
          body[:other][:half_furnished][:living_room]   = nil
          body[:other][:half_furnished][:bedrooms]      = nil
          body[:other][:half_furnished][:kitchen]       = halfFurnsihed_kitchen.present? ? halfFurnsihed_kitchen : nil
          body[:other][:half_furnished][:other]         = nil



          body[:other][:indoor]                         = Hash.new
          body[:other][:indoor][:attic]                 = nil
          body[:other][:indoor][:attic_convertible]     = nil
          body[:other][:indoor][:attic_converted]       = nil
          body[:other][:indoor][:cellar]                = row[seloger_index(42)] == "OUI" ? 1 : 0
          body[:other][:indoor][:central_vacuum]        = nil
          body[:other][:indoor][:entries_washer_dryer]  = nil
          body[:other][:indoor][:entries_dishwasher]    = nil
          body[:other][:indoor][:fireplace]             = row[seloger_index(68)] == "OUI" ? 1 : 0
          body[:other][:indoor][:storage]               = row[seloger_index(73)] == "OUI" ? 1 : 0
          body[:other][:indoor][:no_smoking]            = nil
          body[:other][:indoor][:double_glazing]        = nil
          body[:other][:indoor][:central_vacuum]        = nil
          body[:other][:indoor][:triple_glazing]        = nil



          body[:other][:floor]              = Hash.new
          body[:other][:floor][:carpet]     = nil
          body[:other][:floor][:wood]       = nil
          body[:other][:floor][:floating]   = nil
          body[:other][:floor][:ceramic]    = nil
          body[:other][:floor][:parquet]    = nil
          body[:other][:floor][:cushion]    = nil
          body[:other][:floor][:vinyle]     = nil
          body[:other][:floor][:lino]       = nil
          body[:other][:floor][:marble]     = nil



          body[:other][:exterior]                     = Hash.new
          body[:other][:exterior][:land_access]       = nil
          body[:other][:exterior][:back_balcony]      = row[seloger_index(39)].to_i > 0 ? 1 : nil if row[seloger_index(39)].present?
          body[:other][:exterior][:front_balcony]     = nil
          body[:other][:exterior][:private_patio]     = nil
          body[:other][:exterior][:storage]           = nil
          body[:other][:exterior][:terrace]           = row[seloger_index(48)] == "OUI" ? 1 : 0
          body[:other][:exterior][:veranda]           = nil
          body[:other][:exterior][:garden]            = nil
          body[:other][:exterior][:sea_view]          = nil
          body[:other][:exterior][:mountain_view]     = row[seloger_index(77)] == "OUI" ? 1 : 0


          body[:other][:accessibility]                      = Hash.new
          body[:other][:accessibility][:elevator]           = row[seloger_index(66)] == "OUI" ? 1 : 0
          body[:other][:accessibility][:balcony]            = row[seloger_index(66)] == "OUI" ? 1 : 0
          body[:other][:accessibility][:grab_bar]           = row[seloger_index(66)] == "OUI" ? 1 : 0
          body[:other][:accessibility][:wider_corridors]    = row[seloger_index(66)] == "OUI" ? 1 : 0
          body[:other][:accessibility][:lowered_switches]   = row[seloger_index(66)] == "OUI" ? 1 : 0
          body[:other][:accessibility][:ramp]               = row[seloger_index(66)] == "OUI" ? 1 : 0


          body[:other][:senior]                         = Hash.new
          body[:other][:senior][:autonomy]              = nil
          body[:other][:senior][:half_autonomy]         = nil
          body[:other][:senior][:no_autonomy]           = nil
          body[:other][:senior][:meal]                  = nil
          body[:other][:senior][:nursery]               = nil
          body[:other][:senior][:domestic_help]         = nil
          body[:other][:senior][:community]             = nil
          body[:other][:senior][:activities]            = nil
          body[:other][:senior][:validated_residence]   = nil
          body[:other][:senior][:housing_cooperative]   = nil



          body[:other][:pet]                    = Hash.new
          body[:other][:pet][:allow]            = row[seloger_index(67)] == "OUI" ? 1 : 0
          body[:other][:pet][:cat]              = nil
          body[:other][:pet][:dog]              = nil
          body[:other][:pet][:little_dog]       = nil
          body[:other][:pet][:cage_aquarium]    = nil
          body[:other][:pet][:not_allow]        = nil



          body[:other][:service]              = Hash.new
          body[:other][:service][:internet]   = nil
          body[:other][:service][:tv]         = row[seloger_index(62)] == "OUI" ? 1 : 0
          body[:other][:service][:tv_sat]     = row[seloger_index(62)] == "OUI" ? 1 : 0
          body[:other][:service][:phone]      = row[seloger_index(74)] == "OUI" ? 1 : 0


          body[:other][:composition]                    = Hash.new
          body[:other][:composition][:bar]              = nil
          body[:other][:composition][:living]           = nil
          body[:other][:composition][:dining]           = nil
          body[:other][:composition][:separate_toilet]  = row[seloger_index(32)] == "OUI" ? 1 : 0
          body[:other][:composition][:open_kitchen]     = other_composition_open_kitchen.present? ? other_composition_open_kitchen : nil


          body[:other][:heating]                  = Hash.new
          body[:other][:heating][:electric]       = heating_electric.present? ? heating_electric : nil
          body[:other][:heating][:solar]          = nil
          body[:other][:heating][:gaz]            = heating_gaz.present? ? heating_gaz : nil
          body[:other][:heating][:condensation]   = nil
          body[:other][:heating][:fuel]           = heating_fuel.present? ? heating_fuel : nil
          body[:other][:heating][:heat_pump]      = nil
          body[:other][:heating][:floor_heating]  = nil


          body[:other][:transport]                      = Hash.new
          body[:other][:transport][:bicycle_path]       = nil
          body[:other][:transport][:public_transport]   = nil
          body[:other][:transport][:public_bike]        = nil
          body[:other][:transport][:subway]             = nil
          body[:other][:transport][:train_station]      = nil


          body[:other][:security]                     = Hash.new
          body[:other][:security][:guardian]          = row[seloger_index(47)] == "OUI" ? 1 : 0
          body[:other][:security][:alarm]             = row[seloger_index(61)] == "OUI" ? 1 : 0
          body[:other][:security][:intercom]          = row[seloger_index(46)] == "OUI" ? 1 : 0
          body[:other][:security][:camera]            = nil
          body[:other][:security][:smoke_dectector]   = nil

          body[:pictures] = pictures

          httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",{:headers => apiKeys ,:body => body})
          response = JSON.parse(httparty.body)
          puts httparty.code
          raise "Error on shareimmo API" if  httparty.code != 200



          ##============================================================##
          ## On met à jour le contact
          ##============================================================##
          body = {
            :uid          => row[seloger_index(107)],
            :account_uid  => account[:username],
            :email        => row[seloger_index(107)],
            :first_name   => row[seloger_index(106)],
            :last_name    => nil,
            :type         => 3,
            :phone        => row[seloger_index(105)]
          }
          httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/contacts",{:headers => apiKeys ,:body => body})
          response = JSON.parse(httparty.body)
          puts response


          ##============================================================##
          ## On lie le contact avec la propriété
          ##============================================================##
          body = {
            :contact_uid  => row[seloger_index(107)],
            :property_uid => row[seloger_index(2)]
          }
          httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/contacts/property",{:headers => apiKeys ,:body => body})
          response = JSON.parse(httparty.body)
          puts response
        end


        ##============================================================##
        ## Deactivate old properties not in feed
        ##============================================================##
        puts "Desactivated olds properties : #{properties_to_remove.size}"
        properties_to_remove.each do |property_uid|
          puts "deactivate property #{property_uid}"
          body               = {}
          body[:uid]         = property_uid
          body[:account_uid] = account[:username]
          body[:online]      = 0
          httparty  = HTTParty.put("#{ENV['api_shareimmo_base']}/properties",{:headers => apiKeys ,:body => body})
          response  = JSON.parse(httparty.body)
        end




      end
      puts "import completed"
    rescue Exception => e
      puts "ERROR !! #{e.message}"
      puts e.backtrace
    end
  end
























  def seloger_index(index)
    index-1
  end

end
