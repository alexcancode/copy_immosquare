require 'net/ftp'
require 'rubygems'
require 'zip'
require 'csv'
require 'securerandom'


namespace :magex do

  # Store existing accounts for today
  # Store existing units for today
  # Store existing accounts for today
  # Store existing units for today
  @today_account_ids  = Redis::List.new("magex:accounts:#{Time.now.strftime("%Y%m%d")}")
  @today_unit_ids     = Redis::List.new("magex:units:#{Time.now.strftime("%Y%m%d")}")
  @old_account_ids    = Redis::List.new("magex:accounts:#{(Time.now-15.days).strftime("%Y%m%d")}")
  @old_unit_ids       = Redis::List.new("magex:units:#{(Time.now-15.days).strftime("%Y%m%d")}")



  task :import => :environment do
    pictures_path = "#{Rails.root}/tmp/pictures"
    pictures_url  = "http://apps.shareimmo.com/pictures/magex"

    FileUtils.mkdir_p(pictures_path) unless File.exists?(pictures_path)


    FileUtils.rm_rf(Dir.glob("#{Rails.root}/import/magex/*"))
    FileUtils.rm_rf(Dir.glob("#{Rails.root}/tmp/pictures/*"))
    folder_destination = "#{Rails.root}/import/magex/#{Time.now.strftime("%y-%m-%d")}"
    FileUtils.mkdir_p(folder_destination)
    magex = ImportUser.where(:status=>1,:import_provider=>'magex').first

    ##============================================================##
    ## Connexion au FTP -> on récupère tout les comptes actifs
    ## -> puis on les transferts vers notre dossier temporaire
    ##============================================================##
    Net::FTP.open(magex.ftp_domain) do |ftp|
      ftp.login
      files = ftp.chdir('ProprioExpert/ShareImmo/')
      files = ftp.nlst("*.xml")
      unless files.blank?
        files.each do |magex_client_xml|
          ftp.getbinaryfile(magex_client_xml,File.join(folder_destination, File.basename(magex_client_xml)))
        end
      end
      ftp.close
    end

    ##============================================================##
    ## Parse XML
    ##============================================================##
    @accounts = Array.new
    @units    = Array.new

    pictures_with_base64 = []
    Dir.foreach(folder_destination) do |file|
      next if file == '.' or file == '..'
      f = File.open(File.join(folder_destination,file))
      @xml = Nokogiri::XML(f)
      f.close

      ##============================================================##
      ## Create account
      ##============================================================##
      account = {}
      account[:uid]         = @xml.css('ID').first.content
      account[:first_name]  = @xml.css('FirstName').first.content
      account[:last_name]   = @xml.css('LastName').first.content
      account[:email]       = @xml.css('Email').first.content
      account[:language_id] = LanguageList::LanguageInfo.find(@xml.css('Language').first.content).iso_639_1 if @xml.css('Language').first.present?
      account[:phone]       = @xml.css('TelephoneNumber Number').first.content if @xml.css('TelephoneNumber Number').present?
      account[:status]      = @xml.css('Active').first.content == "true" ? 1 : 0
      account[:web_site]    = @xml.css('Website').first.content  if @xml.css('Website').first.present? and @xml.css('Website').first.content.present?


      logo = @xml.css('Logo').first if @xml.css('Logo').first.present?
      if logo.present? and logo.css('FileBytes').first.present?
        account[:company_logo_url] = get_picture(logo.css('FileBytes').first.content, "logo_#{account[:uid]}", pictures_url, pictures_path)
        pictures_with_base64 << "logo_#{account[:uid]}"
      end

      country = ISO3166::Country.find_country_by_name(@xml.css('Country').first.content) if @xml.css('Country').first.present?
      account[:country_id] = country.present?  ? country.alpha2 : "CA"

      account[:published_at] = @xml.css('PublishedOn').first.content.split('T').first
      account[:secure_token] = @xml.css('SecurityCode').first.content

      @today_account_ids << account[:uid]
      @accounts << account




      ##============================================================##
      ## Building for account
      ##============================================================##
      buildings = @xml.css("Property")
      buildings.each do |building|
        my_building = {}
        my_building[:uid]   = building.css('ID').first.content unless building.css('BaseID').first.content.nil?
        my_building[:name]  = building.css('Identification').first.content unless building.css('Identification').first.content.nil?
        my_building[:units] = building.css('Units').children.count

        pictures_arr = Array.new
        if building.css('Pictures').first.present? and building.css('Pictures').first.content.present?
          building.css('Pictures Document').each do |document|
            if document.css('FileBytes').present?
              puts "#{document.css('ID').first.content}"
              picture_file = SecureRandom.hex
              pictures_with_base64 << picture_file
              pictures_arr << { :url => get_picture(document.css('FileBytes').first.content, picture_file, pictures_url, pictures_path) }
            end
          end
        end
        my_building[:pictures] = pictures_arr  if pictures_arr.present?


        ##============================================================##
        ## Units by Building
        ##============================================================##
        unless building.css('Units').children.count == 0
          building.css("Unit").each do |unit|
            my_unit       = {}
            pictures      = nil
            my_unit[:uid] = unit.css('ID').first.content
            my_unit[:status]            = 0
            my_unit[:listing_id]        = 2
            my_unit[:status_id]         = 2
            my_unit[:is_vacant]         = 1 if unit.css('Leases').children.count == 0
            my_unit[:account_uid]       = account[:uid]
            my_unit[:building_id]       = my_building[:uid]
            my_unit[:building_name]     = my_building[:name]
            my_unit[:building_units]    = my_building[:units]
            my_unit[:building_pictures] = my_building[:pictures]

            pictures_arr = Array.new
            if unit.css('Pictures').first.present? and unit.css('Pictures').first.content.present?
              unit.css('Pictures Document').each do |document|
                if document.css('FileBytes').present?
                  puts "#{document.css('ID').first.content}"
                  picture_file = SecureRandom.hex
                  pictures_with_base64 << picture_file
                  pictures_arr << { :url => get_picture(document.css('FileBytes').first.content, picture_file, pictures_url, pictures_path) }
                end
              end
            end
            my_unit[:pictures] = pictures_arr


            case unit.css('Type').first.content
            when "Appartment"
              my_unit[:classification_id] = 201
            else
              my_unit[:classification_id] = 201
            end
            if unit.css('Leases Lease Type').first.present?
              case unit.css('Leases Lease Type').first.content
              when "Residential"
                my_unit[:classification_id] = 201
              when "Commercial"
                my_unit[:classification_id] = 801
              end
            end

            my_unit[:address_formatted] = "#{unit.css('Address CivicNumber').first.content}, #{unit.css('Address Street').first.content}, #{unit.css('Address City').first.content}, #{unit.css('Address PostalCode').first.content}, #{unit.css('Address Province').first.content}  #{unit.css('Address Country').first.content}"


            unless unit.css('Country').first.nil?
              country = ISO3166::Country.find_country_by_name(unit.css('Country').first.content)
            end
            if country.present?
              my_unit[:country_id] = country.alpha2
            end

            my_unit[:unit] = unit.css('Address UnitNumber').first.content if unit.css('Address UnitNumber').first.present?

            if unit.css('Address Street').first.content.present?
              street = unit.css('Address Street').first.content
              if street.split(" ")[0].present?
                my_unit[:street_number] = street.split(" ")[0].to_i if street.split(" ")[0].to_i > 0
              end
              my_unit[:street_name] = street.sub(my_unit[:street_number].to_s, '') if my_unit[:street_number].present?
            end

            if unit.css('Address CivicNumber').first.present?
              my_unit[:street_number] = unit.css('Address CivicNumber').first.content if unit.css('Address CivicNumber').first.content.present?
            end

            my_unit[:zipcode] = unit.css('Address PostalCode').first.content if unit.css('Address PostalCode').first.content.present?
            my_unit[:locality] = unit.css('Address City').first.content if unit.css('Address City').first.content.present?
            my_unit[:administrativeAreaLevel1] = unit.css('Province').first.content if unit.css('Province').first.content.present?

            if unit.css('ParkingIndoorIncluded').first.present?
              my_unit[:insideParking] = 1 if unit.css('ParkingIndoorIncluded').first.content == "true"
            end
            if unit.css('ParkingOutdoorIncluded').first.present?
              my_unit[:outsideParking] = 1 if unit.css('ParkingOutdoorIncluded').first.content == "true"
            end

            if unit.css('LockerSpaceIncluded').first.present?
              my_unit[:other_indoor_storage] = 1 if unit.css('LockerSpaceIncluded').first.content == "true"
            end

            if unit.css('KitchenRefrigeratorIncluded').first.present?
              my_unit[:inclusion_fridge] = 1 if unit.css('KitchenRefrigeratorIncluded').first.content == "true"
            end

            if unit.css('KitchenDishwasherIncluded').first.present?
              my_unit[:inclusion_dishwasher] = 1 if unit.css('KitchenDishwasherIncluded').first.content == "true"
            end

            if unit.css('NumberOfRooms').first.present?
              my_unit[:rooms] = unit.css('NumberOfRooms').first.content
            end
            if unit.css('NumberOfBedrooms').first.present?
              my_unit[:bedrooms] = unit.css('NumberOfBedrooms').first.content
            end
            if unit.css('Area').first.present?
              my_unit[:area_living] = unit.css('Area').first.content
            end
            if unit.css('AreaType').first.present?
              my_unit[:area_type] = unit.css('AreaType').first.content
            end

            unless unit.css('AreaType').first.content.nil?
              case unit.css('AreaType').first.content
              when "SquareFeets"
                my_unit[:unitId] = 2
              else
                my_unit[:unitId] = 1
              end
            end

            if unit.css('ServiceElectricity').first.present?
              my_unit[:inclusion_electricity] = 1 unless unit.css('ServiceElectricity').first.content == "true"
            end

            if unit.css('ServiceHeating').first.present?
              my_unit[:inclusion_heated] = 1 unless unit.css('ServiceHeating').first.content == "true"
            end

            if unit.css('ServiceHotWater').first.present?
              my_unit[:inclusion_hotWater] = unit.css('ServiceHotWater').first.content unless unit.css('ServiceHotWater').first.content.blank?
            end

            if unit.css('ServiceStove').first.present?
              my_unit[:inclusion_cooker] = unit.css('ServiceStove').first.content unless unit.css('ServiceStove').first.content.blank?
            end

            if unit.css('ServiceFurnished').first.present?
              my_unit[:inclusion_furnished] = 1 unless unit.css('ServiceFurnished').first.content == "true"
            end

            unless unit.css('ServiceSemiFurnished').first.content == "true"
              my_unit[:halfFurnsihed_livingRoom] = 1
              my_unit[:halfFurnsihed_bedRooms] = 1
              my_unit[:halfFurnsihed_kitchen] = 1
              my_unit[:halfFurnsihed_other] = 1
            end

            if unit.css('ServiceWasherDryerHookup').first.present?
              my_unit[:inclusion_dryer] = 1 unless unit.css('ServiceWasherDryerHookup').first.content == "true"
            end

            unless unit.css('LeaseNextDwellingPrice').first.content.blank?
              my_unit[:financial_rent] = unit.css('LeaseNextDwellingPrice').first.content.to_i
            end

            unless unit.css('LeaseNextPaymentInterval').first.content.blank?
              case unit.css('LeaseNextPaymentInterval')
              when "Day"
                my_unit[:financial_rentFrequency] = 1
              when "Week"
                my_unit[:financial_rentFrequency] = 7
              when "Month"
                my_unit[:financial_rentFrequency] = 30
              else
                my_unit[:financial_rentFrequency] = 30
              end
            end

            my_unit[:building_uid] = building.css('Identification').first.content unless building.css('Identification').first.content.blank?
            my_unit[:building_address_formatted] = "#{building.css('Address CivicNumber').first.content}, #{building.css('Address Street').first.content}, #{building.css('Address City').first.content}, #{building.css('Address PostalCode').first.content}, #{building.css('Address Province').first.content}  #{building.css('Address Country').first.content}"
            my_unit[:building_address_street_number] = "#{building.css('Address CivicNumber').first.content}"
            my_unit[:building_address_street_name] = "#{building.css('Address Street').first.content}"
            my_unit[:building_address_zipcode] = building.css('Address PostalCode').first.content
            my_unit[:building_address_locality] = building.css('Address City').first.content
            my_unit[:building_address_administrativeAreaLevel1] = building.css('Province').first.content

            unless building.css('Country').first.nil?
              country = ISO3166::Country.find_country_by_name(building.css('Country').first.content)
            end
            if country.present?
              my_unit[:building_address_country_id] = country.alpha2
            end



            if unit.css('TaxesMunicipal').first.present?
              my_unit[:financial_tax1Annual] = building.css('TaxesMunicipal').first.content unless building.css('TaxesMunicipal').first.content == "0"
            end

            if unit.css('TaxesSchool').first.present?
              my_unit[:financial_tax2Annual] = building.css('TaxesSchool').first.content unless building.css('TaxesSchool').first.content == "0"
            end

            if unit.css('LandArea').first.present?
              my_unit[:area_land] = building.css('LandArea').first.content unless building.css('LandArea').first.content == "0"
            end

            @units << my_unit
            @today_unit_ids << my_unit[:uid]
          end
        end
      end
    end

    ##============================================================##
    ## Create pictures for units
    ##============================================================##
    Net::FTP.open( ENV['apps_ftp_host'], ENV['apps_ftp_username'], ENV['apps_ftp_password'] ) do |ftp|
      ftp.passive = true
      ftp.chdir("pictures/magex")
      ftp.list("*").each do |ftpfile|
        file = ftpfile.split(/\s+/).last
        ftp.delete file
      end
      pictures_with_base64.each do |name|
        file = File.join(pictures_path, "#{name}.png")
        ftp.putbinaryfile(file)
      end
    end


    ##============================================================##
    ## Sync to shareimmo - Accounts
    ##============================================================##
    @api_errors = []
    @accounts.each do |account|
      puts '='*90
      puts "Magex:import - Account #{account[:uid]} (#{account[:email]}) started"
      puts '='*90

      # Redis list to store all old emails
      @account_last_emails_list = Redis::List.new("magex:account:#{account[:uid]}:last_emails")

      magex_account = MagexAccount.where(:uid=>account[:uid]).first_or_create
      magex_account.update_attribute(:email,account[:email]) if magex_account.email.nil?

      options = {:headers    => { "ApiKey" => ENV['magex_api_key'],"ApiToken" => ENV['magex_api_token']}}
      httparty = HTTParty.get("#{ENV['api_shareimmo_base']}/accounts/#{account[:uid]}",options)
      @response = JSON.parse(httparty.body)
      kangalou_account_exists  = @response["uids"].present? and @response["uids"].collect{|p| p["provider"]}.include?('kangalou')


      ##============================================================##
      ## If email updated from latest call, update in Shareimmo
      ##============================================================##
      if magex_account.email != account[:email]
        if @response["email"] != magex_account.email
          @api_errors << { uid: account[:uid], :message => "email_update_error_corresponding_account", :response => @response }
          puts "Magex:import - Account #{account[:uid]} not corresponding with current shareimmo email"
        else
          options[:body] = update_email_body(account)
          # Call Shareimmo to update email associated to an account uid
          httparty = HTTParty.put("#{ENV['api_shareimmo_base']}/accounts/#{account[:uid]}/email",options)
          @response = JSON.parse(httparty.body)
          if @response["email"] && @response["last_email"]
            @account_last_emails_list << magex_account.email if magex_account.email.present?
            magex_account.update_attributes(:email => account[:email])
          else
            @api_errors << { uid: account[:uid], :message => "email_update_error", response: @response, :email => account[:email], :last_emails => @account_last_emails_list.values }
          end
        end
      else
        JulesLogger.info "#{account[:uid]} not update email"
      end

      ##============================================================##
      ## if !isset Shareimmo id create user
      ## else update user if kangalou user does't exists
      ##============================================================##
      if magex_account[:shareimmo_id].present?
        if !kangalou_account_exists
          options[:body] = update_account_body(account)
          httparty = HTTParty.put("#{ENV['api_shareimmo_base']}/accounts/#{account[:uid]}",options)
        end
      else
        options[:body] = create_account_body(account)
        httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/accounts",options)
        @response = JSON.parse(httparty.body)
        if @response["id"]
          # Create MagexAccount with Shareimmo Id
          MagexAccount.find_by_uid(account[:uid]).update_attributes(:shareimmo_id => @response["id"])
          puts "Magex:import - Account #{account[:uid]} created"
        else
          @api_errors << { uid: account[:uid], response: @response }
          puts "Magex:import - Account #{account[:uid]} error"
        end
      end
    end

    ##============================================================##
    ## Sync to shareimmo - Units
    ##============================================================##
    @units.each do |unit|
      # puts "Magex:import - Unit #{unit[:uid]} started"
      if unit[:account_uid].present?
        options = {:body       => common_unit_body(unit),:headers    => { "ApiKey" => ENV['magex_api_key'],"ApiToken" => ENV['magex_api_token']}}
        httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",options)
        @response = JSON.parse(httparty.body)
        if @response["uid"]
          # Create MagexAccount with Shareimmo Id
          puts "Magex:import - Unit #{unit[:uid]} with shareimmo_id #{@response["id"]} created / Updated"
        else
          puts "Magex:import - Unit #{unit[:uid]} error : #{@response.to_json}"
          @api_errors << { uid: unit[:uid], response: @response }
        end
      else
        puts "Magex:import - Unit #{unit[:uid]} error : Bad account uid"
      end
    end


    ##============================================================##
    ## Sync to shareimmo - Deactivate all units not present today
    ##============================================================##
    @yesterday_unit_ids = Redis::List.new("magex:units:#{(Time.now-1.day).strftime("%Y%m%d")}")
    @yesterday_unit_ids.each do |yesterday_unit|
      # If not present, update shareimmo status to 0
      unless @today_unit_ids.include?(yesterday_unit)
        puts "Magex:import - Deactive unit #{yesterday_unit} started"
        options = {:headers    => {"ApiKey" => ENV['magex_api_key'],"ApiToken" => ENV['magex_api_token']}}

        # Get account uid for unit
        httparty_get = HTTParty.delete("#{ENV['api_shareimmo_base']}/properties/#{yesterday_unit}",options)
        @response = JSON.parse(httparty_get.body)
        if @response["uid"]
          httparty_post = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",options)
          @response = JSON.parse(httparty_post.body)
          if @response["id"]
            # Create MagexAccount with Shareimmo Id
            puts "Magex:import - Unit #{yesterday_unit} with shareimmo_id #{@response["id"]} deactivated"
          else
            # ApiMailer.error_monitoring("Magex import",@response.to_json).deliver_now
            puts "Magex:import - Unit #{yesterday_unit} error : #{@response.to_json}"
            @api_errors << { uid: yesterday_unit, response: @response }
          end
        else
          puts "Magex:import - Deactivate unit #{yesterday_unit} error : #{@response.to_json}"
        end
      end
    end


    ##============================================================##
    ## Manage Errors
    ##============================================================##
    if @api_errors.count > 0
      puts "Magex:import - executed with #{@api_errors.count} errors "
      ApiMailer.error_monitoring("Magex import",@api_errors).deliver_now
    else
      puts "Magex:import - executed without error "
    end

  end
end
