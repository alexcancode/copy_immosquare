

require 'net/ftp'
require 'rubygems'
require 'zip'
require 'csv'
# require 'Iconv'

namespace :prestige do

  task :update_password => :environment do

  	puts "prestige:update_password - started"

  	options = {:headers    => { "ApiKey" => ENV['prestige_api_key'],"ApiToken" => ENV['prestige_api_token']}}

	@connection = ActiveRecord::Base.establish_connection(
	            :adapter => "mysql2",
	            :host => ENV["prestige_db_host"],
	            :database => ENV["prestige_db_database"],
	            :username => ENV["prestige_db_username"],
	            :password =>  ENV["prestige_db_password"]
	)

	sql_agencies = "Select a.*, c.IsoCode as country_code, u.LastName as agent_lastname, u.FirstName as agent_firstname, u.Email as agent_email, \
					u.Password as agent_password, \
					up.MD5Sum as agent_picture_md5sum, up.Extension as agent_picture_extension, \
					ad_fr.Description as description_fr, ad_en.Description as description_en \
					from Agency a \
					left join Country c on (a.IdCountry = c.Id) \
					left join User u on (u.idAgency=a.id) \
					left join Picture up on (up.TypeOwner=\"User\" and up.IdOwner = u.id) \
					left join Description ad_fr on (ad_fr.TypeOwner = \"Agency\" and ad_fr.IdOwner = a.Id and ad_fr.IdLanguage = 2) \
					left join Description ad_en on (ad_fr.TypeOwner = \"Agency\" and ad_en.IdOwner = a.Id and ad_en.IdLanguage = 1) \
					group by a.Id \
					order by id desc \
					"

	@result_agencies = @connection.connection.execute(sql_agencies);

	@result_agencies.each(:as => :hash) do |agency|
		body = {}
		body[:old_password] = ["shareimmo", agency["Id"]].join("_")
		body[:password] = agency["agent_password"]
		body[:password_confirmation] = agency["agent_password"]

		puts body

		options[:body] = body

        httparty = HTTParty.put("#{ENV['api_shareimmo_base']}/accounts/#{agency['Id']}/password",options)
        @response = JSON.parse(httparty.body)

        JulesLogger.info @response
        puts @response

	end


  	puts "prestige:update_password - ended"

  end

  task :import => :environment do

  	puts "prestige:import - started"

  	##============================================================##
  	## Define options for API Shareimmo Calls
  	## Define connection to shareimmo + request to get all prestige api properties
	##============================================================##
  	options = {:headers    => { "ApiKey" => ENV['prestige_api_key'],"ApiToken" => ENV['prestige_api_token']}}

	@connection_shareimmo_prod = ActiveRecord::Base.establish_connection(
	            :adapter => "mysql2",
	            :host => ENV["shareimmo_db_host"],
	            :database => ENV["shareimmo_db_database"],
	            :username => ENV["shareimmo_db_username"],
	            :password =>  ENV["shareimmo_db_password"]
	)

	sql_existing_properties = "select * from properties where api_provider_id = 23"
	@result_existing_properties = @connection_shareimmo_prod.connection.execute(sql_existing_properties);


  	##============================================================##
  	## Get array of existing properties
	##============================================================##
	@shareimmo_existing_properties = Array.new
	@result_existing_properties.each(:as => :hash) do |p|
		# puts "#{p["uid"]}"
		@shareimmo_existing_properties << p["uid"].to_i
	end


  	##============================================================##
  	## Get connection to presige db
  	## Get redis key with all prestige existing accounts
  	## Requet to get all prestige agencies
	##============================================================##
	@connection = ActiveRecord::Base.establish_connection(
	            :adapter => "mysql2",
	            :host => ENV["prestige_db_host"],
	            :database => ENV["prestige_db_database"],
	            :username => ENV["prestige_db_username"],
	            :password =>  ENV["prestige_db_password"]
	)

	@prestige_existing_redis_key = "prestige:accounts:existing"
	@prestige_existing = Redis::Set.new(@prestige_existing_redis_key)

	@agencies_arr = Array.new

	sql_agencies = "Select a.*, c.IsoCode as country_code, u.LastName as agent_lastname, u.FirstName as agent_firstname, u.Email as agent_email, \
					up.MD5Sum as agent_picture_md5sum, up.Extension as agent_picture_extension, \
					ap.MD5Sum as company_picture_md5sum, ap.Extension as company_picture_extension, \
					ad_fr.Description as description_fr, ad_en.Description as description_en \
					from Agency a \
					left join Country c on (a.IdCountry = c.Id) \
					left join User u on (u.idAgency=a.id) \
					left join Picture up on (up.TypeOwner=\"User\" and up.IdOwner = u.id) \
					left join Picture ap on (ap.TypeOwner=\"AgencyLogo\" and ap.IdOwner = a.id) \
					left join Description ad_fr on (ad_fr.TypeOwner = \"Agency\" and ad_fr.IdOwner = a.Id and ad_fr.IdLanguage = 2) \
					left join Description ad_en on (ad_fr.TypeOwner = \"Agency\" and ad_en.IdOwner = a.Id and ad_en.IdLanguage = 1) \
					group by a.Id \
					order by a.Id desc \
					"

	@result_agencies = @connection.connection.execute(sql_agencies);

  	##============================================================##
  	## For each agency, update if exists or create new
	##============================================================##
	@result_agencies.each(:as => :hash) do |agency|
		@agencies_arr << agency["Id"].to_i

		if  @prestige_existing.member?(agency["Id"])

			# next 

		  	puts "prestige:import - update #{agency["Id"]}"

		  	account = {}

		  	account[:uid] = agency["Id"].to_i
		  	account[:country_id] = agency["country_code"].downcase
		  	account[:email] = agency["agent_email"]
		  	account[:first_name] = agency["agent_firstname"]
		  	account[:last_name] = agency["agent_lastname"]

			# account[:presentation_text_fr] = agency[""]
			# account[:presentation_text_en] = agency[""]

			account[:bio_fr] = agency["description_fr"] if agency["description_fr"].present?
			account[:bio_en] = agency["description_en"] if agency["description_en"].present?

		  	account[:phone] = agency["Phone"].phony_formatted(:format => :international)[4,200].gsub(" ", "").to_i if agency["Phone"].present?
		  	account[:phone_visibility] = 1

		  	account[:website] = agency["Website"]
		  	if agency["agent_picture_md5sum"].present?
			  	account[:picture_url] = "http://www.prestige-mls.com/system/pictures/#{agency["agent_picture_md5sum"][0..15]}/#{agency["agent_picture_md5sum"][16..32]}/original.#{agency["agent_picture_extension"]}"
			end
		  	account[:facebook_url] = agency["Facebook"]
		  	account[:twitter_url] = agency["Twitter"]
		  	account[:linkedin_url] = agency["Linkedin"]

		  	account[:company_name] = agency["Name"]
		  	account[:company_address] = agency["Address1"]
		  	account[:company_city] = agency["City"]
		  	account[:company_zipcode] = agency["Zip"]

		  	if agency["company_picture_md5sum"].present?
			  	account[:company_logo_url] = "http://www.prestige-mls.com/system/pictures/#{agency["company_picture_md5sum"][0..15]}/#{agency["company_picture_md5sum"][16..32]}/original.#{agency["company_picture_extension"]}"
			end

			options[:body] = update_account_body(account)

	        httparty = HTTParty.put("#{ENV['api_shareimmo_base']}/accounts/#{account[:uid]}",options)
	        @response = JSON.parse(httparty.body)

	        if @response["errors"].present?
		        puts @response['errors']
	          	# ApiMailer.error_monitoring("Prestige import",@response['errors'].to_json).deliver_now	        	
	        end

		else
		  	puts "prestige:import - add #{agency["Id"]}"

		  	account = {}

		  	account[:uid] = agency["Id"]
		  	account[:email] = agency["agent_email"]

	        options[:body] = create_account_body(account)
	        httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/accounts",options)
	        @response = JSON.parse(httparty.body)

	        if @response["uid"] = account[:uid] && @response["errors"].nil?
	        	@prestige_existing << account[:uid]
	        elsif @response["errors"].present? && @response["errors"] == "Uid #{account[:uid]} is already used"
	        	@prestige_existing << account[:uid]
	        else
	        	puts "An error appears : #{@response['errors']}"
	          	# ApiMailer.error_monitoring("Prestige import",@response['errors'].to_json).deliver_now	        	
	        end
		end
	end

  	##============================================================##
  	## Get array of existing properties
	##============================================================##
	# @agencies_arr.each do |agency|

		sql_properties = "	select p.*, s.Id as property_status, p.Id as property_type, c.IsoCode as property_country, \
							pd_fr.Title as title_fr, pd_en.Title as title_en, \
							cu.IsoCode as property_currency,  \
							pd_fr.Description as description_fr, pd_en.Description as description_en, \
							ptr.Name as property_transaction_type, pty.Name as property_type \
							from Property p \
							left join PropertyStatus s on (s.Id=p.IdPropertyStatus) \
							left join PropertyType t on (t.Id=p.IdPropertyType) \
							left join Country c on (c.Id=p.IdCountry) \
							left join Description pd_fr on (pd_fr.TypeOwner = \"Property\" and pd_fr.IdOwner = p.Id and pd_fr.IdLanguage = 2) \
							left join Description pd_en on (pd_en.TypeOwner = \"Property\" and pd_en.IdOwner = p.Id and pd_en.IdLanguage = 1) \
							left join Currency cu on (p.IdCurrency = cu.Id) \
							left join PropertyTransaction ptr on (ptr.Id=p.IdPropertyTransaction) \
							left join PropertyType pty on (pty.Id=p.IdPropertyType) \
							where p.IdAgency in (#{@agencies_arr.join(',')}) \
							group by p.Id \
							"

		@result_properties = @connection.connection.execute(sql_properties);
		@result_properties.each(:as => :hash) do |property|
			# JulesLogger.info "#{property['Id']}"

			# next if @shareimmo_existing_properties.include?(property["Id"].to_i)


		  	##============================================================##
		  	## If property is deleted, get request to remove
		  	## else create / update
			##============================================================##

			if property["DeletedAt"].present?

			  	begin
				  	puts "prestige:import - remove property #{property["Id"]} "

				  	options[:body] = {}

			        httparty = HTTParty.delete("#{ENV['api_shareimmo_base']}/properties/#{property['Id']}",options)
			        @response = JSON.parse(httparty.body)

			        if @response["errors"].present?
				        puts @response['errors']
			          	# ApiMailer.error_monitoring("Prestige import",@response['errors'].to_json).deliver_now
			        end
			    rescue Exception => e
			    	puts e
		          	# ApiMailer.error_monitoring("Prestige import", e).deliver_now
			    end

			else

				options[:body] = {}

				unit = {}

				unit[:uid] = property["Id"]
				unit[:account_uid] = property["IdAgency"]

			  	puts "prestige:import - add / update property #{unit[:uid]}"

			    # sleep(10)

				# Is luxurious
				unit[:is_luxurious] = 1


				case property["property_type"].to_i
				when 1
					unit[:classification_id] = 201 		# Appartment
				when 2
					unit[:classification_id] = 101 		# House
				when 3
					unit[:classification_id] = 401 		# Land only
				when 4
					unit[:classification_id] = 801 		# Commercial office
				when 6
					unit[:classification_id] = 701 		# Building
				when 7
					unit[:classification_id] = 111 		# Castle
				when 8
					unit[:classification_id] = 107 		# Ranch ferm
				when 9
					unit[:classification_id] = 201 		# Loft / Penthouse
				when 11
					unit[:classification_id] = 808 		# Hotel / Resort
				when 14
					unit[:classification_id] = 119 		# Island
				when 22
					unit[:classification_id] = 107 		# Vineyard
				when 25
					unit[:classification_id] = 119 		# Other
				else
					unit[:classification_id] = 119 		# Other
				end

				# Used
				unit[:status_id] = 1

				# Status = online for api 
				case property["property_status"].to_i
				when 1
					unit[:status] = 1
					unit[:flag] = 1
				when 4
					unit[:status] = 0
				when 5
					unit[:status] = 1
					unit[:flag] = 5
				when 6
					unit[:status] = 1
					unit[:flag] = 5
				else
					unit[:status] = 0
					unit[:flag] = 0
				end

				unit[:building_year_of_built] = property["ConstructionYear"]

				# unit[:availability_date] = property["Availability"]
				# unit[:index_energy] = property[""]
				# unit[:index_insulation] = property[""]

				# unit[:building_uid] = property[""]
				# unit[:building_name] = property[""]
				# unit[:building_units] = property[""]
				# unit[:building_levels] = property[""]
				# unit[:building_address_street_number] = property[""]
				# unit[:building_address_street_name] = property[""]
				unit[:building_address_zipcode] = property["Zip"]
				unit[:building_address_locality] = property["City"]
				# unit[:building_address_sublocality] = property[""]
				# unit[:building_address_administrative_area_level1] = property[""]
				# unit[:building_address_administrative_area_level2] = property[""]
				unit[:building_address_country] = property["property_country"]
				# unit[:building_address_latitude] = property[""]
				# unit[:building_address_longitude] = property[""]
				# unit[:building_description] = property[""]
				unit[:address_visibility] = property["PublishAddress"]
				unit[:address_formatted] = property["Address1"]
				# unit[:street_number] = property[""]
				# unit[:street_name] = property[""]
				unit[:zipcode] = property["Zip"]
				unit[:locality] = property["City"]
				# unit[:sublocality] = property[""]
				# unit[:administrativeAreaLevel1] = property[""]
				# unit[:administrativeAreaLevel2] = property[""]
				unit[:country_id] = property["property_country"]
				unit[:latitude] = property["Latitude"]
				unit[:longitude] = property["Longitude"]

				unit[:area_living] = property["LivableSurface"]
				unit[:area_balcony] = property["Balconies"]
				unit[:area_land] = property["LandSurface"]
				unit[:area_unit_id] = 2

				unit[:rooms] = property["Rooms"]
				unit[:half] = property["HalfBathrooms"]
				unit[:bathrooms] = property["Bathrooms"]
				# unit[:bedrooms] = property[""]
				# unit[:showerrooms] = property[""]

				unit[:title_fr] = "fr- #{property['title_en']}"
				unit[:title_en] = property["title_en"]
				# unit[:title_de] = property[""]
				# unit[:title_nl] = property[""]
				# unit[:title_it] = property[""]
				# unit[:title_es] = property[""]
				# unit[:title_pt] = property[""]

				unit[:description_fr] = "fr- #{property['description_en']}" if property["description_fr"].present?
				unit[:description_en] = property["description_en"] if property["description_en"].present?
				# unit[:description_fr] = valid_string("fr- #{property['description_en']}") if property["description_fr"].present?
				# unit[:description_en] = valid_string(property["description_en"]) if property["description_en"].present?

				# unit[:description_de] = property[""]
				# unit[:description_nl] = property[""]
				# unit[:description_it] = property[""]
				# unit[:description_es] = property[""]
				# unit[:description_pt] = property[""]

				unit[:url_fr] = generate_url(property)
				unit[:url_en] = generate_url(property)

				unit[:financial_price] = property["Price"]

				# For sale by default
				unit[:listing_id] = 2

				unit[:financial_rent] = 1;

				case property["property_transaction_type"] #($property->getTransactionType()->getId()) {				
				when "rent"
					unit[:financial_rent_frequency]	= 360
				when "seasonal"
					unit[:financial_rent_frequency]	= 180
				when "seasonal-week"
					unit[:financial_rent_frequency]	= 7
				when "seasonal-month"
					unit[:financial_rent_frequency]	= 30
				else
					unit[:financial_rent_frequency]	= 0
					unit[:listing_id] = 1
					unit[:financial_rent] = 0
				end

				unit[:financial_fees] = property["CondoFees"].to_i
				unit[:financial_tax1_annual] =  property["TaxFees"].to_i
				unit[:financial_tax1_annual] =  property["PropertyTax"].to_i
				# unit[:financial_income_annual] = 
				unit[:financial_currency] = property["property_currency"] 

				unit[:pictures] = []

				sql_pictures = "select pi.Id, pi.MD5Sum as picture_md5sum, pi.Extension as picture_extension \
								from Picture pi \
								where pi.IdOwner = #{unit[:uid]} and pi.TypeOwner = \"Property\" \
								and DeletedAt is null \
								"

				@result_pictures = @connection.connection.execute(sql_pictures);

				@result_pictures.each(:as => :hash) do |picture|
					# $this->picture->getMD5Sum(), 0, 16).'/'.substr($this->picture->getMD5Sum(), 16)
					unit[:pictures] << { :url => "http://www.prestige-mls.com/system/pictures/#{picture["picture_md5sum"][0..15]}/#{picture["picture_md5sum"][16..32]}/original.#{picture["picture_extension"]}" }
				end


				# unit[:videos] = 

				unit[:inclusion_air_conditioning] = property["AirConditioning"].to_i

				unit[:other_detail_golf] = property["Golf"].to_i
				unit[:detail_inside_pool] = property["SwimmingPool"].to_i			
				unit[:detail_outside_pool] = property["SwimmingPool"].to_i			
				unit[:detail_garagebox] = property["Garages"].to_i			

				unit[:exterior_sea_view] = property["Sea"].to_i
				unit[:exterior_mountain_view] = property["Mountain"].to_i

				unit[:other_pet_allow] = property["Pets"].to_i		
				# unit[:inclusion_] = property[""]			
				# unit[:inclusion_] = property[""]			

		        options[:body] = common_unit_body(unit)

			  	begin		  		
				  	puts "prestige:import - request "

			        httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",options)
			        @response = JSON.parse(httparty.body)

			        if @response["errors"].present?
				        puts @response['errors']
			          	# ApiMailer.error_monitoring("Prestige import",@response['errors'].to_json).deliver_now	        	
			        end
			    rescue Exception => e
			    	puts e
		          	# ApiMailer.error_monitoring("Prestige import", e).deliver_now	        	
			    end
			end

		end
	# end




	# Request to get all properties for 1 user


  	puts "prestige:import - ended"


  end

  task :import_for_account => :environment do

  	options = {:headers    => { "ApiKey" => ENV['shareimmo_api_key'],"ApiToken" => ENV['shareimmo_api_token']}}

	@connection = ActiveRecord::Base.establish_connection(
	            :adapter => "mysql2",
	            :host => ENV["prestige_db_host"],
	            :database => ENV["prestige_db_database"],
	            :username => ENV["prestige_db_username"],
	            :password =>  ENV["prestige_db_password"]
	)

	sql_properties = "	select p.*, s.Id as property_status, p.Id as property_type, c.IsoCode as property_country, \
						pd_fr.Title as title_fr, pd_en.Title as title_en, \
						cu.IsoCode as property_currency,  \
						pd_fr.Description as description_fr, pd_en.Description as description_en, \
						ptr.Name as property_transaction_type, pty.Name as property_type \
						from Property p \
						left join PropertyStatus s on (s.Id=p.IdPropertyStatus) \
						left join PropertyType t on (t.Id=p.IdPropertyType) \
						left join Country c on (c.Id=p.IdCountry) \
						left join Description pd_fr on (pd_fr.TypeOwner = \"Property\" and pd_fr.IdOwner = p.Id and pd_fr.IdLanguage = 2) \
						left join Description pd_en on (pd_en.TypeOwner = \"Property\" and pd_en.IdOwner = p.Id and pd_en.IdLanguage = 1) \
						left join Currency cu on (p.IdCurrency = cu.Id) \
						left join PropertyTransaction ptr on (ptr.Id=p.IdPropertyTransaction) \
						left join PropertyType pty on (pty.Id=p.IdPropertyType) \
						where p.Id in (24635,24610,22384,23708,20925,20899,20898) \
						group by p.Id \
						"

	@result_properties = @connection.connection.execute(sql_properties);
	@result_properties.each(:as => :hash) do |property|

		options[:body] = {}

		unit = {}

		unit[:uid] = property["Id"]
		unit[:account_uid] = 5078

	  	puts "prestige:import - add / update property #{unit[:uid]}"

		# Is luxurious
		unit[:is_luxurious] = 1


		case property["property_type"]
		when 1
			unit[:classification_id] = 201 		# Appartment
		when 2
			unit[:classification_id] = 101 		# House
		when 3
			unit[:classification_id] = 401 		# Land only
		when 4
			unit[:classification_id] = 801 		# Commercial office
		when 6
			unit[:classification_id] = 701 		# Building
		when 7
			unit[:classification_id] = 111 		# Castle
		when 8
			unit[:classification_id] = 107 		# Ranch ferm
		when 9
			unit[:classification_id] = 201 		# Loft / Penthouse
		when 11
			unit[:classification_id] = 808 		# Hotel / Resort
		when 14
			unit[:classification_id] = 119 		# Island
		when 22
			unit[:classification_id] = 107 		# Vineyard
		when 25
			unit[:classification_id] = 119 		# Other
		else
			unit[:classification_id] = 119 		# Other
		end

		# Used
		unit[:status_id] = 1

		# Status = online for api 
		case property["property_status"]
		when 1
			unit[:status] = 1
			unit[:flag] = 0
		when 4
			unit[:status] = 0
			unit[:flag] = 5
		when 5
			unit[:status] = 0
			unit[:flag] = 0
		else
			unit[:status] = 0
			unit[:flag] = 0
		end

		unit[:building_year_of_built] = property["ConstructionYear"]

		# unit[:availability_date] = property["Availability"]
		# unit[:index_energy] = property[""]
		# unit[:index_insulation] = property[""]

		# unit[:building_uid] = property[""]
		# unit[:building_name] = property[""]
		# unit[:building_units] = property[""]
		# unit[:building_levels] = property[""]
		# unit[:building_address_street_number] = property[""]
		# unit[:building_address_street_name] = property[""]
		unit[:building_address_zipcode] = property["Zip"]
		unit[:building_address_locality] = property["City"]
		# unit[:building_address_sublocality] = property[""]
		# unit[:building_address_administrative_area_level1] = property[""]
		# unit[:building_address_administrative_area_level2] = property[""]
		unit[:building_address_country] = property["property_country"]
		# unit[:building_address_latitude] = property[""]
		# unit[:building_address_longitude] = property[""]
		# unit[:building_description] = property[""]
		unit[:address_visibility] = property["PublishAddress"]
		unit[:address_formatted] = property["Address1"]
		# unit[:street_number] = property[""]
		# unit[:street_name] = property[""]
		unit[:zipcode] = property["Zip"]
		unit[:locality] = property["City"]
		# unit[:sublocality] = property[""]
		# unit[:administrativeAreaLevel1] = property[""]
		# unit[:administrativeAreaLevel2] = property[""]
		unit[:country_id] = property["property_country"]
		unit[:latitude] = property["Latitude"]
		unit[:longitude] = property["Longitude"]

		unit[:area_living] = property["LivableSurface"]
		unit[:area_balcony] = property["Balconies"]
		unit[:area_land] = property["LandSurface"]
		unit[:area_unit_id] = 2

		unit[:rooms] = property["Rooms"]
		unit[:half] = property["HalfBathrooms"]
		unit[:bathrooms] = property["Bathrooms"]
		# unit[:bedrooms] = property[""]
		# unit[:showerrooms] = property[""]

		unit[:title_fr] = property["title_fr"]
		unit[:title_en] = property["title_en"]
		# unit[:title_de] = property[""]
		# unit[:title_nl] = property[""]
		# unit[:title_it] = property[""]
		# unit[:title_es] = property[""]
		# unit[:title_pt] = property[""]

		unit[:description_fr] = property["description_fr"] if property["description_fr"].present?
		unit[:description_en] = property["description_en"] if property["description_en"].present?

		# unit[:description_de] = property[""]
		# unit[:description_nl] = property[""]
		# unit[:description_it] = property[""]
		# unit[:description_es] = property[""]
		# unit[:description_pt] = property[""]

		unit[:url_fr] = generate_url(property)
		unit[:url_en] = generate_url(property)

		unit[:financial_price] = property["Price"]

		unit[:listing_id] = 2

		unit[:financial_rent] = 1;
		case property["property_transaction_type"] #($property->getTransactionType()->getId()) {				
		when 2
			unit[:financial_rent_frequency]	= 360
		when 3
			unit[:financial_rent_frequency]	= 180
		when 4
			unit[:financial_rent_frequency]	= 7
		when 5
			unit[:financial_rent_frequency]	= 30
		else
			unit[:financial_rent_frequency]	= 0
			unit[:listing_id] = 1
			unit[:financial_rent] = 0
		end

		unit[:financial_fees] = property["CondoFees"].to_i
		unit[:financial_tax1_annual] =  property["TaxFees"].to_i
		unit[:financial_tax1_annual] =  property["PropertyTax"].to_i
		# unit[:financial_income_annual] = 
		unit[:financial_currency] = property["property_currency"] 

		unit[:pictures] = []

		sql_pictures = "select pi.Id, pi.MD5Sum as picture_md5sum, pi.Extension as picture_extension \
						from Picture pi \
						where pi.IdOwner = #{unit[:uid]} and pi.TypeOwner = \"Property\" \
						and DeletedAt is null \
						"

		@result_pictures = @connection.connection.execute(sql_pictures);

		@result_pictures.each(:as => :hash) do |picture|
			# $this->picture->getMD5Sum(), 0, 16).'/'.substr($this->picture->getMD5Sum(), 16)
			unit[:pictures] << { :url => "http://www.prestige-mls.com/system/pictures/#{picture["picture_md5sum"][0..15]}/#{picture["picture_md5sum"][16..32]}/original.#{picture["picture_extension"]}" }
		end


		# unit[:videos] = 

		unit[:inclusion_air_conditioning] = property["AirConditioning"].to_i

		unit[:other_detail_golf] = property["Golf"].to_i
		unit[:detail_inside_pool] = property["SwimmingPool"].to_i			
		unit[:detail_outside_pool] = property["SwimmingPool"].to_i			
		unit[:detail_garagebox] = property["Garages"].to_i			

		unit[:exterior_sea_view] = property["Sea"].to_i
		unit[:exterior_mountain_view] = property["Mountain"].to_i

		unit[:other_pet_allow] = property["Pets"].to_i		
		# unit[:inclusion_] = property[""]			
		# unit[:inclusion_] = property[""]			

        options[:body] = common_unit_body(unit)

	  	begin		  		
		  	puts "prestige:import - request "

	        httparty = HTTParty.post("#{ENV['api_shareimmo_base']}/properties",options)
	        @response = JSON.parse(httparty.body)

	        if @response["errors"].present?
		        puts @response['errors']
	          	# ApiMailer.error_monitoring("Prestige import",@response['errors'].to_json).deliver_now	        	
	        end
	    rescue Exception => e
	    	puts e
          	# ApiMailer.error_monitoring("Prestige import", e).deliver_now	        	
	    end

	end  	
  end



  def generate_url(property)  		
  	return "http://www.prestige-mls.com/luxury-#{generate_slug(property)}-#{property["Id"]}"
  end

  def generate_slug(property)
  	alphabet = {
            'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
            'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f'
	    }

	if property['title_en'].present?
	    clean = property['title_en'].strtr(alphabet).gsub("/[^a-zA-Z0-9\/_|+ -]/", "").gsub(" ", "-").gsub("/[\/_|+ -]+/", "-")
	else
		clean = ""
	end

  	return "#{property['property_type']}-for-#{property['property_transaction_type']}-#{clean}"
  end

  def valid_string(string)
	ic = Iconv.new('UTF-8//IGNORE', 'UTF-8')
	return ic.iconv(string)
  end

end
