
  # Common functions used by tasks

  def create_account_body(account)
    options_body = {}
    options_body[:uid]      = account[:uid]
    options_body[:email]    = account[:email]
    options_body[:password] = ["shareimmo", account[:uid]].join("_")
    return options_body
  end

  def update_account_body(account)
    options_body = {}

    options_body[:country]                      = account[:country_id]
    options_body[:public_email]                 = account[:email] if account[:email].present?
    options_body[:first_name]                   = account[:first_name] if account[:first_name].present?
    options_body[:last_name]                    = account[:last_name] if account[:last_name].present?
    options_body[:phone]                        = account[:phone] if account[:phone].present?
    options_body[:phone_sms]                    = account[:phone] if account[:phone].present?
    options_body[:phone_visibility]             = 1
    options_body[:website]                      = account[:web_site] if account[:web_site].present?
    options_body[:spoken_languages]             = account[:language_id] if account[:language_id].present?
    options_body[:picture_url]                  = ( account[:picture_url].present? ? account[:picture_url] : "" )
    options_body[:facebook_url]                 = ( account[:facebook_url].present? ? account[:facebook_url] : "" )
    options_body[:twitter_url]                  = ( account[:twitter_url].present? ? account[:twitter_url] : "" )
    options_body[:linkedin_url]                 = ( account[:linkedin_url].present? ? account[:linkedin_url] : "" )
    options_body[:pinterest_url]                = ( account[:pinterest_url].present? ? account[:pinterest_url] : "" )
    options_body[:google_plus_url]              = ( account[:google_plus_url].present? ? account[:google_plus_url] : "" )
    options_body[:instagram_url]                = ( account[:instagram_url].present? ? account[:instagram_url] : "" )
    options_body[:company_name]                 = ( account[:company_name].present? ? account[:company_name] : "" )
    options_body[:company_address]              = ( account[:company_address].present? ? account[:company_address] : "" )
    options_body[:company_city]                 = ( account[:company_city].present? ? account[:company_city] : "" )
    options_body[:company_zipcode]              = ( account[:company_zipcode].present? ? account[:company_zipcode] : "" )
    # options_body[:company_administrative_area]  =
    options_body[:company_logo_url] = ( account[:company_logo_url].present? ? account[:company_logo_url] : "" )
    # options_body[:color1] =
    # options_body[:color2] =
    options_body[:presentation_text] = {}
    options_body[:presentation_text][:fr] = ( account[:presentation_text_fr].present? ? account[:presentation_text_fr] : "" )
    options_body[:presentation_text][:en] = ( account[:presentation_text_en].present? ? account[:presentation_text_en] : "" )
    # options_body[:presentation_text][:de] =
    # options_body[:presentation_text][:nl] =
    # options_body[:presentation_text][:it] =
    # options_body[:presentation_text][:es] =
    # options_body[:presentation_text][:pt] =
    options_body[:bio] = {}
    options_body[:bio][:fr] = ( account[:bio_fr].present? ? account[:bio_fr] : "" )
    options_body[:bio][:en] = ( account[:bio_en].present? ? account[:bio_en] : "" )
    # options_body[:bio][:de] =
    # options_body[:bio][:nl] =
    # options_body[:bio][:it] =
    # options_body[:bio][:es] =
    # options_body[:bio][:pt] =
    # options_body[:baseline] = {}
    # options_body[:baseline][:fr] =
    # options_body[:baseline][:en] =
    # options_body[:baseline][:de] =
    # options_body[:baseline][:nl] =
    # options_body[:baseline][:it] =
    # options_body[:baseline][:es] =
    # options_body[:baseline][:pt] =
    return options_body
  end

  # Create property object - Common
  def common_unit_body(unit)
    options_body = {}

    options_body[:uid]                = unit[:uid]         if unit[:uid].present?
    options_body[:account_uid]        = unit[:account_uid] if unit[:account_uid].present?
    options_body[:online]             = unit[:status] unless unit[:status].nil?
    options_body[:cadastre]           = ( unit[:cadastre].present? ? unit[:cadastre] : "" )
    options_body[:mls]                = ( unit[:mls].present? ? unit[:mls] : "" )
    options_body[:unit]               = ( unit[:unit].present? ? unit[:unit] : "" )
    options_body[:level]              = ( unit[:level].present? ? unit[:level] : "" )
    options_body[:is_luxurious]       = ( unit[:is_luxurious].present? ? 1 : 0 )
    options_body[:classification_id]  = ( unit[:classification_id].present? ? unit[:classification_id] : 201 )
    options_body[:status_id]          = unit[:status_id] unless unit[:status_id].blank?
    options_body[:listing_id]         = unit[:listing_id] unless unit[:listing_id].blank?
    options_body[:flag_id]            = unit[:flag] if unit[:flag].present?
    options_body[:year_of_built]      = unit[:building_year_of_built] unless unit[:building_year_of_built].nil?
    options_body[:availability_date]  = ( unit[:availability_date].present? ? unit[:availability_date] : "" )
    options_body[:index_energy]       = ( unit[:index_energy].present? ? unit[:index_energy] : "" )
    options_body[:index_insulation]   = ( unit[:index_insulation].present? ? unit[:index_insulation] : "" )

    options_body[:building] = {}
    options_body[:building][:uid]     = unit[:building_id] unless unit[:building_id].blank?
    options_body[:building][:name]    = unit[:building_name] unless unit[:building_name].blank?
    options_body[:building][:units]   = unit[:building_units] unless unit[:building_units].blank?
    options_body[:building][:levels]  = ( unit[:building_levels].present? ? unit[:building_levels] : "" )
    options_body[:building][:address] = {}
    options_body[:building][:address][:address_formatted]           = unit[:building_address_formatted]                 if unit[:building_address_formatted].present?
    options_body[:building][:address][:street_number]               = unit[:building_address_street_number]             if unit[:building_address_street_number].present?
    options_body[:building][:address][:street_name]                 = unit[:building_address_street_name]               if unit[:building_address_street_name].present?
    options_body[:building][:address][:zipcode]                     = unit[:building_address_zipcode]                   if unit[:building_address_zipcode].present?
    options_body[:building][:address][:locality]                    = unit[:building_address_locality]                  if unit[:building_address_locality].present?
    options_body[:building][:address][:sublocality]                 = unit[:building_address_sublocality]               if unit[:building_address_sublocality].present?
    options_body[:building][:address][:administrative_area_level1]  = unit[:building_address_administrativeAreaLevel1]  if unit[:building_address_administrativeAreaLevel1].present?
    options_body[:building][:address][:administrative_area_level2]  = unit[:building_address_administrativeAreaLevel2]  if unit[:building_address_administrativeAreaLevel2].present?
    options_body[:building][:address][:country]                     = unit[:building_address_country_id]                if unit[:building_address_country_id].present?
    options_body[:building][:address][:latitude]                    = unit[:building_address_latitude]                  if unit[:building_address_latitude].present?
    options_body[:building][:address][:longitude]                   = unit[:building_address_longitude]                 if unit[:building_address_longitude].present?


    # options_body[:building][:description] = {}
    # options_body[:building][:description][:fr] = {}
    # options_body[:building][:description][:en] = {}
    # options_body[:building][:description][:de] = {}
    # options_body[:building][:description][:nl] = {}
    # options_body[:building][:description][:it] = {}
    # options_body[:building][:description][:es] = {}
    # options_body[:building][:description][:pt] = {}

    # options_body[:building][:pictures] = []
    # options_body[:building][:pictures] = unit[:building_pictures] if unit[:building_pictures].present?

    options_body[:address] = {}
    options_body[:address][:address_visibility]         = 1
    options_body[:address][:address_formatted]          = ( unit[:address_formatted].present? ? unit[:address_formatted] : "" )
    options_body[:address][:street_number]              = ( unit[:street_number].present? ? unit[:street_number] : "" )
    options_body[:address][:street_name]                = ( unit[:street_name].present? ? unit[:street_name] : "" )
    options_body[:address][:zipcode]                    = ( unit[:zipcode].present? ? unit[:zipcode] : "" )
    options_body[:address][:locality]                   = ( unit[:locality].present? ? unit[:locality] : "" )
    options_body[:address][:sublocality]                = ( unit[:sublocality].present? ? unit[:sublocality] : "" )
    options_body[:address][:administrativeAreaLevel1]   = ( unit[:administrativeAreaLevel1].present? ? unit[:administrativeAreaLevel1] : "" )
    options_body[:address][:administrative_area_level2] = ( unit[:administrativeAreaLevel2].present? ? unit[:administrativeAreaLevel2] : "" )
    options_body[:address][:country]                    = ( unit[:country_id].present? ? unit[:country_id] : "" )
    options_body[:address][:latitude]                   = ( unit[:latitude].present? ? unit[:latitude] : "" )
    options_body[:address][:longitude]                  = ( unit[:longitude].present? ? unit[:longitude] : "" )

    options_body[:area] = {}
    options_body[:area][:living]  = ( unit[:area_living].present? ? unit[:area_living] : 0 )
    options_body[:area][:balcony] = ( unit[:area_balcony].present? ? unit[:area_balcony] : "" )
    options_body[:area][:land]    =  ( unit[:area_land].present? ? unit[:area_land] : "" )
    options_body[:area][:unit_id] = ( unit[:unitId].present? ? unit[:unitId] : 1 )

    options_body[:rooms] = {}
    options_body[:rooms][:rooms] = ( unit[:rooms].present? ? unit[:rooms] : "" )

    options_body[:rooms][:half]         = ( unit[:half].present? ? unit[:half] : "" )
    options_body[:rooms][:bathrooms]    = ( unit[:bathrooms].present? ? unit[:bathrooms] : "" )
    options_body[:rooms][:bedrooms]     = ( unit[:bedrooms].present? ? unit[:bedrooms] : "" )
    options_body[:rooms][:showerrooms]  = ( unit[:showerrooms].present? ? unit[:showerrooms] : "" )
    options_body[:rooms][:toilets]      = ( unit[:toilets].present? ? unit[:toilets] : "" )

    options_body[:title] = {}
    options_body[:title][:fr] = ( unit[:title_fr].present? ? unit[:title_fr] : "" )
    options_body[:title][:en] = ( unit[:title_en].present? ? unit[:title_en] : "" )
    # options_body[:title][:nl] = {}
    # options_body[:title][:it] = {}
    # options_body[:title][:es] = {}
    # options_body[:title][:pt] = {}

    options_body[:description] = {}
    options_body[:description][:fr] = ( unit[:description_fr].present? ? unit[:description_fr] : "" )
    options_body[:description][:en] = ( unit[:description_en].present? ? unit[:description_en] : "" )
    options_body[:description][:de] = ( unit[:description_de].present? ? unit[:description_de] : "" )
    options_body[:description][:nl] = ( unit[:description_nl].present? ? unit[:description_nl] : "" )
    options_body[:description][:it] = ( unit[:description_it].present? ? unit[:description_it] : "" )
    options_body[:description][:es] = ( unit[:description_es].present? ? unit[:description_es] : "" )
    options_body[:description][:pt] = ( unit[:description_pt].present? ? unit[:description_pt] : "" )


    options_body[:url] = {}
    options_body[:url][:fr] = ( unit[:url_fr].present? ? unit[:url_fr] : "" )
    options_body[:url][:en] = ( unit[:url_en].present? ? unit[:url_en] : "" )
    # options_body[:url][:de] = {}
    # options_body[:url][:nl] = {}
    # options_body[:url][:it] = {}
    # options_body[:url][:es] = {}
    # options_body[:url][:pt] = {}

    options_body[:financial] = {}
    options_body[:financial][:price]          = ( unit[:financial_price].present? ? unit[:financial_price] : "" )
    options_body[:financial][:price_with_tax] = ( unit[:financial_price_with_tax].present? ? unit[:financial_price_with_tax] : "" )
    options_body[:financial][:rent]           = ( unit[:financial_rent].present? ? unit[:financial_rent] : "" )
    options_body[:financial][:fees]           = ( unit[:financial_fees].present? ? unit[:financial_fees] : "" )
    options_body[:financial][:rent_frequency] = unit[:financial_rentFrequency] unless unit[:financial_rentFrequency].nil?
    options_body[:financial][:tax1_annual]    = unit[:financial_tax1Annual] unless unit[:financial_tax1Annual].nil?
    options_body[:financial][:tax2_annual]    = unit[:financial_tax2Annual] unless unit[:financial_tax2Annual].nil?
    options_body[:financial][:income_annual]  = unit[:financial_income_annual] unless unit[:financial_income_annual].nil?
    options_body[:financial][:deposit]        = unit[:financial_deposit] unless unit[:financial_deposit].nil?
    options_body[:financial][:currency]       = unit[:financial_currency] if unit[:financial_currency].present?
    options_body[:pictures]                   = unit[:pictures] if unit[:pictures].present?
    options_body[:videos] = []


    options_body[:other] = {}

    options_body[:other][:orientation] = {}
    options_body[:other][:orientation][:north]  = unit[:other_orientation_north]  if unit[:other_orientation_north].present?
    options_body[:other][:orientation][:south]  = unit[:other_orientation_south]  if unit[:other_orientation_south].present?
    options_body[:other][:orientation][:east]   = unit[:other_orientation_east]   if unit[:other_orientation_east].present?
    options_body[:other][:orientation][:west]   = unit[:other_orientation_west]   if unit[:other_orientation_west].present?

    options_body[:other][:inclusion] = {}
    options_body[:other][:inclusion][:air_conditioning] = ( unit[:inclusion_air_conditioning].present? ? unit[:inclusion_air_conditioning] : "" )
    options_body[:other][:inclusion][:hot_water]        = unit[:inclusion_hotWater] unless unit[:inclusion_hotWater].nil?
    options_body[:other][:inclusion][:heated]           = unit[:inclusion_heated] unless unit[:inclusion_heated].nil?
    options_body[:other][:inclusion][:electricity]      = unit[:inclusion_electricity] unless unit[:inclusion_electricity].nil?
    options_body[:other][:inclusion][:furnished]        = unit[:inclusion_furnished] unless unit[:inclusion_furnished].nil?
    options_body[:other][:inclusion][:fridge]           = unit[:inclusion_fridge] unless unit[:inclusion_fridge].nil?
    options_body[:other][:inclusion][:cooker]           = unit[:inclusion_cooker] unless unit[:inclusion_cooker].nil?
    options_body[:other][:inclusion][:dishwasher]       = unit[:inclusion_dishwasher] unless unit[:inclusion_dishwasher].nil?
    options_body[:other][:inclusion][:dryer]            = ( unit[:other_inclusion_dryer].present? ? unit[:other_inclusion_dryer] : "" )
    options_body[:other][:inclusion][:dryer]            = unit[:inclusion_dryer] unless unit[:inclusion_dishwasher].nil?
    options_body[:other][:inclusion][:washer]           = ( unit[:other_inclusion_washer].present? ? unit[:other_inclusion_washer] : "" )
    options_body[:other][:inclusion][:microwave]        = ( unit[:other_inclusion_microwave].present? ? unit[:other_inclusion_microwave] : "" )

    options_body[:other][:detail] = {}
    options_body[:other][:detail][:elevator]          = ( unit[:other_detail_elevator].present? ? unit[:other_detail_elevator] : "" )
    options_body[:other][:detail][:laundry]           = ( unit[:other_detail_laundry].present? ? unit[:other_detail_laundry] : "" )
    options_body[:other][:detail][:garbage_chute]     = ( unit[:other_detail_garbage_chute].present? ? unit[:other_detail_garbage_chute] : "" )
    options_body[:other][:detail][:common_space]      = ( unit[:other_detail_common_space].present? ? unit[:other_detail_common_space] : "" )
    options_body[:other][:detail][:janitor]           = ( unit[:other_detail_janitor].present? ? unit[:other_detail_janitor] : "" )
    options_body[:other][:detail][:gym]               = ( unit[:other_detail_gym].present? ? unit[:other_detail_gym] : "" )
    options_body[:other][:detail][:golf]              = ( unit[:other_detail_golf].present? ? unit[:other_detail_golf] : "" )
    options_body[:other][:detail][:tennis]            = ( unit[:other_detail_tennis].present? ? unit[:other_detail_tennis] : "" )
    options_body[:other][:detail][:sauna]             = ( unit[:other_detail_sauna].present? ? unit[:other_detail_sauna] : "" )
    options_body[:other][:detail][:spa]               = ( unit[:other_detail_spa].present? ? unit[:other_detail_spa] : "" )
    options_body[:other][:detail][:inside_pool]       = ( unit[:detail_inside_pool].present? ? unit[:detail_inside_pool] : "" )
    options_body[:other][:detail][:outside_pool]      = ( unit[:detail_outside_pool].present? ? unit[:detail_outside_pool] : "" )
    options_body[:other][:detail][:inside_parking]    = unit[:insideParking] unless unit[:insideParking].nil?
    options_body[:other][:detail][:outside_parking]   = unit[:outsideParking] unless unit[:outsideParking].nil?
    options_body[:other][:detail][:parking_on_street] = unit[:parkingOnStreet] unless unit[:parkingOnStreet].nil?
    options_body[:other][:detail][:garagebox]         = ( unit[:other_detail_garagebox].present? ? unit[:other_detail_garagebox] : "" )

    options_body[:other][:half_furnished] = {}
    options_body[:other][:half_furnished][:living_room] = unit[:halfFurnsihed_livingRoom] unless unit[:halfFurnsihed_livingRoom].nil?
    options_body[:other][:half_furnished][:bed_rooms]   = unit[:halfFurnsihed_bedRooms] unless unit[:halfFurnsihed_bedRooms].nil?
    options_body[:other][:half_furnished][:kitchen]     = ( unit[:halfFurnsihed_kitchen].present? ? unit[:halfFurnsihed_kitchen] : "" )
    options_body[:other][:half_furnished][:other]       = unit[:halfFurnsihed_other] unless unit[:halfFurnsihed_other].nil?

    options_body[:other][:indoor] = {}
    options_body[:other][:indoor][:attic]           = unit[:other_indoor_attic].present?          if unit[:other_indoor_attic].present?
    options_body[:other][:indoor][:cellar]          = unit[:other_indoor_cellar].present?         if unit[:other_indoor_cellar].present?
    options_body[:other][:indoor][:central_vacuum]  = unit[:other_indoor_central_vacuum].present? if unit[:other_indoor_central_vacuum].present?
    # options_body[:other][:indoor][:entries_washer_dryer] =
    # options_body[:other][:indoor][:entries_dishwasher] =
    options_body[:other][:indoor][:fireplace] = ( unit[:other_indoor_fireplace].present? ? unit[:other_indoor_fireplace] : "" )
    options_body[:other][:indoor][:storage]   = ( unit[:other_indoor_storage].present? ? unit[:other_indoor_storage] : "" )
    # options_body[:other][:indoor][:walk_in] =
    # options_body[:other][:indoor][:no_smoking] =
    # options_body[:other][:indoor][:double_glazing] =
    # options_body[:other][:indoor][:triple_glazing] =

    options_body[:other][:floor] = {}
    # options_body[:other][:floor][:carpet] =
    # options_body[:other][:floor][:wood] =
    # options_body[:other][:floor][:floating] =
    # options_body[:other][:floor][:ceramic] =
    # options_body[:other][:floor][:parquet] =
    # options_body[:other][:floor][:cushion] =
    # options_body[:other][:floor][:vinyle] =
    # options_body[:other][:floor][:lino] =
    # options_body[:other][:floor][:marble] =

    options_body[:other][:exterior] = {}
    # options_body[:other][:exterior][:land_access] =
    options_body[:other][:exterior][:back_balcony] = ( unit[:exterior_back_balcony].present? ? unit[:exterior_back_balcony] : "" )
    options_body[:other][:exterior][:front_balcony] = ( unit[:exterior_front_balcony].present? ? unit[:exterior_front_balcony] : "" )
    # options_body[:other][:exterior][:private_patio] =
    # options_body[:other][:exterior][:storage] =
    options_body[:other][:exterior][:terrace] = ( unit[:exterior_terrace].present? ? unit[:exterior_terrace] : "" )
    # options_body[:other][:exterior][:veranda] =
    options_body[:other][:exterior][:garden] = ( unit[:exterior_garden].present? ? unit[:exterior_garden] : "" )
    options_body[:other][:exterior][:sea_view] = ( unit[:exterior_sea_view].present? ? unit[:exterior_sea_view] : "" )
    options_body[:other][:exterior][:mountain_view] = ( unit[:exterior_mountain_view].present? ? unit[:exterior_mountain_view] : "" )

    options_body[:other][:accessibility] = {}
    options_body[:other][:accessibility][:elevator] = ( unit[:other_accessibility_elevator].present? ? unit[:other_accessibility_elevator] : "" )
    options_body[:other][:accessibility][:balcony] = ( unit[:other_accessibility_balcony].present? ? unit[:other_accessibility_balcony] : "" )
    options_body[:other][:accessibility][:grab_bar] = ( unit[:other_accessibility_grab_bar].present? ? unit[:other_accessibility_grab_bar] : "" )
    options_body[:other][:accessibility][:wider_corridors] = ( unit[:other_accessibility_wider_corridors].present? ? unit[:other_accessibility_wider_corridors] : "" )
    options_body[:other][:accessibility][:lowered_switches] = ( unit[:other_accessibility_lowered_switches].present? ? unit[:other_accessibility_lowered_switches] : "" )
    options_body[:other][:accessibility][:ramp] = ( unit[:other_accessibility_ramp].present? ? unit[:other_accessibility_ramp] : "" )

    options_body[:other][:senior] = {}
    # options_body[:other][:senior][:autonomy] =
    # options_body[:other][:senior][:half_autonomy] =
    # options_body[:other][:senior][:no_autonomy] =
    # options_body[:other][:senior][:meal] =
    # options_body[:other][:senior][:nursery] =
    # options_body[:other][:senior][:domestic_help] =
    # options_body[:other][:senior][:community] =
    # options_body[:other][:senior][:activities] =
    # options_body[:other][:senior][:validated_residence] =
    # options_body[:other][:senior][:housing_cooperative] =

    options_body[:other][:pet] = {}
    options_body[:other][:pet][:allow] = ( unit[:other_pet_allow].present? ? unit[:other_pet_allow] : "" )
    # options_body[:other][:pet][:cat] =
    # options_body[:other][:pet][:dog] =
    # options_body[:other][:pet][:little_dog] =
    # options_body[:other][:pet][:cage_aquarium] =
    # options_body[:other][:pet][:not_allow] =

    options_body[:other][:service] = {}
    options_body[:other][:service][:internet] = ( unit[:other_service_internet].present? ? unit[:other_service_internet] : "" )
    options_body[:other][:service][:tv] = ( unit[:other_service_tv].present? ? unit[:other_service_tv] : "" )
    options_body[:other][:service][:tv_sat] = ( unit[:other_service_tv_sat].present? ? unit[:other_service_tv_sat] : "" )
    options_body[:other][:service][:phone] = ( unit[:other_service_phone].present? ? unit[:other_service_phone] : "" )

    options_body[:other][:composition] = {}
    # options_body[:other][:composition][:bar] =
    # options_body[:other][:composition][:living] =
    # options_body[:other][:composition][:dining] =
    options_body[:other][:composition][:separate_toilet] = ( unit[:other_composition_separate_toilet].present? ? unit[:other_composition_separate_toilet] : "" )
    options_body[:other][:composition][:open_kitchen] = ( unit[:other_composition_open_kitchen].present? ? unit[:other_composition_open_kitchen] : "" )

    options_body[:other][:heating] = {}
    options_body[:other][:heating][:electric] = ( unit[:other_heating_electric].present? ? unit[:other_heating_electric] : "" )
    options_body[:other][:heating][:solar] = ( unit[:other_heating_solar].present? ? unit[:other_heating_solar] : "" )
    options_body[:other][:heating][:gaz] = ( unit[:other_heating_gaz].present? ? unit[:other_heating_gaz] : "" )
    options_body[:other][:heating][:condensation] = ( unit[:other_heating_condensation].present? ? unit[:other_heating_condensation] : "" )
    options_body[:other][:heating][:fuel] = ( unit[:other_heating_fuel].present? ? unit[:other_heating_fuel] : "" )
    options_body[:other][:heating][:heat_pump] = ( unit[:other_heating_heat_pump].present? ? unit[:other_heating_heat_pump] : "" )
    options_body[:other][:heating][:floor_heating] = ( unit[:other_heating_floor_heating].present? ? unit[:other_heating_floor_heating] : "" )

    options_body[:other][:transport] = {}
    # options_body[:other][:transport][:bicycle_path] =
    # options_body[:other][:transport][:public_transport] =
    # options_body[:other][:transport][:public_bike] =
    # options_body[:other][:transport][:subway] =
    # options_body[:other][:transport][:train_station] =

    options_body[:other][:security] = {}
    options_body[:other][:security][:guardian] = ( unit[:other_security_guardian].present? ? unit[:other_security_guardian] : "" )
    options_body[:other][:security][:alarm] = ( unit[:other_security_alarm].present? ? unit[:other_security_alarm] : "" )
    options_body[:other][:security][:intercom] = ( unit[:other_security_intercom].present? ? unit[:other_security_intercom] : "" )
    options_body[:other][:security][:camera] = ( unit[:other_security_camera].present? ? unit[:other_security_camera] : "" )
    options_body[:other][:security][:smoke_dectector] = ( unit[:other_security_smoke_dectector].present? ? unit[:other_security_smoke_dectector] : "" )

    # Specific by country France
    options_body[:france] = {}
    options_body[:france][:rent_honorary]       = ( unit[:france_rent_honorary].present? ? unit[:france_rent_honorary] : "" )
    options_body[:france][:dpe_indice]          = ( unit[:france_dpe_indice].present? ? unit[:france_dpe_indice] : "" )
    options_body[:france][:dpe_value]           = ( unit[:france_dpe_value].present? ? unit[:france_dpe_value] : "" )
    options_body[:france][:dpe_id]              = ( unit[:france_dpe_id].present? ? unit[:france_dpe_id] : "" )
    options_body[:france][:ges_indice]          = ( unit[:france_ges_indice].present? ? unit[:france_ges_indice] : "" )
    options_body[:france][:ges_value]           = ( unit[:france_ges_value].present? ? unit[:france_ges_value] : "" )
    options_body[:france][:alur_is_condo]       = ( unit[:france_alur_is_condo].present? ? unit[:france_alur_is_condo] : "" )
    options_body[:france][:alur_units]          = ( unit[:france_alur_units].present? ? unit[:france_alur_units] : "" )
    options_body[:france][:alur_uninhabitables] = ( unit[:france_alur_uninhabitables].present? ? unit[:france_alur_uninhabitables] : "" )
    options_body[:france][:alur_spending]       = ( unit[:france_alur_spending].present? ? unit[:france_alur_spending] : "" )
    options_body[:france][:alur_legal_action]   = ( unit[:france_alur_legal_action].present? ? unit[:france_alur_legal_action] : "" )
    options_body[:france][:mandat_exclusif]     = ( unit[:france_mandat_exclusif].present? ? unit[:france_mandat_exclusif] : "" )

    return options_body
  end

  def update_email_body(account)
    options_body = {}
    options_body[:email]        = account[:email]
    options_body[:secure_code]  = Digest::SHA1.hexdigest("#{account[:uid]}_#{account[:email]}")
    return options_body
  end

  def activate_service_body(service)
    options_body = {}
    options_body[:uid] = service[:uid]
    options_body[:status] = service[:status].to_i
    options_body[:option] = service[:option]
    return options_body
  end


  def get_picture(base64, name, pictures_url, pictures_path)
    bytes = Base64.decode64(base64)
    img = Magick::Image.from_blob(bytes).first if bytes.present?
    if img.present?
      filename = [name, img.format].join(".").downcase
      img.write([pictures_path, filename].join("/"))
      return [pictures_url, filename].join("/")
    end
    return false
  end

  def get_hash(string)
    return Digest::MD5.hexdigest(string)
  end

