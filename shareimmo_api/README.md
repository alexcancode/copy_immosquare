

## Sync Manually Centris to shareIMMO
/srv/apps/shareimmo_api/rvm1scripts/rvm-auto.sh . bundle exec rake centris:to_shareimmo  RAILS_ENV=production


## Sync Manually Centris to shareimmo
/srv/apps/shareimmo_api/rvm1scripts/rvm-auto.sh . bundle exec rake centris:to_imperator  RAILS_ENV=production





body                      = Hash.new
body[:uid]                = nil
body[:account_uid]        = nil
body[:building_uid]       = nil
body[:online]             = nil
body[:classification_id]  = nil
body[:status_id]          = nil
body[:listing_id]         = nil
body[:unit]               = nil
body[:level]              = nil
body[:cadastre]           = nil
body[:flag_id]            = nil
body[:year_of_built]      = nil
body[:availability_date]  = nil
body[:is_luxurious]       = nil


body[:address]                                = Hash.new
body[:address][:address_visibility]           = nil
body[:address][:address_formatted]            = nil
body[:address][:street_number]                = nil
body[:address][:street_name]                  = nil
body[:address][:zipcode]                      = nil
body[:address][:locality]                     = nil
body[:address][:sublocality]                  = nil
body[:address][:administrative_area_level1]   = nil
body[:address][:administrative_area_level2]   = nil
body[:address][:country]                      = nil
body[:address][:latitude]                     = nil
body[:address][:longitude]                    = nil


body[:area]             = Hash.new
body[:area][:living]    = nil
body[:area][:land]      = nil
body[:area][:unit_id]   = nil


body[:rooms]                = Hash.new
body[:rooms][:rooms]        = nil
body[:rooms][:bathrooms]    = nil
body[:rooms][:bedrooms]     = nil
body[:rooms][:showerrooms]  = nil
body[:rooms][:toilets]      = nil


body[:title]         = Hash.new
body[:title][:fr]    = nil
body[:title][:en]    = nil
body[:title][:de]    = nil
body[:title][:nl]    = nil
body[:title][:it]    = nil
body[:title][:es]    = nil
body[:title][:pt]    = nil


body[:description_short]         = Hash.new
body[:description_short][:fr]    = nil
body[:description_short][:en]    = nil
body[:description_short][:de]    = nil
body[:description_short][:nl]    = nil
body[:description_short][:it]    = nil
body[:description_short][:es]    = nil
body[:description_short][:pt]    = nil


body[:description]         = Hash.new
body[:description][:fr]    = nil
body[:description][:en]    = nil
body[:description][:de]    = nil
body[:description][:nl]    = nil
body[:description][:it]    = nil
body[:description][:es]    = nil
body[:description][:pt]    = nil


body[:url]         = Hash.new
body[:url][:fr]    = nil
body[:url][:en]    = nil
body[:url][:de]    = nil
body[:url][:nl]    = nil
body[:url][:it]    = nil
body[:url][:es]    = nil
body[:url][:pt]    = nil


body[:financial] = Hash.new
body[:financial][:price]          = nil
body[:financial][:price_with_tax] = nil
body[:financial][:rent]           = nil
body[:financial][:fees]           = nil
body[:financial][:rent_frequency] = nil
body[:financial][:tax1_annual]    = nil
body[:financial][:tax2_annual]    = nil
body[:financial][:income_annual]  = nil
body[:financial][:deposit]        = nil
body[:financial][:currency]       = nil


body[:pictures] = Array.new

body[:canada]       = Hash.new
body[:canada][:mls] = nil

body[:france]                         = Hash.new
body[:france][:dpe_indice]            = nil
body[:france][:dpe_value]             = nil
body[:france][:dpe_id]                = nil
body[:france][:ges_indice]            = nil
body[:france][:ges_value]             = nil
body[:france][:alur_is_condo]         = nil
body[:france][:alur_units]            = nil
body[:france][:alur_uninhabitables]   = nil
body[:france][:alur_spending]         = nil
body[:france][:alur_legal_action]     = nil
body[:france][:rent_honorary]         = nil
body[:france][:mandat_exclusif]       = nil


body[:belgium]                          = Hash.new
body[:belgium][:peb_indice]             = nil
body[:belgium][:peb_value]              = nil
body[:belgium][:peb_id]                 = nil
body[:belgium][:building_permit]        = nil
body[:belgium][:done_assignments]       = nil
body[:belgium][:preemption_property]    = nil
body[:belgium][:subdivision_permits]    = nil
body[:belgium][:sensitive_flood_area]   = nil
body[:belgium][:delimited_flood_zone]   = nil
body[:belgium][:risk_area]              = nil


body[:other] = Hash.new
body[:other][:orientation]          = Hash.new
body[:other][:orientation][:north]  = nil
body[:other][:orientation][:south]  = nil
body[:other][:orientation][:east]   = nil
body[:other][:orientation][:west]   = nil


body[:other][:inclusion]                      = Hash.new
body[:other][:inclusion][:air_conditioning]   = nil
body[:other][:inclusion][:hot_water]          = nil
body[:other][:inclusion][:heated]             = nil
body[:other][:inclusion][:electricity]        = nil
body[:other][:inclusion][:furnished]          = nil
body[:other][:inclusion][:fridge]             = nil
body[:other][:inclusion][:cooker]             = nil
body[:other][:inclusion][:dishwasher]         = nil
body[:other][:inclusion][:dryer]              = nil
body[:other][:inclusion][:washer]             = nil
body[:other][:inclusion][:microwave]          = nil


body[:other][:detail]                       = Hash.new
body[:other][:detail][:elevator]            = nil
body[:other][:detail][:laundry]             = nil
body[:other][:detail][:garbage_chute]       = nil
body[:other][:detail][:common_space]        = nil
body[:other][:detail][:janitor]             = nil
body[:other][:detail][:gym]                 = nil
body[:other][:detail][:golf]                = nil
body[:other][:detail][:tennis]              = nil
body[:other][:detail][:sauna]               = nil
body[:other][:detail][:spa]                 = nil
body[:other][:detail][:inside_pool]         = nil
body[:other][:detail][:outside_pool]        = nil
body[:other][:detail][:inside_parking]      = nil
body[:other][:detail][:outside_parking]     = nil
body[:other][:detail][:parking_on_street]   = nil
body[:other][:detail][:garagebox]           = nil


body[:other][:half_furnished]                 = Hash.new
body[:other][:half_furnished][:living_room]   = nil
body[:other][:half_furnished][:bedrooms]      = nil
body[:other][:half_furnished][:kitchen]       = nil
body[:other][:half_furnished][:other]         = nil



body[:other][:indoor]                         = Hash.new
body[:other][:indoor][:attic]                 = nil
body[:other][:indoor][:attic_convertible]     = nil
body[:other][:indoor][:attic_converted]       = nil
body[:other][:indoor][:cellar]                = nil
body[:other][:indoor][:central_vacuum]        = nil
body[:other][:indoor][:entries_washer_dryer]  = nil
body[:other][:indoor][:entries_dishwasher]    = nil
body[:other][:indoor][:fireplace]             = nil
body[:other][:indoor][:storage]               = nil
body[:other][:indoor][:no_smoking]            = nil
body[:other][:indoor][:double_glazing]        = nil
body[:other][:indoor][:central_vacuum]        = nil
body[:other][:indoor][:triple_glazing]        = nil



body[:other][:floor]              = Hash.new
body[:other][:floor][:carpet]     = nil
body[:other][:floor][:wood]       = nil
body[:other][:floor][:floating]   = nil
body[:other][:floor][:ceramic]    = nil
body[:other][:floor][:parquet]    = nil
body[:other][:floor][:cushion]    = nil
body[:other][:floor][:vinyle]     = nil
body[:other][:floor][:lino]       = nil
body[:other][:floor][:marble]     = nil



body[:other][:exterior]                     = Hash.new
body[:other][:exterior][:land_access]       = nil
body[:other][:exterior][:back_balcony]      = nil
body[:other][:exterior][:front_balcony]     = nil
body[:other][:exterior][:private_patio]     = nil
body[:other][:exterior][:storage]           = nil
body[:other][:exterior][:terrace]           = nil
body[:other][:exterior][:veranda]           = nil
body[:other][:exterior][:garden]            = nil
body[:other][:exterior][:sea_view]          = nil
body[:other][:exterior][:mountain_view]     = nil


body[:other][:accessibility]                      = Hash.new
body[:other][:accessibility][:elevator]           = nil
body[:other][:accessibility][:balcony]            = nil
body[:other][:accessibility][:grab_bar]           = nil
body[:other][:accessibility][:wider_corridors]    = nil
body[:other][:accessibility][:lowered_switches]   = nil
body[:other][:accessibility][:ramp]               = nil


body[:other][:senior]                         = Hash.new
body[:other][:senior][:autonomy]              = nil
body[:other][:senior][:half_autonomy]         = nil
body[:other][:senior][:no_autonomy]           = nil
body[:other][:senior][:meal]                  = nil
body[:other][:senior][:nursery]               = nil
body[:other][:senior][:domestic_help]         = nil
body[:other][:senior][:community]             = nil
body[:other][:senior][:activities]            = nil
body[:other][:senior][:validated_residence]   = nil
body[:other][:senior][:housing_cooperative]   = nil



body[:other][:pet]                    = Hash.new
body[:other][:pet][:allow]            = nil
body[:other][:pet][:cat]              = nil
body[:other][:pet][:dog]              = nil
body[:other][:pet][:little_dog]       = nil
body[:other][:pet][:cage_aquarium]    = nil
body[:other][:pet][:not_allow]        = nil



body[:other][:service]              = Hash.new
body[:other][:service][:internet]   = nil
body[:other][:service][:tv]         = nil
body[:other][:service][:tv_sat]     = nil
body[:other][:service][:phone]      = nil


body[:other][:composition]                    = Hash.new
body[:other][:composition][:bar]              = nil
body[:other][:composition][:living]           = nil
body[:other][:composition][:dining]           = nil
body[:other][:composition][:separate_toilet]  = nil
body[:other][:composition][:open_kitchen]     = nil


body[:other][:heating]                  = Hash.new
body[:other][:heating][:electric]       = nil
body[:other][:heating][:solar]          = nil
body[:other][:heating][:gaz]            = nil
body[:other][:heating][:condensation]   = nil
body[:other][:heating][:fuel]           = nil
body[:other][:heating][:heat_pump]      = nil
body[:other][:heating][:floor_heating]  = nil


body[:other][:transport]                      = Hash.new
body[:other][:transport][:bicycle_path]       = nil
body[:other][:transport][:public_transport]   = nil
body[:other][:transport][:public_bike]        = nil
body[:other][:transport][:subway]             = nil
body[:other][:transport][:train_station]      = nil


body[:other][:security]                     = Hash.new
body[:other][:security][:guardian]          = nil
body[:other][:security][:alarm]             = nil
body[:other][:security][:intercom]          = nil
body[:other][:security][:camera]            = nil
body[:other][:security][:smoke_dectector]   = nil
