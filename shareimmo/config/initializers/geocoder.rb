Geocoder.configure(
  :lookup => :google,
  :google => {
    :api_key => ENV["GOOGLE_API_KEY"],
    :use_https => true
  },
  :ip_lookup => :geoip2,
  :google_places_details => {
    :api_key => ENV["GOOGLE_API_KEY"],
    :use_https => true
  }

)
