Mypdflib.setup do |config|
  config.license        = Rails.env.production? ? ENV['pdflib_license'] : nil
  config.stuff_folder   = "#{Rails.root}/app/pdfs/fonts"
  config.ppi            = 250
end
