# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(front_office.js front_office.css back_office.css back_office.js map.js map.css api.js api.css)


##============================================================##
## https://github.com/singlebrook/jquery-lazy-images
##============================================================##
Rails.application.config.assets.precompile += %w( grey.gif )
