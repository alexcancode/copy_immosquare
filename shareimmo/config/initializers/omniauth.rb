Rails.application.config.middleware.use OmniAuth::Builder do

  ##============================================================##
  ## Facebook
  ##============================================================##
  provider :facebook, ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_SECRET_ID'],
  {
    :scope=>'email,publish_actions,manage_pages',
    :display => 'page',
    :image_size=> 'square',
    :secure_image_url => true
  }


  ##============================================================##
  ## Twitter
  ##============================================================##
  provider :twitter, ENV['TWITTER_API_KEY'], ENV['TWITTER_API_SECRET'],
  {
    :secure_image_url => true,
    :image_size => 'bigger',
  }


  ##============================================================##
  ## Google/Youtube
  ##============================================================##
  provider :google_oauth2, ENV['GOOGLE_CLIENT_ID'], ENV['GOOGLE_CLIENT_SECRET'],
  {
    :scope => "email,profile,youtube",
    :access_type => "offline",
    :image_aspect_ratio => "square",
    :image_size => 50,
    :prompt => "consent",
    :include_granted_scopes => true
  }


  ##============================================================##
  ## Pinterest
  ##============================================================##
  provider :pinterest, ENV['pinterest_client_id'], ENV['pinterest_client_secret']

end


OmniAuth.config.on_failure do |env|
  error_type = env['omniauth.error.type']
  new_path = "#{env['SCRIPT_NAME']}#{OmniAuth.config.path_prefix}/failure?message=#{error_type}"
  [301, {'Location' => new_path, 'Content-Type' => 'text/html'}, []]
end
