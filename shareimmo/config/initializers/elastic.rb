Elasticsearch::Model.client = Elasticsearch::Client.new(
  {
    host: "elastic01.immosquare.com",
    retry_on_failure: 5,
    reload_connections: 1_000,
    reload_on_failure: true,
    request_timeout: 10*60
  }
)

