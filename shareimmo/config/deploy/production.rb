set :stage, :production
set :whenever_environment, :production
set :rvm_ruby_version, "2.3.3@shareimmo"
set :deploy_to, "/srv/apps/shareimmo"
server 'web03.immosquare.com',{:user=>'root', :roles => %w{web app db}}
