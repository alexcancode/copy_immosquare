# set :branch, "develop"
set :stage, :beta
set :puma_env, fetch(:rack_env, fetch(:rails_env, 'beta'))
set :whenever_environment, :beta
set :rvm_ruby_version, "2.3.3@dulcisdomus"
set :deploy_to, "/srv/apps/dulcisdomus"
puts "\e[0;31m########   beta  ########\e[0m\n#"
server 'web03.immosquare.com',{:user=>'root', :roles => %w{web app db}}
