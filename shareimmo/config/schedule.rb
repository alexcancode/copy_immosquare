case @environment
when 'production'


  every 15.minutes do
    rake "craigslist:check_forward_emails"
    # rake "kijiji:validate"
  end


  every 1.hours do
    rake "lespac:xml"
    rake "lespac:scrapping"
    # rake "kijiji:publish"
  end

  every 2.hours do
    rake "louer:xml"
    rake "louer:scrapping"
  end

  every 12.hours do
    rake "easysolutions:admin:services:activate"
  end


  every 1.day, :at => '2:00 am' do
    rake "shelterr:export"
  end

  every 1.day, :at => '3:00 am' do
    rake "shareimmo:check_mls"
    rake "shareimmo:xml"
  end

  every 1.day, :at => '4:00 am' do
    rake "craigslist:check_backlinks"
    rake "our_clients:create_visibilities"
  end



  every 1.day, :at => '5:00 am' do
    rake "youtube:remove_kangalou_videos"
  end

  every 1.day, :at => '9:00 am' do
    rake "prestige:synchronization"
  end


  every 1.day, :at => '10:30 pm' do
    rake "listglobally:create_backlinks"
    rake "listglobally:xml"
  end



when 'beta'
  every 1.hour do
    rake "feedbacks:alerts:execute"
  end
end
