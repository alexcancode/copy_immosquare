require File.expand_path('../boot', __FILE__)

require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"
require "action_view/railtie"

##============================================================##
## JE ne sais plus pourquoie je l'ai mis...
##============================================================##
require 'net/http'


# Require the gems listed in Gemfile, including any gems
Bundler.require(*Rails.groups)


module Shareimmo
  class Application < Rails::Application
    config.time_zone = 'Eastern Time (US & Canada)'

    # add app/assets/fonts to the asset path
    config.assets.paths << Rails.root.join("app", "assets", "fonts")

    # add tmp/themes to the asset path
    config.assets.paths << Rails.root.join('tmp', 'themes')

    Money.use_i18n = false


    ##============================================================##
    ## The default locale is :en and all translations
    ## from config/locales/*.rb,yml are auto loaded.
    ##============================================================##
    config.i18n.default_locale = 'fr'
    config.i18n.available_locales = ['fr','en','de','nl','it','pt','es','mx']
    config.i18n.fallbacks = {:fr => :en, :en => :fr, :de => :fr, :nl => :fr, :it => :fr, :pt => :fr, :es => :fr}


    ##============================================================##
    ## React with coffee
    ##============================================================##
    config.browserify_rails.commandline_options = "-t cjsxify  --extension='.js.coffee'"


    ##============================================================##
    ## Config action_mailer
    ##============================================================##
    config.action_mailer.default_url_options = { :host => ENV["HOST"] }
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :address        => "smtp-relay.sendinblue.com",
      :port           => 587,
      :authentication => :plain,
      :user_name      => ENV["SENDINBLUE_USERNAME"],
      :password       => ENV["SENDINBLUE_KEY"]
    }

    config.middleware.insert_before 0, "Rack::Cors" do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :options]
      end
    end

    ##============================================================##
    ## Config paperclip S3_storage
    ##============================================================##
    config.paperclip_defaults = {
      :storage => :s3,
      :s3_protocol => :https,
      :s3_credentials => {
        :bucket => ENV['AWS_BUCKET'],
        :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
        :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
        },
        :s3_region => ENV["aws_region"]
      }

    ##============================================================##
    ## Racks:: Cors pour autoriser des API externe en Ajax
    ##============================================================##
    config.middleware.insert_before 0, "Rack::Cors" do
      allow do
        origins 'www.lvh.me:4000','lvh.me:4000' , 'immo.immosquare.com','realestate.immosquare.com','api.shareimmo.com'
        resource '/api/*',
        :headers => :any,
        :expose  => ['access-token', 'expiry', 'token-type', 'uid', 'client'],
        :methods => [:get, :post, :options,:delete, :put]
      end
    end



    ##============================================================##
    ## Devise Mailer Layout
    ##============================================================##
    config.to_prepare do
      Devise::Mailer.layout 'mailer'
    end


    config.autoload_paths += ["#{config.root}/lib"]
    config.autoload_paths += ["#{Rails.root}/app/printers"]

    config.active_record.raise_in_transactional_callbacks = true

  end
end
