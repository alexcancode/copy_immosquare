require 'sidekiq/web'

 Rails.application.routes.draw do

  use_doorkeeper
  ##============================================================##
  ## Poud débuger les pages d'erreur, décommeneter ces deux
  ## lignes
  ##============================================================##
  # match "/404" => "errors#error_404", via: [ :get, :post, :patch, :delete ]
  # match "/500" => "errors#error_500", via: [ :get, :post, :patch, :delete ]





  ##============================================================##
  ## API
  ##============================================================##
  namespace :api, defaults: {format: 'json'} do
    ##============================================================##
    ## App health
    ##============================================================##
    match 'health'                => 'api#health',     :via => :get

    ##============================================================##
    ## API V1
    ##============================================================##
    namespace :v1 do
      match 'properties'                => 'properties#import', :via => [:post,:put]
      match 'properties/visibility'     => 'properties#show_visibility', :via => :get
      match 'buildings'                 => 'inventories#buildings', :via => [:get, :post]
      match 'inventories'               => 'inventories#create', :via => :post
      match 'inventory/error'           => 'inventories#send_inventory_error', :via => :post
      match 'sync_edl'                  => 'auth#sync_edl', :via => [:get, :post]
      match 'has-token'                 => 'inventories#has_token', :via => [:post]
      scope 'auth' do
        match 'sign_in'            => 'auth#login', :via => :post
        match 'sign_out'           => 'auth#logout', :via => :delete
        match 'validate_token'     => 'auth#validate_token', :via => :get
      end
      devise_scope :user do
        match 'login'                  => 'auth#login', :via => :post
        match 'logout'                 => 'sessions#destroy', :via => :delete
        match 'registrations'          => 'registrations#create', :via => :post
      end
      match 'connect'             => 'users#connect', :via => :get
      match 'connect'             => 'users#connect_encode', :via => :post
      match 'users/:uid'          => 'users#edit', :via => :post
    end

    ##============================================================##
    ## API V2
    ##============================================================##
    namespace :v2 do
      ##============================================================##
      ## Map
      ##============================================================##
      match 'map/setup'      => 'maps#setup',          :via => :get
      ##============================================================##
      ## Search
      ##============================================================##
      match 'search/geojson/properties'       => 'searchs#properties',        :via => :get
      match 'search/geojson/buildings'        => 'searchs#buildings',         :via => :get
      match 'search/properties'               => 'searchs#properties',        :via => :get
      match 'search/properties/:id'           => 'searchs#properties_show',   :via => :get
      match 'search/buildings'                => 'searchs#buildings',         :via => :get
      match 'search/buildings/:id'            => 'searchs#buildings_show',    :via => :get
      match 'search/accounts/:id'             => 'searchs#accounts_show',     :via => :get
      ##============================================================##
      ## Accounts
      ##============================================================##
      match "accounts"                => 'accounts#create',         :via => :post
      match "accounts/:uid"           => 'accounts#retrieve',       :via => :get
      match "accounts/:uid"           => 'accounts#update',         :via => :put
      match "accounts/:uid/email"     => 'accounts#update_email',   :via => :put
      match "accounts/:uid/password"  => 'accounts#update_password',:via => :put
      match "accounts/:uid"           => 'accounts#destroy',        :via => :delete
      match 'accounts/brokers/centris'=> 'accounts#brokers',        :via => :get
      match "accounts/:uid/token"     => 'accounts#get_token',      :via => :get
      match "accounts/:uid/connect"   => 'accounts#connect',        :via => :get, :format => "html"
      match "accounts/:uid/properties" => 'accounts#properties',    :via => :get

      ##============================================================##
      ## Webmarketing IMMOSQUARE
      ##============================================================##
      match 'accounts/marketing/:subdomain'   => "accounts#marketing",     :via => :get

      ##============================================================##
      ## Properties
      ##============================================================##
      match "properties"       => 'properties#create_and_update',   :via => [:put, :post]
      match "properties/:uid"  => 'properties#retrieve',            :via => :get
      match "properties/:uid"  => 'properties#destroy',             :via => :delete

      ##============================================================##
      ## Properties
      ##============================================================##
      match "buildings"       => 'buildings#create_and_update',   :via => [:put, :post]
      match "buildings/:uid"  => 'buildings#retrieve',            :via => :get
      match "buildings/:uid"  => 'buildings#destroy',             :via => :delete


      ##============================================================##
      ## Contacts
      ##============================================================##
      match "contacts"            => 'contacts#create_and_update',             :via => [:put, :post]
      match "contacts/property"   => 'contacts#contact_for_property_add',      :via => [:post]
      match "contacts/property"   => 'contacts#contact_for_property_destroy',  :via => [:delete]
      match "contacts/:uid"       => 'contacts#retrieve',                      :via => [:get]
      match "contacts/:uid"       => 'contacts#destroy',                       :via => [:delete]



      ##============================================================##
      ## Backlinks
      ##============================================================##
      match 'international'    => 'back_links#international', :via => [:get], :format => "html"
      ##============================================================##
      ## Authentication
      ## /auth
      ##============================================================##
      scope 'auth' do
        match 'sign_in'            => 'auth#sign_in', :via => :post
        match 'sign_out'           => 'auth#logout', :via => :delete
        match 'validate_token'     => 'auth#validate_token', :via => :get
      end
      ##============================================================##
      ## Print Iframe
      ##============================================================##
      scope 'print' do
        match "dictionary/:id"            => 'prints#dictionary',          :via => :post,          :as => "print_dictionary"
      end
      ##============================================================##
      ## Services
      ## /services
      ##============================================================##
      match "services/:service"               => 'services#activate',      :via => :put
      ##============================================================##
      ## Craigslist
      ## /craigslist/:id
      ##============================================================##
      match "craigslist/:id"    => 'properties#craigslist', :via => :get, :as => "craigslist"
      ##============================================================##
      ## Kijiji
      ## /kijiji/:id
      ##============================================================##
      match "kijiji/:id"    => 'properties#kijiji', :via => :get, :as => "kijiji"

      ##============================================================##
      ## Youtube
      ##============================================================##
      scope "youtube" do
        match ':id'           => 'youtube#send_infos',  :via => :get,   :as => "youtube_infos"
        match ':id/enhanced'  => 'youtube#enhanced',    :via => :get,   :as => "youtube_enhanced", :format => "html"
        match ':id/contact'   => 'youtube#contact',     :via => :post,  :as => "youtube_contact",  :format => "js"
      end
      ##============================================================##
      ##
      ##============================================================##
      match "magex/launcher"    => 'magex#launcher',      :via => :get

      ##============================================================##
      ## Syncer
      ##============================================================##
      match "sync/centris"      => 'sync#centris',      :via => :get
    end
  end



  ##============================================================##
  ## SEO
  ##============================================================##
  constraints(BrokerApp) do
    match 'robots.txt'   => 'front_office#robots', via: [:get] ,:as => 'broker_robots',  defaults: { format: "txt" }
    match 'sitemap.xml'  => 'front_office#sitemap',via: [:get] ,:as => 'broker_sitemap', defaults: { format: "xml" }
  end
  ##============================================================##
  ## Doit se trouver après les robots.txt des brokers
  ##============================================================##
  get "robots.txt"         => "pages#robots", as: "robots", defaults: { format: "txt" }
  get "sitemap.xml"        => "pages#sitemap", as: "sitemap", defaults: { format: "xml"}



  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users,
      :controllers => {
        :registrations => :registrations
      },
      :skip => [:sessions]
    devise_scope :user do
      match 'signin'      => 'sessions#create',         :via=>[:post],    :as => :user_session
      match 'signin'      => 'devise/sessions#new',     :via=>[:get],     :as => :new_user_session
      match 'logout'      => 'devise/sessions#destroy', :via=>[:delete],  :as => :destroy_user_session
    end

    ##============================================================##
    ## OAuth
    ##============================================================##
    match 'auth/:provider/callback'   => 'authentifications#create',   :via => [:get]
    match 'auth/failure'              => 'authentifications#fail',     :via => [:get]
    match 'auth/:id'                  => 'authentifications#destroy',  :via => [:delete] ,:as => 'dashboard_delete_app'



    ##============================================================##
    ## Front Office
    ##============================================================##
    constraints(BrokerApp) do
      get 'robots.txt'             ,:to => redirect { |params, request| "/robots.txt" }
      get 'sitemap.xml'            ,:to => redirect { |params, request| "/sitemap.xml" }
      match 'stylesheet'           => 'front_office#stylesheet',        :via => :get, :format => [:css]
      match ''                     => 'front_office#broker_home',       :via => [:get]
      match 'listings'             => 'front_office#listings',          :via => [:get],   :as => "broker_listing"
      match 'about'                => 'front_office#about',             :via => [:get],   :as => "broker_about"
      match 'blog'                 => 'front_office#blogs',             :via => [:get],   :as => "broker_blogs"
      match 'testimonial'          => 'front_office#testimonial',       :via => [:get],   :as => "broker_testimonial"
      match 'testimonial'          => 'front_office#testimonial_new',   :via => [:post],  :as => "broker_testimonial_new"
      match 'blog/cat/:id'         => 'front_office#blog_category',     :via => [:get],   :as => "broker_blog_category"
      match 'blog/:id'             => 'front_office#broker_blog',       :via => [:get],   :as => 'broker_blog'
      match 'contact/mail'         => 'front_office#contact_by_mail',   :via => [:post],  :as => 'contact_by_mail'
      match 'contact/sms'          => 'front_office#contact_by_sms',    :via => [:post],  :as => 'contact_by_sms'
      match ':id'                  => 'front_office#broker_property',   :via => [:get],   :as => 'broker_property'
    end

    ##============================================================##
    ## Short Link
    ##============================================================##
    match 'ad/:id'               => "pages#short_links", :via => [:get], :as => "short_link"

    match 'ad/original/:id'         => "pages#original_links", :via => [:get], :as => "original_link"


    ##============================================================##
    ## Map
    ##============================================================##
    match "explore"           => "front_office#explore",      :via => [:get], :as => 'explore_shareimmo'
    match "map"               => "map#index",                 :via => [:get], :as => 'map_shareimmo'
    match "explore/:id"       => "front_office#explore_ad",   :via => [:get], :as => 'explore_ad'

    ##============================================================##
    ## Files
    ##============================================================##
    scope 'files' do
      match ':type/:secure_id'                => 'files#show',                   :as => 'files_inventory',          :via => [:get, :post]
      match ':type/:secure_id/download'       => 'files#download',               :as => 'files_inventory_download', :via => :get
    end

    root 'pages#home'


    ##============================================================##
    ## Cette contrainte est pour forcer les www si ils ne sont
    ## pas présent....Normalement cette contrainte est inutile
    ## car la même redirection est faite par Nginx en amont
    ##============================================================##
    # constraints subdomain: false do
    #   match ':any', to: redirect(:subdomain=>'www', :path=>'/%{any}'), any: /.*/, via: :all
    # end

    ##============================================================##
    ## Cette methode autorise l'url de sideKiq que pour les
    ## adminisatreur du site
    ##============================================================##
    authenticate :user, lambda { |u| u.check_role? } do
      mount Sidekiq::Web => '/sidekiq'
    end

    ##============================================================##
    ## Manage Users
    ##============================================================##
    resources :users, :only =>[:destroy]

    ##============================================================##
    ## Cette contrainte redirige le sous-domaine, mail.share....
    ## à son compte google mail associé
    ##============================================================##
    constraints(SubdomainMail) do
      get '/' => redirect { |params| "https://www.google.com/a/shareimmo.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/a/shareimmo.com/"}
    end



    ##============================================================##
    ## Admin /  Monitoring
    ##============================================================##
    scope 'monitoring' do
      match ''                          => 'admin#monitoring',            via: [:get],        :as =>'monitoring'
      match 'in-business'               => 'admin#in_business',           via: [:get],        :as => "monitoring_in_business_ads"
      match 'on_youtube'                => 'admin#on_youtube',            via: [:get],        :as => "monitoring_on_youtube"
      match 'users'                     => 'admin#users',                 via: [:get],        :as =>'monitoring_users'
      match 'managers'                  => 'admin#managers',              via: [:get],        :as =>'monitoring_managers'
      match 'users/services'            => 'admin#users_services',        via: [:get],        :as =>'monitoring_users_services'
      match 'users/services/:id'        => 'admin#users_services_manage', via: [:get],        :as =>'monitoring_users_services_manage'
      match 'users/services_users'      => 'admin#users_services_update', via: [:post,:patch],:as =>'monitoring_users_services_update'
      match 'users/services_users/:id'  => 'admin#users_services_delete', via: [:delete],     :as =>'monitoring_users_services_delete'
      match 'scrapping'                 => 'admin#scrapping_monitoring',  via: [:get],        :as => "scrapping_monitoring"
      match 'youtube/regen/:id'         => 'admin#regen_youtube',         via: [:get],        :as => "monitoring_regen_youtube"
      match 'stats/properties'          => 'admin#stats_properties',      via: [:get],        :as => "admin_stats_properties"
      match 'stats/users'               => 'admin#stats_users',           via: [:get],        :as => "admin_stats_users"
      match 'stats/visibilities'        => 'admin#stats_visibilities',    via: [:get],        :as => "admin_stats_visibilities"
      match 'pdf'                       => 'admin#pdf',                   via: [:get],        :as => "admin_pdf"
      match 'pdf/:id'                   => 'admin#pdf_upload',            via: [:patch],      :as => "admin_pdf_upload"
      ##============================================================##
      ## Posts
      ##============================================================##
      match 'posts'     => 'admin#blog',          :via => :get,     :as => :blog_admin
      match 'posts/new' => "admin#post_new",      :via => :get,     :as => :post_new
      match 'posts/:id' => "admin#post_edit",     :via => :get,     :as => :post_edit
      match 'posts/:id' => "admin#post_update",   :via => :patch,   :as => :post_update
      match 'posts/:id' => "admin#post_destroy",  :via => :delete,  :as => :post_delete
      ##============================================================##
      ## Supports
      ##============================================================##
      match 'supports'      => 'admin#supports',        :via => :get,     :as => :support_admin
      match 'supports/new'  => "admin#support_new",     :via => :get,     :as => :support_new
      match 'supports/:id'  => "admin#support_edit",    :via => :get,     :as => :support_edit
      match 'supports/:id'  => "admin#support_update",  :via => :patch,   :as => :support_update
      match 'supports/:id'  => "admin#support_destroy", :via => :delete,  :as => :support_delete


    end



    resources :authentifications, :only=>[:destroy]
    match 'dashboard'                             => 'back_office#dashboard_redirect',via: [:get], :as=>'dashboard_redirect'


    ##============================================================##
    ## dashboard/:username
    ##============================================================##
    scope 'dashboard/:username' do
      ##============================================================##
      ## Dashboard
      ##============================================================##
      match ''                   => 'back_office#dashboard',via: [:get], :as =>'dashboard'


      ##============================================================##
      ## Intrenational
      ##============================================================##
      match 'international'    => 'back_office#international',via: [:get], :as=>'dashboard_international'

      ##============================================================##
      ## Billing
      ##============================================================##
      match 'checkout'                  => 'back_office#checkout',via: [:get] ,:as => 'dashboard_checkout'
      match 'paid'                      => 'back_office#checkout_paid',via: [:get] ,:as => 'dashboard_checkout_paid'





      ##============================================================##
      ## Inventories
      ##============================================================##
      match 'inventory/:id'               => 'back_office#inventory_manage',                      :via => [:post] ,   :as => 'dashboard_inventory_manage'
      match 'inventories'                 => 'back_office#inventories',                           :via => [:get] ,    :as => 'dashboard_inventories'
      match 'inventories/new'             => 'back_office#inventories_new',                       :via => [:get] ,    :as => 'dashboard_inventory_new'
      match 'inventories/destroy'         => 'back_office#inventories_destroy',                   :via => [:get] ,    :as => 'dashboard_inventory_destroy'
      match 'inventories/parameters'      => 'back_office#inventories_parameters',                :via => [:get] ,    :as => 'dashboard_inventory_parameters'
      match 'inventories/parameters'      => 'back_office#inventories_parameters_update',         :via => [:patch] ,  :as => 'dashboard_update_inventory_parameters'
      match 'inventories/:id'             => 'back_office#inventories_show',                      :via => [:get] ,    :as => 'dashboard_inventories_show'
      match 'inventories'                 => 'back_office#inventories_create',                    :via => [:post] ,   :as => 'dashboard_inventory_create'
      match 'glossary'                    => 'back_office#inventories_glossary',                  :via => :get,       :as => 'dashboard_glossary'
      match 'glossary'                    => 'back_office#inventories_glossary_new',              :via => :post,      :as => 'dashboard_new_glossary'
      match 'glossary/:id'                => 'back_office#inventories_glossary_delete',           :via => :delete,    :as => 'dashboard_remove_glossary_element'
      match 'glossary/:id'                => 'back_office#inventories_glossary_edit',             :via => :get,       :as => 'dashboard_edit_glossary_element'
      match 'glossary/:id'                => 'back_office#inventories_glossary_update',           :via => :patch,     :as => 'dashboard_update_glossary_element'
      match 'nomenclatures'               => 'back_office#inventories_nomenclatures',             :via => :get,       :as => 'dashboard_nomenclatures'
      match 'nomenclatures'               => 'back_office#inventories_nomenclature_new',          :via => :post,      :as => 'dashboard_new_nomenclature'
      match 'nomenclatures/import'        => 'back_office#inventories_nomenclature_import',       :via => :post,      :as => 'dashboard_import_nomenclature'
      match 'nomenclature/:id/edit'       => 'back_office#inventories_nomenclature_edit',         :via => :get,       :as => 'dashboard_edit_nomenclature'
      match 'nomenclature/:id'            => 'back_office#inventories_nomenclature_delete',       :via => :delete,    :as => 'dashboard_remove_nomenclature'
      match 'nomenclature/:id'            => 'back_office#inventories_nomenclature_update',       :via => :patch,     :as => 'dashboard_update_nomenclature'
      match 'nomenclature/:id'            => 'back_office#inventories_nomenclature_show',         :via => :get,       :as => 'dashboard_show_nomenclature'
      match 'nomenclature/d/:id'          => 'back_office#inventories_nomenclature_default_show', :via => :get,       :as => 'dashboard_show_nomenclature_default'
      match 'nomenclatures/element'       => 'back_office#inventories_nomenclature_element_new',  :via => :post,      :as => 'dashboard_new_nomenclature_element'
      match 'nomenclatures/sort'          => 'back_office#inventories_nomenclature_element_sort', :via => :post,      :as => 'dashboard_nomenclature_element_sort'
      match 'nomenclatures/element/:id'   => 'back_office#inventories_nomenclature_element_sort', :via => :patch,     :as => 'dashboard_nomenclature_element_update'
      match 'nomenclatures/element/:id'   => 'back_office#inventories_nomenclature_element_edit', :via => :get,       :as => 'dashboard_nomenclature_element_edit'

      ##============================================================##
      ## Buildings
      ##============================================================##
      match 'buildings'                              => 'back_office#buildings',via: [:get] ,:as => 'dashboard_buildings'
      match 'buildings/new'                          => 'back_office#building_new',via: [:get] ,:as => 'dashboard_building_new'
      match 'buildings/:secure_id'                   => 'back_office#building_delete',via: [:delete] ,:as => 'dashboard_delete_building'
      match 'buildings/:secure_id'                   => 'back_office#building_edit',via: [:get] ,:as => 'dashboard_edit_building'
      match 'buildings/:secure_id'                   => 'back_office#building_update',via: [:patch] ,:as => 'dashboard_building'
      match 'buildings/a/:secure_id'                 => 'back_office#building_new_ad_from_buiding',via: [:get] ,:as => 'dashboard_new_ad_from_building'
      match 'buildings/:secure_id/assets'            => 'back_office#create_building_asset',via: [:post] ,:as => 'dashboard_create_building_asset'
      match 'buildings/:secure_id/assets/:asset_id'  => 'back_office#update_building_asset',via: [:post] ,:as => 'dashboard_update_building_asset'
      match 'buildings/:secure_id/assets/:asset_id'  => 'back_office#delete_building_asset',via: [:delete] ,:as => 'dashboard_delete_building_asset'
      match 'buildings/:secure_id/pictures/sort'     => 'back_office#building_assets_sort',via: [:post] , :as => 'dashboard_sort_building_pictures'



      ##============================================================##
      ## Units
      ##============================================================##
      match 'units'                       => 'back_office#ads_edl',via: [:get], :as=>'dashboard_ads_edl'

      ##============================================================##
      ## Properties
      ##============================================================##
      match 'properties/new'                          => 'back_office#property_new',              via: [:get],        :as => 'dashboard_property_new'
      match 'properties/:secure_id'                   => 'back_office#property_edit',             via: [:get],        :as => 'dashboard_property_edit'
      match 'properties/:secure_id'                   => 'back_office#property_update',           via: [:patch],      :as => 'dashboard_property_update'
      match 'properties/:secure_id'                   => 'back_office#property_delete',           via: [:delete],     :as => 'dashboard_delete_property'
      match 'properties/:secure_id/assets'            => 'back_office#property_asset_create',     via: [:post],       :as => 'dashboard_property_asset_create'
      match 'properties/:secure_id/assets/:asset_id'  => 'back_office#property_asset_delete',     via: [:delete],     :as => 'dashboard_property_asset_delete'
      match 'properties/:secure_id/pictures/sort'     => 'back_office#property_assets_sort',      via: [:post],       :as => 'dashboard_sort_property_pictures'
      match 'properties/:secure_id/videos/sort'       => 'back_office#property_videos_sort',      via: [:post],       :as => 'dashboard_sort_property_videos'
      match 'properties/:secure_id/inclusions/:id'    => 'back_office#property_inclusion_update', via: [:patch],      :as => 'dashboard_property_inclusion_update'



      ##============================================================##
      ## Property Contact
      ##============================================================##
      match 'contacts'         => 'back_office#property_contacts',          via: [:get],       :as => 'dashboard_property_contacts'
      match 'contacts/new'     => 'back_office#property_contact_new',       via: [:get],       :as => 'dashboard_property_contact_new'
      match 'contacts/:id'     => 'back_office#property_contact_edit',      via: [:get],       :as => 'dashboard_property_contact_edit'
      match 'contacts/:id'     => 'back_office#property_contact_delete',    via: [:delete],    :as => 'dashboard_property_contact_delete'
      match 'contacts'         => 'back_office#property_contact_create',    via: [:post],      :as => 'dashboard_property_contact_create'
      match 'contacts/:id'     => 'back_office#property_contact_update',    via: [:patch],     :as => 'dashboard_property_contact_update'


      ##============================================================##
      ## Property Contact
      ##============================================================##
      match 'documents/:id/new' => 'back_office#property_document_new',       via: [:get],       :as => 'dashboard_property_document_new'
      match 'documents/:id'     => 'back_office#property_document_edit',      via: [:get],       :as => 'dashboard_property_document_edit'
      match 'documents/:id'     => 'back_office#property_document_delete',    via: [:delete],    :as => 'dashboard_property_document_delete'
      match 'documents/:id'     => 'back_office#property_document_update',    via: [:patch],     :as => 'dashboard_property_document_update'


      ##============================================================##
      ## Add Contact to Property
      ##============================================================##
      match 'properties/:id/contact'                => 'back_office#property_contact_property_ad',       via: [:post],      :as => 'dashboard_property_contact_property_add'
      match 'properties/:id/contact/:contact_id'    => 'back_office#property_contact_property_delete',   via: [:delete],    :as => 'dashboard_property_contact_property_delete'



      ##============================================================##
      ## share Properties
      ##============================================================##
      match 'properties/share/facebook/:id'     => 'back_office#share_facebook_step1',      via: [:get],        :as => 'dashboard_share_facebook_step1'
      match 'properties/share/facebook/:id'     => 'back_office#share_facebook_step2',      via: [:post],       :as => 'dashboard_share_facebook_step2'
      match 'properties/share/twitter/:id'      => 'back_office#share_twitter_step1',       via: [:get],        :as => 'dashboard_share_twitter_step1'
      match 'properties/share/twitter/:id'      => 'back_office#share_twitter_step2',       via: [:post],       :as => 'dashboard_share_twitter_step2'
      match 'properties/share/youtube/:id'      => 'back_office#share_youtube_step1',       via: [:get],        :as => 'dashboard_share_youtube_step1'

      ##============================================================##
      ## Visibility
      ##============================================================##
      match 'visibility/:id'      => 'back_office#visibility_manage',         via: [:get],          :as => 'dashboard_visibility_manage'
      match 'visibility/:id/s'    => 'back_office#visibility_show',           via: [:get],          :as => 'dashboard_visibility_show'
      match 'visibility/portal'   => 'back_office#visibility_update_portal',  via: [:post,:patch],  :as => 'dashboard_visibility_update_portal'

      ##============================================================##
      ## Services
      ##============================================================##
      match 'services'              => 'back_office#services_home',         via: [:get],          :as => 'dashboard_services_home'
      match 'services/display/:id'  => 'back_office#services_display',      via: [:patch],        :as => 'dashboard_services_display'
      match 'services/:service'     => 'back_office#services',              via: [:get],          :as => 'dashboard_services'
      match 'services/custom'       => 'back_office#services_add_custom',   via: [:post,:patch],  :as => 'dashboard_services_add_custom'
      match 'services/custom/:id'   => 'back_office#services_edit_custom',  via: [:post,:patch],  :as => 'dashboard_services_edit_custom'
      match 'services/custom/:id/d' => 'back_office#services_delete_custom',via: [:delete],       :as => 'dashboard_services_delete_custom'
      ##============================================================##
      ## Activate Services
      ##============================================================##
      match 'services/activate/:id'   => 'back_office#services_activation',  via: [:post],  :as => 'dashboard_services_activation'
      ##============================================================##
      ## Order Services
      ##============================================================##
      match 'services/order/:id/update'       => 'back_office#services_order_update',  via: [:patch],  :as => 'dashboard_services_order_update'
      match 'services/order/:id/:service_id'  => 'back_office#services_order',         via: [:get],   :as => 'dashboard_services_order'


      ##============================================================##
      ## Blog + testiomonials
      ##============================================================##
      match 'blog'                          => 'back_office#blog',via: [:get] ,:as => 'dashboard_blog'
      match 'blog/new'                      => 'back_office#blog_new',via: [:get] ,:as => 'dashboard_new_blog'
      match 'blog/:id/edit'                 => 'back_office#blog_edit',via: [:get] ,:as => 'dashboard_edit_blog'
      match 'blog/:id/'                     => 'back_office#blog_update',via: [:patch] ,:as => 'dashboard_update_blog'
      match 'blog/:id/'                     => 'back_office#blog_delete',via: [:delete] ,:as => 'dashboard_delete_blog'
      match 'blog/:id/picture'              => 'back_office#blog_remove_picture',via: [:get] ,:as => 'dashboard_blog_remove_picture'
      match 'testimonials'                  => 'back_office#testimonials',via: [:get] ,:as => 'dashboard_testimonials'
      match 'testimonials/:id'              => 'back_office#testimonials_delete',via: [:delete] ,:as => 'dashboard_testimonials_delete'
      match 'testimonials/:id'              => 'back_office#testimonials_active',via: [:post] ,:as => 'dashboard_testimonials_active'


      ##============================================================##
      ## Wysiwyg
      ##============================================================##
      match 'wysiwyg/pictures'        => 'back_office#wysiwyg_picture_manager',via: [:post] ,:as => 'dashboard_wysiwyg_picture_manager', :format => "json"
      match 'wysiwyg/pictures/d'      => 'back_office#wysiwyg_picture_delete',via: [:post] ,:as => 'dashboard_wysiwyg_picture_delete'



      ##============================================================##
      ## Feedback
      ##============================================================##
      match 'feedback/:secure_id'                     => 'back_office#feedback_manage',via: [:get] ,:as => 'dashboard_manage_feedback'
      match 'feedback/:secure_id/new'                 => 'back_office#feedback_new',via: [:get]    ,:as => 'dashboard_new_feedback'
      match 'feedback/:id/edit'                       => 'back_office#feedback_edit',via: [:get]    ,:as => 'dashboard_edit_feedback'
      match 'feedback/:id/visiting'                   => 'back_office#feedback_visiting',via: [:get]    ,:as => 'dashboard_visiting_feedback'
      match 'feedback/:id'                            => 'back_office#feedback_update',via: [:patch]    ,:as => 'dashboard_update_feedback'
      match 'feedback/:id'                            => 'back_office#feedback_destroy',via: [:delete]    ,:as => 'dashboard_delete_feedback'
      match 'feedback/:id/report'                     => 'back_office#feedback_report_new',via: [:get]    ,:as => 'dashboard_new_feedback_report'
      match 'report/send/:id'                         => 'back_office#feedback_report_send',via: [:patch]    ,:as => 'dashboard_send_feedback_report'


      ##============================================================##
      ## Apps & Portals
      ##============================================================##
      match 'apps'                      => 'back_office#apps',via: [:get] ,:as => 'dashboard_apps'
      match 'portals/:id'               => 'back_office#portal',via: [:get]    ,:as => 'dashboard_portal'
      match 'portals/d/:id'             => 'back_office#portal_delete',via: [:delete]    ,:as => 'dashboard_delete_portal'
      match 'portals/u/:id'             => 'back_office#portal_update',via: [:patch]    ,:as => 'dashboard_update_portal'


      ##============================================================##
      ## Cards
      ##============================================================##
      match 'cards'                           => 'back_office#cards',via: [:get] ,:as => 'dashboard_cards'
      match 'card/:stripe_id'                 => 'back_office#card_delete',via: [:get] ,:as => 'dashboard_delete_card'
      match 'card'                            => 'back_office#card_new',via: [:get] ,:as => 'dashboard_new_card'
      match 'card'                            => 'back_office#card_update',via: [:post] ,:as => 'dashboard_update_card'
      match 'cards/:id'                       => 'back_office#card_default',via: [:get] ,:as => 'dashboard_default_card'



      ##============================================================##
      ## Themes
      ##============================================================##
      match 'themes/:theme_id'                => 'back_office#set_theme',             via: [:post],    :as => 'dashboard_set_theme'
      match 'themes/:theme_id'                => 'back_office#theme_set_colors',      via: [:get],     :as => 'dashboard_set_theme_colors'
      match 'themes/:theme_id/update'         => 'back_office#update_theme_setting',  via: [:patch],   :as => 'update_theme_setting'
      match 'themes/:theme_id/update'         => 'back_office#theme_remove_cover',    via: [:delete],  :as => 'dashboard_theme_remove_cover'

      ##============================================================##
      ## Options
      ##============================================================##
      match 'hosting-checker'                 => 'back_office#hosting_checker',via: [:post] ,:as => 'dashboard_hosting_checker'
      match 'hosting/new'                     => 'back_office#hosting_new',via: [:post] ,:as => 'dashboard_hosting_new'
      match 'options'                         => 'back_office#options',via: [:get] ,:as => 'dashboard_options'
      match 'hosting/:hosting'                => 'back_office#delete_hosting',via: [:delete] ,:as => 'dashboard_remove_hosting'
      match 'hosting/:hosting/detail'         => 'back_office#hosting_detail',via: [:get] ,:as => 'dashboard_hosting_detail'
      match 'hosting/:id'                     => 'back_office#hosting_update',via: [:patch] ,:as => 'dashboard_update_hosting'


      ##============================================================##
      ## Settings
      ##============================================================##
      match 'profile'                   => 'back_office#profile',via: [:get] ,:as => 'dashboard_profile'
      match 'profile/remove/picture'    => 'back_office#profile_remove_picture',via: [:get] ,:as => 'dashboard_profile_remove_picture'
      match 'profile/remove/logo'       => 'back_office#profile_remove_logo',via: [:get] ,:as => 'dashboard_profile_remove_logo'
      match 'profile'                   => 'back_office#profile_update',via: [:patch] ,:as => 'dashboard_profile_update'
      match 'user'                      => 'back_office#user_update',via: [:patch] ,:as => 'dashboard_user_update'
      match 'account'                   => 'back_office#account',via: [:get] ,:as => 'dashboard_account'
    end




    ##============================================================##
    ## Statics Path
    ##============================================================##
    match 'magex'                 => 'pages#magex',             :via => :get
    match 'tour'                  => 'pages#tour',              :via => :get
    match 'terms'                 => 'pages#terms',             :via => :get
    match 'contact'               => 'pages#contact',           :via => :get
    match 'contact/a'             => 'pages#contact_create',    :via => :post,  :as => "create_contact"
    match 'blog'                  => 'pages#blog',              :via => :get,   :as => "blog"
    match 'blog/:id'              => 'pages#blog_show',         :via => :get,   :as => "blog_show"
    match 'support'               => 'pages#support_index',     :via => :get,   :as => :support
    match 'support/:id'           => 'pages#support_type_show', :via => :get,   :as => :support_type
    match 'support/:support/:id'  => 'pages#support_show',      :via => :get,   :as => :support_type_support


    ##============================================================##
    ## Tools
    ##============================================================##
    match 'tools'                   => 'tools#index',             :via => :get,   :as => :tools
    match 'tools/html-to-text'      => 'tools#htmltotext',        :via => :get,   :as => :html_to_text
    match 'tools/html-to-text'      => 'tools#htmltotext_parser', :via => :post,  :as => :html_to_text_parser
    match 'tools/geocoder'          => 'tools#geocoder',          :via => :get,   :as => :tools_geocoder
    match 'tools/geocoder'          => 'tools#geocoder_find',     :via => :post,  :as => :tools_geocoder_find

    ##============================================================##
    ## Docs
    ##============================================================##
    match 'docs'                  => 'pages#docs',              :via => :get,   :as => :docs
    match 'docs/api'              => 'pages#docs_api',          :via => :get,   :as => :docs_api




    unless Rails.application.config.consider_all_requests_local
      match '*not_found', to: 'errors#error_404', via: :all
    end
  end












  unless Rails.application.config.consider_all_requests_local
    match '*not_found', to: 'errors#error_404', via: :all
  end


end



