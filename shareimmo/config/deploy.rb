set :application, 'shareimmo'
set :repo_url, 'git@bitbucket.org:wgroupe/shareimmo.git'
set :bundle_roles, :all
set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{log tmp/pids tmp/sockets tmp/pdfs video_generator}
set :keep_releases, 6
set :keep_assets, 2
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

