class TranslatePropertyContacts < ActiveRecord::Migration
  def change
    PropertyContactType.create_translation_table!({
      :name => :string,
    }, {
      :migrate_data => true
    })
  end
end

