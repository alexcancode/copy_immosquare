class AddDefaultValueToProfilesFont < ActiveRecord::Migration
  def change
    change_column :profiles, :font_id, :integer, :default => 1
    change_column :profiles, :font_special_id, :integer, :default => 1
  end
end
