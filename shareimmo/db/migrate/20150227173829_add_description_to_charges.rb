class AddDescriptionToCharges < ActiveRecord::Migration
  def change
    add_column :charges, :description, :text, :after => :coins
  end
end
