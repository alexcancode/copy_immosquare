class AddTax1Tax2ToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :tax1, :text
    add_column :plans, :tax2, :text
  end
end
