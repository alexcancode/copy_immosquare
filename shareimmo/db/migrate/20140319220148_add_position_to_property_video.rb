class AddPositionToPropertyVideo < ActiveRecord::Migration
  def change
    add_column :property_videos, :position, :integer
  end
end
