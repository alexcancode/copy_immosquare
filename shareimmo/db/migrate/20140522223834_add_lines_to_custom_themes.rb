class AddLinesToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :line_1_text, :string
    add_column :custom_themes, :line_1_visibility, :boolean, :default => true
    add_column :custom_themes, :line_2_text, :string
    add_column :custom_themes, :line_2_visibility, :boolean, :default => true
    add_column :custom_themes, :line_3_text, :string
    add_column :custom_themes, :line_3_visibility, :boolean, :default => true
  end
end
