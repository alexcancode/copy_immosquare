class AddPetsAllowedToProperty < ActiveRecord::Migration
  def change
    add_column :properties, :pets_allowed, :integer
  end
end
