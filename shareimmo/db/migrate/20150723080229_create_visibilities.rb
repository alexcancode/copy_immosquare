class CreateVisibilities < ActiveRecord::Migration
  def change
    create_table :visibilities do |t|
      t.references :property, index: true, foreign_key: false
      t.datetime :validity_until
      t.integer :international_posting_status
      t.integer :video_posting_status
      t.integer :kijiji_posting_status
      t.integer :kijiji_bump
      t.integer :kijiji_ad_id
      t.string  :craigslist_ad_url
      t.integer :craigslist_posting_status
      t.integer :lespac_posting_status
      t.integer :lespac_ad_id
      t.string  :lespac_ad_preview_id
      t.integer :louer_posting_status
      t.string  :louer_ad_id



      t.timestamps null: false
    end
  end
end
