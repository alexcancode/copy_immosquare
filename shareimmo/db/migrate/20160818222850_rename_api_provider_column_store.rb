class RenameApiProviderColumnStore < ActiveRecord::Migration
  def change
    rename_column :api_providers, :is_display_on_sync_with, :is_storeimmo_provider
  end
end
