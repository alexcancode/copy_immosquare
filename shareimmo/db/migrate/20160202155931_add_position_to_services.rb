class AddPositionToServices < ActiveRecord::Migration
  def change
    add_column :services, :position, :integer, :after => :id
  end
end
