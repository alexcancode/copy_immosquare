class AddSocialsToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :facebook, :string
    add_column :profiles, :twitter, :string
    add_column :profiles, :linkedin, :string
    add_column :profiles, :pinterest, :string
    add_column :profiles, :google_plus, :string
  end
end
