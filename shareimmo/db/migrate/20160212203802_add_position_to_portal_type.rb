class AddPositionToPortalType < ActiveRecord::Migration
  def change
    add_column :portal_types, :position, :integer, :after => :id
  end
end
