class AddReviewStatusToAuthentifications < ActiveRecord::Migration
  def change
    add_column :authentifications, :review_status, :integer,:default=>2
  end
end
