class AddIsAfluxToApiProvider < ActiveRecord::Migration
  def change
    add_column :api_providers, :is_a_flux, :integer, :after => :commercial_name
  end
end
