class AddUserIdToCustomThemes < ActiveRecord::Migration
  def change
    add_reference :custom_themes, :user, index: true
    remove_column :custom_themes, :profile_id
  end
end
