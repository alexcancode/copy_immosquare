class RemoveStatusToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :status, :integer
    add_column :properties, :mini_site_status, :integer, :default=>0, :after=>:showerrooms
    change_column :properties, :pets_allowed, :integer, :after=> :mls
  end
end
