class AddSeoToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :google_analytics, :string
  end
end
