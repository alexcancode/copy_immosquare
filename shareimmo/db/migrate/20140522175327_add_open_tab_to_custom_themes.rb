class AddOpenTabToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :open_tab, :integer, :default => 0
  end
end
