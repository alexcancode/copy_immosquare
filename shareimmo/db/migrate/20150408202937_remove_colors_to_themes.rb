class RemoveColorsToThemes < ActiveRecord::Migration
  def change
    remove_column :themes, :primary_color, :string
    remove_column :themes, :secondary_color, :string
    remove_column :themes, :tertiary_color, :string
    remove_column :theme_settings, :primary_color, :string
    remove_column :theme_settings, :secondary_color, :string
    remove_column :theme_settings, :tertiary_color, :string
    remove_column :theme_settings, :stylesheet_url, :string
    remove_column :theme_settings, :text_1, :string
    remove_column :theme_settings, :text_2, :string
    remove_column :theme_settings, :text_3, :string
  end
end
