class DropSubscriptions < ActiveRecord::Migration
  def change
    drop_table :subscriptions
    drop_table :stripe_charges
  end
end
