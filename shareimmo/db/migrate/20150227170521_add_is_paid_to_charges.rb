class AddIsPaidToCharges < ActiveRecord::Migration
  def change
    add_column :charges, :is_paid, :integer, :default => 0, :after => :user_id
  end
end
