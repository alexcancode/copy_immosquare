class RenameIsPocesToPurrchases < ActiveRecord::Migration
  def change
    rename_column :purchases, :is_processing, :need_check
  end
end
