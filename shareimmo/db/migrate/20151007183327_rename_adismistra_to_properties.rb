class RenameAdismistraToProperties < ActiveRecord::Migration
  def change
     rename_column :properties, :administrative_area_level_1_long, :administrative_area_level_1
     rename_column :properties, :administrative_area_level_1_short, :administrative_area_level_2
  end
end
