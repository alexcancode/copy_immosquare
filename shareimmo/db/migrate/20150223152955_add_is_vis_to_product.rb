class AddIsVisToProduct < ActiveRecord::Migration
  def change
    add_column :products, :is_visibility_prodcut, :integer, :default => 0 , :after => :cost
    add_column :products, :visibilities, :text, :after => :slug
  end
end
