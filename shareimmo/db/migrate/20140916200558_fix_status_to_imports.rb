class FixStatusToImports < ActiveRecord::Migration
  def change
    change_column :imports, :status, :integer, :default => 0
  end
end
