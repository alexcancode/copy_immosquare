class AddInfosIiToPrintOrders < ActiveRecord::Migration
  def change
    add_column :print_orders, :images, :text, :after => :primary_color
    add_column :print_orders, :texts, :text, :after => :images
    remove_column :print_orders, :order_infos
  end
end
