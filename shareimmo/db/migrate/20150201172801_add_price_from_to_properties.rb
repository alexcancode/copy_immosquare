class AddPriceFromToProperties < ActiveRecord::Migration
  def change
    add_money :properties, :price_from, currency: { present: false }, amount: { null: true, default: nil }, :after => :price_currency
    add_column :properties, :price_from_currency, :string, :after => :price_from_cents
  end
end
