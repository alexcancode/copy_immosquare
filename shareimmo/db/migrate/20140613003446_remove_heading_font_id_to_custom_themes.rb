class RemoveHeadingFontIdToCustomThemes < ActiveRecord::Migration
  def change
    remove_column :custom_themes, :heading_font_id, :integer
  end
end
