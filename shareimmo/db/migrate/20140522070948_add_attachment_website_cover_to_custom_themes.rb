class AddAttachmentWebsiteCoverToCustomThemes < ActiveRecord::Migration
  def self.up
    change_table :custom_themes do |t|
      t.attachment :website_cover
    end
  end

  def self.down
    drop_attached_file :custom_themes, :website_cover
  end
end
