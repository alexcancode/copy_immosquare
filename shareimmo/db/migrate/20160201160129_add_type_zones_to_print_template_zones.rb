class AddTypeZonesToPrintTemplateZones < ActiveRecord::Migration
  def change
    add_column :print_template_zones, :type_zone, :string, :after => :print_template_id , :default => "A"
  end
end
