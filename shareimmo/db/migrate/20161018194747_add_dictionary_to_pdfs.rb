class AddDictionaryToPdfs < ActiveRecord::Migration
  def change
    add_column :pdfs, :dictionary, :text, :after => :name
  end
end
