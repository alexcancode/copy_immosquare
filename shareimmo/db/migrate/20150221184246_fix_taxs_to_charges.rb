class FixTaxsToCharges < ActiveRecord::Migration
  def change
    change_column :charges, :tax1, :text
    change_column :charges, :tax2, :text
  end
end
