# encoding: utf-8

class AddInfosToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :job, :string, :default => "Courtier immobilier agrée"
    add_column :profiles, :languages, :string, :default => 'Fraçais Anglais'
    add_column :profiles, :bio, :text
    add_column :profiles, :qsc, :int, :default =>'4.5'
    add_column :profiles, :tel, :string, :default => '+1 (514) 123-4567'
    add_column :profiles, :facebook, :string, :default => 'https://www.facebook.com/Wgroupe.official?ref=hl'
    add_column :profiles, :twitter, :string, :default=>'https://twitter.com/jules_welsch'
    change_column :profiles, :first_name, :string, :default => 'mon prénom'
    change_column :profiles, :last_name, :string, :default => 'mon nom'
    change_column :profiles, :website, :string, :default => 'www.mon-site-web.com'
  end
end
