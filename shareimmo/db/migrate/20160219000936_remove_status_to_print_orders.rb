class RemoveStatusToPrintOrders < ActiveRecord::Migration
  def change
    remove_column :print_orders, :status, :string
    add_column :print_orders, :preview_count, :integer, :default => 0, :after =>:print_template_id
  end
end
