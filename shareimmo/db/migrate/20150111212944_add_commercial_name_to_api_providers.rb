class AddCommercialNameToApiProviders < ActiveRecord::Migration
  def change
    add_column :api_providers, :commercial_name, :string, :after => :name
  end
end
