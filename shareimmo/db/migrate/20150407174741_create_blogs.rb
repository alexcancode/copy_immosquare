class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.references :user, index: true
      t.references :blog_category, index: true
      t.integer :draft, :default=> 1
      t.string :locale
      t.string :title
      t.string :slug
      t.string :content

      t.timestamps null: false
    end
    # add_foreign_key :blogs, :users
    # add_foreign_key :blogs, :blog_categories
  end
end
