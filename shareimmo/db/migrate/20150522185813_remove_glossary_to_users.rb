class RemoveGlossaryToUsers < ActiveRecord::Migration
  def change
    remove_reference :users, :glossary, index: true, foreign_key: false
  end
end
