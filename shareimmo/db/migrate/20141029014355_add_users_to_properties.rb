class AddUsersToProperties < ActiveRecord::Migration
  def change
    rename_column :properties, :website_id, :user_id
  end
end
