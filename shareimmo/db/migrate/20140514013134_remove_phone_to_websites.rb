class RemovePhoneToWebsites < ActiveRecord::Migration
  def change
    remove_column :websites, :phone, :string
  end
end
