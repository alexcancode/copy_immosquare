class AddDefaultToProfiles < ActiveRecord::Migration
  def change
    change_column :profiles, :country_name, :string, :default =>"Canada"
    change_column :profiles, :spoken_languages, :string, :default =>"fr,en"
  end
end
