class AddAttachmentPictureToWysiwygPictures < ActiveRecord::Migration
  def self.up
    change_table :wysiwyg_pictures do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :wysiwyg_pictures, :picture
  end
end
