class AddInfosPartIiToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :customer_type, :integer, :default => 1
    add_column :profiles, :spoken_languages, :string
    add_column :profiles, :agency_name, :string
    add_column :profiles, :agency_address, :string
    add_column :profiles, :agency_city, :string
    add_column :profiles, :agency_phone, :string
    add_column :profiles, :agency_email, :string
    add_column :profiles, :agency_zipcode, :string
    add_column :profiles, :agency_website, :string
    add_column :profiles, :agency_first_name, :string
    add_column :profiles, :agency_last_name, :string
    add_column :profiles, :agency_spoken_languages, :string
  end
end
