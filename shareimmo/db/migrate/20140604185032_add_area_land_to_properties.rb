class AddAreaLandToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :area_land, :integer
  end
end
