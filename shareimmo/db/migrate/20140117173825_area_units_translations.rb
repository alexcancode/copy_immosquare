class AreaUnitsTranslations < ActiveRecord::Migration
  def self.up
    AreaUnit.create_translation_table!({
      :name => :string,
      :ref => :string,
      :symbol => :string
      }, {
        :migrate_data => true
        })
  end

  def self.down
    AreaUnit.drop_translation_table! :migrate_data => true
  end

end
