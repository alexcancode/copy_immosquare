class RemoveKijijiExtraStatusToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :kijiji_extra_status, :string
    remove_column :properties, :solihome_posting_status, :string
  end
end
