class AddInfosToAuthentifications < ActiveRecord::Migration
  def change
    add_reference :authentifications, :api_provider, index: true
    remove_column :authentifications, :provider
    add_column :authentifications, :custom_field_1, :string
    add_column :authentifications, :custom_field_2, :string
    add_column :authentifications, :custom_field_3, :string
  end
end
