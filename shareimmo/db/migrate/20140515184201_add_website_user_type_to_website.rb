class AddWebsiteUserTypeToWebsite < ActiveRecord::Migration
  def change
    add_reference :websites, :website_user_type, index: true
  end
end
