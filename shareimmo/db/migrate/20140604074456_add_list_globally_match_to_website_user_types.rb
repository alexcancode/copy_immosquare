class AddListGloballyMatchToWebsiteUserTypes < ActiveRecord::Migration
  def change
    add_column :website_user_types, :list_globally_match, :string
  end
end
