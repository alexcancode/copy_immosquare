class AddKijijiExtraToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :kijiji_extra_status, :integer, :default => 0, :after => :kijiji_posting_status
  end
end
