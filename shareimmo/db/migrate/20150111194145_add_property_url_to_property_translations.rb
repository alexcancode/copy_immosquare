class AddPropertyUrlToPropertyTranslations < ActiveRecord::Migration
  def change
    add_column :property_translations, :property_url, :string
  end
end
