class RenameInclusionsPartIi < ActiveRecord::Migration
  def change
    change_column :property_inclusions, :created_at, :datetime, :after => :security_smoke_dectector
    change_column :property_inclusions, :updated_at, :datetime, :after => :created_at
    change_column :property_inclusions, :pets_allow, :integer, :after => :senior_housing_cooperative
    change_column :property_inclusions, :detail_garagebox, :integer, :after => :detail_parking_on_street
    change_column :property_inclusions, :exterior_veranda, :integer, :after => :exterior_terrace
    change_column :property_inclusions, :exterior_garden, :integer, :after => :exterior_veranda


  end
end
