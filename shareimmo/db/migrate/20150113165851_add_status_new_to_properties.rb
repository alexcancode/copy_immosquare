class AddStatusNewToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :status, :integer, :default => 1, :after => :user_id
  end
end
