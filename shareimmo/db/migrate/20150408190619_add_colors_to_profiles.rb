class AddColorsToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :primary_color, :string, :default => "#f54029"
    add_column :profiles, :secondary_color, :string, :default => "#f54029"
    add_column :profiles, :tertiary_color, :string, :default => "#f54029"
    add_reference :profiles, :font, index: true
  end
end
