class AddInfosToPropertyContacts < ActiveRecord::Migration
  def change
    add_column :property_contacts, :address, :string
    add_column :property_contacts, :zipcode, :string
    add_column :property_contacts, :city, :string
    add_column :property_contacts, :country, :string
  end
end
