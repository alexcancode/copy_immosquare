class DropVideoProvider < ActiveRecord::Migration
  def change
    drop_table :video_providers
  end
end
