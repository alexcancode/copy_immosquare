class AddTaxsToProductsStores < ActiveRecord::Migration
  def change
    add_column :product_stores, :tax1, :text
    add_column :product_stores, :tax2, :text
  end
end
