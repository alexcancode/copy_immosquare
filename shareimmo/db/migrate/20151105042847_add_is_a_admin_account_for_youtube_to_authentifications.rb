class AddIsAAdminAccountForYoutubeToAuthentifications < ActiveRecord::Migration
  def change
    add_column :authentifications, :is_account_for_video_worker, :integer, :default => 0, :after => :user_id
  end
end
