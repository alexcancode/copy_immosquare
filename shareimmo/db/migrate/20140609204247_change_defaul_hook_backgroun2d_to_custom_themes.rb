class ChangeDefaulHookBackgroun2dToCustomThemes < ActiveRecord::Migration
  def change
    change_column :custom_themes, :avatar_position, :integer, :default =>30
    change_column :custom_themes, :hook_position_x, :integer, :default =>60
    change_column :custom_themes, :hook_position_y, :integer, :default =>30
  end
end
