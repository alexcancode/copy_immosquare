class AddSecureIdForInventory < ActiveRecord::Migration
  def change
    add_column :inventories, :secure_id, :string, :after => :user_id
    add_column :inventories, :secure_password, :string, :after => :secure_id
  end
end
