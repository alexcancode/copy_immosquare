class AddDraftToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :draft, :boolean
    change_column :properties, :status, :boolean, :default =>true
  end
end
