class AddImmotopMatchToPropertyClassification < ActiveRecord::Migration
  def change
    add_column :property_classifications, :immotop_match, :string
  end
end
