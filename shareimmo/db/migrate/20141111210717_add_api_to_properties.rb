class AddApiToProperties < ActiveRecord::Migration
  def change
    add_reference :properties, :api_provider, index: true, :after => :id
    add_column :properties, :uid, :integer, :after => :id
  end
end
