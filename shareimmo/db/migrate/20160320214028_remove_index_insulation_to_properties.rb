class RemoveIndexInsulationToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :index_insulation, :string
    remove_column :properties, :index_energy, :string
  end
end
