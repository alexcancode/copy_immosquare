class AddMissingFieldsForVisibilityPortal < ActiveRecord::Migration
  def change
    add_column :visibility_portals, :visibility_until , :date, :after => :id
    add_column :visibility_portals, :visibility_start , :date, :after => :id
    add_reference :visibility_portals, :property, :after => :id, index: true, foreign_key: false
  end
end
