class AddInfosToBuildings < ActiveRecord::Migration
  def change
    remove_column :buildings, :address
    add_column :buildings, :number_of_levels, :integer
    add_column :buildings, :street_number, :string
    add_column :buildings, :street_name, :string
    add_column :buildings, :zipcode, :string
    add_column :buildings, :sublocality, :string
    add_column :buildings, :city, :string
    add_column :buildings, :province, :string
    add_column :buildings, :country, :string
  end
end
