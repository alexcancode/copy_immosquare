class CleanSearchModel < ActiveRecord::Migration
  def change
    add_column :searches, :property_listing, :integer
    add_column :searches, :property_status, :integer
  end
end
