class AddSomeFieldsInApi < ActiveRecord::Migration
  def change
    add_column :property_inclusions, :exterior_sea_view, :integer, :after => :exterior_garden, :default => 0
    add_column :property_inclusions, :exterior_mountain_view, :integer, :after => :exterior_sea_view, :default => 0
    add_column :property_inclusions, :detail_golf, :integer, :after => :detail_gym, :default => 0
  end
end
