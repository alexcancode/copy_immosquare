class RemovePropertyidToPropertyContacts < ActiveRecord::Migration
  def change
    remove_reference :property_contacts, :property, index: true, foreign_key: false
  end
end
