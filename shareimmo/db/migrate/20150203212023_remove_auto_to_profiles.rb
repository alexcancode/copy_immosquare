class RemoveAutoToProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :auto_update_profile, :integer
  end
end
