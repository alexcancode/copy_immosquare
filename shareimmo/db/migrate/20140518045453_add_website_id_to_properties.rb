class AddWebsiteIdToProperties < ActiveRecord::Migration
  def change
    add_reference :properties, :website, index: true
    remove_reference :properties, :profile, index: true
  end
end
