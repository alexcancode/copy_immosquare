class AddBackgroundColorToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :background_color, :string, :default => '#f5f7fa'
    add_column :custom_theme_presets, :background_color, :string
  end
end
