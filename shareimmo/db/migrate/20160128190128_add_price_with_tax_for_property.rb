class AddPriceWithTaxForProperty < ActiveRecord::Migration
  def change
    add_column :properties, :price_with_tax, :integer, :after => :price_cents, :default => 0
  end
end
