class FixTotalToInvoices < ActiveRecord::Migration
  def change
    change_column :invoices, :total, :integer
  end
end
