class AddTaxsToCharges < ActiveRecord::Migration
  def change
    add_column :charges, :tax1, :float
    add_column :charges, :tax2, :float
    add_column :charges, :tax3, :float
  end
end
