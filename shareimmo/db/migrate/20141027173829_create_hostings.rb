class CreateHostings < ActiveRecord::Migration
  def change
    create_table :hostings do |t|
      t.references :user, index: true
      t.string :domain
      t.date :start_on
      t.timestamps
    end
  end
end
