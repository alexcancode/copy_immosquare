class AddCountToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :count_broker, :integer, :default => 0, :after => :property_source_id
    add_column :properties, :count_explore, :integer, :default => 0, :after => :count_broker
  end
end
