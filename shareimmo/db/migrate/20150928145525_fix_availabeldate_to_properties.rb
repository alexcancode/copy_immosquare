class FixAvailabeldateToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :availability_date, :date
  end
end
