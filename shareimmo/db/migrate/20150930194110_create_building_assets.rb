class CreateBuildingAssets < ActiveRecord::Migration
  def change
    create_table :building_assets do |t|
      t.integer :position
      t.references :building, index: true, foreign_key: false
      t.string :name
      t.timestamps null: false
    end
  end
end
