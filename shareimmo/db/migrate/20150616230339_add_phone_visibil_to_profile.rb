class AddPhoneVisibilToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :phone_visibility, :integer, :after => :phone, :default => 1
  end
end
