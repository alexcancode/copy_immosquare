class AddOwnerToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :owner_name, :string, :after=> :income_currency
    add_column :properties, :owner_phone, :string, :after => :owner_name
    add_column :properties, :owner_mail, :string, :after => :owner_phone
  end
end
