class CreateCustomThemePresets < ActiveRecord::Migration
  def change
    create_table :custom_theme_presets do |t|
      t.string :name
      t.string :website_background_color
      t.string :top_toolbar_background_color
      t.string :top_toolbar_text
      t.string :link_color
      t.string :nav_link_color
      t.string :text_color
      t.string :heading_text_color
      t.string :button_background_color
      t.string :border_color
      t.integer :font_id
      t.string :well_background_color
      t.string :description_color
      t.string :description_title_color
      t.string :price_color

      t.timestamps
    end
  end
end
