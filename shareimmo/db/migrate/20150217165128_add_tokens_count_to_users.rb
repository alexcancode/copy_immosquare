class AddTokensCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :token_count, :integer, :default => 0, :after => :api_provider_id
  end
end
