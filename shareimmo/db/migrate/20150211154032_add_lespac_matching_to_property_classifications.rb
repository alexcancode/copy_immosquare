class AddLespacMatchingToPropertyClassifications < ActiveRecord::Migration
  def change
    add_column :property_classifications, :lespac_match, :string, :after => :kijiji_match
  end
end
