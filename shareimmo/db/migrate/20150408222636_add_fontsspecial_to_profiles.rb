class AddFontsspecialToProfiles < ActiveRecord::Migration
  def change
    add_reference :profiles, :font_special, index: true, :after => :font_id
  end
end
