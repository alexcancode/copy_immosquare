class CreateChats < ActiveRecord::Migration
  def change
    create_table :chats do |t|
      t.references :user, index: true
      t.references :customer, index: true

      t.timestamps
    end
  end
end
