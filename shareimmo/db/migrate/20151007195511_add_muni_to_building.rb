class AddMuniToBuilding < ActiveRecord::Migration
  def change
    rename_column :buildings, :administrative_area_level_1_long, :administrative_area_level_1
    add_column :buildings, :administrative_area_level_2, :string, :after => :administrative_area_level_1
  end
end
