class AddDurationToProducts < ActiveRecord::Migration
  def change
    add_column :products, :duration, :integer, :default => 30, :after => :cost
  end
end
