class AddDescriptionShortToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :description_short, :text
    add_column :property_translations, :description_short, :text
  end
end
