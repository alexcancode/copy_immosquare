class AddSecureWebsiteTokenToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :secure_website_token, :string
  end
end
