class AddInfosPartIiToPlans < ActiveRecord::Migration
  def change
    add_reference :plans, :role, index: true
    add_column :plans, :stripe_id, :string
  end
end
