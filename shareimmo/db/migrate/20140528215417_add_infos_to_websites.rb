class AddInfosToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :prefix_phone, :string
    add_column :websites, :contact_phone_2, :string
    add_column :websites, :contact_email, :string
    add_column :websites, :contact_job, :string
    add_column :websites, :contact_languages, :string
    add_column :websites, :business_phone_2, :string
    add_column :websites, :bio, :text
    add_column :websites, :facebook_link, :string
    add_column :websites, :twitter_link, :string
    add_column :websites, :linkedin_link, :string
    add_column :websites, :pinterest_link, :string
    add_column :websites, :google_plus_link, :string
    add_column :websites, :website_url, :string
    add_column :websites, :country, :string
  end
end
