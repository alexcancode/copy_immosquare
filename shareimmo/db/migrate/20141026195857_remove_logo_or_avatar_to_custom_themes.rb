class RemoveLogoOrAvatarToCustomThemes < ActiveRecord::Migration
  def change
    remove_column :custom_themes, :logo_or_avatar, :integer
  end
end
