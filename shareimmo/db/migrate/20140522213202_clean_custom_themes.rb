class CleanCustomThemes < ActiveRecord::Migration
  def change
    remove_column :custom_themes, :full_name
    remove_column :custom_themes, :baseline
    remove_column :custom_themes, :phone
    remove_column :custom_themes, :hook_background_color_id
    remove_column :custom_themes, :hook_color_id
  end
end
