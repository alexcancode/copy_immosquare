class AddApiProviderToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :api_provider, index: true
  end
end
