class RenameStatusAndListintToProperty < ActiveRecord::Migration
  def change
    rename_column :properties, :property_state_id, :property_status_id
    rename_column :properties, :listing_type_id, :property_listing_id
  end
end
