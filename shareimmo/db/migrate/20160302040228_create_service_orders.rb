class CreateServiceOrders < ActiveRecord::Migration
  def change
    create_table :service_orders do |t|
      t.references :user, index: true, foreign_key: false
      t.references :property, index: true, foreign_key: false
      t.references :service, index: true, foreign_key: false
      t.integer :status, :default => 0

      t.timestamps null: false
    end
  end
end
