class AddListGloballyMatchToPropertyClassifications < ActiveRecord::Migration
  def change
    add_column :property_classifications, :list_globally_match, :string
  end
end
