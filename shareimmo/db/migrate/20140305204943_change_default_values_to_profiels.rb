class ChangeDefaultValuesToProfiels < ActiveRecord::Migration
  def change
    change_column :profiles, :first_name, :string, :default => nil
    change_column :profiles, :last_name, :string,:default => nil
    change_column :profiles, :website, :string,:default => nil
    change_column :profiles, :job, :string,:default => nil
    change_column :profiles, :languages, :string,:default => nil
    change_column :profiles, :qsc, :string,:default => nil
    change_column :profiles, :bio, :string,:default => nil
    change_column :profiles, :tel,:string,:default => nil
    change_column :profiles, :facebook, :string,:default => nil
    change_column :profiles, :twitter, :string,:default => nil
  end
end
