class AddAttachmentBusinessLogoToWebsites < ActiveRecord::Migration
  def self.up
    change_table :websites do |t|
      t.attachment :business_logo
    end
  end

  def self.down
    drop_attached_file :websites, :business_logo
  end
end
