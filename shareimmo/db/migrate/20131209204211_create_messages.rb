class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :chat, index: true
      t.boolean :customer
      t.string :message, :limit => 1000

      t.timestamps
    end
  end
end
