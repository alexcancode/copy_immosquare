class AddOriginalUrlToBuildingAssets < ActiveRecord::Migration
  def change
    add_column :building_assets, :original_url, :string
  end
end
