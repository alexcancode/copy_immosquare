class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string  :slug
      t.string  :name
      t.string  :company
      t.integer :on_shareimmo,  :default => 0
      t.integer :on_1clic,      :default => 0
      t.integer :on_kangalou,   :default => 0
      t.integer :on_shelterr,   :default => 0
      t.integer :on_zoomvisit,  :default => 0
      t.integer :on_immopad,    :default => 0
      t.timestamps null: false
    end
  end
end
