class RenameApiProviderId < ActiveRecord::Migration
  def change
    rename_column :properties, :portal_api_provider_id, :api_provider_id
    add_column :properties, :portal_api_provider_id, :integer, :after => :api_provider_id
  end
end
