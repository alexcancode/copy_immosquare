class AddInfoInProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :first_name, :string, :after => :public_name
    add_column :profiles, :last_name, :string, :after => :public_name
  end
end
