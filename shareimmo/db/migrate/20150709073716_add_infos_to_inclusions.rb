class AddInfosToInclusions < ActiveRecord::Migration
  def change
    add_column :property_inclusions, :half_furnsihed_living_room, :integer
    add_column :property_inclusions, :half_furnsihed_rooms, :integer
    add_column :property_inclusions, :half_furnsihed_kitchen, :integer
    add_column :property_inclusions, :half_furnsihed_other, :integer

    add_column :property_inclusions, :central_vacuum, :integer
    add_column :property_inclusions, :granite_quartz_countertops, :integer
    add_column :property_inclusions, :entries_washer_dryer, :integer
    add_column :property_inclusions, :entries_dishwasher, :integer
    add_column :property_inclusions, :fireplace, :integer
    add_column :property_inclusions, :storage, :integer
    add_column :property_inclusions, :walk_in, :integer
    add_column :property_inclusions, :no_smoking, :integer


    add_column :property_inclusions, :floor_carpet, :integer
    add_column :property_inclusions, :floor_wood, :integer
    add_column :property_inclusions, :floor_floating, :integer
    add_column :property_inclusions, :floor_ceramic, :integer
    add_column :property_inclusions, :floor_parquet, :integer
    add_column :property_inclusions, :floor_cushion, :integer
    add_column :property_inclusions, :floor_vinyle, :integer
    add_column :property_inclusions, :floor_lino, :integer


    add_column :property_inclusions, :exterior_land_access, :integer
    add_column :property_inclusions, :exterior_back_balcony, :integer
    add_column :property_inclusions, :exterior_front_balcony, :integer
    add_column :property_inclusions, :exterior_private_patio, :integer
    add_column :property_inclusions, :exterior_storage, :integer
    add_column :property_inclusions, :exterior_terrace, :integer


    add_column :property_inclusions, :accessibility_elevator, :integer
    add_column :property_inclusions, :accessibility_balcony, :integer
    add_column :property_inclusions, :accessibility_grab_bar, :integer
    add_column :property_inclusions, :accessibility_wider_corridors, :integer
    add_column :property_inclusions, :accessibility_lowered_switches, :integer
    add_column :property_inclusions, :accessibility_ramp, :integer



    add_column :property_inclusions, :senior_autonomy, :integer
    add_column :property_inclusions, :senior_half_autonomy, :integer
    add_column :property_inclusions, :senior_no_autonomy, :integer
    add_column :property_inclusions, :senior_meal, :integer
    add_column :property_inclusions, :senior_nursery, :integer
    add_column :property_inclusions, :senior_domestic_help, :integer
    add_column :property_inclusions, :senior_commnunity, :integer
    add_column :property_inclusions, :senior_activties, :integer
    add_column :property_inclusions, :senior_validated_residence, :integer
    add_column :property_inclusions, :senior_housing_cooperative, :integer


    add_column :property_inclusions, :pets_cat, :integer
    add_column :property_inclusions, :pets_dog, :integer
    add_column :property_inclusions, :pets_little_dog, :integer
    add_column :property_inclusions, :pets_cage_aquarium, :integer
    add_column :property_inclusions, :pets_not_allow, :integer


    add_column :property_inclusions, :service_internet, :integer
    add_column :property_inclusions, :service_tv, :integer
    add_column :property_inclusions, :service_tv_sat, :integer
    add_column :property_inclusions, :service_phone, :integer



    add_column :property_inclusions, :criteria_accessible, :integer
    add_column :property_inclusions, :criteria_perfect_for_senior, :integer
    add_column :property_inclusions, :criteria_open_space, :integer
    add_column :property_inclusions, :criteria_pets_allow, :integer
    add_column :property_inclusions, :criteria_no_carpet, :integer
    add_column :property_inclusions, :criteria_balcony, :integer
    add_column :property_inclusions, :criteria_lower_price, :integer
    add_column :property_inclusions, :criteria_many_storage, :integer
    add_column :property_inclusions, :criteria_good_location, :integer
    add_column :property_inclusions, :criteria_charisma, :integer
    add_column :property_inclusions, :criteria_contemporary, :integer
    add_column :property_inclusions, :criteria_accessed_court, :integer
    add_column :property_inclusions, :criteria_separate_shower, :integer
    add_column :property_inclusions, :criteria_sunny, :integer
    add_column :property_inclusions, :criteria_green_area, :integer
    add_column :property_inclusions, :criteria_ideal_for_family, :integer
    add_column :property_inclusions, :criteria_ideal_for_young_couple, :integer
    add_column :property_inclusions, :criteria_ideal_for_studient, :integer
    add_column :property_inclusions, :criteria_ideal_for_one_person, :integer
    add_column :property_inclusions, :criteria_big_space, :integer
    add_column :property_inclusions, :criteria_big_kitchen, :integer
    add_column :property_inclusions, :criteria_very_big, :integer
    add_column :property_inclusions, :criteria_without_smoke, :integer
    add_column :property_inclusions, :criteria_soundproof, :integer
    add_column :property_inclusions, :criteria_loft, :integer
    add_column :property_inclusions, :criteria_luxury, :integer
    add_column :property_inclusions, :criteria_new_building, :integer
    add_column :property_inclusions, :criteria_modern, :integer
    add_column :property_inclusions, :criteria_bicycle_path, :integer
    add_column :property_inclusions, :criteria_high_ceiling, :integer
    add_column :property_inclusions, :criteria_wood_floor, :integer
    add_column :property_inclusions, :criteria_clean, :integer
    add_column :property_inclusions, :criteria_homeowner, :integer
    add_column :property_inclusions, :criteria_renovated, :integer
    add_column :property_inclusions, :criteria_kitchen_renovated, :integer
    add_column :property_inclusions, :criteria_bathroom_renovated, :integer
    add_column :property_inclusions, :criteria_patio, :integer
    add_column :property_inclusions, :criteria_public_transport, :integer
    add_column :property_inclusions, :criteria_nice_neighborhood, :integer
    add_column :property_inclusions, :criteria_nice_view, :integer















  end
end
