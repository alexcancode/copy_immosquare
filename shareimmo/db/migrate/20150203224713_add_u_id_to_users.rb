class AddUIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :uid, :integer, :after => :id
    change_column :users, :api_provider_id, :integer, :after => :uid
  end
end
