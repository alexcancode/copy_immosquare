class RemoveOwnerToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :owner_name, :string
    remove_column :properties, :owner_mail, :string
    remove_column :properties, :owner_phone, :string
  end
end
