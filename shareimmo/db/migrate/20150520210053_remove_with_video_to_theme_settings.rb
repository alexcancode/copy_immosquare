class RemoveWithVideoToThemeSettings < ActiveRecord::Migration
  def change
    remove_column :theme_settings, :with_video, :integer
  end
end
