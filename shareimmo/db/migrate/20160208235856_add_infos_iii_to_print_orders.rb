class AddInfosIiiToPrintOrders < ActiveRecord::Migration
  def change
    remove_column :print_orders, :texts
    add_column :print_orders, :labels, :text, :after => :images
    add_column :print_orders, :values, :text, :after => :labels
  end
end
