class AddDefaultNomenclatureForInventoryParameter < ActiveRecord::Migration
  def change
    add_reference :inventory_parameters, :nomenclature, index: true
  end
end
