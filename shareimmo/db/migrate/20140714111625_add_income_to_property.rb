class AddIncomeToProperty < ActiveRecord::Migration
  def change
    add_money :properties, :income, currency: { present: false }, amount: { null: true, default: nil }
    add_column :properties, :income_currency, :string
    add_column :properties, :parking, :integer
    add_column :properties, :showerrooms, :integer
  end
end
