class AddPortalTypeRefToPortals < ActiveRecord::Migration
  def change
    add_reference :portals, :portal_type, index: true, foreign_key: false, :after => :id ,:default => 1
  end
end
