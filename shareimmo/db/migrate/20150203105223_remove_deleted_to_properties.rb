class RemoveDeletedToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :deleted, :integer
  end
end
