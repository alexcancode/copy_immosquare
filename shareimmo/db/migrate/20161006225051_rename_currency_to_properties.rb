class RenameCurrencyToProperties < ActiveRecord::Migration
  def change
    rename_column :properties, :price_currency, :currency
    remove_column :properties, :rent_currency
    remove_column :properties, :tax_1_currency
    remove_column :properties, :tax_2_currency
    remove_column :properties, :income_currency
    change_column :properties, :description_short, :text, :after => :description
    change_column :properties, :price_with_tax, :integer, :after => :updated_at
    change_column :properties, :price_from, :integer, :after => :price_with_tax
  end
end
