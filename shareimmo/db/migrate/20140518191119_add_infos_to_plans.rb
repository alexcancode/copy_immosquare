class AddInfosToPlans < ActiveRecord::Migration
  def change
    rename_column :plans, :price, :price_cents
    add_column :plans, :price_currency, :string
    add_column :plans, :country, :string
  end
end
