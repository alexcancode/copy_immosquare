class AddSlugToSupports < ActiveRecord::Migration
  def change
    add_column :supports, :slug, :string
  end
end
