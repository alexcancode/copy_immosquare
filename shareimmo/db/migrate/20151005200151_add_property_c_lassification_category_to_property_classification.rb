class AddPropertyCLassificationCategoryToPropertyClassification < ActiveRecord::Migration
  def change
    add_reference :property_classifications, :property_classification_category, index:{:name => "index_category_id"}, foreign_key: false, :after => :id

  end
end
