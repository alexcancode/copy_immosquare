class AddDraftToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :draft, :integer, :default =>0, :after => :id
  end
end
