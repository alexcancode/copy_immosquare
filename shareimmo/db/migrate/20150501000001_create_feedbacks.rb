class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.references :property, index: true, foreign_key: false
      t.datetime :visit_at
      t.integer :advice, :default => 2
      t.string :full_name
      t.string :email
      t.string :phone
      t.integer :reminder_phone, :default =>0
      t.integer :reminder_email, :default =>0
      t.text :note_internal
      t.text :note_for_owner
      t.string :help_sms
      t.text :signature
      t.timestamps null: false
    end
  end
end
