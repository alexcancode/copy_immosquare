class AddApiIdToPropertyClassification < ActiveRecord::Migration
  def change
    add_column :property_classifications, :api_id, :integer, :after => :id
  end
end
