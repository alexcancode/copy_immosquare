class RemoveInfosToApiProvider < ActiveRecord::Migration
  def change
    remove_column :api_providers, :commercial_name, :string
    remove_column :api_providers, :need_purchase, :integer
  end
end
