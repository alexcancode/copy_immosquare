class AddLevelToProperties < ActiveRecord::Migration
  def change
    rename_column :properties, :area, :area_living
    rename_column :properties, :reference, :unit
    rename_column :properties, :condo_fees_cents, :fees_cents
    rename_column :properties, :condo_fees_currency, :fees_currency
    add_column :properties, :level, :integer,:after => :unit
    add_column :properties, :withHalf, :string,:after => :rooms, :default => "no"
    add_column :properties, :rent_frequency, :integer,:after => :rent_currency
  end
end
