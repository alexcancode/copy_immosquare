class AddAttachmentCoverToBlogs < ActiveRecord::Migration
  def self.up
    change_table :blogs do |t|
      t.attachment :cover
    end
  end

  def self.down
    remove_attachment :blogs, :cover
  end
end
