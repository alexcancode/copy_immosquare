class AddInfosToPrintOrders < ActiveRecord::Migration
  def change
    add_column :print_orders, :profile_or_company, :integer, :default => 1, :after => :print_template_id
    add_column :print_orders, :locale, :string, :after => :profile_or_company
  end
end
