class AddStatusToProduts < ActiveRecord::Migration
  def change
    add_column :products, :status, :integer, :default => 1
    add_column :products, :position, :integer
  end
end
