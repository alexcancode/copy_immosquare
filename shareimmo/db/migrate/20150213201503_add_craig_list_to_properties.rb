class AddCraigListToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :craiglist_posting_status, :integer, :default=> 0
    add_column :properties, :lespac_posting_status, :integer, :default=> 0
  end
end
