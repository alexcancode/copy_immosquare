class AddDefaultValueToWebsiteTypeId < ActiveRecord::Migration
  def change
    change_column :websites, :website_type_id, :integer, :default => 1
  end
end
