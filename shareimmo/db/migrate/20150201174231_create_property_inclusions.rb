class CreatePropertyInclusions < ActiveRecord::Migration
  def change
    create_table :property_inclusions do |t|
      t.references :property, index: true
      t.integer :airConditioning
      t.integer :hotWater
      t.integer :heated
      t.integer :electricity
      t.integer :furnished
      t.integer :fridge
      t.integer :cooker
      t.integer :dishwasher
      t.integer :dryer
      t.integer :washer
      t.integer :microwave
      t.integer :elevator
      t.integer :laundry
      t.integer :garbageChute
      t.integer :commonSpace
      t.integer :janitor
      t.integer :gym
      t.integer :sauna
      t.integer :spa
      t.integer :insidePool
      t.integer :outsidePool
      t.integer :insideParking
      t.integer :outsideParking
      t.integer :parkingOnStreet
      t.integer :petsAllow
      t.timestamps
    end
  end
end
