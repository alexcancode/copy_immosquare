class RemovePropertySourceToProperties < ActiveRecord::Migration
  def change
    remove_reference :properties, :property_source, index: true, foreign_key: false
    drop_table :property_sources
  end
end
