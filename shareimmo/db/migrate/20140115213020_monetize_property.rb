class MonetizeProperty < ActiveRecord::Migration
  def change
    add_money :properties, :price
  end
end
