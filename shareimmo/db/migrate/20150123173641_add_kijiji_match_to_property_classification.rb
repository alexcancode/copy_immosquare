class AddKijijiMatchToPropertyClassification < ActiveRecord::Migration
  def change
    add_column :property_classifications, :kijiji_match, :string, :after => :list_globally_match
  end
end
