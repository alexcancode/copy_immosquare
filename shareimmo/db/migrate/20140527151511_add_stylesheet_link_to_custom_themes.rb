class AddStylesheetLinkToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :stylesheet_link, :string
  end
end
