class UpdateDefaultFontsIdCustomThemes < ActiveRecord::Migration
  def change
    change_column :custom_themes, :font_id, :integer, :default =>1
    change_column :custom_themes, :heading_font_id, :integer, :default =>1
  end
end
