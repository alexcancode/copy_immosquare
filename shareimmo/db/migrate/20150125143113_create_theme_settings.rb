class CreateThemeSettings < ActiveRecord::Migration
  def change
    create_table :theme_settings do |t|
      t.references :user, index: true
      t.references :theme, index: true
      t.integer :is_selected
      t.string :primary_color
      t.string :secondary_color
      t.string :tertiary_color

      t.timestamps
    end
  end
end
