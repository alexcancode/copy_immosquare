class RemoveDemoLinkToThemes < ActiveRecord::Migration
  def change
    remove_column :themes, :demo_link, :string
  end
end
