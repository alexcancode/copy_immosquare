class AddTermsAcceptedToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :video_posting_terms_accepted, :integer, :default => 0
  end
end
