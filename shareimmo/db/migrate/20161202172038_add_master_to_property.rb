class AddMasterToProperty < ActiveRecord::Migration
  def change
    add_column :properties, :master, :integer, :default => 1, :after => :uid
  end
end
