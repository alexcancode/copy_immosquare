class AddListingLimitationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :listing_limitation_id, :integer, :after => :id
  end
end
