class AddAttachmentCoverToCustomServices < ActiveRecord::Migration
  def self.up
    change_table :custom_services do |t|
      t.attachment :cover
    end
  end

  def self.down
    remove_attachment :custom_services, :cover
  end
end
