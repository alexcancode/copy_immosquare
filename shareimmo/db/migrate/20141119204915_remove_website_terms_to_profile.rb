class RemoveWebsiteTermsToProfile < ActiveRecord::Migration
  def change
    remove_column :profiles, :website_terms_accepted, :string
  end
end
