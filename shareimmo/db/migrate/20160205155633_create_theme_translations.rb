class CreateThemeTranslations < ActiveRecord::Migration

   def self.up
    Theme.create_translation_table!({
      :description => :string,
    }, {
      :migrate_data => false
    })
  end

  def self.down
    Theme.drop_translation_table! :migrate_data => true
  end

end


