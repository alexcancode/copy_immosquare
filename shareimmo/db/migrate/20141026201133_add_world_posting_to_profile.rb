class AddWorldPostingToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :world_posting_terms_accepted, :integer, :default=>0
  end
end
