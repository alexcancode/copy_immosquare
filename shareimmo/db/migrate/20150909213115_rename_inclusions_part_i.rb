class RenameInclusionsPartI < ActiveRecord::Migration
  def change
      rename_column :property_inclusions, :insideParking, :detail_inside_parking
      rename_column :property_inclusions, :outsideParking, :detail_outsite_parking
      rename_column :property_inclusions, :parkingOnStreet, :detail_parking_on_street
      rename_column :property_inclusions, :details_garagebox, :detail_garagebox
      rename_column :property_inclusions, :petsAllow, :pets_allow
      rename_column :property_inclusions, :airConditioning, :inclusion_air_conditioning
      rename_column :property_inclusions, :hotWater, :inclusion_hot_water
      rename_column :property_inclusions, :heated, :inclusion_heated
      rename_column :property_inclusions, :electricity, :inclusion_electricity
      rename_column :property_inclusions, :furnished, :inclusion_furnished
      rename_column :property_inclusions, :fridge, :inclusion_fridge
      rename_column :property_inclusions, :cooker, :inclusion_cooker
      rename_column :property_inclusions, :dishwasher, :inclusion_dishwasher
      rename_column :property_inclusions, :dryer, :inclusion_dryer
      rename_column :property_inclusions, :washer, :inclusion_washer
      rename_column :property_inclusions, :microwave, :inclusion_microwave
      rename_column :property_inclusions, :elevator, :detail_elevator
      rename_column :property_inclusions, :laundry, :detail_laundry
      rename_column :property_inclusions, :garbageChute, :detail_garbage_chute
      rename_column :property_inclusions, :commonSpace, :detail_common_space
      rename_column :property_inclusions, :janitor, :detail_janitor
      rename_column :property_inclusions, :gym, :detail_gym
      rename_column :property_inclusions, :sauna, :detail_sauna
      rename_column :property_inclusions, :spa, :detail_spa
      rename_column :property_inclusions, :insidePool, :detail_inside_pool
      rename_column :property_inclusions, :outsidePool, :detail_outside_pool




  end
end
