class AddSlugToPropertyTranslation < ActiveRecord::Migration
  def change
    add_column :property_translations, :slug, :string
    add_index :property_translations, :slug, unique: false
  end
end
