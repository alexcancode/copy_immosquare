class DropCustomThemes < ActiveRecord::Migration
  def change
    drop_table :custom_themes
    drop_table :custom_theme_presets
  end
end
