class CreateBrokerContacts < ActiveRecord::Migration
  def change
    create_table :broker_contacts do |t|
      t.references :property, index: true
      t.string :ref
      t.string :full_name
      t.string :email
      t.string :phone
      t.text :message
      t.integer :newsletter

      t.timestamps
    end
  end
end
