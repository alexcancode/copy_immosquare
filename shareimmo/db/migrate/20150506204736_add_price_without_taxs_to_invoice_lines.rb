class AddPriceWithoutTaxsToInvoiceLines < ActiveRecord::Migration
  def change
    add_column :invoice_lines, :price_without_taxs, :integer, :after => :price
  end
end
