class AddPositionToPrintTemplateZones < ActiveRecord::Migration
  def change
    add_column :print_template_zones, :position, :integer, :after => :id
  end
end
