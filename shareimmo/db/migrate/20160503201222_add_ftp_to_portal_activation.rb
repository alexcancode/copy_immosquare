class AddFtpToPortalActivation < ActiveRecord::Migration
  def change
    add_column :portal_activations, :ftp, :string, :after => :password
  end
end
