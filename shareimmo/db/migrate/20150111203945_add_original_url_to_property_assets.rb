class AddOriginalUrlToPropertyAssets < ActiveRecord::Migration
  def change
    add_column :property_assets, :original_url, :string
  end
end
