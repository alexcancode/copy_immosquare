class AddSoftwareInfosToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :software_uid,:string, :index => true , :after => :user_id
    add_reference :properties, :software_api_provider,:index => true , foreign_key: false, :after => :software_uid
    change_column :properties, :uid, :string

    add_column :users, :software_uid,:string,:index => true , :after => :api_provider_id
    add_reference :users, :software_api_provider,:index => true , foreign_key: false, :after => :software_uid


    add_column :buildings, :software_uid,:string, :index => true , :after => :user_id
    add_reference :buildings, :software_api_provider,:index => true , foreign_key: false, :after => :software_uid
    change_column :buildings, :uid, :string


  end
end
