class AddProductIdToPurchase < ActiveRecord::Migration
  def change
    remove_column :purchases, :cancel, :integer
    add_reference :purchases, :product, index: true, foreign_key: true, :after => :user_id
  end
end
