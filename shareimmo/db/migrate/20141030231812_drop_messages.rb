class DropMessages < ActiveRecord::Migration
  def change
    drop_table :messages
    drop_table :chats
    drop_table :imports

  end
end
