class UpdateTypeForBacklinkId < ActiveRecord::Migration
  def change
    change_column :visibility_portals, :backlink_id, :string
  end
end
