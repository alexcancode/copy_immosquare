class AddEnergyIndexesToPropertyInclusions < ActiveRecord::Migration
  def change
    add_column :properties, :index_energy, :string
    add_column :properties, :index_insulation, :string
  end
end
