class AddMoneyFieldsToProperties < ActiveRecord::Migration
  def change
    add_money :properties, :rent, currency: { present: false }
    add_money :properties, :tax_1, currency: { present: false }
    add_money :properties, :tax_2, currency: { present: false }
    add_money :properties, :condo_fees, currency: { present: false }
  end
end
