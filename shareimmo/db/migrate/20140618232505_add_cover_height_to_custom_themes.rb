class AddCoverHeightToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :cover_height, :integer,:default=>350
  end
end
