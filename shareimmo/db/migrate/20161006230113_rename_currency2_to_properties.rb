class RenameCurrency2ToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :rhinov_link, :string, :after => :level
    remove_column :properties, :fees_currency
    change_column :properties, :rent_frequency, :integer, :after => :price_from
    change_column :properties, :currency, :string, :after => :rent_frequency
  end
end
