class CreateFeedbackReports < ActiveRecord::Migration
  def change
    create_table :feedback_reports do |t|
      t.references :feedback, index: true, foreign_key: true
      t.integer :is_sent
      t.text :contact_1
      t.text :contact_2
      t.text :contact_3

      t.timestamps null: false
    end
  end
end
