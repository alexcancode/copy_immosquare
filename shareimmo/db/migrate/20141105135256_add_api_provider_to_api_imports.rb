class AddApiProviderToApiImports < ActiveRecord::Migration
  def change
    add_reference :api_imports, :api_provider, index: true
    remove_column :api_imports, :provider
  end
end
