class RemoveUnitSegToPropertyClassification < ActiveRecord::Migration
  def change
    remove_column :property_classifications, :with_unit_segmentation, :integer
  end
end
