class AddSetupCompletedToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :setup_completed, :integer,:default => 0
    add_column :profiles, :is_demo_user, :integer,:default => 0
    add_column :profiles, :secure_token, :string
    remove_column :websites, :website_completed
    remove_column :websites, :secure_website_token
    remove_column :websites, :demo_website
  end
end
