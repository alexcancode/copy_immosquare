class CreateprovidersServices < ActiveRecord::Migration
  def change
    create_table :providers_services, :id => false do |t|
      t.references :provider
      t.references :service
    end
    add_index :providers_services, [:provider_id, :service_id]
    add_index :providers_services, :service_id

  end



end
