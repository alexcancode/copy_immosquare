class AddOnMarketingPageToServicesUsers < ActiveRecord::Migration
  def change
    add_column :services_users, :on_marketing_page, :integer, :default => 1
  end
end
