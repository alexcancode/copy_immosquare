class AddWortimmoMatchToPropertyClassifications < ActiveRecord::Migration
  def change
    add_column :property_classifications, :wortimmo_match, :integer
  end
end
