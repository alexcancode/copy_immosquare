class AddFavoriteToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :favorite, :integer,:default => 0, :after => :building_id
  end
end
