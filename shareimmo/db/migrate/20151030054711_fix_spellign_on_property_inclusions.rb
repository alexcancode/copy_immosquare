class FixSpellignOnPropertyInclusions < ActiveRecord::Migration
  def change
    rename_column :property_inclusions, :senior_commnunity, :senior_community
    rename_column :property_inclusions, :senior_activties, :senior_activities
  end
end
