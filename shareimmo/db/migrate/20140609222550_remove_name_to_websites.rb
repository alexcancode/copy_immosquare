class RemoveNameToWebsites < ActiveRecord::Migration
  def change
    remove_column :websites, :name, :string
    remove_column :custom_themes, :contact_or_business_to_display, :integer
    remove_column :custom_themes, :open_tab, :integer
  end
end
