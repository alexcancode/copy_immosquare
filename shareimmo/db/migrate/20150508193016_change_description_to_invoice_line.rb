class ChangeDescriptionToInvoiceLine < ActiveRecord::Migration
  def change
    change_column :invoice_lines, :description, :text
  end
end
