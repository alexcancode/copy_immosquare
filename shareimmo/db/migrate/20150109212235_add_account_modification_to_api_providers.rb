class AddAccountModificationToApiProviders < ActiveRecord::Migration
  def change
    add_column :api_providers, :can_erease_account, :integer, :default => 0, :after=> :name
    add_column :api_providers, :can_erease_property, :integer, :default => 0 ,:after => :can_erease_account
  end
end
