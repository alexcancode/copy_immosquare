class AddDefaultValueToRentFrequency < ActiveRecord::Migration
  def change
    change_column :properties, :rent_frequency, :integer, :default => 1
  end
end
