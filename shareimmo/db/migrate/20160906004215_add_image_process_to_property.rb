class AddImageProcessToProperty < ActiveRecord::Migration
  def change
    add_column :properties, :image_process, :integer, :default => 0, :after => :id
  end
end
