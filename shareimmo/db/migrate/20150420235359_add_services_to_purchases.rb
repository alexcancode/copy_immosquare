class AddServicesToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :services, :string
  end
end
