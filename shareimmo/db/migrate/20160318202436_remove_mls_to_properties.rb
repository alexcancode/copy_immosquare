class RemoveMlsToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :mls, :integer
  end
end
