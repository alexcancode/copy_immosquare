class AddInfosPartIToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :locale, :string
    add_column :searches, :status, :integer
  end
end
