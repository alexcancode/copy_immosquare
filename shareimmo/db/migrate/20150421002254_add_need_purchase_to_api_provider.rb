class AddNeedPurchaseToApiProvider < ActiveRecord::Migration
  def change
    add_column :api_providers, :need_purchase, :integer, :default => 1, :after=>:name
    add_index :api_providers, :need_purchase
  end
end
