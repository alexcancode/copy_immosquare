class ChangeAgencyWebsiteToProfiles < ActiveRecord::Migration
  def change
    rename_column :profiles, :agency_website, :website
    change_column :profiles, :is_a_pro_user, :integer, :default =>0
  end
end
