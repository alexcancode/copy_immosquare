class CleanPropertyTablePartV < ActiveRecord::Migration
  def change
    change_table :properties do |t|
      t.change :price_from_cents, :integer, :after => :price_currency
      t.change :price_from_currency, :string, :after => :price_from_cents
    end
  end
end
