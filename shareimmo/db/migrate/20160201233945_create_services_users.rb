class CreateServicesUsers < ActiveRecord::Migration
  def change
    create_table :services_users do |t|
       t.references :service
       t.references :user
    end
  end
end
