class ChangeUsernameToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :trial_end_at
    change_column :users, :username, :string, :after=> :token_count
  end
end
