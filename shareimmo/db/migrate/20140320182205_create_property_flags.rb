class CreatePropertyFlags < ActiveRecord::Migration
  def change
    create_table :property_flags do |t|
      t.string :name
      t.string :color
      t.string :icon

      t.timestamps
    end
  end
end
