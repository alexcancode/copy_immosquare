class RemoveUidsToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :uid, :string
    remove_column :users, :api_provider_id, :integer
  end
end
