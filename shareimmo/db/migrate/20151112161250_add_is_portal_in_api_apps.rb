class AddIsPortalInApiApps < ActiveRecord::Migration
  def change
    add_column :api_apps, :is_portal, :integer, :default => 1, :after => :api_provider_id
  end
end
