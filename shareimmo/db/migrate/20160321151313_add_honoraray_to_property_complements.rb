class AddHonorarayToPropertyComplements < ActiveRecord::Migration
  def change
    add_column :property_complements, :france_rent_honorary, :string
  end
end
