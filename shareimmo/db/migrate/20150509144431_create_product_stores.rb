class CreateProductStores < ActiveRecord::Migration
  def change
    create_table :product_stores do |t|
      t.string :name
      t.string :service
      t.integer :amount
      t.integer :amount_without_taxs
      t.string :currency
      t.integer :position

      t.timestamps null: false
    end
  end
end
