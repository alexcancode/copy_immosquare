class AddLespacToStatistics < ActiveRecord::Migration
  def change
    remove_column :statistics, :solihome
    remove_column :statistics, :minisite
    add_column :statistics, :lespac, :text, :after => :kijiji
  end
end
