class CreateCivilities < ActiveRecord::Migration
  def change
    create_table :civilities do |t|
      t.string :value
      t.timestamps null: false
    end

    reversible do |dir|
      dir.up do
        Civility.create_translation_table! :value => :string
      end

      dir.down do 
        Civility.drop_translation_table!
      end
    end

    add_column :property_contacts, :civility_id, :string, :after => :property_contact_type_id


  end
end
