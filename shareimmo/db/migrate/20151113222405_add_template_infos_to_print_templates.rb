class AddTemplateInfosToPrintTemplates < ActiveRecord::Migration
  def change
    add_column :print_templates, :template_infos, :text
  end
end
