class AddText1ToThemeSettings < ActiveRecord::Migration
  def change
    add_column :theme_settings, :text_1, :text, :after => :stylesheet_url
    add_column :theme_settings, :text_2, :text, :after => :text_1
    add_column :theme_settings, :text_3, :text, :after => :text_2
  end
end
