class AddSeoPartIToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :name, :string
    add_column :websites, :meta_title, :string
    add_column :websites, :meta_description, :text
    add_column :websites, :google_analytics, :text
  end
end
