class AddInfosToPropertyInclusions < ActiveRecord::Migration
  def change
    add_column :property_inclusions, :composition_bar, :integer
    add_column :property_inclusions, :composition_living, :integer
    add_column :property_inclusions, :composition_dining, :integer
    add_column :property_inclusions, :composition_separe_toilet, :integer
    add_column :property_inclusions, :composition_open_kitchen, :integer
    add_column :property_inclusions, :heating_electric, :integer
    add_column :property_inclusions, :heating_solar, :integer
    add_column :property_inclusions, :heating_gaz, :integer
    add_column :property_inclusions, :heating_condensation, :integer
    add_column :property_inclusions, :heating_fuel, :integer
    add_column :property_inclusions, :heating_heat_pump, :integer
    add_column :property_inclusions, :heating_floor_heating, :integer
    add_column :property_inclusions, :details_garagebox, :integer
    add_column :property_inclusions, :indoor_attic, :integer
    add_column :property_inclusions, :indoor_cellar, :integer
    add_column :property_inclusions, :indoor_double_glazing, :integer
    add_column :property_inclusions, :indoor_triple_glazing, :integer
    add_column :property_inclusions, :floor_marble, :integer
    add_column :property_inclusions, :exterior_veranda, :integer
    add_column :property_inclusions, :exterior_garden, :integer
    add_column :property_inclusions, :transport_bicycle_path, :integer
    add_column :property_inclusions, :transport_public_transport, :integer
    add_column :property_inclusions, :transport_public_bike, :integer
    add_column :property_inclusions, :transport_subway, :integer
    add_column :property_inclusions, :transport_train_station, :integer
    add_column :property_inclusions, :security_guardian, :integer
    add_column :property_inclusions, :security_alarm, :integer
    add_column :property_inclusions, :security_intercom, :integer
    add_column :property_inclusions, :security_camera, :integer
    add_column :property_inclusions, :security_smoke_dectector, :integer



  end
end
