class AddPositionHoolToCustomTheme < ActiveRecord::Migration
  def change
    add_column :custom_themes, :hook_position_x, :integer, :default =>80
    add_column :custom_themes, :hook_position_y, :integer, :default =>30
  end
end
