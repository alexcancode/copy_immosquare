class AddNoteForVisitorToFeedbacks < ActiveRecord::Migration
  def change
    add_column :feedbacks, :note_for_visitor, :text
  end
end
