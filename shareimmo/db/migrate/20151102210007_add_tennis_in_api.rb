class AddTennisInApi < ActiveRecord::Migration
  def change
    add_column :property_inclusions, :detail_tennis, :integer, :after => :detail_golf, :default => 0
  end
end
