class TranslateProduct < ActiveRecord::Migration
  def change
    Product.create_translation_table!({
      :name => :string,
      :description => :text,
    }, {
      :migrate_data => true
    })
  end
end

