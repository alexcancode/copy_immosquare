class AddApiProviderdToPropertyContacts < ActiveRecord::Migration
  def change
    add_reference :property_contacts, :api_provider, index: true, foreign_key: false, :after => :uid
  end
end
