class AddMarketingDescriptionToServices < ActiveRecord::Migration
  def change
    add_column :services, :marketing_description, :text
  end
end
