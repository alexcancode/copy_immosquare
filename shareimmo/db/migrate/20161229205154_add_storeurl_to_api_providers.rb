class AddStoreurlToApiProviders < ActiveRecord::Migration
  def change
    add_column :api_providers, :store_url, :string, :after => :is_storeimmo_provider
  end
end
