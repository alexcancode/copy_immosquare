class AddTopToolbarTextToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_theme_presets, :top_toolbar_text, :string, :default => '#f5f7fa'
    add_column :custom_theme_presets, :top_toolbar_background_color, :string, :default => '#f5f7fa'
  end
end
