class AddMlsToPropertyComplements < ActiveRecord::Migration
  def change
    add_column :property_complements, :canada_mls, :integer
  end
end
