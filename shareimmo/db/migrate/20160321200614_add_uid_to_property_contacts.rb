class AddUidToPropertyContacts < ActiveRecord::Migration
  def change
    add_column :property_contacts, :uid, :string, :after => :id
  end
end
