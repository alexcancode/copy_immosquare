class AddJsonContentForInventory < ActiveRecord::Migration
  def change
    add_column :inventories, :file_type, :integer, :after => :designation, :default => 1

  end
end
