class AddPropertyTypeForPortal < ActiveRecord::Migration
  def change
    add_column :portals, :portal_ad_type , :string, :after => :status, :default => "sell,rent"
  end
end
