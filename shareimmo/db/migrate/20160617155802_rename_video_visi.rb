class RenameVideoVisi < ActiveRecord::Migration
  def change
    add_column :videos, :visibility_start,:date, :after => :youtube_id
    rename_column :videos, :validity_until, :visibility_until
  end
end
