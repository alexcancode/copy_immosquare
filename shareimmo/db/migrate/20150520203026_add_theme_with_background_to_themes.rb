class AddThemeWithBackgroundToThemes < ActiveRecord::Migration
  def change
    add_column :themes, :with_background, :integer, :default => 0, :after => :with_cover
  end
end
