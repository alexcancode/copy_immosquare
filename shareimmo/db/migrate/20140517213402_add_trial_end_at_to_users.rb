class AddTrialEndAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :trial_end_at, :datetime
  end
end
