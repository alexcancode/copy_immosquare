class AddHooksColorToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :hook_background_color, :string, :default=> '#444444'
    add_column :custom_themes, :hook_color, :string, :default=> '#ffffff'
  end
end
