class AddDraftToBuildings < ActiveRecord::Migration
  def change
    rename_column :buildings, :api_provider_api_id, :api_provider_id
    add_column :buildings, :draft, :integer, :default => 1, :after => :user_id
  end
end
