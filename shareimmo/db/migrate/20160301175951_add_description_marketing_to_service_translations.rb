class AddDescriptionMarketingToServiceTranslations < ActiveRecord::Migration
  def change
    add_column :service_translations, :marketing_description, :text
  end
end
