class RemoveKijijiMatchToPropertyClassifications < ActiveRecord::Migration
  def change
    remove_column :property_classifications, :kijiji_match
  end
end
