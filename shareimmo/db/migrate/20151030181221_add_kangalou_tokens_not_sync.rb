class AddKangalouTokensNotSync < ActiveRecord::Migration
  def change
    add_column :users, :kangalou_tokens_not_sync, :integer, :after => :kangalou_params, :default => 0
  end
end
