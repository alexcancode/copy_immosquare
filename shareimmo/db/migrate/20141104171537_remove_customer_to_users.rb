class RemoveCustomerToUsers < ActiveRecord::Migration
  def change
    remove_reference :users, :customer, index: true
  end
end
