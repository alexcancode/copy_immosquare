class AddContactsInfosToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :contact_name, :string
    add_column :websites, :contact_phone, :string
    add_column :websites, :contact_address, :string
  end
end
