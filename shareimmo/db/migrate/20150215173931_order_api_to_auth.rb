class OrderApiToAuth < ActiveRecord::Migration
  def change
    change_column :authentifications, :api_provider_id, :integer, :after=> :uid
  end
end
