class RemoveOldGeocodingToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :neighborhood, :string
    remove_column :properties, :administrative_area_level_2_long, :string
    remove_column :properties, :administrative_area_level_2_short, :string
    remove_column :properties, :country_long, :string
  end
end
