class AddVisibilityPortalAdUrl < ActiveRecord::Migration
  def change
    add_column :visibility_portals, :backlink_url , :string, :after => :backlink_id
  end
end
