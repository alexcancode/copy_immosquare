class AddAncestryToNomenclatureElements < ActiveRecord::Migration
  def change
    remove_column :nomenclature_elements, :parent_id
    remove_column :nomenclature_elements, :position
    add_column :nomenclature_elements, :ancestry, :string
    add_index :nomenclature_elements, :ancestry
  end
end
