class AddDraftToPrintTemplate < ActiveRecord::Migration
  def change
    add_column :print_templates, :draft, :integer, :default => 0, :after => :id
  end
end
