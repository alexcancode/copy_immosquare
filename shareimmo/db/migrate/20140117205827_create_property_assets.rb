class CreatePropertyAssets < ActiveRecord::Migration
  def change
    create_table :property_assets do |t|
      t.references :property, index: true
      t.string :name

      t.timestamps
    end
  end
end
