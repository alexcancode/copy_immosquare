class CreateCustomServices < ActiveRecord::Migration
  def change
    create_table :custom_services do |t|
      t.references :user, index: true, foreign_key: false
      t.string :name
      t.text :description
      t.string :link
      t.string :video_url

      t.timestamps null: false
    end
  end
end
