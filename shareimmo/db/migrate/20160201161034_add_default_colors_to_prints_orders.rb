class AddDefaultColorsToPrintsOrders < ActiveRecord::Migration
  def change
    add_column :print_orders, :primary_color, :string, :after => :locale
  end
end
