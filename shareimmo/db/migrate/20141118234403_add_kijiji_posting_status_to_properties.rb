class AddKijijiPostingStatusToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :kijiji_posting_status, :integer, :default=>0
    change_column :properties, :world_posting_status, :integer, :after=> :showerrooms
  end
end
