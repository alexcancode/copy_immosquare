class AddInfosToAuthentification < ActiveRecord::Migration
  def change
    add_column :authentifications, :expires, :boolean
    add_column :authentifications, :email, :string
    add_column :authentifications, :first_name, :string
    add_column :authentifications, :last_name, :string
    add_column :authentifications, :nickname, :string
    add_column :authentifications, :image, :string
    add_column :authentifications, :location, :string
    add_column :authentifications, :link, :string
    add_column :authentifications, :gender, :string
  end
end
