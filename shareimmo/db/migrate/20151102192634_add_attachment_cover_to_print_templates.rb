class AddAttachmentCoverToPrintTemplates < ActiveRecord::Migration
  def self.up
    change_table :print_templates do |t|
      t.attachment :cover
    end
  end

  def self.down
    remove_attachment :print_templates, :cover
  end
end
