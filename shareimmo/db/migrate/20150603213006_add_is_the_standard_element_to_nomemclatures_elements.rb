class AddIsTheStandardElementToNomemclaturesElements < ActiveRecord::Migration
  def change
    add_column :nomenclature_elements, :is_the_standard_element, :integer, :default =>0
    add_column :nomenclature_element_defaults, :is_the_standard_element, :integer, :default =>0
  end
end
