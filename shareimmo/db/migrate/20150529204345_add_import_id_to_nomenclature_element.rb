class AddImportIdToNomenclatureElement < ActiveRecord::Migration
  def change
    add_column :nomenclature_elements, :import_id, :integer, :index=>true
  end
end
