class AddUidtoProperties < ActiveRecord::Migration
  def change
    rename_column :properties, :portal_uid, :uid
    add_column :properties, :portal_uid, :string, :after => :uid
  end
end
