class DropPacks < ActiveRecord::Migration
  def change
    drop_table :pack_translations
    drop_table :packs
  end
end
