class AddAccountUidForPropertySource < ActiveRecord::Migration
  def change
    add_column :property_sources, :account_uid, :string, :after => :uid
  end
end
