class OrderProperty < ActiveRecord::Migration
  def change
    change_column :properties, :latitude, :float, :after => :building_id
    change_column :properties, :longitude, :float, :after => :latitude
    change_column :properties, :zipcode, :string, :after => :longitude
    change_column :properties, :street_number, :string, :after => :zipcode
    change_column :properties, :street_name, :string, :after => :street_number
    change_column :properties, :sublocality, :string, :after => :street_name
    change_column :properties, :locality, :string, :after => :sublocality
    change_column :properties, :administrative_area_level_2, :string, :after => :locality
    change_column :properties, :administrative_area_level_1, :string, :after => :administrative_area_level_2
    change_column :properties, :country_short, :string, :after => :administrative_area_level_1
    change_column :properties, :address, :string, :after => :country_short
  end
end
