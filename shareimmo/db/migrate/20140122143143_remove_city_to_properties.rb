class RemoveCityToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :city, :string
  end
end
