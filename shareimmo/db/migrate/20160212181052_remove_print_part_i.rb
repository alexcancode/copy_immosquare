class RemovePrintPartI < ActiveRecord::Migration
  def change
    drop_table :print_orders
    drop_table :print_template_assets
    drop_table :print_template_zone_translations
    drop_table :print_template_zones
    drop_table :print_templates



  end
end
