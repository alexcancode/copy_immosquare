class AddFlowNameToUserPortalProviders < ActiveRecord::Migration
  def change
    add_column :user_portal_providers, :flow_name, :string, :after => :api_provider_id
  end
end
