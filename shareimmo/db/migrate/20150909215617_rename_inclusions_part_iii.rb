class RenameInclusionsPartIii < ActiveRecord::Migration
  def change
    change_column :property_inclusions, :floor_marble, :integer, :after => :floor_lino
    rename_column :property_inclusions, :central_vacuum, :indoor_central_vacuum
    remove_column :property_inclusions, :granite_quartz_countertops
    rename_column :property_inclusions, :entries_washer_dryer, :indoor_entries_washer_dryer
    rename_column :property_inclusions, :entries_dishwasher, :indoor_entries_dishwasher
    rename_column :property_inclusions, :fireplace, :indoor_fireplace
    rename_column :property_inclusions, :storage, :indoor_storage
    rename_column :property_inclusions, :walk_in, :indoor_walk_in
    rename_column :property_inclusions, :no_smoking, :indoor_no_smoking
    change_column :property_inclusions, :indoor_double_glazing, :integer, :after => :indoor_no_smoking
    change_column :property_inclusions, :indoor_triple_glazing, :integer, :after => :indoor_double_glazing
    change_column :property_inclusions, :indoor_attic, :integer, :after => :half_furnsihed_other
    change_column :property_inclusions, :indoor_cellar, :integer, :after => :indoor_attic
  end
end
