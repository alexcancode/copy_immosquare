class DropOldStore < ActiveRecord::Migration
  def change
    drop_table :products
    drop_table :product_translations
    drop_table :product_stores
  end
end
