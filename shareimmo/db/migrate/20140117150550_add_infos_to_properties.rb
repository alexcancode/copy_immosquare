class AddInfosToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :reference, :string
    add_column :properties, :mls, :integer
    add_reference :properties, :property_classification, index: true
    add_reference :properties, :property_state, index: true
    add_reference :properties, :listing_type, index: true
    add_reference :properties, :area_unit, index: true
    add_column :properties, :year_of_built, :integer
    add_column :properties, :area, :integer
    add_column :properties, :rooms, :integer
    add_column :properties, :bathrooms, :integer
    add_column :properties, :bedrooms, :integer
    add_column :properties, :property_url, :string
    add_column :properties, :availability_date, :datetime
  end
end
