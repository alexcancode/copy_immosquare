class AddUserIdToInventories < ActiveRecord::Migration
  def change
    remove_reference :inventories, :owner, index: true, foreign_key: false
    add_reference :inventories, :user, index: true, foreign_key: false, :after=> :id
  end
end
