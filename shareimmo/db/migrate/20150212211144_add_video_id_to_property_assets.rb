class AddVideoIdToPropertyAssets < ActiveRecord::Migration
  def change
    add_column :property_assets, :video_id, :string, :after => :asset_type
  end
end
