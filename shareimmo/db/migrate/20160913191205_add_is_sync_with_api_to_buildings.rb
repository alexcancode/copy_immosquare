class AddIsSyncWithApiToBuildings < ActiveRecord::Migration
  def change
    add_column :buildings, :is_sync_with_api, :integer, :after => :api_provider_id
  end
end
