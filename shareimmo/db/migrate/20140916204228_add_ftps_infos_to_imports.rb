class AddFtpsInfosToImports < ActiveRecord::Migration
  def change
    add_column :imports, :ftp_user, :string
    add_column :imports, :ftp_password, :string
  end
end
