class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.references :user, index: true
      t.money :charge
      t.integer :token
      t.string :stripeTokenType
      t.string :stripeEmail
      t.string :stripeToken

      t.timestamps null: false
    end
    add_foreign_key :charges, :users
  end
end
