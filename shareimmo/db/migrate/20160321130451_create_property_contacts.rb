class CreatePropertyContacts < ActiveRecord::Migration
  def change
    create_table :property_contacts do |t|
      t.references :property, index: true, foreign_key: false
      t.references :property_contact_type, index: true, foreign_key: false
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.timestamps null: false
    end
  end
end
