class AddSecureIdToBuilding < ActiveRecord::Migration
  def change
    add_column :buildings, :secure_id, :string, :after => :draft
  end
end
