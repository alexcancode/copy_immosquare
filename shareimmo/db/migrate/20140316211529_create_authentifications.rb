class CreateAuthentifications < ActiveRecord::Migration
  def change
    create_table :authentifications do |t|
      t.references :user, index: true
      t.string :provider
      t.string :uid
      t.string :token
      t.integer :expires_at
      t.string :token_secret

      t.timestamps
    end
  end
end
