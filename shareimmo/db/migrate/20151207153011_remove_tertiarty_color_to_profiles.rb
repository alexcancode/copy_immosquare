class RemoveTertiartyColorToProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :tertiary_color, :string
  end
end
