class UpdateSyncWithApi < ActiveRecord::Migration
  def change
    change_column :properties, :is_sync_with_api, :integer, :default => nil
  end
end
