class CreatePropertyLots < ActiveRecord::Migration
  def change
    create_table :property_lots do |t|
      t.references :property, index: true
      t.string :level
      t.integer :area
      t.references :area_unit, index: true
      t.references :unit_type, index: true
      t.integer :rooms
      t.integer :bedrooms
      t.integer :bathrooms
      t.integer :showerrooms
      t.integer :patios
      t.integer :patio_area

      t.timestamps
    end
  end
end
