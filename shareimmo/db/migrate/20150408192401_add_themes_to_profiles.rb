class AddThemesToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :theme, :text
  end
end
