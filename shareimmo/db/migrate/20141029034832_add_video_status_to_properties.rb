class AddVideoStatusToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :video_posting_status, :integer, :default => 0
  end
end
