class TranslateBuilding < ActiveRecord::Migration
  def change
    Building.create_translation_table!({
      :description => :text,
    }, {
      :migrate_data => true
    })
  end
end

