class CreateApiImports < ActiveRecord::Migration
  def change
    create_table :api_imports do |t|
      t.references :user, index: true
      t.string :provider
      t.integer :status,:default=>0
      t.string :custom_field1
      t.string :custom_field2
      t.string :custom_field3

      t.timestamps
    end
  end
end
