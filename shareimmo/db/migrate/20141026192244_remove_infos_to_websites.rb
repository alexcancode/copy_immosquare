class RemoveInfosToWebsites < ActiveRecord::Migration
  def change
    remove_column :websites, :latitude
    remove_column :websites, :longitude
    remove_column :websites, :street_number
    remove_column :websites, :street_name
    remove_column :websites, :neighborhood
    remove_column :websites, :sublocality
    remove_column :websites, :locality
    remove_column :websites, :administrative_area_level_2_long
    remove_column :websites, :administrative_area_level_2_short
    remove_column :websites, :administrative_area_level_1_long
    remove_column :websites, :administrative_area_level_1_short
    remove_column :websites, :country_long
    remove_column :websites, :country_short
    remove_column :websites, :zipcode
    remove_column :websites, :contact_name
    remove_column :websites, :contact_phone
    remove_column :websites, :contact_address
    remove_column :websites, :business_name
    remove_column :websites, :business_phone
    remove_column :websites, :business_description
    remove_column :websites, :contact_avatar_file_name
    remove_column :websites, :contact_avatar_content_type
    remove_column :websites, :contact_avatar_file_size
    remove_column :websites, :contact_avatar_updated_at
    remove_column :websites, :business_logo_file_name
    remove_column :websites, :business_logo_content_type
    remove_column :websites, :business_logo_file_size
    remove_column :websites, :business_logo_updated_at
    remove_column :websites, :prefix_phone
    remove_column :websites, :contact_phone_2
    remove_column :websites, :contact_email
    remove_column :websites, :contact_job
    remove_column :websites, :contact_languages
    remove_column :websites, :bio
    remove_column :websites, :website_url
    remove_column :websites, :country
    remove_column :websites, :world_posting_terms_accepted


  end
end
