class CleanPropertyTablePartIii < ActiveRecord::Migration
  def change
    change_table :properties do |t|
      t.change :address, :string, :after => :availability_date
      t.change :pets_allowed, :integer, :after => :income_currency
      t.change :area_land, :integer, :after => :area
      t.change :area_unit_id, :integer, :after => :area_land
      t.change :latitude, :float, :after => :address
      t.change :longitude, :float, :after => :latitude
      t.change :cadastre, :string, :after => :longitude
      t.change :mls, :integer, :after => :cadastre
      t.change :reference, :string, :after => :mls
    end
  end
end
