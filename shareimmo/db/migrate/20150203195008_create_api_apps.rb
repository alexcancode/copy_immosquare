class CreateApiApps < ActiveRecord::Migration
  def change
    create_table :api_apps do |t|
      t.string :key
      t.string :token
      t.references :api_provider, index: true
      t.string :note_interne
      t.timestamps
    end
  end
end
