class AddWebAppToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :web_app, :integer, :after => :status, :default => 1
  end
end
