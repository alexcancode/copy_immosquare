class FixSpellingOnProducts < ActiveRecord::Migration
  def change
    rename_column :products, :is_visibility_prodcut, :is_visibility_product
  end
end
