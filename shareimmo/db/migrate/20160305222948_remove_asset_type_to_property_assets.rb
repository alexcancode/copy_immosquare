class RemoveAssetTypeToPropertyAssets < ActiveRecord::Migration
  def change
    remove_column :property_assets, :asset_type, :integer
    remove_column :property_assets, :video_id, :string
    remove_column :property_assets, :automatically_generated, :integer
    remove_column :property_assets, :name, :string
  end
end
