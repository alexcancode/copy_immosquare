class AddStatusToImports < ActiveRecord::Migration
  def change
    add_column :imports, :status, :integer,:default=> 1
  end
end
