class AddCanManageShareimmoPageToAuthentifications < ActiveRecord::Migration
  def change
    add_column :authentifications, :can_manage_shareimmo_page, :integer,:default=>0
  end
end
