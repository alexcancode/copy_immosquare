class AddInfosWebsitesToWebsites < ActiveRecord::Migration
  def change
    add_reference :websites, :website_type, index: true
  end
end
