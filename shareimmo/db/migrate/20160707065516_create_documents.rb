class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.references :property, index: true, foreign_key: false
      t.integer :visible_on_front, :default => 1
      t.string :title
      t.string :note

      t.timestamps null: false
    end
  end
end
