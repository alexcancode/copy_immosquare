class RemoveProductIdToPurchases < ActiveRecord::Migration
  def change
    remove_reference :purchases, :product, index: true, foreign_key: false
  end
end
