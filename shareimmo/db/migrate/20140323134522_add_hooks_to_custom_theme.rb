class AddHooksToCustomTheme < ActiveRecord::Migration
  def change
    add_column :custom_themes, :full_name, :boolean
    add_column :custom_themes, :baseline, :boolean
    add_column :custom_themes, :phone, :boolean
  end
end
