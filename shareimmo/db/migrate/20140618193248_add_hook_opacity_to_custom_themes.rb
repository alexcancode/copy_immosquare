class AddHookOpacityToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :hook_background_opacity, :string,:default=> 1
  end
end
