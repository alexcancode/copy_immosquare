class AddNomenclature < ActiveRecord::Migration
  def change
    create_table :nomenclatures do |t|
      t.string :name
      t.string :description

      # Status : 1 => public (par default), 2 => shared (can be used by other users), 3 => private (specific for 1 user),
      t.integer :status, :default => 2

    end

    create_table :nomenclature_elements do |t|
      t.string :name
      t.string :description
      t.integer :position, :default => 1
      t.integer :parent_id

      t.integer :element_type

      t.belongs_to :nomenclature
    end

    create_table :nomenclatures_users do |t|
      t.belongs_to :nomenclature, index: true
      t.belongs_to :user, index: true
    end
  end
end
