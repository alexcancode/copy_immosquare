class CreateWysiwygPictures < ActiveRecord::Migration
  def change
    create_table :wysiwyg_pictures do |t|
      t.references :imageable, index: true, polymorphic: true
      t.timestamps null: false
    end
  end
end
