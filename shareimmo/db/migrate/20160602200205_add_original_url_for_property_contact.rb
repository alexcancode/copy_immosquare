class AddOriginalUrlForPropertyContact < ActiveRecord::Migration
  def change
    add_column :property_contacts, :original_picture_url, :string, :after => :country
  end
end
