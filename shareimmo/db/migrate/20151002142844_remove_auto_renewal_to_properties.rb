class RemoveAutoRenewalToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :auto_renewal, :integer
  end
end
