class CreatePropertyComplements < ActiveRecord::Migration
  def change
    create_table :property_complements do |t|
      t.references :property, index: true, foreign_key: false
      t.text :france_dpe_indice
      t.text :france_dpe_value
      t.text :france_dpe_id
      t.text :france_ges_indice
      t.text :france_ges_value
      t.integer :france_allur_is_condo
      t.integer :france_allur_units
      t.integer :france_allur_uninhabitables
      t.integer :france_allur_spending
      t.integer :france_allur_legal_action
      t.text :belgium_peb_indice
      t.text :belgium_peb_value
      t.text :belgium_peb_id
      t.integer :belgium_building_permit
      t.integer :belgium_done_assignments
      t.integer :belgium_preemption_property
      t.integer :belgium_subdivision_permits
      t.integer :belgium_sensitive_flood_area
      t.integer :belgium_delimited_flood_zone
      t.integer :belgium_risk_area
      t.timestamps null: false
    end
  end
end


