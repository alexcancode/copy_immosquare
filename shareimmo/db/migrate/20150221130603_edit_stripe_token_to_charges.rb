class EditStripeTokenToCharges < ActiveRecord::Migration
  def change
    change_column :charges, :stripeToken, :text
  end
end
