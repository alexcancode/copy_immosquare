class DefaultCurrrenciesToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :rent_currency, :string, :default => "CAD"
    add_column :properties, :tax_1_currency, :string, :default => "CAD"
    add_column :properties, :tax_2_currency, :string, :default => "CAD"
    add_column :properties, :condo_fees_currency, :string, :default => "CAD"
  end
end
