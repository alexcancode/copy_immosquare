class UpdateDefaultHaltToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :withHalf
    add_column :properties, :withHalf, :integer, :default => 0, :after => :rooms
  end
end
