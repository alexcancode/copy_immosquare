class RemoveKijijiToVisibilities < ActiveRecord::Migration
  def change
    remove_column :visibilities, :kijiji_posting_status, :integer
    remove_column :visibilities, :kijiji_bump, :integer
    remove_column :visibilities, :kijiji_ad_id, :integer
  end
end
