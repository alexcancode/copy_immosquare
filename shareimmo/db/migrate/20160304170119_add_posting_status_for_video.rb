class AddPostingStatusForVideo < ActiveRecord::Migration
  def change
    add_column :videos, :posting_status , :integer, :after => :property_id, :default => 1
  end
end
