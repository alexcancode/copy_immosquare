class AddSlugToSupportTypes < ActiveRecord::Migration
  def change
    add_column :support_types, :slug, :string
  end
end
