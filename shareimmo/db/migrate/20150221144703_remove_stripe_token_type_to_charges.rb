class RemoveStripeTokenTypeToCharges < ActiveRecord::Migration
  def change
    rename_column :charges, :token, :coins
    remove_column :charges, :stripeTokenType, :string
    remove_column :charges, :stripeEmail, :string
    remove_column :charges, :tax3
    change_column :charges, :tax1, :float, :after => :coins
    change_column :charges, :tax2, :float, :after => :tax1
  end
end
