class RemoveBusinessPhoneToWebsites < ActiveRecord::Migration
  def change
    remove_column :websites, :business_phone_2, :string
  end
end
