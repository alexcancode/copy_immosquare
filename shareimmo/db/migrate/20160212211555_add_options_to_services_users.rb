class AddOptionsToServicesUsers < ActiveRecord::Migration
  def change
    add_column :services_users, :options, :integer
  end
end
