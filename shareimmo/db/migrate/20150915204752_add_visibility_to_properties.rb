class AddVisibilityToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :address_visibility, :integer, :default => 1, :after => :property_url

  end
end
