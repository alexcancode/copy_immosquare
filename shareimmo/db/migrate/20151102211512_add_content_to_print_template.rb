class AddContentToPrintTemplate < ActiveRecord::Migration
  def change
    add_column :print_templates, :zone_count, :integer
    add_column :print_templates, :content, :text
  end
end
