class AddClassificationMatchForLouercom < ActiveRecord::Migration
  def change
    add_column :property_classifications, :louercom_match, :integer, :after => :craigslist_match
  end
end
