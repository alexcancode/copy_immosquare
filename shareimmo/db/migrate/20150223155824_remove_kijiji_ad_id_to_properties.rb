class RemoveKijijiAdIdToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :kijiji_ad_id, :integer
    add_column :properties, :kijiji_infos, :text, :after => :kijiji_posting_status
  end
end
