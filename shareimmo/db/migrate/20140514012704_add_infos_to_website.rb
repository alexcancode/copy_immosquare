class AddInfosToWebsite < ActiveRecord::Migration
  def change
    add_column :websites, :website_completed, :boolean, :default=> false
    add_column :websites, :line_1, :string
    add_column :websites, :line_2, :string
    add_column :websites, :line_3, :string
    add_column :websites, :line_4, :string
  end
end
