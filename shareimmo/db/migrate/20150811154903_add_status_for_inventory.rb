class AddStatusForInventory < ActiveRecord::Migration
  def change
    add_column :inventories, :status, :integer, :after => :designation, :default => 1
  end
end
