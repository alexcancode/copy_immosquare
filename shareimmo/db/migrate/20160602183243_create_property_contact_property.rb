class CreatePropertyContactProperty < ActiveRecord::Migration
  def change
    create_table :property_contact_properties, :id => false  do |t|
      t.references :property_contact
      t.references :property
    end

    add_index :property_contact_properties, [:property_contact_id, :property_id], :unique => true, :name => 'property_contact_properties_index'
    add_index :property_contact_properties, :property_id

  end
end
