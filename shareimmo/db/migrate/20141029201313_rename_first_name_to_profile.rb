class RenameFirstNameToProfile < ActiveRecord::Migration
  def change
    remove_column :profiles, :agency_phone, :string
    remove_column :profiles, :last_name, :string
    rename_column :profiles, :first_name, :public_name
  end
end
