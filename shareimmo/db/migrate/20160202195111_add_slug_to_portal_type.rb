class AddSlugToPortalType < ActiveRecord::Migration
  def change
    add_column :portal_types, :slug, :string
  end
end
