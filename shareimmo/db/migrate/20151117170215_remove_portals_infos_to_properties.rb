class RemovePortalsInfosToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :software_api_provider_id, :integer
    remove_column :properties, :software_uid, :string
    remove_column :properties, :portal_uid, :string
    remove_column :properties, :portal_api_provider_id, :integer
  end
end
