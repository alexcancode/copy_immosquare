class CreatePortals < ActiveRecord::Migration
  def change
    create_table :portals do |t|
      t.string :name
      t.string :logo
      t.integer :free, :default => 0
      t.string :country
      t.integer :limit
      t.integer :with_user_connexion, :default => 0
      t.timestamps null: false
    end
  end
end
