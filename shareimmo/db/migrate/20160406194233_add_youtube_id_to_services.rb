class AddYoutubeIdToServices < ActiveRecord::Migration
  def change
    add_column :services, :youtube_id, :string, :after => :wistia_id
  end
end
