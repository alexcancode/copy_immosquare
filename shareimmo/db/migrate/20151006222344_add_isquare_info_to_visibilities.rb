class AddIsquareInfoToVisibilities < ActiveRecord::Migration
  def change
    add_column :visibilities, :isquare_posting_status, :integer
    add_column :visibilities, :isquare_ad_id, :string
  end
end
