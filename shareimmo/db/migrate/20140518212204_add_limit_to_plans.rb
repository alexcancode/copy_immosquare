class AddLimitToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :limit, :integer
  end
end
