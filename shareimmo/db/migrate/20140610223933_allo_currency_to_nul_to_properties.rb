class AlloCurrencyToNulToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :price_currency, :string
    add_column :properties, :price_currency, :string
  end
end
