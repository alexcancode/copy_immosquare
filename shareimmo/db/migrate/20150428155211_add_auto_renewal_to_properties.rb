class AddAutoRenewalToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :auto_renewal, :integer, :default => 1, :after => :status
  end
end
