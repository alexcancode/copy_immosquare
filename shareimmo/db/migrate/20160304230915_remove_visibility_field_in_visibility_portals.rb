class RemoveVisibilityFieldInVisibilityPortals < ActiveRecord::Migration
  def change
    remove_column :visibility_portals, :visibility_id
  end
end
