class AddCountry2ToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :country, :string, :default => "CA"
    add_column :profiles, :country_alpha2, :string, :default => "CA"
    add_column :profiles, :country_alpha3, :string, :default => "CAN"
    add_column :profiles, :country_code, :string,:default => "1"
    add_column :profiles, :international_prefix, :string, :default => "011"
  end
end
