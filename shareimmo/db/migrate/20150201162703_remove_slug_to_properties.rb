class RemoveSlugToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :slug, :string
    remove_column :property_translations, :slug, :string
  end
end
