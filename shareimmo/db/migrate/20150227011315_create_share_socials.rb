class CreateShareSocials < ActiveRecord::Migration
  def change
    create_table :share_socials do |t|
      t.references :user, index: true
      t.references :property, index: true
      t.references :api_provider, index: true
      t.text :note

      t.timestamps null: false
    end
    add_foreign_key :share_socials, :users
    add_foreign_key :share_socials, :properties
    add_foreign_key :share_socials, :api_providers
  end
end
