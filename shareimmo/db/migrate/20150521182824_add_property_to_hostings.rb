class AddPropertyToHostings < ActiveRecord::Migration
  def change
    remove_column :hostings, :proprerty_id, :integer
    add_reference :hostings, :property, index: true, foreign_key: false, :after => :user_id
  end
end
