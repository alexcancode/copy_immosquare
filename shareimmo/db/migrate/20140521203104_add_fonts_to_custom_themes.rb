class AddFontsToCustomThemes < ActiveRecord::Migration
  def change
    add_reference :custom_themes, :font, index: true
    add_reference :custom_themes, :heading_font, index: true
  end
end
