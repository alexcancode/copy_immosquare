class RemoveOldsServicesToServices < ActiveRecord::Migration
  def change
    remove_column :services, :on_shareimmo, :interger
    remove_column :services, :on_1clic, :integer
    remove_column :services, :on_kangalou, :integer
    remove_column :services, :on_shelterr, :integer
    remove_column :services, :on_zoomvisit, :integer
    remove_column :services, :on_immopad, :integer
  end
end
