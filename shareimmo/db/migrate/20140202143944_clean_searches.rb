class CleanSearches < ActiveRecord::Migration
  def change
    remove_column :searches, :created_at, :datetime
    remove_column :searches, :updated_at, :datetime
    remove_column :searches, :locale, :string
    remove_column :searches, :listing_types_id, :integer
    remove_column :searches, :property_classifications_id, :integer
    add_column :searches, :listing_type, :integer
    add_column :searches, :property_classification, :integer
  end
end
