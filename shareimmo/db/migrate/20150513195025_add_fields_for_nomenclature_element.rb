class AddFieldsForNomenclatureElement < ActiveRecord::Migration
  def change
    add_column :nomenclature_elements, :value, :string, :after=>:name
    add_column :nomenclature_elements, :status, :integer, :default => 1
  end
end
