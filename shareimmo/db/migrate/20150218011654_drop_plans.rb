class DropPlans < ActiveRecord::Migration
  def change
    drop_table :plans
    drop_table :friendly_id_slugs
  end
end
