class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.references :user, index: true, foreign_key: false
      t.string :title
      t.string :full_name
      t.text :content
      t.integer :status, :default => 0
      t.timestamps null: false
    end
  end
end
