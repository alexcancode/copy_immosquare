class AddUsersToApiApps < ActiveRecord::Migration
  def change
    add_column :api_apps, :search_authorized_users, :string, :after => :is_portal
  end
end
