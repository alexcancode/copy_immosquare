class AddAttachmentPictureToPropertyContacts < ActiveRecord::Migration
  def self.up
    change_table :property_contacts do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :property_contacts, :picture
  end
end
