class CleanPropertyTablePartIi < ActiveRecord::Migration
  def change
    change_table :properties do |t|
      t.change :api_provider_id, :integer, :after => :uid
      t.change :user_id, :integer, :after => :api_provider_id
      t.change :property_classification_id, :integer, :after => :status
      t.change :property_status_id, :integer, :after => :property_classification_id
      t.change :property_listing_id, :integer, :after => :property_status_id
    end
  end
end

