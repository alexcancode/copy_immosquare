class AddFullsizeToThemes < ActiveRecord::Migration
  def change
    add_column :themes, :with_cover_full_size, :integer, :default => 0
  end
end
