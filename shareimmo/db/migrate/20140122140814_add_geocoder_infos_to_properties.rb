class AddGeocoderInfosToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :street_number, :string
    add_column :properties, :street_name, :string
    add_column :properties, :neighborhood, :string
    add_column :properties, :sublocality, :string
    add_column :properties, :locality, :string
    add_column :properties, :administrative_area_level_2_long, :string
    add_column :properties, :administrative_area_level_2_short, :string
    add_column :properties, :administrative_area_level_1_long, :string
    add_column :properties, :administrative_area_level_1_short, :string
    add_column :properties, :country_long, :string
    add_column :properties, :country_short, :string
    add_column :properties, :zipcode, :string
  end
end
