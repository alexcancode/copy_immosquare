class CreateCustomThemes < ActiveRecord::Migration
  def change
    create_table :custom_themes do |t|
      t.references :profile, index: true
      t.string :background_color
      t.string :title_color
      t.string :link_color
      t.string :text_font
      t.string :digest

      t.timestamps
    end
  end
end
