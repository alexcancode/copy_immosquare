class AddUserToGlossary < ActiveRecord::Migration
  def change
    remove_reference :glossaries, :owner, index: true, foreign_key: false
    add_reference :glossaries, :user, index: true, foreign_key: false, :after => :id
  end
end
