class AddWithVideoToThemeSettings < ActiveRecord::Migration
  def change
    add_column :theme_settings, :with_video, :integer, :default => 0
    add_column :theme_settings, :with_slim_button, :integer, :default => 0
  end
end
