class AddStylesheetUrlToThemeSettings < ActiveRecord::Migration
  def change
    add_column :theme_settings, :stylesheet_url, :string, :after=> :tertiary_color
  end
end
