class AddAreaBalconyToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :area_balcony, :integer, :after => :area_living
  end
end
