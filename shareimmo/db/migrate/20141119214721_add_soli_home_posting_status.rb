class AddSoliHomePostingStatus < ActiveRecord::Migration
  def change
    add_column :properties, :solihome_posting_status, :integer, :default=>0
  end
end
