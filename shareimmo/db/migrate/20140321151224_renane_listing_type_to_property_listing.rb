class RenaneListingTypeToPropertyListing < ActiveRecord::Migration
  def change
    rename_table :listing_types, :property_listings
  end
end
