class UpdateStyleShetTocustomeThemes < ActiveRecord::Migration
  def change
    change_column :custom_themes, :stylesheet_link, :text
  end
end
