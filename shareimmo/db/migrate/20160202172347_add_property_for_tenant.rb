class AddPropertyForTenant < ActiveRecord::Migration
  def change
    add_reference :tenants, :property, index: true, foreign_key: false, :after => :user_id  	
  end
end
