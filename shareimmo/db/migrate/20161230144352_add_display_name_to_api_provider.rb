class AddDisplayNameToApiProvider < ActiveRecord::Migration
  def change
    add_column :api_providers, :display_name, :string, :after => :name
  end
end
