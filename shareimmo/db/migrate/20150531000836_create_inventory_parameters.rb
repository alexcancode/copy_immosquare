class CreateInventoryParameters < ActiveRecord::Migration
  def change
    create_table :inventory_parameters do |t|
      t.references :user, index: true, foreign_key: false
      t.string :state1_name
      t.string :state1_label
      t.string :state2_name
      t.string :state2_label
      t.string :state3_name
      t.string :state3_label
      t.string :state4_name
      t.string :state4_label
      t.string :state5_name
      t.string :state5_label
      t.string :pdf_introduction_title
      t.text :pdf_introduction_content
      t.string :pdf_free_section_title
      t.text :pdf_free_section_content
      t.timestamps null: false
    end
  end
end
