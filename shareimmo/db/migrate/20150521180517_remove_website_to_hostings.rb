class RemoveWebsiteToHostings < ActiveRecord::Migration
  def change
    remove_column :hostings, :website_id, :integer
  end
end
