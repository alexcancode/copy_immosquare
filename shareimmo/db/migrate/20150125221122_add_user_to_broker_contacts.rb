class AddUserToBrokerContacts < ActiveRecord::Migration
  def change
    add_reference :broker_contacts, :user, index: true, :after=>:property_id
  end
end
