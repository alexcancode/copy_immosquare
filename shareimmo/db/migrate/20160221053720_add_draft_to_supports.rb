class AddDraftToSupports < ActiveRecord::Migration
  def change
    add_column :supports, :draft, :integer,:default => 0, :after => :id
  end
end
