class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.references :profile, index: true
      t.string :name
      t.string :type
      t.string :city
      t.string :province
      t.string :country
      t.string :tel

      t.timestamps
    end
  end
end
