class AddListGloballyMatchToPropertyListings < ActiveRecord::Migration
  def change
    add_column :property_listings, :list_globally_match, :string
  end
end
