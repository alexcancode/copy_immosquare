class RemoveDescriptionToNomenclatureElement < ActiveRecord::Migration
  def change
    remove_column :nomenclature_elements, :description, :string
  end
end
