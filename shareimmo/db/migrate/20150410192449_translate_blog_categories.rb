class TranslateBlogCategories < ActiveRecord::Migration
  def change
    BlogCategory.create_translation_table!({
      :name => :string,
      :slug => :string,
    }, {
      :migrate_data => true
    })
  end
end


