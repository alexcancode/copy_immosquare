class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :user, index: true
      t.string :stripe_id
      t.string :stripe_plan_id
      t.timestamp :start
      t.string :status
      t.string :customer
      t.timestamp :current_period_start
      t.timestamp :current_period_end
      t.timestamp :ended_at
      t.timestamp :canceled_at

      t.timestamps null: false
    end
    # add_foreign_key :subscriptions, :user_ids
  end
end
