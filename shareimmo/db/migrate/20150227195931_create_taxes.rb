class CreateTaxes < ActiveRecord::Migration
  def change
    create_table :taxes do |t|
      t.string :slug
      t.string :name
      t.float :rate
      t.string :number

      t.timestamps null: false
    end
  end
end
