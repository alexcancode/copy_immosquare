class AddPhoneSmsToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :phone_sms, :string, :after => :phone
  end
end
