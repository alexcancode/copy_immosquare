class RemoveDateToVisibilityPortals < ActiveRecord::Migration
  def change
    remove_column :visibility_portals, :validity_start, :datetime
    remove_column :visibility_portals, :validity_until, :datetime
  end
end
