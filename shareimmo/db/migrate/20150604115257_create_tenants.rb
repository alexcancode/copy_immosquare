class CreateTenants < ActiveRecord::Migration
  def change
    create_table :tenants do |t|
      t.integer :uid , :limit => 8
      t.references :user, index: true, foreign_key: false
      t.string :first_name
      t.string :last_name
      t.integer :tenant_type
      t.string :email
      t.string :phone
      t.string :phone2
      t.string :locale
      t.date :birthday
      t.string :sexe

      t.timestamps null: false
    end
  end
end
