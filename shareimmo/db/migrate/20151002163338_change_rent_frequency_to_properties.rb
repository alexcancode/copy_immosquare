class ChangeRentFrequencyToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :rent_frequency, :integer , :default => 30
  end
end
