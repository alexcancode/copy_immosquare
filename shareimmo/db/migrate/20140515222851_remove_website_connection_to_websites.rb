class RemoveWebsiteConnectionToWebsites < ActiveRecord::Migration
  def change
    remove_reference :websites, :website_type, index: true
    remove_reference :websites, :website_user_type, index: true
  end
end
