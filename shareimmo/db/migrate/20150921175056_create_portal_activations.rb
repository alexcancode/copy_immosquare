class CreatePortalActivations < ActiveRecord::Migration
  def change
    create_table :portal_activations do |t|
      t.references :user, index: true, foreign_key: false
      t.references :portal, index: true, foreign_key: false
      t.integer :status
      t.string :login
      t.string :password

      t.timestamps null: false
    end
  end
end
