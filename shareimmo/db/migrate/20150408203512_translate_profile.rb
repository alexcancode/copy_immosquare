class TranslateProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :baseline, :string, :after => :bio
    Profile.create_translation_table!({
      :job => :string,
      :baseline => :string,
      :bio => :text,
    }, {
      :migrate_data => true
    })
  end
end



