class RemoveInfosToPortals < ActiveRecord::Migration
  def change
    remove_column :portals, :logo, :string
    remove_column :portals, :with_bg, :string
    remove_column :portals, :limit, :integer
  end
end
