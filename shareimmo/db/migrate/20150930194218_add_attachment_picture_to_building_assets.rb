class AddAttachmentPictureToBuildingAssets < ActiveRecord::Migration
  def self.up
    change_table :building_assets do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :building_assets, :picture
  end
end
