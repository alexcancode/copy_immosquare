class RemoveMiniSiteStatusToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :mini_site_status, :integer
  end
end
