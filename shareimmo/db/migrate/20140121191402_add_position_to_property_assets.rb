class AddPositionToPropertyAssets < ActiveRecord::Migration
  def change
    add_column :property_assets, :position, :integer
  end
end
