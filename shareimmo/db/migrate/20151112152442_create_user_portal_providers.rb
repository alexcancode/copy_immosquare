class CreateUserPortalProviders < ActiveRecord::Migration
  def change
    create_table :user_portal_providers do |t|
      t.references :user, index: true, foreign_key: false
      t.references :api_provider, index: true, foreign_key: false
      t.string :uid
    end
    add_index :user_portal_providers, [:api_provider_id, :uid]

  end
end
