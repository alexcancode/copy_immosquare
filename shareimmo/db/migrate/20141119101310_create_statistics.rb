class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.references :user, index: true
      t.text :international
      t.text :video
      t.text :kijiji
      t.text :solihome
      t.text :facebook
      t.text :twitter
      t.timestamps
    end
    User.find_each do |user|
      s = Statistic.create(:user_id => user.id)
      s.international[Time.now-8.day] = 0
      s.international[Time.now-3.day] = 0
      if user.profile.world_posting_terms_accepted == 0
        s.international[Time.now] = 0
      else
        s.international[Time.now] = Property.where(:user_id=>user.id,:world_posting_status=>1).where.not(:draft=>1).count
      end
      s.video[Time.now-8.day] = 0
      s.video[Time.now-3.day] = 0
      if user.profile.video_posting_terms_accepted == 0
        s.video[Time.now] = 0
      else
        s.video[Time.now] = Property.where(:user_id=>user.id,:video_posting_status=>1).where.not(:draft=>1).count
      end
      s.kijiji[Time.now-8.day] = 0
      s.kijiji[Time.now-3.day] = 0
      s.solihome[Time.now-8.day] = 0
      s.solihome[Time.now-3.day] = 0
      s.facebook[Time.now-8.day] = 0
      s.facebook[Time.now-3.day] = 0
      s.twitter[Time.now-8.day] = 0
      s.twitter[Time.now-3.day] = 0
      s.save
    end
  end
end
