class CreatePropertyContactTypes < ActiveRecord::Migration
  def change
    create_table :property_contact_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
