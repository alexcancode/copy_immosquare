class AddDescriptionTitleColorToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :description_title_color, :string,:default=>'#666666'
    add_column :custom_themes, :price_color, :string,:default=>'#666666'
  end
end
