class AddInfosToFonts < ActiveRecord::Migration
  def change
    add_reference :fonts, :font_type, index: true
    remove_column :fonts,:type, :string
  end
end
