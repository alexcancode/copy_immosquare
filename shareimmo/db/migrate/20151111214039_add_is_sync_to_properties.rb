class AddIsSyncToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :is_sync_with_api, :integer, :default => 1, :after => :status
  end
end
