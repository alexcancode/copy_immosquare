class CreatePropertyFlagsTranslations < ActiveRecord::Migration
  def self.up
    PropertyFlag.create_translation_table!({
      :name => :string
    }, {
      :migrate_data => true
    })
  end

  def self.down
    PropertyFlag.drop_translation_table! :migrate_data => true
  end
end
