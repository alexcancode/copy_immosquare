class AddPropertySource < ActiveRecord::Migration
  def change
    create_table :property_sources do |t|
      t.text :property_infos
      t.references :api_provider, index: true, foreign_key: false
      t.string :uid
    end
  end
end
