class AddBlogActiveToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :blog_active, :integer, :default => 0
  end
end
