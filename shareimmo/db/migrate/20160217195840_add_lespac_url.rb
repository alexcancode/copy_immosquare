class AddLespacUrl < ActiveRecord::Migration
  def change
    add_column :visibilities, :lespac_ad_url, :string, :after => :lespac_ad_preview_id
  end
end
