class FixUidToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :uid, :integer, :limit=>8
  end
end
