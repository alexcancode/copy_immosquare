class AddCustomerTypeToProfile < ActiveRecord::Migration
  def change
    remove_column :profiles, :customer_type
    add_reference :profiles, :customer_type, index: true,:default=>1
  end
end
