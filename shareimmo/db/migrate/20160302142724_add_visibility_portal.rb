class AddVisibilityPortal < ActiveRecord::Migration
  def change
    create_table :visibility_portals do |t|
      t.integer :posting_status, index: true
      t.integer :preview_id, index: true, :limit => 8
      t.integer :backlink_id, index: true, :limit => 8
      t.references :portal, index: true, foreign_key: false
      t.references :visibility, index: true, foreign_key: false

      t.timestamps null: false

    end
  end
end
