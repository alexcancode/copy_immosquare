class RemoveStartOnToHosting < ActiveRecord::Migration
  def change
    remove_column :hostings, :start_on, :datetime
  end
end
