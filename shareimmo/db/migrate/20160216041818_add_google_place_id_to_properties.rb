class AddGooglePlaceIdToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :google_place_id, :string, :after =>:building_id
  end
end
