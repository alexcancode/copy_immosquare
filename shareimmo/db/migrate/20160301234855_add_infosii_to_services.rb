class AddInfosiiToServices < ActiveRecord::Migration
  def change
    add_reference :contacts, :service, index: true, foreign_key: false
    add_column :contacts, :activation_required, :integer
  end
end
