class AddToiletsToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :wc, :integer
    add_column :properties, :toilets, :integer,:after=>:showerrooms
  end
end
