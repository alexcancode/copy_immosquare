class AddPositionToTodoNomenclatureElements < ActiveRecord::Migration
  def change
    add_column :nomenclature_elements, :position, :integer
  end
end
