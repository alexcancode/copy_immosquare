class AddCraigslistMatchToPropertyClassification < ActiveRecord::Migration
  def change
    add_column :property_classifications, :craigslist_match, :string, :after=> :lespac_match
  end
end
