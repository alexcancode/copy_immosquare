class AddSlugFieldForPortals < ActiveRecord::Migration
  def change
    add_column :portals, :slug, :string, :default => "", :after =>:name
  end
end
