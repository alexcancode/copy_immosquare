class ChangeDefaulHookBackgroundToCustomThemes < ActiveRecord::Migration
  def change
    change_column :custom_themes, :hook_background, :integer, :default =>0
  end
end
