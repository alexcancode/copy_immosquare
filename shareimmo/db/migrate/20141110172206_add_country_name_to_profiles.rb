class AddCountryNameToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :country_name, :string
    add_column :profiles, :is_a_pro_user, :integer, :default=>1
  end
end
