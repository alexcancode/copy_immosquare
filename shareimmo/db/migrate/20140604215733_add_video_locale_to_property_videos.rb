class AddVideoLocaleToPropertyVideos < ActiveRecord::Migration
  def change
    add_column :property_videos, :locale, :string
  end
end
