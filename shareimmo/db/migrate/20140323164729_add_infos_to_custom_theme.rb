class AddInfosToCustomTheme < ActiveRecord::Migration
  def change
    add_column :custom_themes, :hook_background, :boolean, :default => 1
    add_reference :custom_themes, :hook_background_color, index: true, :default=>1
    add_reference :custom_themes, :hook_color, index: true, :default=>2
  end
end
