class RenameOfficeToWebsite < ActiveRecord::Migration
  def change
    rename_table :offices, :websites
  end
end
