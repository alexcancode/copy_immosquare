class AddCancelToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :cancel, :integer, :after=> :id, :default => 0
  end
end
