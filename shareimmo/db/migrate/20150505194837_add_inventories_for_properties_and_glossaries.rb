class AddInventoriesForPropertiesAndGlossaries < ActiveRecord::Migration
  def change
    create_table :inventories do |t|

      t.string :designation
      t.attachment :xml

      t.belongs_to :property
      t.belongs_to :owner

      t.timestamps
    end

    create_table :glossaries do |t|
      t.string :title

      t.belongs_to :owner
      t.timestamps

    end

    create_table :glossary_elements do |t|
      t.string :content
      t.belongs_to :glossary

      t.timestamps
    end

  end
end
