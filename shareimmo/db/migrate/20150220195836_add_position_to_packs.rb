class AddPositionToPacks < ActiveRecord::Migration
  def change
    add_column :packs, :position, :integer
  end
end
