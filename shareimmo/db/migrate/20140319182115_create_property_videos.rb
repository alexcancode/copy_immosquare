class CreatePropertyVideos < ActiveRecord::Migration
  def change
    create_table :property_videos do |t|
      t.references :property, index: true
      t.string :video_id
      t.references :video_provider, index: true

      t.timestamps
    end
  end
end
