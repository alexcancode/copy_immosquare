class CreateWebsiteTypes < ActiveRecord::Migration
  def change
    create_table :website_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
