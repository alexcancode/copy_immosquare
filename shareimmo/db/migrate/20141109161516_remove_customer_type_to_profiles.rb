class RemoveCustomerTypeToProfiles < ActiveRecord::Migration
  def change
    remove_reference :profiles, :customer_type, index: true
    remove_column :profiles, :agency_country
    remove_column :profiles, :secure_token
    remove_column :profiles, :setup_completed
  end
end
