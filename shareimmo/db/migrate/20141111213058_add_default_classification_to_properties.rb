class AddDefaultClassificationToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :property_classification_id, :integer, :default =>1
    change_column :properties, :property_listing_id, :integer, :default =>1
    change_column :properties, :area_unit_id, :integer, :default =>1
  end
end
