class UpdatePropertyForSystemWithPropertySource < ActiveRecord::Migration
  def change
    add_reference :properties, :property_source,:index => true , foreign_key: false, :after => :software_api_provider_id
  end
end
