class AddNamesToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :card_first_name, :string
    add_column :subscriptions, :card_last_name, :string
  end
end
