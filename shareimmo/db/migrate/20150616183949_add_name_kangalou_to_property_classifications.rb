class AddNameKangalouToPropertyClassifications < ActiveRecord::Migration
  def change
    add_column :property_classifications, :name_kangalou, :string, :after => :name
    add_column :property_classification_translations, :name_kangalou, :string, :after => :name
  end
end
