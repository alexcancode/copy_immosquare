class ChangeDefaultValueToFeedsavaks < ActiveRecord::Migration
  def change
    change_column :feedbacks, :advice, :float, :default => nil
  end
end
