class RemoveSocialsToWebsites < ActiveRecord::Migration
  def change
    remove_column :websites, :facebook_link, :string
    remove_column :websites, :twitter_link, :string
    remove_column :websites, :linkedin_link, :string
    remove_column :websites, :google_plus_link, :string
    remove_column :websites, :pinterest_link, :string
  end
end
