class AddVisibilityUntilForVideo < ActiveRecord::Migration
  def change
    add_column :videos, :validity_until , :date, :after => :youtube_id
  end
end
