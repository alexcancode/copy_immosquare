class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.boolean :status
      t.string :locale
      t.string :title
      t.string :slug
      t.text :summary
      t.text :content

      t.timestamps
    end
  end
end
