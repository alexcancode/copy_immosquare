class UpdatePropertyPriceField < ActiveRecord::Migration
  def change
    change_column :properties, :price_cents, :integer, :limit => 5
  end
end
