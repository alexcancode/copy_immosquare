class AddIsDemoWebsiteToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :demo_website, :string, :default => 'no'
  end
end
