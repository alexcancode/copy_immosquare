class AddTestimonialActiveToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :page_testimonial_active, :integer, :default => 0
    add_column :profiles, :page_team_active, :integer,  :default => 0
  end
end
