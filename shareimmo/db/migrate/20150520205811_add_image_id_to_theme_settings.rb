class AddImageIdToThemeSettings < ActiveRecord::Migration
  def change
    add_column :theme_settings, :img_id, :string, :after => :wistia_id
  end
end
