class AddColorsToCustomThemes < ActiveRecord::Migration
  def change
    remove_column :custom_themes,:background_color, :string
    remove_column :custom_themes,:title_color, :string
    remove_column :custom_themes,:link_color, :string
    add_column :custom_themes, :website_background_color, :string, :default=> '#ffffff'
    add_column :custom_themes, :top_toolbar_background_color, :string, :default=> '#209875'
    add_column :custom_themes, :top_toolbar_text, :string, :default=> '#ffffff'
    add_column :custom_themes, :link_color, :string, :default=> '#cb2b5c'
    add_column :custom_themes, :nav_link_color, :string, :default=> '#888888'
    add_column :custom_themes, :text_color, :string, :default=> '#444444'
    add_column :custom_themes, :heading_text_color, :string, :default=> '#cb2b5c'
    add_column :custom_themes, :button_background_color, :string, :default=> '#209875'
    add_column :custom_themes, :border_color, :string, :default=> '#dddddd'
  end
end
