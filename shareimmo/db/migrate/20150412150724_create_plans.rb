class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.integer :amount
      t.string :interveral
      t.string :stripe_id
      t.string :name

      t.timestamps null: false
    end
  end
end
