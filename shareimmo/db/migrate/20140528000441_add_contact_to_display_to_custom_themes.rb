class AddContactToDisplayToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :contact_or_business_to_display, :integer,:default=>1
  end
end
