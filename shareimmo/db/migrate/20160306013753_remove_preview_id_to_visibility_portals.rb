class RemovePreviewIdToVisibilityPortals < ActiveRecord::Migration
  def change
    remove_column :visibility_portals, :preview_id, :string
  end
end
