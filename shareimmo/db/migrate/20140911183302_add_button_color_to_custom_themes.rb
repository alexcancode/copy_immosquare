class AddButtonColorToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :button_color, :string,:default => '#f5f7fa'
    add_column :custom_theme_presets, :button_color, :string
  end
end
