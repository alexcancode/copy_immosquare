class ChangeColumsOfOffice < ActiveRecord::Migration
  def change
    rename_column :offices, :type, :type_of_office
  end
end
