class AddRemoveAndReplaceToApiApps < ActiveRecord::Migration
  def change
    add_column :api_apps, :cancel_and_replace, :integer, :after => "api_provider_id"
    remove_column :api_providers, :is_a_flux
    remove_column :api_providers, :can_erease_account
    remove_column :api_providers, :can_erease_property
  end
end
