class CreatePrintOrders < ActiveRecord::Migration
  def change
    create_table :print_orders do |t|
      t.references :property,       index: true, foreign_key: false
      t.references :print_template, index: true, foreign_key: false
      t.string :status
      t.text :json_template

      t.timestamps null: false
    end
  end
end
