class AddDeleteOrInactiveOldAdvertsToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :delete_old_adverts, :integer, :default => 1
    add_column :profiles, :auto_update_adverts, :integer, :default => 1
    add_column :profiles, :auto_update_profile, :integer, :default => 1
  end
end
