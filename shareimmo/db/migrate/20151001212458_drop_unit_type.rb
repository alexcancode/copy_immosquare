class DropUnitType < ActiveRecord::Migration
  def change
    drop_table :unit_types
    drop_table :unit_type_translations
  end
end
