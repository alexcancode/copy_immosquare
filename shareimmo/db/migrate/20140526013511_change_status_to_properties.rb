class ChangeStatusToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :status, :integer, :default =>1
  end
end
