class AddMandateWebsiteToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :mandate_website, :integer, :default => 0
  end
end
