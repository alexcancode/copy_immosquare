class AddSecureTokenForInventory < ActiveRecord::Migration
  def change
    add_column :inventories, :secure_token, :string, :after => :secure_password
    add_column :inventories, :secure_token_expiration, :datetime, :after => :secure_token
  end
end
