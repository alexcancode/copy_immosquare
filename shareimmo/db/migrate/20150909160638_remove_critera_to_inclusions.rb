class RemoveCriteraToInclusions < ActiveRecord::Migration
  def change
    remove_column :property_inclusions, :criteria_accessible, :integer
    remove_column :property_inclusions, :criteria_perfect_for_senior, :integer
    remove_column :property_inclusions, :criteria_open_space, :integer
    remove_column :property_inclusions, :criteria_pets_allow, :integer
    remove_column :property_inclusions, :criteria_no_carpet, :integer
    remove_column :property_inclusions, :criteria_balcony, :integer
    remove_column :property_inclusions, :criteria_lower_price, :integer
    remove_column :property_inclusions, :criteria_many_storage, :integer
    remove_column :property_inclusions, :criteria_good_location, :integer
    remove_column :property_inclusions, :criteria_charisma, :integer
    remove_column :property_inclusions, :criteria_contemporary, :integer
    remove_column :property_inclusions, :criteria_accessed_court, :integer
    remove_column :property_inclusions, :criteria_separate_shower, :integer
    remove_column :property_inclusions, :criteria_sunny, :integer
    remove_column :property_inclusions, :criteria_green_area, :integer
    remove_column :property_inclusions, :criteria_ideal_for_family, :integer
    remove_column :property_inclusions, :criteria_ideal_for_young_couple, :integer
    remove_column :property_inclusions, :criteria_ideal_for_studient, :integer
    remove_column :property_inclusions, :criteria_ideal_for_one_person, :integer
    remove_column :property_inclusions, :criteria_big_space, :integer
    remove_column :property_inclusions, :criteria_big_kitchen, :integer
    remove_column :property_inclusions, :criteria_very_big, :integer
    remove_column :property_inclusions, :criteria_without_smoke, :integer
    remove_column :property_inclusions, :criteria_soundproof, :integer
    remove_column :property_inclusions, :criteria_loft, :integer
    remove_column :property_inclusions, :criteria_luxury, :integer
    remove_column :property_inclusions, :criteria_new_building, :integer
    remove_column :property_inclusions, :criteria_modern, :integer
    remove_column :property_inclusions, :criteria_bicycle_path, :integer
    remove_column :property_inclusions, :criteria_high_ceiling, :integer
    remove_column :property_inclusions, :criteria_wood_floor, :integer
    remove_column :property_inclusions, :criteria_clean, :integer
    remove_column :property_inclusions, :criteria_homeowner, :integer
    remove_column :property_inclusions, :criteria_renovated, :integer
    remove_column :property_inclusions, :criteria_kitchen_renovated, :integer
    remove_column :property_inclusions, :criteria_bathroom_renovated, :integer
    remove_column :property_inclusions, :criteria_patio, :integer
    remove_column :property_inclusions, :criteria_public_transport, :integer
    remove_column :property_inclusions, :criteria_nice_neighborhood, :integer
    remove_column :property_inclusions, :criteria_nice_view, :integer

  end
end
