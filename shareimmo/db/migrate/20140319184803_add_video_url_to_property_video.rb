class AddVideoUrlToPropertyVideo < ActiveRecord::Migration
  def change
    add_column :property_videos, :video_url, :string
  end
end
