class AddAvatarPositionToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :avatar_position, :integer, :default => 9
  end
end
