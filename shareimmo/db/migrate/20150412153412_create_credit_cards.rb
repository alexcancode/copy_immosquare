class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.references :user, index: true
      t.string :stripe_id
      t.string :object
      t.string :last4
      t.string :brand
      t.string :funding
      t.integer :exp_month
      t.integer :exp_year
      t.string :country
      t.string :name
      t.text :stripe_infos

      t.timestamps null: false
    end
  end
end
