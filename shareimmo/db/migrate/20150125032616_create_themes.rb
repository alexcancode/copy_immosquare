class CreateThemes < ActiveRecord::Migration
  def change
    create_table :themes do |t|
      t.string :name
      t.string :primary_color
      t.string :secondary_color
      t.string :tertiary_color

      t.timestamps
    end
  end
end
