class RenameAllurToPropertyComplements < ActiveRecord::Migration
  def change
    rename_column :property_complements, :france_allur_is_condo, :france_alur_is_condo
    rename_column :property_complements, :france_allur_units, :france_alur_units
    rename_column :property_complements, :france_allur_uninhabitables, :france_alur_uninhabitables
    rename_column :property_complements, :france_allur_spending, :france_alur_spending
    rename_column :property_complements, :france_allur_legal_action, :france_alur_legal_action
  end
end
