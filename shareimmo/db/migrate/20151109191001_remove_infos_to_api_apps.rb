class RemoveInfosToApiApps < ActiveRecord::Migration
  def change
    remove_column :api_apps, :cancel_and_replace, :integer
  end
end
