class AddPopularToPacks < ActiveRecord::Migration
  def change
    add_column :packs, :is_popular, :integer, :default => 0
  end
end
