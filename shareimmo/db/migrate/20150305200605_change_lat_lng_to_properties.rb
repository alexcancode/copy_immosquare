class ChangeLatLngToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :latitude, :decimal, {:precision=>10, :scale=>6}
    change_column :properties, :longitude, :decimal, {:precision=>10, :scale=>6}
  end
end
