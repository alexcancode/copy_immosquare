class RemoveForgeiToPurchases < ActiveRecord::Migration
  def change
    remove_foreign_key :purchases, :product
    remove_foreign_key :purchases, :property
    remove_foreign_key :purchases, :user
  end
end
