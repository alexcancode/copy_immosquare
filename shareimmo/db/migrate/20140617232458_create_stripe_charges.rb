class CreateStripeCharges < ActiveRecord::Migration
  def change
    create_table :stripe_charges do |t|
      t.references :subscription, index: true
      t.money :amount
      t.string :amount_currency
      t.boolean :paid
      t.boolean :refunded
      t.string :customer
      t.string :invoice

      t.timestamps
    end
  end
end
