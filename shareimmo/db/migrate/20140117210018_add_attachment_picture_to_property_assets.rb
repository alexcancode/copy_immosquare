class AddAttachmentPictureToPropertyAssets < ActiveRecord::Migration
  def self.up
    change_table :property_assets do |t|
      t.attachment :picture
    end
  end

  def self.down
    drop_attached_file :property_assets, :picture
  end
end
