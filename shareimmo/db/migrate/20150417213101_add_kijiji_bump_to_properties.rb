class AddKijijiBumpToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :kijiji_bump, :integer, :after =>:kijiji_posting_status
    add_column :properties, :louer_posting_status, :integer, :after => :lespac_ad_preview_id
    add_column :properties, :louer_ad_id, :integer, :after => :louer_posting_status
  end
end
