class RemoveLinesToWebsites < ActiveRecord::Migration
  def change
    remove_column :websites, :line_1, :string
    remove_column :websites, :line_2, :string
    remove_column :websites, :line_3, :string
    remove_column :websites, :line_4, :string
  end
end
