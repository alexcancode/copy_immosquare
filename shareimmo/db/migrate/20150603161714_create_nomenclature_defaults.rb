class CreateNomenclatureDefaults < ActiveRecord::Migration
  def change
    create_table :nomenclature_defaults do |t|
      t.string :name
      t.string :description
      t.string :country

      t.timestamps null: false
    end
  end
end
