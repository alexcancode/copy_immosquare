class RemoveTypeOfOfficeToWebsite < ActiveRecord::Migration
  def change
    rename_column :websites, :type_of_office, :website_type_id
    change_column :websites, :website_type_id, :integer
    add_index :websites, :website_type_id, unique: true
  end
end
