class AddFreeTokenToPacks < ActiveRecord::Migration
  def change
    add_column :packs, :free_token, :integer
  end
end
