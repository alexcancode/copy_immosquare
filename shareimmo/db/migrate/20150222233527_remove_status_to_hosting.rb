class RemoveStatusToHosting < ActiveRecord::Migration
  def change
    remove_column :hostings, :status, :integer
  end
end
