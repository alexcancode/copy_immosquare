class RenameWorldPostingStatusToProperties < ActiveRecord::Migration
  def change
    rename_column :properties, :world_posting_status, :international_posting_status
  end
end
