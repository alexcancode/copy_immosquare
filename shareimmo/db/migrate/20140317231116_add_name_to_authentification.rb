class AddNameToAuthentification < ActiveRecord::Migration
  def change
    add_column :authentifications, :name, :string
  end
end
