class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.references :user, index: true
      t.references :product, index: true
      t.integer :cost
      t.references :property, index: true
      t.datetime :periode_start
      t.datetime :periode_stop
      t.string :description
      t.timestamps null: false
    end
    add_foreign_key :purchases, :users
    add_foreign_key :purchases, :properties
  end
end
