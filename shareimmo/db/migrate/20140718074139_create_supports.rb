class CreateSupports < ActiveRecord::Migration
  def change
    create_table :supports do |t|
      t.string :title
      t.integer :status
      t.string :locale
      t.references :support_type, index: true
      t.text :summary
      t.text :content

      t.timestamps
    end
  end
end
