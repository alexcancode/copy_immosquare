class RemoveIsPortalFromApiApps < ActiveRecord::Migration
  def change
    remove_column :api_apps, :is_portal, :integer
  end
end
