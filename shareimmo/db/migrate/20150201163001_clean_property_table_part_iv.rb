class CleanPropertyTablePartIv < ActiveRecord::Migration
  def change
    change_table :properties do |t|
      t.change :title, :string, :after => :parking
      t.change :description, :text, :after => :title
      t.change :property_url, :string, :after => :description
      t.change :availability_date, :datetime, :after => :year_of_built
      t.change :price_currency, :string, :after => :price_cents
      t.change :rent_currency, :string, :after => :rent_cents
      t.change :tax_1_currency, :string, :after => :tax_1_cents
      t.change :tax_2_currency, :string, :after => :tax_2_cents
      t.change :condo_fees_currency, :string, :after => :condo_fees_cents
    end
  end
end
