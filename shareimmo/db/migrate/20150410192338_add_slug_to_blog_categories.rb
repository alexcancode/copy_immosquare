class AddSlugToBlogCategories < ActiveRecord::Migration
  def change
    add_column :blog_categories, :slug, :string, :after => :name
  end
end
