class AddIconsToSupporType < ActiveRecord::Migration
  def change
    add_column :support_types, :icon, :string
  end
end
