class RemoveOldFieldsToProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :profile_completed, :boolean
    remove_column :profiles, :username, :string
    remove_column :profiles, :slug, :string
    remove_column :profiles, :email, :string
    remove_column :profiles, :website, :string
    remove_column :profiles, :job, :string
    remove_column :profiles, :languages, :string
    remove_column :profiles, :bio, :string
    remove_column :profiles, :qsc, :string
    remove_column :profiles, :facebook, :string
    remove_column :profiles, :twitter, :string
    remove_column :profiles, :baseline, :string
    remove_column :profiles, :cover_file_name
    remove_column :profiles, :cover_content_type
    remove_column :profiles, :cover_file_size
    remove_column :profiles, :cover_updated_at
    remove_column :profiles, :avatar_file_name
    remove_column :profiles, :avatar_content_type
    remove_column :profiles, :avatar_file_size
    remove_column :profiles, :avatar_updated_at
  end
end
