class AddOriginalsPicturesUrlsToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :picture_original_url, :string, :after => :updated_at
    add_column :profiles, :logo_original_url, :string, :after => :agency_province
  end
end
