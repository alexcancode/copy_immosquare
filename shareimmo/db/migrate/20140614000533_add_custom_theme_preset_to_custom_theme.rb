class AddCustomThemePresetToCustomTheme < ActiveRecord::Migration
  def change
    add_reference :custom_themes, :custom_theme_preset, index: true, :default => 1
  end
end
