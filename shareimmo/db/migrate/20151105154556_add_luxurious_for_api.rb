class AddLuxuriousForApi < ActiveRecord::Migration
  def change
    add_column :properties, :is_luxurious, :integer, :default => 0, :after => :mls
  end
end
