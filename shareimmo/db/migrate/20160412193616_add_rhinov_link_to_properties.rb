class AddRhinovLinkToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :rhinov_link, :string
  end
end
