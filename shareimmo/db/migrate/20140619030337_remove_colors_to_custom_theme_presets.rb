class RemoveColorsToCustomThemePresets < ActiveRecord::Migration
  def change
    remove_column :custom_theme_presets, :top_toolbar_background_color, :string
    remove_column :custom_theme_presets, :top_toolbar_text, :string
  end
end
