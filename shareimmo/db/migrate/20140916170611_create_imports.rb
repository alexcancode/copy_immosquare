class CreateImports < ActiveRecord::Migration
  def change
    create_table :imports do |t|
      t.references :website, index: true
      t.string :import_provider
      t.string :centris_agency_name
      t.string :centris_director_name
      t.string :centris_agency_address
      t.integer :centris_import_type,:default=>1
      t.string :centris_technical_tel

      t.timestamps
    end
  end
end
