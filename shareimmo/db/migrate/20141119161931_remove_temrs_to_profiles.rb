class RemoveTemrsToProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :world_posting_terms_accepted, :string
    remove_column :profiles, :video_posting_terms_accepted, :string
    remove_column :profiles, :kijiji_posting_terms_accepted, :string
  end
end
