class ChangeDefaultValueToCustomThemes < ActiveRecord::Migration
  def change
    change_column :custom_themes, :background_color, :string, :default => '#c6bcd1'
    change_column :custom_themes, :title_color, :string, :default => '#ac88d4'
    change_column :custom_themes, :link_color, :string, :default => '#142d5c'
    change_column :custom_themes, :text_font, :string, :default => 'Avenir,"Helvetica Neue", Helvetica, Arial, sans-serif'
  end
end
