class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.integer :lat
      t.integer :long
      t.integer :zoom

      t.timestamps
    end
  end
end
