class TranslatePacks < ActiveRecord::Migration
  def change
     Pack.create_translation_table!({
      :name => :string,
      :content => :text,
    }, {
      :migrate_data => true
    })
  end
end

