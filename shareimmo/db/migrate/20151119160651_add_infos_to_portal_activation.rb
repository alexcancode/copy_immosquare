class AddInfosToPortalActivation < ActiveRecord::Migration
  def change
    add_column :portal_activations, :limitation, :integer
  end
end
