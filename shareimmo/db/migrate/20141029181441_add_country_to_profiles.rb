class AddCountryToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :agency_province, :string
    add_column :profiles, :agency_country, :string
    remove_column :profiles, :agency_first_name, :string
    remove_column :profiles, :agency_last_name, :string
    remove_column :profiles, :agency_email, :string
    remove_column :profiles, :agency_spoken_languages, :string

  end
end
