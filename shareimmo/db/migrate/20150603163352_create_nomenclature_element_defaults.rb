class CreateNomenclatureElementDefaults < ActiveRecord::Migration
  def change
    create_table :nomenclature_element_defaults do |t|
      t.string :name
      t.string :value
      t.integer :element_type
      t.references :nomenclature_default, index: true, foreign_key: true
      t.integer :status, :default => 1
      t.string :ancestry
      t.integer :position
      t.integer :import_id

      t.timestamps null: false
    end
  end
end
