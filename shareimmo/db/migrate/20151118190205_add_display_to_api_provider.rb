class AddDisplayToApiProvider < ActiveRecord::Migration
  def change
    add_column :api_providers, :is_display_on_sync_with, :integer, :default => 0, :after => :name
  end
end
