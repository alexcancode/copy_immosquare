class AddAttachmentLogoToOffices < ActiveRecord::Migration
  def self.up
    change_table :offices do |t|
      t.attachment :logo
    end
  end

  def self.down
    drop_attached_file :offices, :logo
  end
end
