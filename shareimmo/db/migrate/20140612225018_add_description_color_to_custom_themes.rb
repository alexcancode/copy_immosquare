class AddDescriptionColorToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :description_color, :string,:default=> '#00449e'
  end
end
