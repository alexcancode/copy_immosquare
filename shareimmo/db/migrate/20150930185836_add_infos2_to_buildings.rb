class AddInfos2ToBuildings < ActiveRecord::Migration
  def change
    rename_column :buildings ,:city, :locality
    rename_column :buildings ,:country, :country_short
    rename_column :buildings, :province, :administrative_area_level_1_long
    add_column :buildings, :address, :string, :after => :number_of_levels
    add_column :buildings, :sublocality, :string
    add_column :buildings, :latitude, :float
    add_column :buildings, :longitude, :float
  end
end
