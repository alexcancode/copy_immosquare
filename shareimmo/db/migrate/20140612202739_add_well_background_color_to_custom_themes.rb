class AddWellBackgroundColorToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :well_background_color, :string,:default=> '#E2E6EA'
  end
end
