class AddValidityForVisibilityPortal < ActiveRecord::Migration
  def change
    add_column :visibility_portals, :validity_until , :date, :after => :property_id
    add_column :visibility_portals, :validity_start , :date, :after => :property_id  	
  end
end
