class AddAmountToPacks < ActiveRecord::Migration
  def change
    remove_column :packs, :price_cents, :text
    remove_column :packs, :price_currency, :text
    add_column :packs, :amount, :integer, :after=>:name
    add_column :packs, :amount_without_taxs, :integer, :after=>:amount
    add_column :packs, :currency, :string, :after=>:amount_without_taxs
    add_column :packs, :tax1, :text
    add_column :packs, :tax2, :text
  end
end
