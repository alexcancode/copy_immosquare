class AddUsersToPropertyContacts < ActiveRecord::Migration
  def change
    add_reference :property_contacts, :user, index: true, foreign_key: false, :after => :api_provider_id
    change_column :property_contacts, :email , :string ,:after => :user_id
  end
end
