class AddRoomIdToPropertyAssets < ActiveRecord::Migration
  def change
    add_reference :property_assets, :room, index: true
  end
end
