class CreatePacks < ActiveRecord::Migration
  def change
    create_table :packs do |t|
      t.string :name
      t.integer :token
      t.money :price
      t.timestamps null: false
    end
  end
end
