class RemoveFavortieToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :favorite, :string
  end
end
