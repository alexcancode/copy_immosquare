class AddAccessKeyForPortal < ActiveRecord::Migration
  def change
    add_column :portals, :access_key, 		:string, 	:default => "", :after => :url
  end
end
