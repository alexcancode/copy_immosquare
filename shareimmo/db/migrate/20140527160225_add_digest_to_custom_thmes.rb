class AddDigestToCustomThmes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :digest, :string
  end
end
