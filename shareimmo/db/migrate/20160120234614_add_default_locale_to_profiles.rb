class AddDefaultLocaleToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :default_locale, :string, :default => "fr"
  end
end
