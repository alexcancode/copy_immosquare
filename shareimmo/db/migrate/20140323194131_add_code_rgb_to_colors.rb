class AddCodeRgbToColors < ActiveRecord::Migration
  def change
    add_column :colors, :code_rgb, :string
  end
end
