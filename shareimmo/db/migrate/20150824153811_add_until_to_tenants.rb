class AddUntilToTenants < ActiveRecord::Migration
  def change
    add_column :tenants, :until, :date
  end
end
