class AddChannelToPrintsOrder < ActiveRecord::Migration
  def change
    add_column :print_orders, :channel, :string, :after => :json_template
  end
end
