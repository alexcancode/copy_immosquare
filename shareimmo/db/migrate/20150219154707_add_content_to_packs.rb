class AddContentToPacks < ActiveRecord::Migration
  def change
    add_column :packs, :content, :text
  end
end
