class AddShowProfilePictureToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :show_profile_picture, :integer,:default => 0
  end
end
