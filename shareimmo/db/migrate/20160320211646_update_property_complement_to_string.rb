class UpdatePropertyComplementToString < ActiveRecord::Migration
  def change
    change_column :property_complements, :canada_mls, :string, :after => :belgium_risk_area
    change_column :property_complements, :france_dpe_indice, :string
    change_column :property_complements, :france_dpe_value, :string
    change_column :property_complements, :france_dpe_id, :string
    change_column :property_complements, :france_ges_indice, :string
    change_column :property_complements, :france_ges_value, :string
    change_column :property_complements, :belgium_peb_indice, :string
    change_column :property_complements, :belgium_peb_value, :string
    change_column :property_complements, :belgium_peb_id, :string
  end
end
