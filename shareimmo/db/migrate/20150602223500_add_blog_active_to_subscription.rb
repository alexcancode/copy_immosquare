class AddBlogActiveToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :blog_actif, :integer, :default => 0, :after => :stripe_plan_id
  end
end
