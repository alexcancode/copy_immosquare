class RemoveInfosToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :international_posting_status, :integer
    remove_column :properties, :video_posting_status, :integer
    remove_column :properties, :kijiji_posting_status, :integer
    remove_column :properties, :kijiji_bump, :integer
    remove_column :properties, :kijiji_ad_id, :integer
    remove_column :properties, :craigslist_ad_url, :string
    remove_column :properties, :craigslist_posting_status, :string
    remove_column :properties, :lespac_posting_status, :integer
    remove_column :properties, :lespac_ad_id, :integer
    remove_column :properties, :lespac_ad_preview_id, :string
    remove_column :properties, :louer_posting_status, :integer
    remove_column :properties, :louer_ad_id, :string
  end
end
