class AddDraftToFeedbacks < ActiveRecord::Migration
  def change
    add_column :feedbacks, :draft, :integer, :default => 0, :after => :id
  end
end
