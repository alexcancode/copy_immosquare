class AddInstagramToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :instagram, :string, :after => :google_plus
  end
end
