class UpdateUidForProperties < ActiveRecord::Migration
  def change
    rename_column :properties, :uid, :portal_uid
    rename_column :properties, :api_provider_id, :portal_api_provider_id
  end
end
