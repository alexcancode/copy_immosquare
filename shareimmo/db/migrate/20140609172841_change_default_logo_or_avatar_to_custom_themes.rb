class ChangeDefaultLogoOrAvatarToCustomThemes < ActiveRecord::Migration
  def change
    change_column :custom_themes, :logo_or_avatar, :integer, :default =>0
  end
end
