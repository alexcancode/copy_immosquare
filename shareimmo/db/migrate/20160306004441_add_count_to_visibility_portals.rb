class AddCountToVisibilityPortals < ActiveRecord::Migration
  def change
    add_column :visibility_portals, :count, :integer, :default => 0, :after =>:posting_status
    add_column :visibility_portals, :history, :text
  end
end
