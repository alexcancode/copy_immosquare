class AddWithBgToPortals < ActiveRecord::Migration
  def change
    add_column :portals, :with_bg, :integer
  end
end
