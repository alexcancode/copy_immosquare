class AddAttachmentDocumentToPdfs < ActiveRecord::Migration
  def self.up
    change_table :pdfs do |t|
      t.attachment :document
    end
  end

  def self.down
    remove_attachment :pdfs, :document
  end
end
