class AddAssetTypeToPropertiesAssets < ActiveRecord::Migration
  def change
    add_column :property_assets, :asset_type, :integer, :default=> 1, :after => :property_id
  end
end
