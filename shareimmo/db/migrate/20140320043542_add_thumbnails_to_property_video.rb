class AddThumbnailsToPropertyVideo < ActiveRecord::Migration
  def change
    add_column :property_videos, :thumbnail_small, :string
    add_column :property_videos, :thumbnail_large, :string
  end
end
