class OrderPackPrice < ActiveRecord::Migration
  def change
    change_column :charges, :pack_price_cents, :integer, :after => :user_id
  end
end
