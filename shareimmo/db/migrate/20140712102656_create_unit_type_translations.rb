class CreateUnitTypeTranslations < ActiveRecord::Migration

  def self.up
    UnitType.create_translation_table!({
      :name => :string
    }, {
      :migrate_data => true
    })
  end

  def self.down
    UnitType.drop_translation_table! :migrate_data => true
  end
end




