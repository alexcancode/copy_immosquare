class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.references :property, index: true, foreign_key: false
      t.references :authentification, index: true, foreign_key: false
      t.string :uid
      t.string :youtube_id
      t.timestamps null: false
    end
  end
end
