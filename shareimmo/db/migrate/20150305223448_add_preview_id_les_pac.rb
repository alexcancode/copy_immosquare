class AddPreviewIdLesPac < ActiveRecord::Migration
  def change
    add_column :properties, :lespac_ad_preview_id, :string, :after => :lespac_ad_id
  end
end
