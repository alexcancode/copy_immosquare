class AddDescriptionShortToBuildings < ActiveRecord::Migration
  def change
    add_column :buildings, :description_short, :text
    add_column :building_translations, :description_short, :text
  end
end
