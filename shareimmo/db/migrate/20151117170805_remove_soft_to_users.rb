class RemoveSoftToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :software_uid, :string
    remove_column :users, :software_api_provider_id, :integer
  end
end
