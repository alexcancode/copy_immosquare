class CreatePortalTypeTranslations < ActiveRecord::Migration

   def self.up
    PortalType.create_translation_table!({
      :name => :string,
    }, {
      :migrate_data => false
    })
    Service.create_translation_table!({
      :name => :string,
      :description => :text,
    }, {
      :migrate_data => false
    })
  end

  def self.down
    PortalType.drop_translation_table! :migrate_data => true
    Service.drop_translation_table! :migrate_data => true
  end

end


