class RemoveSubLocalityToBuildings < ActiveRecord::Migration
  def change
    remove_column :buildings, :sublocality, :string
    change_column :buildings, :country, :string, :default => "CA"
  end
end
