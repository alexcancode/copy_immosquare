class AddAthomeMatchToPropertyClassifications < ActiveRecord::Migration
  def change
    add_column :property_classifications, :athome_match, :integer
  end
end
