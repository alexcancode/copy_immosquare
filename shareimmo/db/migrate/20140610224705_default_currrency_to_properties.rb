class DefaultCurrrencyToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :price_currency, :string, :default =>"CAD"
  end
end
