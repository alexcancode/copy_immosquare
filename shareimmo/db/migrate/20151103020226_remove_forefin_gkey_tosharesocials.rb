class RemoveForefinGkeyTosharesocials < ActiveRecord::Migration
  def change
    remove_foreign_key('share_socials', 'user')
    remove_foreign_key('share_socials', 'property')
    remove_foreign_key('share_socials', 'api_provider')
  end
end
