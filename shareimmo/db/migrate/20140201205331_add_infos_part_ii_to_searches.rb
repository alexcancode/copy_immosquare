class AddInfosPartIiToSearches < ActiveRecord::Migration
  def change
    add_reference :searches, :listing_types, index: true
    add_reference :searches, :property_classifications, index: true
    remove_column :searches, :lat, :interger
    remove_column :searches, :long, :interger
    remove_column :searches, :zoom, :interger
    remove_column :searches, :status, :interger
  end
end
