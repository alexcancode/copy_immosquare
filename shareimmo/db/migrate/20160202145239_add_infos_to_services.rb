class AddInfosToServices < ActiveRecord::Migration
  def change
    add_column :services, :wistia_id, :string ,:after => :company
    add_column :services, :immosquare_id, :string ,:after => :wistia_id
    add_column :services, :description, :text, :after => :immosquare_id
  end
end
