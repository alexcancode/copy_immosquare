class AddPackPriceToCharges < ActiveRecord::Migration
  def change
    add_money :charges, :pack_price, currency: { present: false }, :after=> :user_id
  end
end
