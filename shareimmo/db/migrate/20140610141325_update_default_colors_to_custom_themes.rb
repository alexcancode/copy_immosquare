class UpdateDefaultColorsToCustomThemes < ActiveRecord::Migration
  def change
    change_column :custom_themes, :top_toolbar_background_color, :string, :default =>"#2e2e2e"
    change_column :custom_themes, :top_toolbar_text, :string, :default =>"#f0f0f0"
  end
end
