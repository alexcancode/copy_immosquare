class UpdatedefaultValueToAreaUnits < ActiveRecord::Migration
  def change
    change_column :property_lots, :area_unit_id, :integer, :default =>2
  end
end
