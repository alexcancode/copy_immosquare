class AddDuratiobNameToProduct < ActiveRecord::Migration
  def change
    add_column :products, :duration_name, :string, :after=> :duration, :default => "month"
  end
end
