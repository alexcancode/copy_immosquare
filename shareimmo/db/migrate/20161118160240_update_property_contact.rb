class UpdatePropertyContact < ActiveRecord::Migration
  def change
    add_column :property_contacts, :mobile, :string, :after => :phone
    add_column :property_contacts, :iban, :string, :after => :country
  end
end
