class RemovePriceCentsToProperty < ActiveRecord::Migration
  def change
    remove_column :properties, :price_cents, :integer
  end
end
