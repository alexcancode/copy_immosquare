class CreateScrappingMonitorings < ActiveRecord::Migration
  def change
    create_table :scrapping_monitorings do |t|
      t.references :api_provider, index: true
      t.integer :ads_count

      t.timestamps null: false
    end
    add_foreign_key :scrapping_monitorings, :api_providers
  end
end
