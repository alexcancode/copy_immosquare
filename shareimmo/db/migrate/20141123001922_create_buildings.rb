class CreateBuildings < ActiveRecord::Migration
  def change
    create_table :buildings do |t|
      t.integer :uid, index: true
      t.references :api_provider_api, index: true
      t.references :user, index: true
      t.string :name
      t.string :address
      t.integer :number_of_units
      t.timestamps
    end

    add_reference :properties, :building, index: true, :after=>:deleted
  end
end
