class AddCardStripeIdToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :stripe_card_id, :string
  end
end
