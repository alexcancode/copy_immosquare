class CleanPropertyTablePartI < ActiveRecord::Migration
  def change
    change_table :properties do |t|
      t.change :draft, :integer, :after => :api_provider_id
      t.change :secure_id, :string, :after => :uid
      t.change :deleted, :integer, :after => :draft
      t.change :showerrooms, :integer, :after => :bedrooms
      t.change :parking, :integer, :after => :showerrooms
    end
  end
end
