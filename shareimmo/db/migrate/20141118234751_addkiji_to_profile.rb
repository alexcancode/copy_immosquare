class AddkijiToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :kijiji_posting_terms_accepted, :integer, :default=>0
    change_column :profiles, :world_posting_terms_accepted, :integer, :after=> :is_a_pro_user
    change_column :profiles, :video_posting_terms_accepted, :integer, :after=> :world_posting_terms_accepted
  end
end
