class AddWorldPostingTermsAcceptedToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :world_posting_terms_accepted, :integer, :default=>0
  end
end
