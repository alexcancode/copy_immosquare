class ChangeDateToVisibility < ActiveRecord::Migration
  def change
    change_column :visibilities, :validity_until, :date
    add_column :visibilities, :athome_posting_status, :integer
    add_column :visibilities, :athome_ad_id, :string
    add_column :visibilities, :immotop_posting_status, :integer
    add_column :visibilities, :immotop_ad_id, :string
    add_column :visibilities, :wortimmo_posting_status, :integer
    add_column :visibilities, :wortimmo_ad_id, :string
    add_column :visibilities, :habiter_lu_posting_status, :integer
    add_column :visibilities, :habiter_lu_ad_id, :string
  end
end
