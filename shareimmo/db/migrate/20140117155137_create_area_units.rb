class CreateAreaUnits < ActiveRecord::Migration
  def change
    create_table :area_units do |t|
      t.string :name
      t.string :ref
      t.string :symbol
      t.integer :conversion_rate,:default => 1

      t.timestamps
    end
  end
end
