class RemoveOldsToBuildings < ActiveRecord::Migration
  def change
    remove_column :buildings, :software_uid, :integer
    remove_column :buildings, :software_api_provider_id, :integer
  end
end
