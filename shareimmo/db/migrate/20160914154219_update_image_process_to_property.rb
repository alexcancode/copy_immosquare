class UpdateImageProcessToProperty < ActiveRecord::Migration
  def change
    change_column :properties, :image_process, :integer, :default =>1
  end
end
