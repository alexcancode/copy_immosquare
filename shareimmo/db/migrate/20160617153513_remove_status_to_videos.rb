class RemoveStatusToVideos < ActiveRecord::Migration
  def change
    remove_column :videos, :status, :integer
  end
end
