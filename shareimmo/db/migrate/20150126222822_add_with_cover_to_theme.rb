class AddWithCoverToTheme < ActiveRecord::Migration
  def change
    add_column :themes, :with_cover, :integer, :default =>0, :after => :tertiary_color
  end
end
