class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.string :provider_id
      t.string :name
      t.string :sudomains
      t.string :hosts
      t.string :host
      t.string :email
      t.string :link_host
      t.string :color_1
      t.string :color_2
      t.string :logo
      t.string :phone


      t.timestamps null: false
    end
  end
end
