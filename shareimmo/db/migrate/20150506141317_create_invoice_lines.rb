class CreateInvoiceLines < ActiveRecord::Migration
  def change
    create_table :invoice_lines do |t|
      t.references :invoice, index: true, foreign_key: false
      t.integer :price
      t.string :services
      t.integer :quantity
      t.string :description
      t.text :tax1
      t.text :tax2
      t.string :stripe_id
      t.timestamps null: false
    end
  end
end
