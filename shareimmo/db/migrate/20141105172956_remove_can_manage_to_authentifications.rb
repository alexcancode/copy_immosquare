class RemoveCanManageToAuthentifications < ActiveRecord::Migration
  def change
    remove_column :authentifications, :can_manage_shareimmo_page, :integer
    remove_column :authentifications, :manage_shareimmo_twitter, :integer
  end
end
