class AddConnectTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :connect_token, :string
  end
end
