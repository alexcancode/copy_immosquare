class CleanInfotTocustomeThemes < ActiveRecord::Migration
  def change
    remove_column :custom_themes, :text_font, :string
    remove_column :custom_themes, :digest, :string
  end
end
