class AddBySmsToBrokerContacts < ActiveRecord::Migration
  def change
    add_column :broker_contacts, :by_sms, :integer, :default => 0 , :after=>:user_id
  end
end
