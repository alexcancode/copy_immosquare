class AddSomeFieldsInApiV2Attic < ActiveRecord::Migration
  def change
    add_column :property_inclusions, :indoor_attic_convertible, :integer, :after => :indoor_attic, :default => 0
    add_column :property_inclusions, :indoor_attic_converted,   :integer, :after => :indoor_attic_convertible, :default => 0
  end
end
