class AddUidToBuildings < ActiveRecord::Migration
  def change
    add_index :buildings, :uid
  end
end
