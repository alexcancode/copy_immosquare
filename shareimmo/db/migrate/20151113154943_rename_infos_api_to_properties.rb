class RenameInfosApiToProperties < ActiveRecord::Migration
  def change
    rename_column :properties, :portal_uid, :uid
    rename_column :properties, :portal_api_provider_id, :api_provider_id
  end
end
