class AddSettingsToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :settings, :text, :after => :user_id
    Profile.find_each do |profile|
      profile.settings[:show_tuto_1] = true
      profile.settings[:show_tuto_2] = true
      profile.save
    end
  end
end
