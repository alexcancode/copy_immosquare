class AddCanManageApiIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :can_manage_api_id, :integer, :after => :api_provider_id
  end
end
