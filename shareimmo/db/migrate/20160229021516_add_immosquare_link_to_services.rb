class AddImmosquareLinkToServices < ActiveRecord::Migration
  def change
    add_column :services, :immosquare_link, :string
  end
end
