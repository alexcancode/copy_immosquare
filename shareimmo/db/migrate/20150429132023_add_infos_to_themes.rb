class AddInfosToThemes < ActiveRecord::Migration
  def change
    add_column :themes, :demo_link, :string
    add_column :themes, :description, :text
  end
end
