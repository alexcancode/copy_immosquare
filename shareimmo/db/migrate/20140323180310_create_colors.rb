class CreateColors < ActiveRecord::Migration
  def change
    create_table :colors do |t|
      t.string :code_rgba
      t.string :code_hex
      t.string :name
      t.timestamps
    end
  end
end
