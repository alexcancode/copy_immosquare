class CleanVisiPortals < ActiveRecord::Migration
  def change
    change_column :visibility_portals, :portal_id, :integer, :after => :property_id
    change_column :visibility_portals, :posting_status, :integer, :after => :portal_id
  end
end
