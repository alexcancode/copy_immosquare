class AddMiniSiteToStatistics < ActiveRecord::Migration
  def change
    add_column :statistics, :minisite, :text, :after=>:id
    add_column :statistics, :facebook_count, :integer, :after=>:facebook, :default =>0
    add_column :statistics, :twitter_count, :integer, :after=>:twitter, :default =>0
    User.find_each do |user|
      s = Statistic.where(:user_id => user.id).first
      s.minisite[Time.now-8.day] = 0
      s.minisite[Time.now-3.day] = 0
      a = Authentification.joins(:api_provider).where(:user_id=>user.id,:api_providers => { :name =>'shareimmo'}).first
      if a.blank?
        s.minisite[Time.now] = 0
      else
        s.minisite[Time.now] = Property.where(:user_id=>user.id,:status=>1).where.not(:draft=>1).count
      end
      s.kijiji[Time.now-8.day] = 0
      s.kijiji[Time.now-3.day] = 0
      s.solihome[Time.now-8.day] = 0
      s.solihome[Time.now-3.day] = 0
      s.facebook[Time.now-8.day] = 0
      s.facebook[Time.now-3.day] = 0
      s.twitter[Time.now-8.day] = 0
      s.twitter[Time.now-3.day] = 0
      s.save
    end
  end


end
