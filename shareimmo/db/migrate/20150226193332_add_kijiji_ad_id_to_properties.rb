class AddKijijiAdIdToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :kijiji_ad_id, :integer, :after => :kijiji_extra_status
    remove_column :properties, :kijiji_infos
    add_column :properties, :lespac_ad_id, :integer, :after => :lespac_posting_status
  end
end
