class RenameNameToWebsite < ActiveRecord::Migration
  def change
    rename_column :websites, :name, :subdomain
  end
end
