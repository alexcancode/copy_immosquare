class AddInfos2ToPropertyInclusions < ActiveRecord::Migration
  def change
    add_column :property_inclusions, :orientation_north, :integer
    add_column :property_inclusions, :orientation_south, :integer
    add_column :property_inclusions, :orientation_west, :integer
    add_column :property_inclusions, :orientation_east, :integer
    add_column :property_complements,:france_mandat_exclusif, :integer
    add_column :properties, :wc, :integer
    add_money :properties, :deposit, currency: { present: false }, amount: { null: true, default: nil }
  end
end
