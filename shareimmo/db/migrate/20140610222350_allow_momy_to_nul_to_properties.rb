class AllowMomyToNulToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :rent_cents, :integer
    remove_column :properties, :tax_1_cents, :integer
    remove_column :properties, :tax_2_cents, :integer
    remove_column :properties, :condo_fees_cents, :integer
    add_money :properties, :rent, currency: { present: false }, amount: { null: true, default: nil }
    add_money :properties, :tax_1, currency: { present: false }, amount: { null: true, default: nil }
    add_money :properties, :tax_2, currency: { present: false }, amount: { null: true, default: nil }
    add_money :properties, :condo_fees, currency: { present: false }, amount: { null: true, default: nil }
  end
end
