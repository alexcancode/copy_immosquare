class AddValidityStartToVisibilities < ActiveRecord::Migration
  def change
    add_column :visibilities, :validity_start, :date, :after => :property_id
  end
end
