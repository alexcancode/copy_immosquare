class AddBusinessInfosToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :business_name, :string
    add_column :websites, :business_phone, :string
    add_column :websites, :business_description, :string
  end
end
