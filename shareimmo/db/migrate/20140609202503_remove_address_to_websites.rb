class RemoveAddressToWebsites < ActiveRecord::Migration
  def change
    remove_column :websites, :address, :string
  end
end
