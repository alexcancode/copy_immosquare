class ChangeNoteToDocuments < ActiveRecord::Migration
  def change
    remove_column :documents, :note
    add_column :documents, :note, :text, :after => :title
  end
end
