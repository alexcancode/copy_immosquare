class AddBackgroundImageIdToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :background_image_id, :integer, :default =>1
  end
end
