class AddAttachmentContactAvatarToWebsites < ActiveRecord::Migration
  def self.up
    change_table :websites do |t|
      t.attachment :contact_avatar
    end
  end

  def self.down
    drop_attached_file :websites, :contact_avatar
  end
end
