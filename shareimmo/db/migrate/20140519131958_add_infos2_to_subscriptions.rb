class AddInfos2ToSubscriptions < ActiveRecord::Migration
  def change
    remove_column :subscriptions, :last_4_digits, :integer
    remove_column :subscriptions, :stripe_token, :string
    remove_column :subscriptions, :card_first_name, :string
    remove_column :subscriptions, :card_last_name, :string
    rename_column :subscriptions, :customer_id, :stripe_customer_id
    rename_column :subscriptions, :email, :stripe_email

    add_column :subscriptions, :stripe_card_last4, :string
    add_column :subscriptions, :stripe_card_type, :string
    add_column :subscriptions, :stripe_card_name, :string
    add_column :subscriptions, :stripe_card_exp_month, :integer
    add_column :subscriptions, :stripe_card_exp_year, :integer
    add_column :subscriptions, :stripe_card_country, :string
    add_column :subscriptions, :stripe_plan_id, :string
    add_column :subscriptions, :stripe_plan_interval, :string
    add_column :subscriptions, :stripe_plan_name, :string
    add_column :subscriptions, :stripe_plan_amount, :integer
    add_column :subscriptions, :stripe_plan_currency, :string
    add_column :subscriptions, :stripe_status, :string
    add_column :subscriptions, :stripe_current_period_start, :datetime
    add_column :subscriptions, :stripe_current_period_end, :datetime
  end
end
