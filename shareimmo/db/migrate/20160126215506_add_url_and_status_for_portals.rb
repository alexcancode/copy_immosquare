class AddUrlAndStatusForPortals < ActiveRecord::Migration
  def change
    add_column :portals, :url, 		:string, 	:default => "", :after => :name
    add_column :portals, :status, 	:integer, 	:default => 1, 	:after => :url
  end
end
