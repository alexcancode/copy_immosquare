class AddUserIdToNomenclatures < ActiveRecord::Migration
  def change
    add_reference :nomenclatures, :user, index: true, foreign_key: false, :after=> :id
  end
end
