class AddTitleToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :title, :string
    add_column :properties, :description, :text
  end
end
