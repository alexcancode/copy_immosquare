class AllowPriceToNulToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :price_cents, :integer
    add_money :properties, :price, currency: { present: false }, amount: { null: true, default: nil }
  end
end
