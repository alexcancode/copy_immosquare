class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.references :profile, index: true
      t.integer :price
      t.string :address

      t.timestamps
    end
  end
end
