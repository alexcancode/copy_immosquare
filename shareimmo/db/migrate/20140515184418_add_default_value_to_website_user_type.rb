class AddDefaultValueToWebsiteUserType < ActiveRecord::Migration
  def change
    change_column :websites, :website_user_type_id, :integer, :default => 1
  end
end
