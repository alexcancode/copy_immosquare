class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :user, index: true, foreign_key: false
      t.string :total
      t.integer :is_paid
      t.string :currency
      t.string :stripe_id
      t.timestamps null: false
    end
  end
end
