class AddSlugToPropertyContactTypes < ActiveRecord::Migration
  def change
    add_column :property_contact_types, :slug, :string, :after => :id
  end
end
