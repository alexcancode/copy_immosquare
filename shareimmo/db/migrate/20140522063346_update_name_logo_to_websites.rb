class UpdateNameLogoToWebsites < ActiveRecord::Migration
  def change
    remove_column :websites, :logo_file_name
    remove_column :websites, :logo_content_type
    remove_column :websites, :logo_file_size
    remove_column :websites, :logo_updated_at
  end
end
