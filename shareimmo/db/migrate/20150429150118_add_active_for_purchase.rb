class AddActiveForPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :status, :integer, :default => 1
  end
end
