class AddNewsColorsToCustomThemes < ActiveRecord::Migration
  def change
    add_column :custom_themes, :filter_background_color, :string, :default=> '#FFFFFF'
    add_column :custom_themes, :filter_background_opacity, :string, :default=> '0.5'
  end
end
