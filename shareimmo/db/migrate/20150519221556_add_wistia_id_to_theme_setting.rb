class AddWistiaIdToThemeSetting < ActiveRecord::Migration
  def change
    add_column :theme_settings, :wistia_id, :string, :after => :is_selected
  end
end
