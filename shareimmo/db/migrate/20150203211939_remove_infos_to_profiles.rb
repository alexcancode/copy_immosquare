class RemoveInfosToProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :delete_old_adverts, :integer
    remove_column :profiles, :auto_update_adverts, :integer
  end
end
