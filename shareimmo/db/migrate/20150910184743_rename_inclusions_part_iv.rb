class RenameInclusionsPartIv < ActiveRecord::Migration
  def change
    rename_column :property_inclusions, :detail_outsite_parking, :detail_outside_parking
  end
end
