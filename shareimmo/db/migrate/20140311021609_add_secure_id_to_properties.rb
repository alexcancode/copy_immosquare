class AddSecureIdToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :secure_id, :string
  end
end
