class AddStatusForPropertySource < ActiveRecord::Migration
  def change
    add_column :property_sources, :status, :integer, :default => 1, :after => :id
  end
end
