class RemovePetsToProperties < ActiveRecord::Migration
  def change
    remove_column :properties, :pets_allowed, :integer
    remove_column :properties, :parking, :integer
  end
end
