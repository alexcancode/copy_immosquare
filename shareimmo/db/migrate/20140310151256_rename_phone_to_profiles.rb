class RenamePhoneToProfiles < ActiveRecord::Migration
  def change
    rename_column :profiles, :tel, :phone
  end
end
