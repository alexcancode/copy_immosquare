class RenameCraiglitToProperties < ActiveRecord::Migration
  def change
    rename_column :properties, :craiglist_posting_status, :craigslist_posting_status
  end
end
