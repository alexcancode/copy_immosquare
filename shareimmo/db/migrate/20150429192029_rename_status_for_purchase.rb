class RenameStatusForPurchase < ActiveRecord::Migration
  def change
    rename_column :purchases, :status, :is_processing
  end
end
