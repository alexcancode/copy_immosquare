class RemovePriceFromToProperites < ActiveRecord::Migration
  def change
    remove_column :properties, :price_from_cents
    remove_column :properties, :price_from_currency
    add_column :properties, :price_from, :integer, :after => :price_currency, :default=> 0
  end
end
