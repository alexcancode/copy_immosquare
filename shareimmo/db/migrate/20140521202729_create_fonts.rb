class CreateFonts < ActiveRecord::Migration
  def change
    create_table :fonts do |t|
      t.string :name
      t.string :type
      t.string :link_url

      t.timestamps
    end
  end
end
