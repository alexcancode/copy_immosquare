class AddProfileCompletedToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :profile_completed, :boolean, :default =>false
  end
end
