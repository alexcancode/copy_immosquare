class AddInfosToProperty < ActiveRecord::Migration
  def change
    add_reference :properties, :property_flag, index: true
    add_column :properties, :deleted, :boolean, :default => 0
  end
end
