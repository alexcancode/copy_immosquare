class AddAttachmentWebsiteCoverFullSizeToThemeSettings < ActiveRecord::Migration
  def self.up
    change_table :theme_settings do |t|
      t.attachment :website_cover_full_size
    end
  end

  def self.down
    remove_attachment :theme_settings, :website_cover_full_size
  end
end
