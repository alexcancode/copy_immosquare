class ChangeDraftToProperties < ActiveRecord::Migration
  def change
    change_column :properties, :draft, :integer, :default =>1
  end
end
