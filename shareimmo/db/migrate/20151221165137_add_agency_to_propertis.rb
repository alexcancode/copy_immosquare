class AddAgencyToPropertis < ActiveRecord::Migration
  def change
    add_reference :users, :agency, index: true, foreign_key: false, :after => :id
    add_reference :properties, :agency, index: true, foreign_key: false, :after => :user_id
  end
end
