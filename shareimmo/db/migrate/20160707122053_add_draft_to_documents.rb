class AddDraftToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :draft, :integer, :default => 1
  end
end
