class AddStatusToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :status, :integer, :after => :id, :default => 0
    remove_column :videos, :uid
  end
end
