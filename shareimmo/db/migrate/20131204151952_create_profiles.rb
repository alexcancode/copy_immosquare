class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :website
      t.string :username
      t.string :slug

      t.timestamps
    end
  end
end
