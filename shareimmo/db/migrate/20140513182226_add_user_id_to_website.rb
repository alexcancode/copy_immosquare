class AddUserIdToWebsite < ActiveRecord::Migration
  def change
    add_reference :websites, :user, index: true
    remove_column :websites, :profile_id
  end
end
