class CreatePrintTemplateZoneTranslations < ActiveRecord::Migration
  def self.up
    PrintTemplateZone.create_translation_table!({
      :name => :string,
    }, {
      :migrate_data => false
    })
  end

  def self.down
    PrintTemplateZone.drop_translation_table! :migrate_data => true
  end
end
