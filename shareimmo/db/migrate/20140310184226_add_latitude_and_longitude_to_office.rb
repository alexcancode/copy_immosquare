class AddLatitudeAndLongitudeToOffice < ActiveRecord::Migration
  def change
    ## for geocoding
    add_column :offices, :latitude, :float
    add_column :offices, :longitude, :float
    ## for reverse geocoding
    add_column :offices, :street_number, :string
    add_column :offices, :street_name, :string
    add_column :offices, :neighborhood, :string
    add_column :offices, :sublocality, :string
    add_column :offices, :locality, :string
    add_column :offices, :administrative_area_level_2_long, :string
    add_column :offices, :administrative_area_level_2_short, :string
    add_column :offices, :administrative_area_level_1_long, :string
    add_column :offices, :administrative_area_level_1_short, :string
    add_column :offices, :country_long, :string
    add_column :offices, :country_short, :string
    add_column :offices, :zipcode, :string
    ## clean offices
    remove_column :offices, :city, :string
    remove_column :offices, :province, :string
    remove_column :offices, :country, :string
    rename_column :offices, :tel, :phone
  end
end
