class AddAttachmentCoverToThemeSettings < ActiveRecord::Migration
  def self.up
    change_table :theme_settings do |t|
      t.attachment :website_cover
    end
  end

  def self.down
    remove_attachment :theme_settings, :website_cover
  end
end
