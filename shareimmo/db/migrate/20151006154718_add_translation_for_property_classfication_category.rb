class AddTranslationForPropertyClassficationCategory < ActiveRecord::Migration
  def change
    PropertyClassificationCategory.create_translation_table!({
      :name => :string,
    }, {
      :migrate_data => false
    })
  end
end


