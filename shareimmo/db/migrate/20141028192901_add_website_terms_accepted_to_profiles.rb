class AddWebsiteTermsAcceptedToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :website_terms_accepted, :integer,:default =>0
  end
end
