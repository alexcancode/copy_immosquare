# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161230144352) do

  create_table "agencies", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "api_apps", force: :cascade do |t|
    t.string   "key",                     limit: 255
    t.string   "token",                   limit: 255
    t.integer  "api_provider_id",         limit: 4
    t.string   "search_authorized_users", limit: 255
    t.string   "note_interne",            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "api_apps", ["api_provider_id"], name: "index_api_apps_on_api_provider_id", using: :btree

  create_table "api_providers", force: :cascade do |t|
    t.string   "name",                  limit: 255
    t.string   "display_name",          limit: 255
    t.integer  "is_storeimmo_provider", limit: 4,   default: 0
    t.string   "store_url",             limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "area_unit_translations", force: :cascade do |t|
    t.integer  "area_unit_id", limit: 4,   null: false
    t.string   "locale",       limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",         limit: 255
    t.string   "ref",          limit: 255
    t.string   "symbol",       limit: 255
  end

  add_index "area_unit_translations", ["area_unit_id"], name: "index_area_unit_translations_on_area_unit_id", using: :btree
  add_index "area_unit_translations", ["locale"], name: "index_area_unit_translations_on_locale", using: :btree

  create_table "area_units", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "ref",             limit: 255
    t.string   "symbol",          limit: 255
    t.integer  "conversion_rate", limit: 4,   default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authentifications", force: :cascade do |t|
    t.integer  "user_id",                     limit: 4
    t.integer  "is_account_for_video_worker", limit: 4,   default: 0
    t.string   "uid",                         limit: 255
    t.integer  "api_provider_id",             limit: 4
    t.string   "token",                       limit: 255
    t.string   "refresh_token",               limit: 255
    t.integer  "expires_at",                  limit: 4
    t.string   "token_secret",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "expires"
    t.string   "email",                       limit: 255
    t.string   "first_name",                  limit: 255
    t.string   "last_name",                   limit: 255
    t.string   "nickname",                    limit: 255
    t.string   "image",                       limit: 255
    t.string   "location",                    limit: 255
    t.string   "link",                        limit: 255
    t.string   "name",                        limit: 255
    t.string   "custom_field_1",              limit: 255
    t.string   "custom_field_2",              limit: 255
    t.string   "custom_field_3",              limit: 255
    t.integer  "review_status",               limit: 4,   default: 2
  end

  add_index "authentifications", ["api_provider_id"], name: "index_authentifications_on_api_provider_id", using: :btree
  add_index "authentifications", ["user_id"], name: "index_authentifications_on_user_id", using: :btree

  create_table "blog_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "slug",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "blog_category_translations", force: :cascade do |t|
    t.integer  "blog_category_id", limit: 8,   null: false
    t.string   "locale",           limit: 255, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "name",             limit: 255
    t.string   "slug",             limit: 255
  end

  add_index "blog_category_translations", ["blog_category_id"], name: "index_blog_category_translations_on_blog_category_id", using: :btree
  add_index "blog_category_translations", ["locale"], name: "index_blog_category_translations_on_locale", using: :btree

  create_table "blogs", force: :cascade do |t|
    t.integer  "user_id",            limit: 8
    t.integer  "blog_category_id",   limit: 8
    t.integer  "draft",              limit: 4,     default: 1
    t.string   "locale",             limit: 255
    t.string   "title",              limit: 255
    t.string   "slug",               limit: 255
    t.text     "content",            limit: 65535
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "cover_file_name",    limit: 255
    t.string   "cover_content_type", limit: 255
    t.integer  "cover_file_size",    limit: 4
    t.datetime "cover_updated_at"
  end

  add_index "blogs", ["blog_category_id"], name: "index_blogs_on_blog_category_id", using: :btree
  add_index "blogs", ["user_id"], name: "index_blogs_on_user_id", using: :btree

  create_table "broker_contacts", force: :cascade do |t|
    t.integer  "property_id", limit: 4
    t.integer  "user_id",     limit: 4
    t.integer  "by_sms",      limit: 4,     default: 0
    t.string   "ref",         limit: 255
    t.string   "full_name",   limit: 255
    t.string   "email",       limit: 255
    t.string   "phone",       limit: 255
    t.text     "message",     limit: 65535
    t.integer  "newsletter",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "broker_contacts", ["property_id"], name: "index_broker_contacts_on_property_id", using: :btree
  add_index "broker_contacts", ["user_id"], name: "index_broker_contacts_on_user_id", using: :btree

  create_table "building_assets", force: :cascade do |t|
    t.integer  "position",             limit: 4
    t.integer  "building_id",          limit: 8
    t.string   "name",                 limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "picture_file_name",    limit: 255
    t.string   "picture_content_type", limit: 255
    t.integer  "picture_file_size",    limit: 4
    t.datetime "picture_updated_at"
    t.string   "original_url",         limit: 255
  end

  add_index "building_assets", ["building_id"], name: "index_building_assets_on_building_id", using: :btree

  create_table "building_translations", force: :cascade do |t|
    t.integer  "building_id",       limit: 8,     null: false
    t.string   "locale",            limit: 255,   null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.text     "description",       limit: 65535
    t.text     "description_short", limit: 65535
  end

  add_index "building_translations", ["building_id"], name: "index_building_translations_on_building_id", using: :btree
  add_index "building_translations", ["locale"], name: "index_building_translations_on_locale", using: :btree

  create_table "buildings", force: :cascade do |t|
    t.string   "uid",                         limit: 255
    t.integer  "api_provider_id",             limit: 4
    t.integer  "is_sync_with_api",            limit: 4
    t.integer  "user_id",                     limit: 4
    t.integer  "draft",                       limit: 4,     default: 1
    t.string   "secure_id",                   limit: 255
    t.string   "name",                        limit: 255
    t.integer  "number_of_units",             limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "number_of_levels",            limit: 4
    t.string   "address",                     limit: 255
    t.string   "street_number",               limit: 255
    t.string   "street_name",                 limit: 255
    t.string   "zipcode",                     limit: 255
    t.string   "locality",                    limit: 255
    t.string   "administrative_area_level_1", limit: 255
    t.string   "administrative_area_level_2", limit: 255
    t.string   "country_short",               limit: 255,   default: "CA"
    t.string   "sublocality",                 limit: 255
    t.float    "latitude",                    limit: 24
    t.float    "longitude",                   limit: 24
    t.text     "description",                 limit: 65535
    t.text     "description_short",           limit: 65535
  end

  add_index "buildings", ["api_provider_id"], name: "index_buildings_on_api_provider_id", using: :btree
  add_index "buildings", ["uid"], name: "index_buildings_on_uid", using: :btree
  add_index "buildings", ["user_id"], name: "index_buildings_on_user_id", using: :btree

  create_table "civilities", force: :cascade do |t|
    t.string   "value",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "civility_translations", force: :cascade do |t|
    t.integer  "civility_id", limit: 8,   null: false
    t.string   "locale",      limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "value",       limit: 255
  end

  add_index "civility_translations", ["civility_id"], name: "index_civility_translations_on_civility_id", using: :btree
  add_index "civility_translations", ["locale"], name: "index_civility_translations_on_locale", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "email",               limit: 255
    t.string   "first_name",          limit: 255
    t.string   "last_name",           limit: 255
    t.integer  "user_id",             limit: 4
    t.text     "message",             limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone",               limit: 255
    t.integer  "service_id",          limit: 4
    t.integer  "activation_required", limit: 4
  end

  add_index "contacts", ["service_id"], name: "index_contacts_on_service_id", using: :btree

  create_table "credit_cards", force: :cascade do |t|
    t.integer  "user_id",      limit: 8
    t.string   "stripe_id",    limit: 255
    t.string   "object",       limit: 255
    t.string   "last4",        limit: 255
    t.string   "brand",        limit: 255
    t.string   "funding",      limit: 255
    t.integer  "exp_month",    limit: 4
    t.integer  "exp_year",     limit: 4
    t.string   "country",      limit: 255
    t.string   "name",         limit: 255
    t.text     "stripe_infos", limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "credit_cards", ["user_id"], name: "index_credit_cards_on_user_id", using: :btree

  create_table "custom_services", force: :cascade do |t|
    t.integer  "user_id",            limit: 8
    t.string   "name",               limit: 255
    t.text     "description",        limit: 65535
    t.string   "link",               limit: 255
    t.string   "video_url",          limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "cover_file_name",    limit: 255
    t.string   "cover_content_type", limit: 255
    t.integer  "cover_file_size",    limit: 4
    t.datetime "cover_updated_at"
  end

  add_index "custom_services", ["user_id"], name: "index_custom_services_on_user_id", using: :btree

  create_table "documents", force: :cascade do |t|
    t.integer  "position",              limit: 4
    t.integer  "property_id",           limit: 8
    t.integer  "visible_on_front",      limit: 4,     default: 1
    t.string   "title",                 limit: 255
    t.text     "note",                  limit: 65535
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "document_file_name",    limit: 255
    t.string   "document_content_type", limit: 255
    t.integer  "document_file_size",    limit: 4
    t.datetime "document_updated_at"
    t.integer  "draft",                 limit: 4,     default: 1
  end

  add_index "documents", ["property_id"], name: "index_documents_on_property_id", using: :btree

  create_table "feedback_reports", force: :cascade do |t|
    t.integer  "feedback_id", limit: 8
    t.integer  "is_sent",     limit: 4
    t.text     "contact_1",   limit: 65535
    t.text     "contact_2",   limit: 65535
    t.text     "contact_3",   limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "feedback_reports", ["feedback_id"], name: "index_feedback_reports_on_feedback_id", using: :btree

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "draft",            limit: 4,     default: 0
    t.integer  "property_id",      limit: 8
    t.datetime "visit_at"
    t.float    "advice",           limit: 24
    t.string   "full_name",        limit: 255
    t.string   "email",            limit: 255
    t.string   "phone",            limit: 255
    t.integer  "reminder_phone",   limit: 4,     default: 0
    t.integer  "reminder_email",   limit: 4,     default: 0
    t.text     "note_internal",    limit: 65535
    t.text     "note_for_owner",   limit: 65535
    t.string   "help_sms",         limit: 255
    t.text     "signature",        limit: 65535
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.text     "note_for_visitor", limit: 65535
  end

  add_index "feedbacks", ["property_id"], name: "index_feedbacks_on_property_id", using: :btree

  create_table "font_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fonts", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "link_url",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "font_type_id", limit: 4
    t.string   "css_invok",    limit: 255
  end

  add_index "fonts", ["font_type_id"], name: "index_fonts_on_font_type_id", using: :btree

  create_table "glossaries", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "title",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "glossaries", ["user_id"], name: "index_glossaries_on_user_id", using: :btree

  create_table "glossary_elements", force: :cascade do |t|
    t.string   "content",     limit: 255
    t.integer  "glossary_id", limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hostings", force: :cascade do |t|
    t.integer  "user_id",       limit: 4
    t.integer  "property_id",   limit: 4
    t.string   "domain",        limit: 255
    t.integer  "paying_domain", limit: 4,   default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hostings", ["property_id"], name: "index_hostings_on_property_id", using: :btree
  add_index "hostings", ["user_id"], name: "index_hostings_on_user_id", using: :btree

  create_table "inventories", force: :cascade do |t|
    t.integer  "user_id",                 limit: 4
    t.string   "secure_id",               limit: 255
    t.string   "secure_password",         limit: 255
    t.string   "secure_token",            limit: 255
    t.datetime "secure_token_expiration"
    t.string   "designation",             limit: 255
    t.integer  "status",                  limit: 4,   default: 1
    t.integer  "file_type",               limit: 4,   default: 1
    t.string   "xml_file_name",           limit: 255
    t.string   "xml_content_type",        limit: 255
    t.integer  "xml_file_size",           limit: 4
    t.datetime "xml_updated_at"
    t.integer  "property_id",             limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "inventories", ["user_id"], name: "index_inventories_on_user_id", using: :btree

  create_table "inventory_parameters", force: :cascade do |t|
    t.integer  "user_id",                  limit: 8
    t.string   "state1_name",              limit: 255
    t.string   "state1_label",             limit: 255
    t.string   "state2_name",              limit: 255
    t.string   "state2_label",             limit: 255
    t.string   "state3_name",              limit: 255
    t.string   "state3_label",             limit: 255
    t.string   "state4_name",              limit: 255
    t.string   "state4_label",             limit: 255
    t.string   "state5_name",              limit: 255
    t.string   "state5_label",             limit: 255
    t.string   "pdf_introduction_title",   limit: 255
    t.text     "pdf_introduction_content", limit: 65535
    t.string   "pdf_free_section_title",   limit: 255
    t.text     "pdf_free_section_content", limit: 65535
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "nomenclature_id",          limit: 4
  end

  add_index "inventory_parameters", ["nomenclature_id"], name: "index_inventory_parameters_on_nomenclature_id", using: :btree
  add_index "inventory_parameters", ["user_id"], name: "index_inventory_parameters_on_user_id", using: :btree

  create_table "invoice_lines", force: :cascade do |t|
    t.integer  "invoice_id",         limit: 8
    t.integer  "price",              limit: 4
    t.integer  "price_without_taxs", limit: 4
    t.string   "services",           limit: 255
    t.integer  "quantity",           limit: 4
    t.text     "description",        limit: 65535
    t.text     "tax1",               limit: 65535
    t.text     "tax2",               limit: 65535
    t.string   "stripe_id",          limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "invoice_lines", ["invoice_id"], name: "index_invoice_lines_on_invoice_id", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.integer  "user_id",    limit: 8
    t.integer  "total",      limit: 4
    t.integer  "is_paid",    limit: 4
    t.string   "currency",   limit: 255
    t.string   "stripe_id",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "invoices", ["user_id"], name: "index_invoices_on_user_id", using: :btree

  create_table "nomenclature_defaults", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.string   "country",     limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "nomenclature_element_defaults", force: :cascade do |t|
    t.string   "name",                    limit: 255
    t.string   "value",                   limit: 255
    t.integer  "element_type",            limit: 4
    t.integer  "nomenclature_default_id", limit: 8
    t.integer  "status",                  limit: 4,   default: 1
    t.string   "ancestry",                limit: 255
    t.integer  "position",                limit: 4
    t.integer  "import_id",               limit: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "is_the_standard_element", limit: 4,   default: 0
  end

  add_index "nomenclature_element_defaults", ["nomenclature_default_id"], name: "index_nomenclature_element_defaults_on_nomenclature_default_id", using: :btree

  create_table "nomenclature_elements", force: :cascade do |t|
    t.string  "name",                    limit: 255
    t.string  "value",                   limit: 255
    t.integer "element_type",            limit: 4
    t.integer "nomenclature_id",         limit: 8
    t.integer "status",                  limit: 4,   default: 1
    t.string  "ancestry",                limit: 255
    t.integer "position",                limit: 4
    t.integer "import_id",               limit: 4
    t.integer "is_the_standard_element", limit: 4,   default: 0
  end

  add_index "nomenclature_elements", ["ancestry"], name: "index_nomenclature_elements_on_ancestry", using: :btree

  create_table "nomenclatures", force: :cascade do |t|
    t.integer "user_id",     limit: 4
    t.string  "name",        limit: 255
    t.string  "description", limit: 255
    t.integer "status",      limit: 4,   default: 2
  end

  add_index "nomenclatures", ["user_id"], name: "index_nomenclatures_on_user_id", using: :btree

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer  "resource_owner_id", limit: 4,     null: false
    t.integer  "application_id",    limit: 4,     null: false
    t.string   "token",             limit: 255,   null: false
    t.integer  "expires_in",        limit: 4,     null: false
    t.text     "redirect_uri",      limit: 65535, null: false
    t.datetime "created_at",                      null: false
    t.datetime "revoked_at"
    t.string   "scopes",            limit: 255
  end

  add_index "oauth_access_grants", ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer  "resource_owner_id", limit: 4
    t.integer  "application_id",    limit: 4
    t.string   "token",             limit: 255, null: false
    t.string   "refresh_token",     limit: 255
    t.integer  "expires_in",        limit: 4
    t.datetime "revoked_at"
    t.datetime "created_at",                    null: false
    t.string   "scopes",            limit: 255
  end

  add_index "oauth_access_tokens", ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
  add_index "oauth_access_tokens", ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
  add_index "oauth_access_tokens", ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree

  create_table "oauth_applications", force: :cascade do |t|
    t.string   "name",         limit: 255,                null: false
    t.string   "uid",          limit: 255,                null: false
    t.string   "secret",       limit: 255,                null: false
    t.text     "redirect_uri", limit: 65535,              null: false
    t.string   "scopes",       limit: 255,   default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "oauth_applications", ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree

  create_table "pdfs", force: :cascade do |t|
    t.string   "name",                  limit: 255
    t.text     "dictionary",            limit: 65535
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "document_file_name",    limit: 255
    t.string   "document_content_type", limit: 255
    t.integer  "document_file_size",    limit: 4
    t.datetime "document_updated_at"
  end

  create_table "portal_activations", force: :cascade do |t|
    t.integer  "user_id",    limit: 8
    t.integer  "portal_id",  limit: 8
    t.integer  "status",     limit: 4
    t.string   "login",      limit: 255
    t.string   "password",   limit: 255
    t.string   "ftp",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "limitation", limit: 4
  end

  add_index "portal_activations", ["portal_id"], name: "index_portal_activations_on_portal_id", using: :btree
  add_index "portal_activations", ["user_id"], name: "index_portal_activations_on_user_id", using: :btree

  create_table "portal_type_translations", force: :cascade do |t|
    t.integer  "portal_type_id", limit: 8,   null: false
    t.string   "locale",         limit: 255, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "name",           limit: 255
  end

  add_index "portal_type_translations", ["locale"], name: "index_portal_type_translations_on_locale", using: :btree
  add_index "portal_type_translations", ["portal_type_id"], name: "index_portal_type_translations_on_portal_type_id", using: :btree

  create_table "portal_types", force: :cascade do |t|
    t.integer  "position",   limit: 4
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "slug",       limit: 255
  end

  create_table "portals", force: :cascade do |t|
    t.integer  "portal_type_id",      limit: 4,   default: 1
    t.string   "name",                limit: 255
    t.string   "slug",                limit: 255, default: ""
    t.string   "url",                 limit: 255, default: ""
    t.string   "access_key",          limit: 255, default: ""
    t.integer  "status",              limit: 4,   default: 1
    t.string   "portal_ad_type",      limit: 255, default: "sell,rent"
    t.integer  "free",                limit: 4,   default: 0
    t.string   "country",             limit: 255
    t.integer  "with_user_connexion", limit: 4,   default: 0
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
  end

  add_index "portals", ["portal_type_id"], name: "index_portals_on_portal_type_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.integer  "draft",              limit: 4,     default: 0
    t.boolean  "status"
    t.integer  "web_app",            limit: 4,     default: 1
    t.string   "locale",             limit: 255
    t.string   "title",              limit: 255
    t.string   "slug",               limit: 255
    t.text     "summary",            limit: 65535
    t.text     "content",            limit: 65535
    t.string   "cover_file_name",    limit: 255
    t.string   "cover_content_type", limit: 255
    t.integer  "cover_file_size",    limit: 4
    t.datetime "cover_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "print_orders", force: :cascade do |t|
    t.integer  "property_id",       limit: 8
    t.integer  "print_template_id", limit: 8
    t.integer  "preview_count",     limit: 4,     default: 0
    t.text     "json_template",     limit: 65535
    t.string   "channel",           limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "print_orders", ["print_template_id"], name: "index_print_orders_on_print_template_id", using: :btree
  add_index "print_orders", ["property_id"], name: "index_print_orders_on_property_id", using: :btree

  create_table "profile_translations", force: :cascade do |t|
    t.integer  "profile_id", limit: 8,     null: false
    t.string   "locale",     limit: 255,   null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "job",        limit: 255
    t.string   "baseline",   limit: 255
    t.text     "bio",        limit: 65535
  end

  add_index "profile_translations", ["locale"], name: "index_profile_translations_on_locale", using: :btree
  add_index "profile_translations", ["profile_id"], name: "index_profile_translations_on_profile_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id",                 limit: 4
    t.text     "settings",                limit: 65535
    t.string   "public_name",             limit: 255
    t.string   "last_name",               limit: 255
    t.string   "first_name",              limit: 255
    t.string   "phone",                   limit: 255
    t.string   "phone_sms",               limit: 255
    t.integer  "phone_visibility",        limit: 4,     default: 1
    t.string   "job",                     limit: 255
    t.text     "bio",                     limit: 65535
    t.string   "baseline",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_original_url",    limit: 255
    t.string   "spoken_languages",        limit: 255,   default: "fr,en"
    t.string   "agency_name",             limit: 255
    t.string   "agency_address",          limit: 255
    t.string   "agency_city",             limit: 255
    t.string   "agency_zipcode",          limit: 255
    t.string   "website",                 limit: 255
    t.string   "picture_file_name",       limit: 255
    t.string   "picture_content_type",    limit: 255
    t.integer  "picture_file_size",       limit: 4
    t.datetime "picture_updated_at"
    t.string   "email",                   limit: 255
    t.string   "agency_province",         limit: 255
    t.string   "logo_original_url",       limit: 255
    t.string   "logo_file_name",          limit: 255
    t.string   "logo_content_type",       limit: 255
    t.integer  "logo_file_size",          limit: 4
    t.datetime "logo_updated_at"
    t.integer  "is_demo_user",            limit: 4,     default: 0
    t.string   "country",                 limit: 255,   default: "CA"
    t.string   "country_alpha2",          limit: 255,   default: "CA"
    t.string   "country_alpha3",          limit: 255,   default: "CAN"
    t.string   "country_code",            limit: 255,   default: "1"
    t.string   "international_prefix",    limit: 255,   default: "011"
    t.string   "country_name",            limit: 255,   default: "Canada"
    t.integer  "is_a_pro_user",           limit: 4,     default: 0
    t.string   "facebook",                limit: 255
    t.string   "twitter",                 limit: 255
    t.string   "linkedin",                limit: 255
    t.string   "pinterest",               limit: 255
    t.string   "google_plus",             limit: 255
    t.string   "instagram",               limit: 255
    t.string   "primary_color",           limit: 255,   default: "#f54029"
    t.string   "secondary_color",         limit: 255,   default: "#f54029"
    t.integer  "font_id",                 limit: 4,     default: 1
    t.integer  "font_special_id",         limit: 4,     default: 1
    t.text     "theme",                   limit: 65535
    t.integer  "blog_active",             limit: 4,     default: 0
    t.integer  "page_testimonial_active", limit: 4,     default: 0
    t.integer  "page_team_active",        limit: 4,     default: 0
    t.string   "custom_link_1",           limit: 255
    t.string   "custom_link_1_url",       limit: 255
    t.string   "google_analytics",        limit: 255
    t.integer  "mandate_website",         limit: 4,     default: 0
    t.string   "default_locale",          limit: 255,   default: "fr"
  end

  add_index "profiles", ["font_id"], name: "index_profiles_on_font_id", using: :btree
  add_index "profiles", ["font_special_id"], name: "index_profiles_on_font_special_id", using: :btree

  create_table "properties", force: :cascade do |t|
    t.integer  "image_process",               limit: 4,     default: 1
    t.string   "uid",                         limit: 255
    t.integer  "master",                      limit: 4,     default: 1
    t.integer  "uid2",                        limit: 4
    t.integer  "api_provider_id",             limit: 4
    t.integer  "user_id",                     limit: 4
    t.integer  "agency_id",                   limit: 4
    t.integer  "count_broker",                limit: 4,     default: 0
    t.integer  "count_explore",               limit: 4,     default: 0
    t.string   "secure_id",                   limit: 255
    t.integer  "draft",                       limit: 4,     default: 1
    t.integer  "status",                      limit: 4,     default: 1
    t.integer  "is_sync_with_api",            limit: 4
    t.integer  "property_classification_id",  limit: 4,     default: 1
    t.integer  "property_status_id",          limit: 4,     default: 1
    t.integer  "property_listing_id",         limit: 4,     default: 1
    t.integer  "property_flag_id",            limit: 4
    t.integer  "building_id",                 limit: 4
    t.string   "google_place_id",             limit: 255
    t.float    "latitude",                    limit: 24
    t.float    "longitude",                   limit: 24
    t.string   "zipcode",                     limit: 255
    t.string   "street_number",               limit: 255
    t.string   "street_name",                 limit: 255
    t.string   "sublocality",                 limit: 255
    t.string   "locality",                    limit: 255
    t.string   "administrative_area_level_2", limit: 255
    t.string   "administrative_area_level_1", limit: 255
    t.string   "country_short",               limit: 255
    t.string   "address",                     limit: 255
    t.integer  "year_of_built",               limit: 4
    t.date     "availability_date"
    t.integer  "area_living",                 limit: 4
    t.integer  "area_balcony",                limit: 4
    t.integer  "area_land",                   limit: 4
    t.integer  "area_unit_id",                limit: 4,     default: 1
    t.integer  "rooms",                       limit: 4
    t.integer  "withHalf",                    limit: 4,     default: 0
    t.integer  "bathrooms",                   limit: 4
    t.integer  "bedrooms",                    limit: 4
    t.integer  "showerrooms",                 limit: 4
    t.integer  "toilets",                     limit: 4
    t.string   "title",                       limit: 255
    t.text     "description",                 limit: 65535
    t.text     "description_short",           limit: 65535
    t.string   "property_url",                limit: 255
    t.integer  "address_visibility",          limit: 4,     default: 1
    t.string   "cadastre",                    limit: 255
    t.integer  "is_luxurious",                limit: 4,     default: 0
    t.string   "unit",                        limit: 255
    t.integer  "level",                       limit: 4
    t.string   "rhinov_link",                 limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "price_with_tax",              limit: 4,     default: 0
    t.integer  "price_from",                  limit: 4,     default: 0
    t.integer  "rent_frequency",              limit: 4,     default: 30
    t.string   "currency",                    limit: 255,   default: "CAD"
    t.integer  "price_cents",                 limit: 8
    t.integer  "rent_cents",                  limit: 4
    t.integer  "tax_1_cents",                 limit: 4
    t.integer  "tax_2_cents",                 limit: 4
    t.integer  "fees_cents",                  limit: 4
    t.integer  "income_cents",                limit: 4
    t.integer  "deposit_cents",               limit: 4
  end

  add_index "properties", ["agency_id"], name: "index_properties_on_agency_id", using: :btree
  add_index "properties", ["api_provider_id"], name: "index_properties_on_api_provider_id", using: :btree
  add_index "properties", ["area_unit_id"], name: "index_properties_on_area_unit_id", using: :btree
  add_index "properties", ["building_id"], name: "index_properties_on_building_id", using: :btree
  add_index "properties", ["property_classification_id"], name: "index_properties_on_property_classification_id", using: :btree
  add_index "properties", ["property_flag_id"], name: "index_properties_on_property_flag_id", using: :btree
  add_index "properties", ["property_listing_id"], name: "index_properties_on_property_listing_id", using: :btree
  add_index "properties", ["property_status_id"], name: "index_properties_on_property_status_id", using: :btree
  add_index "properties", ["user_id"], name: "index_properties_on_user_id", using: :btree

  create_table "properties_property_contacts", id: false, force: :cascade do |t|
    t.integer "property_contact_id", limit: 8
    t.integer "property_id",         limit: 8
  end

  add_index "properties_property_contacts", ["property_contact_id", "property_id"], name: "property_contact_properties_index", unique: true, using: :btree
  add_index "properties_property_contacts", ["property_id"], name: "index_properties_property_contacts_on_property_id", using: :btree

  create_table "property_assets", force: :cascade do |t|
    t.integer  "position",             limit: 4
    t.integer  "property_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_file_name",    limit: 255
    t.string   "picture_content_type", limit: 255
    t.integer  "picture_file_size",    limit: 4
    t.datetime "picture_updated_at"
    t.integer  "room_id",              limit: 4
    t.string   "original_url",         limit: 255
  end

  add_index "property_assets", ["property_id"], name: "index_property_assets_on_property_id", using: :btree
  add_index "property_assets", ["room_id"], name: "index_property_assets_on_room_id", using: :btree

  create_table "property_classification_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "property_classification_category_translations", force: :cascade do |t|
    t.integer  "property_classification_category_id", limit: 8,   null: false
    t.string   "locale",                              limit: 255, null: false
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "name",                                limit: 255
  end

  add_index "property_classification_category_translations", ["locale"], name: "index_property_classification_category_translations_on_locale", using: :btree
  add_index "property_classification_category_translations", ["property_classification_category_id"], name: "index_9d2c839d7efcacf7bdff992b49269dfca64e9f71", using: :btree

  create_table "property_classification_translations", force: :cascade do |t|
    t.integer  "property_classification_id", limit: 4,   null: false
    t.string   "locale",                     limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                       limit: 255
    t.string   "name_kangalou",              limit: 255
  end

  add_index "property_classification_translations", ["locale"], name: "index_property_classification_translations_on_locale", using: :btree
  add_index "property_classification_translations", ["property_classification_id"], name: "index_fd9c8b5085a4691fa1d1ba1ff9acb0abc44ff800", using: :btree

  create_table "property_classifications", force: :cascade do |t|
    t.integer  "api_id",                              limit: 4
    t.integer  "property_classification_category_id", limit: 4
    t.string   "name",                                limit: 255
    t.string   "name_kangalou",                       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "list_globally_match",                 limit: 255
    t.string   "lespac_match",                        limit: 255
    t.string   "craigslist_match",                    limit: 255
    t.string   "kijiji_match",                        limit: 255, default: ""
    t.integer  "louercom_match",                      limit: 4
    t.string   "immotop_match",                       limit: 255
    t.integer  "athome_match",                        limit: 4
    t.integer  "wortimmo_match",                      limit: 4
  end

  add_index "property_classifications", ["property_classification_category_id"], name: "index_category_id", using: :btree

  create_table "property_complements", force: :cascade do |t|
    t.integer  "property_id",                  limit: 8
    t.string   "france_dpe_indice",            limit: 255
    t.string   "france_dpe_value",             limit: 255
    t.string   "france_dpe_id",                limit: 255
    t.string   "france_ges_indice",            limit: 255
    t.string   "france_ges_value",             limit: 255
    t.integer  "france_alur_is_condo",         limit: 4
    t.integer  "france_alur_units",            limit: 4
    t.integer  "france_alur_uninhabitables",   limit: 4
    t.integer  "france_alur_spending",         limit: 4
    t.integer  "france_alur_legal_action",     limit: 4
    t.string   "belgium_peb_indice",           limit: 255
    t.string   "belgium_peb_value",            limit: 255
    t.string   "belgium_peb_id",               limit: 255
    t.integer  "belgium_building_permit",      limit: 4
    t.integer  "belgium_done_assignments",     limit: 4
    t.integer  "belgium_preemption_property",  limit: 4
    t.integer  "belgium_subdivision_permits",  limit: 4
    t.integer  "belgium_sensitive_flood_area", limit: 4
    t.integer  "belgium_delimited_flood_zone", limit: 4
    t.integer  "belgium_risk_area",            limit: 4
    t.string   "canada_mls",                   limit: 255
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "france_rent_honorary",         limit: 255
    t.integer  "france_mandat_exclusif",       limit: 4
  end

  add_index "property_complements", ["property_id"], name: "index_property_complements_on_property_id", using: :btree

  create_table "property_contact_type_translations", force: :cascade do |t|
    t.integer  "property_contact_type_id", limit: 8,   null: false
    t.string   "locale",                   limit: 255, null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "name",                     limit: 255
  end

  add_index "property_contact_type_translations", ["locale"], name: "index_property_contact_type_translations_on_locale", using: :btree
  add_index "property_contact_type_translations", ["property_contact_type_id"], name: "index_d7d674ccbea6d447ccc0bdffae93041b053731d9", using: :btree

  create_table "property_contact_types", force: :cascade do |t|
    t.string   "slug",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "property_contacts", force: :cascade do |t|
    t.string   "uid",                      limit: 255
    t.integer  "api_provider_id",          limit: 4
    t.integer  "user_id",                  limit: 4
    t.string   "email",                    limit: 255
    t.integer  "property_contact_type_id", limit: 8
    t.string   "civility_id",              limit: 255
    t.string   "first_name",               limit: 255
    t.string   "last_name",                limit: 255
    t.string   "phone",                    limit: 255
    t.string   "mobile",                   limit: 255
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "address",                  limit: 255
    t.string   "zipcode",                  limit: 255
    t.string   "city",                     limit: 255
    t.string   "country",                  limit: 255
    t.string   "iban",                     limit: 255
    t.string   "original_picture_url",     limit: 255
    t.string   "picture_file_name",        limit: 255
    t.string   "picture_content_type",     limit: 255
    t.integer  "picture_file_size",        limit: 4
    t.datetime "picture_updated_at"
  end

  add_index "property_contacts", ["api_provider_id"], name: "index_property_contacts_on_api_provider_id", using: :btree
  add_index "property_contacts", ["property_contact_type_id"], name: "index_property_contacts_on_property_contact_type_id", using: :btree
  add_index "property_contacts", ["user_id"], name: "index_property_contacts_on_user_id", using: :btree

  create_table "property_flag_translations", force: :cascade do |t|
    t.integer  "property_flag_id", limit: 4,   null: false
    t.string   "locale",           limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",             limit: 255
  end

  add_index "property_flag_translations", ["locale"], name: "index_property_flag_translations_on_locale", using: :btree
  add_index "property_flag_translations", ["property_flag_id"], name: "index_property_flag_translations_on_property_flag_id", using: :btree

  create_table "property_flags", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "color",      limit: 255
    t.string   "icon",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "property_inclusions", force: :cascade do |t|
    t.integer  "property_id",                    limit: 4
    t.integer  "inclusion_air_conditioning",     limit: 4
    t.integer  "inclusion_hot_water",            limit: 4
    t.integer  "inclusion_heated",               limit: 4
    t.integer  "inclusion_electricity",          limit: 4
    t.integer  "inclusion_furnished",            limit: 4
    t.integer  "inclusion_fridge",               limit: 4
    t.integer  "inclusion_cooker",               limit: 4
    t.integer  "inclusion_dishwasher",           limit: 4
    t.integer  "inclusion_dryer",                limit: 4
    t.integer  "inclusion_washer",               limit: 4
    t.integer  "inclusion_microwave",            limit: 4
    t.integer  "detail_elevator",                limit: 4
    t.integer  "detail_laundry",                 limit: 4
    t.integer  "detail_garbage_chute",           limit: 4
    t.integer  "detail_common_space",            limit: 4
    t.integer  "detail_janitor",                 limit: 4
    t.integer  "detail_gym",                     limit: 4
    t.integer  "detail_golf",                    limit: 4, default: 0
    t.integer  "detail_tennis",                  limit: 4, default: 0
    t.integer  "detail_sauna",                   limit: 4
    t.integer  "detail_spa",                     limit: 4
    t.integer  "detail_inside_pool",             limit: 4
    t.integer  "detail_outside_pool",            limit: 4
    t.integer  "detail_inside_parking",          limit: 4
    t.integer  "detail_outside_parking",         limit: 4
    t.integer  "detail_parking_on_street",       limit: 4
    t.integer  "detail_garagebox",               limit: 4
    t.integer  "half_furnsihed_living_room",     limit: 4
    t.integer  "half_furnsihed_rooms",           limit: 4
    t.integer  "half_furnsihed_kitchen",         limit: 4
    t.integer  "half_furnsihed_other",           limit: 4
    t.integer  "indoor_attic",                   limit: 4
    t.integer  "indoor_attic_convertible",       limit: 4, default: 0
    t.integer  "indoor_attic_converted",         limit: 4, default: 0
    t.integer  "indoor_cellar",                  limit: 4
    t.integer  "indoor_central_vacuum",          limit: 4
    t.integer  "indoor_entries_washer_dryer",    limit: 4
    t.integer  "indoor_entries_dishwasher",      limit: 4
    t.integer  "indoor_fireplace",               limit: 4
    t.integer  "indoor_storage",                 limit: 4
    t.integer  "indoor_walk_in",                 limit: 4
    t.integer  "indoor_no_smoking",              limit: 4
    t.integer  "indoor_double_glazing",          limit: 4
    t.integer  "indoor_triple_glazing",          limit: 4
    t.integer  "floor_carpet",                   limit: 4
    t.integer  "floor_wood",                     limit: 4
    t.integer  "floor_floating",                 limit: 4
    t.integer  "floor_ceramic",                  limit: 4
    t.integer  "floor_parquet",                  limit: 4
    t.integer  "floor_cushion",                  limit: 4
    t.integer  "floor_vinyle",                   limit: 4
    t.integer  "floor_lino",                     limit: 4
    t.integer  "floor_marble",                   limit: 4
    t.integer  "exterior_land_access",           limit: 4
    t.integer  "exterior_back_balcony",          limit: 4
    t.integer  "exterior_front_balcony",         limit: 4
    t.integer  "exterior_private_patio",         limit: 4
    t.integer  "exterior_storage",               limit: 4
    t.integer  "exterior_terrace",               limit: 4
    t.integer  "exterior_veranda",               limit: 4
    t.integer  "exterior_garden",                limit: 4
    t.integer  "exterior_sea_view",              limit: 4, default: 0
    t.integer  "exterior_mountain_view",         limit: 4, default: 0
    t.integer  "accessibility_elevator",         limit: 4
    t.integer  "accessibility_balcony",          limit: 4
    t.integer  "accessibility_grab_bar",         limit: 4
    t.integer  "accessibility_wider_corridors",  limit: 4
    t.integer  "accessibility_lowered_switches", limit: 4
    t.integer  "accessibility_ramp",             limit: 4
    t.integer  "senior_autonomy",                limit: 4
    t.integer  "senior_half_autonomy",           limit: 4
    t.integer  "senior_no_autonomy",             limit: 4
    t.integer  "senior_meal",                    limit: 4
    t.integer  "senior_nursery",                 limit: 4
    t.integer  "senior_domestic_help",           limit: 4
    t.integer  "senior_community",               limit: 4
    t.integer  "senior_activities",              limit: 4
    t.integer  "senior_validated_residence",     limit: 4
    t.integer  "senior_housing_cooperative",     limit: 4
    t.integer  "pets_allow",                     limit: 4
    t.integer  "pets_cat",                       limit: 4
    t.integer  "pets_dog",                       limit: 4
    t.integer  "pets_little_dog",                limit: 4
    t.integer  "pets_cage_aquarium",             limit: 4
    t.integer  "pets_not_allow",                 limit: 4
    t.integer  "service_internet",               limit: 4
    t.integer  "service_tv",                     limit: 4
    t.integer  "service_tv_sat",                 limit: 4
    t.integer  "service_phone",                  limit: 4
    t.integer  "composition_bar",                limit: 4
    t.integer  "composition_living",             limit: 4
    t.integer  "composition_dining",             limit: 4
    t.integer  "composition_separe_toilet",      limit: 4
    t.integer  "composition_open_kitchen",       limit: 4
    t.integer  "heating_electric",               limit: 4
    t.integer  "heating_solar",                  limit: 4
    t.integer  "heating_gaz",                    limit: 4
    t.integer  "heating_condensation",           limit: 4
    t.integer  "heating_fuel",                   limit: 4
    t.integer  "heating_heat_pump",              limit: 4
    t.integer  "heating_floor_heating",          limit: 4
    t.integer  "transport_bicycle_path",         limit: 4
    t.integer  "transport_public_transport",     limit: 4
    t.integer  "transport_public_bike",          limit: 4
    t.integer  "transport_subway",               limit: 4
    t.integer  "transport_train_station",        limit: 4
    t.integer  "security_guardian",              limit: 4
    t.integer  "security_alarm",                 limit: 4
    t.integer  "security_intercom",              limit: 4
    t.integer  "security_camera",                limit: 4
    t.integer  "security_smoke_dectector",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "orientation_north",              limit: 4
    t.integer  "orientation_south",              limit: 4
    t.integer  "orientation_west",               limit: 4
    t.integer  "orientation_east",               limit: 4
  end

  add_index "property_inclusions", ["property_id"], name: "index_property_inclusions_on_property_id", using: :btree

  create_table "property_listing_translations", force: :cascade do |t|
    t.integer  "property_listing_id", limit: 4,   null: false
    t.string   "locale",              limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                limit: 255
  end

  add_index "property_listing_translations", ["locale"], name: "index_property_listing_translations_on_locale", using: :btree
  add_index "property_listing_translations", ["property_listing_id"], name: "index_property_listing_translations_on_property_listing_id", using: :btree

  create_table "property_listings", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "list_globally_match", limit: 255
    t.string   "kijiji_match",        limit: 255
  end

  create_table "property_status_translations", force: :cascade do |t|
    t.integer  "property_status_id", limit: 4,   null: false
    t.string   "locale",             limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",               limit: 255
  end

  add_index "property_status_translations", ["locale"], name: "index_property_status_translations_on_locale", using: :btree
  add_index "property_status_translations", ["property_status_id"], name: "index_property_status_translations_on_property_status_id", using: :btree

  create_table "property_statuses", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "lespac_name", limit: 255
  end

  create_table "property_translations", force: :cascade do |t|
    t.integer  "property_id",       limit: 4,     null: false
    t.string   "locale",            limit: 255,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title",             limit: 255
    t.text     "description",       limit: 65535
    t.string   "property_url",      limit: 255
    t.text     "description_short", limit: 65535
  end

  add_index "property_translations", ["locale"], name: "index_property_translations_on_locale", using: :btree
  add_index "property_translations", ["property_id"], name: "index_property_translations_on_property_id", using: :btree

  create_table "providers", force: :cascade do |t|
    t.string   "provider_id", limit: 255
    t.string   "name",        limit: 255
    t.string   "sudomains",   limit: 255
    t.string   "hosts",       limit: 255
    t.string   "host",        limit: 255
    t.string   "email",       limit: 255
    t.string   "link_host",   limit: 255
    t.string   "color_1",     limit: 255
    t.string   "color_2",     limit: 255
    t.string   "logo",        limit: 255
    t.string   "phone",       limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "providers_services", id: false, force: :cascade do |t|
    t.integer "provider_id", limit: 8
    t.integer "service_id",  limit: 8
  end

  add_index "providers_services", ["provider_id", "service_id"], name: "index_providers_services_on_provider_id_and_service_id", using: :btree
  add_index "providers_services", ["service_id"], name: "index_providers_services_on_service_id", using: :btree

  create_table "purchases", force: :cascade do |t|
    t.integer  "user_id",       limit: 4
    t.integer  "product_id",    limit: 4
    t.integer  "cost",          limit: 4
    t.integer  "property_id",   limit: 4
    t.datetime "periode_start"
    t.datetime "periode_stop"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "services",      limit: 255
    t.integer  "need_check",    limit: 4,   default: 1
  end

  add_index "purchases", ["product_id"], name: "index_purchases_on_product_id", using: :btree
  add_index "purchases", ["property_id"], name: "index_purchases_on_property_id", using: :btree
  add_index "purchases", ["user_id"], name: "index_purchases_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 8
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "room_translations", force: :cascade do |t|
    t.integer  "room_id",    limit: 4,   null: false
    t.string   "locale",     limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",       limit: 255
  end

  add_index "room_translations", ["locale"], name: "index_room_translations_on_locale", using: :btree
  add_index "room_translations", ["room_id"], name: "index_room_translations_on_room_id", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "scrapping_monitorings", force: :cascade do |t|
    t.integer  "api_provider_id", limit: 4
    t.integer  "ads_count",       limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "scrapping_monitorings", ["api_provider_id"], name: "index_scrapping_monitorings_on_api_provider_id", using: :btree

  create_table "searches", force: :cascade do |t|
    t.integer "property_classification", limit: 4
    t.integer "property_listing",        limit: 4
    t.integer "property_status",         limit: 4
  end

  create_table "service_orders", force: :cascade do |t|
    t.integer  "user_id",     limit: 8
    t.integer  "property_id", limit: 8
    t.integer  "service_id",  limit: 8
    t.integer  "status",      limit: 4, default: 0
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "service_orders", ["property_id"], name: "index_service_orders_on_property_id", using: :btree
  add_index "service_orders", ["service_id"], name: "index_service_orders_on_service_id", using: :btree
  add_index "service_orders", ["user_id"], name: "index_service_orders_on_user_id", using: :btree

  create_table "service_translations", force: :cascade do |t|
    t.integer  "service_id",            limit: 8,     null: false
    t.string   "locale",                limit: 255,   null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name",                  limit: 255
    t.text     "description",           limit: 65535
    t.text     "marketing_description", limit: 65535
  end

  add_index "service_translations", ["locale"], name: "index_service_translations_on_locale", using: :btree
  add_index "service_translations", ["service_id"], name: "index_service_translations_on_service_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.integer  "position",              limit: 4
    t.string   "slug",                  limit: 255
    t.string   "name",                  limit: 255
    t.string   "company",               limit: 255
    t.string   "wistia_id",             limit: 255
    t.string   "youtube_id",            limit: 255
    t.string   "immosquare_id",         limit: 255
    t.text     "description",           limit: 65535
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "immosquare_link",       limit: 255
    t.text     "marketing_description", limit: 65535
    t.integer  "service_id",            limit: 4
  end

  add_index "services", ["service_id"], name: "index_services_on_service_id", using: :btree

  create_table "services_users", force: :cascade do |t|
    t.integer "service_id",        limit: 8
    t.integer "user_id",           limit: 8
    t.integer "options",           limit: 4
    t.integer "on_marketing_page", limit: 4, default: 1
  end

  create_table "share_socials", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "property_id",     limit: 4
    t.integer  "api_provider_id", limit: 4
    t.text     "note",            limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "share_socials", ["api_provider_id"], name: "index_share_socials_on_api_provider_id", using: :btree
  add_index "share_socials", ["property_id"], name: "index_share_socials_on_property_id", using: :btree
  add_index "share_socials", ["user_id"], name: "index_share_socials_on_user_id", using: :btree

  create_table "statistics", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.text     "international",  limit: 65535
    t.text     "video",          limit: 65535
    t.text     "kijiji",         limit: 65535
    t.text     "lespac",         limit: 65535
    t.text     "louercom",       limit: 65535
    t.text     "craigslist",     limit: 65535
    t.text     "facebook",       limit: 65535
    t.integer  "facebook_count", limit: 4,     default: 0
    t.text     "twitter",        limit: 65535
    t.integer  "twitter_count",  limit: 4,     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "statistics", ["user_id"], name: "index_statistics_on_user_id", using: :btree

  create_table "support_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",       limit: 255
    t.string   "icon",       limit: 255
  end

  create_table "supports", force: :cascade do |t|
    t.integer  "draft",           limit: 4,     default: 0
    t.string   "title",           limit: 255
    t.integer  "status",          limit: 4
    t.string   "locale",          limit: 255
    t.integer  "support_type_id", limit: 4
    t.text     "summary",         limit: 65535
    t.text     "content",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",            limit: 255
  end

  add_index "supports", ["support_type_id"], name: "index_supports_on_support_type_id", using: :btree

  create_table "taxes", force: :cascade do |t|
    t.string   "slug",       limit: 255
    t.string   "name",       limit: 255
    t.float    "rate",       limit: 24
    t.string   "number",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "testimonials", force: :cascade do |t|
    t.integer  "user_id",    limit: 8
    t.string   "title",      limit: 255
    t.string   "full_name",  limit: 255
    t.text     "content",    limit: 65535
    t.integer  "status",     limit: 4,     default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "testimonials", ["user_id"], name: "index_testimonials_on_user_id", using: :btree

  create_table "theme_settings", force: :cascade do |t|
    t.integer  "user_id",                              limit: 4
    t.integer  "theme_id",                             limit: 4
    t.integer  "is_selected",                          limit: 4
    t.string   "wistia_id",                            limit: 255
    t.string   "img_id",                               limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "website_cover_file_name",              limit: 255
    t.string   "website_cover_content_type",           limit: 255
    t.integer  "website_cover_file_size",              limit: 4
    t.datetime "website_cover_updated_at"
    t.integer  "with_slim_button",                     limit: 4,   default: 0
    t.string   "website_cover_full_size_file_name",    limit: 255
    t.string   "website_cover_full_size_content_type", limit: 255
    t.integer  "website_cover_full_size_file_size",    limit: 4
    t.datetime "website_cover_full_size_updated_at"
  end

  add_index "theme_settings", ["theme_id"], name: "index_theme_settings_on_theme_id", using: :btree
  add_index "theme_settings", ["user_id"], name: "index_theme_settings_on_user_id", using: :btree

  create_table "theme_translations", force: :cascade do |t|
    t.integer  "theme_id",    limit: 8,   null: false
    t.string   "locale",      limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "description", limit: 255
  end

  add_index "theme_translations", ["locale"], name: "index_theme_translations_on_locale", using: :btree
  add_index "theme_translations", ["theme_id"], name: "index_theme_translations_on_theme_id", using: :btree

  create_table "themes", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.integer  "with_cover",           limit: 4,     default: 0
    t.integer  "with_background",      limit: 4,     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description",          limit: 65535
    t.integer  "with_cover_full_size", limit: 4,     default: 0
  end

  create_table "user_portal_providers", force: :cascade do |t|
    t.integer "user_id",         limit: 8
    t.integer "api_provider_id", limit: 8
    t.string  "flow_name",       limit: 255
    t.string  "uid",             limit: 255
  end

  add_index "user_portal_providers", ["api_provider_id", "uid"], name: "index_user_portal_providers_on_api_provider_id_and_uid", using: :btree
  add_index "user_portal_providers", ["api_provider_id"], name: "index_user_portal_providers_on_api_provider_id", using: :btree
  add_index "user_portal_providers", ["user_id"], name: "index_user_portal_providers_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.integer  "listing_limitation_id",    limit: 4
    t.integer  "agency_id",                limit: 4
    t.string   "stripe_customer_id",       limit: 255
    t.integer  "token_count",              limit: 4,     default: 0
    t.string   "username",                 limit: 255
    t.integer  "can_manage_api_id",        limit: 4
    t.string   "email",                    limit: 255,   default: "",    null: false
    t.string   "kangalou_params",          limit: 255
    t.integer  "kangalou_tokens_not_sync", limit: 4,     default: 0
    t.string   "encrypted_password",       limit: 255,   default: "",    null: false
    t.string   "reset_password_token",     limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            limit: 4,     default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",       limit: 255
    t.string   "last_sign_in_ip",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "confirmation_token",       limit: 255
    t.date     "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",        limit: 255
    t.string   "last_4_digits",            limit: 255
    t.boolean  "is_owner",                               default: false
    t.text     "tokens",                   limit: 65535
    t.string   "connect_token",            limit: 255
  end

  add_index "users", ["agency_id"], name: "index_users_on_agency_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "videos", force: :cascade do |t|
    t.integer  "property_id",         limit: 8
    t.integer  "posting_status",      limit: 4,   default: 1
    t.integer  "authentification_id", limit: 8
    t.string   "youtube_id",          limit: 255
    t.date     "visibility_start"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.date     "visibility_until"
  end

  add_index "videos", ["authentification_id"], name: "index_videos_on_authentification_id", using: :btree
  add_index "videos", ["property_id"], name: "index_videos_on_property_id", using: :btree

  create_table "visibility_portals", force: :cascade do |t|
    t.integer  "property_id",      limit: 4
    t.integer  "portal_id",        limit: 4
    t.integer  "posting_status",   limit: 4
    t.integer  "count",            limit: 4,     default: 0
    t.date     "visibility_start"
    t.date     "visibility_until"
    t.string   "backlink_id",      limit: 255
    t.string   "backlink_url",     limit: 255
    t.text     "craigslist_ids",   limit: 65535
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.text     "history",          limit: 65535
  end

  add_index "visibility_portals", ["backlink_id"], name: "index_visibility_portals_on_backlink_id", using: :btree
  add_index "visibility_portals", ["portal_id"], name: "index_visibility_portals_on_portal_id", using: :btree
  add_index "visibility_portals", ["posting_status"], name: "index_visibility_portals_on_posting_status", using: :btree
  add_index "visibility_portals", ["property_id"], name: "index_visibility_portals_on_property_id", using: :btree

  create_table "wysiwyg_pictures", force: :cascade do |t|
    t.integer  "imageable_id",         limit: 8
    t.string   "imageable_type",       limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "picture_file_name",    limit: 255
    t.string   "picture_content_type", limit: 255
    t.integer  "picture_file_size",    limit: 4
    t.datetime "picture_updated_at"
  end

  add_index "wysiwyg_pictures", ["imageable_type", "imageable_id"], name: "index_wysiwyg_pictures_on_imageable_type_and_imageable_id", using: :btree
  add_index "wysiwyg_pictures", ["imageable_type"], name: "index_wysiwyg_pictures_on_imageable_type", using: :btree

  add_foreign_key "feedback_reports", "feedbacks"
  add_foreign_key "nomenclature_element_defaults", "nomenclature_defaults"
  add_foreign_key "scrapping_monitorings", "api_providers"
end
