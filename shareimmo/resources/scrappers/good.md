# Setup Server
    sudo locale-gen en_CA.UTF-8
    sudo apt-get update && apt-get upgrade
    sudo apt-get install zsh
    sudo apt-get dist-upgrade
    sudo reboot

# Java 8 JDK
    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update
    sudo apt-get install oracle-java8-installer


# Graphic interfac (XFCE4)
    sudo apt-get install xfce4 xfce4-goodies
    sudo reboot

# Vnc
    sudo apt-get install tightvncserver


# Setup User : Customer
    adduser customer
    usermod -aG sudo customer
    ssh-keygen
    ssh-copy-id customer@grid01.immosquare.com
    cp /root/.ssh/authorized_keys /home/customer/.ssh/authorized_keys
    exit
    ssh customer@grid01.immosquare.com
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    exit
    exit
    ssh customer@grid01.immosquare.com
    sudo dpkg-reconfigure tzdata


=========== SNAPCHAT ON DIGITAL ===========


# Vnc Setup
    vncserver

    vncserver -kill :1
    vim ~/.vnc/xstartup
#!/bin/bash
xrdb $HOME/.Xresources
startxfce4 &

    sudo chmod +x ~/.vnc/xstartup
    vncserver
    sudo vim /etc/systemd/system/vncserver@.service
[Unit]
Description=Start TightVNC server at startup
After=syslog.target network.target

[Service]
Type=forking
User=customer
PAMName=login
PIDFile=/home/customer/.vnc/%H:%i.pid
ExecStartPre=-/usr/bin/vncserver -kill :%i > /dev/null 2>&1
ExecStart=/usr/bin/vncserver -depth 24 -geometry 1280x800 :%i
ExecStop=/usr/bin/vncserver -kill :%i

[Install]
WantedBy=multi-user.target

    sudo systemctl daemon-reload
    sudo systemctl enable vncserver@1.service
    vncserver -kill :1
    sudo systemctl start vncserver@1
    sudo reboot




# Selenium
    cd ~/ && wget https://selenium-release.storage.googleapis.com/3.0/selenium-server-standalone-3.0.1.jar


# Firefox
    cd ~ && wget https://ftp.mozilla.org/pub/firefox/releases/45.6.0esr/linux-x86_64/en-US/firefox-45.6.0esr.tar.bz2
    sudo tar -xjf firefox-45.6.0esr.tar.bz2
    ln -s /home/customer/firefox /usr/bin/firefox


# geckodriver
    cd ~ && wget 'https://github.com/mozilla/geckodriver/releases/download/v0.11.1/geckodriver-v0.11.1-linux64.tar.gz' && tar -xvzf geckodriver-v0.11.1-linux64.tar.gz
    sudo chmod +x /home/customer/geckodriver

# PATH
    vim ~/.zshrc

export ZSH=/home/customer/.oh-my-zsh
ZSH_THEME="robbyrussell"
plugins=(git)
source $ZSH/oh-my-zsh.sh

DISPLAY=1
PATH=$PATH:/home/customer


# xvfb
    sudo apt-get install xvfb
    /usr/bin/Xvfb :99 -ac -screen 0 1024x768x8 & export DISPLAY=":99" && java -jar ~/selenium-server-standalone-3.0.1.jar -port 4444


=========== CELA FONCTIONNE AU DESSUS DE CETTE LIGNE ===========



# Lunch Grid (http://grid01.immosquare.com:4444/grid/console)
    java -jar ~/selenium-server-standalone-3.0.1.jar  -role hub -newSessionWaitTimeout 500000 -timeout 120 -browserTimeout 240



#Lunch Node
    java -jar ~/selenium-server-standalone-3.0.1.jar -role node -hub http://10.17.0.14:4444/grid/register/ -browser "browserName=firefox,maxInstances=12,platform=LINUX,seleniumProtocol=WebDriver" -maxSession 10



# Chrome
    sudo apt-get install libxss1 libappindicator1 libindicator7
    cd ~ && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    sudo dpkg -i google-chrome*.deb
    apt-get -f install
    sudo dpkg -i google-chrome*.deb


# ChromeDriver
    cd ~ && wget http://chromedriver.storage.googleapis.com/2.9/chromedriver_linux64.zip
    sudo apt-get install unzip
    unzip chromedriver_linux64.zip chromedriver
    sudo chmod +x chromedriver
    sudo ln -s /home/customer/chromedriver /usr/bin/chromedriver








