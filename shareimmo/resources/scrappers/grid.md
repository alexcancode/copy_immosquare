







# Selenium
    cd /root/Desktop/scrapper && wget https://selenium-release.storage.googleapis.com/3.0/selenium-server-standalone-3.0.1.jar
    cd /root/Desktop/scrapper && wget https://selenium-release.storage.googleapis.com/2.52/selenium-server-standalone-2.52.0.jar


# Firefox
    apt-get install firefox



# Geckodriver
    cd /root/Desktop/scrapper  && wget 'https://github.com/mozilla/geckodriver/releases/download/v0.11.1/geckodriver-v0.11.1-linux64.tar.gz' && tar -xvzf geckodriver-v0.11.1-linux64.tar.gz && rm geckodriver-v0.11.1-linux64.tar.gz
    ln -s /home/geckodriver /usr/bin/geckodriver


    cd /root/Desktop/scrapper  && wget 'http://chromedriver.storage.googleapis.com/2.9/chromedriver_linux64.zip' && unzip chromedriver_linux64.zip && rm chromedriver_linux64.zip




# CHROME
    sudo apt-get install libxss1 libappindicator1 libindicator7
    cd /root/Desktop/scrapper && wget 'https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb'



# Install headless GUI for firefox.
choose a display number that is unlikely to ever clash (even if you add a real display later) – something high like 10 should do

    apt-get install xvfb
    Xvfb :10 -ac &
    export DISPLAY=:10


## Lunch Node
    screen -S node
    xvfb-run java -jar /home/selenium-server-standalone-3.0.1.jar  -role node -hub http://grid01.immosquare.com:4444/grid/registrer -browser "browserName=firefox,maxInstances=12,platform=LINUX,seleniumProtocol=WebDriver" -maxSession 10 -host node01.immosquare.com
    xvfb-run java -jar /home/selenium-server-standalone-2.53.1.jar -role node -hub http://grid01.immosquare.com:4444/grid/registrer -browser "browserName=firefox,maxInstances=12,platform=LINUX,seleniumProtocol=WebDriver" -maxSession 10 -host node01.immosquare.com
