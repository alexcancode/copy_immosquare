Bonjour Jules,

Voici les informations techniques nécessaires :
La doc des fichiers est ici : http://www.cryptml.net/documentation.asp

1 - Publication BackOffice
Il faut que tu publies un fichier 00000098.CryptML dans le FTP suivant :
URL : ftpimmosquare.pagesimmo.com
User: ftpimmosquare.pagesimmo.com|00000098    (il faut préfixer le compte avec le nom de domaine)
Pwd: ba49w55qr

L'import de ce fichier créera les comptes des agences avec leurs dossiers.

2 - Publication Annonces
Il y a un compte ftp par agence, il faut donc publier un fichier par agence.
Il faut que tu publies un fichier xxQyyyyy.CrypML.zip dans le FTP suivant :
URL : ftpimmosquare.pagesimmo.com
User: ftpimmosquare.pagesimmo.com|xxQyyyyy    (il faut préfixer le compte avec le nom de domaine)
Pwd: celui fourni dans le fichier de backoffice

Ci-joint un fichier d'exemple, avec les différents modes de transfert des images.
Les images avec juste l'indice sont des images non modifiées.
Les biens avec juste la référence sont des biens non modifiés.
C'est de l'annule / remplace, donc un bien non fourni sera supprimé s'il existe.

Ci-joint, tu trouveras également le liste des principaux serveurs d'annonces, avec leurs clés, qui te seront utiles pour l'utilisation des web services à venir.

Cordialement.
