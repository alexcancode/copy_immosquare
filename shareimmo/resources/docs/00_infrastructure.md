# Opus - Infrastructure

## 1. Servers

| Name  | Dns | Type | IP | Description | Memory | CPU | More |
--------|-----|------|----|-------------|--------|-----|------|
| db03 | db03.immosquare.com | Virtual machine | 159.203.70.65 | DB | 1G | 2 Virtual C | DB for shareimmo apps |
| ovh-mon-shareimmo | web03.immosquare.com | Virtual machine | 158.69.208.15 | Ruby | 8G | 2 Virtual C | Shareimmo Apps |


## 2. Application

### Environment

| Service | Version | Description |
|---------|---------|-------------|
| Rvm | 1.2.6 | Main language |
| MySQL | 5.5 | Database engine |


### Applications
| Name | Server | Description | More |
|------|--------|-------------|------|
| shareimmo | shareimmo | Main app ||
| api | api | Shareimmo Api project ||


## 3. Some utils DNS

| DNS name | Description |
|----------|-------------|
| shareimmo.com | Main application |
| apps.shareimmo.com | Applications (File for clients) |
| api.shareimmo.com / depot.shareimmo.com | Admin application for monitoring |


## 4. Dependencies
| Service | Description |
|----------|-------------|
| zsh | |
| imagemagick | |
| libmagickwand-dev | |
| vsftpd | |
| libmysqlclient-dev | |
| nodejs |||
| phantomjs |||
| sox |||
| libsox-fmt-mp3 |||
| ffmpeg|||
| redis-server |||



## 5. Shell Framework
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

