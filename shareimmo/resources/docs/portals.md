# Shareimmo - Portals

## France

### Technical implementation


#### Generic

* For France, use SOAP (Savon gem)
* Implementation by portal (Add visibility_portals table). Each portal can have the own visibility (Not global for property)
* Technical implementation solution is Pagesimmo

#### Admin tasks
* Migration from global visibilities to portal_visibilities => rake shareimmo:visibilities:update_with_portals



#### Pagesimmo

In portals namespace, we have some tasks :
* update : Update portal lists (ok) => rake pagesimmo:portals:update

Workflow :
* Publish agence (backoffice) => rake pagesimmo:agencies:publish 						(ok)
* Publish property (backoffice) => rake pagesimmo:properties:publish 					(ok)
* Get inscription for agency in server => rake pagesimmo:agencies:subscriptions	
* Get keys for properties from server => rake pagesimmo:properties:get_subscriptions
* Set diffusions for agency in server => rake pagesimmo:properties:set_diffusions


