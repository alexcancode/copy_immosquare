
<?php
	// Define variables
	$shareimmo_api_key = "your-api-key";
	$shareimmo_api_token = "your-api-password";

	// Set server
	// Staging url
	$shareimmo_host = "http://dulcisdomus.co/api/v2/";
	// Production url
	// $shareimmo_host = "http://shareimmo.com/api/v2/";

	// init curl
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


	// Create account

	$shareimmo_call = "accounts";
	$body = json_encode(
				array(
					'uid' => "1",
					'email' => "your@email.com",
					'password' => "your-password"
				)
			);	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');


	// Update account (where uid = your id)

	// $shareimmo_call = "accounts/1";
	// $body = json_encode(
	// 			array(
	// 				'email' 			=> "tesssstee@shareimmo.com",
	// 				'country' 			=> "fr",
	// 				'phone'				=> '0512345678',
	// 				'phone_visibility'	=> 1,
	//   				'website'			=> "mywebsite.com"
	// 			)
	// 		);	
	// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');


	$headers = array();
	$headers[] = "Content-Type: application/json";
	$headers[] = "ApiKey: ".$shareimmo_api_key;
	$headers[] = "ApiToken: ".$shareimmo_api_token;


	// Add params for curl requests (header, body, url)
	curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
	curl_setopt($ch, CURLOPT_URL,$shareimmo_host.$shareimmo_call);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


	// Execute
	$response=curl_exec($ch);
	// Closing
	curl_close($ch);

	// Get response object
	$result = json_decode($response, true);
	var_dump($result);

?>