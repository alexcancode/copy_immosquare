POST /api/v1/login

curl -H "Content-Type: application/json" -X POST -d '{ "email": "antoine@immosquare.com", "password": "xxxx" }' http://localhost:3000/api/v1/login


GET /api/v1/sync_edl
{
  "credentials" : {
    "apiKey": "XsvfSld1ftVN0qC0ZzdHk7uRpfV4Rh",
    "apiToken" : "mrAl2Ki03RTjD8Z6WyW0AXOvTp50TGqbCdmlisVR"
  },
  "accountUid": 2,
}

curl -H "Content-Type: application/json" -X GET -d '{
  "credentials" : {
    "apiKey": "XsvfSld1ftVN0qC0ZzdHk7uRpfV4Rh",
    "apiToken" : "mrAl2Ki03RTjD8Z6WyW0AXOvTp50TGqbCdmlisVR"
  },
  "accountUid": 2
}
' http://localhost:3000/api/v1/sync_edl


POST /api/v1/inventories
{
  "credentials" : {
    "apiKey": "XsvfSld1ftVN0qC0ZzdHk7uRpfV4Rh",
    "apiToken" : "mrAl2Ki03RTjD8Z6WyW0AXOvTp50TGqbCdmlisVR"
  },
  "accountUid": 2,
  "inventory": {
  	  "designation": "test designation",
	  "property_id": 94,
	  "xml_file" : "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxub3RlPg0KCTx0bz5Ub3ZlPC90bz4NCgk8ZnJvbT5KYW5pPC9mcm9tPg0KCTxoZWFkaW5nPlJlbWluZGVyPC9oZWFkaW5nPg0KCTxib2R5PkRvbid0IGZvcmdldCBtZSB0aGlzIHdlZWtlbmQhPC9ib2R5Pg0KPC9ub3RlPg=="
  }
}

curl -i -X POST -H "Content-Type: application/json" -d '{
  "credentials" : {
    "apiKey": "XsvfSld1ftVN0qC0ZzdHk7uRpfV4Rh",
    "apiToken" : "mrAl2Ki03RTjD8Z6WyW0AXOvTp50TGqbCdmlisVR"
  },
  "accountUid": 2,
  "inventory": {
  	  "designation": "test designation",
	  "property_id": 94,
	  "xml_file" : "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxub3RlPg0KCTx0bz5Ub3ZlPC90bz4NCgk8ZnJvbT5KYW5pPC9mcm9tPg0KCTxoZWFkaW5nPlJlbWluZGVyPC9oZWFkaW5nPg0KCTxib2R5PkRvbid0IGZvcmdldCBtZSB0aGlzIHdlZWtlbmQhPC9ib2R5Pg0KPC9ub3RlPg=="
  }
}' http://lvh.me:3000/api/v1/inventories



GET /api/v1/buildings
{
  "credentials" : {
    "apiKey": "XsvfSld1ftVN0qC0ZzdHk7uRpfV4Rh",
    "apiToken" : "mrAl2Ki03RTjD8Z6WyW0AXOvTp50TGqbCdmlisVR"
  },
  "accountUid": 2
}

curl -i -X GET -H "Content-Type: application/json" -d '{ "credentials" : { "apiKey": "XsvfSld1ftVN0qC0ZzdHk7uRpfV4Rh", "apiToken" : "mrAl2Ki03RTjD8Z6WyW0AXOvTp50TGqbCdmlisVR" }, "accountUid": 2 }' http://lvh.me:3000/api/v1/buildings



