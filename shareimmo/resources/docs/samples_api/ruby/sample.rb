require 'uri'
require 'net/http'
require "httparty"

url = URI("http://shareimmo.com/api/v2/properties")

http = Net::HTTP.new(url.host, url.port)

body = {
	:uid => "2328496",
	:account_uid => "158347",
	:classification_id => 102,
	:status_id => 1,
	:listing_id => 1,
	:flag_id => 1,
	:availability_date => "31-05-2016",
	:index_energy => "A",
	:belgium => {
		:peb_id => 1111,
		:co2_kgm => 123123,
		:peb_value => 631,
		:peb_indice => "A",
		:sensitive_flood_area => 0,
		:delimited_flood_zone => 0,
		:risk_area => 0		
	},
	:address => {
		:address_visibility => 1,
		:address_formatted => "address_formatted",
		:street_name => "street_name",
		:zip_code => "1234",
		:locality => "locality",
		:country => "CA",
		:latitude => "50.8522009",
		:longitude => "4.3303277"
	},
	:area => {
		:living => 123,
		:balcony => 0,
		:land => 123,
		# :unit_id => 0
	},
	:description => {
		:fr => "",
		:en => "",
		:es => "",
		:it => "",
		:nl => "",
		:pt => "",
	},
	:financial => {
		:price => 260000,
		:rent => "",
		:currency => "EUR"
	},
	:pictures => [
			{ :url => URI.escape("http://yoururl.ca")},
			{ :url => URI.escape("http://yoururl2.ca")}
	],
	:rooms => {
		:rooms => 0,
		:half => "",
		:bathrooms => 1,
		:bedrooms => 4,
		:showerrooms => "",
	},
	:other => {
		:exterior => {
			:terrace => 1
		}
	}


}

options = {
    :headers    => {
        "ApiKey" => "your-api-key",
        "ApiToken" => "your-api-token"
    }
}

options[:body] = body

httparty = HTTParty.put("http://shareimmo.com/api/v2/properties",options)
@response = JSON.parse(httparty.body)


puts @response