<?php

	$url = 'http://shareimmo.com/api/v2/properties';
	$ch = curl_init($url);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    'Content-Type: application/json',		
	    'ApiKey: your-api-key',
	    'ApiToken: your-api-token'
	    ));

	$post_params = array (
		'uid' => "2328496",
		'account_uid' => "158347",
		'classification_id' => 102,
		'status_id' => 1,
		'listing_id' => 1,
		'flag_id' => 1,
		'availability_date' => '31-05-2016',
		'index_energy' => 'A',
		'belgium' => array(
			'peb_id' => 1111,
			'co2_kgm' => 123123,
			'peb_value' => 631,
			'peb_indice' => "A",
			'sensitive_flood_area' => 0,
			'delimited_flood_zone' => 0,
			'risk_area' => 0					
		),
		'address' => array( 
			'address_visibility' => 1,
			'address_formatted' => '1080 Sint-jans-molenbeek BELGIQUE, 1080 Molenbeek-Saint-Jean',
			'street_name' => '1080 Sint-jans-molenbeek BELGIQUE',
			'zip_code' => '1080',
			'locality' => 'Molenbeek-Saint-Jean',
			'country' => 'BE',
			'latitude' => '50.8522009',
			'longitude' => '4.3303277'
		),
		'area' => array(
			'living' => 300,
			'balcony' => 0,
			'land' => 123,
			//unit_id => 0
		),
		'description' => array(
			'fr' => "",
			'en' => "",
			'es' => "",
			'it' => "",
			'nl' => "",
			'pt' => "",
		),
		'financial' => array(
			'price' => 260000,
			'rent' => "",
			'currency' => "EUR"
		),
		'pictures' => array (
				array ('url' => urlencode("http//s3.eu-central-1.amazonaws.com/shelterr-jabba/shelterr/images/158347/bg_ACTIVIMMO-a-vendre-shelterr-0.13022300 1463047380.jpg") ),
				array ( 'url' => urlencode("http//s3.eu-central-1.amazonaws.com/shelterr-jarjar/shelterr/images/158347/bg_ACTIVIMMO-a-vendre-shelterr-0.75509300 1463047402.jpg") ),
				array ( 'url' => urlencode("http//s3.eu-central-1.amazonaws.com/shelterr-joda/shelterr/images/158347/bg_ACTIVIMMO-a-vendre-shelterr-0.41135600 1463047426.jpg") ),
				array ( 'url' => urlencode("http//s3.eu-central-1.amazonaws.com/shelterr-luc/shelterr/images/158347/bg_ACTIVIMMO-a-vendre-shelterr-0.11806800 1463047448.jpg") ),
				array ( 'url' => urlencode("http//s3.eu-central-1.amazonaws.com/shelterr-wampa/shelterr/images/158347/bg_ACTIVIMMO-a-vendre-shelterr-0.46443000 1463047469.jpg") )
		),
		'rooms' => array( 
			'rooms' => 0,
			'half' => "",
			'bathrooms' => 1,
			'bedrooms' => 4,
			'showerrooms' => "",
		),
		'other' => array (
			'exterior' => array (
				'terrace' => 1
			)
		)
	);


	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_params));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	$response = curl_exec($ch);
	curl_close($ch);

?>