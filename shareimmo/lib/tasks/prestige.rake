require 'uri'
require 'open-uri'

namespace :prestige do


  ##============================================================##
  ## Syncrhonisations
  ##============================================================##
  task :synchronization => :environment do
    @prestige_in_shareimmo_properties   = {}
    @prestige_in_shareimmo_by_status    = {}
    @prestige_in_shareimmo_by_status[0] = 0
    @prestige_in_shareimmo_by_status[1] = 0
    @properties_missing                 = 0
    @properties_bad_status              = 0

    Property.where(:api_provider_id => 23).each do |p|
      @prestige_in_shareimmo_properties[p.uid.to_i] = p
      @prestige_in_shareimmo_by_status[p.status]    += 1
    end

    ##============================================================##
    ## Get connection to presige db
    ## Get redis key with all prestige existing accounts
    ## Requet to get all prestige properties
    ##============================================================##
    @connection = ActiveRecord::Base.establish_connection(
      :adapter => "mysql2",
      :host => ENV["prestige_db_host"],
      :database => ENV["prestige_db_database"],
      :username => ENV["prestige_db_username"],
      :password =>  ENV["prestige_db_password"]
      )

    @sql_properties = " select p.*, s.Id as property_status, p.Id as property_type, c.IsoCode as property_country, \
    pd_fr.Title as title_fr, pd_en.Title as title_en, \
    cu.IsoCode as property_currency,  \
    pd_fr.Description as description_fr, pd_en.Description as description_en, \
    ptr.Name as property_transaction_type, pty.Name as property_type \
    from Property p \
    left join PropertyStatus s on (s.Id=p.IdPropertyStatus) \
    left join PropertyType t on (t.Id=p.IdPropertyType) \
    left join Country c on (c.Id=p.IdCountry) \
    left join Description pd_fr on (pd_fr.TypeOwner = \"Property\" and pd_fr.IdOwner = p.Id and pd_fr.IdLanguage = 2) \
    left join Description pd_en on (pd_en.TypeOwner = \"Property\" and pd_en.IdOwner = p.Id and pd_en.IdLanguage = 1) \
    left join Currency cu on (p.IdCurrency = cu.Id) \
    left join PropertyTransaction ptr on (ptr.Id=p.IdPropertyTransaction) \
    left join PropertyType pty on (pty.Id=p.IdPropertyType) \
    group by p.Id \
    "

    @result_properties = @connection.connection.execute(@sql_properties);
    @prestige_properties_by_status = {}
    @prestige_properties_by_status[0] = 0
    @prestige_properties_by_status[1] = 0
    @prestige_properties_total = 0

    @prestige_properties = {}
    @result_properties.each(:as => :hash) do |p|
      @prestige_properties_total += 1
      case p["property_status"].to_i
      when 1
        @status = 1
      when 4
        @status = 0
      when 5
        @status = 1
      when 6
        @status = 1
      else
        @status = 0
      end
      @status = 0 if p["DeletedAt"].present?
      @properties_missing += 1 if @prestige_in_shareimmo_properties[p["Id"].to_i].nil?
      @prestige_properties_by_status[@status] += 1
    end


    uri = URI.parse("http://apps.shareimmo.com/zoomvisit.xml")
    @properties_in_xml = []
    @properties_in_xml_for_sale = []

    open(uri) do |file|
      @xml = Nokogiri::XML(file)
      @xml.css("properties").each do |properties|
        properties.css("property").each do |p|
          @properties_in_xml << p.css("uid").first.content
          @properties_in_xml_for_sale << p.css("uid").first.content if p.css("listing_id").first.content.to_i == 1
        end
      end
    end

    puts "prestige:in_prestige #{@prestige_properties_total}"
    puts "prestige:in_prestige:deactivated - #{@prestige_properties_by_status[0]}"
    puts "prestige:in_prestige:activated - #{@prestige_properties_by_status[1]}"
    puts "prestige:in_shareimmo #{@prestige_in_shareimmo_properties.count}"
    puts "prestige:in_shareimmo:missing - #{@properties_missing}"
    puts "prestige:in_shareimmo:deactivated - #{@prestige_in_shareimmo_by_status[0]}"
    puts "prestige:in_shareimmo:activated - #{@prestige_in_shareimmo_by_status[1]}"
    puts "prestige:in_xml - #{@properties_in_xml.count}"
    puts "prestige:in_xml:for_sale - #{@properties_in_xml_for_sale.count}"
    @infos = {
      :prestige_properties_total => @prestige_properties_total,
      :prestige_properties_by_status_0 => @prestige_properties_by_status[0],
      :prestige_properties_by_status_1 => @prestige_properties_by_status[1],
      :prestige_in_shareimmo_properties => @prestige_in_shareimmo_properties,
      :properties_missing => @properties_missing,
      :prestige_in_shareimmo_by_status_0 => @prestige_in_shareimmo_by_status[0],
      :prestige_in_shareimmo_by_status_1 => @prestige_properties_total[1],
      :properties_in_xml => @properties_in_xml,
      :properties_in_xml_for_sale => @properties_in_xml_for_sale.count
    }
    TaskMailer.prestige_mailer(@infos)
  end


end
