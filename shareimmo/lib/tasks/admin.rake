namespace :admin do

  ##============================================================##
  ## 6989: Heytienne
  ## 7677: Osiris
  ## 8468: RGR
  ## 4870: IMMOPRO
  ##============================================================##
  task :resize_pictures => :environment do
    # user_ids    = [1]
    # properties  = Property.where(:user_id => user_ids)
    properties  = Property.where(:image_process => 0,:status =>1).limit(1000)
    count       = properties.count
    start       = Time.now
    puts  start
    puts "Properties count : #{count}"
    pbar        = ANSI::Progressbar.new("Properties",count)
    pbar.__send__ :show
    properties.reverse_order.each do |p|
      pbar.inc(1)
      begin
        p.property_assets.each do |a|
          a.picture.reprocess!
        end
        p.update_attribute(:image_process,1)
      rescue Exception => e
        JulesLogger.info e.message
      end
    end
    puts "Duration #{Time.now - start}"
  end



end

