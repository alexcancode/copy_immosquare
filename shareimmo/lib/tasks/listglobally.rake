namespace :listglobally do

  ##============================================================##
  ## on inclut les helpers des routes pour les backlinks
  ##============================================================##
  include Rails.application.routes.url_helpers
  default_url_options[:host] = 'shareimmo.com'


  ##============================================================##
  ## Création d'un flux xml au format ListGlobally
  ##============================================================##
  task :xml => :environment do
    begin
      listglobally_path = File.join("tmp","apps")
      listglobally_file = File.join(listglobally_path,"listglobally.xml")
      FileUtils.mkdir_p(listglobally_path) unless File.exists?(listglobally_path)
      FileUtils.rm(listglobally_file) if File.exist?(listglobally_file)
      puts "Old listglobally File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
      builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
        xml.Adverts do
          ids = Array.new
          Property.includes(:property_assets,:visibility_portals,:property_complement,:property_inclusion).where.not(:draft=>1).where(:status=>1, :visibility_portals => {:portal_id => Portal.where(:slug => "listglobally").first.id}).where("visibility_portals.visibility_until > ?", Time.now).each do |p|
            puts "Start xml advert for property with id ##{p.id}"
            ids << p.id
            xml.Advert do
              xml.SiteAccountId   p.user.id
              xml.AdvertId        p.id
              xml.Reference       p.uid.present? ? p.uid : p.id
              xml.CustomerType    p.user.profile.is_a_pro_user == 0 ? "Private" : "Professional"
              xml.AdvertType      p.property_listing.list_globally_match
              xml.GoodType        p.property_classification.list_globally_match
              xml.PublicationDate p.created_at.to_time.iso8601
              xml.Rooms           p.rooms     if p.rooms.present?
              xml.Bedrooms        p.bedrooms  if p.bedrooms.blank?
              xml.LivingArea      p.area_unit_id == 1 ? p.area_living.ceil : (p.area_living/10.76391041671).ceil if p.area_living.present?
              xml.PropertyUrl     p.property_url if p.property_url.present?
              if p.description.present?
                xml.Descriptions do
                  ##==================================================================================================##
                  ## Il faut essayer de ne pas envoyer les descriptions vide de texte mais qui contienent des lignes
                  ## blanches en html.Cette méthode compte les mots
                  ##==================================================================================================##
                  french_description = p.description_fr
                  if french_description.present? and (french_description).scan(/[\w-]+/).size.to_i > 5
                    xml.Description(:Language => 'fr') { xml.cdata french_description }
                  end
                  english_description = p.description_en
                  if english_description.present? and (english_description).scan(/[\w-]+/).size.to_i > 5
                    xml.Description(:Language => 'en') { xml.cdata english_description }
                  end
                end
              end
              if p.property_assets.present?
                xml.Photos do
                  p.property_assets.each do |property_asset|
                    if property_asset.picture.url(:medium).start_with?('https')
                      medium_picture = property_asset.picture.url(:medium).gsub('https', 'http')
                    else
                      medium_picture = property_asset.picture.url(:medium)
                    end
                    if property_asset.picture.url(:large).start_with?('https')
                      large_picture = property_asset.picture.url(:large).gsub('https', 'http')
                    else
                      large_picture = property_asset.picture.url(:large)
                    end
                    xml.Photo :Small => medium_picture, :Big => large_picture
                  end
                end
              end
              if p.property_listing.list_globally_match == "Sale"
                xml.Price p.price.to_i unless p.price.blank?
                xml.PriceCurrency p.currency unless p.currency.blank?
              else
                unless p.rent_frequency.blank?
                  case p.rent_frequency.to_i
                  when 1
                    xml.Price p.rent.to_i unless p.rent.blank?
                    xml.PriceType "Daily"
                  when 7
                    xml.Price p.rent.to_i unless p.rent.blank?
                    xml.PriceType "Weekly"
                  when 14
                    xml.Price p.rent.to_i unless p.rent.blank?
                    xml.PriceType "Fortnightly"
                  when 30
                    xml.Price p.rent.to_i unless p.rent.blank?
                    xml.PriceType "Monthly"
                  else
                    xml.Price (p.rent.to_i * 30) / p.rent_frequency.to_i unless p.rent.blank?
                    xml.PriceType "Monthly"
                  end
                else
                  xml.Price p.rent.to_i unless p.rent.blank?
                end
                xml.PriceCurrency p.currency unless p.currency.blank?
              end
              xml.Address "#{p.street_number} #{p.street_name}, #{p.locality}, #{p.administrative_area_level_1} #{p.zipcode}, #{p.country_short} "
              xml.PostalCode p.zipcode              if p.zipcode.present?
              xml.City p.locality                   if p.locality.present?
              xml.Country p.country_short.upcase    if p.country_short.present?
              unless (p.latitude.blank? or p.longitude.blank?)
                xml.Geolocation do
                  xml.Latitude p.latitude
                  xml.Longitude p.longitude
                  xml.Accuracy 8
                end
              end
              xml.ServiceCharge p.fees unless p.fees.blank?
              xml.Furnished p.property_inclusion.inclusion_furnished.to_i == 1 if p.property_inclusion.present? && p.property_inclusion.inclusion_furnished.present?
              xml.Floor p.level unless p.level.blank?
              xml.LandArea p.area_unit_id ==1 ? p.area_land.ceil : (p.area_land/10.76391041671).ceil unless p.area_land.blank?
              xml.ConstructionYear p.year_of_built unless p.year_of_built.blank?
              unless p.bathrooms.blank? && p.showerrooms.blank?
                if p.bathrooms.blank?
                  xml.Bathrooms p.showerrooms
                elsif p.showerrooms.blank?
                  xml.Bathrooms p.bathrooms
                else
                  xml.Bathrooms p.showerrooms
                end
              end

              ##============================================================##
              ##
              ##============================================================##
              xml.ExtendedFeatures do
                if p.availability_date.present?
                  # xml.Feature(:name => "Availability_Day", :value => p.availability_date.split('-')[2])
                  # xml.Feature(:name => "Availability_Month", :value => p.availability_date.split('-')[1])
                  # xml.Feature(:name => "Availability_Year", :value => p.availability_date.split('-')[0])
                end
                if p.property_complement.present?
                  xml.Feature(:name => "DPE", :value => p.property_complement.france_dpe_value) unless p.property_complement.france_dpe_value.blank?
                  xml.Feature(:name => "GES", :value => p.property_complement.france_ges_value) unless p.property_complement.france_ges_value.blank?
                end
                if  p.property_inclusion.present?
                  xml.Feature(:name => "AirConditioning", :value => "AirConditioning") if p.property_inclusion.inclusion_air_conditioning == 1
                  xml.Feature(:name => "Attic", :value => "Attic") if p.property_inclusion.indoor_attic == 1
                  xml.Feature(:name => "Cellar", :value => "Cellar") if p.property_inclusion.indoor_cellar == 1
                  xml.Feature(:name => "Equipments_Phone", :value => "Phone") if p.property_inclusion.service_phone == 1
                  xml.Feature(:name => "Equipments_TV", :value => "TV") if p.property_inclusion.service_tv == 1
                  xml.Feature(:name => "Garden_Type", :value => "Garden") if p.property_inclusion.exterior_garden == 1
                  xml.Feature(:name => "Internet", :value => "Internet") if p.property_inclusion.service_internet == 1
                  xml.Feature(:name => "Heating_Type", :value => "Electric") if p.property_inclusion.heating_electric == 1
                  xml.Feature(:name => "Heating_Type", :value => "Solar") if p.property_inclusion.heating_solar == 1
                  xml.Feature(:name => "Heating_Type", :value => "Gas") if p.property_inclusion.heating_gaz == 1
                  xml.Feature(:name => "Heating_Type", :value => "Fuel") if p.property_inclusion.heating_fuel == 1
                  xml.Feature(:name => "Heating_Type", :value => "HeatPump") if p.property_inclusion.heating_heat_pump == 1
                  xml.Feature(:name => "Heating_Type", :value => "UnderfloorHeating") if p.property_inclusion.heating_floor_heating == 1
                  xml.Feature(:name => "Kitchen_Equipment", :value => "Fridge") if p.property_inclusion.inclusion_fridge == 1
                  xml.Feature(:name => "Kitchen_Equipment", :value => "HotPlate") if p.property_inclusion.inclusion_cooker == 1
                  xml.Feature(:name => "Kitchen_Equipment", :value => "WasherDryer") if p.property_inclusion.inclusion_washer == 1
                  xml.Feature(:name => "Kitchen_Equipment", :value => "TumbleDryer") if p.property_inclusion.inclusion_dryer == 1
                  xml.Feature(:name => "Kitchen_Equipment", :value => "Microwave") if p.property_inclusion.inclusion_microwave == 1
                  xml.Feature(:name => "LaundryRoom", :value => "LaundryRoom") if p.property_inclusion.detail_laundry == 1
                  if p.property_inclusion.pets_not_allow == 1
                    xml.Feature(:name => "Pets", :value => "Refused")
                  elsif p.property_inclusion.pets_allow == 1
                    xml.Feature(:name => "Pets", :value => "Allowed")
                  end
                  xml.Feature(:name => "Security_Alarm", :value => "Alarm")           if p.property_inclusion.security_alarm == 1
                  xml.Feature(:name => "Security_Caretaker", :value => "Caretaker")   if p.property_inclusion.detail_janitor == 1
                  xml.Feature(:name => "Security_Guard", :value => "Guard")           if p.property_inclusion.security_guardian == 1
                  xml.Feature(:name => "Security_EntryPhone", :value => "Entryphone") if p.property_inclusion.security_intercom == 1
                  xml.Feature(:name => "SwimmingPool", :value => "SwimmingPool")      if p.property_inclusion.detail_inside_pool == 1 || p.property_inclusion.detail_outside_pool == 1
                  xml.Feature(:name => "SwimmingPool", :value => "Spa")               if p.property_inclusion.detail_spa == 1
                  xml.Feature(:name => "Terraces_Type", :value => "Veranda")          if p.property_inclusion.exterior_veranda == 1
                end
              end

              ##============================================================##
              ##
              ##============================================================##
              agent = p.property_contacts.joins(:property_contact_type).where(:property_contact_types =>{:slug => "agent"}).first
              xml.Contact do
                xml.SiteAccountId   p.user_id
                xml.CustomerType    p.user.profile.is_a_pro_user == 0 ? "Private" : "Professional"
                xml.CorporateName   p.user.profile.agency_name.present? ? p.user.profile.agency_name : "#{p.user.profile.first_name} #{p.user.profile.last_name}"
                xml.FirstName       p.user.profile.first_name                                     if p.user.profile.first_name.present?
                xml.LastName        p.user.profile.last_name                                      if p.user.profile.last_name.present?
                xml.LandPhone       p.user.profile.phone                                          if p.user.profile.phone.present?
                xml.MobilePhone     p.user.profile.phone_sms                                      if p.user.profile.phone_sms.present?
                xml.Email           p.user.profile.email                                          if p.user.profile.email.present?
                xml.AgentLandPhone  agent.phone                                                   if agent.present? and agent.phone.present?
                xml.AgentId         agent.id                                                      if agent.present?
                xml.AgentEmail      agent.email                                                   if agent.present? and agent.email.present?
                xml.Website         p.user.profile.website                                        if p.user.profile.website.present?
                xml.Address         p.user.profile.agency_address                                 if p.user.profile.agency_address.present?
                xml.PostalCode      p.user.profile.agency_zipcode                                 if p.user.profile.agency_zipcode.present?
                xml.City            p.user.profile.agency_city                                    if p.user.profile.agency_city.present?
                xml.Country         p.user.profile.country_alpha2
                xml.Photo           p.user.profile.picture.url(:large)                            if p.user.profile.picture?
                if p.user.profile.spoken_languages.present?
                  xml.SpokenLanguages do
                    p.user.profile.spoken_languages.split(',').each do |lang|
                      xml.Language lang
                    end
                  end
                end
              end
            end
            puts "End xml advert for property with id ##{p.id}"
          end
          xml.shareimmoStatistics do
            xml.updated_at Time.now.strftime("%Y-%m-%d %H:%M:%S")
            xml.ids ids
            xml.count ids.count
          end
        end
      end

      puts "Write xml file"
      File.open(listglobally_file, 'w') do |file|
        file.write builder.to_xml
      end
      if Rails.env.production?
        puts "Send it on ftp"
        Net::FTP.open('apps.shareimmo.com',ENV['apps_ftp_username'],ENV['apps_ftp_password']) do |ftp|
          ftp.passive = true
          ftp.putbinaryfile(listglobally_file)
        end
      end
      puts "New listglobally File Created #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
    rescue Exception => e
    end
  end

  ##============================================================##
  ## Création des backlinks
  ##============================================================##
  task :create_backlinks => :environment do
    VisibilityPortal.joins(:portal).where(:portals=>{:slug =>"listglobally"}).where(:backlink_url => nil).each do |v|
      v.update_attribute(:backlink_url,api_v2_international_url(:u =>v.property.user_id,:p =>v.property_id,:locale => nil)) if v.property.present?
    end
  end


end
