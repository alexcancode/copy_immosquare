# encoding: utf-8

require "#{Rails.root}/app/helpers/html_to_plain_text_helper"
require "#{Rails.root}/app/helpers/money_helper"
include HtmlToPlainTextHelper
include MoneyHelper

include ActionView::Helpers::SanitizeHelper


namespace :louer do

  task :scrapping => :environment do
    response = HTTParty.get("http://www.louer.com/feeds/props.xml?token=v8iuwerh98h4ne8hg8234234gh324ginet7ohi")
    @xml = Nokogiri::XML(response.body)
    portal_id = Portal.find_by_slug("louer").id
    @xml.css("prop").each do |prop|
      id = prop.css('old_lid').first.content
      shareimmo_id = prop.css('foreign_key').first.content.to_i
      puts "shareimmo ##{shareimmo_id} => #{id}"
      begin
        vp = VisibilityPortal.where(:portal_id =>portal_id ,:property_id =>shareimmo_id).last
        vp.update_attribute(:backlink_id,id) if vp.present?
      rescue ActiveRecord::RecordNotFound
        puts "not found"
      rescue Exception => e
        puts "Error #{e.message}"
      end
    end
  end



  task :xml => :environment do
    louer_path = File.join("tmp","apps")
    louer_file = File.join(louer_path,"louer.xml")
    FileUtils.mkdir_p(louer_path) unless File.exists?(louer_path)
    FileUtils.rm(louer_file) if File.exist?(louer_file)
    puts "Old louer File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
    response = HTTParty.get('http://api.shareimmo.com/api/v1/zipcodes/louer.json')
    @louercom_locations = JSON.parse(response.body)

    builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
      xml.updatedataset do
        ids = Array.new
        xml.props do
          Property.includes(:property_assets,:visibility_portals).where.not(:draft=>1).where(:status=>1,:visibility_portals => {:portal_id => Portal.where(:slug => "louer").first.id, :posting_status=>1}).where("visibility_portals.visibility_until > ?", Time.now).each do |p|
            if p.zipcode.present?
              louercom_location = check_louer_location(p.zipcode[0,3])
              louercom_region = check_louer_region(p.zipcode[0,3])
              @price_fr =  I18n.with_locale('fr'){p.property_listing.id == 1 ? humanized_money_with_symbol(p.price) : "#{humanized_money_with_symbol p.rent}#{humanized_frequency(p.rent_frequency)}"}
              @price_en =  I18n.with_locale('en'){p.property_listing.id == 1 ? humanized_money_with_symbol(p.price) : "#{humanized_money_with_symbol p.rent}#{humanized_frequency(p.rent_frequency)}"}
              ids << p.id
              xml.prop do
                xml.id
                xml.status        p.status
                xml.user_id       p.user_id
                xml.foreign_key   p.id
                xml.region_id     louercom_region
                xml.city_id       louercom_location
                xml.street_name   p.street_name
                xml.street_number p.street_number
                xml.area_code     p.zipcode.present? ?  p.zipcode.gsub(/\s+/, "") : ""
                xml.old_lid
                xml.unit_type p.property_classification.louercom_match
                xml.building_name_en "#{p.property_listing.name_en} - #{p.rooms} #{p.withHalf == 1 ? "½" : p.rooms == 1 ? "room" : "rooms" }  - #{@price_en.present? ? "#{@price_en}" : nil } #{p.locality.present? ? "- #{p.locality}" : nil }"
                xml.building_name_fr "#{p.property_listing.name_fr} - #{p.rooms} #{p.withHalf == 1 ? "½" : p.rooms == 1 ? "pièce" : "pièces" }  - #{@price_fr.present? ? "#{@price_fr}" : nil } #{p.locality.present? ? "- #{p.locality}" : nil }"
                xml.cnt1_fullname p.user.profile.public_name
                xml.cnt1_email p.user.profile.email
                xml.cnt1_phone p.user.profile.phone
                xml.cnt1_linkedin
                xml.cnt2_fullname nil
                xml.cnt2_email nil
                xml.cnt2_phone nil
                xml.cnt2_linkedin nil
                xml.cnt3_fullname nil
                xml.cnt3_email nil
                xml.cnt3_phone nil
                xml.cnt3_linkedin nil
                xml.sublease 0
                xml.rent_monthly_min p.rent_cents.to_s[0..-3]   if p.rent.present?
                xml.rent_monthly_max p.rent_cents.to_s[0..-3]  if p.rent.present?
                xml.rent_weekly_min nil
                xml.rent_weekly_max nil
                xml.rent_weekday_min nil
                xml.rent_weekday_max nil
                xml.rent_weekend_min nil
                xml.rent_weekend_max nil
                xml.rent_daily_min nil
                xml.rent_daily_max nil
                xml.rent_term 3
                xml.furnished p.property_inclusion.inclusion_furnished== 1 ? 1 : 0
                xml.luxury_unit 0
                xml.bedrooms_min p.bedrooms
                xml.bedrooms_max p.bedrooms
                xml.bathrooms_min p.bathrooms
                xml.bathrooms_max p.bathrooms
                xml.kitchen_fridge p.property_inclusion.inclusion_fridge== 1 ? 1 : 0
                xml.kitchen_stove 0
                xml.kitchen_microwave p.property_inclusion.inclusion_microwave== 1 ? 1 : 0
                xml.kitchen_dishwasher p.property_inclusion.inclusion_dishwasher== 1 ? 1 : 0
                xml.kitchen_garbage_disposal 0
                xml.laundry_washer p.property_inclusion.detail_laundry== 1 ? 1 : 0
                xml.laundry_dryer 0
                xml.laundry_in_premises 0
                xml.laundry_near_premises 0
                xml.available_date p.availability_date.to_time.strftime("%Y-%m-%d") if p.availability_date.present?
                xml.url p.property_url
                xml.facebook_link p.user.profile.facebook
                xml.linkedin_link p.user.profile.linkedin
                xml.twitter_link p.user.profile.twitter
                xml.videos do
                  p.videos.where(:posting_status =>1).each do |video|
                    xml.video generate_louer_video(video)
                  end
                end
                xml.built p.year_of_built
                xml.year_renovated
                xml.short_desc1_en
                xml.short_desc2_en
                xml.short_desc3_en
                xml.short_desc4_en
                xml.short_desc5_en
                xml.short_desc1_fr
                xml.short_desc2_fr
                xml.short_desc3_fr
                xml.short_desc4_fr
                xml.short_desc5_fr
                xml.description_en convert_to_text(text_without_html_length(p.description_en) == 0 ? p.description_fr : p.description_en)
                xml.description_fr convert_to_text(text_without_html_length(p.description_fr) == 0 ? p.description_en : p.description_fr)
                xml.air_conditioning p.property_inclusion.inclusion_air_conditioning== 1 ? 1 : 2
                xml.pets_allowed p.property_inclusion.pets_allow== 1 ? 1 : 2
                xml.number_floors_unit
                xml.number_stories_building p.building.present? ? p.building.number_of_levels : nil
                xml.number_unit_building p.building.present? ? p.building.number_of_units : nil
                xml.surface_property p.area_unit_id == 2 ? p.area_living.ceil : (p.area_living* 10.76).ceil if p.area_living.present?
                xml.parking_indoor p.property_inclusion.detail_inside_parking == 1 ? 1 : 0
                xml.parking_outdoor p.property_inclusion.detail_outside_parking == 1 ? 1 : 0
                xml.elevator_number p.property_inclusion.detail_elevator== 1 ? 1 : 0
                xml.flooring
                if p.property_inclusion.heating_gaz == 1
                  @heating = 1
                elsif p.property_inclusion.heating_electric == 1
                  @heating = 2
                elsif p.property_inclusion.heating_fuel == 1
                  @heating = 3
                else
                  @heating = 0
                end
                xml.heating @heating
                xml.outdoor_presence 0
                xml.ground_floor p.level== 1 ? 2 : 1
                xml.fireplace p.property_inclusion.indoor_fireplace == 1 ? 1 : 0
                xml.alarm_system p.property_inclusion.security_alarm == 1 ? 1 : 0
                xml.walk_in_closet p.property_inclusion.indoor_walk_in == 1 ? 1 : 0
                xml.internet_presence p.property_inclusion.service_internet == 1 ? 1 : 0
                if p.property_inclusion.detail_inside_pool == 1
                  xml.pool 1
                elsif p.property_inclusion.detail_outside_pool == 1
                  xml.pool 2
                elsif p.property_inclusion.detail_outside_pool == 0 and p.property_inclusion.detail_inside_pool == 0
                  xml.pool 3
                else
                  xml.pool 0
                end
                xml.exercice_facility p.property_inclusion.detail_gym == 1 ? 2 : 1
                xml.sauna p.property_inclusion.detail_sauna== 1 ? 2 : 1
                xml.handicap_access p.property_inclusion.accessibility_elevator == 1 ? 1 : 0
                xml.security_guard p.property_inclusion.detail_janitor== 1 ? 1 : 0
                xml.access_gate 0
                xml.storage_available 0
                xml.photos do
                  p.property_assets.each do |property_asset|
                    xml.photo property_asset.picture.url(:large,:timestamp => false)
                  end
                end
              end
            end
          end
        end
        xml.shareimmoStatistics do
          xml.updated_at Time.now.strftime("%Y-%m-%d %H:%M:%S")
          xml.ids ids
          xml.count ids.count
        end
      end
    end

    File.open(louer_file, 'w') do |file|
      file.write builder.to_xml
    end

    if Rails.env.production?
      Net::FTP.open('apps.shareimmo.com',ENV['apps_ftp_username'],ENV['apps_ftp_password']) do |ftp|
        ftp.passive = true
        ftp.putbinaryfile(louer_file)
      end
    end

    puts "New louer File Created #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"

  end

  private

  def text_without_html_length(text)
    return 0 if text.blank?
    strip_tags(text.gsub(/\s+/, "")).length
  end


  def is_number?(string)
    true if Float(string) rescue false
  end



  def check_louer_location(zipcode)
    @louercom_locations["louercom_location"].each do |louercom_location|
      if louercom_location[louercom_location.keys.first.to_s].include?(zipcode)
        return louercom_location.keys.first.to_s
      end
    end
    return false
  end

  def check_louer_region(zipcode)
    @louercom_locations["louercom_region"].each do |louercom_region|
      if louercom_region[louercom_region.keys.first.to_s].include?(zipcode)
        return louercom_region.keys.first.to_s
      end
    end
    return false
  end

  def generate_louer_video(video)
    video_obj = { :fr => { :type => "youtube", :link => "https://youtube.com/embed/#{video.youtube_id}" }, :en => {}}
    return video_obj.to_json
  end
end
