# encoding: utf-8

require "#{Rails.root}/app/helpers/html_to_plain_text_helper"

require "base64"
require 'net/ftp'
require 'rubygems'
require 'nokogiri'


namespace :padlister do

  task :xml => :environment do

  	begin

	  	@portal = Portal.where(:slug => "padlister").first
	  	raise "Portal not found" if @portal.nil?

	    next if Rails.env.beta?

	    puts "ENV => #{Rails.env}"
	    padlister_path = File.join("tmp","apps","padlister")
      	@padlister_file = File.join(padlister_path,"padlister.xml")

	    FileUtils.rm_rf(padlister_path)
	    FileUtils.mkdir_p(padlister_path) unless File.exists?(padlister_path)
	
	    puts "Old padlister File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"

	  	@properties = Property.includes(:property_assets,:visibility_portals).where.not(:draft=>1).where(:visibility_portals => { :portal_id => @portal.id }).where("visibility_portals.visibility_until > ?", Time.now)

	    ##============================================================##
	    ## Création du fichier xml
	    ##============================================================##
	    builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
	    	xml.PadMapperFeed(:version => "1.01") do

			  	@properties.each do |p|
			  		@visibility = VisibilityPortal.where(:portal_id => @portal.id, :property_id => p.id, :posting_status => 1).where("visibility_until >= :date", { :date => Time.now }).first
			  		raise "no visibility" if @visibility.nil?

			  		xml.Listing do

  		  			    # Required

			  			xml.ID p.id

			  			@price = ( p.property_listing == 1 ? humanized_money(p.price) : humanized_money(p.rent) )
			  			xml.Price @price.to_s.gsub(",",".").gsub(" ", "")

			  			xml.Bedrooms p.bedrooms

			  			xml.FullBathrooms ( p.withHalf == 1 ? 1 : 0 )

			  			xml.CatsAllowed ( p.property_inclusion.pets_cat.blank? ? 0 : 1 )
			  			xml.LargeDogsAllowed ( p.property_inclusion.pets_dog.blank? ? 0 : 1 )
			  			xml.SmallDogsAllowed ( p.property_inclusion.pets_little_dog.blank? ? 0 : 1 )

			  			xml.Lister ( p.user.profile.is_a_pro_user == 1 ? "BrokerWithFee" : "Owner" )

			  			case p.property_classification.api_id
				  			when 201, 208 
				  				@p_type = "Apartment"
				  			when 101, 102, 103, 104, 105, 106, 114, 115, 116
				  				@p_type = "House"
				  			when 207
				  				@p_type = "Loft"
				  			when 210
				  				@p_type = "Condo"
				  			when 114, 115
				  				@p_type = "Townhouse"

				  			when 101
				  				@p_type = "Duplex"
				  			else
				  				@p_type = "House"
			  			end
			  				
			  			xml.PropertyType @p_type

			  			xml.ContactName p.user.profile.public_name
			  			xml.ContactEmail "portals_padlister_#{p.user.id}@shareimmo.com"
			  			xml.AnonymizeAddress "True" unless p.street_number.present?
			  			xml.StreetAddress ( p.street_name.present? ? "#{p.street_number}, #{p.street_name}" : "" )
			  			xml.City ( p.locality.present? ? p.locality : "" )
			  			xml.State ( p.administrative_area_level_2.present? ? p.administrative_area_level_2 : "" )
			  			xml.Country ( p.country_short.present? ? p.country_short : "" )
			  			xml.PostalCode ( p.zipcode.present? ? p.zipcode : "" )
			  			xml.Description ( p.description_fr.present? ? Nokogiri::HTML(p.description_fr).text : Nokogiri::HTML(p.description_en).text )

			  			xml.Photos do
				  			p.property_assets.each_with_index do |pa, index|
					  			xml.Photo do
						  			xml.URL pa.picture.url(:large,:timestamp => false)	  			
					  			end
					  		end
			  			end

			  			# Optional
			  			xml.ContactPhone ( p.user.profile.phone_visibility == 1 && p.user.profile.phone.present? ? p.user.profile.phone : "" )

			  			xml.MinLeaseLength 1
			  			xml.MinLeaseUnits "Month"

			  			xml.Unit p.unit

			  			xml.DateAvailable ( p.availability_date.present? ? p.availability_date.strftime("%m/%d/%Y") : "" )
			  			# xml.Deposit

			  			xml.SquareFootage p.area_living

			  			@parking = "Carport" if 1 == 2
			  			@parking = "Street Parking" if p.property_inclusion.detail_parking_on_street == 1
			  			@parking = "Garage" if p.property_inclusion.detail_inside_parking == 1
			  			@parking = "Parking Spot" if p.property_inclusion.detail_outside_parking == 1

			  			xml.Parking @parking
			  			# xml.CommunityName
			  			# xml.NeighborhoodName

			  			xml.Amenities do
			  				@amenities = []
			  				@amenities << { :name => "Air conditioning", :value => p.property_inclusion.inclusion_air_conditioning } if p.property_inclusion.inclusion_air_conditioning == 1
			  				@amenities << { :name => "Hot Water", :value => p.property_inclusion.inclusion_hot_water } if p.property_inclusion.inclusion_hot_water == 1
			  				@amenities << { :name => "Heated", :value => p.property_inclusion.inclusion_heated } if p.property_inclusion.inclusion_heated == 1
			  				@amenities << { :name => "Electricity", :value => p.property_inclusion.inclusion_electricity } if p.property_inclusion.inclusion_electricity == 1
			  				@amenities << { :name => "Furnished", :value => p.property_inclusion.inclusion_furnished } if p.property_inclusion.inclusion_furnished == 1
			  				@amenities << { :name => "Fridge", :value => p.property_inclusion.inclusion_fridge } if p.property_inclusion.inclusion_fridge == 1
			  				@amenities << { :name => "Cooker", :value => p.property_inclusion.inclusion_cooker } if p.property_inclusion.inclusion_cooker == 1
			  				@amenities << { :name => "Dishwasher", :value => p.property_inclusion.inclusion_dishwasher } if p.property_inclusion.inclusion_dishwasher == 1
			  				@amenities << { :name => "Dryer", :value => p.property_inclusion.inclusion_dryer } if p.property_inclusion.inclusion_dryer == 1
			  				@amenities << { :name => "Washer", :value => p.property_inclusion.inclusion_washer } if p.property_inclusion.inclusion_washer == 1
			  				@amenities << { :name => "Microwave", :value => p.property_inclusion.inclusion_microwave } if p.property_inclusion.inclusion_microwave == 1
			  				@amenities << { :name => "Elevator", :value => p.property_inclusion.detail_elevator } if p.property_inclusion.detail_elevator == 1
			  				@amenities << { :name => "Laundry", :value => p.property_inclusion.detail_laundry } if p.property_inclusion.detail_laundry == 1
			  				@amenities << { :name => "Garbage chute", :value => p.property_inclusion.detail_garbage_chute } if p.property_inclusion.detail_garbage_chute == 1
			  				@amenities << { :name => "Common space", :value => p.property_inclusion.detail_common_space } if p.property_inclusion.detail_common_space == 1
			  				@amenities << { :name => "Janitor", :value => p.property_inclusion.detail_janitor } if p.property_inclusion.detail_janitor == 1
			  				@amenities << { :name => "Gym", :value => p.property_inclusion.detail_gym } if p.property_inclusion.detail_gym == 1
			  				@amenities << { :name => "Golf", :value => p.property_inclusion.detail_golf } if p.property_inclusion.detail_golf == 1
			  				@amenities << { :name => "Tennis", :value => p.property_inclusion.detail_tennis } if p.property_inclusion.detail_tennis == 1
			  				@amenities << { :name => "Sauna", :value => p.property_inclusion.detail_sauna } if p.property_inclusion.detail_sauna == 1
			  				@amenities << { :name => "Spa", :value => p.property_inclusion.detail_spa } if p.property_inclusion.detail_spa == 1
			  				@amenities << { :name => "Inside pool", :value => p.property_inclusion.detail_inside_pool } if p.property_inclusion.detail_inside_pool == 1
			  				@amenities << { :name => "Outside pool", :value => p.property_inclusion.detail_outside_pool } if p.property_inclusion.detail_outside_pool == 1
			  				# ...

			  				@amenities.each do |a|
			  					xml.Amenity a[:name]
			  				end


			  			end

			  		end

		  		end

		  	end





	    end

      	##============================================================##
      	## Export du fichier
      	##============================================================##
      	File.open(@padlister_file, 'w') do |file|
        	file.write builder.to_xml
      	end

      	##============================================================##
      	## Tranfert
      	##============================================================##

       	Net::FTP.open("apps.shareimmo.com", ENV["apps_ftp_username"], ENV["apps_ftp_password"]) do |ftp|
            ftp.passive = true
       		ftp.putbinaryfile(@padlister_file)
       		puts "New padlister File Created #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
      	end
      	FileUtils.rm(@padlister_file)



  	rescue Exception => e
  		JulesLogger.info e.message
  	end

  end
end
