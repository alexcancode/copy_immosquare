namespace :feedbacks do
  namespace :alerts do
    task :execute => :environment do
      puts "feedbacks:alerts:execute - Started"
      @feedbacks_email = Feedback.where("visit_at > ? and visit_at < ? and reminder_email = 1",Time.now+1.day, Time.now+1.day+1.hour)
      @feedbacks_sms = Feedback.where("visit_at > ? and visit_at < ? and reminder_phone = 1",Time.now+2.hour, Time.now+3.hour)

      @feedbacks_email.each do |feedback|
        ReportMailer.mail_reminder(feedback).deliver_later
      end


      @feedbacks_sms.each do |feedback|
        @phone = validate_phone(feedback.phone)
        @client = Twilio::REST::Client.new ENV['twilio_sid'], ENV['twilio_token']
        @client.messages.create(
          :from => ENV['twilio_number'],
          :to => @phone,
          :body => I18n.t("mailer.feedback_alert.sms_content", :sms_alert => feedback.help_sms, :visite_date => feedback.visit_at, :address => feedback.property.address),
          )
      end

    end
  end
end

def validate_phone(phone)
  PhonyRails.normalize_number(phone, :default_country_code => 'CA')
end
