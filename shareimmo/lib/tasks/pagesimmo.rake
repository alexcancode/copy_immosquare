# encoding: utf-8
require "open-uri"

namespace :pagesimmo do

  # Flow
  # 0) Update portals                       => portals:update
  # 1) Create agency accounts               => agencies:publish
  # 2) Add subscriptions in server          => agencies:subscriptions
  # 3) Publish properties                   => properties:publish
  # 4) Set diffusions                       => properties:set_diffusions

  namespace :portals do


    ##============================================================##
    ## Function to get all existing portals and add them in db
    ## Add redis var to list all portail ids for Pagesimmo
    ##============================================================##
    task :update => :environment do
      @pagesimmo_portals_set = get_portals_key()

      puts "pagesimmo:portals:update - started"
      begin
        ##============================================================##
        ## Authentication with webservice
        ## If auth_token is empty, bad connection
        ##============================================================##
        client = Savon.client(wsdl: ENV['pagesimmo_soap_host'])
        response = client.call(:authentification, message: { "_agence_Login" => ENV["pagesimmo_main_account_login"], "_agence_Pwd" => ENV["pagesimmo_main_account_password"], "_editeur_Key" => ENV["pagesimmo_soap_editor_key"] } )

        auth_token = response.body[:authentification_response][:authentification_result]

        raise "Bad connection" if auth_token.nil?

        ##============================================================##
        ## Request to get all portals
        ## If bad xml, error
        ## else for each server, install it if not present
        ##============================================================##
        response = client.call(:get_liste_serveurs, message: { "_auth_Token" => auth_token , "_auth_Login" => ENV["pagesimmo_main_account_login"] })

        servers_list_response = response.body[:get_liste_serveurs_response][:get_liste_serveurs_result]

        raise "Bad response" if servers_list_response.nil?

        servers_list_xml = Nokogiri::XML(servers_list_response)
        JulesLogger.info servers_list_response

        servers_list_xml.css("Serveur").each do |server|
          ##============================================================##
          ## Check if portal exist
          ## If yes, update it
          ## If no, create and send an email
          ## info : with_user_connexion = 2 => access_key (1 = login - password, 2 => access_key, 0 => free)
          ##============================================================##
          portal = Portal.where(:url => server.css("Nom").first.content).first

          if portal.present?
            portal.update_attributes(:access_key => server.css("Cle").first.content)
            puts "portal #{portal.name} updated"
          else
            name = server.css("Nom").first.content.gsub('http://www.', '').gsub('https:', '').gsub('/', '')
            slug = name.downcase
            Portal.create(:url =>server.css("Nom").first.content, :name => name,  :slug => slug, :status => 1, :portal_type_id => 3, :country => "fr", :with_user_connexion => 2, :access_key => server.css("Cle").first.content)
            puts "portal #{server.css("Nom").first.content} added"
            BackOfficeMailer.new_portal_added(portal, "pagesimmo").deliver_now
          end
          @pagesimmo_portals_set << portal.id

        end

      rescue Exception => e
        puts e.message
      end

      puts "pagesimmo:portals:update - ended"

    end
  end


  namespace :agencies do

    ##============================================================##
    ## Activate accounts :
    ##============================================================##
    task :publish => :environment do
      @users = get_users_pagesimmo()

      puts @users.pluck(:id)

      # next if Rails.env.beta?
      puts "ENV => #{Rails.env}"
      pagesimmo_path = File.join(Rails.root,"tmp","apps","pagesimmo")
      pagesimmo_file = File.join(pagesimmo_path,"00000098.CryptML")
      FileUtils.mkdir_p(pagesimmo_path) unless File.exists?(pagesimmo_path)
      FileUtils.rm(pagesimmo_file)      if File.exist?(pagesimmo_file)
      puts "Old pagesimmo File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
      ##============================================================##
      ## Création du fichier xml
      ##============================================================##
      builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
        xml.ROOT("xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema", "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns" => "http://tempuri.org/CryptML.xsd") do
          xml.CRYPTML 1.0
          xml.TRANSFERT "Backoffice"
          xml.EMETTEUR do
            xml.REFERENCE 98
            xml.LIBELLE "Immosquare"
          end
          xml.DESTINATAIRE do
            xml.REFERENCE "PagesIMMO"
            # User.includes(:profile).where(:profiles => {:country => ["FR","LU","BE"]}).each do |user|
            @users.each do |user|
              xml.AGENCE do
                xml.REFERENCE user.id
                xml.REFDISTANTE  crypto_id(user.id)
                xml.COORDONNEES do
                  xml.RAISON_SOCIALE ( user.profile.agency_name.blank? ? crypto_id(user.id) : user.profile.agency_name )
                  # xml.ATTENTION
                  # xml.GENRE
                  xml.NOM user.profile.last_name if user.profile.last_name.present?
                  xml.PRENOM user.profile.first_name if user.profile.first_name.present?
                  # xml.NUM_RUE
                  # xml.NOM_RUE
                  xml.ADRESSE1 ( user.profile.agency_address.present? ? user.profile.agency_address : "sandbox" )
                  # xml.ADRESSE2 user.profile.agency_address if user.profile.agency_address.present?
                  xml.CP ( user.profile.agency_zipcode.present? ? user.profile.agency_zipcode : "cp" )
                  xml.VILLE ( user.profile.agency_city.present? ? user.profile.agency_city : "city" )
                  xml.PAYS ( user.profile.country_name.present? ? user.profile.country_name : "country" )
                  # xml.TELEPHONE_DOMICILE
                  xml.TELEPHONE_BUREAU "#{user.profile.international_prefix} #{user.profile.phone}" if user.profile.phone.present?
                  xml.TELEPHONE_PORTABLE "#{user.profile.international_prefix} #{user.profile.phone_sms}" if user.profile.phone_sms.present?
                  # xml.TELECOPIE
                  xml.EMAIL "shareimmo_#{crypto_id(user.id)}@shareimmo.com"
                  xml.URL user.profile.website if user.profile.website.present?
                end
                # xml.CARTE_T
                # xml.CARTE_G
                # xml.SIRET
                xml.BACKOFFICE do
                  xml.CONSIGNES do
                    xml.GESTION 0
                    xml.REPERTOIRE_CLIENT crypto_id(user.id)
                    xml.MOT_DE_PASSE "shareimmo_#{crypto_id(user.id)}"
                    xml.CODE_PUBLICATION 0
                    xml.CODE_CATEGORIE 0
                    xml.LISTES_DIFFUSION
                    xml.SERVICES "A"
                  end
                end
              end
            end
          end
        end
      end

      ##============================================================##
      ## Compilation du fichier xml
      ##============================================================##
      File.open(pagesimmo_file, 'w') do |file|
        file.write builder.to_xml
      end

      ##============================================================##
      ## Validation du fichier xml
      ##============================================================##
      xsd = Nokogiri::XML::Schema(open("http://www.cryptml.net/xsd-repository/CryptML.xsd"))
      doc = Nokogiri::XML(File.read(pagesimmo_file))
      xsd.validate(doc).each do |error|
        puts error.message
        raise error.message
      end


      ##============================================================##
      ## Transfert du fichier XML
      ##============================================================##
      # if Rails.env.production?
        begin
          Net::FTP.open(ENV["pagesimmo_ftp_host"], ENV["pagesimmo_ftp_login"], ENV["pagesimmo_ftp_password"]) do |ftp|
            ftp.putbinaryfile(pagesimmo_file)
            ftp.putbinaryfile(pagesimmo_file, "#{File.basename(pagesimmo_file)}")
            puts "New pagesimmo File Transfered #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
          end
        rescue Exception => e
          puts "Error #{e.message}"
        end
      # end
    end


    task :subscriptions => :environment do
      begin
        @users = get_users_pagesimmo()
        @portals_arr = get_portals_key().members

        # For each user (agency), subscribe portals
        @users.each do |user|
          puts "subscribe #{user.id}"

          auth_Login = crypto_id(user.id)
          auth_Password = "shareimmo_#{auth_Login}"

          ##============================================================##
          ## Authentication with webservice
          ## If auth_token is empty, bad connection
          ##============================================================##
          client = Savon.client(wsdl: ENV['pagesimmo_soap_host'])
          response = client.call(:authentification, message: { "_agence_Login" => auth_Login, "_agence_Pwd" => auth_Password, "_editeur_Key" => ENV["pagesimmo_soap_editor_key"] } )

          auth_Token = response.body[:authentification_response][:authentification_result]


          raise "Bad connection" if auth_Token.nil?

          @properties = get_properties_for_user(user)

          portals = []

          VisibilityPortal.where(:portal_id => @portals_arr, :property_id => @properties.pluck(:id)).each do |visi|

            # Register user for portal
            portals << visi.portal_id unless portals.include?(visi.portal_id)
          end
          puts "#{user.id} - #{portals}"

          Portal.where(:id => portals).each do |portal|
            puts "Subscribe portal #{portal.name} - #{portal.access_key}"

            response = client.call(:set_inscrire_agence, message: { "_auth_Token" => auth_Token, "_auth_Login" => auth_Login, "_CleServeur" => portal.access_key, "_ReferenceServeur" => crypto_id(user.id)} )

            response_obj = Hash.from_xml(response.body[:set_inscrire_agence_response][:set_inscrire_agence_result])

            JulesLogger.info response_obj

            if response_obj["Document"]["Error"].present?
              puts response_obj["Document"]["Error"]["Text"]
            elsif response_obj["Document"]["set_InscrireAgence"].present? && response_obj["Document"]["set_InscrireAgence"]["OK"].present?
              puts "subscription ok"
            else
              puts "Unknow error"
            end

          end



        end
      rescue Exception => e
        puts e.message
      end

    end
  end

  namespace :properties do

    task :publish => :environment do
      @users = get_users_pagesimmo()

      # next if Rails.env.beta?

      pagesimmo_path    = File.join(Rails.root,"tmp","apps","pagesimmo")

      @eu_bank = EuCentralBank.new()
      @eu_bank.update_rates

      # User.includes(:profile).where(:profiles => {:country => ["FR","LU","BE"]}).each do |user|
      @users.each do |user|
        @profile = user.profile
        pictures_arr = Array.new
        pagesimmo_file    = File.join(pagesimmo_path,"#{crypto_id(user.id)}.CryptML")
        pagesimmo_zip     = File.join(pagesimmo_path,"#{crypto_id(user.id)}.CryptML.zip")
        FileUtils.mkdir_p(pagesimmo_path)   unless File.exists?(pagesimmo_path)
        FileUtils.rm(pagesimmo_file)        if File.exist?(pagesimmo_file)
        FileUtils.rm(pagesimmo_zip)         if File.exist?(pagesimmo_zip)
        puts "Old pagesimmo File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
        @properties = get_properties_for_user(user)
        JulesLogger.info @properties.pluck(:id)


        ##============================================================##
        ## Création du fichier xml
        ##============================================================##
        builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
          xml.ROOT("xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema", "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns" => "http://tempuri.org/CryptML.xsd") do
            xml.CRYPTML "1.2.3"
            xml.TRANSFERT "Publication"
            xml.EMETTEUR do
              xml.REFERENCE crypto_id(user.id)
              xml.LIBELLE @profile.public_name
              xml.COORDONNEES do
                xml.RAISON_SOCIALE @profile.agency_name
                # xml.ATTENTION
                # xml.GENRE
                xml.NOM @profile.last_name if @profile.last_name.present?
                xml.PRENOM @profile.first_name if @profile.first_name.present?
                # xml.NUM_RUE
                # xml.NOM_RUE
                xml.ADRESSE1 @profile.agency_address if @profile.agency_address.present?
                # xml.ADRESSE2 @profile.agency_address if @profile.agency_address.present?
                xml.CP @profile.agency_zipcode if @profile.agency_zipcode.present?
                xml.VILLE @profile.agency_city if @profile.agency_city.present?
                xml.PAYS @profile.country_name if @profile.country_name.present?
                # xml.TELEPHONE_DOMICILE
                xml.TELEPHONE_BUREAU "#{@profile.international_prefix} #{@profile.phone}" if @profile.phone.present?
                xml.TELEPHONE_PORTABLE "#{@profile.international_prefix} #{@profile.phone_sms}" if @profile.phone_sms.present?
                # xml.TELECOPIE
                xml.EMAIL "shareimmo_#{crypto_id(user.id)}@shareimmo.com"
                xml.URL @profile.website if @profile.website.present?
              end

              xml.LOGICIEL do
                xml.EDITEUR "IMMOSQUARE"
                xml.NOM "shareIMMO"
                xml.VERSION "1.0.0"
              end
            end
            xml.DESTINATAIRE do
              xml.REFERENCE "PagesIMMO"
              xml.AGENCE do
                xml.REFERENCE crypto_id(user.id)
                xml.REFDISTANTE crypto_id(user.id)
                xml.COORDONNEES do
                  xml.RAISON_SOCIALE user.profile.agency_name
                  # xml.ATTENTION
                  # xml.GENRE
                  xml.NOM user.profile.last_name
                  xml.PRENOM user.profile.first_name
                  # xml.NUM_RUE
                  # xml.NOM_RUE
                  xml.ADRESSE1 user.profile.agency_address if user.profile.agency_address.present?
                  # xml.ADRESSE2 user.profile.agency_address if user.profile.agency_address.present?
                  xml.CP user.profile.agency_zipcode if user.profile.agency_zipcode.present?
                  xml.VILLE user.profile.agency_city if user.profile.agency_city.present?
                  xml.PAYS user.profile.country_name if user.profile.country_name.present?
                  # xml.TELEPHONE_DOMICILE
                  xml.TELEPHONE_BUREAU "#{user.profile.international_prefix} #{user.profile.phone}" if user.profile.phone.present?
                  xml.TELEPHONE_PORTABLE "#{user.profile.international_prefix} #{user.profile.phone_sms}" if user.profile.phone_sms.present?
                  # xml.TELECOPIE
                  xml.EMAIL "shareimmo_#{crypto_id(user.id)}@shareimmo.com"
                  xml.URL user.profile.website if user.profile.website.present?
                end
                # xml.CARTE_T
                # xml.CARTE_G
                xml.RESUME_AGENCE do
                  xml.NB_BIEN @properties.count
                end
                @properties.each do |property|


                  next if property.status < 1

                  @inclu = property.property_inclusion || nil
                  @country = ISO3166::Country.new(property.country_short)
                  @geocoded_address = Geocoder.search("#{property.locality.to_s}, #{property.country_short}").first


                  # Price is mandatory
                  # next if property.price.nil?


                  # Zip code is mandatory
                  # next if property.zipcode.nil?

                  xml.BIEN do
                    xml.REFERENCE property.id
                    xml.REFDISTANTE property.id
                    xml.CONSIGNES_BIEN do
                      xml.DIFFUSION_SUR_SERVEURS do
                        property.visibility_portals.each do |visi|
                          xml.DIFFUSION_SUR_SERVEUR do
                            xml.CLE visi.portal.access_key
                          end if visi.portal.access_key.present?
                        end
                      end
                    end
                    xml.MONNAIE do
                      xml.CODE_ISO property.currency
                      xml.CHANGE_VERS_EUR 1.00/@eu_bank.get_rate("EUR",property.currency).to_f
                    end
                    xml.DATE_CREATION property.created_at.strftime("%Y-%m-%d%:z")
                    xml.DATE_FRAICHEUR property.updated_at.strftime("%Y-%m-%d%:z")
                    xml.DATE_PARUTION property.created_at.strftime("%Y-%m-%d%:z")
                    xml.ETAT_GENERAL (property.property_status_id == 1 ? 1 : 5 ) if property.property_status_id.present?
                    xml.ACCROCHE property.property_flag.name if property.property_flag_id.present?
                    xml.DE_PLUS
                    xml.DIAGNOSTICS do
                      xml.DPE_EMISSIONS_GES do
                        xml.DONNEES do
                          xml.LETTRE ( property.property_complement.present? && property.property_complement.france_ges_indice.present? ? property.property_complement.france_ges_indice : "C"  )
                          xml.VALEUR ( property.property_complement.present? && property.property_complement.france_ges_value.present? ? property.property_complement.france_ges_value.to_f : 15.to_f  )
                        end
                      end
                      xml.DPE_CONSOMMATIONS_ENERGIE do
                        xml.DONNEES do
                          xml.LETTRE ( property.property_complement.present? && property.property_complement.france_dpe_indice.present? ? property.property_complement.france_dpe_indice : "C"  )
                          xml.VALEUR ( property.property_complement.present? && property.property_complement.france_dpe_value.present? ? property.property_complement.france_dpe_value.to_f : 140.to_f  )
                        end
                      end
                    end
                    xml.ANCIENNETE (property.property_status_id == 1 ? 1 : 3 ) if property.property_status_id.present?

                    if property.property_listing_id == 1
                      xml.VENTE do
                        xml.NEGOCIATEUR @profile.public_name
                        # xml.NEGOCIATEUR_DETAILS do
                        #   xml.EMAIL @profile.email
                        # end
                        xml.TEXTES do
                          xml.TITRE_FR property.title_fr.present? ? property.title_fr : property.address
                          xml.DESCRIPTION_FR convert_to_text(property.description_fr) if property.description_fr.present?
                          xml.TITRE_EN property.title_en if property.title_en.present?
                          xml.DESCRIPTION_EN convert_to_text(property.description_en) if property.description_en.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "de"
                            xml.TITRE property.title_de
                            xml.DESCRIPTION convert_to_text(property.description_de)
                          end if property.title_de.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "es"
                            xml.TITRE property.title_es
                            xml.DESCRIPTION convert_to_text(property.description_es)
                          end if property.title_es.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "it"
                            xml.TITRE property.title_it
                            xml.DESCRIPTION convert_to_text(property.description_it)
                          end if property.title_it.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "nl"
                            xml.TITRE property.title_nl
                            xml.DESCRIPTION convert_to_text(property.description_nl)
                          end if property.title_nl.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "pt"
                            xml.TITRE property.title_pt
                            xml.DESCRIPTION convert_to_text(property.description_pt)
                          end if property.title_pt.present?
                        end
                        xml.PRIX property.price.present? ? property.price.to_f : 1.to_f
                        # xml.TEXTE_PRIX
                        xml.TAXE_HABITATION property.tax_1.to_f if property.tax_1.present?
                        xml.TAXE_FONCIERE property.tax_2.to_f if property.tax_2.present?
                        xml.CHARGES_MENSUELLES property.fees.to_f if property.fees.present?
                      end
                    else
                      xml.LOCATION do
                        xml.INTERLOCUTEUR ( @profile.public_name.present? ? @profile.public_name : "" )
                        xml.INTERLOCUTEUR_DETAILS do
                          xml.EMAIL "shareimmo_#{crypto_id(user.id)}@shareimmo.com"
                        end
                        xml.TEXTES do
                          xml.TITRE_FR property.title_fr.present? ? property.title_fr : property.address
                          xml.DESCRIPTION_FR convert_to_text(property.description_fr) if property.description_fr.present?
                          xml.TITRE_EN property.title_en if property.title_en.present?
                          xml.DESCRIPTION_EN convert_to_text(property.description_en) if property.description_en.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "de"
                            xml.TITRE property.title_de
                            xml.DESCRIPTION convert_to_text(property.description_de)
                          end if property.title_de.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "nl"
                            xml.TITRE property.title_nl
                            xml.DESCRIPTION convert_to_text(property.description_nl)
                          end if property.title_nl.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "es"
                            xml.TITRE property.title_es
                            xml.DESCRIPTION convert_to_text(property.description_es)
                          end if property.title_es.present?
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "it"
                            xml.TITRE property.title_it
                            xml.DESCRIPTION convert_to_text(property.description_it)
                          end
                          xml.AUTRE_LANGUE do
                            xml.CULTURE "pt"
                            xml.TITRE property.title_pt
                            xml.DESCRIPTION convert_to_text(property.description_pt)
                          end if property.title_pt.present?
                        end
                        xml.LOYER_MENSUEL_TTC ( property.rent.present? ? property.rent.to_f : 1 )
                        xml.PERIODE_LOYER property.rent_frequency
                        xml.TAXE_HABITATION property.tax_1 if property.tax_1.present?
                        # xml.DEPOT_GARANTIE
                        # xml.DUREE_BAIL
                        xml.DISPONIBLE_LE property.availability_date.strftime("%Y-%m-%d%:z") if property.availability_date.present?
                      end
                    end
                    case property.property_classification.property_classification_category_id
                    when 1,6
                      xml.MAISON do
                        category_maison = case property.property_classification.api_id
                        when 101,102,103,105,106,110,114,116,117,118
                          1
                        when 104,109,111
                          9
                        when 107,108
                          7
                        when 113
                          11
                        when 115
                          2
                        when 601,602
                          5
                        when 603,604
                          6
                        else
                          0
                        end
                        xml.CATEGORIE_MAISON category_maison
                        xml.ETAT_INTERIEUR 4
                        # xml.SURFACE_TERRAIN (property.area_unit_id == 1 ? property.area_land.to_f : (property.area_land/10.76391041671).to_f) if property.area_land.present?
                        xml.SURFACE_HABITABLE property.area_living.present? ? (property.area_unit_id == 1 ? property.area_living.to_f : (property.area_living/10.76391041671).to_f) : 1.to_f
                        xml.ANNEE_CONSTRUCTION property.year_of_built if property.year_of_built.present?
                        xml.NB_ETAGES property.level if property.level.present?
                        xml.NB_PIECES property.rooms.present? ? property.rooms : 0
                        xml.NB_CHAMBRES property.bedrooms if property.bedrooms.present?
                        xml.NB_SDB property.bathrooms if property.bathrooms.present?
                        xml.NB_SDE property.showerrooms if property.showerrooms.present?
                        xml.NB_GARAGES @inclu.detail_garagebox == 1 ? 1 : 0
                        xml.NB_PARKINGS @inclu.detail_inside_parking == 1 ? 1 : 0
                        xml.NB_BALCONS (@inclu.exterior_back_balcony == 1 && @inclu.exterior_front_balcony == 1 ) ? 2 :  ((@inclu.exterior_back_balcony == 1 && @inclu.exterior_front_balcony == 1 ) ? 1 : 0 )
                        xml.NB_TERRASSES @inclu.exterior_terrace if @inclu.exterior_terrace.present?
                        xml.NB_CAVES @inclu.indoor_cellar if @inclu.indoor_cellar.present?
                        xml.CUISINE @inclu.half_furnsihed_kitchen == 1 ? 3 : 1
                        # xml.CONSTRUCTION
                        # xml.MITOYENNETE
                        # xml.STANDING
                        xml.MEUBLE @inclu.inclusion_furnished.present? ? 1 : 0
                        xml.CHAUFFAGE do
                          xml.SOURCE 0
                          xml.APPAREIL 0
                          if @inclu.heating_electric == 1
                            source_energie  =  3
                          elsif @inclu.heating_solar == 1
                            source_energie  = 5
                          elsif @inclu.heating_gaz == 1
                            source_energie = 1
                          elsif @inclu.heating_condensation == 1
                            source_energie = 5
                          elsif @inclu.heating_fuel == 1
                            source_energie = 2
                          elsif @inclu.heating_heat_pump == 1
                            source_energie = 6
                          else
                            source_energie  = 0
                          end
                          xml.ENERGIE source_energie
                        end
                        # xml.COUVERTURE
                        # xml.EXPOSITION
                        # xml.EAU_CHAUDE
                        xml.PISCINE (@inclu.detail_inside_pool == 1 || @inclu.detail_outside_pool == 1)  ?  1 : 0
                        xml.ACCES_HANDICAPE (@inclu.accessibility_elevator == 1 || @inclu.accessibility_wider_corridors == 1 ) ? 1 : 0
                      end
                    when 2,3,4,5,7,8
                      xml.APPARTEMENT do
                        xml.CATEGORIE_APPARTEMENT 1
                        xml.ETAT_INTERIEUR 4
                        # xml.SURFACE_TERRAIN (property.area_unit_id == 1 ? property.area_land.to_f : (property.area_land/10.76391041671).to_f) if property.area_land.present?
                        xml.SURFACE_HABITABLE property.area_living.present? ? (property.area_unit_id == 1 ? property.area_living.to_f : (property.area_living/10.76391041671).to_f) : 1.to_f
                        xml.ANNEE_CONSTRUCTION property.year_of_built.to_i if property.year_of_built.present?
                        xml.NB_ETAGES property.level if property.level.present?
                        xml.NB_PIECES property.rooms.present? ? property.rooms : 0
                        xml.NB_CHAMBRES property.bedrooms if property.bedrooms.present?
                        xml.NB_SDB property.bathrooms if property.bathrooms.present?
                        xml.NB_SDE property.showerrooms if property.showerrooms.present?
                        xml.NB_GARAGES @inclu.detail_garagebox == 1 ? 1 : 0
                        xml.NB_PARKINGS @inclu.detail_inside_parking == 1 ? 1 : 0
                        xml.NB_BALCONS (@inclu.exterior_back_balcony == 1 && @inclu.exterior_front_balcony == 1 ) ? 2 :  ((@inclu.exterior_back_balcony == 1 && @inclu.exterior_front_balcony == 1 ) ? 1 : 0 )
                        xml.NB_TERRASSES @inclu.exterior_terrace == 1 ? 1 : 0
                        xml.NB_CAVES @inclu.indoor_cellar == 1 ? 1 : 0
                        xml.CUISINE @inclu.half_furnsihed_kitchen == 1 ? 3 : 1
                        # xml.STANDING
                        # xml.EXPOSITION
                        xml.CHAUFFAGE do
                          xml.SOURCE 0
                          xml.APPAREIL 0
                          if @inclu.heating_electric == 1
                            source_energie  =  3
                          elsif @inclu.heating_solar == 1
                            source_energie  = 5
                          elsif @inclu.heating_gaz == 1
                            source_energie = 1
                          elsif @inclu.heating_condensation == 1
                            source_energie = 5
                          elsif @inclu.heating_fuel == 1
                            source_energie = 2
                          elsif @inclu.heating_heat_pump == 1
                            source_energie = 6
                          else
                            source_energie  = 0
                          end
                          xml.ENERGIE source_energie
                        end
                        xml.MEUBLE @inclu.inclusion_furnished.present? ? 1 : 0
                        # xml.COUVERTURE
                        # xml.EAU_CHAUDE
                        xml.ACCES_HANDICAPE (@inclu.accessibility_elevator == 1 || @inclu.accessibility_wider_corridors == 1 ) ? 1 : 0
                        xml.ASCENSEUR @inclu.detail_elevator.present? ? 1 : 0
                      end
                    end
                    xml.LOCALISATION do
                      xml.NUM_RUE ( property.street_number.present? ? property.street_number : 0 )
                      # xml.TYPE_RUE
                      xml.NOM_RUE ( property.street_name.present? ? property.street_name : "" )
                      xml.ADRESSE1 ( property.street_name.present? ? property.street_name : "" )
                      xml.CP ( property.zipcode.present? ? property.zipcode : @geocoded_address.postal_code )
                      xml.VILLE ( property.locality.present? ? property.locality : "" )
                      xml.PAYS ( @country.name.present? ? @country.name : (@geocoded_address.present? ? @geocoded_address.country_code : "") )
                      xml.SECTEUR ( property.administrative_area_level_1.present? ? property.administrative_area_level_1 : "" )
                      xml.QUARTIER ( property.sublocality.present? ? property.sublocality : "" )
                      xml.LATITUDE ( property.latitude.present? ? property.latitude : "" )
                      xml.LONGITUDE ( property.longitude.present? ? property.longitude : "" )
                    end
                    # Videos
                    property.videos.where(:posting_status=>1).each_with_index do |video, index|
                      xml.FICHIER do
                        xml.REFERENCE video.id
                        xml.INDICE index+201
                        xml.URL_DISTANTE "https://youtube.com/embed/#{video.youtube_id}"
                      end
                    end
                    # Pictures
                    property.property_assets.each_with_index do |asset, index|
                      xml.FICHIER do
                        xml.REFERENCE asset.id
                        xml.INDICE index+1
                        begin
                          filename = "#{property.secure_id}_#{asset.picture_file_name}"
                          File.open([pagesimmo_path, filename].join("/"), 'wb') do |fo|
                            fo.write open(asset.picture.url(:large,:timestamp => false)).read
                          end
                          pictures_arr << filename
                        rescue Exception => e
                          puts "Error #{e.message} for picture #{filename}"
                        end
                        xml.FICHIER_JOINT do
                          xml.NOM_FICHIER filename
                          # xml.NOM_FICHIER asset.picture.url(:large,:timestamp => false)
                          xml.TYPE_MIME asset.picture_content_type
                        end
                        xml.DATE_MODIFICATION asset.updated_at.strftime("%Y-%m-%dT%H:%M:%S%:z")
                      end
                    end
                  end
                end
              end
            end
          end
        end

        ##============================================================##
        ## Compilation du fichier xml
        ##============================================================##
        File.open(pagesimmo_file, 'w') do |file|
          file.write builder.to_xml
        end

        ##============================================================##
        ## Validation du fichier xml
        ##============================================================##
        xsd = Nokogiri::XML::Schema(open("http://www.cryptml.net/xsd-repository/CryptML.xsd"))
        doc = Nokogiri::XML(File.read(pagesimmo_file))
        @errors = []
        xsd.validate(doc).each do |e|
          JulesLogger.info e.message
          @errors << e.message
        end


        BackOfficeMailer.new_exception(@errors, "pagesimmo").deliver_now if @errors.count > 0


        ##============================================================##
        ## Zip
        ##============================================================##
        Zip::File.open(pagesimmo_zip, Zip::File::CREATE) do |zipfile|
         zipfile.add(File.basename(pagesimmo_file),pagesimmo_file)
        end

        ##============================================================##
        ## Transfert du fichier XML
        ##============================================================##
        # if Rails.env.production?
          begin
            Net::FTP.open('ftpimmosquare.pagesimmo.com',"ftpimmosquare.pagesimmo.com|#{crypto_id(user.id)}","shareimmo_#{crypto_id(user.id)}") do |ftp|
              ftp.putbinaryfile(pagesimmo_zip)
              pictures_arr.each do |filename|
                ftp.putbinaryfile(File.join(pagesimmo_path, "/#{filename}"))
              end

              puts "New pagesimmo File Transfered #{Time.now.strftime("%Y-%m-%d %H:%M:%S")} for account #{crypto_id(user.id)}"
            end
          rescue Exception => e
            puts "Error #{e.message} for account #{user.id}"
          end
        # end
      end

      # FileUtils.rm(pagesimmo_file)
      # FileUtils.rm_rf(pagesimmo_path)


    end

    task :get_subscriptions => :environment do
      puts "pagesimmo:get_subscriptions - started"

      get_users_pagesimmo().each do |user|
        puts "Start for user #{user.id}"

        auth_Login = crypto_id(user.id)
        auth_Password = "shareimmo_#{auth_Login}"

        client = Savon.client(wsdl: ENV['pagesimmo_soap_host'])
        response = client.call(:authentification, message: { "_agence_Login" => auth_Login, "_agence_Pwd" => auth_Password, "_editeur_Key" => ENV["pagesimmo_soap_editor_key"] } )
        auth_Token = response.body[:authentification_response][:authentification_result]

        if auth_Token.nil?
          puts "Authentication error for user #{user.id}"
          next
        end

        @properties = get_properties_for_user(user)

        puts "Any property for user #{user.id}" if @properties.count == 0

        # Call activated servers for user
        portals = Portal.where(:id => get_properties_for_user(user).pluck(:portal_id))
        # puts portals.pluck(:id)
        portals.each do |portal|
          next if portal.access_key.blank?
          puts "get portal #{portal.id}"
          response = client.call(:get_serveur_diffusions, message: { "_auth_Token" => auth_Token, "_auth_Login" => auth_Login, "_CleServeur" => portal.access_key } )
          xml_response = Nokogiri::XML(response.body[:get_serveur_diffusions_response][:get_serveur_diffusions_result])
          xml_response.css("Annonce").each do |a|
            # Add Portal ID for property
            visi = VisibilityPortal.where(:property_id => a.css("Ref").first.content.to_i, :portal_id => portal.id).last
            visi.update_attributes(:backlink_id => a.css("Cle").first.content) if visi.present?
            puts "#{a.css("Ref").first.content} - #{a.css("Cle").first.content}"
          end
        end

      end



      puts "pagesimmo:get_subscriptions - ended"
    end


    task :get_inscriptions => :environment do

      @pagesimmo_portals_set = get_portals_key()

      @portals = Portal.where(@pagesimmo_portals_set.members)

      get_users_pagesimmo().each do |user|
        puts "Start for user #{user.id}"

        auth_Login = crypto_id(user.id)
        auth_Password = "shareimmo_#{auth_Login}"

        client = Savon.client(wsdl: ENV['pagesimmo_soap_host'])
        response = client.call(:authentification, message: { "_agence_Login" => auth_Login, "_agence_Pwd" => auth_Password, "_editeur_Key" => ENV["pagesimmo_soap_editor_key"] } )
        auth_Token = response.body[:authentification_response][:authentification_result]

        if auth_Token.nil?
          puts "Authentication error for user #{user.id}"
          next
        end

        response = client.call(:get_liste_inscriptions, message: { "_auth_Token" => auth_Token, "_auth_Login" => auth_Login } )

        JulesLogger.info response.body[:get_liste_inscriptions_response][:get_liste_inscriptions_result]

      end

    end

    #
    task :get_diffusions => :environment do
      @pagesimmo_portals_set = get_portals_key()

      @portals = Portal.where(@pagesimmo_portals_set.members)

      get_users_pagesimmo().each do |user|
        puts "Start for user #{user.id}"

        auth_Login = crypto_id(user.id)
        auth_Password = "shareimmo_#{auth_Login}"

        client = Savon.client(wsdl: ENV['pagesimmo_soap_host'])
        response = client.call(:authentification, message: { "_agence_Login" => auth_Login, "_agence_Pwd" => auth_Password, "_editeur_Key" => ENV["pagesimmo_soap_editor_key"] } )
        auth_Token = response.body[:authentification_response][:authentification_result]

        if auth_Token.nil?
          puts "Authentication error for user #{user.id}"
          next
        end

        @portals.each do |portal|
          next if portal.access_key.nil?
          # response = client.call(:get_liste_diffusions, message: { "_auth_Token" => auth_Token, "_auth_Login" => auth_Login } )
          JulesLogger.info portal.access_key
          response = client.call(:get_serveur_diffusions, message: { "_auth_Token" => auth_Token, "_auth_Login" => auth_Login, "_CleServeur" => portal.access_key } )

          JulesLogger.info response.body[:get_serveur_diffusions_response][:get_serveur_diffusions_result]

        end


      end

    end




    task :set_diffusions => :environment do
      @pagesimmo_portals_set = get_portals_key()

      puts "update diffusion"

      @portals = Portal.where(@pagesimmo_portals_set.members)

      get_users_pagesimmo().each do |user|
        puts "Start for user #{user.id}"

        auth_Login = crypto_id(user.id)
        auth_Password = "shareimmo_#{auth_Login}"

        client = Savon.client(wsdl: ENV['pagesimmo_soap_host'])
        response = client.call(:authentification, message: { "_agence_Login" => auth_Login, "_agence_Pwd" => auth_Password, "_editeur_Key" => ENV["pagesimmo_soap_editor_key"] } )
        auth_Token = response.body[:authentification_response][:authentification_result]

        if auth_Token.nil?
          puts "Authentication error for user #{user.id}"
          next
        end

        @properties = get_properties_for_user(user)

        # Get all visibilities for user properties
        @visibility_portals = VisibilityPortal.where(:property => @properties).where("visibility_start < :date and visibility_until > :date", { date: Time.now })

        # For each portal, send diffusion update
        @portals.where(:status => 1).each do |portal|
          visibility_portals = @visibility_portals.where(:portal_id => portal.id)
          builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
            xml.Document("xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema", "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns" => "http://tempuri.org/xmlShareIMMO.xsd") do
              xml.set_ServeurDiffusions_Arg do
                visibility_portals.each do |visi|
                  xml.Annonce do
                    xml.Cle "#{auth_Login}/#{visi.property_id}"
                    xml.Diffusee visi.posting_status
                  end
                end
              end
            end
          end

          # puts builder.to_xml
          if visibility_portals.count > 0
            puts "portal #{portal.id}" if visibility_portals.count > 0

            response = client.call(:set_serveur_diffusions, message: { "_auth_Token" => auth_Token, "_auth_Login" => auth_Login, "_CleServeur" => portal.access_key, "_XML" => builder.to_xml} )
            # response = client.call(:get_serveur_diffusions, message: { "_auth_Token" => auth_Token, "_auth_Login" => auth_Login, "_CleServeur" => portal.access_key} )
            # JulesLogger.info response.body[:set_serveur_diffusions_response][:set_serveur_diffusions_result]
            # puts response.body
          else
            # puts "nothing to update for portal #{portal.id}"
          end


        end
      end


  end


    #   visibilities_by_portal = get_visibilities_for_user(user)

    #   visibilities_by_portal.each do |p|
    #     # puts p.id
    #     # p.visibilities.each do |visi|
    #     #   visi_portal = visi.visibility_portals.first
    #     #   puts visi.id
    #     #   visi_obj =  {}
    #     #   visi_obj[:Annonce] = []
    #     #   visi_obj[:Annonce] << { :Ref => p.id, :Cle => visi_portal.portal.access_key, :Diffusee => visi_portal.posting_status}

    #     #   xml[:set_ServeurDiffusions_Arg] << visi_obj

    #     # end

    #     # response = client.call(:set_serveur_diffusions, message: { "_auth_Token" => auth_token, "_auth_Login" => ENV["pagesimmo_main_account_login"], ], "_CleServeur" => "", "_XML" => xml.to_xml} )

    #     # puts response.body

    #   end


     # end






  end

  ##============================================================##
  ## Usefull Fonction
  ##============================================================##
  def get_portals_key()
    Redis::Set.new(ENV['pagesimmo_portals_set_redis'])
  end

  def crypto_id(user_id)
    id = sprintf("%07d",user_id).split(//).last(7).join
    "#{id[0..1]}Q#{id[2..6]}"
  end

  def get_users_pagesimmo()
    # french_users = User.joins(:profile).where(:profiles => { :country => "FR" })
    @user_ids = VisibilityPortal.where(:portal_id => Portal.where(:country => "fr").pluck(:id)).joins(:property).pluck(:user_id).uniq
    @user_ids << 3719
    return User.where(:id => @user_ids).includes(:profile)
  end


  def get_properties_for_user(user)
    # Get properties activated
    return Property.joins(:visibility_portals).where(:visibility_portals => { :portal_id => Portal.where(:country => "fr") }, :user_id => user.id ).uniq
    # return Property.joins(:visibility_portals).where(:status => 1, :user_id => user.id, :service_orders => { :service_id => Service.where(:name => "Diffusion nationale").first.id } )
  end
end


