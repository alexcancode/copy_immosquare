# encoding: utf-8

require 'uri'
require 'open-uri'
require 'net/imap'
require 'capybara/poltergeist'
require 'selenium-webdriver'

Encoding.default_external = Encoding::UTF_8

namespace :craigslist do


  pictures_path           = "#{Rails.root}/tmp/craigslist/pictures"
  screenshots_path        = "#{Rails.root}/tmp/craigslist/screenshots"

  sleep_activation = false


  @mailbox_01 = "01-accounts"
  @mailbox_02 = "02-forwards"
  @mailbox_03 = "03-robots"

  ##============================================================##
  ## Setup Firefox (by Selenium)
  ##============================================================##
  Capybara.register_driver :selenium_with_firefox do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.firefox(
      :proxy            => Selenium::WebDriver::Proxy.new(:http => "proxy01.immosquare.com:7776"),
      :marionette       => false
      )
    Capybara::Selenium::Driver.new(app, :browser => :firefox, :desired_capabilities => capabilities)
  end

  ##============================================================##
  ## Setup Chrome (by Selenium)
  ##============================================================##
  Capybara.register_driver :selenium_with_chrome do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      :proxy            => Selenium::WebDriver::Proxy.new(:http => "proxy01.immosquare.com:7776"),
      )
    Capybara::Selenium::Driver.new(app, :browser => :chrome, :desired_capabilities => capabilities)
  end


  ##============================================================##
  ## Setup Phanthomjs (by poltergeist )
  ##============================================================##
  Capybara.register_driver :poltergeist_with_phantomjs do |app|
    Capybara::Poltergeist::Driver.new(app, { :phantomjs_options => ["--proxy=proxy01.immosquare.com:7776"], :js_errors => false })
  end



  ##============================================================##
  ## Craigslist Publisher
  ##============================================================##
  task :publish_property => :environment do
    ##============================================================##
    ## Start Browsing
    ##============================================================##
    Capybara.javascript_driver  = :selenium_with_firefox
    Capybara.default_driver     = :selenium_with_firefox
    browser                     = Capybara.current_session

    puts "Robot IP : #{check_proxy(browser)} - Date : #{Time.now}"
    begin
      portal      = Portal.where(:slug => "craigslist").first
      remove_ids  = []

      ##============================================================##
      ## Find Random property to publish
      ##============================================================##
      properties  = Property.joins(:visibility_portals).where.not(:id=>remove_ids).where(:visibility_portals =>{:posting_status => 1,:backlink_url => nil,:portal_id=>portal.id}).where("visibility_portals.visibility_until > ?",Time.now).where(:status=>1).where.not(:draft=>1).distinct
      if properties.blank?
        puts "No property to publish"
        next
      end

      puts "Properties to process : #{properties.count}"
      property  = properties[rand(properties.size)]

      puts "Property #{property.id} : Already inserted - #{property.visibility_portals.last.craigslist_ids.to_s.split(',').count}"
      next if property.visibility_portals.last.craigslist_ids.to_s.split(",").count >= 10

      shareimmo_url = "http://shareimmo.com/api/v2/craigslist/#{property.id}?apiKey=123immo&apiToken=square456"
      puts "get info from => #{shareimmo_url}"
      response = HTTParty.get(shareimmo_url)
      raise "Error API" if response.code != 200
      response  = JSON.parse(response.body)
      puts "Property ID : #{property.id} - #{response["sell_or_rent"]} - #{response["ad"]["title"]}"

      ##============================================================##
      ## Etape #1 : PrÃ©partion des dossiers
      ##============================================================##
      FileUtils.mkdir_p(pictures_path)     unless File.exists?(pictures_path)
      FileUtils.mkdir_p(screenshots_path)  unless File.exists?(screenshots_path)
      FileUtils.rm_rf(Dir.glob(File.join(pictures_path,'*')))

      ##============================================================##
      ## Étape #2 : Home Page
      ##============================================================##
      puts "Navigate to : #{response["home_link"]}"
      browser.visit(response["home_link"])

      get_sleep() if sleep_activation
      if browser.has_xpath?("//*[@id='postlks']/li[1]/a")
        puts "Click to : 'Publier dans petites annonces'"
        browser.find(:xpath, "//*[@id='postlks']/li[1]/a").click
      end
      get_sleep() if sleep_activation
      puts "Select category : 'housing offered'"
      browser.find(:xpath, "//*[@value='ho']").set(true)
      puts 'Ajax submission...'
      get_sleep() if sleep_activation
      case response["sell_or_rent"]
      when "rent"
        puts "Select subcategory : 'apts/housing for rent'"
        browser.find(:xpath, "//input[@value=1]").set(true)
        puts 'Ajax submission... and now we are on form'
        puts "Fill cats & dogs (only rent)"
        browser.find(:xpath, "//*[@id='pets_cat']").click if response["details2"]["cats"]
        browser.find(:xpath, "//*[@id='pets_dog']").click if response["details2"]["dogs"]
      else
        puts "Property is for sale - define real estate"
        browser.find(:xpath, "//input[@value=144]").set(true)
        puts "Click to accept conditions"
        browser.find(:xpath, "//button[@name=\"go\"]").click
      end
      get_sleep() if sleep_activation

      ##============================================================##
      ##
      ##============================================================##
      email = "ad_#{(0...8).map { (65 + rand(26)).chr }.join.downcase}_#{property.id}@mailhubimmo.com"
      puts "Fill Emails : #{email}"
      browser.fill_in('FromEMail', with: email)
      get_sleep() if sleep_activation
      browser.fill_in('ConfirmEMail', with: email)
      puts "Fill phone"
      get_sleep() if sleep_activation
      browser.fill_in('contact_phone', with: response["contact"]["phone"]) unless response["contact"]["phone"] == "DO NOT DISPLAY"
      puts "Fill contact name"
      browser.fill_in('contact_name', with: response["contact"]["name"])
      puts "Fill title"
      get_sleep() if sleep_activation
      browser.fill_in('PostingTitle', with: "#{response['ad']['title']}")
      puts "Fill postal code"
      browser.fill_in('postal_code', with: response["address"]["zipcode"])
      get_sleep() if sleep_activation
      puts "Fill body (description)"
      if response['property_id'] == 26502
        browser.fill_in('PostingBody', with: "Venez vivre le Sud-Ouest dans l’un des deux magnifiques immeubles de condos neufs (2014) en location. Donnant sur le canal Lachine, vous serez non seulement charmé par la tranquillité du quartier mais surtout de sa proximité de tous les services et activités disponible. À moins de 15 minutes à pied de la station Place Saint-Henri, moins de 21 minutes à pied du Marché Atwater, à quelques pas seulement de la célèbre brasserie McAuslan, tout près du Centre Sportif Gadbois et à moins de 10 minutes des meilleurs restos de Montréal (Grumman 78, Tuck Shop, Campanelli, Rustique, Bar de Courcelles, Cho, Tequila Bar, F&F Pizza, Loic, etc.). Pour les professionnels du domaine de la santé, le condo est situé à moins de 10 minutes en voiture du nouveau MUHC.\n\nLe condo inclus :\n- Électroménagers haut de gamme\n- Locker\n- Luminaires\n- Air climatisé mural\n- Concierge sur place\n- Système d’intercom et surveillance par caméra\n\n1395$ non meublé.\n\n2000$ entièrement meublée incluant un stationnement.\n\nConstruction en béton, insonorisation et isolation supérieure, coût d'électricité très bas, finition de très haute qualité. Animaux à discuter.\n\nNous avons plusieurs unités disponibles. Une visite vous permettra de trouver celui qui vous convient!\n\nAppelez maintenant au 514-583-4419\nAntoine\n\n############\n\nCome live the Southwest in one of the two beautiful residential condo building built in 2014. Directly on the Canal Lachine, you will be charmed by the quiet neighborhood and it’s proximity to all services and activities. Less than 15 minutes’ walk to Place Saint-Henri metro station, less than 20 minutes’ walk to Atwater Market, 1 minutes’ walk to the famous MacAuslan Brewery (Canal Lachine also), close to the Centre Sportif Gadbois and less than 10 minute’s to the best restaurants in Montréal (Grumman 78, Tuck Shop, Campanelli, Rustique, Bar de Courcelles, Cho, Tequila Bar, F&F Pizza, Loic, etc.). And for the professionnals working in health care, it is less than 10 minutes by car to the new MUHC.\n\nCondo includes :\n- High-end appliances\n- Locker\n- Light fixtures\n- Wall mounted air conditioning system\n- Concierge services\n- Intercom system with camera surveillance system\n\n1395$ non furnished.\n2000$ entirely furnished including a parking spot.\n\nConcrete building, superior soundproofing and insulation, low cost electricity, high-end finishes. Pets to be discussed.\n\nWe have several units available. A visit will allow you to find the perfect unit for you!\n\nCall now at 514-583-4419\nAntoine \n\n[SI-2451-26502]\n")
      elsif response['property_id'] == 29228
        browser.fill_in('PostingBody', with: "PRIVATE BEDROOM AVAILABLE from JULY 1ST (short terms available)\nWe offer for rent private bedroom in 4 or 5 bedroom apartments with 2 bathrooms. (You share common area with roommate)\n\nRATE: CA$959 all- included (Wi-Fi, Hot water, Heating, Electricity, Taxes, Air Conditioner ……all included)\nROOMMATE MATCHING PROFIL AVAILABLE \n\nLOCATION: Located in heart of downtown Montreal (24h Pharmacy, Supermarket, Hospital, Mc Gill Metro Station...)\n•\t4mn walk to McGill University and McGill Station\n•\t8mn from UQAM\n•\t10mn from Concordia\n\nBUILDING FACILITY:\n•\tLounges\n•\tStudy room\n•\tMultimedia room\n•\tGym and Cardio room\n•\tYoga classe\n•\tGame room\n•\tLandry room\n\nCall today and ask for our promotion: 2 MONTHS RENT FOR FREE\nPLEASE CONTACT Amina: 514-452-3694 (AMINA@LAMARQ.CA)\n\n############\n\nPRIVATE BEDROOM AVAILABLE from JULY 1ST (short terms available)\nWe offer for rent private bedroom in 4 or 5 bedroom apartments with 2 bathrooms. (You share common area with roommate)\n\nRATE: CA$959 all- included (Wi-Fi, Hot water, Heating, Electricity, Taxes, Air Conditioner ……all included)\nROOMMATE MATCHING PROFIL AVAILABLE (you can choose between: There is a female roommate-only_ or coed)\n\nLOCATION: Located in heart of downtown Montreal (24h Pharmacy, Supermarket, Hospital, Mc Gill Metro Station...)\n•\t4mn walk to McGill University and McGill Station\n•\t8mn from UQAM\n•\t10mn from Concordia\n\nBUILDING FACILITY:\n•\tLounges\n•\tStudy room\n•\tMultimedia room\n•\tGym and Cardio room\n•\tYoga classe\n•\tGame room\n•\tLandry room\n\nCall today and ask for our promotion: 2 MONTHS RENT FOR FREE\nPLEASE CONTACT Amina: 514-452-3694 (AMINA@LAMARQ.CA) [SI-2648-29228]")
      else
        browser.fill_in('PostingBody', with: "#{response['ad']['body'].gsub('@', '[at]')}")
      end
      get_sleep() if sleep_activation
      puts "Fill price"
      browser.fill_in('Ask', with: response["details"]["rent"])
      get_sleep() if sleep_activation
      puts "Fill date"
      browser.find(:xpath, "//select[@name=\"moveinMonth\"]/option[@value='#{response["details"]["availability_date"]["month"].to_i}']").select_option
      browser.fill_in('moveinDay', with:  response["details"]["availability_date"]["day"].to_i)
      get_sleep() if sleep_activation
      browser.fill_in('moveinYear', with: 2016)
      puts "Fill bedrooms & bathrooms"
      browser.select(response["details"]["bedrooms"], :from => "Bedrooms")   if response["details"]["bedrooms"].is_a?(Integer)
      get_sleep() if sleep_activation
      browser.select(response["details"]["bathrooms"], :from => "bathrooms") if response["details"]["bathrooms"].is_a?(Integer)
      get_sleep() if sleep_activation
      puts "Fill housing type"
      browser.select(response["details"]["type"], :from => "housing_type")
      puts "Fill parking"
      case response["details"]["parking"]
      when "Inside Parking"
        parking = "attached garage"
      when "Outside Parking"
        parking = "detached garage"
      when "Parking On Street"
        parking = "street parking"
      end
      browser.select(parking, :from => "parking") if parking.present?
      get_sleep() if sleep_activation
      puts "Fill more options - furnished"
      browser.find(:xpath, "//*[@id=\"is_furnished\"]").click if response["details2"]["furnished"]
      get_sleep() if sleep_activation
      puts "Fill more options - accessibility"
      browser.find(:xpath, "//*[@id=\"wheelchaccess\"]").click if response["details2"]["accessibility"]
      get_sleep() if sleep_activation
      puts "Fill Address"
      browser.fill_in('xstreet0',   with: response["address"]["street_name"])
      browser.fill_in('city',   with: response["address"]["city"])
      browser.find(:xpath, "//button[contains(@class, \"bigbutton\")]").click
      get_sleep() if sleep_activation
      puts "Fill Address for map :"
      browser.fill_in('xstreet0',   with: response["address"]["street_name"])
      get_sleep() if sleep_activation
      puts "  - postal code"
      browser.fill_in('postal_code',   with: response["address"]["zipcode"])
      get_sleep() if sleep_activation
      puts "  - city"
      browser.fill_in('city',   with: response["address"]["city"])
      browser.find(:xpath, "//button[contains(@class, \"bigbutton\")]").click
      get_sleep() if sleep_activation
      puts "  - skip"

      # Attention, le skip_map ne fonctionne pas avec Chrome ...
      skip_map = browser.all(:xpath, "//*[@class='skipmap']").first
      skip_map.click if skip_map.present?
      ##============================================================##
      ## Pictures
      ##============================================================##
      classic = browser.first(:css, "#classic")
      unless classic.nil?
        puts "Click to go classic picture"
        classic.click
        get_sleep() if sleep_activation
      end
      puts "Fill pictures (#{response["pictures"].size} picture(s))"
      response["pictures"].each_with_index do |picture, index|
        if index < 22
          picture_name = "property_#{property.id}_#{index}.#{picture['large_url'].split('.').last}"
          picture_path = File.join(pictures_path, picture_name)
          open(picture_path, 'wb') do |file|
            file.write HTTParty.get(picture["large_url"]).parsed_response
          end

          picture_input = browser.all(:xpath, "//input[@type=\"file\"]").first
          if picture_input.present?
            picture_input.set(File.expand_path(picture_path))
            puts "##{index+1} #{File.basename(picture["large_url"])} => #{File.basename(picture_path)}"
            sleep(1)
          end
        end
        sleep(1)
      end
      sleep(3)
      puts "On attends pour éviter l'âne sur la prochaine page"
      puts "Click to 'Terminé avec image'"
      browser.find(:xpath, "//button[contains(@class, 'bigbutton')]").click
      ##============================================================##
      ## Step 3.4 : Publish draft
      ##============================================================##
      get_sleep() if sleep_activation
      if browser.has_xpath?("//button[contains(@class,'bigbutton')]")
        puts "Click to publish"
        browser.find(:xpath, "//button[contains(@class,'bigbutton')]").click
      else
        ##============================================================##
        ## Nous sommes dans le cas où nous arrivons sur la page
        ## 'bizarre (anti-robot)' de Craigslist
        ##  -------
        ##   O
        ##    O   ^__^
        ##     o  (oo)\_______
        ##        (__)\       )\/\
        ##            ||----w |
        ##            ||     ||
        ##
        ##============================================================##
        puts "On trouve le lien click here... et on retourne à l'edition d'image"
        browser.find("a", :text => "here").click
        sleep(3)
        puts "On reclick sur le boutton terminer"
        browser.find(:xpath, "//button[contains(@class, 'bigbutton')]").click
      end
      screenshot = File.join(screenshots_path,"craigslist_#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.png")
      browser.save_screenshot(screenshot)
      if browser.has_xpath?("//*[@class='ul']/li[1]/a").present?
        visi = property.visibility_portals.where(:portal_id => Portal.where(:slug => "craigslist").first.id).last
        visi.update_attribute(:backlink_url,browser.all(:xpath, "//*[@class='ul']/li[1]/a").first.text)
        puts "#{visi.id} => #{craigslist_link.text}"
      else
        puts "Email Validation required"
      end
    rescue Exception => e
      puts "Error #{e.message}"
      puts "="*30
      puts e.backtrace
      screenshot = "#{screenshots_path}_ad_#{property.present? ? property.id : nil}"
      browser.save_screenshot(screenshot)
      CraigslistMailer.craigslist_error("craigslist#publish Ad #{property.present? ? property.id : nil}","#{e.message}",screenshot).deliver_now
    end
  end


  ##============================================================##
  ## Check Activation Emails
  ##============================================================##
  task :check_activations_emails => :environment do
    ##============================================================##
    ## Start Browsing
    ##============================================================##
    Capybara.javascript_driver  = :selenium_with_firefox
    Capybara.default_driver     = :selenium_with_firefox
    browser                     = Capybara.current_session

    puts "Robot IP : #{check_proxy(browser)}"
    portal = Portal.where(:slug => "craigslist").first
    ##============================================================##
    ## Étape #1 : Login
    ##============================================================##
    imap = Net::IMAP.new("mail.gandi.net",993,true)
    imap.login(ENV["shareimmo_email_login"], ENV["shareimmo_email_password"])

    ##============================================================##
    ## Étape #2 : CrÃ©ation de la boite de sauvegarde puis placement
    ## dans INBOX
    ##============================================================##
    puts "#{'='*30}\nCréation des boites mails\n#{'='*30}"
    imap.create("MyMails/#{@mailbox_01}") unless imap.list('MyMails/',@mailbox_01)
    imap.create("MyMails/#{@mailbox_02}") unless imap.list('MyMails/',@mailbox_02)
    imap.create("MyMails/#{@mailbox_03}") unless imap.list('MyMails/',@mailbox_03)
    imap.select('INBOX')

    puts "#{'='*30}\nTraitement des emails de validation d'annonces\n#{'='*30}"
    #============================================================##
    # Étape #3 : Traitement des emails de validations
    #============================================================##
    imap.search(['NOT','DELETED',"from", "robot@craigslist.org","subject","AFFICHER/MODIF/SUPPRIMER"]).each do |message_id|
      mail = Mail.read_from_string(imap.fetch(message_id, 'RFC822')[0].attr['RFC822'])
      begin
        puts "#{"="*30}\nsubject : #{mail.subject}"
        email_links = mail.html_part.decoded.match(/<a[^>]* href="([^"]*)"/).captures
        ##============================================================##
        ## Etape #3.1 : Visit url for posting
        ##============================================================##
        browser.visit(email_links.first)
        puts "Visit email link : #{email_links.first}"
        if browser.has_xpath?("//*[@class='mustaccepttou']").present?
          get_sleep() if sleep_activation
          puts "Accept terms of use"
          browser.all(:xpath, "//*[@class='previewButtons'][2]/form[1]/button").first.click
        end
        get_sleep() if sleep_activation
        if browser.has_xpath?('//*[@class="ul"]/li[1]/a').present?
          backlink_url = browser.all(:xpath, '//*[@class="ul"]/li[1]/a').first.text
        elsif browser.has_xpath?("//*[@class='managestatus']/table/tbody/tr[1]/td/p[1]/a").present?
          backlink_url = browser.all(:xpath, "//*[@class='managestatus']/table/tbody/tr[1]/td/p[1]/a").first.text
        end
        raise "backlink_url is missing" if backlink_url.blank?
        puts "Visit Ad : #{backlink_url}"
        browser.visit(backlink_url)
        get_sleep() if sleep_activation
        if browser.has_xpath?("//*[@id='postingbody']").present?
          ad_text = browser.all(:xpath,"//*[@id='postingbody']").first
          if ad_text.text.split("[SI-").count > 1
            ad = ad_text.text.split("[SI-")[1].gsub("]", "").split("-")
            puts "update visibility ##{ad[0]} for property ##{ad[1]}"
            visibility = VisibilityPortal.where(:property_id => ad[1],:id => ad[0]).first
            craigslist_ids = visibility.craigslist_ids.to_s.split(",")
            craigslist_id = backlink_url.to_s.split("craigslist.ca/apa/")[1].to_s.split(".html")[0]
            craigslist_id = backlink_url.to_s.split("craigslist.ca/reb/")[1].to_s.split(".html")[0] if craigslist_id.blank?
            if craigslist_id.present?
              craigslist_ids << craigslist_id.to_s
              visibility.update_attributes(:backlink_url => backlink_url, :backlink_id => craigslist_id, :craigslist_ids => craigslist_ids.uniq.join(",")) if visibility.present?
              imap.copy(message_id,"MyMails/#{@mailbox_03}")
              imap.store(message_id, "+FLAGS", [:Deleted])
              puts "Email Archived"
            else
              puts "Craigslist id is missing"
            end
          end
        else
          raise "shareimmo identifiers are missings"
        end
      rescue Exception => e
        puts "error #{e.message}"
        screenshot = File.join(screenshots_path,"craigslist_#{Time.now.strftime("%Y-%m-%d_%H:%M:%S")}.png")
        browser.save_screenshot(screenshot)
        CraigslistMailer.craigslist_error("craigslist#validate Email #{message_id}","#{e.message}<br>#{e.backtrace.join("<br>")}",screenshot).deliver_now
      end
    end
    puts "#{'='*30}\nLogout\n#{'='*30}"
    ##============================================================##
    ## Étape #4 : Logout
    ##============================================================##
    imap.logout()
    imap.disconnect()
    browser.driver.quit
  end


  ##============================================================##
  ## Craigslist Check Backlinks validity
  ##============================================================##
  task :check_backlinks => :environment do
    ##============================================================##
    ## Start Browsing
    ##============================================================##
    Capybara.javascript_driver  = :poltergeist_with_phantomjs
    Capybara.default_driver     = :poltergeist_with_phantomjs
    browser                     = Capybara.current_session

    begin
      portal = Portal.where(:slug => "craigslist").first
      puts "Robot IP : #{check_proxy(browser)}"
      removed_ads = []
      VisibilityPortal.where("visibility_until > ?",Time.now).where(:portal_id => portal.id).where.not(:backlink_url => nil).each do |v|
        puts "check url for visibility #{v.id} (property #{v.property_id})"
        browser.visit(v.backlink_url)
        if browser.status_code != 200
          removed_ads << v.backlink_url
          v.update_attributes(:backlink_id => nil, :backlink_url => nil)
        else
          removed_flag = browser.all(:xpath, "//*[@id=\"userbody\"]/div[@class=\"removed\"]/h2").first
          if removed_flag.present?
            removed_ads << v.backlink_url
            v.update_attributes(:backlink_id => nil, :backlink_url => nil)
          end
        end
      end
      puts "#{removed_ads.count} ads removed"
    rescue Exception => e
      puts e.message
    end
  end


  ##============================================================##
  ## Check Emails to Forward
  ##============================================================##
  task :check_forward_emails => :environment do
    ##============================================================##
    ## Start Browsing
    ##============================================================##
    Capybara.javascript_driver  = :poltergeist_with_phantomjs
    Capybara.default_driver     = :poltergeist_with_phantomjs
    browser                     = Capybara.current_session

    puts "Robot IP : #{check_proxy(browser)}"
    portal = Portal.where(:slug => "craigslist").first
    ##============================================================##
    ## Éape #1 : Login
    ##============================================================##
    imap = Net::IMAP.new("mail.gandi.net",993,true)
    imap.login(ENV["shareimmo_email_login"], ENV["shareimmo_email_password"])

    ##============================================================##
    ## Étape #2 : CrÃ©ation de la boite de sauvegarde puis placement
    ## dans INBOX
    ##============================================================##
    puts "#{'='*30}\nCrÃ©ation des boites mails\n#{'='*30}"
    imap.create("MyMails/#{@mailbox_01}") unless imap.list('MyMails/',@mailbox_01)
    imap.create("MyMails/#{@mailbox_02}") unless imap.list('MyMails/',@mailbox_02)
    imap.create("MyMails/#{@mailbox_03}") unless imap.list('MyMails/',@mailbox_03)
    imap.select('INBOX')


    puts "#{'='*30}\nTraitement des mails Ã  forward\n#{'='*30}"
    ##============================================================##
    ## Étape #4 : Traitement des mails Ã  forward
    ##============================================================##
    imap.search(['NOT','DELETED',"from", "@reply.craigslist.org"]).each do |message_id|
      begin
        mail = Mail.read_from_string(imap.fetch(message_id, 'RFC822')[0].attr['RFC822'])
        if mail.to.first.include?("@hous.craigslist.org")
          ##============================================================##
          ## On ne veut envoyer des mails que en production
          ##============================================================##
          if Rails.env.production?
            craigslist_id =  mail.to.first.split('@hous.craigslist.org').first.split('-').last
            @v = VisibilityPortal.where(:portal_id =>portal.id).where("craigslist_ids like ?", "%#{craigslist_id}%").last
            if @v.present?
              CraigslistMailer.craigslist_notification(@v.property.user.profile.email,"#{mail.html_part ? mail.html_part.decoded : ''}",mail.from.first).deliver_now
              imap.copy(message_id,"MyMails/#{@mailbox_02}")
              imap.store(message_id, "+FLAGS", [:Deleted])
              puts "email Forwared"
            else
              puts "No visibility exist for #{craigslist_id}"
            end
          end
        else
          imap.copy(message_id,"MyMails/#{@mailbox_02}")
          imap.store(message_id, "+FLAGS", [:Deleted])
        end
      rescue Exception => e
        JulesLogger.info e.message
      end
    end


    puts "#{'='*30}\nTraitement des mails Ã  supprimer\n#{'='*30}"
    ##============================================================##
    ## Éape #5 : Traitement des mails Ã  supprimer
    ##============================================================##
    imap.search(['NOT','DELETED',"from", "robot@craigslist.org","subject","craigslist post"]).each do |message_id|
      imap.copy(message_id,"MyMails/#{@mailbox_03}")
      imap.store(message_id, "+FLAGS", [:Deleted])
    end
    imap.search(['NOT','DELETED',"from", "noreply@shareimmo.com","subject","craigslist#publish"]).each do |message_id|
      imap.store(message_id, "+FLAGS", [:Deleted])
    end

    puts "#{'='*30}\nLogout\n#{'='*30}"
    ##============================================================##
    ## Étape #6 : Logout
    ##============================================================##
    imap.logout()
    imap.disconnect()
    browser.driver.quit
  end




  def get_sleep
    sleep(rand(1..3))
  end


end
