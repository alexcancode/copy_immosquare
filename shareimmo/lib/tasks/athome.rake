namespace :athome do

  require "base64"
  require 'net/ftp'

  task :xml => :environment do
    next if Rails.env.beta?
    puts "ENV => #{Rails.env}"
    athome_path = File.join("tmp","apps","athome")
    FileUtils.rm_rf(athome_path)
    FileUtils.mkdir_p(athome_path) unless File.exists?(athome_path)
    @eu_bank = EuCentralBank.new()
    @eu_bank.update_rates

    @at_home_ftp = ENV['athome_ftp_server']
    # Get credentials for all clients (If single ftp mode)
    @portal_activation = {
      :login => ENV['athome_ftp_login'],
      :password => ENV['athome_ftp_password']
    }


    @properties = Property.includes(:property_assets,:visibility_portals).where.not(:draft=>1).where(:status =>1,:visibility_portals => {:portal_id => Portal.where(:name => "atHome").first.id, :posting_status=>1}).where("visibility_portals.visibility_until > ?", Time.now)

    accounts = Array.new
    @properties.each do |p|
      accounts << p.user_id
    end

    # Create file by account
    accounts.uniq.each do |account|

      ids = Array.new

      # Get credentials for each client (If multiple ftp mode)
#      @portal_activation = PortalActivation.joins(:portal).where(:user_id=> account,:portals => {:name => "atHome"}).first
@zip_name = "export_immosquare-#{account}_#{Time.now.strftime("%Y%m%d")}"

athome_file = File.join(athome_path,"#{@zip_name}.xml")

      #      FileUtils.rm(athome_file) if File.exist?(athome_file)
      #      puts "Old athome File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"

      builder = Nokogiri::XML::Builder.new(:encoding => 'ISO-8859-1') do |xml|
        xml.atHomeExtranet do
          xml.METADATA do
            xml.Version 1.0
            xml.TimeStamp Time.now
          end
          xml.Agence do
            xml.Nom "immosquare"
            xml.CollectionBiens do
              @properties.where(:user_id => account).each do |p|
                xml.Bien(:Action => (p.status == 1 ? "S" : "D")) do
                  ids << p.id
                  xml.ID p.id
                  xml.TypeOffre p.property_listing == 1 ? 0 : 1
                  xml.TypeMandat
                  xml.Type p.property_classification.athome_match
                  xml.Prix p.property_listing_id == 1 ? "#{money_without_cents(@eu_bank.exchange(p.price, p.currency, "EUR")).gsub(/\s+/, "").to_i}.00" : "#{money_without_cents(@eu_bank.exchange(p.rent, p.currency, "EUR")).gsub(/\s+/, "").to_i}.00"
                  xml.Charges p.fees.present? ? "#{money_without_cents(@eu_bank.exchange(p.fees, p.currency, "EUR")).gsub(/\s+/, "").to_i}.00" : nil
                  xml.Caution
                  xml.Commission
                  xml.Localite p.locality
                  xml.Pays p.country_short
                  xml.Adresse "#{p.street_number} #{p.street_name}, #{p.locality}, #{p.administrative_area_level_1}"
                  xml.CodePostal p.zipcode
                  xml.Etage p.level
                  xml.Surface p.area_living.present? ? (p.area_unit_id == 1 ? p.area_living.ceil : (p.area_living/10.76391041671).ceil) : nil
                  xml.Terrain p.area_land.present? ? (p.area_unit_id == 1 ? p.area_land.ceil : (p.area_land/10.76391041671).ceil) : nil
                  xml.NombreChambres p.bedrooms
                  xml.NumPiece p.rooms
                  xml.ParentId
                  xml.GaragesCouverts p.property_inclusion.detail_inside_parking == 1 ? 1 : nil
                  xml.GaragesNonCouverts p.property_inclusion.detail_outside_parking == 1 ? 1 : nil
                  xml.NombreSalleDeBain p.bathrooms
                  xml.Age p.year_of_built
                  xml.DateDisponible  p.availability_date.present? ? (I18n.localize p.availability_date.to_time,:format=>("%d %B %Y")) : nil
                  xml.Description p.description_fr
                  xml.DescriptionSituation
                  xml.DpeEnergie do
                    xml.valeur
                    xml.lettre
                  end
                  xml.NoPhotoUpdate 0
                  xml.TVA
                  xml.Caracteristiques do
                    xml.Chauffage p.property_inclusion.inclusion_heated.present? ? p.property_inclusion.inclusion_heated : -1
                    xml.ChauffageGaz p.property_inclusion.heating_gaz.present? ? p.property_inclusion.heating_gaz : -1
                    xml.ChauffageMazout p.property_inclusion.heating_fuel.present? ? p.property_inclusion.heating_fuel : -1
                    xml.ChauffageElect p.property_inclusion.heating_electric.present? ? p.property_inclusion.heating_electric : -1
                    xml.Meuble p.property_inclusion.inclusion_furnished.present? ? p.property_inclusion.inclusion_furnished : -1
                    xml.Renove -1
                    xml.CuisineEquipee p.property_inclusion.half_furnsihed_kitchen.present? ? p.property_inclusion.half_furnsihed_kitchen : -1
                    xml.CuisineOuverte p.property_inclusion.composition_open_kitchen.present? ? p.property_inclusion.composition_open_kitchen : -1
                    xml.Cave p.property_inclusion.indoor_cellar.present? ? p.property_inclusion.indoor_cellar : -1
                    xml.Grenier p.property_inclusion.indoor_attic.present? ? p.property_inclusion.indoor_attic : -1
                    xml.WcSepare p.property_inclusion.composition_separe_toilet.present? ? p.property_inclusion.composition_separe_toilet : -1
                    xml.Piscine p.property_inclusion.detail_outside_pool.present? ? p.property_inclusion.detail_outside_pool : -1
                    xml.Sauna p.property_inclusion.detail_sauna.present? ? p.property_inclusion.detail_sauna : -1
                    xml.Jardin  p.property_inclusion.exterior_garden.present? ? p.property_inclusion.exterior_garden : -1
                    xml.TerraseBalcon p.property_inclusion.exterior_private_patio.present? ? p.property_inclusion.exterior_private_patio : -1
                    xml.FeuOuvert p.property_inclusion.indoor_fireplace.present? ? p.property_inclusion.indoor_fireplace : -1
                    xml.Parquet p.property_inclusion.floor_wood.present? ? p.property_inclusion.floor_wood : -1
                    xml.Assenceur p.property_inclusion.detail_elevator.present? ? p.property_inclusion.detail_elevator : -1
                    xml.AnimauxAdmis p.property_inclusion.pets_allow.present? ? p.property_inclusion.pets_allow : -1
                    xml.InstallationHC -1
                    xml.Living -1
                    xml.Vacance -1
                  end

                  xml.CollectionPhotos do
                    p.property_assets.each_with_index do |property_asset,n|
                      xml.Photo do
                        xml.Sequence n+1
                        xml.DataBig Base64.encode64(open(property_asset.picture.url(:large,:timestamp => false)) { |io| io.read })
                      end
                    end
                  end
                end
              end

            end
          end
          xml.shareimmoStatistics do
            xml.updated_at Time.now.strftime("%Y-%m-%d %H:%M:%S")
            xml.ids ids
            xml.count ids.count
          end
        end

      end

      File.open(athome_file, 'w') do |file|
        file.write builder.to_xml
      end

      if @portal_activation[:login].nil? || @portal_activation[:password].nil?
        JulesLogger.info "Athome export => Portal activation parameters are missing (login/password)}"
      else
        Net::FTP.open(@at_home_ftp,@portal_activation[:login],@portal_activation[:password]) do |ftp|
          ftp.putbinaryfile(athome_file)
          puts "New at home File Created for user #{account} #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
        end
      end




    end



  end

end
