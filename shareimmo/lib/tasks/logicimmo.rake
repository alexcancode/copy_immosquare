
namespace :logicimmo do

  namespace :properties do

    task :activation, [] => [:environment] do |t, args|

	  @international_service = international_service_common
	  @international_portal = international_portal_common
	  user_ids = user_ids_common


	  # For each user, activate international service
	  Property.where(:user_id => user_ids).each do |property|
		@visibility_portal = VisibilityPortal.where(:property_id => property.id, :portal_id => @international_portal.id).first_or_create
		JulesLogger.info "#{property.user_id} - #{@visibility_portal.posting_status}"
		@visibility_portal.update_attributes(:posting_status => 1) if @visibility_portal.posting_status == 0
	  end if @international_service.present? && @international_portal.present?
	end

	task :deactivation, [] => [:environment] do |t, args|
	  @international_portal = international_portal_common
	  user_ids = user_ids_common

	  # For all properties not updated from 48 hours, disable international visibility
	  Property.where(:user_id => user_ids).each do |property|
	  	visibility = property.visibility_portals.where(:portal_id => @international_portal.id, :posting_status => 1).last
	  	next if visibility.nil?

	  	# if property.updated_at < Time.now-48.hours
	  	# 	visibility.update_attributes(:posting_status => 0)
	  	# 	puts "to deactivated"
	  	# else
	  	# 	puts "Still activated"
	  	# end

	  end


	end

  end


  def user_ids_common
    return UserPortalProvider.joins(:api_provider).joins(:user).where(:api_providers => { :name => "evosys" }).pluck(:user_id)
  end

  def international_service_common
    return Service.where(:slug => "visibility_int").first
  end

  def international_portal_common
    return Portal.where(:slug => "listglobally").first
  end


end
