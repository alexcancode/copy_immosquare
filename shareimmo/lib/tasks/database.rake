namespace :database do

  task :storeimmo_reset => :environment do
    ApiProvider.where(:is_storeimmo_provider => 1).each do |provider|
      UserPortalProvider.where(:api_provider_id => provider.id).destroy_all
    end
  end

  task :update_centris_accounts => :environment do
    @api_provider_centris = ApiProvider.find_by_name("centris")
    Authentification.where(:api_provider_id=>@api_provider_centris.id).each do |auth|
      @user = User.where(:id => auth.user_id).first
      if @user.present?
        @user.user_portal_providers.where(:api_provider_id =>@api_provider_centris.id, :uid => auth.uid).first_or_create
      end
    end
  end



  task :remove_property_for_providers => :environment  do
    puts "database:remove_property_for_providers - Started"
      @api_providers = ApiProvider.where(:name => ["magex", "prestigemls"]).pluck(:id)
      @properties = Property.where(:api_provider_id => @api_providers)
      @properties.each do |property|
      puts "database:remove_property_for_providers - Remove property #{property.id}"
      property.destroy
      puts "database:remove_property_for_providers - property #{property.id} removed"
    end
    puts "database:remove_property_for_providers - Ended"
  end
end
