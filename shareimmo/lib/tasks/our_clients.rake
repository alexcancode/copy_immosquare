namespace :our_clients do

  ##============================================================##
  ## Cette tâche créée de la diffusion automatiquement pour
  ## certains clients
  ##============================================================##
  task :create_visibilities => :environment do

    listglobally  = Portal.where(:slug => "listglobally").first
    shelterr      = Portal.where(:slug => "shelterr").first


    ##============================================================##
    ## Client #1 : Joelle bitar
    ## ListGlobally
    ##============================================================##
    puts "client #1 Joelle Bitar"
    joelle = User.where(:id =>'5798').first
    if joelle.present?
      Property.where(:user_id => joelle.id).each do |p|
        visi = VisibilityPortal.where(:property_id=>p.id,:portal_id => listglobally.id).first_or_create
        visi.visibility_start = Time.now  if visi.visibility_start.blank?
        visi.visibility_until = "2099-12-31"
        visi.posting_status   = 1
        visi.save
      end
    end


    ##============================================================##
    ## Client #2 : ViaCapitale du Mont-Royal
    ## ListGlobally
    ## Shelterr
    ##============================================================##
    puts "client #2 ViaCapitalduMontRoyal"
    response = HTTParty.post('http://api.shareimmo.com/api/v1/centris/brokers',:body =>{
      :offices_ids => ["C3480"]
      })
    if response.code == 200
      JSON.parse(response.body)["brokers"].each do |broker|
        puts "broker => #{broker["no_certificat"]}"
        user_portal = UserPortalProvider.joins(:api_provider).where(:api_providers=>{:name=>'centris'}).where(:uid =>broker["no_certificat"]).first
        if user_portal.present?
          ##============================================================##
          ## On leur active le service visilibié int
          ##============================================================##
          int_service = Service.where(:slug =>"visibility_int").first
          service     = ServicesUser.where(:user_id => user_portal.user_id,:service_id=>int_service.id).first_or_create
          service.update_attributes(:on_marketing_page => 1)
          ##============================================================##
          ## On active la visibilité des annonces
          ##============================================================##
          Property.where(:user_id => user_portal.user_id).each do |p|
            ##============================================================##
            ## ListGlobally
            ##============================================================##
            visi = VisibilityPortal.where(:property_id=>p.id,:portal_id => listglobally.id).first_or_create
            visi.visibility_start = Time.now  if visi.visibility_start.blank?
            visi.visibility_until = "2099-12-31"
            visi.posting_status   = 1
            visi.save
            ##============================================================##
            ## Shelterr
            ##============================================================##
            visi = VisibilityPortal.where(:property_id=>p.id,:portal_id => shelterr.id).first_or_create
            visi.visibility_start = Time.now  if visi.visibility_start.blank?
            visi.visibility_until = "2099-12-31"
            visi.posting_status   = 1
            visi.save
          end
        end
      end
    end


    ##============================================================##
    ## Client #3 centris_shelterr, centris_rciiq
    ## Shelterr
    ##============================================================##
    puts "client #3 Yvan"
    UserPortalProvider.joins(:api_provider).where(:api_providers=>{:name=>'centris'}).where(:flow_name =>["centris_shelterr","centris_rciiq"]).each do |upp|
      Property.where(:user_id => upp.user_id).each do |p|
        visi = VisibilityPortal.where(:property_id=>p.id,:portal_id => shelterr.id).first_or_create
        visi.visibility_start = Time.now  if visi.visibility_start.blank?
        visi.visibility_until = "2099-12-31"
        visi.posting_status   = 1
        visi.save
      end
    end


  end
end
