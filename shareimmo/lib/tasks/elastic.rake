require 'ansi/progressbar'

##============================================================##
## rake elastic:import CLASS="Property"
## rake elastic:import CLASS="Building"
##============================================================##
namespace :elastic do
  task :import => :environment do
    if ENV['CLASS'].to_s == ''
      puts '='*90,"Missing CLASS : rake elastic:import CLASS='MyModel' BATCH=50",'='*90
      exit(1)
    end
    ##============================================================##
    ## Define Model + start progress bar
    ##============================================================##
    klass  = eval(ENV['CLASS'].to_s)
    total  = klass.count rescue nil
    puts '='*90,"#{klass} : #{total} reccords",'='*90
    ##============================================================##
    ## Delete the previous MeilleurPrixCitys index in Elasticsearch
    ##============================================================##
    klass.__elasticsearch__.client.indices.delete index: klass.index_name rescue nil
    puts "old index deleted"
    ##============================================================##
    ## Create the new index with the new mapping
    ##============================================================##
    klass.__elasticsearch__.client.indices.create index: klass.index_name, body: { settings: klass.settings.to_hash, mappings: klass.mappings.to_hash }
    puts "new index created with associted mapping"

    #============================================================##
    # Indexes
    #============================================================##
    pbar   = ANSI::Progressbar.new(klass.to_s, total) rescue nil
    pbar.__send__ :show if pbar

    total_errors = klass.__elasticsearch__.import(force: false, batch_size: ENV.fetch('BATCH',50).to_i) do |response|
      pbar.inc response['items'].size
    end
    pbar.finish if pbar
    puts "[IMPORT] #{total_errors} errors occurred" unless total_errors.zero?
    puts '[IMPORT] Done'
  end
end


