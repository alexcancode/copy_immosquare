# encoding: utf-8



namespace :immotop do
  task :json => :environment do

    next if Rails.env.beta?
    puts "ENV => #{Rails.env}"
    immotop_path = File.join("tmp","apps")
    immotop_file = File.join(immotop_path,"immotop.json")
    FileUtils.mkdir_p(immotop_path) unless File.exists?(immotop_path)
    FileUtils.rm(immotop_file) if File.exist?(immotop_file)
    puts "Old immotop File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
    @myArray = Array.new
    ids = Array.new
    @eu_bank = EuCentralBank.new()
    @eu_bank.update_rates

    Property.includes(:property_assets,:visibility_portals).where.not(:draft=>1).where(:status=>1,:visibility_portals => {:portal_id => Portal.where(:name => "immoTop").first.id, :posting_status=>1}).where("visibility_portals.visibility_until > ?", Time.now).each do |p|
      ids << p.id
      my_photos = Array.new
      p.property_assets.each do |property_asset|
        my_photos << property_asset.picture.url(:large,:timestamp => false)
      end
      youtube_vid = p.videos.where(:posting_status => 1).last
      @inclu = p.property_inclusion || nil
      @country = ISO3166::Country.new(p.country_short)
      @myArray << {
        :listing => {
          :ID                         => p.id,
          :Parent_ID                  => p.property_classification.immotop_match.to_i/100*100,
          :Kind_ID                    => p.property_classification.immotop_match.to_i,
          :is_new                     => p.property_status == 1 ? "1" : "0",
          :Type                       => p.property_listing == 1 ? "sale" : "rent",
          :country                    => @country.present? ? @country.name : "",
          :city                       => p.locality,
          :region1                    => p.administrative_area_level_1,
          :region2                    => p.administrative_area_level_2,
          :region3                    => "",
          :region4                    => "",
          :address                    => p.address,
          :zip                        => p.zipcode,
          :latitude                   => p.latitude,
          :longitude                  => p.longitude,
          :Date                       => p.created_at.strftime("%Y-%m-%d %H:%M:%S"),
          :date_modifyed              => p.updated_at.strftime("%Y-%m-%d %H:%M:%S"),
          :group_parent               => "",
          :year                       => p.year_of_built,
          :title                      => p.title,
          :price                      => p.property_listing_id == 1 ? "#{money_without_cents(@eu_bank.exchange(p.price, p.currency, "EUR")).gsub(/\s+/, "").to_i}.00" : "#{money_without_cents(@eu_bank.exchange(p.rent, p.currency, "EUR")).gsub(/\s+/, "").to_i}.00",
          :price4m                    => 0,
          :charges                    => "#{money_without_cents(@eu_bank.exchange(p.fees, p.currency, "EUR")).gsub(/\s+/, "").to_i}.00",
          :guarantee                  => "",
          :rooms                      => p.rooms,
          :bathrooms                  => p.bathrooms,
          :floors                     => p.building.present? ? p.building.number_of_levels : "",
          :floor                      => p.level,
          :surface                    => p.area_living.present? ? (p.area_unit_id == 1 ? p.area_living.ceil : (p.area_living/10.76391041671).ceil) : "",
          :surface_of_the_terrace     => "",
          :balcony_surface            =>  p.area_balcony.present? ? (p.area_unit_id == 1 ? p.area_balcony.ceil : (p.area_balcony/10.76391041671).ceil) : "" ,
          :garden_surface             => "",
          :surface_of_the_ground      => p.area_land.present? ? (p.area_unit_id == 1 ? p.area_land.ceil : (p.area_land/10.76391041671).ceil) : "",
          :garages_number             => "",
          :mad_ops                    => 1,
          :divisble                   => "",
          :compromis                  => "off",
          :comfort                    => "#{@inclu.detail_elevator == 1 ?  '1,'  :  ""}"+"#{@inclu.detail_spa == 1 ?  '2,'  :  ""}"+"#{@inclu.inclusion_furnished == 1 ?  '3,'  :  ""}"+"#{p.showerrooms.to_i >= 1 ?  '5,'  :  ""}"+"#{@inclu.composition_separe_toilet == 1 ?  '6,'  :  ""}"+"#{@inclu.detail_sauna == 1 ?  '7,'  :  ""}"+"#{@inclu.inclusion_dishwasher == 1 ?  '8,'  :  ""}"+"#{p.bathrooms.to_i >= 1 ?  '9'  :  ""}" ,
          :kitchen                    => "#{@inclu.composition_open_kitchen == 1 ?  '2,'  :  ""}"+"#{@inclu.half_furnsihed_kitchen == 1 ?  '3,'  :  ""}"+"#{@inclu.half_furnsihed_kitchen == 1 ?  '4,'  :  ""}"+"#{@inclu.inclusion_dishwasher == 1 ?  '5'  :  ""}"+"#{@inclu.composition_bar == 1 ?  '6'  :  ""}",
          :security                   => "#{@inclu.security_alarm == 1 ?  '1,'  :  ""}"+"#{@inclu.security_intercom == 1 ?  '2,'  :  ""}"+"#{@inclu.security_camera == 1 ?  '4,'  :  ""}",
          :inside                     => "#{@inclu.composition_living == 1 ?  '1,'  :  ""}"+"#{@inclu.composition_dining == 1 ?  '2,'  :  ""}"+"#{@inclu.indoor_storage == 1 ?  '3,'  :  ""}"+"#{@inclu.indoor_fireplace == 1 ?  '4,'  :  ""}"+"#{@inclu.detail_laundry == 1 ?  '5,'  :  ""}"+"#{@inclu.indoor_cellar == 1 ?  '6,'  :  ""}"+"#{@inclu.indoor_attic == 1 ?  '8,'  :  ""}"+"#{@inclu.indoor_double_glazing == 1 ?  '9,'  :  ""}"+"#{@inclu.indoor_triple_glazing == 1 ?  '10,'  :  ""}",
          :heating                    => "#{@inclu.heating_gaz == 1 ?  '1,'  :  ""}"+"#{@inclu.heating_fuel == 1 ?  '2,'  :  ""}"+"#{@inclu.heating_electric == 1 ?  '3,'  :  ""}"+"#{@inclu.heating_condensation == 1 ?  '4,'  :  ""}"+"#{@inclu.heating_floor_heating == 1 ?  '5,'  :  ""}"+"#{@inclu.heating_heat_pump == 1 ?  '6,'  :  ""}",
          :other                      => "#{p.property_flag_id == 1 ?  '4,'  :  "" }"+"#{@inclu.pets_allow == 1 ?  '6'  :  ""}"+"#{@inclu.accessibility_wider_corridors == 1 ?  '9'  :  ""}",
          :outside                    => "#{@inclu.exterior_back_balcony == 1 || @inclu.exterior_front_balcony == 1 ?  '1,'  :  ""}"+ "#{@inclu.detail_garagebox == 1 ?  '4,'  :  ""}"+"#{@inclu.detail_inside_parking == 1 || @inclu.detail_outside_parking == 1 ?  '5,'  :  ""}"+"#{@inclu.exterior_garden == 1 ?  '6,'  :  ""}"+"#{@inclu.exterior_terrace == 1 ?  '7,'  :  ""}"+"#{@inclu.exterior_veranda == 1 ?  '8,'  :  ""}"+"#{@inclu.detail_inside_pool  == 1 || @inclu.detail_outside_pool ?  '9,'  :  ""}"+"#{@inclu.service_tv_sat == 1 ?  '10,'  :  ""}",
          :inside_office              => "",
          :flooring                   => "#{@inclu.floor_marble == 1 ?  '1,'  :  ""}"+"#{@inclu.floor_carpet == 1 ?  '2,'  :  ""}"+"#{@inclu.floor_vinyle == 1 ?  '3,'  :  ""}"+"#{@inclu.floor_parquet == 1 ?  '5,'  :  ""}"+"#{@inclu.floor_ceramic == 1 ?  '6,'  :  ""}",
          :outside_office             => "",
          :equipment                  => "#{@inclu.service_internet == 1 ?  '5,'  :  ""}"+"#{@inclu.inclusion_air_conditioning == 1 ?  '6,'  :  ""}"+"#{@inclu.heating_solar == 1 ?  '7,'  :  ""}",
          :specific                   => "#{@inclu.detail_elevator == 1 ?  '3,'  :  ""}",
          :safety                     => "#{@inclu.security_guardian == 1 ?  '1,'  :  ""}"+"#{@inclu.security_smoke_dectector == 1 ?  '2,'  :  ""}"+"#{@inclu.security_alarm == 1 ?  '3,'  :  ""}"+"#{@inclu.security_intercom == 1 ?  '5,'  :  ""}"+"#{@inclu.security_camera == 1 ?  '7,'  :  ""}",
          :services                   => "",
          :location                   =>  "#{@inclu.transport_public_transport == 1 ?  '4,'  :  ""}",
          :other_residence            => "",
          :energy_pass                => "",
          :isolation                  => "",
          :extra_file_pdf             => "",
          :extra_file_doc             => "",
          :video                      => youtube_vid.present? ? "https://www.youtube.com/watch?v=#{youtube_vid.youtube_id}" : ""
        },
        :photos => my_photos,
        :description => {
          :fr => p.description_fr.present? ? p.description_fr : "",
          :en => p.description_en.present? ? p.description_en : "",
          :de => p.description_de.present? ? p.description_de : "",
          :nl => p.description_nl.present? ? p.description_nl : "",
          :es => p.description_es.present? ? p.description_es : "",
          :it => p.description_it.present? ? p.description_it : "",
          :pt => p.description_pt.present? ? p.description_pt : ""
        }
      }
    end




    @myArray << {
      :statistics => {
        :updated_at => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
        :ids =>  ids,
        :count => ids.count
      }
    }

    File.open(immotop_file, 'w') do |file|
      file.write @myArray.to_json
    end
    puts "New immotop File Created #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"


    # if Rails.env.production?
      Net::FTP.open('apps.shareimmo.com',ENV['apps_ftp_username'],ENV['apps_ftp_password']) do |ftp|
        ftp.putbinaryfile(immotop_file)
      end
    # end


  end
end
