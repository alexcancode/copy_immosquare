# encoding: utf-8

namespace :immopad do

  task :import_nomenclature, [:filename] => [:environment] do |t, args|
    file = "#{Rails.root}/resources/nomenclatures_default/#{args[:filename]}"
    if File.exists?(file) == true
      begin
        @nomenclature  = NomenclatureDefault.create(
          :name=> args[:filename].split('.csv').join,
          :description=>"Nomenclature par défault"
          )
        CSV.foreach(file,:col_sep => "\t") do |i|
          NomenclatureElementDefault.create(
            :nomenclature_default_id => @nomenclature.id,
            :import_id => i[0],
            :name=> i[1].present? ? i[1] : nil,
            :value => i[2].present? ? i[2] : nil,
            :element_type => i[3].present? ? i[3] : nil,
            :status =>i[4].present? ? i[4] : nil,
            :parent_id => i[5].present? ? (NomenclatureElementDefault.where(:nomenclature_default_id=>@nomenclature.id,:import_id=>i[5].to_i).first).id : nil,
            :is_the_standard_element => i[6].present? ? i[6] : 0,
            )
        end
      rescue
        @nomenclature.nomenclature_element_defaults.roots.destroy_all
        @nomenclature.destroy
      end
    else
      puts "File not found"
    end
  end


end
