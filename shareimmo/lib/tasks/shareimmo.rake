require "#{Rails.root}/app/helpers/html_to_plain_text_helper"
require "#{Rails.root}/app/helpers/cleaner_helper"
include HtmlToPlainTextHelper
include CleanerHelper
require 'capybara/poltergeist'
require 'net/ftp'
require 'active_support/core_ext/hash'
require 'json'

##============================================================##
##  Pour l'upload des images...
##============================================================##
require "open-uri"
require 'net/https'




namespace :shareimmo do

  ##============================================================##
  ## This mehtod add MLS number on centris properties if
  ## missing
  ##============================================================##
  task :check_mls => :environment do
    centris = ApiProvider.where(:name => 'centris').first
    Property.where(:api_provider_id => centris.id).each do |p|
      complement = p.property_complement
      if complement.canada_mls.blank?
        puts "#{p.uid} => MLS : #{p.uid}"
        complement.update_attribute(:canada_mls,p.uid)
      end
    end
  end


  ##============================================================##
  ## http://apps.shareimmo.com/:provider.xml
  ##============================================================##
  task :xml => :environment do
    begin
      next if Rails.env.beta?
      flows = [
        {
          :id           =>  "evosys",
          :properties   => Property.joins(:api_provider).where(:api_providers => {:name =>"evosys"}).where.not(:draft=>1),
          :ftp_host     => ENV["ftp_visite_immo"],
          :ftp_username => ENV["ftp_visite_immo_user"],
          :ftp_password => ENV["ftp_visite_immo_password"]
          },
          {
            :id           => "kangalou",
            :properties   => Property.joins(:api_provider).where(:api_providers => {:name =>"magex"}).where.not(:draft=>1).where(:status=>0),
            :ftp_host     => ENV["apps_ftp_url"],
            :ftp_username => ENV["apps_ftp_username"],
            :ftp_password => ENV["apps_ftp_password"]
          }
        ]


      ##============================================================##
      ##
      ##============================================================##
      flows.each do |provider|
        puts "ENV => #{Rails.env}"
        provider_path     = "#{Rails.root}/tmp/apps"
        provider_file     = "#{provider_path}/#{provider[:id]}.xml"
        old_provider_file = "#{provider_path}/#{provider[:id]}.xml"

        FileUtils.mkdir_p(provider_path)                unless File.exists?(provider_path)
        FileUtils.rm(old_provider_file)                 if File.exist?(old_provider_file)
        puts "Old #{provider[:id]} File Deleted"
        start = Time.now

        puts "#{provider[:id]} : #{provider[:properties].size} listings"

        builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
          account_ids   = Array.new
          property_ids  = Array.new

          xml.export do
            xml.accounts do
              provider[:properties].each do |p|
                @profile = p.user.profile
                unless account_ids.include?(p.user_id)
                  account_ids << p.user_id
                  xml.account do
                    xml.id                          p.user_id
                    xml.country                     @profile.country
                    xml.email                       @profile.user.email
                    xml.public_email                @profile.email
                    xml.first_name                  @profile.first_name
                    xml.last_name                   @profile.last_name
                    xml.phone                       @profile.phone if @profile.phone_visibility == 1
                    xml.phone_sms                   @profile.phone_sms if @profile.phone_visibility == 1
                    xml.phone_visibility            @profile.phone_visibility
                    xml.website                     @profile.website
                    xml.spoken_languages            @profile.spoken_languages
                    xml.picture_url                 @profile.picture.file? ? @profile.picture.url(:large,:timestamp => false) : nil
                    xml.facebook_url                @profile.facebook
                    xml.twitter_url                 @profile.twitter
                    xml.linkedin_url                @profile.linkedin
                    xml.pinterest_url               @profile.pinterest
                    xml.google_plus_url             @profile.google_plus
                    xml.instagram_url               @profile.instagram
                    xml.company_name                @profile.agency_name
                    xml.company_address             @profile.agency_address
                    xml.company_city                @profile.agency_city
                    xml.company_zipcode             @profile.agency_zipcode
                    xml.company_administrative_area @profile.agency_province
                    xml.company_logo_url            @profile.logo.file? ? @profile.logo.url(:large,:timestamp => false) : nil
                    xml.color1                      @profile.primary_color
                    xml.color2                      @profile.secondary_color
                    xml.presentation_text do
                      xml.fr @profile.job_fr
                      xml.en @profile.job_en
                      xml.de @profile.job_de
                      xml.nl @profile.job_nl
                      xml.it @profile.job_it
                      xml.es @profile.job_es
                      xml.pt @profile.job_pt
                    end
                    xml.baseline do
                      xml.fr @profile.baseline_fr
                      xml.en @profile.baseline_en
                      xml.de @profile.baseline_de
                      xml.nl @profile.baseline_nl
                      xml.it @profile.baseline_it
                      xml.es @profile.baseline_es
                      xml.pt @profile.baseline_pt
                    end
                  end
                end
              end
            end
            xml.properties do
              provider[:properties].each do |p|
                begin
                  property_ids << p.id
                  xml.property do
                    xml.id                  p.id
                    xml.uid                 p.uid
                    xml.account_id          p.user_id
                    xml.online              p.status
                    xml.cadastre            p.cadastre
                    xml.is_luxurious        p.is_luxurious
                    xml.unit                p.unit
                    xml.level               p.level
                    xml.classification_id   p.property_classification.api_id
                    xml.classification_name PropertyClassification.where(:id => p.property_classification_id.to_i).first.present? ? PropertyClassification.where(:id => p.property_classification_id.to_i).first.name : nil
                    xml.status_id           p.property_status_id
                    xml.listing_id          p.property_listing_id
                    xml.flag_id             p.property_flag_id
                    xml.year_of_built       p.year_of_built
                    xml.availability_date   p.availability_date
                    ##============================================================##
                    ## Complements
                    ##============================================================##
                    complement = p.property_complement
                    if complement.present?
                      xml.france do
                        xml.dpe_indice           complement.france_dpe_indice
                        xml.dpe_value            complement.france_dpe_value
                        xml.dpe_id               complement.france_dpe_id
                        xml.ges_indice           complement.france_ges_indice
                        xml.ges_value            complement.france_ges_value
                        xml.alur_is_condo        complement.france_alur_is_condo
                        xml.alur_units           complement.france_alur_units
                        xml.alur_uninhabitables  complement.france_alur_uninhabitables
                        xml.alur_spending        complement.france_alur_spending
                        xml.alur_spending        complement.france_alur_spending
                        xml.alur_legal_action    complement.france_alur_legal_action
                      end
                      xml.belgium do
                        xml.peb_indice           complement.belgium_peb_indice
                        xml.peb_value            complement.belgium_peb_value
                        xml.peb_id               complement.belgium_peb_id
                        xml.building_permit      complement.belgium_building_permit
                        xml.done_assignments     complement.belgium_done_assignments
                        xml.preemption_property  complement.belgium_preemption_property
                        xml.subdivision_permits  complement.belgium_subdivision_permits
                        xml.sensitive_flood_area complement.belgium_sensitive_flood_area
                        xml.delimited_flood_zone complement.belgium_delimited_flood_zone
                        xml.risk_area            complement.belgium_risk_area
                      end
                      xml.canada do
                        xml.mls complement.canada_mls
                      end
                    end
                    ##============================================================##
                    ## Buildings
                    ##============================================================##
                    xml.building do
                      xml.id p.building_id
                      if p.building.present?
                        xml.name            p.building.name
                        xml.units           p.building.number_of_units
                        xml.levels          p.building.number_of_levels
                        xml.address do
                          xml.address_formatted          (p.building.address.present?)                       ? p.building.address : nil
                          xml.street_number              (p.building.street_number.present?)                 ? p.building.street_number : nil
                          xml.street_name                (p.building.street_name.present?)                   ? p.building.street_name : nil
                          xml.zipcode                    (p.building.zipcode.present?)                       ? p.building.zipcode : nil
                          xml.locality                   (p.building.locality.present?)                      ? p.building.locality : nil
                          xml.sublocality                (p.building.sublocality.present?)                   ? p.building.sublocality : nil
                          xml.administrative_area_level1 (p.building.administrative_area_level_1.present?)   ? p.building.administrative_area_level_1 : nil
                          xml.administrative_area_level2 (p.building.administrative_area_level_2.present?)   ? p.building.administrative_area_level_2 : nil
                          xml.country                    (p.building.country_short.present?)                 ? p.building.country_short : nil
                          xml.latitude                   (p.building.latitude.present?)                      ? p.building.latitude : nil
                          xml.longitude                  (p.building.longitude.present?)                     ? p.building.longitude : nil
                        end
                        xml.description do
                          xml.fr ( p.building.description_fr.present? ? clean_converter(p.building.description_fr) : nil )
                          xml.en ( p.building.description_en.present? ? clean_converter(p.building.description_en) : nil )
                          xml.de ( p.building.description_de.present? ? clean_converter(p.building.description_de) : nil )
                          xml.nl ( p.building.description_nl.present? ? clean_converter(p.building.description_nl) : nil )
                          xml.it ( p.building.description_it.present? ? clean_converter(p.building.description_it) : nil )
                          xml.es ( p.building.description_es.present? ? clean_converter(p.building.description_es) : nil )
                          xml.pt ( p.building.description_pt.present? ? clean_converter(p.building.description_pt) : nil )
                        end
                        xml.pictures do
                          if p.building.building_assets.present?
                            p.building.building_assets.each_with_index do |building_asset,index|
                              xml.url(:order=>index+1) do
                                xml.text building_asset.picture.url(:large,:timestamp => false)
                              end
                            end
                          end
                        end
                      end
                    end
                    xml.address do
                      xml.address_visibility          (p.address_visibility.present?)            ? p.address_visibility : nil
                      xml.address_formatted           (p.address.present?)                       ? p.address : nil
                      xml.street_number               (p.street_number.present?)                 ? p.street_number : nil
                      xml.street_name                 (p.street_name.present?)                   ? p.street_name : nil
                      xml.zipcode                     (p.zipcode.present?)                       ? p.zipcode : nil
                      xml.locality                    (p.locality.present?)                      ? p.locality : nil
                      xml.sublocality                 (p.sublocality.present?)                   ? p.sublocality : nil
                      xml.administrative_area_level1  (p.administrative_area_level_1.present?)   ? p.administrative_area_level_1 : nil
                      xml.administrative_area_level2  (p.administrative_area_level_2.present?)   ? p.administrative_area_level_2 : nil
                      xml.country                     (p.country_short.present?)                 ? p.country_short : nil
                      xml.latitude                    (p.latitude.present?)                      ? p.latitude : nil
                      xml.longitude                   (p.longitude.present?)                     ? p.longitude : nil
                    end
                    xml.area do
                      xml.living    (p.area_living.present?)     ? p.area_living : nil
                      xml.balcony   (p.area_balcony.present?)    ? p.area_balcony : nil
                      xml.land      (p.area_land.present?)       ? p.area_land : nil
                      xml.unit_id   (p.area_unit_id.present?)    ? p.area_unit_id : nil
                      xml.unit_name (p.area_unit.present? and p.area_unit.name.present?)  ? p.area_unit.name : nil
                    end
                    xml.rooms do
                      xml.rooms       (p.rooms.present?)         ? p.rooms : nil
                      xml.half        (p.withHalf.present?)      ? p.withHalf : nil
                      xml.bathrooms   (p.bathrooms.present?)     ? p.bathrooms : nil
                      xml.bedrooms    (p.bedrooms.present?)      ? p.bedrooms : nil
                      xml.showerrooms (p.showerrooms.present?)   ? p.showerrooms : nil
                    end
                    xml.title do
                      xml.fr (p.title_fr.present?)  ?  p.title_fr : nil
                      xml.en (p.title_en.present?)  ?  p.title_en : nil
                      xml.de (p.title_de.present?)  ?  p.title_de : nil
                      xml.nl (p.title_nl.present?)  ?  p.title_nl : nil
                      xml.it (p.title_it.present?)  ?  p.title_it : nil
                      xml.es (p.title_es.present?)  ?  p.title_es : nil
                      xml.pt (p.title_pt.present?)  ?  p.title_pt : nil
                    end
                    xml.description do
                      xml.fr ( p.description_fr.present?  ?  p.description_fr : nil )
                      xml.en ( p.description_en.present?  ?  p.description_en : nil )
                      xml.de ( p.description_de.present?  ?  p.description_de : nil )
                      xml.nl ( p.description_nl.present?  ?  p.description_nl : nil )
                      xml.it ( p.description_it.present?  ?  p.description_it : nil )
                      xml.es ( p.description_es.present?  ?  p.description_es : nil )
                      xml.pt ( p.description_pt.present?  ?  p.description_pt : nil )
                    end
                    xml.url do
                      xml.fr (p.property_url_fr.present?)  ?  p.property_url_fr : nil
                      xml.en (p.property_url_en.present?)  ?  p.property_url_en : nil
                      xml.de (p.property_url_de.present?)  ?  p.property_url_de : nil
                      xml.nl (p.property_url_nl.present?)  ?  p.property_url_nl : nil
                      xml.it (p.property_url_it.present?)  ?  p.property_url_it : nil
                      xml.es (p.property_url_es.present?)  ?  p.property_url_es : nil
                      xml.pt (p.property_url_pt.present?)  ?  p.property_url_pt : nil
                    end
                    xml.pictures do
                      if p.property_assets.present?
                        p.property_assets.each_with_index do |property_asset,index|
                          xml.url(:order => index+1) do
                            xml.text(property_asset.picture.url(:large,:timestamp => false))
                          end
                        end
                      end
                    end
                    xml.videos do
                    end
                    xml.financial do
                      xml.price           (p.price.present?)            ? p.price : nil
                      xml.price_with_tax  (p.price_with_tax.present?)   ? p.price_with_tax : nil
                      xml.rent            (p.rent.present?)             ? p.rent : nil
                      xml.rent_frequency  (p.rent_frequency.present?)   ? p.rent_frequency : nil
                      xml.fees            (p.fees.present?)             ? p.fees : nil
                      xml.tax1            (p.tax_1.present?)            ? p.tax_1 : nil
                      xml.tax2            (p.tax_2.present?)            ? p.tax_2 : nil
                      xml.income          (p.income.present?)           ? p.income : nil
                      xml.currency        (p.currency.present?)         ? p.currency : nil
                    end
                    xml.other do
                      if p.property_inclusion.present?
                        xml.inclusion do
                          xml.air_conditioning    (p.property_inclusion.inclusion_air_conditioning.present?)               ? p.property_inclusion.inclusion_air_conditioning : nil
                          xml.hot_water           (p.property_inclusion.inclusion_hot_water.present?)            ? p.property_inclusion.inclusion_hot_water : nil
                          xml.heated              (p.property_inclusion.inclusion_heated.present? )              ? p.property_inclusion.inclusion_heated : nil
                          xml.electricity         (p.property_inclusion.inclusion_electricity.present?)          ? p.property_inclusion.inclusion_electricity : nil
                          xml.furnished           (p.property_inclusion.inclusion_furnished.present?)            ? p.property_inclusion.inclusion_furnished : nil
                          xml.fridge              (p.property_inclusion.inclusion_fridge.present?)            ? p.property_inclusion.inclusion_fridge : nil
                          xml.cooker              (p.property_inclusion.inclusion_cooker.present?)               ? p.property_inclusion.inclusion_cooker : nil
                          xml.dishwasher          (p.property_inclusion.inclusion_dishwasher.present?)               ? p.property_inclusion.inclusion_dishwasher : nil
                          xml.dryer               (p.property_inclusion.inclusion_dryer.present?)               ? p.property_inclusion.inclusion_dryer : nil
                          xml.washer              (p.property_inclusion.inclusion_washer.present?)               ? p.property_inclusion.inclusion_washer : nil
                          xml.microwave           (p.property_inclusion.inclusion_microwave.present?)               ? p.property_inclusion.inclusion_microwave : nil
                        end
                        xml.detail do
                          xml.elevator          (p.property_inclusion.detail_elevator.present?)           ? p.property_inclusion.detail_elevator : nil
                          xml.laundry           (p.property_inclusion.detail_laundry.present?)            ? p.property_inclusion.detail_laundry : nil
                          xml.garbage_chute     (p.property_inclusion.detail_garbage_chute.present?)      ? p.property_inclusion.detail_garbage_chute : nil
                          xml.common_space      (p.property_inclusion.detail_common_space.present?)       ? p.property_inclusion.detail_common_space : nil
                          xml.janitor           (p.property_inclusion.detail_janitor.present?)            ? p.property_inclusion.detail_janitor : nil
                          xml.gym               (p.property_inclusion.detail_gym.present?)                ? p.property_inclusion.detail_gym : nil
                          xml.sauna             (p.property_inclusion.detail_sauna.present?)              ? p.property_inclusion.detail_sauna : nil
                          xml.golf              (p.property_inclusion.detail_golf.present?)               ? p.property_inclusion.detail_golf : nil
                          xml.tennis            (p.property_inclusion.detail_tennis.present?)             ? p.property_inclusion.detail_tennis : nil
                          xml.spa               (p.property_inclusion.detail_spa.present?)                ? p.property_inclusion.detail_spa : nil
                          xml.inside_pool       (p.property_inclusion.detail_inside_pool.present?)        ? p.property_inclusion.detail_inside_pool : nil
                          xml.outside_pool      (p.property_inclusion.detail_outside_pool.present?)       ? p.property_inclusion.detail_outside_pool : nil
                          xml.inside_parking    (p.property_inclusion.detail_inside_parking.present?)     ? p.property_inclusion.detail_inside_parking : nil
                          xml.outside_parking   (p.property_inclusion.detail_outside_parking.present?)    ? p.property_inclusion.detail_outside_parking : nil
                          xml.parking_on_street (p.property_inclusion.detail_parking_on_street.present?)  ? p.property_inclusion.detail_parking_on_street : nil
                          xml.garagebox         (p.property_inclusion.detail_garagebox.present?)          ? p.property_inclusion.detail_garagebox : nil
                        end
                        xml.half_furnished do
                          xml.living_room   (p.property_inclusion.half_furnsihed_living_room.present?)  ? p.property_inclusion.half_furnsihed_living_room : nil
                          xml.rooms         (p.property_inclusion.half_furnsihed_rooms.present?)        ? p.property_inclusion.half_furnsihed_rooms : nil
                          xml.kitchen       (p.property_inclusion.half_furnsihed_kitchen.present?)      ? p.property_inclusion.half_furnsihed_kitchen : nil
                          xml.other         (p.property_inclusion.half_furnsihed_other.present?)        ? p.property_inclusion.half_furnsihed_other : nil
                        end
                        xml.indoor do
                          xml.attic                (p.property_inclusion.indoor_attic.present?)                 ? p.property_inclusion.indoor_attic : nil
                          xml.attic_convertible    (p.property_inclusion.indoor_attic_convertible.present?)     ? p.property_inclusion.indoor_attic_convertible : nil
                          xml.attic_converted      (p.property_inclusion.indoor_attic_converted.present?)       ? p.property_inclusion.indoor_attic_converted : nil
                          xml.cellar               (p.property_inclusion.indoor_cellar.present?)                ? p.property_inclusion.indoor_cellar : nil
                          xml.central_vacuum       (p.property_inclusion.indoor_central_vacuum.present?)        ? p.property_inclusion.indoor_central_vacuum : nil
                          xml.entries_washer_dryer (p.property_inclusion.indoor_entries_washer_dryer.present?)  ? p.property_inclusion.indoor_entries_washer_dryer : nil
                          xml.entries_dishwasher   (p.property_inclusion.indoor_entries_dishwasher.present?)    ? p.property_inclusion.indoor_entries_dishwasher : nil
                          xml.fireplace            (p.property_inclusion.indoor_fireplace.present?)             ? p.property_inclusion.indoor_fireplace : nil
                          xml.storage              (p.property_inclusion.indoor_storage.present?)               ? p.property_inclusion.indoor_storage : nil
                          xml.walk_in              (p.property_inclusion.indoor_walk_in.present?)               ? p.property_inclusion.indoor_walk_in : nil
                          xml.no_smoking           (p.property_inclusion.indoor_no_smoking.present?)            ? p.property_inclusion.indoor_no_smoking : nil
                          xml.double_glazing       (p.property_inclusion.indoor_double_glazing.present?)        ? p.property_inclusion.indoor_double_glazing : nil
                          xml.triple_glazing       (p.property_inclusion.indoor_triple_glazing.present?)        ? p.property_inclusion.indoor_triple_glazing : nil
                        end
                        xml.floor do
                          xml.carpet   (p.property_inclusion.floor_carpet.present?)    ? p.property_inclusion.floor_carpet : nil
                          xml.wood     (p.property_inclusion.floor_wood.present?)      ? p.property_inclusion.floor_wood : nil
                          xml.floating (p.property_inclusion.floor_floating.present?)  ? p.property_inclusion.floor_floating : nil
                          xml.ceramic  (p.property_inclusion.floor_ceramic.present?)   ? p.property_inclusion.floor_ceramic : nil
                          xml.parquet  (p.property_inclusion.floor_parquet.present?)   ? p.property_inclusion.floor_parquet : nil
                          xml.cushion  (p.property_inclusion.floor_cushion.present?)   ? p.property_inclusion.floor_cushion : nil
                          xml.vinyle   (p.property_inclusion.floor_vinyle.present?)    ? p.property_inclusion.floor_vinyle : nil
                          xml.lino     (p.property_inclusion.floor_lino.present?)      ? p.property_inclusion.floor_lino : nil
                          xml.marble   (p.property_inclusion.floor_marble.present?)    ? p.property_inclusion.floor_marble : nil
                        end
                        xml.exterior do
                          xml.land_access    (p.property_inclusion.exterior_land_access.present?)     ? p.property_inclusion.exterior_land_access : nil
                          xml.back_balcony   (p.property_inclusion.exterior_back_balcony.present?)    ? p.property_inclusion.exterior_back_balcony : nil
                          xml.front_balcony  (p.property_inclusion.exterior_front_balcony.present?)   ? p.property_inclusion.exterior_front_balcony : nil
                          xml.private_patio  (p.property_inclusion.exterior_private_patio.present?)   ? p.property_inclusion.exterior_private_patio : nil
                          xml.storage        (p.property_inclusion.exterior_storage.present?)         ? p.property_inclusion.exterior_storage : nil
                          xml.terrace        (p.property_inclusion.exterior_terrace.present?)         ? p.property_inclusion.exterior_terrace : nil
                          xml.veranda        (p.property_inclusion.exterior_veranda.present?)         ? p.property_inclusion.exterior_veranda : nil
                          xml.garden         (p.property_inclusion.exterior_garden.present?)          ? p.property_inclusion.exterior_garden : nil
                          xml.sea_view       (p.property_inclusion.exterior_sea_view.present?)        ? p.property_inclusion.exterior_sea_view : nil
                          xml.mountain_view  (p.property_inclusion.exterior_mountain_view.present?)   ? p.property_inclusion.exterior_mountain_view : nil
                        end
                        xml.accessibility do
                          xml.elevator         (p.property_inclusion.accessibility_elevator.present?)           ? p.property_inclusion.accessibility_elevator : nil
                          xml.balcony          (p.property_inclusion.accessibility_balcony.present?)            ? p.property_inclusion.accessibility_balcony : nil
                          xml.grab_bar         (p.property_inclusion.accessibility_grab_bar.present?)           ? p.property_inclusion.accessibility_grab_bar : nil
                          xml.wider_corridors  (p.property_inclusion.accessibility_wider_corridors.present?)    ? p.property_inclusion.accessibility_wider_corridors : nil
                          xml.lowered_switches (p.property_inclusion.accessibility_lowered_switches.present?)   ? p.property_inclusion.accessibility_lowered_switches : nil
                          xml.ramp             (p.property_inclusion.accessibility_ramp.present?)               ? p.property_inclusion.accessibility_ramp : nil
                        end
                        xml.senior do
                          xml.autonomy              (p.property_inclusion.senior_autonomy.present?)               ? p.property_inclusion.senior_autonomy : nil
                          xml.half_autonomy         (p.property_inclusion.senior_half_autonomy.present?)          ? p.property_inclusion.senior_half_autonomy : nil
                          xml.no_autonomy           (p.property_inclusion.senior_no_autonomy.present?)            ? p.property_inclusion.senior_no_autonomy : nil
                          xml.meal                  (p.property_inclusion.senior_meal.present?)                   ? p.property_inclusion.senior_meal : nil
                          xml.nursery               (p.property_inclusion.senior_nursery.present?)                ? p.property_inclusion.senior_nursery : nil
                          xml.domestic_help         (p.property_inclusion.senior_domestic_help.present?)          ? p.property_inclusion.senior_domestic_help : nil
                          xml.commnunity            (p.property_inclusion.senior_community.present?)              ? p.property_inclusion.senior_community : nil
                          xml.activties             (p.property_inclusion.senior_activities.present?)             ? p.property_inclusion.senior_activities : nil
                          xml.validated_residence   (p.property_inclusion.senior_validated_residence.present?)    ? p.property_inclusion.senior_validated_residence : nil
                          xml.housing_cooperative   (p.property_inclusion.senior_housing_cooperative.present?)    ? p.property_inclusion.senior_housing_cooperative : nil
                        end
                        xml.pet do
                          xml.allow          (p.property_inclusion.pets_allow.present?)           ? p.property_inclusion.pets_allow : nil
                          xml.cat            (p.property_inclusion.pets_cat.present?)             ? p.property_inclusion.pets_cat : nil
                          xml.dog            (p.property_inclusion.pets_dog.present?)             ? p.property_inclusion.pets_dog : nil
                          xml.little_dog     (p.property_inclusion.pets_little_dog.present?)      ? p.property_inclusion.pets_little_dog : nil
                          xml.cage_aquarium  (p.property_inclusion.pets_cage_aquarium.present?)   ? p.property_inclusion.pets_cage_aquarium : nil
                          xml.not_allow      (p.property_inclusion.pets_not_allow.present?)       ? p.property_inclusion.pets_not_allow : nil
                        end
                        xml.service do
                          xml.internet  (p.property_inclusion.service_internet.present?)  ? p.property_inclusion.service_internet : nil
                          xml.tv        (p.property_inclusion.service_tv.present?)        ? p.property_inclusion.service_tv : nil
                          xml.tv_sat    (p.property_inclusion.service_tv_sat.present?)    ? p.property_inclusion.service_tv_sat : nil
                          xml.phone     (p.property_inclusion.service_phone.present?)     ? p.property_inclusion.service_phone : nil
                        end
                        xml.composition do
                          xml.bar            (p.property_inclusion.composition_bar.present?)             ? p.property_inclusion.composition_bar : nil
                          xml.living         (p.property_inclusion.composition_living.present?)          ? p.property_inclusion.composition_living : nil
                          xml.dining         (p.property_inclusion.composition_dining.present?)          ? p.property_inclusion.composition_dining : nil
                          xml.separe_toilet  (p.property_inclusion.composition_separe_toilet.present?)   ? p.property_inclusion.composition_separe_toilet : nil
                          xml.open_kitchen   (p.property_inclusion.composition_open_kitchen.present?)    ? p.property_inclusion.composition_open_kitchen : nil
                        end
                        xml.heating do
                          xml.electric       (p.property_inclusion.heating_electric.present?)        ? p.property_inclusion.heating_electric : nil
                          xml.solar          (p.property_inclusion.heating_solar.present?)           ? p.property_inclusion.heating_solar : nil
                          xml.gaz            (p.property_inclusion.heating_gaz.present?)             ? p.property_inclusion.heating_gaz : nil
                          xml.condensation   (p.property_inclusion.heating_condensation.present?)    ? p.property_inclusion.heating_condensation : nil
                          xml.fuel           (p.property_inclusion.heating_fuel.present?)            ? p.property_inclusion.heating_fuel : nil
                          xml.heat_pump      (p.property_inclusion.heating_heat_pump.present?)       ? p.property_inclusion.heating_heat_pump : nil
                          xml.floor_heating  (p.property_inclusion.heating_floor_heating.present?)   ? p.property_inclusion.heating_floor_heating : nil
                        end
                        xml.transport do
                          xml.bicycle_path      (p.property_inclusion.transport_bicycle_path.present?)       ? p.property_inclusion.transport_bicycle_path : nil
                          xml.public_transport  (p.property_inclusion.transport_public_transport.present?)   ? p.property_inclusion.transport_public_transport : nil
                          xml.public_bike       (p.property_inclusion.transport_public_bike.present?)        ? p.property_inclusion.transport_public_bike : nil
                          xml.subway            (p.property_inclusion.transport_subway.present?)             ? p.property_inclusion.transport_subway : nil
                          xml.train_station     (p.property_inclusion.transport_train_station.present?)      ? p.property_inclusion.transport_train_station : nil
                        end
                        xml.security do
                          xml.guardian         (p.property_inclusion.security_guardian.present?)          ? p.property_inclusion.security_guardian : nil
                          xml.alarm            (p.property_inclusion.security_alarm.present?)             ? p.property_inclusion.security_alarm : nil
                          xml.intercom         (p.property_inclusion.security_intercom.present?)          ? p.property_inclusion.security_intercom : nil
                          xml.camera           (p.property_inclusion.security_camera.present?)            ? p.property_inclusion.security_camera : nil
                          xml.smoke_dectector  (p.property_inclusion.security_smoke_dectector.present?)   ? p.property_inclusion.security_smoke_dectector : nil
                        end
                      end
                    end
                  end
                rescue Exception => e
                  puts "Error #{e.message}"
                end
              end
            end
            xml.shareimmo_statistics do
              xml.updated_at Time.now.strftime("%Y-%m-%d %H:%M:%S")
              xml.accounts do
                xml.count account_ids.count
                xml.ids account_ids
              end
              xml.properties do
                xml.count property_ids.count
                xml.ids property_ids
              end
            end
          end
        end

        File.open(provider_file, 'w') do |file|
          file.write(builder.to_xml)
        end

        Net::FTP.open(provider[:ftp_host],provider[:ftp_username],provider[:ftp_password]) do |ftp|
          ftp.passive = true
          ftp.putbinaryfile(provider_file)
        end if Rails.env.production?
        puts "New #{provider[:id]} File Created #{Time.now - start}s"
      end
    rescue Exception => e
      JulesLogger.info e.message
    end
  end



end




