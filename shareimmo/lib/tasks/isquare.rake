# encoding: utf-8

require "#{Rails.root}/app/helpers/html_to_plain_text_helper"

require "base64"
require 'net/ftp'


namespace :isquare do

  task :xml => :environment do
    next if Rails.env.beta?
    puts "ENV => #{Rails.env}"
    isquare_path = File.join("tmp","apps","isquare")
    FileUtils.rm_rf(isquare_path)
    FileUtils.mkdir_p(isquare_path) unless File.exists?(isquare_path)
    puts "Old isquare File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"

    @is_square_ftp = ENV["isquare_ftp_server"]
    @eu_bank = EuCentralBank.new()
    @eu_bank.update_rates



    Property.includes(:property_assets,:visibility_portals).where.not(:draft=>1).where(:status=>1,:visibility_portals => {:portal_id => Portal.where(:slug => "isquare").first.id, :posting_status=>1}).where("visibility_portals.visibility_until > ?", Time.now).each do |p|
      @zip_name = "export_immosquare-#{p.id}_#{Time.now.to_i}"
      @portal_activation = PortalActivation.joins(:portal).where(:user_id=> p.user_id,:portals => {:name => "iSquare"}).first
      isquare_file = File.join(isquare_path,"#{@zip_name}.xml")
      archive_file = File.join(isquare_path,"#{@zip_name}.zip")
      FileUtils.rm(isquare_file) if File.exist?(isquare_file)
      @inclu = p.property_inclusion || nil
      @country = ISO3166::Country.new(p.country_short)

      ##============================================================##
      ## Création du fichier xml
      ##============================================================##
      builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
        xml.annonces do
          xml.metadata do
            xml.version 1.0
            xml.timestamp Time.now.to_i
          end
          xml.agence do
            xml.name "user##{p.user_id}"
            xml.login @portal_activation.login
            xml.password @portal_activation.password
          end
          xml.export_to_site
          xml.collection_biens do
            xml.bien(:action => "S") do
              xml.id p.id
              xml.reference p.unit
              xml.vente_location p.property_listing == 1 ? "Vente" : "Location"
              xml.date_entree p.created_at.present? ? p.created_at.strftime("%d/%m/%Y") : nil
              xml.titre p.title_fr
              xml.etat
              xml.commerciaux do
                xml.id p.user_id
                xml.id_agence "AGENCE-TEST"
                xml.nom_agence p.user.profile.agency_name
                xml.initiales
                xml.nom p.user.profile.last_name
                xml.prenom p.user.profile.last_name
                xml.email p.user.profile.email
              end
              xml.nature check_classification(p.property_classification.api_id.to_s.first)
              xml.sous_nature p.property_classification.name_fr
              xml.budget p.property_listing_id == 1 ? "#{money_without_cents(@eu_bank.exchange(p.price, p.currency, "EUR")).gsub(/\s+/, "").to_i}" : "#{money_without_cents(@eu_bank.exchange(p.rent, p.currency, "EUR")).gsub(/\s+/, "").to_i}"
              xml.cacher_prix 0
              xml.charges_mensuelles "#{money_without_cents(@eu_bank.exchange(p.fees, p.currency, "EUR")).gsub(/\s+/, "").to_i}"
              xml.ville p.locality.present? ? p.locality.upcase  : nil
              xml.pays @country.present? ? @country.name.upcase : nil
              xml.region p.administrative_area_level_1
              xml.adresse  "#{p.street_name} #{p.locality} #{p.administrative_area_level_1} #{p.country_short}"
              xml.adresse_num p.street_number
              xml.adresse_bis
              xml.immeuble p.building.present? ? p.building.name : nil
              xml.quartier p.sublocality
              xml.code_postal p.zipcode
              xml.numero_etage p.level
              xml.nombre_etage p.building.present? ? p.building.number_of_levels : nil
              xml.surface p.area_living.present? ? (p.area_unit_id == 1 ? p.area_living.ceil : (p.area_living/10.76391041671).ceil) : nil
              xml.surface_terrain  p.area_land.present? ? (p.area_unit_id == 1 ? p.area_land.ceil : (p.area_land/10.76391041671).ceil) : nil
              xml.parking_souterrain
              xml.garages
              xml.parking_ouvert
              xml.annee_de_construction p.year_of_built
              xml.date_disponibilite p.availability_date.present? ? p.availability_date.strftime("%d/%m/%Y") : nil
              xml.description_fr p.description_fr
              xml.description_en p.description_en
              xml.description_de p.description_de
              xml.cuisine  @inclu.composition_open_kitchen
              xml.cuisine_equipee @inclu.half_furnsihed_kitchen
              xml.cuisine_independante
              xml.description_cuisine
              xml.sejour @inclu.composition_living
              xml.description_sejour
              xml.sejour_double @inclu.composition_dining
              xml.description_sejour_double
              xml.nombre_salles_de_bain p.bathrooms
              xml.description_salle_de_bain
              xml.salle_de_douche p.showerrooms
              xml.description_salle_de_douche
              xml.wc_separe @inclu.composition_separe_toilet
              xml.description_wc
              xml.placard @inclu.indoor_storage
              xml.description_placard
              xml.cave @inclu.indoor_cellar
              xml.description_cave
              xml.nombre_chambres p.bedrooms
              xml.chauffage_gaz @inclu.heating_gaz
              xml.chauffage_mazout @inclu.heating_fuel
              xml.chauffage_elect @inclu.heating_electric
              xml.meuble @inclu.inclusion_furnished
              xml.renove
              xml.numero_cadastral p.cadastre
              xml.antenne_satellite @inclu.service_tv_sat
              xml.parking_collectif @inclu.detail_inside_parking
              xml.grenier @inclu.indoor_attic
              xml.surface_grenier
              xml.piscine @inclu.detail_inside_pool == 1 || @inclu.detail_outside_pool == 1  ? 1 : 0
              xml.jardin @inclu.exterior_garden
              xml.surface_jardin
              xml.ascenseur @inclu.detail_elevator
              xml.installation_handicapes @inclu.accessibility_elevator == 1 || @inclu.accessibility_balcony == 1 || @inclu.accessibility_grab_bar || @inclu.accessibility_wider_corridors == 1 || @inclu.accessibility_lowered_switches == 1 || @inclu.accessibility_ramp == 1  ? 1 : 0
              xml.terrasse @inclu.exterior_terrace
              xml.surface_terrasse
              xml.balcon @inclu.exterior_back_balcony == 1 || @inclu.exterior_front_balcony == 1 ? 1 : 0
              xml.surface_balcon p.area_balcony
              xml.buanderie @inclu.detail_laundry
              xml.indice_energetique
              xml.indice_isolation
              xml.date_fin_ecopass
              xml.numero_ecopass
              xml.date_emission_ecopass
              xml.dpe_energie_valeur
              xml.dpe_energie_lettre
              xml.dpe_co2_valeur
              xml.dpe_co2_lettre
              xml.last_modification p.updated_at
              xml.triple_vitrage @inclu.indoor_triple_glazing
              xml.pompe_a_chaleur  @inclu.heating_heat_pump
              xml.chauffage_au_sol @inclu.heating_floor_heating
              xml.ventilation_mecanique @inclu.inclusion_air_conditioning
              xml.geothermie
              xml.animaux_acceptes @inclu.pets_allow
              xml.revenu_cadastral
              xml.chauffage_condensation @inclu.heating_condensation
              xml.hall
              xml.description_hall
              xml.longueur_hall
              xml.largeur_hall
              xml.surface_hall
              xml.longueur_cuisine
              xml.largeur_cuisine
              xml.surface_cuisine
              xml.longueur_sejour
              xml.largeur_sejour
              xml.surface_terrain
              xml.surface_sejour
              xml.longueur_sejour_double
              xml.largeur_sejour_double
              xml.surface_sejour_double
              xml.longueur_bureau
              xml.largeur_bureau
              xml.surface_bureau
              xml.longueur_sdb
              xml.largeur_sdb
              xml.surface_sdb
              xml.longueur_salle_de_douche
              xml.largeur_salle_de_douche
              xml.surface_salle_de_douche
              xml.longueur_cave
              xml.largeur_cave
              xml.surface_cave
              xml.longueur_ch1
              xml.largeur_ch1
              xml.surface_ch1
              xml.autre_piece
              xml.autre
              xml.caution
              xml.commission
              xml.collection_photos do
                p.property_assets.each_with_index do |property_asset,n|
                  xml.photo do
                    xml.sequence n+1
                    xml.description "#{p.property_classification.name} - #{p.locality}"
                    xml.image_data_big Base64.encode64(open(property_asset.picture.url(:large,:timestamp => false)) { |io| io.read })
                  end
                end
              end
            end
          end
        end
      end


      ##============================================================##
      ## Export du fichier
      ##============================================================##
      File.open(isquare_file, 'w') do |file|
        file.write builder.to_xml
      end
#      Zip::File.open(archive_file, Zip::File::CREATE) do |zipfile|
#        zipfile.add("#{@zip_name}.xml",isquare_file)
#      end

      ##============================================================##
      ## Tranfert
      ##============================================================##
      puts "New isquare #{p.id}"

      if @portal_activation.login.nil? || @portal_activation.password.nil?
        JulesLogger.info "Isquare export => Portal activation parameters are missing (login/password) for user #{p.user.id}"
      else
        Net::FTP.open(@is_square_ftp,@portal_activation.login,@portal_activation.password) do |ftp|
          ftp.putbinaryfile(isquare_file)
          puts "New is square File Created for user #{p.user.id} #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
        end
      end
      FileUtils.rm(isquare_file)

    end
  end



  def check_classification(classification)
    case classification
    when 1
      "Maison"
    when 2
      "Appartement"
    when 4
      "Terrain"
    when 8
      "Bureau"
    else
      "Maison"
    end
  end

end
