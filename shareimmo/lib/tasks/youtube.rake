# encoding: utf-8

namespace :youtube do
  task :remove_kangalou_videos => :environment do
    begin
      puts "Connection to youtube"
      video_removed = []
      Authentification.where(:is_account_for_video_worker => 1, :name =>"Kangalou").each do |authentification|
        account = Yt::Account.new(:refresh_token => authentification.refresh_token)
        puts "check for account : #{account.name}"
        account.videos.each do |video|
          si_video = Video.where(:youtube_id =>video.id,:posting_status => 1).where("visibility_until > ?",Time.now).first
          if si_video.present?
            puts "video #{video.id} still activated"
          else
            if video.title.start_with?("à Louer - ") || video.title.end_with?("/mois") || video.title.end_with?("/month")
              Yt::Video.new(:id =>video.id,:auth => account).delete if Rails.env == "production"
              video_removed << video.id
              JulesLogger.info "#{video.id}video deleted"
            else
              JulesLogger.info "#{video.id} - #{video.title} - not a dynamic video"
            end
          end
        end
      end

      ApiMailer.video_removed_on_youtube(video_removed).deliver_now if video_removed.count > 0
    rescue Exception => e
      JulesLogger.info e.message
      JulesLogger.info e.backtrace
    end
  end



end

