namespace :imminence do
	task :deactivate => :environment do
		puts "imminence:deactivate - started"
		Property.where(:api_provider_id => ApiProvider.where(:name => "emulis").first.id).each do |property|
			if property.updated_at < Time.now-30.hours
				property.update_attributes(:status => 0)
			end
		end
		puts "imminence:deactivate - ended"
	end

	task :reactivate => :environment do
		puts "imminence:reactivate - started"
		Property.where(:api_provider_id => 29).each do |property|
			property.update_attributes(:status => 1)
		end
		puts "imminence:reactivate - ended"
	end

end