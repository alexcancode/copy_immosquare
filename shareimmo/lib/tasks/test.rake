namespace :test do



  task :chine => :environment do
    kangalou = ApiProvider.where(:name => "kangalou").first
    puts Visibility.joins(:property).where(:international_posting_status => 1).where('visibility_start >= ?',Date.today.beginning_of_month-1.month).where('visibility_start <= ?',Date.today.beginning_of_month).where(:properties => { :api_provider_id =>kangalou.id}).count
    puts Visibility.joins(:property).where(:video_posting_status => 1).where('visibility_start >= ?',Date.today.beginning_of_month-1.month).where('visibility_start <= ?',Date.today.beginning_of_month).where(:properties => { :api_provider_id =>kangalou.id}).count
  end

  task :update_username => :environment do
    User.all.each do |u|
      u.save
    end
  end



  task :d => :environment do
    begin
      puts "Start"
      require 'selenium-webdriver'

      ##============================================================##
      ## Setup Firefox (by Selenium)
      ##============================================================##
      Capybara.register_driver :selenium_with_firefox do |app|
        capabilities = Selenium::WebDriver::Remote::Capabilities.firefox(
          :marionette => true,
          :proxy            => Selenium::WebDriver::Proxy.new(:http => "proxy01.immosquare.com:7776")
          )
        # Capybara::Selenium::Driver.new(app, :browser => :firefox, :desired_capabilities => capabilities)
        # Capybara::Selenium::Driver.new(app, :browser => :remote,  :desired_capabilities => capabilities, :url => "http://lvh.me:4444/wd/hub")
        Capybara::Selenium::Driver.new(app, :browser => :remote,  :desired_capabilities => capabilities, :url => "http://grid01.immosquare.com:4444/wd/hub")
      end


      ##============================================================##
      ## Start Browsing
      ##============================================================##
      Capybara.javascript_driver  = :selenium_with_firefox
      Capybara.default_driver     = :selenium_with_firefox
      browser                     = Capybara.current_session
      browser.visit('http://checkip.amazonaws.com')
      puts browser.find(:xpath, "//pre").text
      puts  "End"
    rescue Exception => e
      puts e.message
      puts e.backtrace
    end
  end






  task :check_country => :environment do
    broker_file = "#{Rails.root}/public/quel_pays.csv"
    @geoip ||= GeoIP.new("#{Rails.root}/db/GeoIP.dat")
    CSV.open(broker_file,'w') do |writer|
      writer << ["ip","country"]
      File.open("#{Rails.root}/public/quel_pays.txt", "r") do |f|
        f.each_line do |line|
          ip = line.gsub("\n","").gsub("\r","")
          r  = @geoip.country(ip)
          writer << [ip,r.country_name]
        end
      end
    end
  end


end
