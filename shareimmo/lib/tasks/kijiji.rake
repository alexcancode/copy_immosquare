require 'uri'
require 'open-uri'
require 'net/imap'
require 'capybara/poltergeist'
require 'selenium-webdriver'

namespace :kijiji do

  pictures_path           = "#{Rails.root}/tmp/kijiji/pictures"
  screenshots_path        = "#{Rails.root}/tmp/kijiji/screenshots"

  @mailbox_01 = "01-accounts"
  @mailbox_02 = "02-forwards"
  @mailbox_03 = "03-robots"


  ##============================================================##
  ## Setup Firefox (by Selenium)
  ##============================================================##
  Capybara.register_driver :selenium_with_firefox do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.firefox(
      :proxy            => Selenium::WebDriver::Proxy.new(:http => "proxy01.immosquare.com:7776"),
      :marionette       => false
      )
    Capybara::Selenium::Driver.new(app, :browser => :firefox, :desired_capabilities => capabilities)
  end

  ##============================================================##
  ## Setup Chrome (by Selenium)
  ##============================================================##
  Capybara.register_driver :selenium_with_chrome do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      :proxy            => Selenium::WebDriver::Proxy.new(:http => "proxy01.immosquare.com:7776"),
      )
    Capybara::Selenium::Driver.new(app, :browser => :chrome, :desired_capabilities => capabilities)
  end


  ##============================================================##
  ## Setup Phanthomjs (by poltergeist )
  ##============================================================##
  Capybara.register_driver :poltergeist_with_phantomjs do |app|
    Capybara::Poltergeist::Driver.new(app, { :phantomjs_options => ["--proxy=proxy01.immosquare.com:7776"], :js_errors => false })
  end


  ##============================================================##
  ## Start Browsing
  ##============================================================##
  Capybara.javascript_driver  = :selenium_with_firefox
  Capybara.default_driver     = :selenium_with_firefox

  browser                     = Capybara.current_session



  task :publish => :environment do
    begin
      puts "Robot IP : #{check_proxy(browser)}"
      portal      = Portal.where(:slug => "kijiji").first
      properties  = Property.joins(:visibility_portals).where(:visibility_portals =>{:posting_status => 1, :backlink_url => nil, :portal_id=>portal.id}).where("visibility_portals.visibility_until > ?",Time.now).where(:status=>1).where.not(:draft=>1).distinct
      property    = properties[rand(properties.size)]
      @response   = JSON.parse(HTTParty.get("http://shareimmo.com/api/v2/kijiji/#{property.id}?apiKey=123immo&apiToken=square456").body)
      # @response   = JSON.parse(HTTParty.get("http://lvh.me:3000/api/v2/kijiji/#{property.id}?apiKey=123immo&apiToken=square456").body)

      puts "Properties to process : #{properties.count}"
      puts "Property ID : #{property.id} - is_home? #{@response["is_home?"]}"

      ##============================================================##
      ## Étape #1 : Prépartion des dossiers
      ##============================================================##
      FileUtils.mkdir_p(pictures_path)     unless File.exists?(pictures_path)
      FileUtils.mkdir_p(screenshots_path)  unless File.exists?(screenshots_path)
      FileUtils.rm_rf(Dir.glob(File.join(pictures_path,'*')))

      ##============================================================##
      ## Étape #2 : Home Page
      ##============================================================##
      puts "Navigate to : #{@response["home_link"]}"
      browser.visit(@response["home_link"])
      # on visite d'abord l'adresse ci-dessus pour "enregistrer" la ville désirée
      puts "Navigate to : https://www.kijiji.ca/p-select-category.html"
      browser.visit('https://www.kijiji.ca/p-select-category.html')

      ##============================================================##
      ## Choix de la catégorie
      ##============================================================##
      case @response["sell_or_rent"]
      when "rent"
        browser.all(:xpath, @response["is_home?"] ==  true ? "//a[@id='CategoryId43']" : "//a[@id='CategoryId37']").first.click
      else
        browser.all(:xpath, @response["is_home?"] ==  true ? "//a[@id='CategoryId35']" : "//a[@id='CategoryId643']").first.click
      end

      ##============================================================##
      ## Anonce propriétaire (gratuit)
      ##============================================================##
      puts "choose display as owner"
      browser.all(:xpath, "//a[@id='PagePostAsOwner']").first.click if @response["sell_or_rent"] == "rent"

      ##============================================================##
      ## Only for Condo à louer
      ##============================================================##
      if @response["sell_or_rent"] == "rent" and @response["is_home?"] == false
        puts "Choose size #{@response["details"]["bedrooms"]}"
        case @response["details"]["bedrooms"]
        when 0
          type = 211
        when 1
          type = 212
        when 2
          type = 213
        when 3
          type = 214
        when 4
          type = 215
        when 5
        else
          type = 216
        end
        type_btn = browser.all(:xpath, "//*[@data-cat-id='#{type}']").first
        type_btn.click if type_btn.present?

        puts "choose display as owner"
        btn = browser.all(:xpath, "//*[@id='PagePostAsOwner']").first
        btn.click if btn.present?
      end

      ##============================================================##
      ## Choix du plan gratuit
      ##============================================================##
      sleep(1)
      browser.all(:xpath, "//button[contains(@class, 'button--jss-0-1026')]").first.click

      ##============================================================##
      ## Fill form
      ##============================================================##
      puts "Fill price"
      case @response["sell_or_rent"]
      when "rent"
        browser.fill_in('priceAmount', with: @response["details"]["rent"].gsub(",", "")) if @response["details"]["rent"].present?
      else
        browser.fill_in('priceAmount', with: @response["details"]["sell"].gsub(",", "")) if @response["details"]["sell"].present?
      end

      puts "Check rent/sell by ..."
      case @response["sell_or_rent"]
      when "rent"
        browser.all(:xpath, "//input[@id='forrentbyhousing_s' and @value='ownr']").first.click
      else
        browser.all(:xpath, "//input[@id='forsalebyhousing_s' and @value='ownr']").first.click
      end

      puts "Fill bathrooms"
      bathrooms = 10
      bathrooms = @response['details']['bathrooms']*10 if @response["details"]["bathrooms"].present? && @response["details"]["bathrooms"].to_i > 1
      bathrooms = 10 if ![10,15,20,25,30,35,40,45,55,60].include?(bathrooms)
      browser.find('#numberbathrooms_s').find(:xpath,"option[@value='#{bathrooms}']").select_option

      puts "Fill bedrooms"
      unless @response["sell_or_rent"] == "rent" && !@response["is_home?"]
        # we don't do that if it's a type "condo"
        bedrooms = @response["details"]["bedrooms"].present? && @response["details"]["bedrooms"].to_i >= 1 ? [@response["details"]["bedrooms"],6].min : 1
        browser.find('#numberbedrooms_s').find(:xpath,"option[@value='#{bedrooms}']").select_option
      end

      puts "Fill furnished"
      if @response["sell_or_rent"] == "rent"
        if @response["details2"]["furnished"]
          browser.find(:xpath, "//*[@id='furnished_s'][@value='1']").set(true)
        else
          browser.find(:xpath, "//*[@id='furnished_s'][@value='0']").set(true)
        end
      end

      puts "Fill pets accepted"
      if @response["sell_or_rent"] == "rent"
        if @response["details2"]["cats"] or @response["details2"]["dogs"]
          browser.find(:xpath, "//*[@id='petsallowed_s'][@value='1']").set(true)
        else
          browser.find(:xpath, "//*[@id='petsallowed_s'][@value='0']").set(true)
        end
      end

      puts "Fill title"
      browser.fill_in('postAdForm.title', with: @response["ad"]["title"])

      puts "Fill description"
      browser.fill_in('postAdForm.description', with: "#{@response["ad"]["body"].gsub('@', '[at]')}")

      puts "Fill Address"
      browser.fill_in('postAdForm.addressStreetNumber', with: @response["address"]["street_number"])
      browser.fill_in('postAdForm.addressStreetName',   with: @response["address"]["street_name"])
      browser.fill_in('postAdForm.addressPostalCode',   with: @response["address"]["zipcode"])
      browser.fill_in('postAdForm.addressCity',         with: @response["address"]["city"])
      browser.find('#AddressProv').find(:xpath,"option[@value='QC']").select_option

      puts "Fill (#{@response["pictures"].size} picture(s))"
      @response["pictures"].each_with_index do |picture, index|
        if index < 10
          picture_name = "property_#{property.id}_#{index}.#{picture['large_url'].split('.').last}"
          picture_path = File.join(pictures_path, picture_name)
          open(picture_path, 'wb') do |file|
            file.write HTTParty.get(picture["large_url"]).parsed_response
          end

          id = browser.evaluate_script("$('div.moxie-shim > input').attr('id');")
          puts "##{index+1} #{File.basename(picture["large_url"])} => #{id}"
          sleep(1)
          browser.attach_file("#{id}", picture_path, visible: false)
          sleep(1)
        end
      end

      puts "fill phone"
      browser.fill_in('postAdForm.phoneNumber',  with: @response["contact"]["phone"]) if @response["contact"]["phone"].present? && @response["contact"]["phone"] != "DO NOT DISPLAY"

      puts "fill email"
      email = "ad_#{(0...8).map { (65 + rand(26)).chr }.join.downcase}_#{property.id}@mailhubimmo.com"
      browser.fill_in('postAdForm.email', with: email)

      puts "click to post"
      browser.all(:xpath, "//button[@name='saveAndCheckout']").first.click

      # screenshot = File.join(screenshots_path,"kijiji_#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.png")
      # browser.save_screenshot(screenshot)
      # KijijiMailer.validation_required("kijiji#publish Ad",screenshot).deliver_now

      sleep(5)
      
      puts "kijiji end"
    rescue Exception => e
      puts "Error #{e.message}"
      screenshot = File.join(screenshots_path,"kijiji_#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.png")
      browser.save_screenshot(screenshot)
      KijijiMailer.kijiji_error("kijiji#publish Ad #{property.id}","#{e.message}",screenshot).deliver_now
    end
  end



  task :validate => :environment do
  	puts "kijiji:validate - started"

    ##============================================================##
    ## Setup IMAP connection
    ##============================================================##
    imap = Net::IMAP.new("mail.gandi.net",993.to_i,true)
    imap.login(ENV["shareimmo_email_login"], ENV["shareimmo_email_password"])

    imap.create("MyMails/#{@mailbox_01}") unless imap.list('MyMails/',@mailbox_01)
    imap.create("MyMails/#{@mailbox_02}") unless imap.list('MyMails/',@mailbox_02)
    imap.create("MyMails/#{@mailbox_03}") unless imap.list('MyMails/',@mailbox_03)

    imap.select('INBOX')
    imap.search(['NOT','DELETED',"from", "donot-reply@kijiji.ca","subject","Activez votre annonce Kijiji"]).each do |message_id|
      mail = Mail.read_from_string(imap.fetch(message_id, 'RFC822')[0].attr['RFC822'])
      puts "open email subject : #{mail.subject}"
      begin
        ##============================================================##
        ## Get and visit validation link
        ##============================================================##
        email_links = mail.html_part.decoded.scan(/<a[^>]* href="([^"]*)"/)

        puts "open url : #{email_links[1].first}"
        browser.visit(email_links[1].first.gsub(/&amp;/, "&"))

        ##============================================================##
        ## Update visibility
        ##============================================================##
        text = browser.all(:xpath, "//span[@itemprop='description']").first
        code = text.text.split("SI-")

        if code.count > 1
          ids = code[1].split("-")
          puts "update visibility for property #{ids[1]}"
          visibility = VisibilityPortal.where(:property_id => ids[1],:id => ids[0]).first

          ##============================================================##
          ## get and save backlinks
          ##============================================================##
          kijiji_id = email_links[2].first.to_s.split("adId=")[1].to_s.split("&")[0]
          if kijiji_id.present?
            backlink_url = email_links[2].first.gsub("m-activate-ad-mail", "v-view-details")
            visibility.update_attributes(:backlink_url => backlink_url, :backlink_id => kijiji_id) if visibility.present?
            imap.copy(message_id,"MyMails/#{@mailbox_03}")
            imap.store(message_id, "+FLAGS", [:Deleted])
            puts "Email Archived"
          else
            puts "Kijiji id is misssssing"
          end
        end

        puts "treatment over for #{message_id}"
      rescue Exception => e
        puts e.message
      end
    end

    puts "kijiji:validate - ended"
  end


 #  def kijiji_email(property_id)
 #    return "kijiji_#{property_id}@shareimmo.com"
 #  end

end
