# encoding: utf-8

require "#{Rails.root}/app/helpers/html_to_plain_text_helper"

require "base64"
require 'net/ftp'
require 'rubygems'
require 'nokogiri'


namespace :trovit do

  task :xml => :environment do

  	@portal = Portal.where(:slug => "trovit").first
  	raise "Portal not found" if @portal.nil?

    next if Rails.env.beta?
    puts "ENV => #{Rails.env}"
    trovit_path = File.join("tmp","apps","trovit")
  	@trovit_file = File.join(trovit_path,"trovit.xml")

    FileUtils.rm_rf(trovit_path)
    FileUtils.mkdir_p(trovit_path) unless File.exists?(trovit_path)

    puts "Old trovit File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"

  	@prestige_user_ids = UserPortalProvider.where(:api_provider_id => 23).pluck(:user_id)

  	@properties = Property.includes(:property_assets,:visibility_portals).where.not(:draft=>1).where(:visibility_portals => { :portal_id => @portal.id }).where("visibility_portals.visibility_until > ?", Time.now).limit(2)

  	@properties_prestige = Property.includes(:property_assets).where.not(:draft=>1).where(:user_id => @prestige_user_ids).limit(50-@properties.count)
  	JulesLogger.info @prestige_user_ids
  	JulesLogger.info "#{@properties.count} - #{@properties_prestige.count}"

  	@properties = @properties + @properties_prestige

	JulesLogger.info @properties.count

    ##============================================================##
    ## Création du fichier xml
    ##============================================================##
    builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
	    xml.trovit do		    	

		  	@properties.each do |p|
			  	begin

			  		@visibility = VisibilityPortal.where(:portal_id => @portal.id, :property_id => p.id, :posting_status => 1).where("visibility_until >= :date", { :date => Time.now }).first
			  		# raise "no visibility" if @visibility.nil?

			  		xml.ad do
			  			xml.id { xml.cdata "#{p.secure_id}" }
			  			xml.url { xml.cdata "http://www.shareimmo.com/ad/original/#{p.secure_id}" }
			  			# xml.mobile_url p

	  					@title  = "#{p.property_classification.name_kangalou} - #{p.rooms} #{p.withHalf == 1 ? "1/2" : ""} - #{p.locality}"
			  			xml.title  { xml.cdata  "#{@title}" }
			  			xml.content  { xml.cdata ( p.description_fr.present? ? Nokogiri::HTML(p.description_fr).text : Nokogiri::HTML(p.description_en).text ) }

			  			if p.property_listing_id == 1
				  			xml.price { xml.cdata ( p.price.to_f < 100 ? 100 : p.price.to_f ) }
			  			else
			  				case p.rent_frequency
			  					# Daily
  				  				when 1  				  					
						  			xml.price(:period => "daily") do xml.cdata ( p.price.to_f < 100 ? 100 : p.price.to_f ) end  				  					
			  					# Weekly
  				  				when 7
						  			xml.price(:period => "weekly") do xml.cdata ( p.price.to_f < 100 ? 100 : p.price.to_f ) end  				  					
  				  			 	# Monthly, by default 
				  				else
						  			xml.price(:period => "monthly") do xml.cdata ( p.price.to_f < 100 ? 100 : p.price.to_f ) end
			  				end
			  			end

			  			case p.property_classification_id
				  			when 801
				  				@p_type = ( p.property_listing_id == 1 ? "Office For Sale" : "Office For Rent" )
				  			when 812
				  				@p_type = ( p.property_listing_id == 1 ? "Parking For Sale" : "Parking For Rent" )
				  			else
				  				@p_type = ( p.property_listing_id == 1 ? "For Sale" : "For Rent" )
				  				@p_property_type = p.property_classification.name
				  		end
			  			xml.type  { xml.cdata "#{@p_type}" }
			  			xml.property_type  { xml.cdata "#{@p_property_type}" } if @p_property_type.present?

			  			# xml.foreclosure_type

			  			xml.address  { xml.cdata "#{p.address}" }
			  			xml.floor_number
			  			xml.neighborhood
			  			# xml.city_area
			  			xml.city  { xml.cdata "#{p.locality}" }
			  			xml.region  { xml.cdata "#{p.administrative_area_level_1}" }
			  			xml.country  { xml.cdata "#{ISO3166::Country.new(p.country_short)}" }

			  			xml.postcode  { xml.cdata ( is_number?(p.zipcode) ? "" : "#{p.zipcode}" ) }

			  			xml.latitude  { xml.cdata "#{p.latitude}" }
			  			xml.longitude  { xml.cdata "#{p.longitude}" }
			  			# xml.orientation
			  			xml.agency
			  			xml.mls_database  { xml.cdata "#{p.property_complement.canada_mls}" }

			  			xml.floor_area  { xml.cdata "#{p.area_living}" }
			  			# xml.plot_area

			  			xml.rooms  { xml.cdata "#{p.rooms}" }
			  			xml.bathrooms  { xml.cdata "#{p.bathrooms}" }
			  			# xml.condition

			  			xml.year  { xml.cdata ( p.year_of_built.to_i >= 1700 ? p.year_of_built.to_i : nil ) }
			  			# xml.virtual_tour
			  			# xml.eco_score

			  			xml.pictures do
				  			p.property_assets.each_with_index do |pa, index|
					  			xml.picture do
					  				xml.picture_title  { xml.cdata "#{@title} - picture #{index+1}" }
						  			xml.picture_url  { xml.cdata "#{pa.picture.url(:large,:timestamp => false)}" }		
					  			end
					  		end
					  	end

				  		xml.date  { xml.cdata ( @visibility.present? && @visibility.created_at.present? ? @visibility.created_at.strftime("%d/%m/%Y %H:%M:%S") : DateTime.now.strftime('%d/%m/%Y %H:%M:%S') ) }
				  		xml.expiration_date  { xml.cdata ( @visibility.present? ? @visibility.visibility_until.strftime("%d/%m/%Y") : (Time.now+1.days).strftime("%d/%m/%Y") ) }

				  		# xml.by_owner
				  		# xml.is_rent_to_own
				  		xml.parking  { xml.cdata "#{( p.property_inclusion.detail_parking_on_street == 1 or p.property_inclusion.detail_inside_parking == 1 or p.property_inclusion.detail_outside_parking == 1 ? 1 : 0 )}" }
				  		# xml.foreclosure
				  		xml.is_furnished  { xml.cdata "#{p.property_inclusion.inclusion_furnished}" }

				  		xml.is_new  { xml.cdata "#{( p.property_status.id == 1 ? 1 : 0 )}" }

				  		xml.contact_name  { xml.cdata "#{p.user.profile.public_name}" }
			  			xml.contact_email  { xml.cdata "portals_trovit_#{p.user.id}@shareimmo.com" }

				  		xml.contact_telephone  { xml.cdata "#{( p.user.profile.phone_visibility == 1 && p.user.profile.phone.present? ? p.user.profile.phone : "" )}" }

			  		end

			  	rescue Exception => e

			  		JulesLogger.info e.message

			  	end

		  	end


	    end



    end

  	##============================================================##
  	## Export du fichier
  	##============================================================##
  	File.open(@trovit_file, 'w') do |file|
    	file.write builder.to_xml
  	end

  	##============================================================##
  	## Tranfert
  	##============================================================##

   	Net::FTP.open("apps.shareimmo.com", ENV["apps_ftp_username"], ENV["apps_ftp_password"]) do |ftp|
		ftp.passive = true
   		ftp.putbinaryfile(@trovit_file)
   		puts "New trovit File Created #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
  	end
  	FileUtils.rm(@trovit_file)

  end
end
