

  def ad_tracker(visibility_id, property_id)
    "[SI-#{visibility_id}-#{property_id}]"
  end


  ##============================================================##
  ## Pour tester si les proxys fonctionnent
  ##============================================================##
  def check_proxy(browser)
    browser.visit "http://checkip.amazonaws.com/"
    return browser.find(:xpath, "//pre").text
  end


  def error_filename(task, step="")
    tmp_path      = File.join(Rails.root,'tmp')
    errors_path   = File.join(tmp_path, 'errors')
    return File.join(errors_path, "#{task}_#{step}_#{Time.now}.png")
  end
