

namespace :wortimmo do
  require "base64"
  task :xml => :environment do
    next if Rails.env.beta?
    puts "ENV => #{Rails.env}"
    wortimmo_path = File.join("tmp","apps")
    wortimmo_file = File.join(wortimmo_path,"wortimmo.xml")

    @login = "immosquare_test"
    @password = "x*Jx69n1"
    @eu_bank = EuCentralBank.new()
    @eu_bank.update_rates


    Property.includes(:property_assets).where.not(:draft=>1).where(:status=>1,:country_short => "LU").each do |p|
      @zipName = "#{@login}_#{p.id}_#{Time.now.to_i}"
      archive = File.join(wortimmo_path,"#{@zipName}.zip")

      FileUtils.mkdir_p(wortimmo_path) unless File.exists?(wortimmo_path)
      FileUtils.rm(wortimmo_file) if File.exist?(wortimmo_file)
      puts "Old wortimmo File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
      builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
        xml.root do
          xml.agence do
            xml.login @login
            xml.password @password
          end
          xml.bien(:action => (p.status == 1 ? "S" : "D")) do
            xml.id p.id
            xml.id_parent
            xml.id_nature p.property_classification.wortimmo_match
            xml.reference
            xml.vente_location p.property_listing == 1 ? "vente" : "location"
            xml.url p.property_url
            xml.contrat_de_construction
            xml.type_projet
            xml.budget p.property_listing_id == 1 ? "#{money_without_cents(@eu_bank.exchange(p.price, p.currency, "EUR")).gsub(/\s+/, "").to_i}.00" : "#{money_without_cents(@eu_bank.exchange(p.rent, p.currency, "EUR")).gsub(/\s+/, "").to_i}.00"
            xml.prix_m2
            xml.cacher_prix 0
            xml.charges_mensuelles p.fees
            xml.copropriete
            xml.nombre_lots_copropriete
            xml.procedure_en_cours
            xml.charges_annuelles
            xml.ville p.locality
            xml.pays p.country_short
            xml.adresse p.street_name
            xml.adresse_num p.street_number
            xml.code_postal p.zipcode
            xml.numero_etage p.level
            xml.nombre_etage
            xml.surface p.area_living.present? ? (p.area_unit_id == 1 ? p.area_living.ceil : (p.area_living/10.76391041671).ceil) : nil
            xml.surface_terrain p.area_land.present? ? (p.area_unit_id == 1 ? p.area_land.ceil : (p.area_land/10.76391041671).ceil) : nil
            xml.parking_souterrain p.property_inclusion.detail_inside_parking == 1 ? 1 : nil
            xml.parking_collectif (p.property_inclusion.detail_inside_parking == 1  || p.property_inclusion.detail_outside_parking == 1 ) ? 1 : nil
            xml.garages p.property_inclusion.detail_garagebox == 1 ? 1 : nil
            xml.parking_ouvert p.property_inclusion.detail_outside_parking == 1 ? 1 : nil
            xml.annee_de_construction p.year_of_built
            xml.date_disponibilite p.availability_date.present? ? (I18n.localize p.availability_date.to_time,:format=>("%d %B %Y")) : nil
            xml.garantie_bancaire
            xml.description_fr p.description_fr
            xml.description_en p.description_en
            xml.description_de
            xml.description_nl
            xml.cuisine
            xml.cuisine_equipee p.property_inclusion.half_furnsihed_kitchen == 1 ? 1 : nil
            xml.cuisine_independante p.property_inclusion.half_furnsihed_kitchen == 1 ? nil : 1
            xml.sejour p.property_inclusion.composition_living == 1 ? 1 : nil
            xml.salon p.property_inclusion.composition_living == 1 ? 1 : nil
            xml.salle_a_manger p.property_inclusion.composition_dining == 1 ? 1 : nil
            xml.sejour_double
            xml.nombre_salles_de_bain p.bathrooms
            xml.salle_de_douche p.showerrooms
            xml.wc_separe p.property_inclusion.composition_separe_toilet == 1 ? 1 : nil
            xml.cave p.property_inclusion.indoor_cellar == 1 ? 1 : nil
            xml.servitude
            xml.nombre_pieces p.rooms
            xml.nombre_chambres p.bedrooms
            xml.chauffage_gaz p.property_inclusion.heating_gaz == 1 ? 1 : nil
            xml.chauffage_mazout p.property_inclusion.heating_fuel == 1 ? 1 : nil
            xml.chauffage_elect p.property_inclusion.heating_electric == 1 ? 1 : nil
            xml.chauffage_col p.property_inclusion.inclusion_heated == 1 ? 1 : nil
            xml.meuble p.property_inclusion.inclusion_furnished == 1 ? 1 : nil
            xml.renove
            xml.grenier p.property_inclusion.indoor_attic == 1 ? 1 : nil
            xml.surface_grenier
            xml.type_grenier
            xml.piscine (p.property_inclusion.detail_inside_pool == 1  || p.property_inclusion.detail_outside_pool == 1 ) ? 1 : nil
            xml.jardin p.property_inclusion.exterior_garden == 1 ? 1 : nil
            xml.surface_jardin
            xml.ascenseur p.property_inclusion.detail_elevator
            xml.installation_handicapes p.property_inclusion.accessibility_elevator == 1 ? 1 : nil
            xml.animaux_acceptes p.property_inclusion.pets_allow
            xml.terrasse p.property_inclusion.exterior_terrace == 1 ? 1 : nil
            xml.surface_terrasse
            xml.balcon (p.property_inclusion.exterior_back_balcony == 1  || p.property_inclusion.exterior_front_balcony == 1 ) ? 1 : nil
            xml.surface_balcon
            xml.buanderie p.property_inclusion.detail_laundry == 1  ?  1 : nil
            xml.indice_energetique
            xml.valeur_indice_energetique
            xml.indice_isolation
            xml.valeur_indice_isolation
            xml.triple_vitrage p.property_inclusion.indoor_triple_glazing == 1 ? 1 : nil
            xml.double_vitrage p.property_inclusion.indoor_double_glazing == 1 ? 1 : nil
            xml.pompe_a_chaleur p.property_inclusion.heating_heat_pump == 1  ? 1 : nil
            xml.chauffage_au_sol p.property_inclusion.heating_floor_heating == 1  ? 1 : nil
            xml.ventilation_mecanique
            xml.geothermie
            xml.chaudiere_condensation p.property_inclusion.heating_condensation == 1  ? 1 : nil
            xml.porte_blindee
            xml.climatisation  p.property_inclusion.inclusion_air_conditioning == 1  ? 1 : nil
            xml.parlophone p.property_inclusion.security_intercom == 1 ? 1 : nil
            xml.videophone
            xml.alarme p.property_inclusion.security_alarm == 1 ? 1 : nil
            xml.bureau
            xml.antenne_satellite
            xml.volets_electriques
            xml.volets_roulants
            xml.volets_battants
            xml.cave_a_vin
            xml.achat_type p.property_status == 1 ? "neuf" : "ancien"
            xml.hall
            xml.feu_ouvert  p.property_inclusion.indoor_fireplace == 1 ? 1 : nil
            xml.caution
            xml.location_saisonniere
            xml.exclusivite
            xml.fumeurs_acceptes
            xml.digicode
            xml.gardien p.property_inclusion.security_guardian == 1 ? 1 : nil
            xml.cour
            xml.surface_cour
            xml.veranda p.property_inclusion.exterior_veranda == 1 ? 1 : nil
            xml.surface_veranda
            xml.charbon
            xml.granules
            xml.insert_cheminee
            xml.cheminee
            xml.chauffage_central
            xml.dernier_etage
            xml.radiateurs
            xml.vmc_double_flux
            xml.solaire_thermique p.property_inclusion.heating_solar == 1 ? 1 : nil
            xml.solaire_photovoltaique
            xml.collectionPhotos do
              p.property_assets.each_with_index do |property_asset,n|
                xml.photo do
                  xml.sequence n+1
                  xml.description "#{p.property_classification.name} - #{p.locality}"
                  xml.image_data_big Base64.encode64(open(property_asset.picture.url(:large)) { |io| io.read })
                end
              end
            end
          end
          xml.shareimmoStatistics do
            xml.updated_at Time.now.strftime("%Y-%m-%d %H:%M:%S")
            xml.ids p.id
            xml.count 1
          end
        end
      end
      File.open(wortimmo_file, 'w') do |file|
        file.write builder.to_xml
      end
      Zip::File.open(archive, Zip::File::CREATE) do |zipfile|
        zipfile.add("#{@zipName}.xml",wortimmo_file)
      end
      File.chmod 0755, archive
      if Rails.env.production?
        Net::FTP.open('92.222.170.249',@login,@password) do |ftp|
          ftp.putbinaryfile(archive)
        end
      end
      FileUtils.rm(archive)
      puts "New wortimmo File Created #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
    end
  end
end
