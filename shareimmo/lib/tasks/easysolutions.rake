
namespace :easysolutions do

	namespace :admin do

		namespace :services do

			# For all users from this api, activate Immopad
		    task :activate => :environment do
				user_portal_providers = UserPortalProvider.joins(:api_provider).where(:api_providers => { :name => "easy2pilot" })
		    	services_existant = ServicesUser.where(:user_id => user_portal_providers.pluck(:user_id))

				user_portal_providers.each do |upp|
					unless services_existant.pluck(:user_id).include?(upp.user_id)
						puts "Service 'Etats des lieux' for User #{upp.user_id} activated"
						@su = ServicesUser.where(:service_id => 8, :user_id => upp.user_id).first_or_create					
			        	ApiMailer.new_services_user_activated(@su, "task").deliver_now if @su
			        end

				end
		    end



	    end

	end





end