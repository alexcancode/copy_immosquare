# encoding: utf-8

require "#{Rails.root}/app/helpers/html_to_plain_text_helper"
require "#{Rails.root}/app/helpers/application_helper"

include HtmlToPlainTextHelper
include ApplicationHelper

require 'capybara/poltergeist'
require 'net/ftp'
require 'json'

namespace :lespac do

  screenshots_path        = File.join(Rails.root,'tmp','lespac','screenshots')



  task :xml => :environment do
    begin
      next if Rails.env.beta?
      puts "ENV => #{Rails.env}"
      lespac_path = File.join("tmp","apps")
      lespac_file = File.join(lespac_path,"lespac.xml")
      FileUtils.mkdir_p(lespac_path) unless File.exists?(lespac_path)
      FileUtils.rm(lespac_file) if File.exist?(lespac_file)
      puts "Old lespac File Deleted #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
      builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
        xml.UnitsRental do
          ids = Array.new
          xml.Units do
            Property.includes(:property_assets,:visibility_portals).where.not(:draft=>1).where(:status=>1,:visibility_portals => {:portal_id => Portal.where(:slug => "lespac").first.id, :posting_status=>1}).where("visibility_portals.visibility_until > ?", Time.now).each do |p|
              ids << p.id
              @price =  p.property_listing.id == 1 ? humanized_money_with_symbol(p.price) : "#{humanized_money_with_symbol p.rent} #{humanized_frequency p.rent_frequency}"
              @title = "#{p.rooms} #{p.withHalf == 1 ? "1/2" : nil} - #{p.locality} #{p.sublocality.present? ? "(#{p.sublocality})" : nil} - #{@price.present? ? "#{@price}" : nil }"
              if p.description_fr.present? or p.description_en.present?
                description = convert_to_text("#{p.description_fr.present? ? "#{p.description_fr}#{p.description_en.present? ? "\n\n############\n\n" : nil }" : nil}#{p.description_en.present? ? p.description_en : nil }")
              else
                description = @title
              end
              xml.Unit do
                xml.UnitID p.id
                xml.Title @title[0..139]
                if p.property_listing_id == 1
                  xml.Price "#{p.price_cents.to_s[0..-3]}.00"
                else
                  xml.Price "#{p.rent_cents.to_s[0..-3]}.00"
                  xml.PriceNote humanized_frequency(p.rent_frequency)
                end
                xml.Photos do
                  p.property_assets.each do |property_asset|
                    xml.Photo do
                      xml.SourceUrl property_asset.picture.url(:large)
                      xml.ImageNote nil
                    end
                  end
                end
                if p.videos.where(:posting_status=>1).present?
                  video = p.videos.where(:posting_status=>1).last
                  xml.Video{xml.text "https://www.youtube.com/watch?v=#{video.youtube_id}"}
                end
                xml.UnitAddress do
                  xml.PostalCode p.zipcode
                  xml.City p.locality
                  xml.StreetNumber p.street_number
                  xml.StreetName p.street_name
                end
                xml.Year p.year_of_built.present? ? (p.year_of_built.to_s.length > 2 ? p.year_of_built : "19#{p.year_of_built}")  : nil
                xml.Condition  p.property_status.present? ? p.property_status.lespac_name.downcase : "neuf"
                rental_type = "location long terme"
                rental_type = "location court terme" if !p.rent_frequency.blank? and p.rent_frequency < 30
                xml.RentalType rental_type
                xml.RentalPlaceType p.property_classification.lespac_match
                xml.AvailabilityDate  p.availability_date.to_time.strftime("%Y-%m-%d") unless p.availability_date.blank?
                case p.rooms
                when nil
                  number_of_rooms = nil
                when 1
                  number_of_rooms = "1 pièce"
                  if p.withHalf == 1
                    number_of_rooms = "1 1/2 pièces"
                  end
                when 2..11
                  number_of_rooms = "#{p.rooms} pièces"
                  if p.withHalf == 1
                    number_of_rooms = "#{p.rooms} 1/2 pièces"
                  end
                else
                  number_of_rooms = "12 pièces et plus"
                end

                xml.NumberOfRooms number_of_rooms
                case p.level
                when nil
                  level = nil
                when 1
                  level = "1er étage"
                when 1..11
                  level = "#{p.level}ème étage"
                else
                  level = "12ème étage et plus"
                end
                xml.RentalFloor level
                xml.Furnished  p.property_inclusion.inclusion_furnished== 1 ? "meublé" : "" if p.property_inclusion.present?
                xml.PetsAllowed p.property_inclusion.pets_allow== 1 ? "oui" : nil if p.property_inclusion.present?
                xml.UnitRentalType p.property_classification.name_fr if p.property_classification_id.present?
                xml.Orientation
                xml.AirConditioning p.property_inclusion.inclusion_air_conditioning== 1 ? "oui" : "" if p.property_inclusion.present?
                xml.HeatingIncluded p.property_inclusion.inclusion_heated== 1 ? "oui" : "" if p.property_inclusion.present?
                xml.HotWaterIncluded p.property_inclusion.inclusion_hot_water == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.ElectricityIncluded p.property_inclusion.inclusion_electricity == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.AdaptedReducedMobility
                xml.WasherDryer p.property_inclusion.inclusion_washer == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.Dishwasher p.property_inclusion.inclusion_dishwasher == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.SubsidizedLodgement
                xml.PatioDoor
                xml.WaterTaxIncluded
                xml.UtilityRoom p.property_inclusion.detail_laundry == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.Pool p.property_inclusion.detail_inside_pool == 1  || p.property_inclusion.detail_outside_pool == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.Sauna p.property_inclusion.detail_sauna == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.Elevator p.property_inclusion.detail_elevator == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.Parking p.property_inclusion.detail_inside_parking == 1  || p.property_inclusion.detail_outside_parking == 1 ? "oui" : "" if p.property_inclusion.present?
                xml.Description ( description.present? ? description[0..950] : "" )
                if p.property_url.blank?
                  unless p.user.profile.website.blank?
                    website_url = my_url_with_protocol(p.user.profile.website,false)
                  end
                else
                  website_url = my_url_with_protocol(p.property_url,false)
                end
                xml.Website website_url unless  website_url.blank?
                xml.URL website_url unless website_url.blank?
                xml.UnitContact do
                  xml.FirstName p.user.profile.public_name
                  xml.LastName
                  xml.Email p.user.profile.email
                  xml.Telephone1 p.user.profile.phone
                  xml.NoteTelephone1
                  xml.Telephone2
                  xml.NoteTelephone2
                end
              end
            end
          end
          xml.shareimmoStatistics do
            xml.updated_at Time.now.strftime("%Y-%m-%d %H:%M:%S")
            xml.ids ids
            xml.count ids.count
          end
        end
      end
      File.open(lespac_file, 'w') do |file|
        file.write builder.to_xml
      end
      if Rails.env.production?
        Net::FTP.open('apps.shareimmo.com',ENV['apps_ftp_username'],ENV['apps_ftp_password']) do |ftp|
          ftp.passive = true
          ftp.putbinaryfile(lespac_file)
        end
      end
      puts "New lespac File Created #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"
    rescue Exception => e
      JulesLogger.info e.message
    end
  end


  task :scrapping => :environment do
    begin
      Capybara.register_driver :poltergeist do |app|
        Capybara::Poltergeist::Driver.new(app, js_errors: false)
      end

      Capybara.javascript_driver  = :poltergeist
      Capybara.default_driver     = :poltergeist

      browser = Capybara.current_session
      puts "Robot IP : #{check_proxy(browser)} - Date : #{Time.now}"
      puts "start lespac:scrapping"
      ##============================================================##
      ## Visit website and go to "Mes annonces"
      ##============================================================##
      website = "https://www.lespac.com/public/registration.jsa"
      puts "Visit website : #{website}"
      browser.visit(website)

      puts "Click to open connection modal"
      browser.find('a.login').click()
      puts "Fill username / password"
      browser.fill_in 'j_username', :with => ENV['lespac_username']
      browser.fill_in 'j_password', :with => ENV['lespac_password']
      puts "Click to connect"
      browser.find(".align .button").click()
      puts "Click to open menu"
      browser.all(:xpath, "//*[@class='mypac']").first.click()
      puts "Click to mes annonces button"
      browser.click_link("Mes annonces")

      ads_count = Array.new
      @current_page = 1
      # @total_pages = browser.find(:xpath, "//*[@class='pagination']/*[@class='center']/*[last()]").text.to_i
      @total_pages = 1
      puts "Nombre de pages total : #{@total_pages}"


      while @current_page <= @total_pages
        puts "Récupération des annonces sur la page #{@current_page}"
        sleep(2)
        browser.all("table.evo-mesAnnoncesListing tr").each do |tr|
          txt1 = tr.text
          if txt1.include?("Client no.: ")
            ad_id         = txt1.split("Client no.: ").last.split(" ").first
            lespac_ad_id  = txt1.split("Annonce PAC no.: ").last.split(" ").first
            property      = Property.find(ad_id)
            ads_count << ad_id
            lespac_ad_url = "http://www.lespac.com/#{I18n.transliterate(property.locality.downcase)}/immobilier-location-logements/a-a-a-a_#{lespac_ad_id}D0.jsa"
            puts "Update id #{ad_id} - preview_id  - id #{lespac_ad_id} - url #{lespac_ad_url}"
            visi = property.visibility_portals.where("portal_id = :portal_id and visibility_until > :date", { :portal_id => Portal.where(:slug => "lespac").first.id,:date => Time.now }).last
            visi.update_attributes(:backlink_id =>lespac_ad_id,:backlink_url =>lespac_ad_url) if visi.present?
          end
        end
        @current_page += 1
        if @current_page <= @total_pages
          puts "Clic pour aller sur la page suivante"
          browser.find(:xpath, "//*[contains(@class, 'nextPage')]").click()
        end
      end

      api_provider  = ApiProvider.where(:name=>"lespac").select(:id).first
      ScrappingMonitoring.create(:api_provider_id=>api_provider.id,:ads_count=>ads_count.size)
    rescue Exception => e
      JulesLogger.info e.message
      screenshot = File.join(screenshots_path,"lespac_#{Time.now.strftime("%Y-%m-%d_%H:%M:%S")}.png")
      browser.save_screenshot(screenshot)
    end
  end


end
