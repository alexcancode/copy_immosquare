class Immopad

  require 'net/ftp'


  include HtmlToPlainTextHelper
  include MoneyHelper


  attr_reader :status


  def initialize(property_id)
    ##============================================================##
    ## Setup Variables
    ##============================================================##
    @property           = Property.find(property_id)
    raise "property blank"  if @property.blank?
    @user               = @property.user
    @portal_activation  = PortalActivation.joins(:portal).where(:user_id=>@user.id,:portals => {:slug => "immopad"}).first
    @status             = false
    @tenant             = @property.property_contacts.joins(:property_contact_type).where(:property_contact_types =>{:slug => "tenant"}).first
    @owner              = @property.property_contacts.joins(:property_contact_type).where(:property_contact_types =>{:slug => "owner"}).first
  end


  def create_xml_and_send_it_to_immopad
    begin
      ##============================================================##
      ## Création du fichier xml
      ##============================================================##
      immopad_path        = File.join("tmp","apps","immopad","#{@user.id}-#{@property.id}_immopad.imo")
      FileUtils.mkdir_p(File.dirname(immopad_path))   unless File.directory?(File.dirname(immopad_path))

      builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
        xml.document do
          xml.lot do
            xml.reference_lot             @property.uid.present? ?  @property.uid : @property.id
            xml.type_bien                 @property.property_classification.name
            xml.sous_type_bien            ''
            xml.designation               @property.uid.present? ?  @property.uid : @property.id
            xml.adresse_ligne_1           "#{@property.street_number} #{@property.street_name}"
            xml.adresse_ligne_2           ''
            xml.code_postal               @property.zipcode
            xml.ville                     @property.locality
            xml.pays                      @property.country_short
            xml.type_contrat_de_location  ''
            xml.reference_contrat         ''
            xml.date_debut_bail           ''
            xml.date_fin_bail             ''
            xml.nb_etages                 (@property.building.present? ? @property.building.number_of_levels : "")
            xml.num_etage                 @property.level
            xml.surface_habitable         (@property.area_living.present? ? @property.area_living : "")
            xml.lot_en_gestion            ''
            xml.lot_en_colocation         ''
            xml.description_logement      convert_to_text(@property.description)
            xml.num_appart                @property.unit
            xml.num_cave                  ''
            xml.date_construction         @property.year_of_built
            xml.chauffage                 (@property.property_inclusion.inclusion_heated == 1 ? 1 : 0)
            xml.production_eau_chaude     (@property.property_inclusion.inclusion_hot_water == 1 ? 1 : 0)
            xml.lot_meuble                (@property.property_inclusion.inclusion_furnished == 1 ? 1 : 0)
          end
          if @owner.present?
            xml.proprietaire do
              xml.reference_proprio         @owner.id
              xml.qualite                   (@owner.civility.present? ? @owner.civility.value : '')
              xml.raison_sociale            ''
              xml.nom                       @owner.last_name
              xml.prenom                    @owner.first_name
              xml.adresse_ligne_1           @owner.address
              xml.adresse_ligne_2           ''
              xml.code_postal               @owner.zipcode
              xml.ville                     @owner.city
              xml.pays                      @owner.country
              xml.telephone                 @owner.phone
              xml.fax                       ''
              xml.portable                  @owner.mobile
              xml.email                     @owner.email
            end
          end
          if @tenant.present?
            xml.locataire do
              xml.reference_proprio         @tenant.id
              xml.qualite                   (@tenant.civility.present? ? @tenant.civility.value : '')
              xml.raison_sociale            ''
              xml.nom                       @tenant.last_name
              xml.prenom                    @tenant.first_name
              xml.adresse_ligne_1           @tenant.address
              xml.adresse_ligne_2           ''
              xml.code_postal               @tenant.zipcode
              xml.ville                     @tenant.city
              xml.pays                      @tenant.country
              xml.telephone                 @tenant.phone
              xml.fax                       ''
              xml.portable                  @tenant.mobile
              xml.email                     @tenant.email
              xml.date_entree               ''
              xml.depot_garantie            ''
              xml.date_debut_bail           ''
              xml.date_fin_bail             ''
              xml.date_preavis              ''
            end
          end
          xml.signature do
            xml.iban_locataire            @tenant.iban
            xml.agence                    (@property.user.profile.agency_name.present? ? @property.user.profile.agency_name : "")
            xml.num_colocataires          ''
            xml.nom_signature_locataire_2 ''
            xml.email_locataire_2         ''
            xml.nom_signature_locataire_3 ''
            xml.email_locataire_3         ''
            xml.nom_signature_locataire_4 ''
            xml.email_locataire_4         ''
            xml.nom_signature_locataire_5 ''
            xml.email_locataire_5         ''
          end
        end
      end

      ##============================================================##
      ## Export du fichier
      ##============================================================##
      File.open(immopad_path, 'w') do |file|
        file.write builder.to_xml
      end

      ##============================================================##
      ## Send to FTP
      ##============================================================##
      Net::FTP.open(@portal_activation.ftp,@portal_activation.login,@portal_activation.password) do |ftp|
        ftp.putbinaryfile(immopad_path)
      end

      @status = true
    rescue Exception => e
      @status = e.message
      JulesLogger.info e.message
    end
  end

end
