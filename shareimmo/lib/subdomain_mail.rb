class SubdomainMail
  def self.matches?(request)
    request.subdomain.present? && request.subdomain == 'mail'
  end
end
