# encoding: utf-8

class DictionaryPopulater

  include HtmlToPlainTextHelper
  include MoneyHelper

  def initialize(args)
    args = args.deep_symbolize_keys
    I18n.locale   = args[:locale]
    @dictionary   = args[:dictionary]
    @property     = Property.find(args[:property_id])
    @profile      = @property.user.profile
    @complement   = PropertyComplement.where(:property_id=>@property.id).first_or_create
    @inclu        = PropertyInclusion.where(:property_id=>@property.id).first_or_create
  end


  ##============================================================##
  ## Send a Populated dictionary
  ## Il faut utiliser .clone sinon on change text[:name]
  ## dans decompose_definition avec slice!
  ##============================================================##
  def populate_dictionary
    if @dictionary[:document].present?
      @dictionary[:document][:primary_color] = @profile.primary_color
    end
    @dictionary[:text].each do |text|
      text[:content] = decompose_definition(text[:name].clone)
    end if @dictionary[:text].present?
    @dictionary[:image].each do |image|
      image[:url] = self.send(image[:name])
    end if @dictionary[:image].present?
    @dictionary
  end


  ##============================================================##
  ## _with_comma
  ## _with_newline
  ## _and_
  ##============================================================##
  def decompose_definition(name)
    with_comma    = name.slice!('_with_comma').present?
    with_newline  = name.slice!('_with_newline').present?
    methods       = name.split('_and_')

    methods.collect! { |meth| self.send(meth) }

    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth}, "
      end
    end if with_comma == true


    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth}\n"
      end
    end if with_newline == true

    final = methods.join((with_comma == true || with_newline == true)  ? "" : " ")
    return final
  end

  ##============================================================##
  ## When Missing property is called
  ##============================================================##
  def method_missing(sym, *args, &block)
    # JulesLogger.info "method missing => #{sym}"
    return nil
  end

















  ##============================================================##
  ## DICTONNARY BELOW
  ##============================================================##


  ##============================================================##
  ## A
  ##============================================================##
  def agent_certificate
    @profile.job
  end

  def agent_email
    @profile.email
  end

  def agent_name
    "#{@profile.first_name} #{@profile.last_name}"
  end

  def agent_phone_plus_label
    "Tél : #{@profile.phone}"
  end

  def agent_photo
    @profile.logo.url(:large,:timestamp=>false)
  end


  ##============================================================##
  ## B
  ##============================================================##
  def broker_bio
    convert_to_text(@property.user.profile.bio)
  end

  def broker_photo
    @profile.picture.url(:large,:timestamp=>false)
  end

  def broker_logo
    @profile.logo.url(:large,:timestamp=>false)
  end

  def broker_agency_name
    @profile.agency_name
  end

  def broker_email
    @profile.email
  end

  def broker_name
    "#{@profile.first_name} #{@profile.last_name}"
  end

  def broker_phone
    @profile.phone
  end

  def broker_website
    @profile.website
  end


  ##============================================================##
  ## F
  ##============================================================##
  def firm_office_address_only
    add = []
    add << @profile.agency_address                                                                                  if @profile.agency_address.present?
    add << "#{@profile.agency_city}#{@profile.agency_zipcode.present? ? ", #{@profile.agency_zipcode}" : nil }"    if(@profile.agency_city.present?)
    add.join("\n")
  end

  def firm_office_name
    @profile.agency_name
  end

  ##============================================================##
  ## I
  ##============================================================##
  def inscription_address1
    "#{@property.street_number} #{@property.street_name}"
  end

  def inscription_address2
    @property.unit
  end

  def inscription_bedroom_count
    @property.bedrooms.present? ? @property.bedrooms : "-"
  end

  def inscription_bathrooms_plus_washrooms
    @property.bathrooms.present? ? @property.bathrooms : "-"
  end

  def inscription_municipality
    @property.locality
  end

  def inscription_price
    @property.property_listing.id == 1 ? humanized_money_with_symbol(@property.price) : "#{humanized_money_with_symbol @property.rent} #{humanized_frequency @property.rent_frequency}"
  end

  def inscription_room_count
    @property.rooms.present? ? @property.rooms : "-"
  end

  def inscription_status
    @property.property_listing.name
  end


  def inscription_title
    @property.title
  end

  def inscription_description
    @property.description_short
  end


  ##============================================================##
  ## M
  ##============================================================##
  def mls_id
    @complement.canada_mls.present? ? @complement.canada_mls : @property.uid
  end

  ##============================================================##
  ## P
  ##============================================================##
  def property_address_2_lines
    line_1 = "#{@property.street_number} #{@property.street_name}"
    line_2 = "#{@property.zipcode} #{@property.locality}"
    "#{line_1.present? ? "#{line_1}\n" : nil }#{line_2}"
  end


  def property_title
    Globalize.with_locale(I18n.locale){@property.title}
  end

  def property_map
    "https://api.mapbox.com/v4/mapbox.streets/pin-m-marker+e3a21d(#{@property.longitude},#{@property.latitude})/#{@property.longitude},#{@property.latitude},14/500x500@2x.png?access_token=#{ENV['MAPBOX_API_TOKEN']}"
  end

  def property_floor
    @property.level
  end

  def property_cover
    @property.property_assets.present? ? @property.property_assets[0].picture.url(:large) : nil
  end


  def property_address
    "#{@property.street_number} #{@property.street_name} #{@property.zipcode} #{@property.locality}"
  end

  def property_price
    @property.property_listing.id == 1 ? humanized_money_with_symbol(@property.price) : "#{humanized_money_with_symbol @property.rent} #{humanized_frequency @property.rent_frequency}"
  end

  def property_classification_listing
    "#{Globalize.with_locale(I18n.locale){@property.property_classification.name}} #{Globalize.with_locale(I18n.locale){@property.property_listing.name}}"
  end

  def property_classification
    Globalize.with_locale(I18n.locale){@property.property_classification.name}
  end

  def property_listing
    Globalize.with_locale(I18n.locale){@property.property_listing.name}
  end

  def property_status
    @property.property_status.present? ? Globalize.with_locale(I18n.locale){@property.property_status.name} : "-"
  end

  def property_area_living
    ((@property.area_living.present? and @property.area_unit.present?) ? "#{@property.area_living.ceil} #{@property.area_unit.symbol}" : "-").html_safe
  end

  def property_area_land
    ((@property.area_land.present? and @property.area_unit.present?) ? "#{@property.area_land.ceil} #{@property.area_unit.symbol}" : "-").html_safe
  end

  def property_bathrooms
    @property.bathrooms.present? ? @property.bathrooms : "-"
  end

  def property_bedrooms
    @property.bedrooms.present? ? @property.bedrooms : "-"
  end

  def property_mensual_fees
    @property.fees.present? ? humanized_money_with_symbol(@property.fees) : "-"
  end

  def property_reference
    @property.uid.present? ? "##{@property.uid}" : "##{@property.id}"
  end

  def property_available_date
    "#{I18n.t('front_office.availability')} : #{@property.availability_date.present? ? I18n.localize(@property.availability_date.to_time,:format=>("%d %B %Y")) : I18n.t('front_office.immediately')}"
  end

  def property_link
    @property.property_url.present? ? @property.property_url : "https://www.shareimmo.com/ad/#{@property.id}"
  end

  def property_description
    convert_to_text(Globalize.with_locale(I18n.locale){@property.description})
  end

  def property_description_short
    convert_to_text(Globalize.with_locale(I18n.locale){@property.description_short})
  end

  def property_cover_url
    @property.property_assets.present? ? @property.property_assets[0].picture.url(:large,:timestamp=>false) : PropertyAsset.new.picture.options[:default_url]
  end

  def property_france_dpe_indice
    @complement.france_dpe_indice.present? ?  @complement.france_dpe_indice : "-"
  end

  def property_france_dpe_value
    @complement.france_dpe_value.present? ?  @complement.france_dpe_value : "-"
  end

  def property_france_ges_indice
    @complement.france_ges_indice.present? ?  @complement.france_ges_indice : "-"
  end

  def property_france_ges_value
    @complement.france_ges_value.present? ?  @complement.france_ges_value : "-"
  end

  def property_france_alur_is_condo
    @complement.france_alur_is_condo == 1 ?  I18n.t('back_office.yes_oui') : I18n.t('back_office.no_non')
  end

  def property_france_alur_units
    @complement.france_alur_units.present? ?  @complement.france_alur_units : "-"
  end

  def property_france_alur_uninhabitables
    @complement.france_alur_uninhabitables.present? ?  @complement.france_alur_uninhabitables : "-"
  end

  def property_france_alur_spending
    @complement.france_alur_spending.present? ?  @complement.france_alur_spending : "-"
  end

  def property_france_alur_legal_action
    @complement.france_alur_legal_action == 1 ?  I18n.t('back_office.yes_oui') : I18n.t('back_office.no_non')
  end

  def property_furnished
    @inclu.inclusion_furnished.present? ? (@inclu.inclusion_furnished == 1 ? I18n.t('back_office.yes_oui') : I18n.t('back_office.no_non')) : "-"
  end

  def property_parking
    (@inclu.detail_inside_parking.present? or @inclu.detail_outside_parking.present?) ? ((@inclu.detail_inside_parking == 1 or @inclu.detail_outside_parking == 1) ?  I18n.t('back_office.yes_oui') : I18n.t('back_office.no_non')) : "-"
  end

  def property_terrace
    @inclu.exterior_terrace.present? ? (@inclu.exterior_terrace == 1 ? I18n.t('back_office.yes_oui') : I18n.t('back_office.no_non')) : "-"
  end

  def property_garden
    @inclu.exterior_garden.present? ? (@inclu.exterior_garden == 1 ? I18n.t('back_office.yes_oui') : I18n.t('back_office.no_non')) : "-"
  end

  def property_pool
    (@inclu.detail_inside_pool.present? or @inclu.detail_outside_pool.present?) ? ((@inclu.detail_inside_pool == 1 or @inclu.detail_outside_pool == 1) ? I18n.t('back_office.yes_oui') : I18n.t('back_office.no_non')) : "-"
  end

  def property_locality
    @property.locality
  end

  def property_sublocality
    @property.sublocality
  end

  def property_mandat_exclusif
    @complement.france_mandat_exclusif == 1 ? t('activerecord.attributes.france_mandat_exclusif') : nil
  end


  def property_equipments
    text = ""
    [:inclusion_air_conditioning,:inclusion_hot_water,:inclusion_heated,:inclusion_electricity,:inclusion_furnished,:inclusion_fridge,:inclusion_cooker,:inclusion_dishwasher,:inclusion_washer,:inclusion_dryer,:inclusion_microwave].each do |i|
      text += "#{I18n.t("activerecord.attributes.property_inclusion.#{i}")}\n" if @inclu[i] == 1
    end
    return text
  end









end
