class TwitterPoster

  include Twitter::Validation

  def initialize(args)
    @property        = Property.find(args[:property_id])
    @property_assets = @property.property_assets
    @stat            = Statistic.where(:user_id =>@property.user_id).first
    @auth_twitter    = Authentification.joins(:api_provider).where(:user_id=>@property.user_id,:api_providers => { :name =>'twitter'}).first
    @message         = args[:message]
  end

  def post_on_twitter
    if @auth_twitter.present?
      @stat.update_attribute(:twitter_count,@stat[:twitter_count]+1)
      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV['TWITTER_API_KEY']
        config.consumer_secret     = ENV['TWITTER_API_SECRET']
        config.access_token        = @auth_twitter.token
        config.access_token_secret = @auth_twitter.token_secret
      end

      media_ids = Array.new
      if @property_assets.present?
        twitter_folder = "#{Rails.root}/tmp/twitter/#{@property.id}"
        FileUtils.mkdir_p(twitter_folder) unless File.exists?(twitter_folder)
        @property_assets.each_with_index do |asset,n|
          if n < 4
            twitter_image = "#{twitter_folder}/twitter_#{asset.id}#{File.extname(asset.picture.url(:large,:timestamp => false))}"
            open(twitter_image,'wb') { |file| file.write HTTParty.get(asset.picture.url(:large,:timestamp=>false)).parsed_response }
            t_image = client.upload(File.new(twitter_image))
            media_ids << t_image
          end
        end
        FileUtils.rm_rf(Dir.glob(twitter_folder))
      end

        ##============================================================##
        ## Si @twitter_limit > 120... on recoit une erreur de Twitter
        ## Status is over 140 characters...
        ## surement à cause des photos  du type pic.twitter.com/3VZjlZQecb
        ##============================================================##
        @twitter_limit = 120
        len = tweet_length(@message)
        while len >= @twitter_limit
          len = len-1
          @message = @message[0..len]
        end
        client.update(@message,:media_ids => media_ids.join(','))
      end
    end
  end







