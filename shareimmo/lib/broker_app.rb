class BrokerApp
  def self.matches?(request)
    domain_with_port = "#{request.domain}#{(request.port.present? and Rails.env.development?) ? ":#{request.port}" : nil}"
    my_domains       = Provider.all.pluck(:hosts).split(',').join(',')
    return !(my_domains.include?(domain_with_port) and (request.subdomain.blank? || request.subdomain == "www" || request.subdomain == "mail"))
  end
end
