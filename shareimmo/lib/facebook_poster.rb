class FacebookPoster


  def initialize(args)
    @property       = Property.find(args[:property_id])
    @auth_facebook  = Authentification.joins(:api_provider).where(:user_id=>@property.user_id,:api_providers => { :name =>'facebook'}).first
    @stat           = Statistic.where(:user_id => @property.user_id).first
    @myself         = args[:myself]
    @message        = args[:message]
    @host_url       = args[:host_url]
    @locale         = args[:locale]
    @pages          = args[:pages]
  end

  def post_on_facebook
    user_graph = Koala::Facebook::API.new(@auth_facebook.token)
    if @auth_facebook.present? &&  @myself == true
      @stat.update_attribute(:facebook_count,@stat[:facebook_count]+1)
      user_graph.put_connections("me", "feed",
        :message      => @message,
        :link         => "http://#{@host_url}/#{@locale}/ad/#{@property.id}",
        :caption      => @property.user.profile.public_name,
        :description  => ActionController::Base.helpers.strip_tags(Globalize.with_locale(@locale){@property.description}),
        :picture      => @property.cover_url_large
        )
    end
    if @auth_facebook.present? && @pages.present?
      @pages.each do |page_id|
        @stat.update_attribute(:facebook_count,@stat[:facebook_count]+1)
        page_token = user_graph.get_page_access_token(page_id)
        page_graph = Koala::Facebook::API.new(page_token)
        page_graph.put_connections(page_id, 'feed',
          :message      => @message,
          :link         => "http://#{@host_url}/#{@locale}/ad/#{@property.id}",
          :caption      => @property.user.profile.public_name,
          :description  => ActionController::Base.helpers.strip_tags(Globalize.with_locale(@locale){@property.description}),
          :picture      => @property.cover_url_large
          )
      end
    end
  end
end

