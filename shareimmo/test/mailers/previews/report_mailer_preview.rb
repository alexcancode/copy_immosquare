class ReportMailerPreview < ActionMailer::Preview


  def notification_to_prospect
    report = FeedbackReport.last
    ReportMailer.notification_to_prospect(report)
  end

  def notification_to_owner
    report = FeedbackReport.last
    ReportMailer.notification_to_owner(report)
  end

  def notification_to_myself
    report = FeedbackReport.last
    ReportMailer.notification_to_myself(report)
  end


  def mail_reminder
    feedback = Feedback.last
    ReportMailer.mail_reminder(feedback)
  end

end
