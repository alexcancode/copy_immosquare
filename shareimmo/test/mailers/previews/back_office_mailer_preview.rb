class BackOfficeMailerPreview < ActionMailer::Preview

  def new_user_registred
    user = User.last
    BackOfficeMailer.new_user_registred(user)
  end


  def new_service_activation_required
    contact = Contact.where(:activation_required => 1).first
    BackOfficeMailer.new_service_activation_required(contact)
  end

  def new_order_placed
    services_order = ServiceOrder.last
    BackOfficeMailer.new_order_placed(services_order)
  end


  def new_portal_added
    portal = Portal.first
    BackOfficeMailer.new_portal_added(portal,"pagesimmo")
  end


end
