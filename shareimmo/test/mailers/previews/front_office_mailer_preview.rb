class FrontOfficeMailerPreview < ActionMailer::Preview


  def broker_contact
    contact = BrokerContact.first
    FrontOfficeMailer.broker_contact(contact)
  end

  def sms_send
    contact = BrokerContact.first
    FrontOfficeMailer.sms_send(contact)
  end

  def new_testimonial_notification
    FrontOfficeMailer.new_testimonial_notification(Testimonial.first)
  end


end
