class TaskMailer <  ApplicationMailer

  default :template_path => "mailers/task_mailer"


  def visibility_no_credit(user, property)
    @property = property
    @user = user
    @title = t('mailer.no_credit_visibility.title')
    mail(
     :to => @user.email,
     :bcc => "contact@immosquare.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end

  def visibility_warning(user, property)
    @property = property
    @user = user
    @title = t('mailer.warning_visibility.title',:id => @property.id)
    mail(
     :to => @user.email,
     :bcc => "contact@immosquare.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end

  def jarinux_mailer(properties)
    @properties = properties
    @title = "Daily search"
    mail(
     :to => "jarinux@nion.email",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
    )
  end

  def prestige_mailer(infos)
    @infos = infos
    @title = "Prestige import"
    mail(
     :to => "it@immosquare.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
    )
  end  




end
