class ApplicationMailer < ActionMailer::Base

  before_filter :set_website
  layout 'mailer'
  include MailerHelper
  add_template_helper(MailerHelper)


  def set_website
    begin
      domain_with_port = "#{request.domain}#{(request.port.present? and Rails.env.development?) ? ":#{request.port}" : nil}"
      provider = Provider.where("hosts LIKE ?", "%#{domain_with_port}%").first
      @my_provider = provider.present? ? provider : Provider.first
    rescue
      @my_provider = Provider.first
    end
  end

  def default_url_options(options = {})
    {:locale => I18n.locale, :host => request.present? ? request.base_url : @my_provider.link_host}
  end



end

