class ReportMailer < ApplicationMailer

  default :template_path => "mailers/report_mailer"

  def notification_to_prospect(report)
    @contact_1 = JSON.parse(report.contact_1)
    @property = report.feedback.property
    @title = t('mailer.reports.title',:id => @property.id)
    mail(
     :to => @contact_1["email"],
     :cc => "#{@property.user.profile.email}",
     :bcc => "contact@immosquare.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end


  def notification_to_owner(report)
    @report = report
    @property = report.feedback.property
    @title = t('mailer.reports.title',:id => @property.id)
    mail(
     :to => JSON.parse(@report.contact_2)["email"],
     :cc => "#{@property.user.profile.email}",
     :bcc => "contact@immosquare.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end

  def notification_to_myself(report)
    @report = report
    @property = report.feedback.property
    @title = t('mailer.reports.title',:id => @property.id)
    mail(
     :to => JSON.parse(@report.contact_3)["email"],
     :from => "no-reply@shareimmo.com",
     :subject => set_my_subject(@title)
     )
  end


  def mail_reminder(feedback)
    @feedback = feedback
    @property = feedback.property
    @title = t('mailer.feedback_alert.title')
    mail(
      :to => feedback.email,
      :cc => @property.user.profile.email,
      :bcc => "contact@immosquare.com",
      :from => set_my_from('no-reply'),
      :subject => set_my_subject(@title)
     )
  end


end
