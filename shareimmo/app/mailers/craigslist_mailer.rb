class CraigslistMailer < ApplicationMailer

  layout 'mailer_craigslist'
  default :template_path => "mailers/craigslist_mailer"

  def ad_removed(visibility)
    @title = "Craigslist - Property #{visibility.property_id} removed"
    @visibility = visibility
    mail(
      :to      => "it@immosquare.com",
      :from    => set_my_from('no-reply'),
      :subject => set_my_subject(@title)
      )

  end

  def overquota_properties(op)
    @title = "Craigslist - overquota properties"
    @properties = op
    mail(
      :to         => "it@immosquare.com",
      :from    => set_my_from('no-reply'),
      :subject => set_my_subject(@title)
      )    
  end


  def craigslist_notification(user_email,content,reply_to)
    @content = content
    @title = "craigslist"
    mail(
      :to         => user_email,
      :reply_to   => reply_to,
      :bcc        => "it@immosquare.com",
      :from       => set_my_from('reply'),
      :subject    => set_my_subject(@title)
      )
  end

  def craigslist_error(exception_location,exception,screenshot)
    @title      = exception_location
    @exception  = exception
    @screenshot = screenshot
    attachments[File.basename(@screenshot)] = File.read(@screenshot) if File.exist?(@screenshot)
    mail(
      :to       => "error@shareimmo.com",
      :from     => set_my_from('no-reply'),
      :subject  => set_my_subject(@title)
      )
  end

  def validation_required(title,screenshot)
    @title      = title
    @screenshot = screenshot
    attachments[File.basename(@screenshot)] = File.read(@screenshot) if File.exist?(@screenshot)
    mail(
      :to       => "shareimmo@mailhubimmo.com",
      :from     => set_my_from('no-reply'),
      :subject  => set_my_subject(@title)
      )
  end

end
