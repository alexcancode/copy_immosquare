class ContactMailer < ApplicationMailer

  default :template_path => "mailers/contact_mailer"

  def contact(contact)
    @contact = contact
    mail(
     :to => "contact@immosquare.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject("Demande information - formulaire contact")
     )
  end

end
