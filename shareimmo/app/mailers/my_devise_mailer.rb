class MyDeviseMailer < Devise::Mailer

  def confirmation_instructions(record, token, opts={})
    opts[:from] = set_my_from('no-reply')
    opts[:reply_to] = set_my_from('no-reply')
    super
  end


  def reset_password_instructions(record, token, opts={})
    opts[:from] = set_my_from('no-reply')
    opts[:reply_to] = set_my_from('no-reply')
    super
  end

  def unlock_instructions(record, token, opts={})
    opts[:from] = set_my_from('no-reply')
    opts[:reply_to] = set_my_from('no-reply')
    super

  end

  def password_change(record, opts={})
    opts[:from] = set_my_from('no-reply')
    opts[:reply_to] = set_my_from('no-reply')
    super
  end


end
