class BackOfficeMailer < ApplicationMailer

  layout 'mailer_craigslist'
  default :template_path => "mailers/back_office_mailer"

  def new_user_registred(user)
    @user = user
    @title = "New user registred"
    mail(
      :to => "api@shareimmo.com",
      :from => set_my_from('no-reply'),
      :subject => set_my_subject(@title)
      )
  end


  def new_service_activation_required(contact)
    @contact = contact
    mail(
      :to => "contact@immosquare.com",
      :from => set_my_from('no-reply'),
      :subject => set_my_subject("Service Activation Required")
      )
  end

  def new_order_placed(service_order)
    @service_order = service_order
    mail(
      :to => ["allison@immosquare.com","gael@immosquare.com","jerome@immosquare.com"],
      :cc => "contact@immosquare.com",
      :from => set_my_from('no-reply'),
      :subject => set_my_subject("Service Order placed")
      )
  end


  def new_portal_added(portal, provider)
    @portal = portal
    @provider = provider
    mail(
     :to => "it@immosquare.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject("New portal added from provider #{@provider}")
     )
  end


  def new_exception(exceptions, provider)
    @provider = provider
    @exceptions = exceptions
    mail(
     :to => "error@shareimmo.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject("An error occured for #{@provider}")
     )
  end


end
