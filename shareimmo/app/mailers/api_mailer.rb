# encoding: utf-8

class ApiMailer < ApplicationMailer

  layout 'mailer_craigslist'
  default :template_path => "mailers/api_mailer"

  require 'open-uri'
  OpenURI::Buffer.send :remove_const, 'StringMax' if OpenURI::Buffer.const_defined?('StringMax')
  OpenURI::Buffer.const_set 'StringMax', 0


  def error_monitoring(args)
    @error_message  = args[:message]
    @error_title    = args[:title]
    @screenshot     = args[:screenshot]
    @params         = args[:params]

    attachments[File.basename(@screenshot)] = File.read(@screenshot) if(@screenshot.present? and File.exist?(@screenshot))
    mail(
     :to      => "error@shareimmo.com",
     :from    => set_my_from('no-reply'),
     :subject => set_my_subject("API|error|#{@error_title}")
     )
  end





  def new_property_visibilities(visiblity_infos)
    @visiblity_infos = visiblity_infos
    @title = "new Visibility From API"
    mail(
     :to => "api@shareimmo.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end


  def new_users_deleted(accounts_infos)
    @accounts_infos = accounts_infos
    @title = "new Users Deleted from API"
    mail(
     :to => "api@shareimmo.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end


  def video_uploaded_on_youtube(video)
    @video = video
    @title = "New video uploaded  on Youtube"
    mail(
     :to => "api@shareimmo.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end

  def video_removed_on_youtube(video_ids)
    @video_ids = video_ids
    @title = "Unused videos was removed on Youtube"
    mail(
     :to      => "api@shareimmo.com",
     :from    => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end

  def new_services_user_activated(services_user, mode)
    @services_user = services_user
    @title = "Service #{@services_user.service.name} activé via #{mode}"
    mail(
     :to => "api@shareimmo.com",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject(@title)
     )
  end


end
