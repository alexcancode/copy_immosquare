class KijijiMailer < ApplicationMailer

  layout 'mailer_craigslist'
  default :template_path => "mailers/kijiji_mailer"


  def kijiji_error(exception_location,exception,screenshot)
    @title      = exception_location
    @exception  = exception
    @screenshot = screenshot
    attachments[File.basename(@screenshot)] = File.read(@screenshot) if File.exist?(@screenshot)
    mail(
      :to => "error@shareimmo.com",
      :from => set_my_from('no-reply'),
      :subject => set_my_subject(@title),
      )
  end

  def validation_required(title,screenshot)
    @title      = title
    @screenshot = screenshot
    attachments[File.basename(@screenshot)] = File.read(@screenshot) if File.exist?(@screenshot)
    mail(
      :to => "shareimmo@mailhubimmo.com",
      :from => set_my_from('no-reply'),
      :subject => set_my_subject(@title),
      )
  end

end
