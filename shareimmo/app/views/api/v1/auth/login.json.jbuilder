@profile = @user.profile
json.success true
json.data do
  json.accountUid @user.id
  json.uid @user.uid
  json.api_kangalou_url Settings.api.kangalou_url
  json.api_shareimmo_url Settings.api.shareimmo_url
  json.nbTokens @kangalou_params["nbTokens"] if @kangalou_params["nbTokens"].present?
  json.encrypted_password @kangalou_params["encryptedpassword"] if @kangalou_params["encryptedpassword"].present?
  json.email_address @user.email
  json.first_name @profile.first_name
  json.last_name @profile.last_name
  json.signature nil
  json.tokens_count @user.token_count
  json.public_name @profile.public_name
  json.phone @profile.phone
  json.avatar avatar_to_display(@user,100)
  json.spoken_languages @profile.spoken_languages
  json.job @profile.job
  json.website @profile.website
  json.bio  @profile.bio.present? ? simple_format(@profile.bio) : ""
  if @api_app.present?
    json.auth do
      json.api_key @api_app.key
      json.api_token @api_app.token
    end
  end
end
