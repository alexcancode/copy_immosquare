json.success true

json.nomenclatures @nomenclatures do |n|
  json.id n.id
  json.name n.name
  json.description n.description
  json.elements n.user_id.blank? ? n.nomenclature_element_defaults :  n.nomenclature_elements  do |e|
    json.id e.id
    json.name e.name
    json.value (e.value.nil? ? "" : e.value)
    json.position e.position
    json.nomenclature_id n.user_id.blank? ? e.nomenclature_default_id :  e.nomenclature_id
    json.type e.element_type
    json.parent_id (e.parent_id ? e.parent_id : 0)
  end
end

json.glossary (@glossary.glossary_elements.nil? ? [] : @glossary.glossary_elements.pluck(:content))

json.parameters do
  if @user.inventory_parameter.present?
    json.default_nomenclature (@user.inventory_parameter.nomenclature_id.present? ? @user.inventory_parameter.nomenclature_id : @nomenclatures.first.id)
    json.state_1 (@user.inventory_parameter.state1_name ? @user.inventory_parameter.state1_name : t("immopad.default_parameters.state1_name"))
    json.state_2 (@user.inventory_parameter.state2_name ? @user.inventory_parameter.state2_name : t("immopad.default_parameters.state2_name"))
    json.state_3 (@user.inventory_parameter.state3_name ? @user.inventory_parameter.state3_name : t("immopad.default_parameters.state3_name"))
    json.state_4 (@user.inventory_parameter.state4_name ? @user.inventory_parameter.state4_name : t("immopad.default_parameters.state4_name"))
    json.state_5 (@user.inventory_parameter.state5_name ? @user.inventory_parameter.state5_name : t("immopad.default_parameters.state5_name"))
    json.state_1_label (@user.inventory_parameter.state1_label ? @user.inventory_parameter.state1_label : t("immopad.default_parameters.state1_label"))
    json.state_2_label (@user.inventory_parameter.state2_label ? @user.inventory_parameter.state2_label : t("immopad.default_parameters.state2_label"))
    json.state_3_label (@user.inventory_parameter.state3_label ? @user.inventory_parameter.state3_label : t("immopad.default_parameters.state3_label"))
    json.state_4_label (@user.inventory_parameter.state4_label ? @user.inventory_parameter.state4_label : t("immopad.default_parameters.state4_label"))
    json.state_5_label (@user.inventory_parameter.state5_label ? @user.inventory_parameter.state5_label : t("immopad.default_parameters.state5_label"))
    json.pdf_introduction_text (@user.inventory_parameter.pdf_introduction_content ? @user.inventory_parameter.pdf_introduction_content : "" )
    json.pdf_introduction_sentence (@user.inventory_parameter.pdf_introduction_title ? @user.inventory_parameter.pdf_introduction_title : "" )
    json.pdf_free_section_title (@user.inventory_parameter.pdf_free_section_title ? @user.inventory_parameter.pdf_free_section_title : "" )
    json.pdf_free_section_content (@user.inventory_parameter.pdf_free_section_content ? @user.inventory_parameter.pdf_free_section_content : "")
  else
    json.default_nomenclature @nomenclatures.first.id
    json.state_1 t("immopad.default_parameters.state1_name")
    json.state_2 t("immopad.default_parameters.state2_name")
    json.state_3 t("immopad.default_parameters.state3_name")
    json.state_4 t("immopad.default_parameters.state4_name")
    json.state_5 t("immopad.default_parameters.state5_name")
    json.state_1_label t("immopad.default_parameters.state1_label")
    json.state_2_label t("immopad.default_parameters.state2_label")
    json.state_3_label t("immopad.default_parameters.state3_label")
    json.state_4_label t("immopad.default_parameters.state4_label")
    json.state_5_label t("immopad.default_parameters.state5_label")
  end
end
