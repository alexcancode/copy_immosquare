json.buildings @buildings do |b|
  if b.properties.count > 0
    json.id b.id
    json.name b.name
    json.address_full b.address
    json.address "#{b.street_number} #{b.street_name}"
    json.street_number b.street_number
    json.street_name b.street_name
    json.zipcode b.zipcode
    json.city b.locality

	json.picture_content Base64.encode64(open(b.building_assets[0].picture.url(:medium)) { |io| io.read }) if b.building_assets.present?
	json.picture_url b.building_assets[0].picture.url(:medium) if b.building_assets.present?

    json.properties b.properties do |p|
      json.id p.id
      json.status p.status
      json.title p.title
      json.area_living p.area_living
      json.address_full p.address

      json.inventories p.inventories do |i|
        json.id i.id
        json.designation i.designation
        json.xml i.xml.url
      end
    end
  end
end


