if @property.present?
  json.status 1
  json.id         @property.id
  json.uid        @property.uid
  json.uid2       @property.uid2
  json.created_at @property.created_at
  json.updated_at @property.updated_at

  youtube_vid       = @property.videos.last
  vp_louer          = @property.visibility_portals.joins(:portal).where(:posting_status =>1, :portals =>{:slug => "louer"}).last
  vp_lespac         = @property.visibility_portals.joins(:portal).where(:posting_status =>1, :portals =>{:slug => "lespac"}).last
  vp_craigslist     = @property.visibility_portals.joins(:portal).where(:posting_status =>1, :portals =>{:slug => "craigslist"}).last
  vp_international  = @property.visibility_portals.joins(:portal).where(:posting_status =>1, :portals =>{:slug => "listglobally"}).last


  json.visibility do
    json.visibility_start  @property.visibility_portals.present? ? @property.visibility_portals.last.visibility_start : nil
    json.visibility_until  @property.visibility_portals.present? ? @property.visibility_portals.last.visibility_until : nil
    json.youtube do
      json.status   youtube_vid.present? ? 1 : 0
      json.id       youtube_vid.present? ? youtube_vid.youtube_id : nil
      json.backlink youtube_vid.present? ? "https://www.youtube.com/watch?v=#{youtube_vid.youtube_id}" : nil
    end
    json.louer do
      json.status   (vp_louer.present? && vp_louer.visibility_until > Time.now) ? vp_louer.posting_status : nil
      json.id       (vp_louer.present? && vp_louer.visibility_until > Time.now) ? vp_louer.backlink_id : nil
      json.backlink (vp_louer.present? && vp_louer.visibility_until > Time.now) ? "http://www.louer.com/#{vp_louer.backlink_id}" : nil
      json.backlinks do
        json.fr   (vp_louer.present? && vp_louer.visibility_until > Time.now) ? "http://www.louer.ca/#{vp_louer.backlink_id}" : nil
        json.en   (vp_louer.present? && vp_louer.visibility_until > Time.now) ? "http://www.louer.com/#{vp_louer.backlink_id}" : nil
      end
    end
    json.lespac do
      json.status     (vp_lespac.present? && vp_lespac.visibility_until > Time.now) ? vp_lespac.posting_status : nil
      json.backlink   (vp_lespac.present? && vp_lespac.visibility_until > Time.now) ? vp_lespac.backlink_url : nil
    end
    json.craigslist do
      json.status     (vp_craigslist.present? && vp_craigslist.visibility_until > Time.now) ? vp_craigslist.posting_status : nil
      json.backlink   (vp_craigslist.present? && vp_craigslist.visibility_until > Time.now) ? vp_craigslist.backlink_url : nil
    end
    json.international do
      json.status   vp_international.present? ? vp_international.posting_status : nil
      json.backlink api_v2_international_url(:u =>@property.user_id,:p => @property.id,:locale => nil)  if(@listglobally_link.present? and vp_international.present? and vp_international.posting_status == 1)
    end
  end
else
 json.status 0
 json.error "Not Found"
end
