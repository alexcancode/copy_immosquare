json.env Rails.env
json.elastic do
  json.property do
    json.index_name Property.index_name
    json.document_type Property.document_type
  end
end
