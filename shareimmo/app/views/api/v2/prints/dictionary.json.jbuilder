json.dictionary @dictionary
json.pictures do
  json.array! @property.property_assets do |asset|
    json.medium   asset.picture? ? asset.picture.url(:medium)  : PropertyAsset.new.picture.options[:default_url]
    json.large    asset.picture? ? asset.picture.url(:large)   : PropertyAsset.new.picture.options[:default_url]
  end
end
