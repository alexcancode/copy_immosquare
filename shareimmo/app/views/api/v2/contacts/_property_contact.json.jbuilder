api_uid = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:user_id => @property_contact.user_id).first

json.id           @property_contact.id
json.uid          @property_contact.uid
json.account_uid  api_uid.uid
json.email        @property_contact.email
json.civility     @property_contact.civility_id
json.first_name   @property_contact.first_name
json.last_name    @property_contact.last_name
json.phone        @property_contact.phone
json.mobile       @property_contact.mobile
json.type         @property_contact.property_contact_type_id
json.iban        @property_contact.iban
json.address do
  json.street_name  @property_contact.address
  json.city         @property_contact.city
  json.zipcode      @property_contact.zipcode
  json.city         @property_contact.city
  json.country      @property_contact.country
end
json.picture_url @property_contact.original_picture_url
