api_uid = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:user_id => @property_contact.user_id).first

json.id           @property_contact.id
json.uid          @property_contact.uid
json.account_id   @property_contact.user_id
json.account_uid  api_uid.uid
json.message "Contact deletion was launched"
