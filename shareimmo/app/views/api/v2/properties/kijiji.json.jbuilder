json.property_id    @property.id
json.visibility_id  @visibility_portal.present? ? @visibility_portal.id : nil
json.sell_or_rent   @property.property_listing_id == 1 ? "sell" : "rent"
json.home_link      @home_link
json.is_home?       @property.property_classification.api_id == 101 ? true : false

json.contact do
  json.phone @property.user.profile.phone_visibility == 1 ?  @property.user.profile.phone : "DO NOT DISPLAY"
  json.name  @property.user.profile.public_name.present? ? @property.user.profile.public_name : "shareIMMO"
end


json.ad do
  @title  = "#{@property.property_classification.name_kangalou} - #{@property.rooms} #{@property.withHalf == 1 ? "1/2" : ""} - #{@property.locality}"
  @body   = convert_to_text("#{@property.description_fr.present? ? "#{@property.description_fr}#{@property.description_en.present? ? "\n\n############\n\n" : nil }" : nil}#{@property.description_en.present? ? @property.description_en : nil }")
  json.title    @title
  json.zipcode  @property.zipcode
  json.body     @body.present? ? "#{@body}\n\nSI-#{@visibility_portal.id}-#{@property.id}" : "#{@title}\n\nSI-#{@visibility_portal.id}-#{@property.id}"
end


json.details do
  json.area @property.area_living.present? ? @property.area_unit_id == 2 ? "#{@property.area_living.ceil} pc" : "#{(@property.area_living*10.76).ceil} pc" : nil
  json.rent "#{money_without_cents(@property.rent)}"  if @property.rent.present?
  json.sell "#{money_without_cents(@property.price)}" if @property.price.present?
  json.availability_date do
    json.day    @property.availability_date.present?  ? I18n.localize(@property.availability_date.to_time,:format=>("%d")) : I18n.localize(Time.now,:format=>("%d"))
    json.month  @property.availability_date.present?  ? I18n.localize(@property.availability_date.to_time,:format=>("%m")) : I18n.localize(Time.now,:format=>("%m"))
    json.year   @property.availability_date.present?  ? I18n.localize(@property.availability_date.to_time,:format=>("%y")) : I18n.localize(Time.now,:format=>("%y"))
  end
  json.bedrooms @property.bedrooms
  json.bathrooms @property.bathrooms
  json.type (@property.property_classification.kijiji_match.blank? ? 37 : @property.property_classification.kijiji_match)
  json.buanderie "NO ATTRIBUT"
  json.parking @property.property_inclusion.detail_inside_parking == 1 ? "Inside Parking" :  (@property.property_inclusion.detail_outside_parking == 1 ? "Outside Parking" : ( @property.property_inclusion.detail_parking_on_street == 1 ? "Parking On Street" :  "NO ATTRIBUT"))
end

json.details2 do
  json.cats @property.property_inclusion.pets_cat == 1 ? true : false
  json.dogs (@property.property_inclusion.pets_dog == 1 || @property.property_inclusion.pets_little_dog == 1) ? true : false
  json.furnished @property.property_inclusion.inclusion_furnished == 1 ? true : false
  json.no_smoking "NO ATTRIBUT"
  json.accessibility @property.property_inclusion.accessibility_elevator == 1 ? true : false
end



json.address do
  json.zipcode @property.zipcode
  json.street_number @property.street_number
  json.street_name @property.street_name
  json.city @property.locality
  json.formated_street "#{@property.street_number} #{@property.street_name}, #{@property.zipcode}"
end



json.pictures do
  json.array! @property.property_assets do |asset|
    json.large_url asset.picture? ? asset.picture.url(:large,:timestamp => false) : "ERROR"
  end
end

