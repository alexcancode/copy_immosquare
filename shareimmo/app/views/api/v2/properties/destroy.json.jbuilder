api_uid = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:user_id => @property.user_id).first

json.id           @property.id
json.uid          @property.uid
json.account_id   @property.user_id
json.account_uid  api_uid.uid
json.message "Property deletion was launched"
