@building   = @property.building || Building.new
@complement = @property.property_complement
@inclu      = @property.property_inclusion
api_uid = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:user_id => @property.user_id).first

json.id                 @property.id
json.uid                @property.uid
json.master             @property.master
json.account_id         @property.user_id
json.account_uid        api_uid.present? ? api_uid.uid : nil


json.building do
    json.id           @building.id
    json.uid          @building.uid
    json.name         @building.name
    json.cover_url    @building.building_assets.present? ? @building.building_assets[0].picture.url(:large,:timestamp => false) : BuildingAsset.new.picture.options[:default_url]
    json.cover_url_hd @building.building_assets.present? ? @building.building_assets[0].picture.url(:hd,:timestamp => false) : BuildingAsset.new.picture.options[:default_url]
end


json.online             @property.status
json.classification_id  @property.property_classification.api_id

json.classification do
  json.fr @property.property_classification.name_fr
  json.en @property.property_classification.name_en
  json.category do
    json.id @property.property_classification.property_classification_category.id
    json.fr @property.property_classification.property_classification_category.name_fr
    json.en @property.property_classification.property_classification_category.name_en
  end if @property.property_classification.property_classification_category.present?
end

json.status_id @property.property_status_id
json.status do
  json.fr @property.property_status.name_fr
  json.en @property.property_status.name_en
end if @property.property_status.present?

json.listing_id @property.property_listing_id
json.listing do
  json.fr @property.property_listing.name_fr
  json.en @property.property_listing.name_en
end if @property.property_listing.present?

json.unit               @property.unit
json.level              @property.level
json.cadastre           @property.cadastre

json.flag_id @property.property_flag_id
json.flag do
  json.fr @property.property_flag.name_fr
  json.en @property.property_flag.name_en
end if @property.property_flag.present?

json.year_of_built      @property.year_of_built
json.availability_date  @property.availability_date
json.is_luxurious       @property.is_luxurious

json.address do
  json.address_visibility         @property.address_visibility
  json.address_formatted          @property.address
  json.street_number              @property.street_number
  json.street_name                @property.street_name
  json.zipcode                    @property.zipcode
  json.locality                   @property.locality
  json.sublocality                @property.sublocality
  json.administrative_area_level1 @property.administrative_area_level_1
  json.administrative_area_level2 @property.administrative_area_level_2
  json.country                    @property.country_short
  json.latitude                   @property.latitude
  json.longitude                  @property.longitude
end


json.area do
  json.living   @property.area_living
  json.balcony  @property.area_balcony
  json.land     @property.area_land
  json.unit_id  @property.area_unit_id
  json.formated do
    json.living   @property.area_living.present?  ?   "#{@property.area_living} #{@property.area_unit.symbol}"   : nil
    json.balcony  @property.area_balcony.present? ?   "#{@property.area_balcony} #{@property.area_unit.symbol}"  : nil
    json.land     @property.area_land.present?    ?   "#{@property.area_land} #{@property.area_unit.symbol}"     : nil
  end
end
json.rooms do
  json.rooms        @property.rooms
  json.bathrooms    @property.bathrooms
  json.bedrooms     @property.bedrooms
  json.showerrooms  @property.showerrooms
end
json.title do
  json.fr @property.title_fr
  json.en @property.title_en
  json.de @property.title_de
  json.nl @property.title_nl
  json.it @property.title_it
  json.es @property.title_es
  json.pt @property.title_pt
end
json.description_short do
  json.fr @property.description_short_fr
  json.en @property.description_short_en
  json.de @property.description_short_de
  json.nl @property.description_short_nl
  json.it @property.description_short_it
  json.es @property.description_short_es
  json.pt @property.description_short_pt
end
json.description do
  json.fr @property.description_fr
  json.en @property.description_en
  json.de @property.description_de
  json.nl @property.description_nl
  json.it @property.description_it
  json.es @property.description_es
  json.pt @property.description_pt
end
json.url do
  json.fr @property.property_url_fr
  json.en @property.property_url_en
  json.de @property.property_url_de
  json.nl @property.property_url_nl
  json.it @property.property_url_it
  json.es @property.property_url_es
  json.pt @property.property_url_pt
end
json.financial do
  json.price_with_tax @property.price_with_tax
  json.price_from     @property.price_from
  json.rent_frequency @property.rent_frequency
  json.currency       @property.currency
  json.price_formated my_beautiful_price(@property)
  json.price          @property.price_cents.present?  ?   @property.price_cents/100   : nil
  json.rent           @property.rent_cents.present?   ?   @property.rent_cents/100    :  nil
  json.fees           @property.fees_cents.present?   ?   @property.fees_cents/100    :  nil
  json.tax1_annual    @property.tax_1_cents.present?  ?   @property.tax_1_cents/100   :  nil
  json.tax2_annual    @property.tax_2_cents.present?  ?   @property.tax_2_cents/100   :  nil
  json.income_annual  @property.income_cents.present? ?   @property.income_cents/100  :  nil
  json.formated do
    json.price          @property.price_cents.present?  ?   humanized_money_with_symbol(@property.price)    : nil
    json.rent           @property.rent_cents.present?   ?   humanized_money_with_symbol(@property.rent)     : nil
    json.fees           @property.fees_cents.present?   ?   humanized_money_with_symbol(@property.fees)     : nil
    json.tax1_annual    @property.tax_1_cents.present?  ?   humanized_money_with_symbol(@property.tax_1)    : nil
    json.tax2_annual    @property.tax_2_cents.present?  ?   humanized_money_with_symbol(@property.tax_2)    : nil
    json.income_annual  @property.income_cents.present? ?   humanized_money_with_symbol(@property.income)   : nil
  end
end


json.cover_url    @property.property_assets.present? ? @property.property_assets[0].picture.url(:large) : PropertyAsset.new.picture.options[:default_url]
json.cover_url_hd @property.property_assets.present? ? @property.property_assets[0].picture.url(:large_hd) : PropertyAsset.new.picture.options[:default_url]


json.pictures do
  if @picture_in_progress.present? && @picture_in_progress == 1
    json.in_progess true
  else
    json.array! @property.property_assets do |asset|
      test = asset.picture?
      json.medium         test ? asset.picture.url(:medium)       : PropertyAsset.new.picture.options[:default_url]
      json.medium_crop    test ? asset.picture.url(:medium_crop)  : PropertyAsset.new.picture.options[:default_url]
      json.medium_hd      test ? asset.picture.url(:medium_hd)    : PropertyAsset.new.picture.options[:default_url]
      json.large          test ? asset.picture.url(:large)        : PropertyAsset.new.picture.options[:default_url]
      json.large_crop     test ? asset.picture.url(:large_crop)   : PropertyAsset.new.picture.options[:default_url]
      json.large_hd       test ? asset.picture.url(:large_hd)     : PropertyAsset.new.picture.options[:default_url]
    end
  end
end


if @complement.present?
  json.canada do
    json.mls @complement.canada_mls
  end
  json.france do
    json.dpe_indice           @complement.france_dpe_indice
    json.dpe_value            @complement.france_dpe_value
    json.dpe_id               @complement.france_dpe_id
    json.ges_indice           @complement.france_ges_indice
    json.ges_value            @complement.france_ges_value
    json.alur_is_condo        @complement.france_alur_is_condo
    json.alur_units           @complement.france_alur_units
    json.alur_uninhabitables  @complement.france_alur_uninhabitables
    json.alur_spending        @complement.france_alur_spending
    json.alur_spending        @complement.france_alur_spending
    json.alur_legal_action    @complement.france_alur_legal_action
    json.exclusive_mandat     @complement.france_mandat_exclusif
  end
  json.belgium do
    json.peb_indice           @complement.belgium_peb_indice
    json.peb_value            @complement.belgium_peb_value
    json.peb_id               @complement.belgium_peb_id
    json.building_permit      @complement.belgium_building_permit
    json.done_assignments     @complement.belgium_done_assignments
    json.preemption_property  @complement.belgium_preemption_property
    json.subdivision_permits  @complement.belgium_subdivision_permits
    json.sensitive_flood_area @complement.belgium_sensitive_flood_area
    json.delimited_flood_zone @complement.belgium_delimited_flood_zone
    json.risk_area            @complement.belgium_risk_area
  end
end

json.videos do
  json.array! @property.videos(:status=>1) do |video|
    json.youtube_id video.youtube_id
  end
end


if @inclu.present?
  json.other do
    json.orientation do
      json.north  @inclu.orientation_north
      json.south  @inclu.orientation_south
      json.east   @inclu.orientation_east
      json.west   @inclu.orientation_west
    end
    json.inclusion do
      json.air_conditioning @inclu.inclusion_air_conditioning
      json.hot_water        @inclu.inclusion_hot_water
      json.heated           @inclu.inclusion_heated
      json.electricity      @inclu.inclusion_electricity
      json.furnished        @inclu.inclusion_furnished
      json.fridge           @inclu.inclusion_fridge
      json.cooker           @inclu.inclusion_cooker
      json.dishwasher       @inclu.inclusion_dishwasher
      json.dryer            @inclu.inclusion_dryer
      json.washer           @inclu.inclusion_washer
      json.microwave        @inclu.inclusion_microwave
    end
    json.detail do
      json.elevator           @inclu.detail_elevator
      json.laundry            @inclu.detail_laundry
      json.garbage_chute      @inclu.detail_garbage_chute
      json.common_space       @inclu.detail_common_space
      json.janitor            @inclu.detail_janitor
      json.gym                @inclu.detail_gym
      json.golf               @inclu.detail_golf
      json.tennis             @inclu.detail_tennis
      json.sauna              @inclu.detail_sauna
      json.spa                @inclu.detail_spa
      json.inside_pool        @inclu.detail_inside_pool
      json.outside_pool       @inclu.detail_outside_pool
      json.inside_parking     @inclu.detail_inside_parking
      json.outside_parking    @inclu.detail_outside_parking
      json.parking_on_street  @inclu.detail_parking_on_street
      json.garagebox          @inclu.detail_garagebox
    end
    json.half_furnsihed do
      json.living_room  @inclu.half_furnsihed_living_room
      json.bedrooms     @inclu.half_furnsihed_rooms
      json.kitchen      @inclu.half_furnsihed_kitchen
      json.other        @inclu.half_furnsihed_other
    end
    json.indoor do
      json.attic                @inclu.indoor_attic
      json.attic_convertible    @inclu.indoor_attic_convertible
      json.attic_converted      @inclu.indoor_attic_converted
      json.cellar               @inclu.indoor_cellar
      json.central_vacuum       @inclu.indoor_central_vacuum
      json.entries_washer_dryer @inclu.indoor_entries_washer_dryer
      json.entries_dishwasher   @inclu.indoor_entries_dishwasher
      json.fireplace            @inclu.indoor_fireplace
      json.storage              @inclu.indoor_storage
      json.walk_in              @inclu.indoor_walk_in
      json.no_smoking           @inclu.indoor_no_smoking
      json.double_glazing       @inclu.indoor_double_glazing
      json.triple_glazing       @inclu.indoor_triple_glazing
    end
    json.floor do
      json.carpet     @inclu.floor_carpet
      json.wood       @inclu.floor_wood
      json.floating   @inclu.floor_floating
      json.ceramic    @inclu.floor_ceramic
      json.parquet    @inclu.floor_parquet
      json.cushion    @inclu.floor_cushion
      json.vinyle     @inclu.floor_vinyle
      json.lino       @inclu.floor_lino
      json.marble     @inclu.floor_marble
    end
    json.exterior do
      json.land_access    @inclu.exterior_land_access
      json.back_balcony   @inclu.exterior_back_balcony
      json.front_balcony  @inclu.exterior_front_balcony
      json.private_patio  @inclu.exterior_private_patio
      json.storage        @inclu.exterior_storage
      json.terrace        @inclu.exterior_terrace
      json.veranda        @inclu.exterior_veranda
      json.garden         @inclu.exterior_garden
      json.sea_view       @inclu.exterior_sea_view
      json.mountain_view  @inclu.exterior_mountain_view
    end
    json.accessibility do
      json.elevator         @inclu.accessibility_elevator
      json.balcony          @inclu.accessibility_balcony
      json.grab_bar         @inclu.accessibility_grab_bar
      json.wider_corridors  @inclu.accessibility_wider_corridors
      json.lowered_switches @inclu.accessibility_lowered_switches
      json.ramp             @inclu.accessibility_ramp
    end
    json.senior do
      json.autonomy             @inclu.senior_autonomy
      json.half_autonomy        @inclu.senior_half_autonomy
      json.no_autonomy          @inclu.senior_no_autonomy
      json.meal                 @inclu.senior_meal
      json.nursery              @inclu.senior_nursery
      json.domestic_help        @inclu.senior_domestic_help
      json.community            @inclu.senior_community
      json.activities           @inclu.senior_activities
      json.validated_residence  @inclu.senior_validated_residence
      json.housing_cooperative  @inclu.senior_housing_cooperative
    end
    json.pet do
      json.allow          @inclu.pets_allow
      json.cat            @inclu.pets_cat
      json.dog            @inclu.pets_dog
      json.little_dog     @inclu.pets_little_dog
      json.cage_aquarium  @inclu.pets_cage_aquarium
      json.not_allow      @inclu.pets_not_allow
    end
    json.service do
      json.internet @inclu.service_internet
      json.tv       @inclu.service_tv
      json.tv_sat   @inclu.service_tv_sat
      json.phone    @inclu.service_phone
    end
    json.composition do
      json.bar            @inclu.composition_bar
      json.living         @inclu.composition_living
      json.dining         @inclu.composition_dining
      json.separe_toilet  @inclu.composition_separe_toilet
      json.open_kitchen   @inclu.composition_open_kitchen
    end
    json.heating do
      json.electric       @inclu.heating_electric
      json.solar          @inclu.heating_solar
      json.gaz            @inclu.heating_gaz
      json.condensation   @inclu.heating_condensation
      json.fuel           @inclu.heating_fuel
      json.heat_pump      @inclu.heating_heat_pump
      json.floor_heating  @inclu.heating_floor_heating
    end
    json.transport do
      json.bicycle_path     @inclu.transport_bicycle_path
      json.public_transport @inclu.transport_public_transport
      json.public_bike      @inclu.transport_public_bike
      json.subway           @inclu.transport_subway
      json.train_station    @inclu.transport_train_station
    end
    json.security do
      json.guardian         @inclu.security_guardian
      json.alarm            @inclu.security_alarm
      json.intercom         @inclu.security_intercom
      json.camera           @inclu.security_camera
      json.smoke_dectector  @inclu.security_smoke_dectector
    end
  end
end
