json.posts do
  json.array! @posts.each do |post|
    json.title    post.title
    json.subtitle "Subtile #{post.id}"
    json.content  post.content
    json.picture  post.cover.url(:large)
  end
end
json.promos do
  json.array! @posts.each do |promo|
    json.id       promo.id
    json.content  "blabla"
  end
end
