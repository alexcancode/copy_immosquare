
json.status @status

json.uid @user.uid if @user.uid.present?

json.nbTokens @kangalou_params["nbTokens"] if @kangalou_params["nbTokens"].present?
