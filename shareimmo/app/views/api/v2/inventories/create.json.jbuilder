
json.status @status

json.inventory do
  json.id @inventory.id
  json.property_id @inventory.property_id
  json.designation @inventory.designation
  json.xml @inventory.xml.url
end

json.errors @errors