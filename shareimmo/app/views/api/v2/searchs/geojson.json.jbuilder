if @response
  json.took    @response.took


  ##============================================================##
  ## Pagination
  ##============================================================##
  json.pagination do
    from = @response.offset+1
    to   = @response.offset+@response.length >= @response.results.total ? @response.results.total : @response.offset+@response.length
    json.total          @response.results.total
    json.current_page   @response.current_page
    json.previous_page  @response.previous_page
    json.next_page      @response.next_page
    json.length         @response.length
    json.offset         @response.offset
    json.from           from
    json.to             to
    json.formated      "#{from}-#{to} sur #{@response.results.total}"
  end

  ##============================================================##
  ## Results
  ##============================================================##
  json.results do
    json.type "FeatureCollection"
    json.features do
      json.array!(@response.results.select{|b| b[:_source][:location].present?}) do |list|
        json.type "Feature"
        json.properties list[:_source]
        json.geometry do
          json.type "Point"
          json.coordinates [list[:_source][:location][:lon],list[:_source][:location][:lat]]
        end
      end
    end
  end

else
  json.response false
end


