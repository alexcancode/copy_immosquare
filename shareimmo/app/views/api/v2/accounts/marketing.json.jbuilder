json.partial! 'api/v2/accounts/account', user: @user
if @my_services.present?
  json.services @my_services
end
if @property.present?
  json.property do
    json.partial! 'api/v2/properties/property', property: @property
  end
end
