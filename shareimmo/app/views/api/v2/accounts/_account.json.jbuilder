@profile = @user.profile
json.id             @user.id
json.uid            @portal.present? ? @portal.uid : nil
json.email          @user.email
json.username       @user.username
json.sign_in_count  @user.sign_in_count
##============================================================##
## Editable infos with api
##============================================================##
json.country          @profile.country
json.public_email     @profile.email
json.first_name       @profile.first_name
json.last_name        @profile.last_name
json.phone            @profile.phone
json.phone_sms        @profile.phone_sms
json.phone_visibility @profile.phone_visibility
json.picture_url      @profile.picture.url(:original,:timestamp=>false)
json.website          @profile.website
json.spoken_languages @profile.spoken_languages
json.facebook         @profile.facebook
json.twitter          @profile.twitter
json.linkedin         @profile.linkedin
json.pinterest        @profile.pinterest
json.google_plus      @profile.google_plus
json.instagram        @profile.instagram
json.color1           @profile.primary_color
json.color2           @profile.secondary_color
json.company_name                @profile.agency_name
json.company_address             @profile.agency_address
json.company_city                @profile.agency_city
json.company_zipcode             @profile.agency_zipcode
json.company_administrative_area @profile.agency_province
json.company_logo_url            @profile.logo.url(:original,:timestamp=>false)
json.corporate_account           @profile.is_a_pro_user
json.presentation_text do
  json.fr @profile.job_fr
  json.en @profile.job_en
  json.de @profile.job_de
  json.nl @profile.job_nl
  json.it @profile.job_it
  json.es @profile.job_es
  json.pt @profile.job_pt
end
json.bio do
  json.fr @profile.bio_fr
  json.en @profile.bio_en
  json.de @profile.bio_de
  json.nl @profile.bio_nl
  json.it @profile.bio_it
  json.es @profile.bio_es
  json.pt @profile.bio_pt
end
json.baseline do
  json.fr @profile.baseline_fr
  json.en @profile.baseline_en
  json.de @profile.baseline_de
  json.nl @profile.baseline_nl
  json.it @profile.baseline_it
  json.es @profile.baseline_es
  json.pt @profile.baseline_pt
end
json.created_at @profile.created_at
json.updated_at @profile.updated_at
json.uids do
  json.array! UserPortalProvider.where(:user_id => @user.id).each do |provider|
    json.uid      provider.uid
    json.provider provider.api_provider.name
  end
end
