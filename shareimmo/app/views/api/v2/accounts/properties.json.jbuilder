json.uid @portal.uid
json.properties @properties do |property|
	json.partial! 'api/v2/properties/property', property: property
end
