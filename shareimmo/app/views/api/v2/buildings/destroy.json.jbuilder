api_uid = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:user_id => @building.user_id).first

json.id           @building.id
json.uid          @building.uid
json.account_id   @building.user_id
json.account_uid  api_uid.uid
json.message "Building deletion was launched"
