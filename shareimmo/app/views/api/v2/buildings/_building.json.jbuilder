api_uid = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:user_id => @building.user_id).first

json.id           @building.id
json.uid          @building.uid
json.account_id   @building.user_id
json.account_uid  api_uid.present? ? api_uid.uid : nil
json.name         @building.name
json.units        @building.number_of_units
json.levels       @building.number_of_levels
json.address do
  json.address_formatted          @building.address
  json.street_number              @building.street_number
  json.street_name                @building.street_name
  json.zipcode                    @building.zipcode
  json.locality                   @building.locality
  json.sublocality                @building.sublocality
  json.administrative_area_level1 @building.administrative_area_level_1
  json.administrative_area_level2 @building.administrative_area_level_2
  json.country                    @building.country_short
  json.latitude                   @building.latitude
  json.longitude                  @building.longitude
end
json.description_short do
  json.fr @building.description_short_fr
  json.en @building.description_short_en
  json.de @building.description_short_de
  json.nl @building.description_short_nl
  json.it @building.description_short_it
  json.es @building.description_short_es
  json.pt @building.description_short_pt
end
json.description do
  json.fr @building.description_fr
  json.en @building.description_en
  json.de @building.description_de
  json.nl @building.description_nl
  json.it @building.description_it
  json.es @building.description_es
  json.pt @building.description_pt
end
json.pictures do
  if @picture_in_progress.present? && @picture_in_progress == 1
    json.in_progess true
  else
    json.array! @building.building_assets do |asset|
      json.small        asset.picture? ? asset.picture.url(:small,:timestamp => false)  : PropertyAsset.new.picture.options[:default_url]
      json.medium       asset.picture? ? asset.picture.url(:medium,:timestamp => false) : PropertyAsset.new.picture.options[:default_url]
      json.large        asset.picture? ? asset.picture.url(:large,:timestamp => false)  : PropertyAsset.new.picture.options[:default_url]
      json.hd_small     asset.picture? ? asset.picture.url(:hd_s,:timestamp => false)     : PropertyAsset.new.picture.options[:default_url]
      json.hd_medium    asset.picture? ? asset.picture.url(:hd_m,:timestamp => false)     : PropertyAsset.new.picture.options[:default_url]
      json.hd           asset.picture? ? asset.picture.url(:hd,:timestamp => false)     : PropertyAsset.new.picture.options[:default_url]
    end
  end
end

