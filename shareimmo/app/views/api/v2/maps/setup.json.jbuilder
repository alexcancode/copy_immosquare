
json.infos do
  json.logo_url                   image_url "logos/shareimmo.png"
  json.infobox_close_button_url   image_url "map/close.png"
  if current_user.present?
    json.current_user do
      json.id current_user.id
      json.email current_user.email
      json.avatar avatar_to_display(current_user,30)
    end
  end
end


json.property_listing do
  json.name t('map.property_listing')
  json.infos do
    PropertyListing.all.each do |s|
      json.set! s.id, s.name
    end
  end
end


json.property_classification do
  json.name t('map.property_classification')
  json.infos do
    PropertyClassificationCategory.all.each do |s|
      json.set! s.id, s.name
    end
  end
end


json.property_subclassification do
  json.name t('map.property_subclassification')
  json.infos do
    PropertyClassification.all.order('api_id').each do |s|
      json.set! s.api_id, s.name
    end
  end
end

json.property_status do
  json.name t('map.property_status')
  json.infos do
    PropertyStatus.all.each do |s|
      json.set! s.id, s.name
    end
  end
end



json.bedrooms do
  json.name t('map.rooms')
  json.infos do
    json.set! 1, "1+"
    json.set! 2, "2+"
    json.set! 3, "3+"
    json.set! 4, "4+"
    json.set! 5, "5+"
    json.set! 6, "6+"
  end
end

json.bathrooms do
  json.name t('map.bathrooms')
  json.infos do
    json.set! 1, "1+ #{t('map.sdb')}"
    json.set! 2, "2+ #{t('map.sdb')}"
    json.set! 3, "3+ #{t('map.sdb')}"
    json.set! 4, "4+ #{t('map.sdb')}"
    json.set! 5, "5+ #{t('map.sdb')}"
    json.set! 6, "6+ #{t('map.sdb')}"
  end
end



json.zones @cities
