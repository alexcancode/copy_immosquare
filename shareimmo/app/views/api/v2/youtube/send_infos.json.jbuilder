# encoding: utf-8
json.id           @property.id


if @property.property_assets.present?
  json.pictures do
    json.array! @property.property_assets do |asset|
      json.url asset.picture.url(:large,:timestamp => false)
    end
  end
end

kangalou = ApiProvider.where(:name=>"kangalou").first

if @property.api_provider_id != kangalou.id
  ##============================================================##
  ## Pas Kangalou
  ##============================================================##
  @auth = Authentification.joins(:api_provider).where(:user_id  => @property.user_id ,:api_providers => { :name =>'google_oauth2'}).last
  @auth = Authentification.joins(:api_provider).where(:name => "shareIMMO" ,:api_providers => { :name =>'google_oauth2'}).last if @auth.blank?
  settings = {
    :opening => {
      :video_url        => "https://s3.amazonaws.com/socialimmo/shared/youtube/v1/openning/video_#{rand(1..8)}.mp4",
      :video_start      => 1,
      :video_stop       => 10,
      :audio_url        => "https://s3.amazonaws.com/shareimmo/shared/video-generator/soundtrack/shareimmo_#{rand(1..2)}.mp3",
      :color_primary    => @profile.primary_color,
      :color_secondary  => @profile.primary_color,
    },
    :slider =>{
      :image_on_picture => @profile.picture? ? @profile.picture.url(:medium,:timestamp => false) : "https://s3.amazonaws.com/shareimmo/shared/video-generator/shareimmo/shareimmo-s.jpg",
      :line_top_1       => "#{@profile.public_name[0..35]}",
      :line_top_2       => @complememnt.canada_mls.present? ? "MLS : #{@complememnt.canada_mls}"  : "Annonce shareIMMO ##{@property.id}",
      :line_bottom_1    => "#{my_beautiful_price(@property)} #{@property.rooms.present? ? "#{@property.rooms} pièces -" : ""} #{@property.property_classification.name}",
      :line_bottom_2    => "#{@property.street_number} #{@property.street_name}#{@property.locality.present? ? ", #{@property.locality}" : ""}#{@property.zipcode.present? ? ", #{@property.zipcode}" : ""}"
    },
    :closing=>{
      :map_url        => (@property.longitude.present? and @property.latitude.present?) ? "https://maps.googleapis.com/maps/api/staticmap?center=#{@property.latitude},#{@property.longitude}&zoom=14&size=320x360&scale=2&maptype=roadmap&markers=color:green%7Clabel:S%7C#{@property.latitude},#{@property.longitude}" : "https://maps.googleapis.com/maps/api/staticmap?center=45.449273,-73.573725&zoom=14&size=320x360&scale=2&maptype=roadmap&markers=color:green%7Clabel:S%7C45.449273,-73.573725",
      :logo_url       => @profile.logo? ? @profile.logo.url(:medium,:timestamp => false) : "https://s3.amazonaws.com/shareimmo/shared/video-generator/shareimmo/shareimmo-white.png",
      :baseline       => (@profile.phone.present? and @profile.phone_visibility == 1) ?  @profile.phone : @profile.email ,
      :avatar_url     => @profile.picture? ? @profile.picture.url(:medium,:timestamp => false)  : "https://s3.amazonaws.com/shareimmo/shared/video-generator/shareimmo/shareimmo-s.jpg",
      :property_link  =>  "www.shareimmo.com/ad/#{@property.id}",
      :line_1         => @profile.public_name.present? ? @profile.public_name  : nil,
      :line_2         => @profile.email.present? ? @profile.email  : nil,
      :line_3         => @profile.job.present? ? @profile.job  : nil,
      :line_4         => nil,
      :line_5         => nil
    },
    :youtube_uploader => {
      :to_be_upload       => Rails.env.production? ? true : false,
      :refresh_token      => @auth.present? ? @auth.refresh_token : nil,
      :auth_id            => @auth.present? ? @auth.id : nil,
      :video_link         => "https://www.shareimmo.com/ad/#{@property.id}",
      :video_title        => truncate("#{@property.property_listing.name} - #{@property.property_classification.name}  #{@property.locality.present? ? "- #{@property.locality}" : nil} #{@property.sublocality.present? ? "(#{@property.sublocality})" : nil}", :length=> 80),
      :video_description  => convert_to_text("#{@property.description_fr.present? || @property.title_fr.present?  ? "#{@property.title_fr}\n#{@property.description_fr}#{@property.description_en.present? || @property.title_en.present? ? "\n\n############\n\n" : nil }" : nil}#{@property.description_en.present? || @property.title_en.present? ? "#{@property.title_en}\n#{@property.description_en}" : nil }")
    }
  }
else
  ##============================================================##
  ## Kangalou
  ##============================================================##
  @auth = Authentification.joins(:api_provider).where(:name=> "Kangalou" ,:api_providers => { :name =>'google_oauth2'}).last
  title = "#{@property.rooms.present? ? "#{@property.rooms}½"  : nil}  #{@property.locality.present? ? "- #{@property.locality}" : nil} #{@property.sublocality.present? ? "(#{@property.sublocality})" : nil} - #{my_beautiful_price(@property)}"
  settings = {
    :opening =>{
      :video_url        => "https://s3.amazonaws.com/shareimmo/shared/video-generator/opening/opening_kangalou_#{rand(1..5)}.mp4",
      :video_start      => 1,
      :video_stop       => 10,
      :audio_url        => "https://s3.amazonaws.com/shareimmo/shared/video-generator/soundtrack/kangalou_1.mp3",
      :color_primary    => "#f54029",
      :color_secondary  => "#B83322",
    },
    :slider =>{
      :image_on_picture => "https://s3.amazonaws.com/shareimmo/shared/video-generator/kangalou/kangalou-k.png",
      :line_top_1       => "#{@profile.public_name[0..35]}",
      :line_top_2       => "Kangalou ##{@property.uid2}",
      :line_bottom_1    => title,
      :line_bottom_2    => "#{@property.street_number} #{@property.street_name}#{@property.locality.present? ? ", #{@property.locality}" : ""}#{@property.zipcode.present? ? ", #{@property.zipcode}" : ""}"
    },
    :closing =>{
      :map_url        => (@property.longitude.present? and @property.latitude.present?) ? "https://maps.googleapis.com/maps/api/staticmap?center=#{@property.latitude},#{@property.longitude}&zoom=14&size=320x360&scale=2&maptype=roadmap&markers=color:green%7Clabel:S%7C#{@property.latitude},#{@property.longitude}" : "https://maps.googleapis.com/maps/api/staticmap?center=45.449273,-73.573725&zoom=14&size=320x360&scale=2&maptype=roadmap&markers=color:green%7Clabel:S%7C45.449273,-73.573725",
      :logo_url       => "https://s3.amazonaws.com/shareimmo/shared/video-generator/kangalou/kangalou-white.png",
      :baseline       => "Date de disponibilité : #{@property.availability_date.present? ? I18n.localize(@property.availability_date.to_time,:format =>("%d %B %Y")) : "immédiate"}",
      :avatar_url     => @profile.logo? ? @profile.picture.url(:medium,:timestamp => false)  : "https://s3.amazonaws.com/shareimmo/shared/video-generator/kangalou/kangalou-k.png",
      :property_link  => @property.uid2.present?  ? "www.kangalou.com/fr/annonce/#{@property.uid2}" : "www.kangalou.com" ,
      :line_1         => @profile.public_name.present? ? @profile.public_name  : nil,
      :line_2         => (@profile.phone.present? and @profile.phone_visibility == 1) ? @profile.phone  : nil,
      :line_3         => @profile.email.present? ? @profile.email  : nil,
      :line_4         => @profile.job.present? ? @profile.job  : nil,
      :line_5         => nil
    },
    :youtube_uploader => {
      :to_be_upload       => Rails.env.production? ? true : false,
      :refresh_token      => @auth.present? ? @auth.refresh_token : nil,
      :auth_id            => @auth.present? ? @auth.id : nil,
      :video_link         => "https://kangalou.com/fr/annonce/#{@property.uid2}",
      :video_title        => truncate(title, :length=> 80),
      :video_description  => convert_to_text("#{@property.description_fr.present? || @property.title_fr.present?  ? "#{@property.title_fr}\n#{@property.description_fr}#{@property.description_en.present? || @property.title_en.present? ? "\n\n############\n\n" : nil }" : nil}#{@property.description_en.present? || @property.title_en.present? ? "#{@property.title_en}\n#{@property.description_en}" : nil }")
    }
  }
end
json.settings settings
