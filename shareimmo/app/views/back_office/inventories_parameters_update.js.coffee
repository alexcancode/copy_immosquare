
<% if @parameter.errors.any? %>
errors = jQuery.parseJSON('<%= raw(escape_javascript(@parameter.errors.to_hash.to_json)) %>')
$.each errors, (key, value) ->
  $(".inventory_parameter_#{key}")
    .addClass("has-error")
    .append("<p class='hint'>#{value[0]}</p>")


<% else %>


$('#setupState1_info').html('<%= @parameter.state1_name %>')
$('#setupState2_info').html('<%= @parameter.state2_name %>')
$('#setupState3_info').html('<%= @parameter.state3_name %>')
$('#setupState4_info').html('<%= @parameter.state4_name %>')
$('#setupState5_info').html('<%= @parameter.state5_name %>')
$('#pdfIntroTtile_info').html('<%= @parameter.pdf_introduction_title %>')
$('#pdfIntroContent_info').html('<%= truncate(@parameter.pdf_introduction_content,:length=>150) %>')
$('#pdfFreeSectionTitle_info').html('<%= @parameter.pdf_free_section_title %>')
$('#pdfFreeSectionContent_info').html('<%= truncate(@parameter.pdf_free_section_content,:length=>150) %>')
$('#nomenclatureDefault_info').html('<%= @parameter.default_nomenclature.name %>')


$("ul.is-open").each ->
  $(@).removeClass('is-open').addClass('no-processing')
  $("##{$(@).data('form')}_info").toggleClass('hide')
  $("##{$(@).data('form')}_form").toggleClass('hide')
  $(@).find('.edit_section').toggleClass('hide')

<% end %>
