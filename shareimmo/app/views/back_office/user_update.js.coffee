<% if @user.errors.any? %>
errors = jQuery.parseJSON('<%= raw(escape_javascript(@user.errors.to_hash.to_json)) %>')
$.each errors, (key, value) ->
  $(".user_#{key}")
    .addClass("has-error")
    .append("<p class='hint'>#{value[0]}</p>")
<% else %>
window.location.replace("<%= j dashboard_apps_path(@user.username, :website => 1) %>")

<% end %>
