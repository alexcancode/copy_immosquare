$('#myGlossary').html("<%= j render 'back_office/partials/glossary', elements: @glossary.glossary_elements %>")
$('form#new_glossary_element')[0].reset()
$('#myNewGLossaryElementModal').modal('hide')
$('#EditGLossaryElementModal').modal('hide')

