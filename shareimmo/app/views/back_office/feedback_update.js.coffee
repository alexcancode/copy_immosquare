<% if @feedback.errors.any? %>
$('#addVisit .modal-content').html('<%= j render :partial => "back_office/partials/feedback_form", locals: {feedback: @feedback} %>')
<% else %>
$('#addVisit').modal('hide')
$('#makeVisit').modal('hide')
$("#feedsContainer").html('<%= j render :partial => "back_office/partials/feedbacks", :locals => { :feedbacks => @feedbacks } %>')
<% end %>





