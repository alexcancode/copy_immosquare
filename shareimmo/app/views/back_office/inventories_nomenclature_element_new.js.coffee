$('#myTree').html("<%= j render 'back_office/partials/nomenclature_trees', nomenclature: @nomenclature %>")
$('#new_nomenclature_element')[0].reset()
$('#nomenclature_element_parent_id').html('<%= j options_for_select(@my_list) %>');
$('#NomenclatureElementEditModal').modal('hide')
