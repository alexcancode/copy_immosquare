<% if @inventory.errors.any? %>
  $('#ImportEdlModal .modal-content').html('<%= j render :partial => "back_office/partials/inventory_form", :locals=> {:inventory => @inventory, :user => @user} %>')
<% else %>
  $('#ImportEdlModal').modal('hide')
  $("#myEdls").html('<%= j render :partial => "back_office/partials/inventories", locals: {inventories: @inventories, :user =>@user} %>')
<% end %>

