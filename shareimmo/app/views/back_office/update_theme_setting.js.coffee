$('.flashMessage').remove()
$('body').prepend("<%= j render 'layouts/partials/messages' %>")


<% if @theme_setting.errors.blank? %>
$("#coverContainer img").attr('src','<%= j @theme_setting.website_cover.url(:large) %>')
$("#coverContainerFullSize img").attr('src','<%= j @theme_setting.website_cover_full_size.url(:large) %>')
<% end %>
