<% if @document.errors.any? %>
$("#myModalPropertyDocument .modal-content").html("<%= j render 'back_office/partials/document', :document => @document %>")
<% else %>
$('#myModalPropertyDocument').modal('hide')
$('#myPropertyDocuments').html("<%= j render :partial => 'back_office/partials/property_documents', locals: {documents: @property.documents} %>")
<% end %>
