$('.flashMessage').remove()
$('body').prepend("<%= j render 'layouts/partials/messages' %>")

<% if @profile.errors.blank? %>
#============================================================##
# Page du profil
#============================================================##
$('#setupCountry').html('<%= j @profile.country_name %>')
$("#setupPicture_info img, #setupPicture_form img").attr("src","<%= j @profile.picture(:small) %>")
$("#userAvatar img").attr("src","<%= j @profile.picture(:large) %>")
$('#setupPublicName_info').html('<%= j @profile.public_name %>')
$('#setupMail_info').html('<%= j @profile.email %>')
$('#setupPhone_info').html('<%= j @profile.phone %>')
$('#setupPhoneSms_info').html('<%= j @profile.phone_sms %>')
$('#setupPhoneVisiblity_info').html("<%= j  @profile.phone_visibility == 0 ? t('back_office.no_non') : t('back_office.yes_oui') %>")
$('#setupLanguages_info').html('<%= j render :partial => "back_office/partials/profile_languages", locals: {my_languages: @profile.spoken_languages.present? ? @profile.spoken_languages.split(",") : nil} %>')
$("#setupLogo_info img, #setupLogo_form img").attr("src","<%= j @profile.logo(:small) %>")
$('#setupAgencyName_info').html('<%= j @profile.agency_name %>')
$('#setupAgencyAddress_info').html('<%= j render :partial => "back_office/partials/profile_agency_address", locals: {profile: @profile} %>')
$('#setupProUser_info').html("<%= j @profile.is_a_pro_user == 0 ? t('back_office.no_non') : t('back_office.yes_oui') %>")
$('#setupJob_info').html('<%= j @profile.job %>')
$('#setupBaseline_info').html('<%= j @profile.baseline %>')
$('#setupBio_info').html("<%= j truncate(strip_tags(simple_format(@profile.bio)),:length=>100) %>")
$('#setupWebsite_info').html("<%= j @profile.website %>")
$('#setupSocialLinks_info').html('<%= j render :partial => "back_office/partials/profile_social_links", locals: {profile: @profile} %>')
#============================================================##
# Page du Website
#============================================================##
$('#setupColors_info').html('<%= j render :partial => "back_office/partials/profile_colors", locals: {profile: @profile} %>')
$('#setupFont_info').html('<%= j "#{@user.profile.font.present? ? @user.profile.font.name : ""} / #{@user.profile.font_special.present? ? @user.profile.font_special.name : ""}" %>')
$('#setupGoogleAnalytics_info').html('<%= @profile.google_analytics %>')
$('#setupBlog_info').html("<%= j @profile.blog_active == 0 ? t('back_office.inactive') : t('back_office.active') %>")
$('#setupTestimonial_info').html("<%= j @profile.page_testimonial_active == 0 ? t('back_office.inactive') : t('back_office.active') %>")
$('#mandate_website_info').html("<%= j @user.profile.mandate_website == 0 ? t('back_office.inactive') : t('back_office.active') %>")



##============================================================##
## Fermeture du form
##============================================================##
$('.setting_edit_line').each ->
  $(@).removeClass 'active'

<% end %>
