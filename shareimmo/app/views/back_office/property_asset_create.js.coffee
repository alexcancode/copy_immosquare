<% if @property_asset.errors.any? %>
swal("Failed to upload picture: <%= j @property_asset.errors.full_messages.join(', ').html_safe %>");
<% else %>
$('#new_property_asset')[0].reset()
$('#my-ui-list-picture').append('<%= j render :partial => "back_office/partials/property_asset", locals: {property_asset: @property_asset, :user => @user, :secure_id=>@property.secure_id} %>')
<% end %>
