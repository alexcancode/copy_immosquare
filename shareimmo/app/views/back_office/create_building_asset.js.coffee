<% if @building_asset.errors.any? %>
alert("Failed to upload picture: <%= j @building_asset.errors.full_messages.join(', ').html_safe %>");
<% else %>
$('#new_building_asset')[0].reset()
$('#my-ui-list-picture').append('<%= j render :partial => "back_office/partials/building_asset", locals: {building_asset: @building_asset, :user => @user, :secure_id=>@building.secure_id} %>')
<% end %>
