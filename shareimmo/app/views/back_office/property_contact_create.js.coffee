$('#myPropertyContacts').html('<%= j render :partial => "back_office/partials/property_contacts", :locals => {:property_contacts => @property.present? ? @property.property_contacts :  @user.property_contacts} %>')
$('#myModalPropertyContact').modal('hide')
$('#propertyContactForm')[0].reset()
