xml.instruct!
xml.urlset(:xmlns=>"http://www.sitemaps.org/schemas/sitemap/0.9", "xmlns:xhtml"=>"http://www.w3.org/1999/xhtml") do

  if ['1clic.immo','shareimmo.com','lvh.me'].include?(request.domain.to_s)
    @static_urls.each do |static|
      xml.url do
        xml.loc(static[0][:url])
        static[0][:all_urls].each do |tab|
          xml.tag!("xhtml:link",:rel=>'alternate',:hreflang=>tab[0][:hreflang].to_s,:href=>tab[0][:url])
        end
        xml.changefreq(static[0][:changefreq])
        xml.priority(static[0][:priority])
      end
    end

    @posts.each do |p|
      xml.url do
        xml.loc(blog_show_url(p,:locale=>p.locale,:protocol => ENV['PROTOCOL']))
        xml.changefreq('monthly')
        xml.lastmod(p.updated_at.strftime("%Y-%m-%d"))
        xml.priority(0.8)
      end
    end

    if @my_provider.provider_id == "oneclic"
      User.all.each do |u|
        xml.url do
          xml.loc(root_url(:subdomain=>u.username,:protocol=>'http'))
          xml.changefreq('weekly')
          xml.priority(1)
        end
      end
    end

  end


end
