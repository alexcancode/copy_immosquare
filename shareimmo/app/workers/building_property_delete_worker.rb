class BuildingPropertyDeleteWorker
  include Sidekiq::Worker

  def perform(args)
    begin
      args      = args.deep_symbolize_keys
      model     = args[:model]
      id        = args[:id]
      if model == "building"
        @building = Building.find(id)
        @building.destroy if @building.present?
      elsif model == "property"
        @property = Property.find(id)
        @property.destroy if @property.present?
      end
    rescue Exception => e
      JulesLogger.info e.message
    end
  end
end
