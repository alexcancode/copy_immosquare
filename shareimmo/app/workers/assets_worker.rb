class AssetsWorker
  include Sidekiq::Worker

  require 'fastimage'

  def perform(args)
    begin
      args      = args.deep_symbolize_keys
      model     = args[:model]
      id        = args[:id]
      pictures  = args[:pictures]

      to_upload = Array.new
      in_db     = Array.new
      to_delete = Array.new


      ##============================================================##
      ## Keep only valid image with valid url
      ##============================================================##
      pictures.each do |picture|
        to_upload << picture[:url] if picture.is_a?(Hash) && picture.key?(:url)  && url_is_a_image?(picture[:url])
      end



      ##============================================================##
      ## On supprime toutes les anciennes images qui
      ## ont une original_url (donc pas uploadé manuellement)
      ## et qui ne font plus partie
      ##============================================================##
      if model == "building"
        assets = BuildingAsset.where(:building_id => id).where.not(:original_url => nil)
      elsif model == "property"
        assets = PropertyAsset.where(:property_id => id).where.not(:original_url => nil)
      end
      assets.order(:position).each do |asset|
        if !to_upload.include?(asset.original_url)
          to_delete << asset.id
        else
          in_db << asset.original_url
        end
      end


      ##============================================================##
      ## On delete une par une  pour eviter des erreurs de lock
      ## MYSQL.
      ## plutôt que de les effacer 1 par 1 dans la méthode du dessus
      ## (boucle en cours)
      ##============================================================##
      if model == "building"
        BuildingAsset.where(:id =>to_delete).each do |asset|
          asset.delete
        end
      elsif model == "property"
        PropertyAsset.where(:id =>to_delete).each do |asset|
          asset.delete
        end
      end



      ##============================================================##
      ## On upload les nouvelles images et on met à jour
      ## la position pour tt les images sauf les manuelles
      ##============================================================##
      to_upload.each_with_index do |url,i|
        if model == "building"
          pics = BuildingAsset.where(:building_id=>id,:original_url => url).first_or_create
        elsif model == "property"
          pics = PropertyAsset.where(:property_id=>id,:original_url => url).first_or_create
        end
        pics.position = i+1
        pics.picture  = open(url) if !in_db.include?(url)
        pics.save
      end



      ##============================================================##
      ## On met à jour l'ordre
      ##============================================================##
      new_position = to_upload.size+1
      if model == "building"
        assets = BuildingAsset.where(:building_id => id,:original_url => nil).order(:position)
      elsif model == "property"
        assets = PropertyAsset.where(:property_id => id,:original_url => nil).order(:position)
      end
      assets.each_with_index do |asset|
        asset.set_list_position(new_position)
        new_position += 1
      end

      ##============================================================##
      ## on doit relancer l'indexation elastic pour mettre à
      ## jour la cover
      ##============================================================##
      ElasticIndexer.new(:index,model,id)


    rescue Exception => e
      JulesLogger.info e.message
      ApiMailer.error_monitoring(
        :title    => "api.v2.properties#assets_worker",
        :message  => e.message,
        :params   => args
        ).deliver_later
    end

  end


  def url_is_a_image?(url)
    begin
      FastImage.size(url).kind_of?(Array)
    rescue Exception => e
      JulesLogger.info e.message
      return false
    end
  end




end




