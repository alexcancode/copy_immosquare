# encoding: utf-8

class VideoGeneratorWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :critical

  require 'rmagick'
  require "mp3info"
  include Magick


  def perform(args)
    @property_id        = args["property_id"]
    @channel            = args["channel"]
    @youtube_info_url   = args["youtube_info_url"]
    @visibility_start   = args["visibility_start"] || Time.now
    @visibility_until   = args["visibility_until"] || Time.now+1.month

    JulesLogger.info "Sidekiq Action Start for ad # #{@property_id}"
    begin
      JulesLogger.info @youtube_info_url
      response  = HTTParty.get(@youtube_info_url)
      raise "api response => #{response.code}" if response.code != 200
      prop = JSON.parse(response.body)
      final_size = {:width=>1280,:height=>720}

      ##============================================================##
      ## Préparation de l'espace de travail
      ##============================================================##
      pusher_and_log("Préparation de l'espace de travail",@channel)
      @video_folder               = File.join(Rails.root,"tmp",'video_generator')
      @video_folder_current_work  = File.join(@video_folder,@property_id.to_s)
      FileUtils.rm_rf(@video_folder_current_work) if File.directory?(@video_folder_current_work)
      FileUtils.mkdir_p(@video_folder_current_work)


      ##============================================================##
      ## Génération des images annotées
      ##============================================================##
      pusher_and_log("Génération des images annotées",@channel)
      @final_frame = 1
      if prop["pictures"].present?
        prop["pictures"].each_with_index do |pic,n|
          if n <= 12
            create_commented_picture(pic["url"],sprintf('%02d',n+1),prop["settings"],final_size[:width],final_size[:height])
            @final_frame += 1
          end
        end
      end

      ##============================================================##
      ## Création de l'image de final
      ##============================================================##
      pusher_and_log("Création de l'image finale",@channel)
      create_final_frame(@final_frame,final_size[:width],final_size[:height],prop["settings"])


      ##============================================================##
      ## Téléchargement de la séquence d'ouverture et du mp3
      ##============================================================##
      pusher_and_log("Téléchargement de la séquence d'ouverture et du mp3",@channel)
      File.open(File.join(@video_folder_current_work,File.basename(prop["settings"]["opening"]["video_url"])), "wb") do |saved_file|
        open(prop["settings"]["opening"]["video_url"], "rb") do |read_file|
          saved_file.write(read_file.read)
        end
      end
      File.open(File.join(@video_folder_current_work,File.basename(prop["settings"]["opening"]["audio_url"])), "wb") do |saved_file|
        open(prop["settings"]["opening"]["audio_url"], "rb") do |read_file|
          saved_file.write(read_file.read)
        end
      end

      ##============================================================##
      ## Création du fichier son au format mp3
      ## on converti en mp3 quelque soit le fichier original
      ##============================================================##
      pusher_and_log("Création d'un long fichier audio",@channel)
      system "sox #{File.join(@video_folder_current_work,File.basename(prop["settings"]["opening"]["audio_url"]))} #{File.join(@video_folder_current_work,'audio_convert.mp3')}"
      mp3info = Mp3Info.open(File.join(@video_folder_current_work,'audio_convert.mp3'))
      system "sox #{File.join(@video_folder_current_work,'audio_convert.mp3')} #{File.join(@video_folder_current_work,'audio_looped.mp3')} repeat #{((@final_frame+2)*5.00/mp3info.length).ceil - 1}"


      ##============================================================##
      ## Normalisation du niveau sonore avec mp3gain
      ##============================================================##
      pusher_and_log("Traitement du niveau sonore",@channel)
      system "mp3gain -r #{File.join(@video_folder_current_work,'audio_looped.mp3')}"

      ##============================================================##
      ## Création des 2 parties de la vidéo
      ##============================================================##
      pusher_and_log("Création des 2 parties de la vidéo",@channel)
      system "ffmpeg -ss 02 -t 10 -i #{File.join(@video_folder_current_work,File.basename(prop["settings"]["opening"]["video_url"]))} -an -s hd720 -vcodec libx264 -r 30 -pix_fmt yuv420p  #{File.join(@video_folder_current_work,'step1.mp4')}"
      system "ffmpeg -framerate 1/5  -i #{File.join(@video_folder_current_work,'img_%02d.png')} -s hd720 -vcodec libx264 -r 30 -pix_fmt yuv420p #{File.join(@video_folder_current_work,'step2.mp4')}"


      ##============================================================##
      ## Concatenation des 2 parties
      ##============================================================##
      pusher_and_log("Concatenation des 2 parties",@channel)
      system "ffmpeg -i #{File.join(@video_folder_current_work,'step1.mp4')} -c copy -bsf:v h264_mp4toannexb -f mpegts #{File.join(@video_folder_current_work,'intermediate1.mp4')}"
      system "ffmpeg -i #{File.join(@video_folder_current_work,'step2.mp4')} -c copy -bsf:v h264_mp4toannexb -f mpegts #{File.join(@video_folder_current_work,'intermediate2.mp4')}"
      system "ffmpeg -i 'concat:#{File.join(@video_folder_current_work,'intermediate1.mp4')}|#{File.join(@video_folder_current_work,'intermediate2.mp4')}' -c copy -bsf:a aac_adtstoasc #{File.join(@video_folder_current_work,'step3.mp4')}"


      ##============================================================##
      ## Mastering
      ##============================================================##
      pusher_and_log("Mastering",@channel)
      my_video_path =  File.join(@video_folder_current_work,"ad_#{@property_id}.mp4")
      system "ffmpeg -i #{File.join(@video_folder_current_work,'step3.mp4')} -i #{File.join(@video_folder_current_work,'audio_looped.mp3')} -acodec aac -strict experimental  -shortest #{my_video_path}"


      ##============================================================##
      ## Upload on Youtube
      ##============================================================##
      if(prop["settings"]["youtube_uploader"]["to_be_upload"] == true  and prop["settings"]["youtube_uploader"]["refresh_token"].present?)
        pusher_and_log("Upload on Youtube",@channel)
        account = Yt::Account.new(:refresh_token =>prop["settings"]["youtube_uploader"]["refresh_token"])
        Video.where(:property_id=>prop["id"],:posting_status => 1).each do |old_video|
          if old_video.youtube_id.present?
            vid = Yt::Video.new(:id => old_video.youtube_id, :auth => account)
            begin
              vid.delete
            rescue Exception => e
              JulesLogger.info "Error : Video not found on youtube (Shhh, let's ignore that ...)"
            end
          end
          old_video.update_attribute(:posting_status, 0)
        end
        video_youtube = account.upload_video(my_video_path,
          :title        => prop["settings"]["youtube_uploader"]["video_title"],
          :description  => "Fiche détaillée: #{prop["settings"]["youtube_uploader"]["video_link"]}\n#{prop["settings"]["youtube_uploader"]["video_title"]}\n#{prop["settings"]["youtube_uploader"]["video_description"]}",
          :category     => 'Howto',
          :keywords     => %w[immobilier RealEstate]
          )
        @video = Video.create(
          :property_id            =>  prop["id"],
          :youtube_id             =>  video_youtube.id,
          :authentification_id    =>  prop["settings"]["youtube_uploader"]["auth_id"],
          :posting_status         =>  1,
          :visibility_start       =>  @visibility_start,
          :visibility_until       =>  @visibility_until
          )
        Pusher.trigger(@channel,'my_event',{:display_link => true})
        Pusher.trigger(@channel,'my_event',{:video_link => "https://www.youtube.com/watch?v=#{video_youtube.id}"})
        ApiMailer.video_uploaded_on_youtube(@video).deliver_now
      end

      #============================================================##
      # Nettoyage de l'espace de travail
      #============================================================##
      Pusher.trigger(@channel,'my_event',{:display_link => true})
      FileUtils.rm_rf(@video_folder_current_work)

    rescue Exception => e
      JulesLogger.info e.message
      JulesLogger.info e.backtrace
      pusher_and_log("Error : #{e.message}",@channel)
    end
  end

  private

  def pusher_and_log(message,channel)
    JulesLogger.info message
    Pusher.trigger(channel,'my_event',{:step => message})
  end


  def create_commented_picture(picture,index,settings,width,height)

    my_file     = ImageList.new
    url_pic     = open(picture)
    my_file.from_blob(url_pic.read)
    my_file.resize!(width,height)

    my_logo     = ImageList.new
    url_logo     = open(settings["slider"]["image_on_picture"])
    my_logo.from_blob(url_logo.read)
    my_logo           = my_logo.resize_to_fill(100,100)
    result            = my_file.composite(my_logo, NorthWestGravity,0,0,OverCompositeOp)
    primary_color     = settings["opening"]["color_primary"]
    rectangle_primary = Draw.new
    rectangle_darken  = Draw.new
    text              = Draw.new
    text.stroke = 'transparent'
    text.fill = '#FFFFFF'
    text.font = "Arial"
    text.interline_spacing = 6
    ##============================================================##
    ## Rectangles
    ##============================================================##
    rectangle_primary.fill = primary_color
    rectangle_primary.rectangle(0,height-85,width,height)
    rectangle_primary.draw(result)

    rectangle_primary.rectangle(width/2,0,width,60)
    rectangle_primary.draw(result)

    rectangle_darken.fill = darken_color(primary_color,40)
    rectangle_darken.rectangle(0,height-10,width,height)
    rectangle_darken.draw(result)

    ##============================================================##
    ## Texts Top
    ##============================================================##
    text.annotate(result, width/2,60, width/2,0, "#{settings["slider"]["line_top_1"]}\n#{settings["slider"]["line_top_2"]}") do |t|
      t.gravity = CenterGravity
      t.pointsize = 20
    end

    text.annotate(result, 0, 0, 0, 20, "#{settings["slider"]["line_bottom_1"]}\n#{settings["slider"]["line_bottom_2"]}") do |t|
      t.gravity = SouthGravity
      t.pointsize = 25
    end
    result.write(File.join(@video_folder_current_work,"img_#{index.to_s}.png"))
  end


  def create_final_frame(index,width,height,settings)
    text  = Draw.new
    text.stroke = 'transparent'
    text.fill = '#FFFFFF'
    text.font = "Arial"
    text.interline_spacing = 6

    map_url     = ImageList.new
    url_map     = open(settings["closing"]["map_url"].start_with?('https') ? settings["closing"]["map_url"].gsub("https","http") : settings["closing"]["map_url"])
    map_url.from_blob(url_map.read)
    map_url     = map_url.resize_to_fill(width/2,height)

    my_logo     = ImageList.new
    url_logo    = open(settings["closing"]["logo_url"])
    my_logo.from_blob(url_logo.read)
    my_logo     = my_logo.resize_to_fit(width/2-50,120)

    avatar      = ImageList.new
    url_avatar = open(settings["closing"]["avatar_url"])
    avatar.from_blob(url_avatar.read)
    avatar      = avatar.resize_to_fill(100,100)

    canvas = Image.new(width/2,height) do |c|
      c.background_color = settings["opening"]["color_primary"]
    end
    result  = canvas.composite(my_logo, NorthGravity,0,10,OverCompositeOp)
    result  = result.composite(avatar, NorthWestGravity,50,400,OverCompositeOp)


    text.annotate(result,width/2-20,60,20,150, "#{settings["slider"]["line_bottom_1"]}\n#{settings["slider"]["line_bottom_2"]}") do |t|
      t.gravity = CenterGravity
      t.pointsize = 20
    end

    text.annotate(result,width/2-20,60,20,250, "#{settings["closing"]["baseline"]}\n#{settings["closing"]["property_link"]}") do |t|
      t.gravity = CenterGravity
      t.pointsize = 25
    end

    text.annotate(result,width/2-20,10,200,400, "#{settings["closing"]["line_1"]}\n#{settings["closing"]["line_2"]}\n#{settings["closing"]["line_3"]}\n#{settings["closing"]["line_4"]}\n#{settings["closing"]["line_5"]}") do |t|
      t.gravity = NorthWestGravity
      t.pointsize = 18
    end

    canvas  = Image.new(width,height)
    result  = canvas.composite(result,NorthEastGravity,0,0,OverCompositeOp)
    result  = result.composite(map_url,NorthWestGravity,0,0,OverCompositeOp)

    result.write(File.join(@video_folder_current_work,"img_#{sprintf('%02d',index.to_i)}.png"))
  end

  def darken_color(hex_color, amount=20)
    hex_color = hex_color.gsub('#','')
    rgb = hex_color.scan(/../).map {|color| color.hex}
    rgb[0] = (rgb[0].to_i * (1-amount/100.00)).round
    rgb[1] = (rgb[1].to_i * (1-amount/100.00)).round
    rgb[2] = (rgb[2].to_i * (1-amount/100.00)).round
    "#%02x%02x%02x" % rgb
  end


end
