class AdminMail < ActiveRecord::Base


  ##============================================================##
  ## Validation
  ##============================================================##
  validates :from, :presence => true
  validates :subject, :presence => true
  validates :content, :presence => true


end
