class PropertyContact < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  has_and_belongs_to_many :properties
  belongs_to :user
  belongs_to :property_contact_type
  belongs_to :civility


  ##============================================================##
  ## Paperclip : picture
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/property_contacts/:id/picture_:id_:style.:extension",
  :styles => {
    :medium => ["500x500#", :png]
  },
  :convert_options => {
    :medium => "-quality 85 -strip"
  },
  :default_url => "https://placehold.it/500x500"
  validates_attachment_content_type :picture, :content_type => /\Aimage/
  validates_attachment_size :picture, :less_than=>5.megabytes



end
