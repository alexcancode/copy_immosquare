class PropertyClassification < ActiveRecord::Base

  ##============================================================##
  ## Translations
  ##============================================================##
  translates :name, :name_kangalou
  globalize_accessors

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :properties
  belongs_to :property_classification_category


end
