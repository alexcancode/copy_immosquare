class Provider < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  has_and_belongs_to_many :services

end
