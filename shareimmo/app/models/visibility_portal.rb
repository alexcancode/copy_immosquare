class VisibilityPortal < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :property
  belongs_to :portal

  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :history, Hash


  ##============================================================##
  ## Before & After
  ##============================================================##
  before_update :create_history


  private

  def create_history
    if self.posting_status_changed?
      self.count = self.count+1
      self.visibility_start = nil if self.posting_status == 0
      self.visibility_until = nil if self.posting_status == 0
      self.history[self.count] = {
        "visibility_start"  =>  self.visibility_start,
        "visibility_until"  =>  self.visibility_until,
        "posting_status"    =>  self.posting_status,
        "backlink_id"       =>  self.backlink_id,
        "backlink_url"      =>  self.backlink_url,
        "updated_at"        =>  Time.now
      }
    end
  end



end
