class User < ActiveRecord::Base


  Paperclip.interpolates('user_id_paperclip') do |attachment, style|
    attachment.instance.user.id.to_s.parameterize
  end

  ##============================================================##
  ## Pour Api de Connexion
  ##============================================================##
  serialize :tokens, JSON


  ##============================================================##
  ## Associations
  ##============================================================##
  rolify
  belongs_to :agency
  has_one :profile, dependent: :destroy
  has_one :statistic, dependent: :destroy
  has_one :glossary,  dependent: :destroy
  has_one :inventory_parameter,  dependent: :destroy
  has_many :credit_cards, dependent: :destroy
  has_many :authentifications, dependent: :destroy
  has_many :custom_services, dependent: :destroy
  has_many :properties, :dependent => :destroy
  has_many :theme_settings, :dependent => :destroy
  has_many :themes, :through => :theme_settings, dependent: :destroy
  has_many :share_socials,  dependent: :destroy
  has_many :hostings,  dependent: :destroy
  has_many :blogs,  dependent: :destroy
  has_many :buildings,  dependent: :destroy
  has_many :nomenclatures, dependent: :destroy
  has_many :testimonials, dependent: :destroy
  has_many :property_contacts, dependent: :destroy
  has_many :user_portal_providers, dependent: :destroy
  has_many :api_providers, :through => :user_portal_providers, dependent: :destroy
  has_and_belongs_to_many :services
  has_many :services_user, dependent: :destroy



  ##============================================================##
  ## Validation
  ##============================================================##
  validates :username, :presence => true
  validates :username, :uniqueness => {:case_sensitive => false}


  ##============================================================##
  ## Fake attributes
  ## login : pour la conneixon avec username ou email
  ## uid : pour la création de user relié à une api via
  ## le menu d' admin...(ZoomVisit par exemple)
  ##============================================================##
  attr_accessor :login, :uid

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end


  ##============================================================##
  ## Active Record Callbacks
  ##============================================================##
  before_validation :check_username
  after_create :create_profile
  after_create :assign_default_role
  before_save :destroy_expired_tokens

  ##============================================================##
  ## Devise
  ## =======
  ## Include default devise modules. Others available are:
  ## :lockable, :timeoutable and :omniauthable, :confirmable
  ## Voir l'article suivant pour ajouter un champ dans le sign_in
  ## https://github.com/plataformatec/devise/wiki/How-To:-Allow-users-to-sign-in-using-their-username-or-email-address
  ##============================================================##
  devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable,:authentication_keys => [:login]


  def destroy_expired_tokens
    if self.tokens.present?
      self.tokens.delete_if{|cid,v|
        expiry = v[:expiry] || v["expiry"]
        DateTime.strptime(expiry.to_s, '%s') < Time.now
      }
    end
  end


  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end


  def check_role?
    return true if self.has_any_role? :admin, :manager
  end

  private


  ##============================================================##
  ##
  ##============================================================##
  def check_username
    if self.email.present? and self.username.blank?
      my_user  = self.email.split('@').first.downcase.tr("/!#$%&'*+-=?^_`{|}~,.",'')
      my_test  = my_user
      my_count = 1
      while User.where(:username => my_test).count != 0 do
        my_count  = my_count+1
        my_test   = "#{my_user}#{my_count}"
      end
      self.username = my_test
    end
  end

  def assign_default_role
    add_role(:user)
  end




  ##============================================================##
  ## Cette fonction permet de créer un 'profile' à chaque
  ## utilisateur lors de sa création et un website et un theme
  ##============================================================##
  def create_profile
    p = Profile.create(:user_id => self.id,:email=>self.email,:public_name=>self.username)
    st = Statistic.create(:user_id => self.id)
    p.save(:validate=>false)
    st.save(:validate=>false)
  end



end
