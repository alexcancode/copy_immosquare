class Contact < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :service
  belongs_to :user

  ##============================================================##
  ## Règles de validations
  ##============================================================##
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :email, :presence => true
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
  validates :message, :length => { minimum: 4}
  validates :message, :length => { maximum: 2500}



end
