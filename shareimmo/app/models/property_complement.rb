class PropertyComplement < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :property

  before_save :check_france_dpe
  before_save :check_france_ges

  def check_france_dpe
    begin
      self.france_dpe_indice = case self.france_dpe_value.to_i
      when 0
        ""
      when 1..50
        "A"
      when 51..90
        "B"
      when 91..150
        "C"
      when 151..230
        "D"
      when 231..330
        "E"
      when 331..450
        "F"
      else
        "G"
      end if self.france_dpe_value.present?
    rescue Exception => e
      JulesLogger.info e.message
    end
  end

  def check_france_ges
    begin
      self.france_ges_indice = case self.france_ges_value.to_i
      when 0
        ""
      when 1..5
        "A"
      when 6..10
        "B"
      when 11..20
        "C"
      when 21..35
        "D"
      when 35..55
        "E"
      when 56..80
        "F"
      else
        "G"
      end if self.france_ges_value.present?
    rescue Exception => e
      JulesLogger.info e.message
    end
  end

end
