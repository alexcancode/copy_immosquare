class FeedbackReport < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :feedback

  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :contact_1, JSON
  serialize :contact_2, JSON
  serialize :contact_3, JSON


end
