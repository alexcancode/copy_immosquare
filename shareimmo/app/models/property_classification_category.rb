class PropertyClassificationCategory < ActiveRecord::Base

  ##============================================================##
  ## Assocations
  ##============================================================##
  has_many :property_classifications,  -> {order('name ASC')}


  ##============================================================##
  ## GLOBOLIZE
  ##============================================================##
  translates :name
  globalize_accessors



end
