class NomenclatureElementDefault < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :nomenclature_default
  has_ancestry
  acts_as_list scope: [:ancestry]

end
