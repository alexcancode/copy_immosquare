class Profile < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  has_many :properties
  belongs_to :font
  belongs_to :font_special, :foreign_key => :font_special_id, :class_name => 'Font'
  has_many :wysiwyg_pictures, as: :imageable,:dependent => :destroy


  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :settings, Hash


  ##============================================================##
  ## GLOBOLIZE
  ##============================================================##
  translates :job, :baseline, :bio
  globalize_accessors


  ##============================================================##
  ## Validates
  ##============================================================##
  validates :email, :presence => true
  validates :email, :email => {:strict_mode => true}
  validates :primary_color, :css_hex_color => true
  validates :secondary_color, :css_hex_color => true
  validates :job, length: { maximum: 150 }

  ##============================================================##
  ## Before & After
  ##============================================================##
  before_save :fix_country_and_phone
  before_save :update_public_name
  after_create :compile
  after_save :compile, :if => :compiled_attributes_changed?
  before_validation :smart_add_url_protocol




  ##============================================================##
  ## CUSTOM CSS
  ##============================================================##
  COMPILED_FIELDS = [:primary_color,:secondary_color,:font_id,:font_special_id]


  #============================================================##
  ## Paperclip : Profil Picture
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/brokers/:user_id_paperclip/picture_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small    => ["50x50", :png],
    :medium   => ["200x200", :png],
    :large    => ["450x450", :png]
  },
  :convert_options => {
    :small    => "-background transparent -gravity center -extent 50x50   -quality 70",
    :medium   => "-background transparent -gravity center -extent 200x200 -quality 80",
    :large    => "-background transparent -gravity center -extent 450x450",
  },
  :default_url => "https://placehold.it/450x450"

  validates_attachment_content_type :picture, :content_type => /\Aimage/
  validates_attachment_size :picture, :less_than=>2.megabytes


  #============================================================##
  ## Paperclip : Logo
  ##============================================================##
  has_attached_file :logo,
  :path => "#{Rails.env}/brokers/:user_id_paperclip/logo_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small    => ["50x50", :png],
    :medium   => ["200x200", :png],
    :large    => ["450x450", :png]
  },
  :convert_options => {
    :small    => "-background transparent -gravity center -extent 50x50   -quality 70",
    :medium   => "-background transparent -gravity center -extent 200x200 -quality 80",
    :large    => "-background transparent -gravity center -extent 450x450",
  },
  :default_url => "https://placehold.it/450x450"

  validates_attachment_content_type :logo, :content_type => /\Aimage/
  validates_attachment_size :logo, :less_than=>2.megabytes



  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_picture_post_process :rename_picture
  before_logo_post_process :rename_logo




  def self.compile(profile_id)
    profile = Profile.find(profile_id)
    body = ERB.new(File.read(File.join(Rails.root, 'app', 'assets', 'stylesheets', 'my_theme.scss.erb'))).result(profile.get_binding)
    tmp_themes_path = File.join(Rails.root,'tmp','themes')
    tmp_asset_name  = "#{profile.id.to_s}.scss"
    temp_full_path  = File.join(tmp_themes_path,tmp_asset_name)

    FileUtils.mkdir_p(tmp_themes_path) unless File.directory?(tmp_themes_path)
    File.open(temp_full_path, 'w') { |f| f.write(body) }


    begin
      if Rails.application.assets.is_a?(Sprockets::Index)
        env =  Rails.application.assets.instance_variable_get('@environment')
      else
        env = Rails.application.assets
      end

      my_asset = env.find_asset(tmp_asset_name)
      compressed_body = ::Sass::Engine.new(my_asset.body,{:syntax => :scss,:cache => false,:read_cache => false,:style => :compressed}).render
      profile.update_attribute(:theme,compressed_body.html_safe)

    rescue Sass::SyntaxError => error
      profile.revert(error)
    end
  end


  def revert(error)
    JulesLogger.info 'Revert fonction'
    JulesLogger.info error
  end

  def get_binding
    binding
  end


  private

  def compile
    self.class.compile(id)
  end

  def smart_add_url_protocol
      self.facebook     = "https://#{self.facebook}"    if(self.facebook.present?     and !(self.facebook[/\Ahttp:\/\//] || self.facebook[/\Ahttps:\/\//]))
      self.twitter      = "https://#{self.twitter}"     if(self.twitter.present?      and !(self.twitter[/\Ahttp:\/\//] || self.twitter[/\Ahttps:\/\//]))
      self.linkedin     = "https://#{self.linkedin}"    if(self.linkedin.present?     and !(self.linkedin[/\Ahttp:\/\//] || self.linkedin[/\Ahttps:\/\//]))
      self.pinterest    = "https://#{self.pinterest}"   if(self.pinterest.present?    and !(self.pinterest[/\Ahttp:\/\//] || self.pinterest[/\Ahttps:\/\//]))
      self.google_plus  = "https://#{self.google_plus}" if(self.google_plus.present?  and !(self.google_plus[/\Ahttp:\/\//] || self.google_plus[/\Ahttps:\/\//]))
      self.instagram    = "https://#{self.instagram}"   if(self.instagram.present?    and !(self.instagram[/\Ahttp:\/\//] || self.instagram[/\Ahttps:\/\//]))
  end


  def compiled_attributes_changed?
    changed_attributes.keys.map(&:to_sym).any? { |f| COMPILED_FIELDS.include?(f)}
  end

  def fix_country_and_phone
    if self.country_changed?
      country = ISO3166::Country.find_country_by_alpha2(self.country)
      self.country_alpha2 =  country.alpha2                       if country.present?
      self.country_alpha3 =  country.alpha3                       if country.present?
      self.country_name =  country.name                           if country.present?
      self.country_code =  country.country_code                   if country.present?
      self.international_prefix =  country.international_prefix   if country.present?
    end
  end

  def update_public_name
    self.public_name = [self.first_name, self.last_name].join(" ")
  end

  ##============================================================##
  ## Cette fonction renome les avatars avant de les uploaders
  ##============================================================##
  def rename_picture
    extension = File.extname(picture_file_name).downcase
    self.picture.instance_write :file_name, "avatar_#{Time.now.to_i.to_s}#{extension}"
  end

  def rename_logo
    extension = File.extname(logo_file_name).downcase
    self.logo.instance_write :file_name, "logo_#{Time.now.to_i.to_s}#{extension}"
  end


end
