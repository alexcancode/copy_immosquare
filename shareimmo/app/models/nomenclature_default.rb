class NomenclatureDefault < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  has_many :nomenclature_element_defaults,  dependent: :destroy

  ##============================================================##
  ## Fake Attribute
  ##============================================================##
  attr_accessor :user_id
end
