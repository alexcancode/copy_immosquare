class Statistic < ActiveRecord::Base

  serialize :international, Hash
  serialize :video, Hash
  serialize :lespac, Hash
  serialize :louercom, Hash
  serialize :craigslist, Hash
  serialize :facebook, Hash
  serialize :twitter, Hash


  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :user




end
