class Agency < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :users,  dependent: :nullify

end
