class CreditCard < ActiveRecord::Base
  belongs_to :user

  serialize :stripe_infos, JSON
  ##============================================================##
  ## Fake attributes
  ##============================================================##
  attr_accessor :card_number, :cvc, :stripeToken, :card_id
end
