class BlogCategory < ActiveRecord::Base

  ##============================================================##
  ## GLOBOLIZE
  ##============================================================##
  translates :name, :slug
  globalize_accessors

  ##============================================================##
  ## ASSOCIATIONS
  ##============================================================##
  has_many :blogs

  ##============================================================##
  ## Slug
  ##============================================================##
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]
  def should_generate_new_friendly_id?
    name_changed?
  end


end
