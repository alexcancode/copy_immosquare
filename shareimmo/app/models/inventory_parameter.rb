class InventoryParameter < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :user

  belongs_to :default_nomenclature, class_name: "Nomenclature", foreign_key: "nomenclature_id"

end
