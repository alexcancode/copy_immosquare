class PortalActivation < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :user
  belongs_to :portal

end
