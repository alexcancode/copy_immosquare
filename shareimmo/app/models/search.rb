class Search < ActiveRecord::Base

  ##============================================================##
  ## Fake attributes
  ##============================================================##
  attr_accessor :keywords


  def self.parse_params(params)
    Search.new.parse_params(params)
  end


  def parse_params(params)
    params=params.to_hash
    params = params.except("action").except("controller").except("locale")
    params = params.reject{ |k,v| (v == "" || !is_number?(v) )}
    assign_attributes(params)
    self
  end


  def is_number?(param)
    true if Float(param) rescue false
  end
end
