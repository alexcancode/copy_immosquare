class Theme < ActiveRecord::Base

  ##============================================================##
  ## Assocations
  ##============================================================##
  has_many :theme_settings
  has_many :users, :through => :theme_settings


  ##============================================================##
  ## Globalize
  ##============================================================##
  translates :description
  globalize_accessors

end

