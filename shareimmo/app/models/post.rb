class Post < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  has_many :wysiwyg_pictures, as: :imageable,:dependent => :destroy

  ##============================================================##
  ## Slug
  ##============================================================##
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  def should_generate_new_friendly_id?
    title_changed?
  end


  ##============================================================##
  ## Paperclip : Post Cover
  ##============================================================##
  has_attached_file :cover,
  :path => "#{Rails.env}/:class/:id/cover_:id_:style.:extension",
  :styles => {
    :small => ["366x200#", :png],
    :large => ["712x400#", :png]
  },
  :convert_options => {
    :small => "-quality 70 -strip",
    :large => "-quality 85 -strip"
  },
  :default_url => "https://placehold.it/712x400"
  validates_attachment_content_type :cover, :content_type => /\Aimage/
  validates_attachment_size :cover, :less_than=>2.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_picture

  private

  def rename_picture
    extension = File.extname(cover_file_name).downcase
    self.cover.instance_write :file_name, "shareimmo_#{Time.now.to_i.to_s}#{extension}"
  end



 end
