class Building < ActiveRecord::Base

  ##============================================================##
  ## BEFORE AND AFTER
  ##============================================================##
  before_validation :geocode, :if => :address_changed?
  before_validation :reverse_geocode, :if => Proc.new { |b| b.address.blank? }
  before_save :check_country


  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :api_provider_api
  belongs_to :user
  has_many :properties,dependent: :nullify
  has_many :building_assets, -> {order('position ASC')},dependent: :destroy
  has_many :wysiwyg_pictures, as: :imageable,:dependent => :destroy


  ##============================================================##
  ## GLOBOLIZE
  ##============================================================##
  translates :description, :description_short
  globalize_accessors


  ##============================================================##
  ## Geocoding
  ##============================================================##
  geocoded_by :address do |obj,results|
    if geo = results.first
      obj.street_number                   = (geo.address_components_of_type(:street_number).present? and geo.address_components_of_type(:street_number)[0]["long_name"] != "0") ? geo.address_components_of_type(:street_number)[0]["long_name"] : nil     if obj.street_number.blank?
      obj.street_name                     = geo.address_components_of_type(:route).present? ? geo.address_components_of_type(:route)[0]["long_name"] : nil                                               if obj.street_name.blank?
      obj.sublocality                     = geo.address_components_of_type(:neighborhood).present? ? geo.address_components_of_type(:neighborhood)[0]["long_name"] : nil                                 if obj.sublocality.blank?
      obj.locality                        = geo.address_components_of_type(:locality).present? ? geo.address_components_of_type(:locality)[0]["long_name"] : nil                                         if obj.locality.blank?
      obj.administrative_area_level_2     = geo.address_components_of_type(:administrative_area_level_2).present? ? geo.address_components_of_type(:administrative_area_level_2)[0]["long_name"] : nil   if obj.administrative_area_level_2.blank?
      obj.administrative_area_level_1     = geo.address_components_of_type(:administrative_area_level_1).present? ? geo.address_components_of_type(:administrative_area_level_1)[0]["long_name"] : nil   if obj.administrative_area_level_1.blank?
      obj.country_short                   = geo.address_components_of_type(:country).present? ? geo.address_components_of_type(:country)[0]["short_name"] : nil                                          if obj.country_short.blank?
      obj.zipcode                         = geo.address_components_of_type(:postal_code).present? ? geo.address_components_of_type(:postal_code)[0]["long_name"] : nil                                   if obj.zipcode.blank?
      obj.address    = "#{obj.street_number.present? ? "#{obj.street_number} " : nil}#{obj.street_name} #{obj.locality} #{obj.administrative_area_level_1} #{obj.country_short}"  if obj.address.blank?
    end
  end

  reverse_geocoded_by :latitude, :longitude do |obj,results|
    if geo = results.first
      obj.street_number                   = (geo.address_components_of_type(:street_number).present? and geo.address_components_of_type(:street_number)[0]["long_name"] != "0") ? geo.address_components_of_type(:street_number)[0]["long_name"] : nil     if obj.street_number.blank?
      obj.street_name                     = geo.address_components_of_type(:route).present? ? geo.address_components_of_type(:route)[0]["long_name"] : nil                                               if obj.street_name.blank?
      obj.sublocality                     = geo.address_components_of_type(:neighborhood).present? ? geo.address_components_of_type(:neighborhood)[0]["long_name"] : nil                                 if obj.sublocality.blank?
      obj.locality                        = geo.address_components_of_type(:locality).present? ? geo.address_components_of_type(:locality)[0]["long_name"] : nil                                         if obj.locality.blank?
      obj.administrative_area_level_2     = geo.address_components_of_type(:administrative_area_level_2).present? ? geo.address_components_of_type(:administrative_area_level_2)[0]["long_name"] : nil   if obj.administrative_area_level_2.blank?
      obj.administrative_area_level_1     = geo.address_components_of_type(:administrative_area_level_1).present? ? geo.address_components_of_type(:administrative_area_level_1)[0]["long_name"] : nil   if obj.administrative_area_level_1.blank?
      obj.country_short                   = geo.address_components_of_type(:country).present? ? geo.address_components_of_type(:country)[0]["short_name"] : nil                                          if obj.country_short.blank?
      obj.zipcode                         = geo.address_components_of_type(:postal_code).present? ? geo.address_components_of_type(:postal_code)[0]["long_name"] : nil                                   if obj.zipcode.blank?
      obj.address    = "#{obj.street_number.present? ? "#{obj.street_number} " : nil}#{obj.street_name} #{obj.locality} #{obj.administrative_area_level_1} #{obj.country_short}"  if obj.address.blank?
    end
  end


  ##============================================================##
  ## Elasticseach
  ##============================================================##
  include Elasticsearch::Model
  index_name "#{Rails.application.class.parent_name.underscore}_#{Rails.env}_buildings"
  document_type "building"

  after_save     { ElasticIndexer.new(:index, self.class.name, self.id)}
  after_destroy  { ElasticIndexer.new(:delete,self.class.name, self.id)}


  ##============================================================##
  ## Pour définir l'indexation de chaque champs....
  ## Si dynamic == true le bloc sert à overwrite et le reste est automatique
  ## Si dynamic == false, on définie quels sont à indexer manuellement
  ##============================================================##
  settings index: { number_of_shards:5, number_of_replicas:0 } do
    settings :analysis => {
      analyzer: {
        description_analyzer: {
          type: 'custom',
          tokenizer: 'standard',
          filter: ['asciifolding', 'lowercase']
        }
      }
    }
    mappings dynamic: true do
      indexes :location,         :type => 'geo_point'
      # indexes :description,      :analyzer => 'description_analyzer'
    end
  end

  ##============================================================##
  ## Pour définir manuellement les champs présent dans les
  ## sources
  ## Geohash ne sert pas pour elasticsearch, mais pour grouper
  ## les résultats par la suite dans la réponse de l'api
  ##============================================================##
  def as_indexed_json(options={})
    my_infos = Jbuilder.encode do |json|
      json.id                self.id
      json.uid               self.uid
      json.draft             self.draft
      json.api_provider_id   self.api_provider_id
      json.user_id           self.user_id
      json.address           self.address
      json.location_geohash  GeoHash.encode(self.latitude,self.longitude,11) if (self.latitude.present? and self.longitude.present?)
      json.locality          self.locality
      json.country           self.country_short
      json.cover_url         self.building_assets.present? ? self.building_assets[0].picture.url(:medium,:timestamp=>false) : BuildingAsset.new.picture.options[:default_url]
      json.updated_at        I18n.localize self.updated_at.to_time,:format =>("%Y-%m-%d")
      json.units             self.number_of_levels
      json.name              self.name
      if (self.latitude.present? and self.longitude.present?)
        json.location do
          json.lat   self.latitude
          json.lon   self.longitude
        end
      end
      json.description_short do
        json.fr self.description_short_fr
        json.en self.description_short_en
        json.de self.description_short_de
        json.nl self.description_short_nl
        json.it self.description_short_it
        json.es self.description_short_es
        json.pt self.description_short_pt
      end
    end
    return JSON.parse(my_infos)
  end



  ##============================================================##
  ## ~ : recherche partielle sur le mot
  ## must     : The clause (query) must appear in matching documents and will contribute to the score.
  ## filter   : The clause (query) must appear in matching documents. However unlike must the score of the query will be ignored.
  ## should   : The clause (query) should appear in the matching document. In a boolean query with no must clauses, one or more should clauses must match a document. The minimum number of should clauses to match can be set using the minimum_should_match parameter.
  ## must_not : The clause (query) must not appear in the matching documents.
  ##============================================================##
  def self.my_search_elastic(params)
    filter  = []
    should  = []
    must    = []
    sort    = []

    ##============================================================##
    ## Default Settings
    ##============================================================##
    filter << { term: { draft: 0 }}
    filter << { exists: { field: "location" }}
    filter << { exists: { field: "country" }}

    ##============================================================##
    ##
    ##============================================================##
    filter << { terms: { api_provider_id: params[:provider].split(',') }}   if params[:provider].present?
    filter << { terms: { user_id: params[:user].split(',') }}               if params[:user].present?
    filter << { terms: { "id":  params[:id].split(',') }}                   if params[:id].present?

    ##============================================================##
    ##
    ##============================================================##
    filter << { "query": { "match": { "country": params[:country] }}} if params[:country].present?

    ##============================================================##
    ## Sort
    ##============================================================##
    sort << {"#{params[:order_by]}": { "order": ['asc','desc'].include?(params['order']) ? params['order'] : 'asc'  }} if params[:order_by].present?

    ##============================================================##
    ## Search
    ##============================================================##
    @response = Building.__elasticsearch__.search(
    {
      "query": {
        "bool": {
          "filter": filter,
          "should": should,
          "must":   must,
        }
        },
        "sort": sort
      }
      ).paginate(:page => params[:page], :per_page => params[:per_page])
  end

  ##============================================================##
  ## Fin Elasticsearh
  ##============================================================##


  private

  def check_country
    begin
      if self.country_short.length > 2
        c = ISO3166::Country.find_country_by_name(self.country_short.downcase)
        self.country_short = c.present? ? c.alpha2 : "CA"
      elsif self.country_short.length == 2
        self.country_short = self.country_short.upcase
      else
        self.country_short = "CA"
      end
    rescue
      self.country_short = "CA"
    end
  end


end
