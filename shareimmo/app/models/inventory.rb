
class Inventory < ActiveRecord::Base


  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :property
  belongs_to :user


  #============================================================##
  ## Paperclip : XML
  ##============================================================##
  has_attached_file :xml,
  :path => "#{Rails.env}/inventories/:id/shareimmo_inventory_:xml_file_name_:style.:extension"
  validates_attachment_size :xml, :less_than => 20.megabytes
  validates_attachment_content_type :xml, :content_type => ["application/xml", "text/xml", "application/json", "application/javascript"]
  validates :xml, :presence => { :message => "Bad file"}
#  before_post_process :rename_xml


  ##============================================================##
  ## Active Record Callbacks
  ##============================================================##
  before_create :init_secure_params

  private

  def init_secure_params
    self.secure_id = Digest::SHA1.hexdigest([Time.now, self.id].join)
    self.secure_password = Random.rand(1000..9999)
  end

  def rename_xml
    extension = MIME::Types[xml_content_type].first.extensions.first
    self.xml.instance_write :file_name, "inventory_#{Time.now.to_i.to_s}.#{extension}"
  end

end
