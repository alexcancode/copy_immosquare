class PropertyInclusion < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :property

  def format
    my_inclusion   = Array.new
    my_inclusion << { :name => I18n.t('back_office.orientation'),:inclus => Array.new}
    [:orientation_north,:orientation_south,:orientation_east,:orientation_west].each do |i|
      my_inclusion[0][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.equipments'),:inclus => Array.new}
    [:inclusion_air_conditioning,:inclusion_hot_water,:inclusion_heated,:inclusion_electricity,:inclusion_furnished,:inclusion_fridge,:inclusion_cooker,:inclusion_dishwasher,:inclusion_washer,:inclusion_dryer,:inclusion_microwave].each do |i|
      my_inclusion[1][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.details'),:inclus => Array.new}
    [:detail_elevator,:detail_laundry,:detail_garbage_chute,:detail_common_space,:detail_janitor,:detail_gym,:detail_golf,:detail_tennis,:detail_sauna,:detail_spa,:detail_inside_pool,:detail_outside_pool,:detail_inside_parking,:detail_outside_parking,:detail_parking_on_street,:detail_garagebox].each do |i|
      my_inclusion[2][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.half_furnsihed'),:inclus => Array.new}
    [:half_furnsihed_living_room,:half_furnsihed_rooms,:half_furnsihed_kitchen,:half_furnsihed_other].each do |i|
      my_inclusion[3][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.indoor_features'),:inclus => Array.new}
    [:indoor_attic,:indoor_cellar,:indoor_central_vacuum,:indoor_entries_washer_dryer,:indoor_entries_dishwasher,:indoor_fireplace,:indoor_storage,:indoor_walk_in,:indoor_no_smoking,:indoor_double_glazing,:indoor_triple_glazing].each do |i|
      my_inclusion[4][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.floor_covering'),:inclus => Array.new}
    [:floor_carpet,:floor_wood,:floor_floating,:floor_ceramic,:floor_parquet,:floor_cushion,:floor_vinyle,:floor_lino,:floor_marble].each do |i|
      my_inclusion[5][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.exterior_features'),:inclus => Array.new}
    [:exterior_land_access,:exterior_back_balcony,:exterior_front_balcony,:exterior_private_patio,:exterior_storage,:exterior_terrace,:exterior_veranda,:exterior_garden].each do |i|
      my_inclusion[6][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.accessibility'),:inclus => Array.new}
    [:accessibility_elevator,:accessibility_balcony,:accessibility_grab_bar,:accessibility_wider_corridors,:accessibility_lowered_switches,:accessibility_ramp].each do |i|
      my_inclusion[7][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.senior'),:inclus => Array.new}
    [:senior_autonomy,:senior_half_autonomy,:senior_no_autonomy,:senior_meal,:senior_nursery,:senior_domestic_help,:senior_community,:senior_activities,:senior_validated_residence,:senior_housing_cooperative].each do |i|
      my_inclusion[8][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.pets'),:inclus => Array.new}
    [:pets_allow,:pets_cat,:pets_dog,:pets_little_dog,:pets_cage_aquarium,:pets_not_allow].each do |i|
      my_inclusion[9][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.services'),:inclus => Array.new}
    [:service_internet,:service_tv,:service_tv_sat,:service_phone].each do |i|
      my_inclusion[10][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.heating'),:inclus => Array.new}
    [:heating_electric,:heating_solar,:heating_gaz,:heating_condensation,:heating_fuel,:heating_heat_pump,:heating_floor_heating].each do |i|
      my_inclusion[11][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.composition'),:inclus => Array.new}
    [:composition_bar,:composition_living,:composition_dining,:composition_separe_toilet,:composition_open_kitchen].each do |i|
      my_inclusion[12][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.transport'),:inclus => Array.new}
    [:transport_bicycle_path,:transport_public_transport,:transport_public_bike,:transport_subway,:transport_train_station].each do |i|
      my_inclusion[13][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end
    my_inclusion << { :name => I18n.t('back_office.security'),:inclus => Array.new}
    [:security_guardian,:security_alarm,:security_intercom,:security_camera,:security_smoke_dectector].each do |i|
      my_inclusion[14][:inclus].push(I18n.t("activerecord.attributes.property_inclusion.#{i}")) if self[i] == 1
    end

    return my_inclusion
  end
end
