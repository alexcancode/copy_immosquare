class Support < ActiveRecord::Base
  belongs_to :support_type


  ##============================================================##
  ## Slug
  ##============================================================##
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  def should_generate_new_friendly_id?
    title_changed?
  end


end
