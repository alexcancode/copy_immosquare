class ThemeSetting < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  belongs_to :theme



  ##============================================================##
  ## Paperclip : Website Cover
  ##============================================================##
  has_attached_file :website_cover,
  :path => "#{Rails.env}/brokers/:user_id_paperclip/website_cover_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :large => ["1900x430#", :jpg]
  },
  :convert_options => {
    :large => "-background white -flatten +matte -quality 90 -strip"
  },
  :default_url => "https://placehold.it/1900x430"
  validates_attachment_content_type :website_cover, :content_type => /\Aimage/
  validates_attachment_size :website_cover, :less_than=>2.megabytes



  ##============================================================##
  ## Paperclip : Website CoverFull
  ##============================================================##
  has_attached_file :website_cover_full_size,
  :path => "#{Rails.env}/brokers/:user_id_paperclip/website_cover_full_size_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :large => ["990x660#", :jpg]
  },
  :convert_options => {
    :large => "-background white -flatten +matte -quality 90 -strip"
  },
  :default_url => "https://placehold.it/990x660"
  validates_attachment_content_type :website_cover, :content_type => /\Aimage/
  validates_attachment_size :website_cover, :less_than=>2.megabytes



  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_website_cover_post_process :rename_cover

  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_website_cover_full_size_post_process :rename_website_cover_full_size

  private

  def rename_cover
    extension = File.extname(website_cover_file_name).downcase
    self.website_cover.instance_write :file_name, "shareimmo_cover_#{Time.now.to_i.to_s}#{extension}"
  end

  def rename_website_cover_full_size
    extension = File.extname(website_cover_full_size_file_name).downcase
    self.website_cover.instance_write :file_name, "shareimmo_cover_#{Time.now.to_i.to_s}#{extension}"
  end


end
