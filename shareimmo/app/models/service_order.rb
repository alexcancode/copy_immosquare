class ServiceOrder < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :user
  belongs_to :property
  belongs_to :service
end
