class PropertyAsset < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :property
  belongs_to :room

  ##============================================================##
  ## AutoUpdate position on creation
  ##============================================================##
  acts_as_list scope: :property


  ##============================================================##
  ## PaperClip Interoplation
  ##============================================================##
  Paperclip.interpolates :property_id do |attachment, style|
    attachment.instance.property_id
  end

  ##============================================================##
  ## Paperclip : Property_asset
  ## not option:
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/properties/:property_id/shareimmo_:property_id_:id_:style.:extension",
  :styles => {
    :medium       => ["960x",       :jpg],
    :medium_crop  => ["960x640#",   :jpg],
    :medium_hd    => ["960x640",    :jpg],
    :large        => ["1920x",      :jpg],
    :large_crop   => ["1920x1280#", :jpg],
    :large_hd     => ["1920x1280",  :jpg],
    },
    :convert_options => {
      :medium       => "-strip -quality 80",
      :medium_crop  => "-strip -quality 90",
      :medium_hd    => "-background black -gravity center -extent 960x640  -quality 90",
      :large        => "-strip -quality 80",
      :large_crop   => "-strip -quality 95",
      :large_hd     => "-background black -gravity center -extent 1920x1280 -quality 100",
    },

    :default_url => "https://placehold.it/1920X1280"
    validates_attachment_content_type :picture, :content_type => /\Aimage/
    validates_attachment_size :picture, :less_than=>11.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_picture


  private

  def rename_picture
    extension = MIME::Types[picture_content_type].first.extensions.first
    self.picture.instance_write :file_name, "shareimmo_#{Time.now.to_i.to_s}.#{extension}"
  end



end
