class Feedback < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :property
  has_many :feedback_reports, dependent: :destroy

  ##============================================================##
  ## fake Attribute
  ##============================================================##
  attr_accessor :day,:hour



end
