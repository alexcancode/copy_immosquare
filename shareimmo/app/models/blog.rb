class Blog < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  belongs_to :blog_category
  has_many :wysiwyg_pictures, as: :imageable,:dependent => :destroy


  ##============================================================##
  ## Slug
  ##============================================================##
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders,:scoped], :scope => :blog_category_id
  def should_generate_new_friendly_id?
    title_changed?
  end


  ##============================================================##
  ## Paperclip : Post Cover
  ##============================================================##
  has_attached_file :cover,
  :path => "#{Rails.env}/brokers/:user_id_paperclip/blog/:id/cover_:style.:extension",
  :styles => {
    :small => ["630x315#", :jpg],
    :medium => ["630x630#", :jpg],
    :large => ["1920x784#", :jpg],
  },
  :convert_options => {
    :small => "-background white -gravity center",
    :medium => "-background white -gravity center",
    :large => "-background white -gravity center"
  },
  :default_url => "https://placehold.it/1920x784"
  validates_attachment_content_type :cover, :content_type => /\Aimage/
  validates_attachment_size :cover, :less_than=>2.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_picture

  private

  def rename_picture
    extension = File.extname(cover_file_name).downcase
    self.cover.instance_write :file_name, "shareimmo_#{Time.now.to_i.to_s}#{extension}"
  end



end
