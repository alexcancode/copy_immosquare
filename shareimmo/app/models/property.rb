class Property < ActiveRecord::Base


  ##============================================================##
  ## includes router helper
  ##============================================================##
  include Rails.application.routes.url_helpers
  include MoneyHelper


  ##============================================================##
  ## BEFORE AND AFTER
  ##============================================================##
  before_validation :geocode,           :if => Proc.new { |property| property.address_changed? or (property.latitude.blank? or property.longitude.blank?) }
  before_validation :reverse_geocode,   :if => Proc.new { |property| property.address.blank? and (property.latitude.present? and property.longitude.present?) }
  before_save :check_country
  before_save :check_halfroom
  after_save :check_google_place_id



  ##============================================================##
  ## Règles de validations
  ##============================================================##
  validates :property_classification, :presence => true
  validates :property_status,         :presence => true
  validates :property_listing,        :presence => true


  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  belongs_to :agency
  belongs_to :area_unit
  belongs_to :api_provider
  belongs_to :building
  belongs_to :property_classification,  -> {order('name ASC')}
  belongs_to :property_status,          -> {order('name ASC')}
  belongs_to :property_flag,            -> {order('name ASC')}
  belongs_to :property_listing,         -> {order('name ASC')}
  has_many :property_assets,            -> {order('position ASC')},      dependent: :destroy
  has_many :documents,                  -> {order('position ASC')},      dependent: :destroy
  has_many :share_socials,        dependent: :destroy
  has_many :feedbacks,            dependent: :destroy
  has_many :hostings,             dependent: :destroy
  has_many :visibility_portals,   dependent: :destroy
  has_many :videos,               dependent: :destroy
  has_many :wysiwyg_pictures, as: :imageable,:dependent => :destroy
  has_many :service_orders
  has_one :property_inclusion,  dependent: :destroy
  has_one :property_complement, dependent: :destroy
  has_and_belongs_to_many :property_contacts


  ##============================================================##
  ## Pagination
  ##============================================================##
  self.per_page = 100

  ##============================================================##
  ##
  ##============================================================##
  accepts_nested_attributes_for :property_complement

  ##============================================================##
  ## PaperClip Interpolates
  ##============================================================##
  Paperclip.interpolates('secure_token') do |attachment, style|
    attachment.instance.property.secure_id.parameterize
  end



  ##============================================================##
  ## GLOBOLIZE
  ##============================================================##
  translates :title, :description, :property_url, :description_short
  globalize_accessors


  ##============================================================##
  ## MONETIZE
  ##============================================================##
  monetize :price_cents,    :allow_nil => true,  with_model_currency: :currency
  monetize :rent_cents,     :allow_nil => true,  with_model_currency: :currency
  monetize :tax_1_cents,    :allow_nil => true,  with_model_currency: :currency
  monetize :tax_2_cents,    :allow_nil => true,  with_model_currency: :currency
  monetize :fees_cents,     :allow_nil => true,  with_model_currency: :currency
  monetize :income_cents,   :allow_nil => true,  with_model_currency: :currency
  monetize :deposit_cents,  :allow_nil => true,  with_model_currency: :currency






  ##============================================================##
  ## Geocoding
  ##============================================================##
  geocoded_by :address do |obj,results|
    if geo = results.first
      obj.latitude                        = geo.latitude
      obj.longitude                       = geo.longitude
      obj.street_number                   = (geo.address_components_of_type(:street_number).present? and geo.address_components_of_type(:street_number)[0]["long_name"] != "0") ? geo.address_components_of_type(:street_number)[0]["long_name"] : nil     if obj.street_number.blank?
      obj.street_name                     = geo.address_components_of_type(:route).present? ? geo.address_components_of_type(:route)[0]["long_name"] : nil                                               if obj.street_name.blank?
      obj.sublocality                     = geo.address_components_of_type(:neighborhood).present? ? geo.address_components_of_type(:neighborhood)[0]["long_name"] : nil                                 if obj.sublocality.blank?
      obj.locality                        = geo.address_components_of_type(:locality).present? ? geo.address_components_of_type(:locality)[0]["long_name"] : nil                                         if obj.locality.blank?
      obj.administrative_area_level_2     = geo.address_components_of_type(:administrative_area_level_2).present? ? geo.address_components_of_type(:administrative_area_level_2)[0]["long_name"] : nil   if obj.administrative_area_level_2.blank?
      obj.administrative_area_level_1     = geo.address_components_of_type(:administrative_area_level_1).present? ? geo.address_components_of_type(:administrative_area_level_1)[0]["long_name"] : nil   if obj.administrative_area_level_1.blank?
      obj.country_short                   = geo.address_components_of_type(:country).present? ? geo.address_components_of_type(:country)[0]["short_name"] : nil                                          if obj.country_short.blank?
      obj.zipcode                         = geo.address_components_of_type(:postal_code).present? ? geo.address_components_of_type(:postal_code)[0]["long_name"] : nil                                   if obj.zipcode.blank?
      obj.address                         = "#{obj.street_number.present? ? "#{obj.street_number} " : nil}#{obj.street_name} #{obj.locality} #{obj.administrative_area_level_1} #{obj.country_short}"  if obj.address.blank?
    end
  end

  reverse_geocoded_by :latitude, :longitude do |obj,results|
    if geo = results.first
      obj.street_number                   = (geo.address_components_of_type(:street_number).present? and geo.address_components_of_type(:street_number)[0]["long_name"] != "0") ? geo.address_components_of_type(:street_number)[0]["long_name"] : nil     if obj.street_number.blank?
      obj.street_name                     = geo.address_components_of_type(:route).present? ? geo.address_components_of_type(:route)[0]["long_name"] : nil                                               if obj.street_name.blank?
      obj.sublocality                     = geo.address_components_of_type(:neighborhood).present? ? geo.address_components_of_type(:neighborhood)[0]["long_name"] : nil                                 if obj.sublocality.blank?
      obj.locality                        = geo.address_components_of_type(:locality).present? ? geo.address_components_of_type(:locality)[0]["long_name"] : nil                                         if obj.locality.blank?
      obj.administrative_area_level_2     = geo.address_components_of_type(:administrative_area_level_2).present? ? geo.address_components_of_type(:administrative_area_level_2)[0]["long_name"] : nil   if obj.administrative_area_level_2.blank?
      obj.administrative_area_level_1     = geo.address_components_of_type(:administrative_area_level_1).present? ? geo.address_components_of_type(:administrative_area_level_1)[0]["long_name"] : nil   if obj.administrative_area_level_1.blank?
      obj.country_short                   = geo.address_components_of_type(:country).present? ? geo.address_components_of_type(:country)[0]["short_name"] : nil                                          if obj.country_short.blank?
      obj.zipcode                         = geo.address_components_of_type(:postal_code).present? ? geo.address_components_of_type(:postal_code)[0]["long_name"] : nil                                   if obj.zipcode.blank?
      obj.address                         = "#{obj.street_number.present? ? "#{obj.street_number} " : nil}#{obj.street_name} #{obj.locality} #{obj.administrative_area_level_1} #{obj.country_short}"  if obj.address.blank?
    end
  end




  def cover_url_small
    self.property_assets.present? ? self.property_assets[0].picture.url(:medium) : PropertyAsset.new.picture.options[:default_url]
  end

  def cover_url_large
    self.property_assets.present? ? self.property_assets[0].picture.url(:large) : PropertyAsset.new.picture.options[:default_url]
  end


  ##============================================================##
  ## On essaie de trouver le google_place_id si possible
  ##============================================================##
  def check_google_place_id
    begin
      if self.google_place_id.blank?
        client = GooglePlaces::Client.new(ENV["GOOGLE_API_KEY"])
        spots = client.spots_by_query(self.address)
        self.update_columns(:google_place_id => spots.first.place_id) if spots.present?
      end
    rescue Exception => e
      JulesLogger.info "error #{e.message}"
    end
  end




  def self.shareimmo_currency
    currency_array = Array.new
    [:eur,:cad,:usd,:gbp,:idr,:xpf,:thb].each do |curr|
      currency = Money::Currency.find(curr)
      currency_array << currency
    end
    currency_array
  end







  ##============================================================##
  ## Elasticseach
  ##============================================================##
  include Elasticsearch::Model
  index_name "#{Rails.application.class.parent_name.underscore}_#{Rails.env}_properties"
  document_type "property"

  after_save     { ElasticIndexer.new(:index, self.class.name, self.id)}
  after_destroy  { ElasticIndexer.new(:delete,self.class.name, self.id)}


  ##============================================================##
  ## Pour définir l'indexation de chaque champs....
  ## Si dynamic == true le bloc sert à overwrite et le reste est automatique
  ## Si dynamic == false, on définie quels sont à indexer manuellement
  ##============================================================##
  settings index: { number_of_shards:5, number_of_replicas:0 } do
    settings :analysis => {
      analyzer: {
        description_analyzer: {
          type: 'custom',
          tokenizer: 'standard',
          filter: ['asciifolding', 'lowercase']
        }
      }
    }
    mappings dynamic: true do
      indexes :location,         :type => 'geo_point'
      # indexes :description,      :analyzer => 'description_analyzer'
    end
  end

  ##============================================================##
  ## Pour définir manuellement les champs présent dans les
  ## sources
  ## Geohash ne sert pas pour elasticsearch, mais pour grouper
  ## les résultats par la suite dans la réponse de l'api
  ##============================================================##
  def as_indexed_json(options={})
    building  = self.building || Building.new


    my_infos = Jbuilder.encode do |json|
      json.id                self.id
      json.uid               self.uid
      json.draft             self.draft
      json.api_provider_id   self.api_provider_id
      json.user_id           self.user_id
      json.status            self.status
      json.address           self.address
      json.price             self.property_listing_id == 1 ? (self.price_cents.present? ? self.price_cents/100 : 0) : (self.rent_cents.present? ? self.rent_cents/100 : 0)
      json.currency          self.currency.downcase
      json.price_formated    my_beautiful_price(self)
      json.price_with_tax    self.price_with_tax
      json.price_from        self.price_from
      json.location_geohash  GeoHash.encode(self.latitude,self.longitude,11) if (self.latitude.present? and self.longitude.present?)
      json.cover_url         self.property_assets.present? ? self.property_assets[0].picture.url(:medium,:timestamp=>false) : PropertyAsset.new.picture.options[:default_url]
      json.cover_url_hd      self.property_assets.present? ? self.property_assets[0].picture.url(:medium_hd,:timestamp=>false) : PropertyAsset.new.picture.options[:default_url]
      json.area              self.area_living
      json.created_at        I18n.localize self.created_at.to_time,:format =>("%Y-%m-%d")
      json.updated_at        I18n.localize self.updated_at.to_time,:format =>("%Y-%m-%d")
      json.link              short_link_path(self.id,:locale =>"fr")
      json.rooms             self.rooms
      json.bedrooms          self.bedrooms
      json.bathrooms         self.bathrooms
      json.showerrooms       self.showerrooms
      json.locality          self.locality
      json.country           self.country_short
      json.unit              self.unit
      if (self.latitude.present? and self.longitude.present?)
        json.location do
          json.lat   self.latitude
          json.lon   self.longitude
        end
      end
      json.property_listing do
        if self.property_listing.present?
          json.id          self.property_listing_id
          json.fr          self.property_listing.name_fr
          json.en          self.property_listing.name_en
        end
      end
      json.property_status do
        if self.property_status.present?
          json.id          self.property_status_id
          json.fr          self.property_status.name_fr
          json.en          self.property_status.name_en
        end
      end
      json.property_flag do
        if self.property_flag.present?
          json.id          self.property_flag_id
          json.fr          self.property_flag.name_fr
          json.en          self.property_flag.name_en
        end
      end
      json.property_classification do
        if self.property_classification.present?
          json.id self.property_classification.api_id
          json.fr self.property_classification.name_fr
          json.en self.property_classification.name_en
          json.category do
            if self.property_classification.property_classification_category.present?
              json.id self.property_classification.property_classification_category.id
              json.fr self.property_classification.property_classification_category.name_fr
              json.en self.property_classification.property_classification_category.name_en
            end
          end
        end
      end
      json.title do
        json.fr self.title_fr
        json.en self.title_en
        json.de self.title_de
        json.nl self.title_nl
        json.it self.title_it
        json.es self.title_es
        json.pt self.title_pt
      end
      json.description_short do
        json.fr self.description_short_fr
        json.en self.description_short_en
        json.de self.description_short_de
        json.nl self.description_short_nl
        json.it self.description_short_it
        json.es self.description_short_es
        json.pt self.description_short_pt
      end
      json.building do
        json.id             building.present? ? building.id : nil
        json.name           building.present? ? building.name : nil
        json.units          building.present? ? building.number_of_units : nil
        json.cover_url      building.building_assets.present? ? building.building_assets[0].picture.url(:medium,:timestamp=>false) : BuildingAsset.new.picture.options[:default_url]
        json.cover_url_hd   building.building_assets.present? ? building.building_assets[0].picture.url(:hd_m,:timestamp=>false) : BuildingAsset.new.picture.options[:default_url]
      end
    end
    return JSON.parse(my_infos)
  end




  ##============================================================##
  ## ~ : recherche partielle sur le mot
  ## must     : The clause (query) must appear in matching documents and will contribute to the score.
  ## filter   : The clause (query) must appear in matching documents. However unlike must the score of the query will be ignored.
  ## should   : The clause (query) should appear in the matching document. In a boolean query with no must clauses, one or more should clauses must match a document. The minimum number of should clauses to match can be set using the minimum_should_match parameter.
  ## must_not : The clause (query) must not appear in the matching documents.
  ##============================================================##
  def self.my_search_elastic(params)
    filter  = []
    should  = []
    must    = []
    sort    = []

    ##============================================================##
    ## Default Settings
    ##============================================================##
    filter << { term: { draft: 0 }}
    filter << { term: { status: 1 }}
    # filter << { exists: { field: "location" }}
    # filter << { exists: { field: "country" }}

    ##============================================================##
    ##
    ##============================================================##
    filter << { terms: { api_provider_id: params[:provider].split(',') }}       if params[:provider].present?
    filter << { terms: { user_id: params[:user].split(',') }}                   if params[:user].present?
    filter << { terms: { "id":  params[:id].split(',') }}                       if params[:id].present?
    filter << { terms: { "building.id":  params[:building].split(',') }}        if params[:building].present?

    ##============================================================##
    ##
    ##============================================================##
    filter << { "query": { "match": { "country": params[:country] }}} if params[:country].present?




    ##============================================================##
    ## Slider
    ##============================================================##
    filter << { range: { price: { from: params[:price_from] }}}   if params[:price_from].present?
    filter << { range: { price: { to:   params[:price_to]   }}}   if params[:price_to].present?
    filter << { range: { area:  { from: params[:area_from]  }}}   if params[:area_from].present?
    filter << { range: { area:  { to:   params[:area_to]    }}}   if params[:area_to].present?


    ##============================================================##
    ## Geoloc
    ## bounds = sw_lat,sw_lng,ne_lat,ne_lng
    ##============================================================##
    if params[:bounds].present? and params[:custom_circle].blank? and params[:custom_poly].blank?
      bounds = params[:bounds].split(',')
      if(bounds[1].to_f < -180 or bounds[3].to_f > 180)
        bounds[1] = -180
        bounds[3] = 180
      end

      filter << {
        geo_bounding_box: {
          location: {
            bottom_left: { lat: [-90,bounds[0].to_f].max, lon: bounds[1]},
            top_right:   { lat: [90,bounds[2].to_f].min, lon: bounds[3]}
          }
        }
      }
    end
    if params[:custom_circle].present?
      circle = params[:custom_circle].split(',')
      filter << {geo_distance: {location: {lat: circle[0], lon: circle[1]},:distance => circle[2]}}
    end
    if params[:custom_poly].present?
      points = Array.new
      params[:custom_poly].split(',').in_groups_of(2) do |p|
        points << {lat: p[0], lon:p[1]}
      end
      filter << { geo_polygon: { location: { points: points}}}
    end
    if params[:zones].present?
      response = HTTParty.get("http://meilleurprix.immo/api/v1/zones?ids=#{params[:zones]}")
      body = JSON.parse(response.body)
      polyFilters = []
      body["cities"].each do |z|
        z["bounds"].map do |b|
          b["lon"] = b["lng"]
          b.delete("lng")
        end
        polyFilters << { geo_polygon: { location: { points: z["bounds"]}}}
      end
      filter << { or: polyFilters }
    end


    ##============================================================##
    ## Dropdowns
    ##============================================================##
    filter << {      terms: { "property_classification.id":  params[:subclassification].split(',') }}     if params[:subclassification].present?
    filter << {not:{ terms: { "property_classification.id":  params[:subclassification!].split(',') }}}   if params[:subclassification!].present?


    filter << {       terms: { "property_classification.category.id":  params[:classification].split(',') }}    if params[:classification].present?
    filter << {not: { terms: { "property_classification.category.id":  params[:classification!].split(',') }}}  if params[:classification!].present?


    filter << {      terms: { "property_status.id": params[:status].split(',') }}     if params[:status].present?
    filter << {not: {terms: { "property_status.id": params[:status!].split(',') }}}   if params[:status!].present?


    filter << {       terms: { "property_flag.id":  params[:flag].split(',') }}     if params[:flag].present?
    filter << {not: { terms: { "property_flag.id":  params[:flag!].split(',') }}}   if params[:flag!].present?


    filter << {       terms: { "property_listing.id":   params[:listing].split(',') }}    if params[:listing].present?
    filter << {not:{  terms: { "property_listing.id":   params[:listing!].split(',') }}}  if params[:listing!].present?


    filter << {terms: { rooms:        (params[:rooms].to_i..40).to_a }}         if params[:rooms].present?
    filter << {terms: { bedrooms:     (params[:bedrooms].to_i..40).to_a }}      if params[:bedrooms].present?
    filter << {terms: { bathrooms:    (params[:bathrooms].to_i..40).to_a }}     if params[:bathrooms].present?


    ##============================================================##
    ## Référence
    ##============================================================##
    filter << { query: { multi_match: { query: params[:reference].downcase, operator: :or, fields: %w(id uid) }}} if params[:reference].present?


    #============================================================##
    ## Keywords
    ##============================================================##
    should << {"query_string":{"query": params[:keywords]}}       if params[:keywords].present?


    ##============================================================##
    ## Sort
    ##============================================================##
    sort << {"#{params[:order_by]}": { "order": ['asc','desc'].include?(params['order']) ? params['order'] : 'asc'  }} if params[:order_by].present?

    ##============================================================##
    ## Search
    ##============================================================##
    @response = Property.__elasticsearch__.search(
    {
      "query": {
        "bool": {
          "filter": filter,
          "should": should,
          "must":   must,
        }
        },
        "sort": sort
      }
      ).paginate(:page => params[:page], :per_page => params[:per_page])
  end

  ##============================================================##
  ## Fin Elasticsearh
  ##============================================================##





  private



  def check_country
    begin
      if self.country_short.length > 2
        c = ISO3166::Country.find_country_by_name(self.country_short.downcase)
        self.country_short = c.present? ? c.alpha2 : "CA"
      elsif self.country_short.length == 2
        self.country_short = self.country_short.upcase
      else
        self.country_short = "CA"
      end
    rescue
      self.country_short = "CA"
    end
  end

  def check_halfroom
    self.withHalf = 1   if self.country_short == "CA"
  end

end


