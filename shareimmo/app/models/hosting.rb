class Hosting < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  belongs_to :property


  ##============================================================##
  ## Validation
  ##============================================================##
  validates :domain, :uniqueness => true
  validates :domain, format: { :with=> /\A(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\z/ix,message: "is not a url"}




  def self.check_domain_name(field,value)
    validity = Hosting.new(field => value)
    if validity.valid?
      return true
    else
      return false
    end
  end



end
