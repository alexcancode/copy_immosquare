class BuildingAsset < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :building

  ##============================================================##
  ## AutoUpdate position on creation
  ##============================================================##
  acts_as_list scope: :building



  ##============================================================##
  ## PaperClip Interoplation
  ##============================================================##
  Paperclip.interpolates :building_id do |attachment, style|
    attachment.instance.building_id
  end

  ##============================================================##
  ## Paperclip : building_asset
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/buildings/:building_id/shareimmo_:building_id_:id_:style.:extension",
  :styles => {
    :small    => ["480x270#",   :jpg],
    :medium   => ["960x540#",   :jpg],
    :large    => ["1920x1080#", :jpg],
    :hd_s     => ["480x270",    :jpg],
    :hd_m     => ["960x540",    :jpg],
    :hd       => ["1920x1080",  :jpg],
    },
    :convert_options => {
      :small      => "-strip -quality 70",
      :medium     => "-strip -quality 80",
      :large      => "-strip -quality 100",
      :hd_s       => "-background black -gravity center -extent 480x270     -quality 70",
      :hd_m       => "-background black -gravity center -extent 960x540     -quality 80",
      :hd         => "-background black -gravity center -extent 1920x1080   -quality 100"
    },
  :default_url => "https://placehold.it/900X600"

  validates_attachment_content_type :picture, :content_type => /\Aimage/
  validates_attachment_size :picture, :less_than=>11.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_picture


  private

  def rename_picture
    extension = MIME::Types[picture_content_type].first.extensions.first
    self.picture.instance_write :file_name, "shareimmo_#{Time.now.to_i.to_s}.#{extension}"
  end




end
