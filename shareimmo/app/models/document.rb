class Document < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :property

  ##============================================================##
  ## AutoUpdate position on creation
  ##============================================================##
  acts_as_list scope: :property


  ##============================================================##
  ## Paperclip : Document
  ##============================================================##
  has_attached_file :document,
  :path         => "#{Rails.env}/documents/:property_id/:filename",
  :default_url  => "https://placehold.it/960X540"

  validates_attachment_presence :document, :if => Proc.new{|document| document.draft == 0}
  validates_attachment_content_type :document, :content_type => ['application/pdf', 'application/msword', 'text/plain']
  validates_attachment_size :document, :less_than=>11.megabytes



end
