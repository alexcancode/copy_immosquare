class ApiProvider < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :api_imports

  has_one :api_app

end
