class BrokerContact < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :property
  belongs_to :user


  ##============================================================##
  ## Validations
  ##============================================================##
  validates :email, :presence => true
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
  validates :message, :length => { minimum: 4}
  validates :message, :length => { maximum: 2500}

end
