class CustomService < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :user



  #============================================================##
  ## Paperclip : cover
  ##============================================================##
  has_attached_file :cover,
  :path => "#{Rails.env}/brokers/:user_id_paperclip/custom_services/:id/cover_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small    => ["960x540#", :png],
    :large    => ["1920x1080#", :png]
  },
  :default_url => "https://placehold.it/1920x1080"

  validates_attachment_content_type :cover, :content_type => /\Aimage/
  validates_attachment_size :cover, :less_than=>2.megabytes



end
