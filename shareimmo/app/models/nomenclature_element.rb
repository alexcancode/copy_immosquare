class NomenclatureElement < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :nomenclature
  has_ancestry
  acts_as_list scope: [:ancestry]


end
