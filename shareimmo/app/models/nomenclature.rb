# encoding: utf-8

class Nomenclature < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :users
  has_many :nomenclature_elements,  dependent: :destroy

  ##============================================================##
  ## fake Attribute
  ##============================================================##
  attr_accessor :file


  ##============================================================##
  ## Import Nomenclature
  ##============================================================##
  def self.import(file ,user_id)
    return false if file.nil? or file.content_type != "text/csv"
    file_name = file.original_filename.split('.csv')
    new_csv_format = file_name.any? { |s| s.include?('_nomenclature') }
    begin
      @nomenclature  = Nomenclature.create(
        :user_id=> user_id,
        :name=> new_csv_format == true ? file_name.join.gsub("_nomenclature", "") : file_name.join,
        :description=>"Importé le #{I18n.localize Time.now,:format =>("%d %B %Y")}"
      )
      if new_csv_format == true
        CSV.foreach(file.path,:col_sep => "\t") do |i|
          NomenclatureElement.create(
            :nomenclature_id => @nomenclature.id,
            :import_id => i[0],
            :name=> i[1].present? ? i[1] : nil,
            :value => i[2].present? ? i[2] : nil,
            :element_type => i[3].present? ? i[3] : nil,
            :status =>i[4].present? ? i[4] : nil,
            :parent_id => i[5].present? ? (NomenclatureElement.where(:nomenclature_id=>@nomenclature.id,:import_id=>i[5].to_i).first).id : nil,
            :is_the_standard_element => i[6].present? ? i[6] : 0,
          )
        end
      else
        @my_ids = Array.new
        CSV.foreach(file.path,:col_sep => "\t") do |i|
          import_old_immopad_nomenclature(i,file.path,@nomenclature.id)
        end
      end
      return true
    rescue
      @nomenclature.nomenclature_elements.roots.destroy_all
      @nomenclature.destroy
      return false
    end
  end

  def to_csv(options = {})
    CSV.generate(options) do |csv|
      self.nomenclature_elements.where(:ancestry=>nil).order(:position).each do |n|
        csv << [n.id,n.name,n.value,n.element_type,n.status,n.parent_id,n.is_the_standard_element]
        complete_csv(n.descendants.arrange(:order => :position),csv)
      end
    end
  end



  private

  def complete_csv(elements,csv)
    elements.each do |m,children|
      csv << [m.id,m.name,m.value,m.element_type,m.status,m.parent_id]
      complete_csv(children,csv) if children.present?
    end
  end


  def self.import_old_immopad_nomenclature(i,file_path,nomenclature_id)
    @my_ids << i[0].to_i
    search_parent_in_csv(file_path,i[1],nomenclature_id) if i[1].present? and !@my_ids.include?(i[1].to_i)
    import_element(i,nomenclature_id)
  end


  def self.search_parent_in_csv(file_path,my_id,nomenclature_id)
    CSV.foreach(file_path,:col_sep => "\t") do |i|
      next if i[0] != my_id
      import_old_immopad_nomenclature(i,file_path,nomenclature_id)
    end
  end

  def self.import_element(i,nomenclature_id)
    NomenclatureElement.create(
      :nomenclature_id => nomenclature_id,
      :import_id => i[0],
      :name=> i[2].present? ? i[2] : nil,
      :element_type => i[3].present? ? i[3] : nil,
      :value => i[7].present? ? i[7] : nil,
      :status =>i[5].present? ? i[5] : nil,
      :parent_id => i[1].present? ? (NomenclatureElement.where(:nomenclature_id=>nomenclature_id,:import_id=>i[1].to_i).first).id : nil
      )
  end


end
