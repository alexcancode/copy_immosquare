class Glossary < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  has_many :glossary_elements, -> { order "content" }, dependent: :destroy


end
