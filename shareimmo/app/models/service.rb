class Service < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  has_and_belongs_to_many :users
  has_and_belongs_to_many :providers

  ##============================================================##
  ## AutoUpdate position on creation
  ##============================================================##
  acts_as_list


  ##============================================================##
  ## Globalize
  ##============================================================##
  translates :name,:description, :marketing_description
  globalize_accessors


end
