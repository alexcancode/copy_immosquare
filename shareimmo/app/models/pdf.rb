class Pdf < ActiveRecord::Base


  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :dictionary, JSON



  #============================================================##
  ## Paperclip : Document
  ##============================================================##
  has_attached_file :document,
  :path => "#{Rails.env}/pdfs/:id/:filename"
  validates_attachment_content_type :document, :content_type => ['application/pdf']


end
