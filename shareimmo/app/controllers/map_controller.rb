class MapController < ApplicationController

  layout 'map'

  def index
    @datas = {
      :mapboxtoken      =>  ENV['MAPBOX_API_TOKEN'],
      :maptheme         =>  'dark',
      :mapthemes        =>  ["streets","bright","light","dark"],
    }
  end

end
