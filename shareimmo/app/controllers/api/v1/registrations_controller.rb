class Api::V1::RegistrationsController < Devise::RegistrationsController

  skip_before_action :verify_authenticity_token

  def create
    accounts_infos = Array.new
    begin
      return render :json =>{:error=>"No Credentials Found"}, :status=>403 if params[:credentials].blank?
      @api_app = ApiApp.where(:key=>params[:credentials][:apiKey],:token=>params[:credentials][:apiToken]).first
      return render :json =>{:message=>"Not Authorized"}, :status=>403 if @api_app.blank?
      if params["accounts"].present?
        params["accounts"].each do |account|
          if account["uid"].present? and  account["email"].present?
            @portal = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:uid => account[:uid])
            if @portal.present?
              accounts_infos << {
                account["uid"] => {
                  :id => nil,
                  :uid => account["uid"],
                  :status => 0 ,
                  :email => account["email"],
                  :error => "Uid already used for your api"
                }
              }
            else
              @user = User.find_by_email( account["email"])
              if @user.present?
                if @user.user_portal_providers.where(:api_provider_id=>@api_app.api_provider_id).present?
                  accounts_infos << {
                    account["uid"] => {
                      :id => @user.id,
                      :uid => account["uid"],
                      :status => 0 ,
                      :email => account["email"],
                      :error => "This email is already use with an other uid"
                    }
                  }
                else
                  UserPortalProvider.create(:api_provider_id => @api_app.api_provider_id, :uid => account[:uid], :user_id => @user.id)
                  accounts_infos << {
                    account["uid"] => {
                      :id => @user.id,
                      :uid => account["uid"],
                      :status => 1 ,
                      :email => account["email"],
                      :error => nil
                    }
                  }
                end
              else
                @user = User.new(:email => account["email"],:password => account["password"].present? ? account["password"] : account["email"])
                if @user.save
                  UserPortalProvider.create(:api_provider_id => @api_app.api_provider_id, :uid => account[:uid], :user_id => @user.id)
                  BackOfficeMailer.new_user_registred(@user).deliver_later
                end
                accounts_infos << {
                  account["uid"] => {
                    :id => @user.save ? @user.id : nil,
                    :uid => account["uid"],
                    :status => @user.save ? 1 : 0 ,
                    :email => account["email"],
                    :error => @user.save ? nil : @user.errors
                  }
                }
              end
            end
          end
        end
      end
    rescue Exception => e
      accounts_infos << {:error => e.message}
    end
    render :json =>{:result=> accounts_infos}, :status=>201
  end


end
