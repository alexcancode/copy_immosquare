class Api::V1::UsersController < Api::V1::ApplicationController

  require "open-uri"
  require 'net/http'

  def connect
    begin
      @user = User.where(:connect_token=>params[:token]).first if params[:token].present?
      sign_in(@user, :store =>true)
      respond_with @user, location: after_sign_in_path_for(@user)
    rescue Exception => e
      flash[:danger] = "Access Denied"
      redirect_to root_path
    end
  end


  def connect_encode
    begin
      return render :json =>{:error=>"No Credentials Found"}, :status=>403 if params[:credentials].blank?
      api_app = ApiApp.where(:key=>params[:credentials][:apiKey],:token=>params[:credentials][:apiToken]).first
      return render :json =>{:message=>"Not Authorized"}, :status=>403 if api_app.blank?
      user = User.where(:uid=>params[:uid],:api_provider_id=>api_app.api_provider_id).first
      return render :json =>{:message=>"User not found"}, :status=>403 if user.blank?
      @token = SecureRandom.hex(45)
      user.update_attribute(:connect_token,@token)
    rescue Exception => e
    end
  end

  def edit
    begin
      return render :json =>{:error=>"No Credentials Found"}, :status=>403 if params[:credentials].blank?
      api_app = ApiApp.where(:key=>params[:credentials][:apiKey],:token=>params[:credentials][:apiToken]).first
      return render :json =>{:message=>"Not Authorized"}, :status=>403 if api_app.blank?


      portal = UserPortalProvider.where(:api_provider_id=>api_app.api_provider_id,:uid=>params[:uid])
      if portal.present?
        user = portal.first.user
      else
        raise "User not found"
      end
      profile = user.profile
      my_json = params[:user]
      if my_json["infos"].present?
        profile.picture_original_url  = my_json["infos"]["avatarUrl"]
        profile.picture               = open(my_json["infos"]["avatarUrl"]) if profile.picture_original_url_changed? and url_is_a_image?(my_json["infos"]["avatarUrl"])
        profile.email                 = my_json["infos"]["email"]
        profile.first_name            = my_json["infos"]["firstName"]
        profile.last_name             = my_json["infos"]["lastName"]
        profile.public_name           = my_json["infos"]["fullName"] || "#{my_json["infos"]["firstName"]} #{my_json["infos"]["lastName"]}"
        profile.job                   = my_json["infos"]["job"]
        profile.phone                 = my_json["infos"]["phone"]
        profile.phone_visibility      = is_number?(my_json[:infos]["phoneVisibility"]) ? ([0,1].include?(my_json[:infos]["phoneVisibility"].to_i) ? my_json[:infos]["phoneVisibility"] : 0) : 0
        profile.spoken_languages      = my_json["infos"]["spokenLanguages"]
        if my_json[:infos][:company].present?
          profile.agency_address    = my_json[:infos][:company]["address"]
          profile.agency_city       = my_json[:infos][:company]["city"]
          profile.logo_original_url = my_json[:infos]["company"]["logoUrl"]
          profile.logo              = open(my_json[:infos]["company"]["logoUrl"]) if profile.logo_original_url_changed? and url_is_a_image?(my_json[:infos]["company"]["logoUrl"])
          profile.agency_name       = my_json[:infos][:company]["name"]
          profile.agency_province   = my_json[:infos][:company]["province"]
          profile.agency_zipcode    = my_json[:infos][:company]["zipcode"]
          profile.website           = my_json[:infos][:company]["website"]
        end
        if my_json[:infos][:socials].present?
          profile.facebook           = my_json["infos"]["socials"]["facebook"]
          profile.twitter            = my_json["infos"]["socials"]["twitter"]
          profile.linkedin           = my_json["infos"]["socials"]["linkedin"]
          profile.pinterest          = my_json["infos"]["socials"]["pinterest"]
          profile.google_plus        = my_json["infos"]["socials"]["googlePlus"]
        end
        if my_json[:infos][:websiteInfos].present?
          if my_json[:infos][:websiteInfos]["baseline"].present?
            my_json[:infos][:websiteInfos]["baseline"].each do |baseline|
              profile.baseline_fr = baseline[1] if baseline[0] == "fr"
              profile.baseline_en = baseline[1] if baseline[0] == "en"
            end
          end
          if my_json[:infos][:websiteInfos]["bio"].present?
            my_json[:infos][:websiteInfos]["bio"].each do |bio|
              profile.bio_fr = bio[1] if bio[0] == "fr"
              profile.bio_en = bio[1] if bio[0] == "en"
            end
          end
          profile.blog_active       = is_number?(my_json[:infos][:websiteInfos][:blogActif]) ? ([0,1].include?(my_json[:infos][:websiteInfos][:blogActif].to_i) ? my_json[:infos][:websiteInfos][:blogActif] : 0) : 0
          profile.primary_color     = my_json[:infos][:websiteInfos][:color1]
          profile.secondary_color   = my_json[:infos][:websiteInfos][:color2]
          if my_json[:infos][:websiteInfos][:theme].present?
            if is_number?(my_json[:infos][:websiteInfos][:theme][:theme_selected]) and [1,2,3].include?(my_json[:infos][:websiteInfos][:theme][:theme_selected].to_i)
              user.theme_settings.update_all(:is_selected=>0)
              ThemeSetting.where(:user_id => user.id,:theme_id =>Theme.find(my_json[:infos][:websiteInfos][:theme][:theme_selected]).id).first_or_create.update_attribute(:is_selected,1)
            end
          end
          if my_json[:infos][:websiteInfos][:subdomain].present?
            new_subdomain = my_json[:infos][:websiteInfos][:subdomain].downcase
            user.username =  new_subdomain
            user.update_attribute(:username,new_subdomain) if user.valid?
          end
        end
      end
      profile.save
      render :json => {
        :status => profile.save ? 1 : 0,
        :kangalou_website => "http://#{user.username}.monkangalou.com",
        :oneclic_website => "http://#{user.username}.1clic.immo",
        :errors => profile.save ? nil : profile.errors
        }, :status=>201

      rescue Exception => e
        JulesLogger.info "Error message  => #{e.message}"
        ApiMailer.error_monitoring(
          :title    => "api.v1.users.edit",
          :message  => e.message,
          :params   => params
          ).deliver_now
      end

    end








    private

    def is_number?(string)
      true if Float(string) rescue false
    end

    def url_is_a_image?(url)
      begin
        FastImage.size(url).kind_of?(Array)
      rescue Exception => e
        JulesLogger.info e.message
        return false
      end
    end



  end
