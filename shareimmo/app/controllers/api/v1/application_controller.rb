module Api
  module V1
    class ApplicationController < ActionController::Base

      ##============================================================##
      ## Before & After
      ##============================================================##
      skip_before_action :verify_authenticity_token
      after_action :update_auth_header



      def update_auth_header
        return unless @user and @user.valid? and @client_id and @token
        expiry = @user.tokens[@client_id]['expiry'] || @user.tokens[@client_id][:expiry]
        auth_header = {
          "access-token" => @token,
          "token-type"   => "Bearer",
          "client"       => @client_id,
          "expiry"       => expiry.to_s,
          "uid"          => @user.email
        }
        response.headers.merge!(auth_header)
        # Add logs
        # Api Version

      end



      private


      def set_user_by_token
        uid        = request.headers['uid'] || params['uid']
        @client_id = request.headers['client'] || params['client']
        @token     = request.headers['access-token'] || params['access-token']
        return false unless uid
        @user = User.where(:email =>uid).first
        if @user && valid_token?(@user,@token, @client_id)
          sign_in(:user, @user, store: false, bypass: true)
          return @user
        else
          return @user = nil
        end
      end


      def check_account_uid
        unless User.where(params[:accountUid]).first.nil?
        end
      end


      def valid_token?(user,token, client_id)
        return false unless user.tokens[client_id]
        return true if token_is_current?(user,token, client_id)
        return false
      end


      def token_is_current?(user,token, client_id)
        expiry     = user.tokens[client_id]['expiry'] || user.tokens[client_id][:expiry]
        token_hash = user.tokens[client_id]['token'] || user.tokens[client_id][:token]
        return true if (
          expiry and token and DateTime.strptime(expiry.to_s, '%s') > Time.now and BCrypt::Password.new(token_hash) == token
          )
      end


     

    end
  end
end
