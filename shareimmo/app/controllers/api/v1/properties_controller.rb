class Api::V1::PropertiesController < ApplicationController


  skip_before_action :verify_authenticity_token




  def show_visibility
    @property = Property.find(params[:id]) if params[:id].present?
    @property = Property.joins(:api_provider).where(:uid=>params[:uid],:api_providers => {:name => params[:provider]}).first if params[:uid].present? and params[:id].blank?
    @property = Property.joins(:api_provider).where(:uid2=>params[:uid2],:api_providers => {:name => params[:provider]}).first if params[:uid2].present? and params[:id].blank?
    @listglobally_link = HTTParty.get("https://identityserver.portia.ch:33333/api/v1/onbehalfof?apikey=#{ENV['ListGlobally_api_key']}&accountid=#{Rails.env.production? ? @property.user_id : 1 }") if @property.present?
  end




  def import
    begin
      raise "No Credentials Found" if params[:credentials].blank?
      api_app = ApiApp.where(:key=>params[:credentials][:apiKey],:token=>params[:credentials][:apiToken]).first
      raise "Not Authorized" if api_app.blank?
      raise "params not set" if params[:properties].blank?
      my_app = api_app
      properties_infos  = Array.new

      params[:properties].each do |property|
        portal = UserPortalProvider.where(:api_provider_id=>my_app.api_provider_id,:uid=>property["accountUid"])
        if portal.present?
          user = portal.first.user
        else
          raise "User #{property["accountUid"]} not defined"
        end
        ##============================================================##
        ## 1er Vérification : on check si l'annonce appartient à bien
        ## à un user qui peut utiliser l'api
        ##============================================================##
        if user.present? and property["uid"].present?
          if property["id"].present?
            prop = Property.where(:id => property["id"]).first
            raise "property not found" if prop.blank?
            if prop.is_sync_with_api == my_app.api_provider_id
              prop.uid  = property["uid"]
              prop.uid2 = property["uid2"]
              prop.api_provider_id = my_app.api_provider_id
            end
          else
            prop = Property.where(:api_provider_id=>my_app.api_provider_id,:uid=>property["uid"],:user_id=>user.id).first_or_create
            prop.is_sync_with_api = my_app.api_provider_id  if (prop.secure_id.blank?)
          end
          raise "Not authorized to update this API" if prop.is_sync_with_api != my_app.api_provider_id


          ##============================================================##
          ## Gestion de la property... À sauvergarder avant de créer
          ## les visi, pictures, et inclusions pour récuperérer l'id
          ## en création
          ##============================================================##
          prop.secure_id = SecureRandom.urlsafe_base64 if prop.secure_id.blank?
          prop.user_id = user.id
          prop.draft = 0
          prop.address_visibility = 1
          prop.uid2 = property["uid2"] if property["uid2"].present?
          prop.status = is_number?(property["status"]) ? ([0,1].include?(property["status"].to_i) ? property["status"] : 0) : 0
          if property["geo"].present?
            if property["geo"]["geocoding"].present?
              prop.latitude             = property["geo"]['geocoding']['latitude']
              prop.longitude            = property["geo"]['geocoding']['longitude']
              prop.address              = property["geo"]['geocoding']['address']
            end
            prop.street_number                    = property["geo"]['streetNumber']
            prop.street_name                      = property["geo"]['streetName']
            prop.sublocality                      = property["geo"]['subLocality']
            prop.locality                         = property["geo"]['locality']
            prop.zipcode                          = property["geo"]['zipcode']
            prop.administrative_area_level_1      = property["geo"]['province']
            prop.administrative_area_level_2      = property["geo"]['municipality']
            prop.country_short                    = property["geo"]['country']
            prop.unit                             = property["geo"]['unit']
            prop.level                            = property["geo"]['level']
            prop.cadastre                         = property["geo"]["cadastre"]
            if property["geo"]["building"].present? and  property["geo"]["building"]["uid"].present?
              building = Building.where(:uid=>property["geo"]["building"]["uid"],:user_id=>user.id).first_or_create
              prop.building_id = building.id
              if property["geo"]["building"]["geocoding"].present?
                building.latitude             = property["geo"]["building"]["geocoding"]['latitude']
                building.longitude            = property["geo"]["building"]["geocoding"]['longitude']
                building.address              = property["geo"]["building"]["geocoding"]['address']
              end
              building.secure_id = SecureRandom.urlsafe_base64 if building.secure_id.blank?
              building.draft = 0
              building.api_provider_id                      = my_app.api_provider_id
              building.name                                 = property["geo"]["building"]["name"]
              building.number_of_levels                     = property["geo"]["building"]["NumberOfLevels"]
              building.number_of_units                      = property["geo"]["building"]["NumberOfUnits"]
              building.street_number                        = property["geo"]["building"]["streetNumber"]
              building.street_name                          = property["geo"]["building"]["streetName"]
              building.zipcode                              = property["geo"]["building"]["zipcode"]
              building.locality                             = property["geo"]["building"]["city"]
              building.sublocality                          = property["geo"]["building"]["subLocality"]
              building.administrative_area_level_1          = property["geo"]["building"]["province"]
              building.administrative_area_level_2          = property["geo"]["building"]['municipality']
              building.country_short                        = property["geo"]["building"]["country"]
              if property["geo"]["building"]["description"].present?
                property["geo"]["building"]["description"].each do |description|
                  building.description_fr = description[1] if description[0] == "fr"
                  building.description_en = description[1] if description[0] == "en"
                  building.description_de = description[1] if description[0] == "de"
                  building.description_nl = description[1] if description[0] == "nl"
                  building.description_it = description[1] if description[0] == "it"
                  building.description_es = description[1] if description[0] == "es"
                  building.description_pt = description[1] if description[0] == "pt"
                end
              end
              building.save(:validate => false)
              if property["geo"]["building"]["pictures"].present?
                property["geo"]["building"]["pictures"].each_with_index do |picture,limit|
                  if limit <= 50
                    if picture["url"].present?
                      pics = BuildingAsset.where(:building_id=>building.id,:original_url=>picture["url"])
                      if pics.blank?
                        pics = BuildingAsset.new(:building_id=>building.id,:original_url=>picture["url"])
                        if url_is_a_image?(picture["url"])
                          pics.picture = open(picture["url"])
                          pics.save
                        end
                      end
                    end
                  end
                end
              end
            end
          end
          if property["title"].present?
            property["title"].each do |title|
              prop.title_fr = title[1] if title[0] == "fr"
              prop.title_en = title[1] if title[0] == "en"
              prop.title_de = title[1] if title[0] == "de"
              prop.title_nl = title[1] if title[0] == "nl"
              prop.title_it = title[1] if title[0] == "it"
              prop.title_es = title[1] if title[0] == "es"
              prop.title_pt = title[1] if title[0] == "pt"
            end
          end
          if property["description"].present?
            property["description"].each do |description|
              prop.description_fr = description[1] if description[0] == "fr"
              prop.description_en = description[1] if description[0] == "en"
              prop.description_de = description[1] if description[0] == "de"
              prop.description_nl = description[1] if description[0] == "nl"
              prop.description_it = description[1] if description[0] == "it"
              prop.description_es = description[1] if description[0] == "es"
              prop.description_pt = description[1] if description[0] == "pt"
            end
          end
          if property["url"].present?
            property["url"].each do |url|
              prop.property_url_fr = url[1] if url[0] == "fr"
              prop.property_url_en = url[1] if url[0] == "en"
              prop.property_url_de = url[1] if url[0] == "de"
              prop.property_url_nl = url[1] if url[0] == "nl"
              prop.property_url_it = url[1] if url[0] == "it"
              prop.property_url_es = url[1] if url[0] == "es"
              prop.property_url_pt = url[1] if url[0] == "pt"
            end
          end
          if property["area"].present?
            prop.area_living = property["area"]["living"]
            prop.area_land = property["area"]["land"]
            prop.area_balcony = property["area"]["balcony"]
            prop.area_unit_id = property["area"]["unit"] == "mc" ? 1 : 2
          end
          if property["rooms"].present?
            prop.rooms = property["rooms"]["rooms"]
            prop.withHalf = property["rooms"]["withHalf"] == "yes" ? 1 : 0
            prop.bedrooms = property["rooms"]["bedrooms"]
            prop.bathrooms = property["rooms"]["bathrooms"]
            prop.showerrooms = property["rooms"]["showerrooms"]
          end
          if property["financial"].present?
            prop.price = property["financial"]["price"]
            prop.price_from = property["financial"]["priceFrom"]
            prop.availability_date = property["financial"]["availableDate"]
            if property["financial"]["rentInfos"].present?
              prop.rent = property["financial"]["rentInfos"]["rent"]
              prop.fees = property["financial"]["rentInfos"]["fees"]
              prop.rent_frequency = property["financial"]["rentInfos"]["frequency"]
            end
            if property["financial"]["annual"].present?
              prop.tax_1 = property["financial"]["annual"]["tax_1"]
              prop.tax_2 = property["financial"]["annual"]["tax_2"]
              prop.income = property["financial"]["annual"]["income"]
            end
            prop.currency = property["financial"]['currency']
          end
          if property["infos"].present?
            prop.property_classification_id = check_classification(property["infos"]['propertyClassification'])
            prop.property_status_id = is_number?(property["infos"]['propertyStatus']) ? ([1,2].include?(property["infos"]['propertyStatus'].to_i) ?  property["infos"]['propertyStatus'] : 1) : 1
            prop.property_listing_id = is_number?(property["infos"]['propertyListing']) ? ([1,2].include?(property["infos"]['propertyListing'].to_i) ? property["infos"]['propertyListing'] : 1 ) : 1
            prop.property_flag_id = is_number?(property["infos"]['propertyFlag']) ? ([1,2,3,4,5].include?(property["infos"]['propertyFlag'].to_i) ? property["infos"]['propertyFlag'] : 1 ) : 1
            prop.year_of_built = property["infos"]['yearOfBuild']
          end
          prop.save if prop.changed?
          ##============================================================##
          ## Fin de sauvegarde de la property prop.id est defnie
          ## dans tous les cas
          ##============================================================##


          ##============================================================##
          ## Pictures : à faire avant la génération de la vidéos
          ##============================================================##
          property_images_current = Array.new
          if property["pictures"].present?
            property["pictures"].each_with_index do |picture,limit|
              if limit <= 50
                if picture["url"].present?
                  pic = PropertyAsset.where(:property_id=>prop.id,:original_url=>picture["url"]).first
                  if pic.present?
                    property_images_current << pic.id
                    pic.move_to_bottom
                  else
                    if url_is_a_image?(picture["url"])
                      pic = PropertyAsset.new(:property_id=>prop.id,:original_url=>picture["url"])
                      pic.picture = open(picture["url"])
                      pic.save
                      property_images_current << pic.id
                      pic.move_to_bottom
                    else
                      JulesLogger.info "NOOOOOOOOOO"
                    end
                  end
                end
              end
            end
          end
          PropertyAsset.where(:property_id => prop.id).where.not(:id=>property_images_current).destroy_all
          ##============================================================##
          ## End Pictures
          ##============================================================##


          ##============================================================##
          ## Visibility
          ##============================================================##
          if property["visibility"].present?
            visibility_start               = property["visibility"]["start"].present? ? property["visibility"]["start"] : Time.now
            visibility_until               = property["visibility"]["until"].present? ? property["visibility"]["until"] : Time.now+1.month

            ##============================================================##
            ## International
            ##============================================================##
            if(is_number?(property["visibility"]["international"]) and [0,1].include?(property["visibility"]["international"].to_i))
              VisibilityPortal.where(:portal_id =>Portal.find_by_slug("listglobally").id, :property_id => prop.id).first_or_create.update_attributes(
                :posting_status => property["visibility"]["international"],
                :visibility_start => visibility_start,
                :visibility_until => visibility_until
                )
            end

            ##============================================================##
            ## Canada
            ##============================================================##
            if property["visibility"]["canada"].present?
              ##============================================================##
              ## louer
              ##============================================================##
              if(is_number?(property["visibility"]["canada"]["louer_com"]) and [0,1].include?(property["visibility"]["canada"]["louer_com"].to_i))
                VisibilityPortal.where(:portal_id => Portal.find_by_slug("louer").id, :property_id => prop.id).first_or_create.update_attributes(
                  :posting_status => property["visibility"]["canada"]["louer_com"],
                  :visibility_start => visibility_start,
                  :visibility_until => visibility_until
                  )
              end

              ##============================================================##
              ## lespac
              ##============================================================##
              if(is_number?(property["visibility"]["canada"]["lespac"]) and [0,1].include?(property["visibility"]["canada"]["lespac"].to_i))
                VisibilityPortal.where(:portal_id => Portal.find_by_slug("lespac").id, :property_id => prop.id).first_or_create.update_attributes(
                  :posting_status => property["visibility"]["canada"]["lespac"],
                  :visibility_start => visibility_start,
                  :visibility_until => visibility_until
                  )
              end
              ##============================================================##
              ## craigslist
              ##============================================================##
              if(is_number?(property["visibility"]["canada"]["craigslist"]) and [0,1].include?(property["visibility"]["canada"]["craigslist"].to_i))
                VisibilityPortal.where(:portal_id => Portal.find_by_slug("craigslist").id, :property_id => prop.id).first_or_create.update_attributes(
                  :posting_status => property["visibility"]["canada"]["craigslist"],
                  :visibility_start => visibility_start,
                  :visibility_until => visibility_until
                  )
              end
            end


            ##============================================================##
            ## Videos
            ##============================================================##
            if property["visibility"]["youtube"] == 0
              begin
                Video.where(:property_id => prop.id).each do |old_video|
                  old_video.update_attribute(:posting_status,0)
                  if(old_video.youtube_id.present? and  old_video.authentification.present?)
                    account = Yt::Account.new(:refresh_token => old_video.authentification.refresh_token)
                    Yt::Video.new(:id =>old_video.youtube_id,:auth => account).delete
                  end
                end
              rescue Exception => e
                ApiMailer.error_monitoring(
                  :title    => "api.v1.property.delete_video",
                  :message  => "#{e.message}",
                  :params   => params
                  ).deliver_now
              end
            end

            if property["visibility"]["youtube"] == 1
              VideoGeneratorWorker.perform_async(
                :property_id      => prop.id,
                :youtube_info_url => "#{request.base_url}/api/v2/youtube/#{prop.id}",
                :channel          => prop.user_id,
                :visibility_start => visibility_start,
                :visibility_until => visibility_until
                )
            end
          end
          ##============================================================##
          ## End Visibility
          ##============================================================##


          ##============================================================##
          ## Inclusions
          ##============================================================##
          inclusion = PropertyInclusion.where(:property_id=>prop.id).first_or_create
          if property["inclusions"].present?
            inclusion.inclusion_air_conditioning = is_number?(property["inclusions"]["airConditioning"]) ? ([0,1].include?(property["inclusions"]['airConditioning'].to_i) ? property["inclusions"]['airConditioning'] : 0 ) : 0
            inclusion.inclusion_hot_water = is_number?(property["inclusions"]["hotWater"]) ? ([0,1].include?(property["inclusions"]['hotWater'].to_i) ? property["inclusions"]['hotWater'] : 0 ) : 0
            inclusion.inclusion_heated = is_number?(property["inclusions"]["heated"]) ? ([0,1].include?(property["inclusions"]['heated'].to_i) ? property["inclusions"]['heated'] : 0 ) : 0
            inclusion.inclusion_electricity = is_number?(property["inclusions"]["electricity"]) ? ([0,1].include?(property["inclusions"]['electricity'].to_i) ? property["inclusions"]['electricity'] : 0 ) : 0
            inclusion.inclusion_furnished = is_number?(property["inclusions"]["furnished"]) ? ([0,1].include?(property["inclusions"]['furnished'].to_i) ? property["inclusions"]['furnished'] : 0 ) : 0
            inclusion.inclusion_fridge = is_number?(property["inclusions"]["fridge"]) ? ([0,1].include?(property["inclusions"]['fridge'].to_i) ? property["inclusions"]['fridge'] : 0 ) : 0
            inclusion.inclusion_cooker = is_number?(property["inclusions"]["cooker"]) ? ([0,1].include?(property["inclusions"]['cooker'].to_i) ? property["inclusions"]['cooker'] : 0 ) : 0
            inclusion.inclusion_dishwasher = is_number?(property["inclusions"]["dishwasher"]) ? ([0,1].include?(property["inclusions"]['dishwasher'].to_i) ? property["inclusions"]['dishwasher'] : 0 ) : 0
            inclusion.inclusion_dryer = is_number?(property["inclusions"]["dryer"]) ? ([0,1].include?(property["inclusions"]['dryer'].to_i) ? property["inclusions"]['dryer'] : 0 ) : 0
            inclusion.inclusion_washer = is_number?(property["inclusions"]["washer"]) ? ([0,1].include?(property["inclusions"]['washer'].to_i) ? property["inclusions"]['washer'] : 0 ) : 0
            inclusion.inclusion_microwave = is_number?(property["inclusions"]["microwave"]) ? ([0,1].include?(property["inclusions"]['microwave'].to_i) ? property["inclusions"]['microwave'] : 0 ) : 0
          end
          if property["partiallyFurnished"].present?
            inclusion.half_furnsihed_living_room = is_number?(property["partiallyFurnished"]["livingRoom"]) ? ([0,1].include?(property["partiallyFurnished"]['livingRoom'].to_i) ? property["partiallyFurnished"]['livingRoom'] : 0 ) : 0
            inclusion.half_furnsihed_rooms = is_number?(property["partiallyFurnished"]["rooms"]) ? ([0,1].include?(property["partiallyFurnished"]['rooms'].to_i) ? property["partiallyFurnished"]['rooms'] : 0 ) : 0
            inclusion.half_furnsihed_kitchen = is_number?(property["partiallyFurnished"]["kitchen"]) ? ([0,1].include?(property["partiallyFurnished"]['kitchen'].to_i) ? property["partiallyFurnished"]['kitchen'] : 0 ) : 0
            inclusion.half_furnsihed_other = is_number?(property["partiallyFurnished"]["other"]) ? ([0,1].include?(property["partiallyFurnished"]['other'].to_i) ? property["partiallyFurnished"]['other'] : 0 ) : 0
          end
          if property["composition"].present?
            inclusion.composition_bar = is_number?(property["composition"]["bar"]) ? ([0,1].include?(property["composition"]['bar'].to_i) ? property["composition"]['bar'] : 0 ) : 0
            inclusion.composition_living = is_number?(property["composition"]["living"]) ? ([0,1].include?(property["composition"]['living'].to_i) ? property["composition"]['living'] : 0 ) : 0
            inclusion.composition_dining = is_number?(property["composition"]["dining"]) ? ([0,1].include?(property["composition"]['dining'].to_i) ? property["composition"]['dining'] : 0 ) : 0
            inclusion.composition_separe_toilet = is_number?(property["composition"]["separateToilet"]) ? ([0,1].include?(property["composition"]['separateToilet'].to_i) ? property["composition"]['separateToilet'] : 0 ) : 0
            inclusion.composition_open_kitchen = is_number?(property["composition"]["openKitchen"]) ? ([0,1].include?(property["composition"]['openKitchen'].to_i) ? property["composition"]['openKitchen'] : 0 ) : 0
          end
          if property["heatingSystems"].present?
            inclusion.heating_electric = is_number?(property["heatingSystems"]["electric"]) ? ([0,1].include?(property["heatingSystems"]['electric'].to_i) ? property["heatingSystems"]['electric'] : 0 ) : 0
            inclusion.heating_solar = is_number?(property["heatingSystems"]["solar"]) ? ([0,1].include?(property["heatingSystems"]['solar'].to_i) ? property["heatingSystems"]['solar'] : 0 ) : 0
            inclusion.heating_gaz = is_number?(property["heatingSystems"]["gaz"]) ? ([0,1].include?(property["heatingSystems"]['gaz'].to_i) ? property["heatingSystems"]['gaz'] : 0 ) : 0
            inclusion.heating_condensation = is_number?(property["heatingSystems"]["condensation"]) ? ([0,1].include?(property["heatingSystems"]['condensation'].to_i) ? property["heatingSystems"]['condensation'] : 0 ) : 0
            inclusion.heating_fuel = is_number?(property["heatingSystems"]["fuel"]) ? ([0,1].include?(property["heatingSystems"]['fuel'].to_i) ? property["heatingSystems"]['fuel'] : 0 ) : 0
            inclusion.heating_heat_pump = is_number?(property["heatingSystems"]["heatPump"]) ? ([0,1].include?(property["heatingSystems"]['heatPump'].to_i) ? property["heatingSystems"]['heatPump'] : 0 ) : 0
            inclusion.heating_floor_heating = is_number?(property["heatingSystems"]["floorHeating"]) ? ([0,1].include?(property["heatingSystems"]['floorHeating'].to_i) ? property["heatingSystems"]['floorHeating'] : 0 ) : 0
          end
          if property["details"].present?
            inclusion.detail_elevator = is_number?(property["details"]["elevator"]) ? ([0,1].include?(property["details"]['elevator'].to_i) ? property["details"]['elevator'] : 0 ) : 0
            inclusion.detail_laundry = is_number?(property["details"]["laundry"]) ? ([0,1].include?(property["details"]['laundry'].to_i) ? property["details"]['laundry'] : 0 ) : 0
            inclusion.detail_garbage_chute = is_number?(property["details"]["garbageChute"]) ? ([0,1].include?(property["details"]['garbageChute'].to_i) ? property["details"]['garbageChute'] : 0 ) : 0
            inclusion.detail_common_space = is_number?(property["details"]["commonSpace"]) ? ([0,1].include?(property["details"]['commonSpace'].to_i) ? property["details"]['commonSpace'] : 0 ) : 0
            inclusion.detail_janitor = is_number?(property["details"]["janitor"]) ? ([0,1].include?(property["details"]['janitor'].to_i) ? property["details"]['janitor'] : 0 ) : 0
            inclusion.detail_gym = is_number?(property["details"]["gym"]) ? ([0,1].include?(property["details"]['gym'].to_i) ? property["details"]['gym'] : 0 ) : 0
            inclusion.detail_sauna = is_number?(property["details"]["sauna"]) ? ([0,1].include?(property["details"]['sauna'].to_i) ? property["details"]['sauna'] : 0 ) : 0
            inclusion.detail_spa = is_number?(property["details"]["spa"]) ? ([0,1].include?(property["details"]['spa'].to_i) ? property["details"]['spa'] : 0 ) : 0
            inclusion.detail_inside_pool = is_number?(property["details"]["insidePool"]) ? ([0,1].include?(property["details"]['insidePool'].to_i) ? property["details"]['insidePool'] : 0 ) : 0
            inclusion.detail_outside_pool = is_number?(property["details"]["outsidePool"]) ? ([0,1].include?(property["details"]['outsidePool'].to_i) ? property["details"]['outsidePool'] : 0 ) : 0
            inclusion.detail_inside_parking = is_number?(property["details"]["insideParking"]) ? ([0,1].include?(property["details"]['insideParking'].to_i) ? property["details"]['insideParking'] : 0 ) : 0
            inclusion.detail_outside_parking = is_number?(property["details"]["outsideParking"]) ? ([0,1].include?(property["details"]['outsideParking'].to_i) ? property["details"]['outsideParking'] : 0 ) : 0
            inclusion.detail_parking_on_street = is_number?(property["details"]["parkingOnStreet"]) ? ([0,1].include?(property["details"]['parkingOnStreet'].to_i) ? property["details"]['parkingOnStreet'] : 0 ) : 0
            inclusion.detail_garagebox = is_number?(property["details"]["garageBox"]) ? ([0,1].include?(property["details"]['garageBox'].to_i) ? property["details"]['garageBox'] : 0 ) : 0
          end
          if property["indoorFeatures"].present?
            inclusion.indoor_attic = is_number?(property["indoorFeatures"]["attic"]) ? ([0,1].include?(property["indoorFeatures"]['attic'].to_i) ? property["indoorFeatures"]['attic'] : 0 ) : 0
            inclusion.indoor_cellar = is_number?(property["indoorFeatures"]["cellar"]) ? ([0,1].include?(property["indoorFeatures"]['cellar'].to_i) ? property["indoorFeatures"]['cellar'] : 0 ) : 0
            inclusion.indoor_central_vacuum = is_number?(property["indoorFeatures"]["centralVacuum"]) ? ([0,1].include?(property["indoorFeatures"]['centralVacuum'].to_i) ? property["indoorFeatures"]['centralVacuum'] : 0 ) : 0
            inclusion.indoor_entries_washer_dryer = is_number?(property["indoorFeatures"]["entriesWasherDryer"]) ? ([0,1].include?(property["indoorFeatures"]['entriesWasherDryer'].to_i) ? property["indoorFeatures"]['entriesWasherDryer'] : 0 ) : 0
            inclusion.indoor_entries_dishwasher = is_number?(property["indoorFeatures"]["entriesDishwasher"]) ? ([0,1].include?(property["indoorFeatures"]['entriesDishwasher'].to_i) ? property["indoorFeatures"]['entriesDishwasher'] : 0 ) : 0
            inclusion.indoor_fireplace = is_number?(property["indoorFeatures"]["fireplace"]) ? ([0,1].include?(property["indoorFeatures"]['fireplace'].to_i) ? property["indoorFeatures"]['fireplace'] : 0 ) : 0
            inclusion.indoor_storage = is_number?(property["indoorFeatures"]["storage"]) ? ([0,1].include?(property["indoorFeatures"]['storage'].to_i) ? property["indoorFeatures"]['storage'] : 0 ) : 0
            inclusion.indoor_walk_in = is_number?(property["indoorFeatures"]["walkIn"]) ? ([0,1].include?(property["indoorFeatures"]['walkIn'].to_i) ? property["indoorFeatures"]['walkIn'] : 0 ) : 0
            inclusion.indoor_no_smoking = is_number?(property["indoorFeatures"]["noSmoking"]) ? ([0,1].include?(property["indoorFeatures"]['noSmoking'].to_i) ? property["indoorFeatures"]['noSmoking'] : 0 ) : 0
            inclusion.indoor_double_glazing = is_number?(property["indoorFeatures"]["doubleGlazing"]) ? ([0,1].include?(property["indoorFeatures"]['doubleGlazing'].to_i) ? property["indoorFeatures"]['doubleGlazing'] : 0 ) : 0
            inclusion.indoor_triple_glazing = is_number?(property["indoorFeatures"]["tripleGlazing"]) ? ([0,1].include?(property["indoorFeatures"]['tripleGlazing'].to_i) ? property["indoorFeatures"]['tripleGlazing'] : 0 ) : 0
          end
          if property["flooring"].present?
            inclusion.floor_carpet = is_number?(property["flooring"]["carpet"]) ? ([0,1].include?(property["flooring"]['carpet'].to_i) ? property["flooring"]['carpet'] : 0 ) : 0
            inclusion.floor_wood = is_number?(property["flooring"]["wood"]) ? ([0,1].include?(property["flooring"]['wood'].to_i) ? property["flooring"]['wood'] : 0 ) : 0
            inclusion.floor_floating = is_number?(property["flooring"]["floating"]) ? ([0,1].include?(property["flooring"]['floating'].to_i) ? property["flooring"]['floating'] : 0 ) : 0
            inclusion.floor_ceramic = is_number?(property["flooring"]["ceramic"]) ? ([0,1].include?(property["flooring"]['ceramic'].to_i) ? property["flooring"]['ceramic'] : 0 ) : 0
            inclusion.floor_parquet = is_number?(property["flooring"]["parquet"]) ? ([0,1].include?(property["flooring"]['parquet'].to_i) ? property["flooring"]['parquet'] : 0 ) : 0
            inclusion.floor_cushion = is_number?(property["flooring"]["cushion"]) ? ([0,1].include?(property["flooring"]['cushion'].to_i) ? property["flooring"]['cushion'] : 0 ) : 0
            inclusion.floor_vinyle = is_number?(property["flooring"]["vinyle"]) ? ([0,1].include?(property["flooring"]['vinyle'].to_i) ? property["flooring"]['vinyle'] : 0 ) : 0
            inclusion.floor_lino = is_number?(property["flooring"]["lino"]) ? ([0,1].include?(property["flooring"]['lino'].to_i) ? property["flooring"]['lino'] : 0 ) : 0
            inclusion.floor_marble = is_number?(property["flooring"]["marble"]) ? ([0,1].include?(property["flooring"]['marble'].to_i) ? property["flooring"]['marble'] : 0 ) : 0
          end
          if property["outdoorFeatures"].present?
            inclusion.exterior_land_access = is_number?(property["outdoorFeatures"]["landAccess"]) ? ([0,1].include?(property["outdoorFeatures"]['landAccess'].to_i) ? property["outdoorFeatures"]['landAccess'] : 0 ) : 0
            inclusion.exterior_back_balcony = is_number?(property["outdoorFeatures"]["backBalcony"]) ? ([0,1].include?(property["outdoorFeatures"]['backBalcony'].to_i) ? property["outdoorFeatures"]['backBalcony'] : 0 ) : 0
            inclusion.exterior_front_balcony = is_number?(property["outdoorFeatures"]["frontBalcony"]) ? ([0,1].include?(property["outdoorFeatures"]['frontBalcony'].to_i) ? property["outdoorFeatures"]['frontBalcony'] : 0 ) : 0
            inclusion.exterior_private_patio = is_number?(property["outdoorFeatures"]["privatePatio"]) ? ([0,1].include?(property["outdoorFeatures"]['privatePatio'].to_i) ? property["outdoorFeatures"]['privatePatio'] : 0 ) : 0
            inclusion.exterior_storage = is_number?(property["outdoorFeatures"]["storage"]) ? ([0,1].include?(property["outdoorFeatures"]['storage'].to_i) ? property["outdoorFeatures"]['storage'] : 0 ) : 0
            inclusion.exterior_terrace = is_number?(property["outdoorFeatures"]["terrace"]) ? ([0,1].include?(property["outdoorFeatures"]['terrace'].to_i) ? property["outdoorFeatures"]['terrace'] : 0 ) : 0
            inclusion.exterior_veranda = is_number?(property["outdoorFeatures"]["veranda"]) ? ([0,1].include?(property["outdoorFeatures"]['veranda'].to_i) ? property["outdoorFeatures"]['veranda'] : 0 ) : 0
            inclusion.exterior_garden = is_number?(property["outdoorFeatures"]["garden"]) ? ([0,1].include?(property["outdoorFeatures"]['garden'].to_i) ? property["outdoorFeatures"]['garden'] : 0 ) : 0
          end
          if property["accessibility"].present?
            inclusion.accessibility_elevator = is_number?(property["accessibility"]["elevator"]) ? ([0,1].include?(property["accessibility"]['elevator'].to_i) ? property["accessibility"]['elevator'] : 0 ) : 0
            inclusion.accessibility_balcony = is_number?(property["accessibility"]["balcony"]) ? ([0,1].include?(property["accessibility"]['balcony'].to_i) ? property["accessibility"]['balcony'] : 0 ) : 0
            inclusion.accessibility_grab_bar = is_number?(property["accessibility"]["grabBar"]) ? ([0,1].include?(property["accessibility"]['grabBar'].to_i) ? property["accessibility"]['grabBar'] : 0 ) : 0
            inclusion.accessibility_wider_corridors = is_number?(property["accessibility"]["widerCorridors"]) ? ([0,1].include?(property["accessibility"]['widerCorridors'].to_i) ? property["accessibility"]['widerCorridors'] : 0 ) : 0
            inclusion.accessibility_lowered_switches = is_number?(property["accessibility"]["loweredSwitches"]) ? ([0,1].include?(property["accessibility"]['loweredSwitches'].to_i) ? property["accessibility"]['loweredSwitches'] : 0 ) : 0
            inclusion.accessibility_ramp = is_number?(property["accessibility"]["loweredSwitches"]) ? ([0,1].include?(property["accessibility"]['loweredSwitches'].to_i) ? property["accessibility"]['loweredSwitches'] : 0 ) : 0
          end
          if property["senior"].present?
            inclusion.senior_autonomy = is_number?(property["senior"]["autonomy"]) ? ([0,1].include?(property["senior"]['autonomy'].to_i) ? property["senior"]['autonomy'] : 0 ) : 0
            inclusion.senior_half_autonomy = is_number?(property["senior"]["halfAutonomy"]) ? ([0,1].include?(property["senior"]['halfAutonomy'].to_i) ? property["senior"]['halfAutonomy'] : 0 ) : 0
            inclusion.senior_no_autonomy = is_number?(property["senior"]["noAutonomy"]) ? ([0,1].include?(property["senior"]['noAutonomy'].to_i) ? property["senior"]['noAutonomy'] : 0 ) : 0
            inclusion.senior_meal = is_number?(property["senior"]["meal"]) ? ([0,1].include?(property["senior"]['meal'].to_i) ? property["senior"]['meal'] : 0 ) : 0
            inclusion.senior_nursery = is_number?(property["senior"]["nursery"]) ? ([0,1].include?(property["senior"]['nursery'].to_i) ? property["senior"]['nursery'] : 0 ) : 0
            inclusion.senior_domestic_help = is_number?(property["senior"]["domesticHelp"]) ? ([0,1].include?(property["senior"]['domesticHelp'].to_i) ? property["senior"]['domesticHelp'] : 0 ) : 0
            inclusion.senior_community = is_number?(property["senior"]["community"]) ? ([0,1].include?(property["senior"]['community'].to_i) ? property["senior"]['community'] : 0 ) : 0
            inclusion.senior_activities = is_number?(property["senior"]["activities"]) ? ([0,1].include?(property["senior"]['activities'].to_i) ? property["senior"]['activities'] : 0 ) : 0
            inclusion.senior_validated_residence = is_number?(property["senior"]["validatedResidence"]) ? ([0,1].include?(property["senior"]['validatedResidence'].to_i) ? property["senior"]['validatedResidence'] : 0 ) : 0
            inclusion.senior_housing_cooperative = is_number?(property["senior"]["housingCooperative"]) ? ([0,1].include?(property["senior"]['housingCooperative'].to_i) ? property["senior"]['housingCooperative'] : 0 ) : 0
          end
          if property["pets"].present?
            inclusion.pets_allow = is_number?(property["pets"]["allow"]) ? ([0,1].include?(property["pets"]['allow'].to_i) ? property["pets"]['allow'] : 0 ) : 0
            inclusion.pets_not_allow = is_number?(property["pets"]["notAllow"]) ? ([0,1].include?(property["pets"]['notAllow'].to_i) ? property["pets"]['notAllow'] : 0 ) : 0
            inclusion.pets_cat = is_number?(property["pets"]["cat"]) ? ([0,1].include?(property["pets"]['cat'].to_i) ? property["pets"]['cat'] : 0 ) : 0
            inclusion.pets_dog = is_number?(property["pets"]["dog"]) ? ([0,1].include?(property["pets"]['dog'].to_i) ? property["pets"]['dog'] : 0 ) : 0
            inclusion.pets_little_dog = is_number?(property["pets"]["littleDog"]) ? ([0,1].include?(property["pets"]['littleDog'].to_i) ? property["pets"]['littleDog'] : 0 ) : 0
            inclusion.pets_cage_aquarium = is_number?(property["pets"]["cageAquarium"]) ? ([0,1].include?(property["pets"]['cageAquarium'].to_i) ? property["pets"]['cageAquarium'] : 0 ) : 0
          end
          if property["transport"].present?
            inclusion.transport_bicycle_path = is_number?(property["transport"]["bicyclePath"]) ? ([0,1].include?(property["transport"]['bicyclePath'].to_i) ? property["transport"]['bicyclePath'] : 0 ) : 0
            inclusion.transport_public_transport = is_number?(property["transport"]["publicTransport"]) ? ([0,1].include?(property["transport"]['publicTransport'].to_i) ? property["transport"]['publicTransport'] : 0 ) : 0
            inclusion.transport_public_bike = is_number?(property["transport"]["publicBike"]) ? ([0,1].include?(property["transport"]['publicBike'].to_i) ? property["transport"]['publicBike'] : 0 ) : 0
            inclusion.transport_subway = is_number?(property["transport"]["subway"]) ? ([0,1].include?(property["transport"]['subway'].to_i) ? property["transport"]['subway'] : 0 ) : 0
            inclusion.transport_train_station = is_number?(property["transport"]["trainStation"]) ? ([0,1].include?(property["transport"]['trainStation'].to_i) ? property["transport"]['trainStation'] : 0 ) : 0
          end
          if property["security"].present?
            inclusion.security_guardian = is_number?(property["security"]["guardianJanitor"]) ? ([0,1].include?(property["security"]['guardianJanitor'].to_i) ? property["security"]['guardianJanitor'] : 0 ) : 0
            inclusion.security_alarm = is_number?(property["security"]["alarm"]) ? ([0,1].include?(property["security"]['alarm'].to_i) ? property["security"]['alarm'] : 0 ) : 0
            inclusion.security_intercom = is_number?(property["security"]["interCom"]) ? ([0,1].include?(property["security"]['interCom'].to_i) ? property["security"]['interCom'] : 0 ) : 0
            inclusion.security_camera = is_number?(property["security"]["camera"]) ? ([0,1].include?(property["security"]['camera'].to_i) ? property["security"]['camera'] : 0 ) : 0
            inclusion.security_smoke_dectector = is_number?(property["security"]["smokeDectector"]) ? ([0,1].include?(property["security"]['smokeDectector'].to_i) ? property["security"]['smokeDectector'] : 0 ) : 0
          end
          if property["services"].present?
            inclusion.service_internet = is_number?(property["services"]["internet"]) ? ([0,1].include?(property["services"]['internet'].to_i) ? property["services"]['internet'] : 0 ) : 0
            inclusion.service_tv = is_number?(property["services"]["tv"]) ? ([0,1].include?(property["services"]['tv'].to_i) ? property["services"]['tv'] : 0 ) : 0
            inclusion.service_tv_sat = is_number?(property["services"]["tvSat"]) ? ([0,1].include?(property["services"]['tvSat'].to_i) ? property["services"]['tvSat'] : 0 ) : 0
            inclusion.service_phone = is_number?(property["services"]["phone"]) ? ([0,1].include?(property["services"]['phone'].to_i) ? property["services"]['phone'] : 0 ) : 0
          end
          inclusion.save if inclusion.changed?
          ##============================================================##
          ## End Inclusions
          ##============================================================##

          ##============================================================##
          ## Infos + Worker
          ##============================================================##
          properties_infos << {
            :visibilities => property["visibility"],
            :id => prop.id,
            :uid => prop.uid,
            :api_provider_id => prop.api_provider_id,
            :infos_api => property
          }
        end
      end
      render :json => { :property => properties_infos }, :status => 201
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v1.property",
        :message  => e.message,
        :params   => params
        ).deliver_later
      render :json => { :errors => "#{e.message}\n#{e.backtrace}" }, :status => 201
    end
  end





  private

  ##============================================================##
  ## Si on recoit un nombre inférieur ou égale à 15, c'est
  ## l'ancienne API, c'est à dire que c'est directement l"id
  ## qui est envoyé...
  ##============================================================##
  def check_classification(id)
    begin
      return id.to_i if id.to_i <= 15
      return PropertyClassification.where(:api_id =>id).first.id
    rescue
      return 1
    end
  end


  def is_number?(string)
    true if Float(string) rescue false
  end



  def url_is_a_image?(url)
    begin
      FastImage.size(url).kind_of?(Array)
    rescue Exception => e
      JulesLogger.info e.message
      return false
    end
  end



end

