class Api::V2::PrintsController < Api::V2::ApplicationController


  ##============================================================##
  ## Dictionary for template with 1 property
  ##============================================================##
  def dictionary
    begin
      @property = Property.find(params[:id])
      raise "Not Found property with uid #{params[:uid]}" if @property.blank?
      @dictionary = DictionaryPopulater.new(
        :dictionary   => params["dictionary"],
        :property_id  => params[:id],
        :locale       => I18n.locale
        ).populate_dictionary
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.print#dictionary",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors => e.message }, :status=> 402
    end
  end

end