class Api::V2::ContactsController < Api::V2::ApplicationController

  ##============================================================##
  ## POST|PUT /api/v2/contacts
  ##============================================================##
  def create_and_update
    begin
      raise "contact.account_uid  is missing"            if params[:account_uid].blank?
      raise "contact.email is missing"                   if params[:email].blank?
      raise "contact.email not valid"                    if !EmailValidator.valid?(params[:email])
      raise "contact.type #{params[:type]} is not allow" if check_1_to_4(params[:type]).blank?
      property_account = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id, :uid => params[:account_uid]).first
      raise "account #{params[:account_uid]} doesn't exist for your Api" if property_account.nil?
      raise "you have already associated this email and this type with another Account" if PropertyContact.where(:email => params[:email], :property_contact_type => params[:type]).where.not(:uid => params[:uid]).first.present?

      @property_contact = PropertyContact.where(
        :uid              => params[:uid],
        :api_provider_id  => @api_app.api_provider_id,
        :user_id          => property_account.user.id
        ).first_or_create

      @property_contact.property_contact_type_id = params[:type]

      ##============================================================##
      ## On vérifie en plus que la key est présente, qu'il y ait une
      ## valeur..Car certains contacts ont des infos mises manuellement
      ##============================================================##
      @property_contact.civility_id = params[:civility].to_i if params.has_key?(:civility)   and params[:civility].check_1_to_3
      @property_contact.email       = params[:email]        if params.has_key?(:email)      and params[:email].present?
      @property_contact.first_name  = params[:first_name]   if params.has_key?(:first_name) and params[:first_name].present?
      @property_contact.last_name   = params[:last_name]    if params.has_key?(:last_name)  and params[:last_name].present?
      @property_contact.phone       = params[:phone]        if params.has_key?(:phone)      and params[:phone].present?
      @property_contact.mobile      = params[:mobile]       if params.has_key?(:mobile)     and params[:mobile].present?
      @property_contact.iban        = params[:iban]         if params.has_key?(:iban)       and params[:iban].present?

      if params[:address].present?
        address = params[:address]
        @property_contact.address   = address[:street_name] if address.has_key?(:street_name)
        @property_contact.zipcode   = address[:zipcode]     if address.has_key?(:zipcode)
        @property_contact.city      = address[:city]        if address.has_key?(:city)
        @property_contact.country   = address[:country]     if address.has_key?(:country)
      end

      if params[:picture_url].present? and @property_contact.original_picture_url != params[:picture_url] && url_is_a_image?(params[:picture_url])
        @property_contact.picture               = open(params[:picture_url])
        @property_contact.original_picture_url  = params[:picture_url]
      end
      @property_contact.save if @property_contact.changed?
      render 'retrieve'
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.contacts#create_and_update",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors => e.message }, :status=> 402
    end
  end


  ##============================================================##
  ## GET /api/v2/contacts/:uid
  ##============================================================##
  def retrieve
    begin
      @property_contact = PropertyContact.where(:uid =>params[:uid], :api_provider_id => @api_app.api_provider_id).first
      return render :json =>{:errors=>"Not Found contact with uid #{params[:uid]}" }, :status=>400 if @property_contact.blank?
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.contacts#retrieve",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=> 400
    end
  end



  ##============================================================##
  ## DELETE /api/v2/contacts/:uid
  ##============================================================##
  def destroy
    begin
      @property_contact = PropertyContact.where(:uid => params[:uid], :api_provider_id => @api_app.api_provider_id).first
      raise "contact with uid #{params[:uid]} doesn't exist" if @property_contact.blank?
      @property_contact.destroy
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.contacts#create_and_update",
        :message  => e.message
        ).deliver_later
      return render :json =>{:errors => e.message }, :status=> 402
    end
  end


  ##============================================================##
  ## POST /api/v2/contacts/property
  ##============================================================##
  def contact_for_property_add
    begin
      raise "property_uid not set"  if params[:property_uid].blank?
      raise "contact_uid not set"   if params[:contact_uid].blank?

      @property = Property.where(:uid => params[:property_uid], :api_provider_id => @api_app.api_provider_id).first
      raise "property with #uid #{params[:property_uid]} doesn't exist" if @property.blank?

      @property_contact = PropertyContact.where(:uid => params[:contact_uid], :api_provider_id => @api_app.api_provider_id).first
      raise "contact with uid #{params[:contact_uid]} doesn't exist" if @property_contact.blank?

      @property.property_contacts << @property_contact if @property_contact.present? and !@property.property_contacts.pluck(:id).include?(@property_contact.id)

    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.contacts#create_and_update",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors => e.message }, :status=> 400
    end
  end



  ##============================================================##
  ## DELETE /api/v2/contacts/property
  ##============================================================##
  def contact_for_property_destroy
    begin
      raise "property_uid not set"  if params[:property_uid].blank?
      raise "contact_uid not set"   if params[:contact_uid].blank?

      @property = Property.where(:uid => params[:property_uid], :api_provider_id => @api_app.api_provider_id).first
      raise "property with uid #{params[:property_uid]} doesn't exist" if @property.blank?

      @property_contact = PropertyContact.where(:uid => params[:contact_uid], :api_provider_id => @api_app.api_provider_id).first
      raise "contact with uid #{params[:contact_uid]} doesn't exist" if @property_contact.blank?

      @property.property_contacts.delete(@property_contact)

    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.contacts#create_and_update",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors => e.message }, :status=> 402
    end
  end



end
