class Api::V2::BuildingsController < Api::V2::ApplicationController

  ##============================================================##
  ## POST | PUT /api/v2/buildings
  ##============================================================##
  def create_and_update
    begin
      ##============================================================##
      ## Étape 1 : On vérifie si le propriétaire de l'immeuble existe et
      ## qu'il dépend bien de l'api qui appelle. Puis si l'immeuble
      ## existe on vérifie que le user en est bien le propriétaire
      # ##============================================================##
      raise "uid  is missing"       if params[:uid].blank?
      raise "acount_uid is missing" if params[:account_uid].blank?

      ##============================================================##
      ## Si création, rend obligatoire les champs ci-dessous, sinon facultatif
      ##============================================================##
      if request.post?
        raise "name is missing"    if params[:name].blank?
      end

      ##============================================================##
      ## On recherche le user avec son uid
      ##============================================================##
      portal = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id, :uid =>params[:account_uid]).first
      raise "Any user match with uid : #{params[:account_uid]} with your api credentials" if portal.blank?
      user  = portal.user


      ##============================================================##
      ##
      ##============================================================##
      if params[:id].present?
        @building = Building.find(params[:id])
        raise "Building not found" if @building.blank?
        raise "Building ##{params[:id]} is not associeted with user #{user.email}" if @building.user_id != user.id

        if @building.is_sync_with_api == @api_app.api_provider_id
          @building.uid             = params[:uid]  if params[:uid].present?
          @building.api_provider_id = @api_app.api_provider_id
        end
      else
        @building = Building.where(:uid => params[:uid],:api_provider_id => @api_app.api_provider_id,:user_id => user.id).first_or_create
        @building.is_sync_with_api = @api_app.api_provider_id if @building.is_sync_with_api.blank?
      end


      raise "#{user.email} chose to synchronize this building from one another api (#{@building.is_sync_with_api})"   if @building.is_sync_with_api != @api_app.api_provider_id
      raise "building not updatable"                                                                                  if(@building.is_sync_with_api.present? && @building.is_sync_with_api == 0)


      ##============================================================##
      ## Étape 2 : Informations de base
      ## Penser à save @property avant de créer les pictures
      ##============================================================##
      @building.secure_id            = SecureRandom.urlsafe_base64 if @building.secure_id.blank?
      @building.draft                = 0
      @building.api_provider_id      = @api_app.api_provider_id
      @building.name                 = params[:name]   if params.has_key?(:name)
      @building.number_of_units      = params[:units]  if params.has_key?(:units)
      @building.number_of_levels     = params[:levels] if params.has_key?(:levels)
      if params[:address].present?
        @building.address                              = params[:address][:address_formatted]          if params[:address].has_key?(:address_formatted)
        @building.street_number                        = params[:address][:street_number]              if params[:address].has_key?(:street_number)
        @building.street_name                          = params[:address][:street_name]                if params[:address].has_key?(:street_name)
        @building.zipcode                              = params[:address][:zipcode]                    if params[:address].has_key?(:zipcode)
        @building.locality                             = params[:address][:locality]                   if params[:address].has_key?(:locality)
        @building.sublocality                          = params[:address][:sublocality]                if params[:address].has_key?(:sublocality)
        @building.administrative_area_level_1          = params[:address][:administrative_area_level1] if params[:address].has_key?(:administrative_area_level1)
        @building.administrative_area_level_2          = params[:address][:administrative_area_level2] if params[:address].has_key?(:administrative_area_level2)
        @building.country_short                        = params[:address][:country]                    if params[:address].has_key?(:country) && ISO3166::Country.find_country_by_alpha2(params[:address][:country]).present?
        @building.latitude                             = params[:address][:latitude]                   if params[:address].has_key?(:latitude)
        @building.longitude                            = params[:address][:longitude]                  if params[:address].has_key?(:longitude)
      end
      if params[:description_short].present?
        params[:description_short].each do |description_short|
          @building.description_short_fr = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "fr"
          @building.description_short_en = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "en"
          @building.description_short_de = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "de"
          @building.description_short_nl = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "nl"
          @building.description_short_it = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "it"
          @building.description_short_es = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "es"
          @building.description_short_pt = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "pt"
        end
      end
      if params[:description].present?
        params[:description].each do |description|
          @building.description_fr = description[1] if description[0] == "fr"
          @building.description_en = description[1] if description[0] == "en"
          @building.description_de = description[1] if description[0] == "de"
          @building.description_nl = description[1] if description[0] == "nl"
          @building.description_it = description[1] if description[0] == "it"
          @building.description_es = description[1] if description[0] == "es"
          @building.description_pt = description[1] if description[0] == "pt"
        end
      end

      ##============================================================##
      ## Il ne faut pas mettre if @building.changed? car si on
      ## change que les traductions, cela ne sauvegardera pas
      ##============================================================##
      @building.save(:validate => false)
      if @building.errors.any?
        prop_error = ""
        @property.errors.each do |attribute,err|
          prop_error += "#{attribute} #{err},"
        end
        raise prop_error
      end

      ##============================================================##
      ## @building.id est définie dans tous les cas
      ## car @building.save
      ##============================================================##
      if params[:pictures].present? && params[:pictures].size >= 1
        @picture_in_progress = 1
        AssetsWorker.perform_async(:model => 'building',:id => @building.id,:pictures => params[:pictures])
      end
      render "retrieve"
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.buildings#create_and_update",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message}, :status=> 400
    end
  end


  ##============================================================##
  ## GET /api/v2/buildings/:uid
  ##============================================================##
  def retrieve
    begin
      @building = Building.where(:uid =>params[:uid], :api_provider_id => @api_app.api_provider_id).first
      return render :json =>{:errors=>"Not Found building with uid #{params[:uid]}" }, :status=>400 if @building.blank?
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.buildings#retrieve",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=> 400
    end
  end


  ##============================================================##
  ## DELETE /api/v2/buildings/:uid
  ##============================================================##
  def destroy
    begin
      @building = Building.where(:uid =>params[:uid], :api_provider_id => @api_app.api_provider_id).first
      BuildingPropertyDeleteWorker.perform_async(:model => "building", :id => @building.id)
      return render :json =>{:errors=>"Not Found building with uid #{params[:uid]}" }, :status => 400 if @building.blank?
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.buildings#destroy",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status => 400
    end
  end



end
