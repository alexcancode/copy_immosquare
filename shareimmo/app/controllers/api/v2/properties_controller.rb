# encoding: utf-8

class Api::V2::PropertiesController < Api::V2::ApplicationController

  ##============================================================##
  ## POST | PUT /api/v2/properties
  ##============================================================##
  def create_and_update
    begin
      ##============================================================##
      ## Étape 1 : On vérifie si le propriétaire de l'annonce existe et
      ## qu'il dépend bien de l'api qui appelle. Puis si l'annonce
      ## existe on vérifie que le user en est bien le propriétaire
      ##============================================================##
      raise "uid  is missing"         if params[:uid].blank?
      raise "account_uid is missing"  if params[:account_uid].blank?

      ##============================================================##
      ## Si création, rend obligatoire les champs ci-dessous, sinon facultatif
      ##============================================================##
      if request.post?
        raise "classification_id is missing"  if params[:classification_id].blank?
        raise "status_id is missing"          if params[:status_id].blank?
        raise "listing_id is missing"         if params[:listing_id].blank?
      end

      ##============================================================##
      ## On recherche le user avec son uid
      ##============================================================##
      @portal = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id, :uid =>params[:account_uid]).first
      raise "Any user match with uid : #{params[:account_uid]} with your api credentials" if @portal.blank?
      user = @portal.user


      if params[:id].present?
        @property = Property.find(params[:id])
        raise "property not found" if @property.blank?

        if @property.is_sync_with_api == @api_app.api_provider_id
          @property.uid             = params[:uid]  if params[:uid].present?
          @property.api_provider_id = @api_app.api_provider_id
        end
      else
        @property = Property.where(:uid => params[:uid],:api_provider_id => @api_app.api_provider_id,:user_id => user.id).first_or_create
        @property.is_sync_with_api = @api_app.api_provider_id if (@property.secure_id.blank?)
      end


      if @property.is_sync_with_api != @api_app.api_provider_id
        return render :json =>{:errors=>  "#{user.email} chose to synchronize this ad from one another api (#{@property.is_sync_with_api})"}, :status=> 400
      end

      raise "property not updatable" if (@property.is_sync_with_api.present? and @property.is_sync_with_api == 0)


      ##============================================================##
      ## Étape 2 : Informations de base
      ## Penser à save @property avant de créer les pictures, et
      ## inclusions pour récuperérer l'id en création
      ##============================================================##
      @property.secure_id                   = SecureRandom.urlsafe_base64                       if @property.secure_id.blank?
      @property.user_id                     = user.id
      @property.draft                       = 0
      @property.master                      = check_0_or_1(params[:master])                     if params.has_key?(:master)
      @property.status                      = check_0_or_1(params[:online])                     if params.has_key?(:online)
      @property.cadastre                    = params[:cadastre]                                 if params.has_key?(:cadastre)
      @property.is_luxurious                = check_0_or_1(params[:is_luxurious])               if params.has_key?(:is_luxurious)
      @property.unit                        = params[:unit]                                     if params.has_key?(:unit)
      @property.level                       = params[:level]                                    if params.has_key?(:level)
      @property.property_classification_id  = check_classification(params[:classification_id])  if params.has_key?(:classification_id)
      @property.property_status_id          = check_1_to_2(params[:status_id])                  if params.has_key?(:status_id)
      @property.property_listing_id         = check_1_to_2(params[:listing_id])                 if params.has_key?(:listing_id)
      @property.property_flag_id            = check_1_to_5(params[:flag_id])                    if params.has_key?(:flag_id)
      @property.year_of_built               = params[:year_of_built]                            if params.has_key?(:year_of_built)
      @property.availability_date           = params[:availability_date]                        if params.has_key?(:availability_date)


      ##============================================================##
      ## Étape 2.1 Gestion du batiment
      ##============================================================##
      if params[:building_uid].present?
        @building = Building.where(:uid => params[:building_uid],:api_provider_id => @api_app.api_provider_id).first
        @property.building_id = @building.id if @building.present?
      end


      ##============================================================##
      ## Étape 2.3 Suite de @property
      ##============================================================##
      if params[:address].present?
        @property.address_visibility           = check_0_or_1(params[:address][:address_visibility])  if params[:address].has_key?(:address_visibility)
        @property.address                      = params[:address][:address_formatted]                 if params[:address].has_key?(:address_formatted)
        @property.street_number                = params[:address][:street_number]                     if params[:address].has_key?(:street_number)
        @property.street_name                  = params[:address][:street_name]                       if params[:address].has_key?(:street_name)
        @property.zipcode                      = params[:address][:zipcode]                           if params[:address].has_key?(:zipcode)
        @property.locality                     = params[:address][:locality]                          if params[:address].has_key?(:locality)
        @property.sublocality                  = params[:address][:sublocality]                       if params[:address].has_key?(:sublocality)
        @property.administrative_area_level_1  = params[:address][:administrative_area_level1]        if params[:address].has_key?(:administrative_area_level1)
        @property.administrative_area_level_2  = params[:address][:administrative_area_level2]        if params[:address].has_key?(:administrative_area_level2)
        @property.country_short                = params[:address][:country]                           if params[:address].has_key?(:country)
        @property.latitude                     = params[:address][:latitude]                          if params[:address].has_key?(:latitude)
        @property.longitude                    = params[:address][:longitude]                         if params[:address].has_key?(:longitude)
      end
      if params[:area].present?
        @property.area_living   = params[:area][:living]      if params[:area].has_key?(:living)
        @property.area_balcony  = params[:area][:balcony]     if params[:area].has_key?(:balcony)
        @property.area_land     = params[:area][:land]        if params[:area].has_key?(:land)
        @property.area_unit_id  = check_1_to_2(params[:area][:unit_id]) if params[:area].has_key?(:unit_id)
        @property.area_unit_id  = 1 if @property.area_unit_id.blank?
      end
      if params[:rooms].present?
        @property.rooms       = check_1_to_20(params[:rooms][:rooms])       if params[:rooms].has_key?(:rooms)
        @property.bathrooms   = check_1_to_20(params[:rooms][:bathrooms])   if params[:rooms].has_key?(:bathrooms)
        @property.bedrooms    = check_1_to_20(params[:rooms][:bedrooms])    if params[:rooms].has_key?(:bedrooms)
        @property.showerrooms = check_1_to_20(params[:rooms][:showerrooms]) if params[:rooms].has_key?(:showerrooms)
        @property.toilets     = check_1_to_20(params[:rooms][:toilets]) if params[:rooms].has_key?(:toilets)
      end
      if params[:title].present?
        params[:title].each do |title|
          @property.title_fr = title[1] if title[0] == "fr"
          @property.title_en = title[1] if title[0] == "en"
          @property.title_de = title[1] if title[0] == "de"
          @property.title_nl = title[1] if title[0] == "nl"
          @property.title_it = title[1] if title[0] == "it"
          @property.title_es = title[1] if title[0] == "es"
          @property.title_pt = title[1] if title[0] == "pt"
        end
      end
      if params[:description].present?
        params[:description].each do |description|
          @property.description_fr = description[1] if description[0] == "fr"
          @property.description_en = description[1] if description[0] == "en"
          @property.description_de = description[1] if description[0] == "de"
          @property.description_nl = description[1] if description[0] == "nl"
          @property.description_nl = description[1] if description[0] == "nl"
          @property.description_it = description[1] if description[0] == "it"
          @property.description_es = description[1] if description[0] == "es"
          @property.description_pt = description[1] if description[0] == "pt"
        end
      end
      if params[:description_short].present?
        params[:description_short].each do |description_short|
          @property.description_short_fr = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "fr"
          @property.description_short_en = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "en"
          @property.description_short_de = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "de"
          @property.description_short_nl = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "nl"
          @property.description_short_nl = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "nl"
          @property.description_short_it = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "it"
          @property.description_short_es = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "es"
          @property.description_short_pt = ActionView::Base.full_sanitizer.sanitize(description_short[1]) if description_short[0] == "pt"
        end
      end
      if params[:url].present?
        params[:url].each do |url|
          @property.property_url_fr = url[1] if url[0] == "fr"
          @property.property_url_en = url[1] if url[0] == "en"
          @property.property_url_de = url[1] if url[0] == "de"
          @property.property_url_nl = url[1] if url[0] == "nl"
          @property.property_url_it = url[1] if url[0] == "it"
          @property.property_url_es = url[1] if url[0] == "es"
          @property.property_url_pt = url[1] if url[0] == "pt"
        end
      end
      if params[:financial].present?
        @property.price           = params[:financial]["price"]                         if params[:financial].has_key?(:price)
        @property.price_with_tax  = check_0_or_1(params[:financial][:price_with_tax])   if params[:financial].has_key?(:price_with_tax)
        @property.rent            = params[:financial][:rent]                           if params[:financial].has_key?(:rent)
        @property.deposit         = params[:financial][:deposit]                        if params[:financial].has_key?(:deposit)
        @property.fees            = params[:financial][:fees]                           if params[:financial].has_key?(:fees)
        @property.rent_frequency  = params[:financial][:rent_frequency]                 if params[:financial].has_key?(:rent_frequency)
        @property.tax_1           = params[:financial][:tax1_annual]                    if params[:financial].has_key?(:tax1_annual)
        @property.tax_2           = params[:financial][:tax2_annual]                    if params[:financial].has_key?(:tax2_annual)
        @property.income          = params[:financial][:income_annual]                  if params[:financial].has_key?(:income_annual)
        @property.currency        = params[:financial][:currency].upcase                if (params[:financial].has_key?(:currency) && Money::Currency.new(params[:financial][:currency]).present?)
      end


      @property.save
      if @property.errors.any?
        prop_error = ""
        @property.errors.each do |attribute,err|
          prop_error += "#{attribute} #{err},"
        end
        raise prop_error
      end


      ##============================================================##
      ## @property.id est définie dans tous les cas
      ## car @propery.save
      ##============================================================##

      ##============================================================##
      ## Étape 4 : Inclusions (après @property.save)
      ##============================================================##
      @inclusion = PropertyInclusion.where(:property_id=>@property.id).first_or_create
      if params[:other].present?
        if params[:other][:orientation].present?
          @inclusion.orientation_north   = check_0_or_1(params[:other][:orientation][:north])   if params[:other][:orientation].has_key?(:north)
          @inclusion.orientation_south   = check_0_or_1(params[:other][:orientation][:south])   if params[:other][:orientation].has_key?(:south)
          @inclusion.orientation_east    = check_0_or_1(params[:other][:orientation][:east])    if params[:other][:orientation].has_key?(:east)
          @inclusion.orientation_west    = check_0_or_1(params[:other][:orientation][:west])    if params[:other][:orientation].has_key?(:west)
        end
        if params[:other][:inclusion].present?
          @inclusion.inclusion_air_conditioning    = check_0_or_1(params[:other][:inclusion][:air_conditioning])  if params[:other][:inclusion].has_key?(:air_conditioning)
          @inclusion.inclusion_hot_water           = check_0_or_1(params[:other][:inclusion][:hot_water])         if params[:other][:inclusion].has_key?(:hot_water)
          @inclusion.inclusion_heated              = check_0_or_1(params[:other][:inclusion][:heated])            if params[:other][:inclusion].has_key?(:heated)
          @inclusion.inclusion_electricity         = check_0_or_1(params[:other][:inclusion][:electricity])       if params[:other][:inclusion].has_key?(:electricity)
          @inclusion.inclusion_furnished           = check_0_or_1(params[:other][:inclusion][:furnished])         if params[:other][:inclusion].has_key?(:furnished)
          @inclusion.inclusion_fridge              = check_0_or_1(params[:other][:inclusion][:fridge])            if params[:other][:inclusion].has_key?(:fridge)
          @inclusion.inclusion_cooker              = check_0_or_1(params[:other][:inclusion][:cooker])            if params[:other][:inclusion].has_key?(:cooker)
          @inclusion.inclusion_dishwasher          = check_0_or_1(params[:other][:inclusion][:dishwasher])        if params[:other][:inclusion].has_key?(:dishwasher)
          @inclusion.inclusion_dryer               = check_0_or_1(params[:other][:inclusion][:dryer])             if params[:other][:inclusion].has_key?(:dryer)
          @inclusion.inclusion_washer              = check_0_or_1(params[:other][:inclusion][:washer])            if params[:other][:inclusion].has_key?(:washer)
          @inclusion.inclusion_microwave           = check_0_or_1(params[:other][:inclusion][:microwave])         if params[:other][:inclusion].has_key?(:microwave)
        end
        if params[:other][:detail].present?
          @inclusion.detail_elevator                = check_0_or_1(params[:other][:detail]["elevator"])         if params[:other][:detail].has_key?(:elevator)
          @inclusion.detail_laundry                 = check_0_or_1(params[:other][:detail]["laundry"])          if params[:other][:detail].has_key?(:laundry)
          @inclusion.detail_garbage_chute           = check_0_or_1(params[:other][:detail][:garbage_chute])     if params[:other][:detail].has_key?(:garbage_chute)
          @inclusion.detail_common_space            = check_0_or_1(params[:other][:detail][:common_space])      if params[:other][:detail].has_key?(:common_space)
          @inclusion.detail_janitor                 = check_0_or_1(params[:other][:detail]["janitor"])          if params[:other][:detail].has_key?(:janitor)
          @inclusion.detail_gym                     = check_0_or_1(params[:other][:detail]["gym"])              if params[:other][:detail].has_key?(:gym)
          @inclusion.detail_golf                    = check_0_or_1(params[:other][:detail]["golf"])             if params[:other][:detail].has_key?(:golf)
          @inclusion.detail_tennis                  = check_0_or_1(params[:other][:detail]["tennis"])           if params[:other][:detail].has_key?(:tennis)
          @inclusion.detail_sauna                   = check_0_or_1(params[:other][:detail]["sauna"])            if params[:other][:detail].has_key?(:sauna)
          @inclusion.detail_spa                     = check_0_or_1(params[:other][:detail]["spa"])              if params[:other][:detail].has_key?(:spa)
          @inclusion.detail_inside_pool             = check_0_or_1(params[:other][:detail][:inside_pool])       if params[:other][:detail].has_key?(:inside_pool)
          @inclusion.detail_outside_pool            = check_0_or_1(params[:other][:detail][:outside_pool])      if params[:other][:detail].has_key?(:outside_pool)
          @inclusion.detail_inside_parking          = check_0_or_1(params[:other][:detail][:inside_parking])    if params[:other][:detail].has_key?(:inside_parking)
          @inclusion.detail_outside_parking         = check_0_or_1(params[:other][:detail][:outside_parking])   if params[:other][:detail].has_key?(:outside_parking)
          @inclusion.detail_parking_on_street       = check_0_or_1(params[:other][:detail][:parking_on_street]) if params[:other][:detail].has_key?(:parking_on_street)
          @inclusion.detail_garagebox               = check_0_or_1(params[:other][:detail]["garagebox"])        if params[:other][:detail].has_key?(:garagebox)
        end
        if params[:other][:half_furnished].present?
          @inclusion.half_furnsihed_living_room     = check_0_or_1(params[:other][:half_furnished][:living_room]) if params[:other][:half_furnished].has_key?(:living_room)
          @inclusion.half_furnsihed_rooms           = check_0_or_1(params[:other][:half_furnished][:bedrooms])    if params[:other][:half_furnished].has_key?(:bedrooms)
          @inclusion.half_furnsihed_kitchen         = check_0_or_1(params[:other][:half_furnished]["kitchen"])    if params[:other][:half_furnished].has_key?(:kitchen)
          @inclusion.half_furnsihed_other           = check_0_or_1(params[:other][:half_furnished]["other"])      if params[:other][:half_furnished].has_key?(:other)
        end
        if params[:other][:indoor].present?
          @inclusion.indoor_attic                   = check_0_or_1(params[:other][:indoor]["attic"])                if params[:other][:indoor].has_key?(:attic)
          @inclusion.indoor_attic_convertible       = check_0_or_1(params[:other][:indoor]["attic_convertible"])    if params[:other][:indoor].has_key?(:attic_convertible)
          @inclusion.indoor_attic_converted         = check_0_or_1(params[:other][:indoor]["attic_converted"])      if params[:other][:indoor].has_key?(:attic_converted)
          @inclusion.indoor_cellar                  = check_0_or_1(params[:other][:indoor]["cellar"])               if params[:other][:indoor].has_key?(:cellar)
          @inclusion.indoor_central_vacuum          = check_0_or_1(params[:other][:indoor][:central_vacuum])        if params[:other][:indoor].has_key?(:central_vacuum)
          @inclusion.indoor_entries_washer_dryer    = check_0_or_1(params[:other][:indoor][:entries_washer_dryer])  if params[:other][:indoor].has_key?(:entries_washer_dryer)
          @inclusion.indoor_entries_dishwasher      = check_0_or_1(params[:other][:indoor][:entries_dishwasher])    if params[:other][:indoor].has_key?(:entries_dishwasher)
          @inclusion.indoor_fireplace               = check_0_or_1(params[:other][:indoor]["fireplace"])            if params[:other][:indoor].has_key?(:fireplace)
          @inclusion.indoor_storage                 = check_0_or_1(params[:other][:indoor]["storage"])              if params[:other][:indoor].has_key?(:storage)
          @inclusion.indoor_walk_in                 = check_0_or_1(params[:other][:indoor][:walk_in])               if params[:other][:indoor].has_key?(:walk_in)
          @inclusion.indoor_no_smoking              = check_0_or_1(params[:other][:indoor][:no_smoking])            if params[:other][:indoor].has_key?(:no_smoking)
          @inclusion.indoor_double_glazing          = check_0_or_1(params[:other][:indoor][:double_glazing])        if params[:other][:indoor].has_key?(:double_glazing)
          @inclusion.indoor_triple_glazing          = check_0_or_1(params[:other][:indoor][:triple_glazing])        if params[:other][:indoor].has_key?(:triple_glazing)
        end
        if params[:other][:floor].present?
          @inclusion.floor_carpet                   = check_0_or_1(params[:other][:floor]["carpet"])    if params[:other][:floor].has_key?(:carpet)
          @inclusion.floor_wood                     = check_0_or_1(params[:other][:floor]["wood"])      if params[:other][:floor].has_key?(:wood)
          @inclusion.floor_floating                 = check_0_or_1(params[:other][:floor]["floating"])  if params[:other][:floor].has_key?(:floating)
          @inclusion.floor_ceramic                  = check_0_or_1(params[:other][:floor]["ceramic"])   if params[:other][:floor].has_key?(:ceramic)
          @inclusion.floor_parquet                  = check_0_or_1(params[:other][:floor]["parquet"])   if params[:other][:floor].has_key?(:parquet)
          @inclusion.floor_cushion                  = check_0_or_1(params[:other][:floor]["cushion"])   if params[:other][:floor].has_key?(:cushion)
          @inclusion.floor_vinyle                   = check_0_or_1(params[:other][:floor]["vinyle"])    if params[:other][:floor].has_key?(:vinyle)
          @inclusion.floor_lino                     = check_0_or_1(params[:other][:floor]["lino"])      if params[:other][:floor].has_key?(:lino)
          @inclusion.floor_marble                   = check_0_or_1(params[:other][:floor]["marble"])    if params[:other][:floor].has_key?(:marble)
        end
        if params[:other][:exterior].present?
          @inclusion.exterior_land_access           = check_0_or_1(params[:other][:exterior][:land_access])   if params[:other][:exterior].has_key?(:land_access)
          @inclusion.exterior_back_balcony          = check_0_or_1(params[:other][:exterior][:back_balcony])  if params[:other][:exterior].has_key?(:back_balcony)
          @inclusion.exterior_front_balcony         = check_0_or_1(params[:other][:exterior][:front_balcony]) if params[:other][:exterior].has_key?(:front_balcony)
          @inclusion.exterior_private_patio         = check_0_or_1(params[:other][:exterior][:private_patio]) if params[:other][:exterior].has_key?(:private_patio)
          @inclusion.exterior_storage               = check_0_or_1(params[:other][:exterior]["storage"])      if params[:other][:exterior].has_key?(:storage)
          @inclusion.exterior_terrace               = check_0_or_1(params[:other][:exterior]["terrace"])      if params[:other][:exterior].has_key?(:terrace)
          @inclusion.exterior_veranda               = check_0_or_1(params[:other][:exterior]["veranda"])      if params[:other][:exterior].has_key?(:veranda)
          @inclusion.exterior_garden                = check_0_or_1(params[:other][:exterior]["garden"])       if params[:other][:exterior].has_key?(:garden)
          @inclusion.exterior_sea_view              = check_0_or_1(params[:other][:exterior][:sea_view])      if params[:other][:exterior].has_key?(:sea_view)
          @inclusion.exterior_mountain_view         = check_0_or_1(params[:other][:exterior][:mountain_view]) if params[:other][:exterior].has_key?(:mountain_view)
        end
        if params[:other][:accessibility].present?
          @inclusion.accessibility_elevator         = check_0_or_1(params[:other][:accessibility]["elevator"])        if params[:other][:accessibility].has_key?(:elevator)
          @inclusion.accessibility_balcony          = check_0_or_1(params[:other][:accessibility]["balcony"])         if params[:other][:accessibility].has_key?(:balcony)
          @inclusion.accessibility_grab_bar         = check_0_or_1(params[:other][:accessibility][:grab_bar])         if params[:other][:accessibility].has_key?(:grab_bar)
          @inclusion.accessibility_wider_corridors  = check_0_or_1(params[:other][:accessibility][:wider_corridors])  if params[:other][:accessibility].has_key?(:wider_corridors)
          @inclusion.accessibility_lowered_switches = check_0_or_1(params[:other][:accessibility][:lowered_witches])  if params[:other][:accessibility].has_key?(:lowered_witches)
          @inclusion.accessibility_ramp             = check_0_or_1(params[:other][:accessibility][:ramp])             if params[:other][:accessibility].has_key?(:ramp)
        end
        if params[:other][:senior].present?
          @inclusion.senior_autonomy                = check_0_or_1(params[:other][:senior]["autonomy"])           if params[:other][:senior].has_key?(:autonomy)
          @inclusion.senior_half_autonomy           = check_0_or_1(params[:other][:senior][:half_autonomy])       if params[:other][:senior].has_key?(:half_autonomy)
          @inclusion.senior_no_autonomy             = check_0_or_1(params[:other][:senior][:no_autonomy])         if params[:other][:senior].has_key?(:no_autonomy)
          @inclusion.senior_meal                    = check_0_or_1(params[:other][:senior]["meal"])               if params[:other][:senior].has_key?(:meal)
          @inclusion.senior_nursery                 = check_0_or_1(params[:other][:senior]["nursery"])            if params[:other][:senior].has_key?(:nursery)
          @inclusion.senior_domestic_help           = check_0_or_1(params[:other][:senior][:domestic_help])       if params[:other][:senior].has_key?(:domestic_help)
          @inclusion.senior_community               = check_0_or_1(params[:other][:senior]["community"])          if params[:other][:senior].has_key?(:community)
          @inclusion.senior_activities              = check_0_or_1(params[:other][:senior]["activities"])         if params[:other][:senior].has_key?(:activities)
          @inclusion.senior_validated_residence     = check_0_or_1(params[:other][:senior][:validated_residence]) if params[:other][:senior].has_key?(:validated_residence)
          @inclusion.senior_housing_cooperative     = check_0_or_1(params[:other][:senior][:housing_cooperative]) if params[:other][:senior].has_key?(:housing_cooperative)
        end
        if params[:other][:pet].present?
          @inclusion.pets_allow                     = check_0_or_1(params[:other][:pet]["allow"])         if params[:other][:pet].has_key?(:allow)
          @inclusion.pets_not_allow                 = check_0_or_1(params[:other][:pet][:not_allow])      if params[:other][:pet].has_key?(:not_allow)
          @inclusion.pets_cat                       = check_0_or_1(params[:other][:pet]["cat"])           if params[:other][:pet].has_key?(:cat)
          @inclusion.pets_dog                       = check_0_or_1(params[:other][:pet]["dog"])           if params[:other][:pet].has_key?(:dog)
          @inclusion.pets_little_dog                = check_0_or_1(params[:other][:pet][:little_dog])     if params[:other][:pet].has_key?(:little_dog)
          @inclusion.pets_cage_aquarium             = check_0_or_1(params[:other][:pet][:cage_aquarium])  if params[:other][:pet].has_key?(:cage_aquarium)
        end
        if params[:other][:service].present?
          @inclusion.service_internet               = check_0_or_1(params[:other][:service]["internet"])  if params[:other][:service].has_key?(:internet)
          @inclusion.service_tv                     = check_0_or_1(params[:other][:service]["tv"])        if params[:other][:service].has_key?(:tv)
          @inclusion.service_tv_sat                 = check_0_or_1(params[:other][:service][:tv_sat])     if params[:other][:service].has_key?(:tv_sat)
          @inclusion.service_phone                  = check_0_or_1(params[:other][:service]["phone"])     if params[:other][:service].has_key?(:phone)
        end
        if params[:other][:composition].present?
          @inclusion.composition_bar                = check_0_or_1(params[:other][:composition]["bar"])             if params[:other][:composition].has_key?(:bar)
          @inclusion.composition_living             = check_0_or_1(params[:other][:composition]["living"])          if params[:other][:composition].has_key?(:living)
          @inclusion.composition_dining             = check_0_or_1(params[:other][:composition]["dining"])          if params[:other][:composition].has_key?(:dining)
          @inclusion.composition_separe_toilet      = check_0_or_1(params[:other][:composition][:separate_toilet])  if params[:other][:composition].has_key?(:separate_toilet)
          @inclusion.composition_open_kitchen       = check_0_or_1(params[:other][:composition][:open_kitchen])     if params[:other][:composition].has_key?(:open_kitchen)
        end
        if params[:other][:heating].present?
          @inclusion.heating_electric               = check_0_or_1(params[:other][:heating]["electric"])      if params[:other][:heating].has_key?(:electric)
          @inclusion.heating_solar                  = check_0_or_1(params[:other][:heating]["solar"])         if params[:other][:heating].has_key?(:solar)
          @inclusion.heating_gaz                    = check_0_or_1(params[:other][:heating]["gaz"])           if params[:other][:heating].has_key?(:gaz)
          @inclusion.heating_condensation           = check_0_or_1(params[:other][:heating]["condensation"])  if params[:other][:heating].has_key?(:condensation)
          @inclusion.heating_fuel                   = check_0_or_1(params[:other][:heating]["fuel"])          if params[:other][:heating].has_key?(:fuel)
          @inclusion.heating_heat_pump              = check_0_or_1(params[:other][:heating][:heat_pump])      if params[:other][:heating].has_key?(:heat_pump)
          @inclusion.heating_floor_heating          = check_0_or_1(params[:other][:heating][:floor_heating])  if params[:other][:heating].has_key?(:floor_heating)
        end
        if params[:other][:transport].present?
          @inclusion.transport_bicycle_path         = check_0_or_1(params[:other][:transport][:bicycle_path])     if params[:other][:transport].has_key?(:bicycle_path)
          @inclusion.transport_public_transport     = check_0_or_1(params[:other][:transport][:public_transport]) if params[:other][:transport].has_key?(:public_transport)
          @inclusion.transport_public_bike          = check_0_or_1(params[:other][:transport][:public_bike])      if params[:other][:transport].has_key?(:public_bike)
          @inclusion.transport_subway               = check_0_or_1(params[:other][:transport]["subway"])          if params[:other][:transport].has_key?(:subway)
          @inclusion.transport_train_station        = check_0_or_1(params[:other][:transport][:train_station])    if params[:other][:transport].has_key?(:train_station)
        end
        if params[:other][:security].present?
          @inclusion.security_guardian              = check_0_or_1(params[:other][:security]["guardian"])       if params[:other][:security].has_key?(:guardian)
          @inclusion.security_alarm                 = check_0_or_1(params[:other][:security]["alarm"])          if params[:other][:security].has_key?(:alarm)
          @inclusion.security_intercom              = check_0_or_1(params[:other][:security]["intercom"])       if params[:other][:security].has_key?(:intercom)
          @inclusion.security_camera                = check_0_or_1(params[:other][:security]["camera"])         if params[:other][:security].has_key?(:camera)
          @inclusion.security_smoke_dectector       = check_0_or_1(params[:other][:security][:smoke_dectector]) if params[:other][:security].has_key?(:smoke_dectector)
        end
        @inclusion.save if @inclusion.changed?
      end


      ##============================================================##
      ## Étape 5 : Property Complements (après @property.save)
      ##============================================================##
      @complement = PropertyComplement.where(:property_id=>@property.id).first_or_create
      if params[:france].present?
        @complement.france_dpe_indice           = params[:france][:dpe_indice]  if params[:france].has_key?(:dpe_indice)
        @complement.france_dpe_value            = params[:france][:dpe_value]   if params[:france].has_key?(:dpe_value)
        @complement.france_dpe_id               = params[:france][:dpe_id]      if params[:france].has_key?(:dpe_id)
        @complement.france_ges_indice           = params[:france][:ges_indice]  if params[:france].has_key?(:ges_indice)
        @complement.france_ges_value            = params[:france][:ges_value]   if params[:france].has_key?(:ges_value)
        @complement.france_alur_is_condo       = check_0_or_1(params[:france][:alur_is_condo])        if params[:france].has_key?(:alur_is_condo)
        @complement.france_alur_legal_action   = check_0_or_1(params[:france][:alur_legal_action])    if params[:france].has_key?(:alur_legal_action)

        @complement.france_alur_units          = params[:france][:alur_units]           if params[:france].has_key?(:alur_units)
        @complement.france_alur_uninhabitables = params[:france][:alur_uninhabitables]  if params[:france].has_key?(:alur_uninhabitables)
        @complement.france_alur_spending       = params[:france][:alur_spending]        if params[:france].has_key?(:alur_spending)

        @complement.france_mandat_exclusif     = check_0_or_1(params[:france][:mandat_exclusif])      if params[:france].has_key?(:mandat_exclusif)
        @complement.france_rent_honorary       = params[:france][:rent_honorary]   if params[:france].has_key?(:rent_honorary)

      end
      if params[:belgium].present?
        @complement.belgium_peb_indice            = params[:belgium][:peb_indice]  if params[:belgium][:peb_indice].present?
        @complement.belgium_peb_value             = params[:belgium][:peb_value]   if params[:belgium][:peb_value].present?
        @complement.belgium_peb_id                = params[:belgium][:peb_id]      if params[:belgium].has_key?(:peb_id)
        @complement.belgium_building_permit       = check_0_or_1(params[:belgium][:building_permit])      if params[:belgium].has_key?(:building_permit)
        @complement.belgium_done_assignments      = check_0_or_1(params[:belgium][:done_assignments])     if params[:belgium].has_key?(:done_assignments)
        @complement.belgium_preemption_property   = check_0_or_1(params[:belgium][:preemption_property])  if params[:belgium].has_key?(:preemption_property)
        @complement.belgium_subdivision_permits   = check_0_or_1(params[:belgium][:subdivision_permits])  if params[:belgium].has_key?(:subdivision_permits)
        @complement.belgium_sensitive_flood_area  = check_0_or_1(params[:belgium][:sensitive_flood_area]) if params[:belgium].has_key?(:sensitive_flood_area)
        @complement.belgium_delimited_flood_zone  = check_0_or_1(params[:belgium][:delimited_flood_zone]) if params[:belgium].has_key?(:delimited_flood_zone)
        @complement.belgium_risk_area             = check_0_or_1(params[:belgium][:risk_area ])           if params[:belgium].has_key?(:risk_area)
      end
      if params[:canada].present?
        @complement.canada_mls    = params[:canada][:mls]           if params[:canada].has_key?(:mls)
      end
      @complement.save if @complement.changed?


      ##============================================================##
      ## Étape 3 : Pictures (après @property.save)
      # If one picture failed, log warning in logs
      ##============================================================##
      if params[:pictures].present? && params[:pictures].size >= 1
        @picture_in_progress = 1
        AssetsWorker.perform_async(:model => 'property',:id => @property.id,:pictures => params[:pictures])
      end



      render "retrieve"
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.properties#create_and_update",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message}, :status=> 400
    end
  end


  ##============================================================##
  ## GET /api/v2/properties/:uid
  ##============================================================##
  def retrieve
    begin
      @property = Property.where(:uid =>params[:uid], :api_provider_id => @api_app.api_provider_id).first
      return render :json =>{:errors=>"Not Found property with uid #{params[:uid]}" }, :status=>400 if @property.blank?
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.properties#retrieve",
        :message  => e.message,
        :params   => title
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=> 400
    end
  end


  ##============================================================##
  ## DELETE /api/v2/properties/:uid
  ##============================================================##
  def destroy
    begin
      @property = Property.where(:uid =>params[:uid],:api_provider_id => @api_app.api_provider_id).first
      BuildingPropertyDeleteWorker.perform_async(:model => "property",:id => @property.id)
      return render :json =>{:errors=>"Not Found property with uid #{params[:uid]}" }, :status=>400 if @property.blank?
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.properties#destroy",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=>500
    end
  end

  ##============================================================##
  ## GET /api/v2/craigslist/:id
  ##============================================================##
  def craigslist
    @property = Property.where(:id => params[:id]).first
    @visibility_portal = @property.visibility_portals.where(:portal_id => Portal.where(:slug => "craigslist").first.id).last
    cities = []
    cities << { :name => "Montréal",        :id => "montreal, qc, ca",        :home_link => "http://montreal.craigslist.ca/",       :geocoded_address => [45.501689, -73.567256] }
    cities << { :name => "saguenay",        :id => "saguenay, qc, ca",        :home_link => "http://saguenay.craigslist.ca/",       :geocoded_address => [48.428053, -71.068492] }
    cities << { :name => "Trois-Rivières",  :id => "trois-rivieres, qc, ca",  :home_link => "http://troisrivieres.craigslist.ca/",  :geocoded_address => [46.343240, -72.543283] }
    cities << { :name => "Sherbrooke",      :id => "sherbrooke, qc, ca",      :home_link => "http://sherbrooke.craigslist.ca/",     :geocoded_address => [45.400993, -71.882429] }
    cities << { :name => "Québec",          :id => "quebec, qc, ca",          :home_link => "http://quebec.craigslist.ca/",         :geocoded_address => [46.813878, -71.207981] }
    property_location = [@property.latitude, @property.longitude]
    near = nil
    @home_link = ""
    @near_city = ""
    cities.each do |c|
      distance = Geocoder::Calculations.distance_between(property_location, c[:geocoded_address])
      if near.nil? || near > distance
        @near_city = c[:id]
        @home_link = c[:home_link]
        near = distance
      end
    end
  end

  ##============================================================##
  ## GET /api/v2/kijiji/:id
  ##============================================================##
  def kijiji
    begin
      @property = Property.where(:id => params[:id]).first
      @visibility_portal = @property.visibility_portals.where(:portal_id => Portal.where(:slug => "kijiji").first.id).last
      cities = []
      cities << { :name => "RouynNoranda",              :home_link => "http://www.kijiji.ca/h-rouyn-noranda/1700060",               :geocoded_address => [48.805400, -79.199100] }
      cities << { :name => "Val d'or",                  :home_link => "http://www.kijiji.ca/h-val-dor/1700061",                     :geocoded_address => [48.106800, -77.783300] }
      cities << { :name => "Baie Comeau",               :home_link => "http://www.kijiji.ca/h-baie-comeau/1700251",                 :geocoded_address => [48.106800, -68.144200] }
      cities << { :name => "Drummondville",             :home_link => "http://www.kijiji.ca/h-drummondville/1700122",               :geocoded_address => [45.909200, -72.480800] }
      cities << { :name => "Victoriaville",             :home_link => "http://www.kijiji.ca/h-victoriaville/1700123",               :geocoded_address => [46.060600, -71.947700] }
      cities << { :name => "Lévis",                     :home_link => "http://www.kijiji.ca/h-levis/1700063",                       :geocoded_address => [46.820700, -71.178700] }
      cities << { :name => "Saint Georges",             :home_link => "http://www.kijiji.ca/h-st-georges-de-beauce/1700065",        :geocoded_address => [46.137900, -70.671500] }
      cities << { :name => "Thetford mines",            :home_link => "http://www.kijiji.ca/h-thetford-mines/1700064",              :geocoded_address => [46.113400, -71.310800] }
      cities << { :name => "Chibougamau",               :home_link => "http://www.kijiji.ca/h-chibougamau-nord-du-quebec/1700284",  :geocoded_address => [49.921400, -74.360100] }
      cities << { :name => "Gaspesie",                  :home_link => "http://www.kijiji.ca/h-gaspesie/1700066",                    :geocoded_address => [48.929800, -64.343800] }
      cities << { :name => "Granby",                    :home_link => "http://www.kijiji.ca/h-granby/1700253",                      :geocoded_address => [45.410900, -72.710300] }
      cities << { :name => "Laval",                     :home_link => "http://www.kijiji.ca/h-laval-rive-nord/1700278",             :geocoded_address => [45.577200, -73.700700] }
      cities << { :name => "Longueil",                  :home_link => "http://www.kijiji.ca/h-longueuil-rive-sud/1700279",          :geocoded_address => [45.529000, -73.503900] }
      cities << { :name => "Ouest montreal",            :home_link => "http://www.kijiji.ca/h-ouest-de-lile-qc/1700280",            :geocoded_address => [45.457500, -73.664900] }
      cities << { :name => "Montréal",                  :home_link => "http://www.kijiji.ca/h-ville-de-montreal/1700281",           :geocoded_address => [45.501689, -73.567256] }
      cities << { :name => "Lanaudiere",                :home_link => "http://www.kijiji.ca/h-lanaudiere/1700283",                  :geocoded_address => [45.837300, -74.138700] }
      cities << { :name => "Laurentides",               :home_link => "http://www.kijiji.ca/h-laurentides/1700282",                 :geocoded_address => [45.837300, -74.138700] }
      cities << { :name => "Laurentides",               :home_link => "http://www.kijiji.ca/h-laurentides/1700282",                 :geocoded_address => [46.263400, -74.768700] }
      cities << { :name => "Shawinigan",                :home_link => "http://www.kijiji.ca/h-shawinigan/1700148",                  :geocoded_address => [46.542900, -72.748000] }
      cities << { :name => "Trois-Rivières",            :home_link => "http://www.kijiji.ca/h-trois-rivieres/1700150",              :geocoded_address => [46.343240, -72.543283] }
      cities << { :name => "Rimouski",                  :home_link => "http://www.kijiji.ca/h-rimouski-bas-st-laurent/1700250",     :geocoded_address => [48.452500, -68.523200] }
      cities << { :name => "Lac Saint Jean",            :home_link => "http://www.kijiji.ca/h-lac-saint-jean/1700180",              :geocoded_address => [48.870700, -72.214100] }
      cities << { :name => "Saguenay",                  :home_link => "http://www.kijiji.ca/h-saguenay/1700179",                    :geocoded_address => [48.344800, -70.986900] }
      cities << { :name => "Saint hyacinthe",           :home_link => "http://www.kijiji.ca/h-saint-hyacinthe/1700151",             :geocoded_address => [45.635200, -72.972600] }
      cities << { :name => "Saint Jean Sur Richelieu",  :home_link => "http://www.kijiji.ca/h-saint-jean-sur-richelieu/1700252",    :geocoded_address => [45.323400, -73.266200] }
      cities << { :name => "Sept-iles",                 :home_link => "http://www.kijiji.ca/h-sept-iles/1700071",                   :geocoded_address => [50.220600, -66.358100] }
      cities << { :name => "Sherbrooke",                :home_link => "http://www.kijiji.ca/h-sherbrooke-qc/1700156",               :geocoded_address => [45.411700, -71.907400] }
      cities << { :name => "Québec",                    :home_link => "http://www.kijiji.ca/h-ville-de-quebec/1700124",             :geocoded_address => [46.816500, -71.236400] }


      property_location = [@property.latitude, @property.longitude]
      near = nil
      @home_link = ""
      cities.each do |c|
        distance = Geocoder::Calculations.distance_between(property_location, c[:geocoded_address])
        if near.nil? || near > distance
          @home_link = c[:home_link]
          near = distance
        end
      end
    rescue Exception => e
      JulesLogger.info e
    end
  end




  private

  def check_classification(id)
    begin
      return PropertyClassification.where(:api_id =>id).first.id
    rescue
      return 1
    end
  end



end
