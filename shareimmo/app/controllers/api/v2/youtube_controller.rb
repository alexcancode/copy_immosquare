class Api::V2::YoutubeController < Api::V2::ApplicationController

  ##============================================================##
  ## BeforeFilter
  ##============================================================##
  skip_before_action :verify_authenticity_token
  skip_before_filter :check_credentials_in_header_or_url

  def send_infos
    @property     = Property.find(params[:id])
    @profile      = @property.user.profile
    @complememnt  = PropertyComplement.where(:property_id => @property.id).first_or_create
  end

  def enhanced
    @property = Property.find(params[:id])
    @profile  = @property.user.profile
    videos    = Video.where(:property_id => @property.id, :posting_status => 1)
    @video_id = videos.present? ?  videos.last.youtube_id : "M-zdSDFuUSE"
    response.headers.delete('X-Frame-Options')
    render :layout => "api"
  end

  def contact
    @contact = BrokerContact.new(broker_contact_params)
    if @contact.save
      @message = t('back_office.thank_you')
      FrontOfficeMailer.broker_contact(@contact).deliver_later
    else
      @message = @contact.errors.full_messages.first
    end
  end


  private

  def broker_contact_params
    params.require(:broker_contact).permit(
      :email,
      :message,
      :user_id,
      :property_id
      )
  end


end
