class Api::V2::AccountsController < Api::V2::ApplicationController


  ##============================================================##
  ## POST /api/v2/accounts
  ##============================================================##
  def create
    begin
      raise "Uid Missing"  if params[:uid].blank?
      raise "Uid #{params[:uid]} is already used" if UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:uid => params[:uid]).present?
      @user = User.where(:email => params[:email]).first
      if @user.present?
        upp = UserPortalProvider.where(:user_id => @user.id,:api_provider_id =>@api_app.api_provider_id).first
        if upp.present?
          raise "Account #{@user.email} (##{@user.id}) is already registered with an other uid (##{upp.uid}). You can't create this user with this uid (#{params[:uid]})"
        else
           upp = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:uid => params[:uid],:user_id=>@user.id).first_or_create
           upp.update_attribute(:flow_name,@api_app.flow_name) if @api_app.flow_name.present?
          render :json => {:id => @user.id,:email => @user.email,:uid => params[:uid],:message => "User #{@user.email} already exists in our system. It's now linked with your api"}, :status => 200
        end
      else
        @user = User.new(:email => params[:email],:password => params[:password])
        if @user.save
          UserPortalProvider.create(:api_provider_id => @api_app.api_provider_id, :uid => params[:uid], :user_id => @user.id,:flow_name => @api_app.flow_name)
          BackOfficeMailer.new_user_registred(@user).deliver_later
          render :json =>{:id => @user.id,:uid=>params[:uid],:email => @user.email, :message => "User was created"}, :status=>200
        else
          render :json =>{:errors=>@user.errors}, :status => 402
        end
      end
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.accounts#create",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status => 402
    end
  end



  ##============================================================##
  ## GET /api/v2/accounts/:uid
  ##============================================================##
  def retrieve
    begin
      @portal = UserPortalProvider.where(:uid =>params[:uid],:api_provider_id => @api_app.api_provider_id).first
      raise "User ##{params[:uid]} Not found for your API (#{@api_app.api_provider.name})" if @portal.blank?
      @user = @portal.user
    rescue Exception => e
      return render :json =>{:errors=>e.message }, :status=>402
    end
  end


  ##============================================================##
  ## POST|PUT /api/v2/accounts/:uid
  ##============================================================##
  def update
    begin
      raise "uid Missing"                      if params[:uid].blank?
      raise "uid #{params[:uid]} is not found" if UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:uid => params[:uid]).blank?
      @portal = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id, :uid =>params[:uid]).first
      @portal.update_attribute(:flow_name,@api_app.flow_name) if @api_app.flow_name.present?
      user = @portal.user
      raise "Bad country define (#{params[:country]}), could be a ISO 3166-1 alpha-2 standard"   if params.has_key?(:country) && params[:country].present? && ISO3166::Country.find_country_by_alpha2(params[:country]).blank?
      profile = user.profile
      profile.country               = params[:country]        if params.has_key?(:country)
      profile.email                 = params[:public_email]   if params.has_key?(:public_email)
      profile.first_name            = params[:first_name]     if params.has_key?(:first_name)
      profile.last_name             = params[:last_name]      if params.has_key?(:last_name)
      profile.phone                 = params[:phone]          if params.has_key?(:phone)
      profile.phone_sms             = params[:phone_sms]      if params.has_key?(:phone_sms)
      if params[:phone_visibility].present?
        profile.phone_visibility      = is_number?(params[:phone_visibility]) ? ([0,1].include?(params[:phone_visibility].to_i) ? params[:phone_visibility] : 0) : 0
      end
      profile.picture_original_url  = params[:picture_url] if params.has_key?(:picture_url)
      if params[:picture_url].present?
        profile.picture               = open(params[:picture_url]) if profile.picture_original_url_changed? && url_is_a_image?(params[:picture_url])
      end
      profile.website               = params[:website]  if params.has_key?(:website)
      # Vérifie les languages
      if params[:spoken_languages].present?
        spoken_languages = []
        params[:spoken_languages].split(",").each do |spoken_language|
          spoken_languages << spoken_language if LanguageList::LanguageInfo.find(spoken_language).present?
        end
        profile.spoken_languages      = spoken_languages.join(",")
      end
      profile.facebook              = params[:facebook_url] if params[:facebook_url].present?
      profile.twitter               = params[:twitter_url] if params[:twitter_url].present?
      profile.linkedin              = params[:linkedin_url] if params[:linkedin_url].present?
      profile.pinterest             = params[:pinterest_url] if params[:pinterest_url].present?
      profile.google_plus           = params[:google_plus_url] if params[:google_plus_url].present?
      profile.instagram             = params[:instagram_url] if params[:instagram_url].present?
      profile.primary_color         = params[:color1] if params[:color1].present?
      profile.secondary_color       = params[:color2] if params[:color2].present?
      profile.agency_name           = params[:company_name] if params[:company_name].present?
      profile.agency_address        = params[:company_address] if params[:company_address].present?
      profile.agency_city           = params[:company_city] if params[:company_city].present?
      profile.agency_zipcode        = params[:company_zipcode] if params[:company_zipcode].present?
      profile.agency_province       = params[:company_administrative_area] if params[:company_administrative_area].present?
      profile.logo_original_url     = params[:company_logo_url] if params[:company_logo_url].present?
      profile.is_a_pro_user         = is_number?(params[:corporate_account]) ? ([0,1].include?(params[:corporate_account].to_i) ? params[:corporate_account] : 1) : 1
      if params[:company_logo_url].present?
        profile.logo                  = open(params[:company_logo_url]) if profile.logo_original_url_changed? && url_is_a_image?(params[:company_logo_url])
      end
      if params[:presentation_text].present?
        profile.job_fr              = ActionView::Base.full_sanitizer.sanitize(params[:presentation_text][:fr]) if params[:presentation_text][:fr].present?
        profile.job_en              = ActionView::Base.full_sanitizer.sanitize(params[:presentation_text][:en]) if params[:presentation_text][:en].present?
        profile.job_de              = ActionView::Base.full_sanitizer.sanitize(params[:presentation_text][:de]) if params[:presentation_text][:de].present?
        profile.job_nl              = ActionView::Base.full_sanitizer.sanitize(params[:presentation_text][:nl]) if params[:presentation_text][:nl].present?
        profile.job_it              = ActionView::Base.full_sanitizer.sanitize(params[:presentation_text][:it]) if params[:presentation_text][:it].present?
        profile.job_es              = ActionView::Base.full_sanitizer.sanitize(params[:presentation_text][:es]) if params[:presentation_text][:es].present?
        profile.job_pt              = ActionView::Base.full_sanitizer.sanitize(params[:presentation_text][:pt]) if params[:presentation_text][:pt].present?
      end
      if params[:bio].present?
        profile.bio_fr              = params[:bio][:fr] if params[:bio][:fr].present?
        profile.bio_en              = params[:bio][:en] if params[:bio][:en].present?
        profile.bio_de              = params[:bio][:de] if params[:bio][:de].present?
        profile.bio_nl              = params[:bio][:nl] if params[:bio][:nl].present?
        profile.bio_it              = params[:bio][:it] if params[:bio][:it].present?
        profile.bio_es              = params[:bio][:es] if params[:bio][:es].present?
        profile.bio_pt              = params[:bio][:pt] if params[:bio][:pt].present?
      end
      if params[:baseline].present?
        profile.baseline_fr              = ActionView::Base.full_sanitizer.sanitize(params[:baseline][:fr]) if params[:baseline][:fr].present?
        profile.baseline_en              = ActionView::Base.full_sanitizer.sanitize(params[:baseline][:en]) if params[:baseline][:en].present?
        profile.baseline_de              = ActionView::Base.full_sanitizer.sanitize(params[:baseline][:de]) if params[:baseline][:de].present?
        profile.baseline_nl              = ActionView::Base.full_sanitizer.sanitize(params[:baseline][:nl]) if params[:baseline][:nl].present?
        profile.baseline_it              = ActionView::Base.full_sanitizer.sanitize(params[:baseline][:it]) if params[:baseline][:it].present?
        profile.baseline_es              = ActionView::Base.full_sanitizer.sanitize(params[:baseline][:es]) if params[:baseline][:es].present?
        profile.baseline_pt              = ActionView::Base.full_sanitizer.sanitize(params[:baseline][:pt]) if params[:baseline][:pt].present?
      end
      if profile.save
        @user = user
        render "retrieve"
      else
        render :json =>{:errors=>profile.errors}, :status=>402
      end
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.accounts#update",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=>402
    end
  end

  ##============================================================##
  ## PUT /api/v2/accounts/:uid/password
  ## Params
  ## :uid
  ## :old_password
  ## :password
  ## :password_confirmation
  ##============================================================##
  def update_password
    begin
      raise "Uid Missing"  if params[:uid].blank?
      raise "Uid #{params[:uid]} is not found" if UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:uid => params[:uid]).blank?
      @portal = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id, :uid =>params[:uid]).first
      @user = @portal.user
      raise "Old password is invalid Missing"  if !@user.valid_password?(params[:old_password])
      render :json =>{:errors=>@user.errors}, :status=>402 unless @user.update(:password => params[:password], :password_confirmation => params[:password_confirmation])
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.accounts#update_password",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=>402
    end
  end


  ##============================================================##
  ## PUT /api/v2/accounts/:uid/
  ## Params
  ## :uid
  ## :email
  ## :secure_code (Hash from ":uid_:email)
  ##============================================================##
  def update_email
    begin
      raise "uid Missing"  if params[:uid].blank?
      raise "uid #{params[:uid]} is not found" if UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:uid => params[:uid]).blank?
      @last_email = Digest::SHA1.hexdigest("#{params[:uid]}_#{params[:email]}")
      if (params[:secure_code] == Digest::SHA1.hexdigest("#{params[:uid]}_#{params[:email]}"))
        @portal = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id, :uid =>params[:uid]).first
        @user = @portal.user
        @last_email = @user.email
        raise "Email was already defined"  if @last_email == params[:email]
        @user.email = params[:email]
        render :json =>{:errors=>@user.errors}, :status=>402 unless @user.save
      else
        raise "Bad secure code"
      end
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.accounts#update_email",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=>402
    end
  end


  ##============================================================##
  ## GET api/v2/accounts/:uid/token
  ## Se token sert pour une connexion automatique
  ##============================================================##
  def get_token
    begin
      @portal = UserPortalProvider.where(:uid =>params[:uid],:api_provider_id => @api_app.api_provider_id).first
      raise "User ##{params[:uid]} Not found for your API (#{@api_app.api_provider.name})" if @portal.blank?
      @user = @portal.user
      @token = SecureRandom.hex(45)
      @user.update_attribute(:connect_token,@token)
      render :json =>{:token => @token}
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.accounts#get_token",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status => 400
    end
  end


  ##============================================================##
  ## GET api/v2/accounts/:uid/connect
  ##============================================================##
  def connect
    begin
      raise "token missing" if params[:token].blank?
      @portal = UserPortalProvider.where(:uid =>params[:uid],:api_provider_id => @api_app.api_provider_id).first
      raise "User ##{params[:uid]} Not found for your API (#{@api_app.api_provider.name})" if @portal.blank?
      @user = @portal.user
      raise "token invalid" if @user.connect_token != params[:token]


      if params[:redirect] == "property" and params[:property].present?
        redirect = dashboard_property_edit_path(@user.username, Property.find(params[:property]).secure_id,:store=> params[:store])
      elsif params[:redirect] == "contact"
        redirect = dashboard_property_contacts_path(@user.username,:store=> params[:store])
      elsif params[:redirect] == "profile"
        redirect = dashboard_profile_path(@user.username,:store=> params[:store])
      elsif params[:redirect] == "building"
        redirect = dashboard_buildings_path(@user.username,:store=> params[:store])
      else
        redirect = dashboard_path(@user.username,:store=> params[:store])
      end

      store_location_for(@user,redirect)
      sign_in_and_redirect(@user, :store =>true)
    rescue Exception => e
      flash[:danger] = "Access Denied  => #{e.message}"
      redirect_to root_path
    end
  end

  ##============================================================##
  ## GET api/v2/accounts/uid/properties
  ##============================================================##
  def properties
    begin
      @portal = UserPortalProvider.where(:uid =>params[:uid],:api_provider_id => @api_app.api_provider_id).first
      raise "User ##{params[:uid]} Not found for your API" if @portal.blank?
      @user = @portal.user
      @properties = @user.properties.where(:status => 1)

    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.accounts#properties",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=>402
    end
  end



  ##============================================================##
  ## GET api/v2/marketing/
  ##============================================================##
  def marketing
    begin
      @user = User.where(:username=>params[:subdomain]).first
      raise "User with username #{params[:subdomain]} not found" if @user.blank?
      @property = Property.where(:id=>params[:property],:user_id =>@user.id).first  if params[:property].present?
      @my_services = Array.new
      ServicesUser.where(:user_id=> @user.id,:on_marketing_page => 1).order(:service_id).each do |service_user|
        service = service_user.service
        @my_services << {
          :name           => service.name,
          :wistia_id      => service.wistia_id,
          :youtube_id     => service.youtube_id,
          :description    => service.marketing_description,
          :link           => service.immosquare_link,
          :image_url      => view_context.image_url("shareimmo/services/#{service.slug}_color.svg")
        }
      end
      @user.custom_services.each do |service|
        @my_services << {
          :name           => service.name,
          :wistia_id      => nil,
          :youtube_url    => service.video_url,
          :description    => service.description,
          :link           => service.link,
          :image_url     => service.cover.url(:large,:timestamp => false),
        }
      end
    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.accounts#marketing",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message }, :status=>400
    end
  end



  ##============================================================##
  ## DELETE /api/v2/accounts/:uid
  ##============================================================##
  def destroy
    begin
      raise "uid is Missing"  if params[:uid].blank?
      raise "uid #{params[:uid]} is not found" if UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id,:uid => params[:uid]).blank?
      @portal = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id, :uid =>params[:uid]).first
      @portal.destroy
    rescue Exception => e
      return render :json =>{:errors=>e.message }, :status=>500
      ApiMailer.error_monitoring(
        :title    => "api.v2.accounts#destroy",
        :message  => e.message,
        :params   => params
        ).deliver_later
    end
  end

  ##============================================================##
  ## GET /api/v2/accounts/brokers
  ##============================================================##
  def brokers
    @response = []
    user_uids = []

    UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id).each do |user|
      user_response = { :uid => user.uid,:properties => []}
      user_uids << user.uid
      Property.where(:api_provider_id => @api_app.api_provider_id, :user_id => user.uid).each do |property|
        user_response[:properties] << {
          :uid => property.uid,
        }
      end
      @response << user_response
    end
    render :json => {:brokers => @response, :user_uids => user_uids}, :status=>201
  end

end
