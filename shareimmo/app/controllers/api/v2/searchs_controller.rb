module Api
  module V2
    class SearchsController < Api::V2::ApplicationController

      ##============================================================##
      ## Before & After
      ##============================================================##
      skip_before_action  :verify_authenticity_token


      def properties
        begin
          raise "Your ApiKey/ApiToken are not authorized for search"  if @api_app.search_authorized_users.blank?
          params[:user] = @api_app.search_authorized_users            if @api_app.search_authorized_users != "*"
          @response = Property.my_search_elastic(params)
          @index = "properties"
          render request.path.include?("geojson") ? "api/v2/searchs/geojson" : "api/v2/searchs/elastic"
        rescue Exception => e
          ApiMailer.error_monitoring(
            :title    => "api.v2.searches#properties",
            :message  => e.message,
            :params   => params
            ).deliver_later
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end



      def properties_show
        begin
          raise "Your ApiKey/ApiToken are not authorized for search" if @api_app.search_authorized_users.blank?
          @property = Property.find(params[:id])
          if @api_app.search_authorized_users != "*"
            raise "Not authorized" if !@api_app.search_authorized_users.split(',').include?(@property.user_id.to_s)
          end
          render 'api/v2/properties/retrieve'
        rescue Exception => e
          ApiMailer.error_monitoring(
            :title    => "api.v2.searches#properties_show",
            :message  => e.message,
            :params   => params
            ).deliver_later
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end


      def buildings
        begin
          raise "Your ApiKey/ApiToken are not authorized for search" if @api_app.search_authorized_users.blank?
          params[:user] = @api_app.search_authorized_users           if @api_app.search_authorized_users != "*"
          @response = Building.my_search_elastic(params)
          @index = "buildings"
          render request.path.include?("geojson") ? "api/v2/searchs/geojson" : "api/v2/searchs/elastic"
        rescue Exception => e
          ApiMailer.error_monitoring(
            :title    => "api.v2.searches#properties",
            :message  => e.message,
            :params   => params
            ).deliver_later
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end


      def buildings_show
        begin
          raise "Your ApiKey/ApiToken are not authorized for search" if @api_app.search_authorized_users.blank?
          @building = Building.find(params[:id])
          raise "Not authorized" if  @api_app.search_authorized_users != "*" && !@api_app.search_authorized_users.split(',').include?(@building.user_id.to_s) if
          render 'api/v2/buildings/retrieve'
        rescue Exception => e
          ApiMailer.error_monitoring(
            :title    => "api.v2.searches#building_show",
            :message  => e.message,
            :params   => params
            ).deliver_later
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end


      def accounts_show
        begin
          raise "Your ApiKey/ApiToken are not authorized for search" if @api_app.search_authorized_users.blank?
          @user = User.find(params[:id])
          if @api_app.search_authorized_users != "*"
            raise "Not authorized" if !@api_app.search_authorized_users.split(',').include?(@user.id.to_s)
          end
          render 'api/v2/accounts/retrieve'
        rescue Exception => e
          ApiMailer.error_monitoring(
            :title    => "api.v2.searches#accounts_show",
            :message  => e.message,
            :params   => params
            ).deliver_later
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end





    end
  end
end
