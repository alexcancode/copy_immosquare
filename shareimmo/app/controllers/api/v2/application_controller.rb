module Api
  module V2
    class ApplicationController < ActionController::Base



      ##============================================================##
      ## Before & After
      ##============================================================##
      skip_before_action :verify_authenticity_token
      before_filter :check_credentials_in_header_or_url, :except => [:sign_in]
      before_filter :setup_locale


      def default_url_options(options = {})
        {locale: I18n.locale}
      end


      ##============================================================##
      ## Setup Language for Api Response
      ##============================================================##
      def setup_locale
        I18n.locale = params[:locale] || "en"
      end



      def check_credentials_in_header_or_url
        begin
          if request.headers["apiKey"].blank? or request.headers["apiToken"].blank?
            raise "No Credentials Found" if(params["apiKey"].blank? or params["apiToken"].blank?)
            @api_app = ApiApp.where(:key=>params["apiKey"],:token=>params["apiToken"]).first
            @api_app.flow_name = params["flowName"] if params.has_key?("flowName")
            raise "Not Authorized" if @api_app.blank?
          else
            @api_app = ApiApp.where(:key=>request.headers["apiKey"],:token=>request.headers["apiToken"]).first
            @api_app.flow_name = request.headers["flowName"] if request.headers["flowName"].present?
            raise "Not Authorized" if @api_app.blank?
          end
        rescue Exception => e
          return render :json =>{:errors=>e.message}, :status=> 402
        end
      end


      private


      def url_is_a_image?(url)
        begin
          FastImage.size(url).kind_of?(Array)
        rescue Exception => e
          JulesLogger.info e.message
          return false
        end
      end

      def check_account_uid
        unless User.where(params[:accountUid]).first.nil?
        end
      end

      def is_number?(string)
        true if Float(string) rescue false
      end

      def check_0_or_1(params)
        is_number?(params) ? ((0..1).include?(params.to_i) ? params : nil ) : nil
      end

      def check_1_to_2(params)
        is_number?(params) ? ((1..2).include?(params.to_i) ? params : nil ) : nil
      end

      def check_1_to_3(params)
        is_number?(params) ? ((1..3).include?(params.to_i) ? params : nil ) : nil
      end

      def check_1_to_5(params)
        is_number?(params) ? ((1..5).include?(params.to_i) ? params : nil ) : nil
      end

      def check_1_to_4(params)
        is_number?(params) ? ((1..4).include?(params.to_i) ? params : nil ) : nil
      end

      def check_1_to_20(params)
        is_number?(params) ? ((1..20).include?(params.to_i) ? params : nil ) : nil
      end




    end
  end
end
