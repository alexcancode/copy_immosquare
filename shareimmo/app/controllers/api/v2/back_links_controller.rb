class Api::V2::BackLinksController < Api::V2::ApplicationController

  ##============================================================##
  ## API PUBLIC
  ##============================================================##
  skip_before_filter :check_credentials_in_header_or_url


  def international
    url = "https://identityserver.portia.ch:33333/api/v1/onbehalfof?apikey=#{ENV['ListGlobally_api_key']}&accountid=#{params[:u]}"
    httpparty = HTTParty.get(url)
    if httpparty.code == 200
      token       = JSON.parse(httpparty.body)['AccessToken']
      # @iframe_url = "https://www.mylistglobally.com/Security/LoginByToken?accessToken=#{token}&ReturnUrl=#{params[:p].present? ? "/AdvertExternal/#{params[:p]}" : "/dashboard" }?nav=1"
      @iframe_url = "https://www.mylistglobally.com/Security/LoginByToken?accessToken=#{token}&ReturnUrl=/listings?nav=1"
      JulesLogger.info @iframe_url
    end

    render :layout => 'application'
  end

end


