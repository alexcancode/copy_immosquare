class Api::V2::ServicesController < Api::V2::ApplicationController

  def activate
    begin
      raise "service is missing" if params[:service].blank?
      raise "uid is missing" if params[:uid].blank?
      raise "Status (0,1) is missing" if params[:status].blank?



      @status = params[:status].to_i
      raise "Status should be 0 (deactivate) or 1 (activate)" unless [0,1].include?(@status)

      @portal = UserPortalProvider.where(:api_provider_id => @api_app.api_provider_id, :uid =>params[:uid]).first
      raise "Any user match with account_uid : #{params[:uid]} for your api" if @portal.blank?

      @user = @portal.user

      @service = Service.where(:id => params[:service]).first

      raise "Bad service selected" if @service.nil?

      @servicesUser = ServicesUser.where(:service => @service.id, :user_id => @user.id)

      @return = {:service=>@service.id, :uid => params[:uid], :status => @status}
      if @status == 1
        @servicesUser = @servicesUser.first_or_create
        @servicesUser.update_attributes(:options => ( params[:option].present? ? params[:option] : "") )
        @message = "Your service was activated"
      elsif @status == 0
        @servicesUser.first.destroy unless @servicesUser.first.nil?
        @message = "Your service was deactivated"
      else
        @message = ""
      end
      @return[:option] = params[:option] if params[:option].present?
      @return[:message] = @message

      return render :json => @return, :status=>402

    rescue Exception => e
      ApiMailer.error_monitoring(
        :title    => "api.v2.services#activate",
        :message  => e.message,
        :params   => params
        ).deliver_later
      return render :json =>{:errors=>e.message}, :status=> 402
    end

  end



end
