class Api::V2::AuthController < Api::V2::ApplicationController

  require 'bcrypt'

  ##============================================================##
  ## Before
  ##============================================================##
  before_filter :set_user_by_token, :only => [:validate_token,:logout]
  before_action :check_account_uid, :only => [:sync_edl]

  def sign_in
  	begin

	    @kangalou_api_app = ApiProvider.where(:name=>"kangalou").first.api_app

	    @user = User.where(:email =>params[:email]).first

	    raise "no user for email ##{params[:email]}" if @user.nil?

		@user_portal_provider = @user.user_portal_providers.where(:api_provider_id => @kangalou_api_app.api_provider_id).first
	    raise "User dont exist for portal kangalou, email : ##{params[:email]}" if @user_portal_provider.nil?		


	    @api_app = @user_portal_provider.api_provider.api_app

	    # Kangalou authentification if provider is ...
	    if @user_portal_provider.present?
	      @callApiConnection = HTTParty.post("https://management-staging.kangalou.com/fr/membre/api/login-verification",:body => {
	        :secretkey => "xouLq83HNWK293I6ak81T24P0GeGjO3x",
	        :email => params[:email],
	        :password => params[:password]}.to_json
	      )
	      if ActiveSupport::JSON.decode(@callApiConnection.body)["uid"] != 0
	        # signin was ok
	        @user.update_attribute(:tokens,{}) if @user.tokens.blank?
	        @client_id = SecureRandom.urlsafe_base64(nil, false)
	        @token     = SecureRandom.urlsafe_base64(nil, false)
	        @user.tokens[@client_id] = { :token => BCrypt::Password.create(@token),:expiry => (Time.now + 2.years).to_i}
	        @kangalou_params = ActiveSupport::JSON.decode(@callApiConnection.body)

	        @has_token_body = {
	          :member => @user.uid,
	          :credentials => {
	            :apikey => "a989f817-1dd6-6d92-ec89-4486cd187fk9",
	            :email => @user.email,
	            :encryptedpassword => @kangalou_params["encryptedpassword"]
	          }
	        }

	        # Check token - Kangalou
	        @callApiHasTokens = HTTParty.post("https://staging.kangalou.com/api/has-tokens",:body => @has_token_body.to_json)
	        JulesLogger.info @kangalou_params
	        if ActiveSupport::JSON.decode(@callApiHasTokens.body)["nbTokens"]
	          @kangalou_params["nbTokens"] = ActiveSupport::JSON.decode(@callApiHasTokens.body)["nbTokens"]
	        else
	          @kangalou_params["nbTokens"] = 0
	        end
	        JulesLogger.info @kangalou_params

	        @user.kangalou_params = ActiveSupport::JSON.encode(@kangalou_params)
	        @user.save
	        # sign_in(:user, @user, store: false, bypass: false)

	      else
	      	raise "Error login for user #{params[:email]}"
	      end
	    # Else shareimmo authentification (classic)
	    else
	      @user = User.where(:email =>params[:email]).first || nil
	      if @user and @user.valid_password?(params[:password])
	        # can't set default on text fields in mysql, simulate here instead.
	        @user.update_attribute(:tokens,{}) if @user.tokens.blank?
	        @client_id = SecureRandom.urlsafe_base64(nil, false)
	        @token     = SecureRandom.urlsafe_base64(nil, false)
	        @user.tokens[@client_id] = { :token => BCrypt::Password.create(@token),:expiry => (Time.now + 2.years).to_i}
	        sign_in(:user, @user, store: false, bypass: false)
	        @user.save
	      else
	      end
	    end

	  rescue Exception => e
        render json: {errors: ["Invalid login credentials. Please try again."]}, status: 401	  	
	  	JulesLogger.info e
	  end


  end


  def validate_token
    if @user
      render 'api/v2/auth/login'
    else
      render json: { success: false, errors: ["Invalid login credentials"]}, status: 401
    end
  end


  def logout
    user = remove_instance_variable(:@user) if @user
    client_id = remove_instance_variable(:@client_id) if @client_id
    remove_instance_variable(:@token) if @token

    if user and client_id and user.tokens[client_id]
      user.tokens.delete(client_id)
      user.save!
      render json: {success:true}, status: 200
    else
      render json: {errors: ["User was not found or was not logged in."]}, status: 404
    end
  end


  def sync_edl

      @user = User.find(params[:accountUid])

      @glossary = Glossary.where(:user_id => @user.id).first_or_create
      @nomenclatures = @user.nomenclatures+NomenclatureDefault.all

  end


  private

  def token_validation_response(user)
    user.as_json(except: [:tokens, :created_at, :updated_at])
  end


end