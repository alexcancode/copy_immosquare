class Api::V2::InventoriesController < ApplicationController
  skip_before_action :verify_authenticity_token


  def create

    @inventory = Inventory.new(inventory_params)
    @inventory.user = User.find(params[:accountUid])

    # Check if user has token to create
    @callApiHasTokens = has_token_api_request(@inventory.user.uid, @inventory.user.email, ActiveSupport::JSON.decode(@inventory.user.kangalou_params)["encryptedpassword"])

    if ActiveSupport::JSON.decode(@callApiHasTokens.body)["nbTokens"].to_i == 0
      @status = 400
      @errors = ["No credit"]
    else

      case params[:inventory][:file_type]
      # JSON File
      when 2
        unless params[:inventory][:json_content] && params[:inventory][:json_content].empty?

        require 'json'
        @json_content = params[:inventory][:json_content]

          @final_file = Rails.root+"tmp/#{current_user}_#{@inventory.id}.json"
          File.open(@final_file, "w") do |file|
            file.write(@json_content.to_json)
          end
          @inventory.xml = File.new(@final_file)

        end
      # XML File
      else

        require 'base64'

        unless params[:inventory][:xml_file] && params[:inventory][:xml_file].empty?

          @final_file = Rails.root+"tmp/#{current_user}_#{@inventory.id}.xml"
          File.open(@final_file, "wb") do |file|
            file.write(Base64.decode64(params[:inventory][:xml_file]))
          end
          @inventory.xml = File.new(@final_file)

          @json_content = {}
        end

      end

      if @inventory.save(:validate => false)
        @status = 200

        renters_emails = params[:inventory][:renters_emails]

        # Send email to renters
        if renters_emails.kind_of?(Array)
          renters_emails.each do |renter|
            ApiMailer.success_edl_sent(@inventory, @json_content, renter).deliver_later
          end
        end
        # Send email to admin
        ApiMailer.success_edl_sent(@inventory, @json_content, @inventory.user.email).deliver_later

        # Remove token - todo
        @callApiRemoveTokens = remove_token_api_request(@inventory.user.uid, @inventory.user.email, ActiveSupport::JSON.decode(@inventory.user.kangalou_params)["encryptedpassword"])

        # If remove token failed, add missing token in backend
        if !ActiveSupport::JSON.decode(@callApiRemoveTokens.body)["removed"]
          JulesLogger.info "Token not remove for user {@inventory.user.uid}"
          @user = @inventory.user
          @user.update_attribute(:kangalou_tokens_not_sync, @user.kangalou_tokens_not_sync+1)
        end


      else
        @status = 400
        @errors = @inventory.errors
      end

    end
  end

  def index

  end

  def buildings
    unless params[:accountUid].nil?
      @buildings = Building.where(:user_id => params[:accountUid])
    end
  end

  def show
    @property = Property.find_by_secure_id(params[:secure_id])
  end

  def send_inventory_error
    @title = "Error"
    if params[:error]
      @error = params[:error]
    end
    if params[:account]
      @account = params[:account]
    end
    unless @account.nil? && @error.nil?
      ApiMailer.send_inventory_error(@account, @error, @title).deliver_later
      @status = 200
    else
      @status = 400
    end

  end

  def has_token

    @user_portal_provider = UserPortalProvider.joins(:api_provider).where(:uid => params[:uid], :api_providers=>{:name=>"kangalou"}).first

    return render json: {errors: ["Bad user"]}, status: 401 if @user_portal_provider.nil?

  @user = @user_portal_provider.user
    @kangalou_params = ActiveSupport::JSON.decode(@user.kangalou_params) if @user.kangalou_params.present?

    if @kangalou_params.present?
      # Get Call to Kangalou to check tokens number
      # params[:uid]    =>  Uid
      # params[:number] =>  Number tokens
      # Check token - Kangalou

      @callApiHasTokens = has_token_api_request(@user_portal_provider.uid, @user.email, @kangalou_params["encryptedpassword"])


      if ActiveSupport::JSON.decode(@callApiHasTokens.body)["nbTokens"]
        @kangalou_params["nbTokens"] = ActiveSupport::JSON.decode(@callApiHasTokens.body)["nbTokens"]
        if @kangalou_params["nbTokens"].to_i > params[:number].to_i
          @status = 200
        else
          @status = 401
        end
        @user.update_attribute(:kangalou_params, ActiveSupport::JSON.encode(@kangalou_params))
      else
        @status = 400
      end
    else
      @status = 400
    end


  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inventory
      @inventory = Inventory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inventory_params
      params[:inventory].permit(:designation, :property_id)
    end

    def has_token_api_request(uid, email, encryptedpassword)
      @has_token_body =  {
        :member => uid,
        :credentials => {
          :apikey => "a989f817-1dd6-6d92-ec89-4486cd187fk9",
          :email => email,
          :encryptedpassword => encryptedpassword
        }
      }.to_json

      JulesLogger.info "HAs token body"
      JulesLogger.info @has_token_body

      # Get Call to Kangalou to check tokens number
      # params[:uid]    =>  Uid
      # params[:number] =>  Number tokens
      # Check token - Kangalou
      return HTTParty.post("https://staging.kangalou.com/api/has-tokens",:body => @has_token_body)

    end

    def remove_token_api_request(uid, email, encryptedpassword)
      @remove_token_body =  {
        :member => uid,
        :credentials => {
          :apikey => "a989f817-1dd6-6d92-ec89-4486cd187fk9",
          :email => email,
          :encryptedpassword => encryptedpassword
        }
      }.to_json

      JulesLogger.info "Remove token body"
      JulesLogger.info @remove_token_body

      # Get Call to Kangalou to check tokens number
      # Check token - Kangalou
      return HTTParty.post("https://staging.kangalou.com/api/remove-token",:body => @remove_token_body)

    end

end

