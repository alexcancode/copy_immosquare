class Api::V2::SyncController < Api::V2::ApplicationController

  ##============================================================##
  ## Cette méthode sert à la syncrhonisation avec Centris
  ## via api.shareimmo.com pour calculer les annoces à
  ## supprimer
  ##============================================================##
  def centris
    centris = ApiProvider.where(:name => "centris").first
    @mls_id = Property.where(:api_provider_id => centris.id).pluck(:uid)
  end

end
