class UsersController < ApplicationController

  before_filter :authenticate_user!
  layout "back_office"

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to monitoring_users_path
  end

end
