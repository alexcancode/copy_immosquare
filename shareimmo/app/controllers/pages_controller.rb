class PagesController < ApplicationController


  before_filter :set_my_locale


  def home
    if(@my_provider.provider_id == "kangalou" and request.protocol == "https://")
      redirect_to "http://#{request.host_with_port}"
    else
      render "pages/#{@my_provider.provider_id}/home"  if File.exists?("#{Rails.root}/app/views/pages/#{@my_provider.provider_id}/home.html.erb")
    end
  end

  def tour
    begin
      render "pages/#{@my_provider.provider_id}/tour"  if File.exists?("#{Rails.root}/app/views/pages/#{@my_provider.provider_id}/tour.html.erb")
    rescue Exception => e
      redirect_to root_path
    end
  end


  def terms
  end

  def magex
    redirect_to "http://monkangalou.com" if params[:email].blank?
  end


  def docs
    redirect_to "http://docs.shareimmo.com"
  end

  def docs_api
    redirect_to "http://docs.shareimmo.com"
  end



  def blog
    @posts = Post.where(:status=>true,:locale=>I18n.locale,:web_app=>@my_provider.provider_id == "oneclic" ? 2 : 1).order('created_at DESC')
  end

  def blog_show
    @post = Post.find(params[:id])
    unless @post.locale.to_s == I18n.locale.to_s
      raise ActionController::RoutingError.new('Page introuvable')
    end
    unless @post.status == true
      redirect_to blog_path, status: :moved_permanently
    end
  end


  def support_index
    @support_types = SupportType.all
    @support = Support.where(:status=>true,:locale=>I18n.locale).order('created_at DESC')
  end


  def support_type_show
    @support_types = SupportType.all
    @support_type = SupportType.find(params[:id])
    @supports = Support.where(:support_type_id=>@support_type.id,:locale=>I18n.locale,:status=>1)
  end


  def support_show
    @support_types = SupportType.all
    @support = Support.find(params[:id])
    unless @support.locale.to_s == I18n.locale.to_s
      raise ActionController::RoutingError.new('Page introuvable')
    end
    unless @support.status == 1
      redirect_to support_path, status: :moved_permanently
    end
  end






  def short_links
    begin
      property = Property.find(params[:id])
      user = property.user
      website_active = ServicesUser.joins(:service).where(:user_id=>user.id, :services =>{:slug =>"website"}).count
      if website_active == 1
        redirect_to broker_property_url(:protocol=>"http",:subdomain=>user.username,:host=>@my_provider.link_host,:id => property.id)
      else
        redirect_to explore_ad_url(:protocol=>ENV["PROTOCOL"],:subdomain=>nil,:id=>property.id)
      end
    rescue Exception => e
      flash[:danger] = e.message
      redirect_to explore_shareimmo_path
    end
  end


  # Redirect url for external portals
  def original_links
    begin
      property = Property.where(:secure_id => params[:id]).first
      user = property.user
      website_active = ServicesUser.joins(:service).where(:user_id=>user.id, :services =>{:slug =>"website"}).count
      if property.property_url.present?
        redirect_to property.property_url
      elsif website_active == 1
        redirect_to broker_property_url(:protocol=>"http",:subdomain=>user.username,:host=>@my_provider.link_host,:id => property.id)
      else
        redirect_to explore_ad_url(:protocol=>ENV["PROTOCOL"],:subdomain=>nil,:id=>property.id)
      end
    rescue Exception => e
      raise ActionController::RoutingError.new('Not Found')
    end
  end




  def contact
    return redirect_to root_path if @my_provider.provider_id == "kangalou"
  end

  def contact_create
    @contact = Contact.new(contact_params)
    if @contact.save
      ContactMailer.contact(@contact).deliver_later
    end
  end






  def robots
  end


  def sitemap
    ##============================================================##
    ## Statics Pages Management
    ##============================================================##
    all_locales = I18n.available_locales
    @static_urls = Array.new
    all_urls_root = Array.new
    all_urls_tour = Array.new
    all_urls_contact = Array.new
    all_urls_blog = Array.new


    all_locales.each do |my_locale|
      all_urls_root     << [:locale=>my_locale,:hreflang=>my_locale,:url=>root_url(:subdomain=>false,:locale=>my_locale,:protocol => ENV['PROTOCOL'])]
      all_urls_contact  << [:locale=>my_locale,:hreflang=>my_locale,:url=>contact_url(:subdomain=>false,:locale=>my_locale,:protocol => ENV['PROTOCOL'])]
      all_urls_tour     << [:locale=>my_locale,:hreflang=>my_locale,:url=>tour_url(:subdomain=>false,:locale=>my_locale,:protocol => ENV['PROTOCOL'])]
      all_urls_blog     << [:locale=>my_locale,:hreflang=>my_locale,:url=>blog_url(:subdomain=>false,:locale=>my_locale,:protocol => ENV['PROTOCOL'])]
    end


    all_locales.each do |my_locale|
      @static_urls << [:url=>root_url(:locale=>my_locale,:protocol => ENV['PROTOCOL']),:changefreq=>'monthly',:priority=>0.8,:all_urls=>all_urls_root]
      @static_urls << [:url=>tour_url(:locale=>my_locale,:protocol => ENV['PROTOCOL']),:changefreq=>'monthly',:priority=>1,:all_urls=>all_urls_tour]
      @static_urls << [:url=>contact_url(:locale=>my_locale,:protocol => ENV['PROTOCOL']),:changefreq=>'monthly',:priority=>0.5,:all_urls=>all_urls_contact]
      @static_urls << [:url=>blog_url(:locale=>my_locale,:protocol => ENV['PROTOCOL']),:changefreq=>'monthly',:priority=>0.5,:all_urls=>all_urls_blog]
    end



    @posts = Post.where(:status=>true,:web_app=> @my_provider.provider_id == "oneclic" ? 2 : 1)

    respond_to do |format|
      format.xml
    end
  end





  private

  def set_my_locale
    redirect_to url_for(:locale=>:fr), :status => 301 if params[:locale].to_s == "fr-ca"
    redirect_to url_for(:locale => :en), :status => 301 if params[:locale].to_s == "en-ca"
  end


  def contact_params
    params.require(:contact).permit(:email,:first_name,:last_name,:phone,:message)
  end






end

