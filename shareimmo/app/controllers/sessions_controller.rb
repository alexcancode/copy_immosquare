class SessionsController < Devise::SessionsController


  def create
    @user = User.includes(:api_providers).where(:email =>params[:user][:login] ,:api_providers=>{:name=>"kangalou"}).first
    if @user.present? and Rails.env.production?
      @callApi = HTTParty.post("https://management.kangalou.com/fr/membre/api/login-verification",:body => {
        :secretkey => "xouLq83HNWK293I6ak81T24P0GeGjO3x",
        :email => params[:user][:login],
        :password => params[:user][:password]}.to_json
      )
      if(ActiveSupport::JSON.decode(@callApi.body)["uid"] != 0 and @user.services.pluck(:slug).include?("website"))
        sign_in(@user, :store =>true)
        respond_with @user, location: after_sign_in_path_for(@user)
      else
        flash[:danger] = t('devise.failure.invalid')
        redirect_to(:back)
      end
    else
      super
    end
  end



end
