class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  layout :layout_by_resource


  ##============================================================##
  ## BEFORE AND AFTER
  ##============================================================##
  before_filter :set_locale
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :set_website


  def after_sign_in_path_for(resource)
    user = resource
    resource.profile.update_attribute(:country,(request.location.present? and request.location.country_code != "RD") ? request.location.country_code : "FR") if user.sign_in_count == 1
    dashboard_path(resource.username,:locale=>user.profile.default_locale)
  end



  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {
      :locale => I18n.locale,
      :store  => params[:store]
    }
  end


  ##============================================================##
  ## CUSTOM ERRORS PAGE
  ##============================================================##
  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception, with: lambda { |exception| render_error 500, exception }
    rescue_from ActionController::RoutingError, ActionController::UnknownController, ::AbstractController::ActionNotFound, ActiveRecord::RecordNotFound, with: lambda { |exception| render_error 404, exception }
  end





  private


  def set_website
    begin
      domain_with_port = "#{request.domain}#{(request.port.present? and Rails.env.development?) ? ":#{request.port}" : nil}"
      provider = Provider.where("hosts LIKE ?", "%#{domain_with_port}%").first
      if provider.present?
        @my_provider = provider
      else
        hosting = Hosting.find_by_domain(request.domain)
        raise "host not set #{domain_with_port}" if hosting.blank?
        @my_provider = Provider.where(:provider_id => "oneclic").first
      end
    rescue Exception => e
      JulesLogger.info e.message
      @my_provider = Provider.first
    end
  end



  def set_locale
    I18n.locale = params[:locale]
  end





  def render_error(status, exception)
    respond_to do |format|
      format.html { render template: "errors/error_#{status}", layout: 'layouts/error', status: status }
      format.all { render nothing: true, status: status }
    end
  end


  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :email, :password, :password_confirmation, :remember_me])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :username, :email, :password, :remember_me])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username, :email, :password, :password_confirmation, :current_password])
  end



  protected
  def layout_by_resource
    "devise" if devise_controller?
  end



end
