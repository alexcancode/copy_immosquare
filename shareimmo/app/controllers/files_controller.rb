class FilesController < ApplicationController
  skip_before_filter  :verify_authenticity_token

  def show
    @errors = []

    if params[:inventory]
      case params[:type]
        when "inventory"
          @object = Inventory.find_by_secure_id(params[:secure_id])
          if @object.secure_password == params[:inventory][:secure_password]
            @object.secure_token = Digest::SHA1.hexdigest([Time.now, @object.id, @object.designation].join)
            @object.secure_token_expiration = Time.now + 5.minutes
            @object.save(:validate => false)
            redirect_to :action => :download, :secure_id => params[:secure_id], :secure_token => @object.secure_token
          else
            @errors << "Bad password"
          end
      end
    else
      case params[:type]
        when "inventory"
          @object = Inventory.find_by_secure_id(params[:secure_id])
      end
      if @object.nil?
        raise ActiveRecord::RecordNotFound, "Record not found."
      end
    end

  end

  def download

    @object = Inventory.where(:secure_id => params[:secure_id])
    if @object.first.present? && @object.first.secure_token == params[:secure_token]
      @object = @object.first
      if @object.secure_token_expiration.present? && @object.secure_token_expiration > Time.now
        respond_to do |format|
          format.html
          format.pdf do
            data = {
              :inventory => @object,
              :company => @my_provider.name
            }
            builder = InventoryBuilder.new(data)
            send_data builder.build, filename: builder.filename,type: "application/pdf",disposition: "inline"
          end
        end
      else
        redirect_to :action => :show, :secure_id => params[:secure_id], :secure_token => params[:secure_token]
      end
    elsif @object.first.present?
        redirect_to :action => :show, :secure_id => params[:secure_id], :secure_token => params[:secure_token]
    else
      raise ActiveRecord::RecordNotFound, "Record not found."
    end

  end

end
