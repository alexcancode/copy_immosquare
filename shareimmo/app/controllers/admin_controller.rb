class AdminController < ApplicationController

  layout "back_office"

  ##============================================================##
  ## Before Filters
  ##============================================================##
  force_ssl(:subdomain=>"www") if Rails.env.production?
  before_filter :authenticate_user!
  before_filter :check_ability
  before_filter :is_only_for_admin, :except => [:monitoring,:managers,:users,:in_business,:users_add,:users_services,:on_youtube,:regen_youtube]


  ##============================================================##
  ## Monitoring
  ##============================================================##
  def monitoring
  end


  ##============================================================##
  ##
  ##============================================================##
  def pdf
    @pdf = Pdf.first_or_create
  end

  def pdf_upload
    @pdf = Pdf.find(params[:id])
    @pdf.update_attributes(pdf_params)
    locale_pdf  = "#{Rails.root}/tmp/pdfs/original/#{@pdf.document_file_name}"
    FileUtils.mkdir_p(File.dirname(locale_pdf))  unless File.exists?(File.dirname(locale_pdf))
    open(locale_pdf, 'wb') do |file|
      file.write HTTParty.get(@pdf.document.url).parsed_response
    end
    dictionary = Mypdflib::PdfParser.new(:pdf=>locale_pdf).parse
    @pdf.update_attribute(:dictionary, dictionary)
    redirect_to admin_pdf_path
  end

  ##============================================================##
  ## Business
  ##============================================================##
  def in_business
    begin
      if current_user.has_role? :admin
        @visibility_portals = VisibilityPortal.where("visibility_until > ?", Time.now).where(:posting_status => 1).order(:visibility_start => :desc,:created_at => :desc) if params[:api].blank?
        @visibility_portals = VisibilityPortal.joins(:property).where(:properties=>{:api_provider_id =>params[:api], :status => 1}).where("visibility_until > ?", Time.now).where(:posting_status => 1).order(:visibility_start => :desc,:created_at => :desc) if params[:api].present?
      else
        @visibility_portals = VisibilityPortal.joins(:property).where(:properties=>{:api_provider_id => current_user.can_manage_api_id, :status => 1}).where("visibility_until > ?", Time.now).where(:posting_status => 1).order(:visibility_start => :desc,:created_at => :desc)
      end
      if params[:search].present?
        @visibility_portals = @visibility_portals.reject{|v| v.property.uid2 != params[:search][:uid2].to_i }  if params[:search][:uid2].present?
        @visibility_portals = @visibility_portals.reject{|v| v.property.uid  != params[:search][:uid].to_i }   if params[:search][:uid].present?
        @visibility_portals = @visibility_portals.reject{|v| v.property.id   != params[:search][:id].to_i }    if params[:search][:id].present?
      end
      @googles = Authentification.joins(:api_provider).where(:is_account_for_video_worker => 1,:api_providers => { :name =>'google_oauth2'})
    rescue
      @properties  = []
    end
  end

  def on_youtube
   if current_user.has_role? :admin
    @videos = Video.where("visibility_until > ?", Time.now).where(:posting_status => 1).order(:visibility_start => :desc,:created_at => :desc) if params[:api].blank?
    @videos = Video.joins(:property).where(:properties=>{:api_provider_id =>params[:api], :status => 1}).where("visibility_until > ?", Time.now).where(:posting_status => 1).order(:visibility_start => :desc,:created_at => :desc) if params[:api].present?
  else
    @videos = Video.joins(:property).where(:properties=>{:api_provider_id => current_user.can_manage_api_id, :status => 1}).where("visibility_until > ?", Time.now).where(:posting_status => 1).order(:visibility_start => :desc,:created_at => :desc)
  end
end


  ##============================================================##
  ## Users Management
  ##============================================================##
  def users
    begin
      @app = ApiApp.where(:api_provider_id =>current_user.can_manage_api_id).first
      if params[:search].present?
        @users = User.where('users.email LIKE ?', "%#{params[:search][:email]}%").reverse_order.paginate(:page => params[:page], :per_page => 50)  if(params[:search][:email].present? and current_user.has_role?(:admin))
        @users = User.where('users.email LIKE ?', "%#{params[:search][:email]}%").includes(:api_providers).where(:api_providers => {:id => current_user.can_manage_api_id}).reverse_order.paginate(:page => params[:page], :per_page => 50) if(params[:search][:email].present? and current_user.has_role?(:manager))
      else
        @users = User.all.reverse_order.paginate(:page => params[:page], :per_page => 50) if current_user.has_role? :admin
        @users = User.includes(:api_providers).where(:api_providers => {:id => current_user.can_manage_api_id}).reverse_order.paginate(:page => params[:page], :per_page => 50) if current_user.has_role? :manager
      end
    rescue
      @users  = []
    end
  end

  def managers
    @users = User.with_role(:manager) if current_user.has_role? :admin
    @users = User.with_role(:manager).where(:can_manage_api_id =>current_user.can_manage_api_id) if current_user.has_role? :manager
  end


  ##============================================================##
  ## Services
  ##============================================================##
  def users_services
    @services_users = ServicesUser.all.order(:user_id)    if(current_user.has_role?(:admin))
    @services_users = ServicesUser.includes(user: [:api_providers]).order(:user_id).where(:api_providers => {:id => current_user.can_manage_api_id}) if(current_user.has_role?(:manager))
  end


  ##============================================================##
  ## Manage Services
  ##============================================================##
  def users_services_manage
    @my_user = User.find(params[:id])
    @services = Service.all
  end


  def users_services_update
    @service_user = ServicesUser.new(service_user_params)
    @service_user.save
  end

  def users_services_delete
    @service_user = ServicesUser.find(params[:id])
    @service_user.destroy
    render 'users_services_update'
  end





  def stats_properties
  end

  def stats_users
  end

  def stats_visibilites
  end





  def mails
  end



  def send_mail
    redirect_to admin_mails_path
  end



  def regen_youtube
    property  = Property.find(params[:id])
    visi      = VisibilityPortal.where(:property_id=>property.id).last
    video     = property.videos.last
    video.update_attributes(:posting_status=>0) if video.present?
    VideoGeneratorWorker.perform_async(
      :property_id        => property.id,
      :youtube_info_url   => api_v2_youtube_infos_url(params[:id]),
      :channel            => property.user_id,
      :visibility_start   => video.present? ? video.visibility_start : (visi.present? ? visi.visibility_start : Time.now),
      :visibility_until   => video.present? ? video.visibility_until : (visi.present? ? visi.visibility_until : Time.now+1.month)
      )
    flash[:notice] = "Regeneration Start"
    redirect_to :back
  end



  def scrapping_monitoring
    @scraps = ScrappingMonitoring.all
  end


  ##============================================================##
  ## Pots
  ##============================================================##
  def blog
    @posts= Post.all
  end

  def post_new
    @post = Post.where(:draft => 1).first_or_create
    redirect_to post_edit_path(@post.id)
  end

  def post_edit
    @post = Post.find(params[:id])
  end

  def post_update
    @post = Post.find(params[:id])
    @post.update(clean_params_post)
    redirect_to blog_admin_path
  end

  def post_destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to blog_admin_path
  end


  ##============================================================##
  ## Supports
  ##============================================================##
  def supports
    @supports= Support.all
  end


  def support_new
    @support = Support.where(:draft=>1).first_or_create
    redirect_to support_edit_path(@support.id)
  end

  def support_edit
    @support = Support.find(params[:id])
  end


  def support_update
    @support = Support.find(params[:id])
    @support.update(clean_params_support)
    redirect_to support_admin_path
  end

  def support_destroy
    @support = Support.find(params[:id])
    @support.destroy
    redirect_to support_admin_path
  end






  private

  def check_ability
    @user = current_user
    redirect_to dashboard_path(current_user.username,:subdomain=>false) unless current_user.has_any_role? :admin, :manager
  end

  def is_only_for_admin
    redirect_to dashboard_path(current_user.username,:subdomain=>false) unless current_user.has_role? :admin
  end

  def clean_params_post
    params.require(:post).permit!
  end

  def clean_params_support
    params.require(:support).permit!
  end

  def service_user_params
    params.require(:services_user).permit!
  end

  def pdf_params
    params.require(:pdf).permit!
  end



end
