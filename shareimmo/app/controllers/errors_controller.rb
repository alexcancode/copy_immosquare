class ErrorsController < ApplicationController
  def error_404
    @not_found_path = params[:not_found]
    render layout: "error",  status: :not_found
  end

  def error_500
    render layout: "error", status: :internal_server_error
  end
end
