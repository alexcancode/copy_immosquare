class NomenclatureElementsController < ApplicationController

  def create
    @nomenclature_element = NomenclatureElement.new(clean_params)

    if params[:parent_id].to_i > 0
      @nomenclature_element.parent_id = params[:parent_id]
    end

    if @nomenclature_element.save
      redirect_to dashboard_inventories_nomenclature_show_path(:id => @nomenclature_element.nomenclature_id)
    end
    Rails.logger.warn @nomenclature_element.errors.to_json
  end

  def duplicate
    @nomenclature = Nomenclature.find(params[:id])
    @nomenclature_elements = @nomenclature.nomenclature_elements

    @nomenclature_new = Nomenclature.create(@nomenclature.attributes.merge(:name => "#{@nomenclature.name} - duplicate", :id => nil, :status =>3))

    current_user.nomenclatures << @nomenclature_new

    @nomenclature.nomenclature_elements.each do |e|
      @element_new = NomenclatureElement.create(e.attributes.merge(:id => nil))
      @nomenclature_new.nomenclature_elements << @element_new
    end
    @nomenclature_new.nomenclature_elements.each do |e|
      unless e.parent_id.nil?
        e.update_attributes(:parent_id => NomenclatureElement.where(:name => e.parent.name, :nomenclature_id => @nomenclature_new.id).first.id)
      end
    end

  end


  private

  def clean_params
    params.require(:nomenclature_element).permit(
      :name,
      :value,
      :description,
      :element_type,
      :nomenclature_id,
      :status
      )
  end

end