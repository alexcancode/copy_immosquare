class ToolsController < ApplicationController

  include HtmlToPlainTextHelper
  include MathHelper

  def index
  end

  def htmltotext
    @support_types = SupportType.all
  end


  def htmltotext_parser
    @new_text = convert_to_text(params[:text][:my_text])
  end


  def geocoder
  end

  def geocoder_find
    if params[:finder][:place_id].present?
      place_id        = params[:finder][:place_id]
      googleClient    = GooglePlaces::Client.new(ENV["GOOGLE_API_KEY"])
      myCenter        = googleClient.spot(params[:finder][:place_id])
      mode            = "walking"

      ##============================================================##
      ## Bus & Subways
      ##============================================================##
      if params[:finder][:type] == "bus_and_subways"
        title   = "Bus & Subways"
        types   = [
          { :name => "Subways", :type => ["subway_station"],:radius => 2500, :order =>1},
          { :name => "Bus",     :type => ["bus_station"],   :radius => 500, :order => 2},
        ]
      end

      ##============================================================##
      ## Train Stations
      ##============================================================##
      if params[:finder][:type] == "train_stations"
        title   = "Trains"
        types   = [
          { :name => "Trains",     :type => "train_station",   :radius => 5000, :order => 1},
        ]
      end

      ##============================================================##
      ## Schools
      ##============================================================##
      if params[:finder][:type] == "schools_and_universities"
        title   = "Schools & universities"
        types   = [
          { :name => "university",  :type => ["university"],  :radius => 200, :order => 2},
          { :name => "school",      :type => ["school"],      :radius => 1500, :order => 1},
        ]
      end


      ##============================================================##
      ## Call
      ##============================================================##
      a1 = Array.new
      a2 = Array.new
      types.each do |type|
        googleClient.spots(myCenter.lat,myCenter.lng,:multipage => true,:radius=>type[:radius], :types=>type[:type]).each do |place|
          a1 << {
            :name           => place.name,
            :type           => type[:name],
            :distance       => haversine(myCenter.lat,myCenter.lng,place.lat,place.lng).ceil,
            :place_id       => place.place_id
          }
        end
      end


      a1.uniq{|x| x[:name]}.group_by{|x| x[:type]}.each do |type,r|
        r.sort_by{ |x| x[:distance]}.each_with_index do |rr,i|
          if i <= 2
            call  = HTTParty.get("https://maps.googleapis.com/maps/api/distancematrix/json?origins=place_id:#{place_id}&destinations=place_id:#{rr[:place_id]}&mode=#{mode}&language=#{I18n.locale}&key=#{ENV['GOOGLE_API_KEY']}")
            call  = JSON.parse(call.body)["rows"][0]["elements"]
            dist  = call[0]["distance"]["text"]
            time  = call[0]["duration"]["text"]
          else
            dist  = nil
            time  = nil
          end
          a2 << {
            :name           => rr[:name],
            :type           => rr[:type],
            :distance       => rr[:distance],
            :place_id       => rr[:place_id],
            :distance_real  => dist,
            :duration       => time
          }
        end
        @result = a2
      end
    end





  end

end
