class BackOfficeController < ApplicationController
  include EncryptHelper
  require 'whois'
  require 'uri'


  skip_before_filter :verify_authenticity_token, :only => [:wysiwyg_picture_manager,:services_display]
  force_ssl(:subdomain=>"www") unless Rails.env.development?

  ##============================================================##
  ## Before Filters
  ##============================================================##
  before_filter :authenticate_user!
  before_filter :remove_subdomain
  before_filter :check_apps, :except =>[:dashboard_redirect]
  before_filter :set_store_menu





  def dashboard_redirect
    redirect_to dashboard_path(current_user.username)
  end

  def dashboard
    redirect_to dashboard_path(@user.username,:online => 1) if params[:online].blank?
    params.delete :listing if params[:listing].blank?
    @properties   = Property.where(
      :user_id=>@user.id,
      :property_listing_id => params[:listing].present? ? params[:listing] : PropertyListing.all.pluck(:id),
      :status => params[:online].present? ? params[:online] : [0,1])
    .where.not(:draft=>1).paginate(:page => params[:page], :per_page => 15).reverse_order
  end


  def apps
    if params[:t].present?
      @portal_type = PortalType.where(:slug => params[:t]).first
      @apps = @portal_type.portals.where(:status => 1).group_by(&:country)
      ##============================================================##
      ## Socials Networks
      ##============================================================##
      if @portal_type.slug == "socials_networks"
        @twitter   = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'twitter'}).last
        @facebook  = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'facebook'}).last
        @instagram = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'instagram'}).last
        @pinterest = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'pinterest'}).last
        @google    = Authentification.joins(:api_provider).where(:is_account_for_video_worker => 0, :user_id=>@user.id,:api_providers => { :name =>'google_oauth2'}).last
      end
    end
    ##============================================================##
    ## Wesbite
    ##============================================================##
    if params[:website].present?
      @profile          = @user.profile
      @themes           = Theme.all
      @selected_theme   = @user.theme_settings.where(:is_selected=>1).pluck(:theme_id).first
      @hostings         = Hosting.where(:user_id=>@user.id)
    end
  end

  def portal
    @portal = Portal.find(params[:id])
    redirect_to dashboard_path(@user.username,:portal=>@portal.id)
  end

  def portal_update
    @activation = PortalActivation.find(params[:id])
    @activation.update_attributes(portal_activation_params)
    redirect_to dashboard_apps_path(@user.username,:t => @activation.portal.portal_type.slug)
  end

  def portal_delete
    @activation = PortalActivation.find(params[:id])
    @activation.destroy
    redirect_to dashboard_apps_path(@user.username,:t => @activation.portal.portal_type.slug)
  end



  def ads_edl
    @properties = params[:building].present? ? Property.where(:user_id=>@user.id,:building_id=>params[:building]).where.not(:draft=>1) : Property.where(:user_id=>@user.id).where.not(:draft=>1)
  end

  ##============================================================##
  ## International
  ##============================================================##
  def international
    @listglobally_link = HTTParty.get("https://identityserver.portia.ch:33333/api/v1/onbehalfof?apikey=#{ENV['ListGlobally_api_key']}&accountid=#{Rails.env.production? ? @user.id : 1 }")
    @id = params[:id]
  end

  ##============================================================##
  ## Service Socials Networks
  ##============================================================##
  def share_facebook_step1
    @property = Property.find(params[:id])
    @facebook = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'facebook'}).last
    graph = Koala::Facebook::API.new(@facebook.token)
    @pages = graph.get_connections("me","accounts")
    @share = ShareSocial.new
  end

  def share_facebook_step2
    pages       = params[:share_social]['facebook_pages_id']
    @share      = ShareSocial.new(share_social_params)
    @message    = @share.note
    @share.note = {:pages =>pages, :message => @share.note}.to_json
    @share.save
    FacebookPoster.new(
      :property_id  => params[:id],
      :myself       => params[:share_social]['myself'] == "1",
      :message      => @message,
      :pages        => pages,
      :host_url     => @my_provider.link_host,
      :locale       => I18n.locale
      ).post_on_facebook
    redirect_to dashboard_services_path(@user.username,:service=>"socials_networks")
  end


  def share_twitter_step1
    @property = Property.find(params[:id])
    @twitter  = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'twitter'}).last
    @share    = ShareSocial.new
  end


  def share_twitter_step2
    @share = ShareSocial.new(share_social_params)
    @tweet = @share.note
    @share.note = {:tweet => @share.note}.to_json
    @share.save
    TwitterPoster.new(
      :property_id => params[:id],
      :message     => @tweet
      ).post_on_twitter
    redirect_to dashboard_services_path(@user.username,:service=>"socials_networks")
  end


  def share_youtube_step1
    VideoGeneratorWorker.perform_async(
      :property_id      => params[:id],
      :youtube_info_url => api_v2_youtube_infos_url(params[:id]),
      :channel          => @user.id
      )
  end





  ##============================================================##
  ## MENU PROPRIÉTÉS
  ## ===============
  ##============================================================##
  def property_new
    @property = Property.where(:user_id=>@user.id,:draft=>1).first_or_create
    @property.update_attributes(
      :secure_id =>SecureRandom.urlsafe_base64,
      :api_provider_id => ApiProvider.where(:name=>"shareimmo").select(:id).first.id
      )
    redirect_to dashboard_property_edit_path(@user.username, @property.secure_id)
  end


  def property_edit
    @property   = Property.where(:user_id=>@user.id,:secure_id=>params[:secure_id]).first
    @buildings  = Building.where(:user_id=>@user.id)
    @profile    = @user.profile
    @currencies = Property.shareimmo_currency
    @inclusion  = PropertyInclusion.where(:property_id => @property.id).first_or_create
    @complement = PropertyComplement.where(:property_id => @property.id).first_or_create
    ##============================================================##
    ## Geocode
    ##============================================================##
    if @property.latitude.present? and @property.longitude.present?
      @lat = @property.latitude
      @lng = @property.longitude
    else
      @lat = (request.location.present? and request.location.latitude  != 0) ? request.location.latitude : 45.5053515
      @lng = (request.location.present? and request.location.longitude != 0) ? request.location.longitude : -73.567758
    end
    ##============================================================##
    ## Property Contacts
    ##============================================================##
    @property_contacts =  Array.new
    @user.property_contacts.each do |c|
      @property_contacts << {
        :id      => c.id,
        :value   => "#{c.first_name} #{c.last_name} #{c.email} - #{c.property_contact_type.name}"
      }
    end
  end


  def property_update
    @property = Property.where(:user_id=>@user.id,:secure_id=>params[:secure_id]).first
    @property.update_attributes(properties_params)
    respond_to do |format|
      format.html do
        redirect_to dashboard_path(@user.username)
      end
      format.js do
        flash[:success] = "Property was saved successfully" if @property.errors.blank?
        flash[:danger] = @property.errors.full_messages if @property.errors.any?
        flash.discard
        render 'flash_message'
      end
    end
  end


  def property_delete
    @property = Property.where(:user_id=>@user.id,:secure_id=>params[:secure_id]).first
    BuildingPropertyDeleteWorker.perform_async(
      :model => "property",
      :id    => @property.id
      )
  end



  ##============================================================##
  ## Property Contact
  ##============================================================##
  def property_contacts
    @property_contacts = @user.property_contacts
  end

  def property_contact_new
    @property_contact = PropertyContact.new
  end

  def property_contact_create
    property_contact = PropertyContact.new(property_contact_params)
    property_contact.save
  end

  def property_contact_edit
    @property_contact = PropertyContact.find(params[:id])
    render 'property_contact_new'
  end

  def property_contact_update
    property_contact = PropertyContact.find(params[:id])
    property_contact.update_attributes(property_contact_params)
    render 'property_contact_create'
  end

  def property_contact_delete
    property_contact = PropertyContact.find(params[:id])
    property_contact.destroy
    render 'property_contact_create'
  end

  def property_contact_property_ad
    contact   = PropertyContact.find(params[:property_contact][:property_contact_id])
    @property = Property.find(params[:id])
    @property.property_contacts << contact if contact.present? and !@property.property_contacts.pluck(:id).include?(contact.id)
    render 'property_contact_create'
  end

  def property_contact_property_delete
    @property = Property.find(params[:id])
    @property.property_contacts.delete(params[:contact_id])
    @property_contacts = @property.property_contacts
    render 'property_contact_create'
  end

  ##============================================================##
  ## Documents
  ##============================================================##
  def property_document_new
    @document = Document.where(:property_id => params[:id],:draft =>1).first_or_create
  end

  def property_document_edit
    @document = Document.find(params[:id])
    render 'property_document_new'
  end

  def property_document_update
    @document = Document.find(params[:id])
    @document.update_attributes(document_attributes)
    @property = @document.property
  end

  def property_document_delete
    @document = Document.find(params[:id])
    @document.destroy
    @property = @document.property
    render 'property_document_update'
  end


  ##============================================================##
  ## Inclusions
  ##============================================================##
  def property_inclusion_update
    @inclusion = PropertyInclusion.find(params[:id])
    @inclusion.update_attributes(inclusions_params)
    render nothing: true
  end


  ##============================================================##
  ## Def Buildings
  ##============================================================##
  def buildings
    @buildings = Building.where(:user_id=>@user.id,:draft => 0)
  end


  def building_new
    @building = Building.where(:user_id=>@user.id,:draft=>1).first_or_create
    @building.update_attribute(:secure_id,SecureRandom.urlsafe_base64)
    @building.update_attribute(:api_provider_id,ApiProvider.where(:name=>"shareimmo").select(:id).first.id)
    redirect_to dashboard_edit_building_path(@user.username, @building.secure_id)
  end

  def building_edit
    @building = Building.where(:secure_id => params[:secure_id],:user_id => @user.id).first
     ##============================================================##
    ## Geocode
    ##============================================================##
    if @building.latitude.present? and @building.longitude.present?
      @lat = @building.latitude
      @lng = @building.longitude
    else
      @lat = (request.location.present? and request.location.latitude  != 0) ? request.location.latitude : 45.5053515
      @lng = (request.location.present? and request.location.longitude != 0) ? request.location.longitude : -73.567758
    end
  end

  def building_update
    @building = Building.where(:secure_id => params[:secure_id],:user_id => @user.id).first
    @building.update_attributes(building_params)
    respond_to do |format|
      format.html do
        redirect_to dashboard_buildings_path(@user.username)
      end
      format.js do
        flash[:success] = "Building was saved successfully" if @building.errors.blank?
        flash[:danger] = @building.errors.full_messages if @building.errors.any?
        flash.discard
        render 'flash_message'
      end
    end
  end

  def building_delete
    @building = Building.where(:secure_id => params[:secure_id],:user_id => @user.id).first
    @building.destroy
    redirect_to dashboard_buildings_path(@user.username)
  end

  def building_new_ad_from_buiding
    @building = Building.where(:secure_id => params[:secure_id],:user_id => @user.id).first
    @property = Property.where(:user_id=>@user.id,:draft=>1).first_or_create
    @property.property_assets.destroy_all
    @property.update_attributes(
      :api_provider_id => ApiProvider.where(:name=>"shareimmo").select(:id).first.id,
      :secure_id => SecureRandom.urlsafe_base64,
      :building_id => @building.id,
      :address => @building.address,
      :latitude => @building.latitude,
      :longitude => @building.longitude,
      :street_number => @building.street_number,
      :street_name =>  @building.street_name,
      :zipcode => @building.zipcode,
      :locality => @building.locality,
      :administrative_area_level_1 => @building.administrative_area_level_1,
      :country_short => @building.country_short,
      :sublocality => @building.sublocality,
      :title_fr => @building.name,
      :title_en => @building.name,
      :title_de => @building.name,
      :title_nl => @building.name,
      :title_it => @building.name,
      :title_es => @building.name,
      :title_pt => @building.name,
      :description_fr => @building.description_fr,
      :description_en => @building.description_en,
      :description_de => @building.description_de,
      :description_nl => @building.description_nl,
      :description_it => @building.description_it,
      :description_es => @building.description_es,
      :description_pt => @building.description_pt
      )
@building.building_assets.each do |asset|
  @property.property_assets.new(:picture => asset.picture.url).save
end
redirect_to dashboard_property_edit_path(@user.username,@property.secure_id)
end


  ##============================================================##
  ## Feedbacks
  ##============================================================##
  def feedback_manage
    @property = Property.where(:user_id=>@user.id,:secure_id=>params[:secure_id]).first
    @feedbacks = @property.feedbacks.where(:draft => 0).reverse_order
  end

  def feedback_new
    @property = Property.where(:user_id=>@user.id,:secure_id=>params[:secure_id]).first
    @feedback = Feedback.where(:property_id=>@property.id,:draft =>1).first_or_create
  end

  def feedback_edit
    @feedback = Feedback.find(params[:id])
  end

  def feedback_update
    params[:feedback][:visit_at] = "#{params[:feedback][:day]} #{params[:feedback][:hour].present? ? params[:feedback][:hour] : "12:00:00"}" if params[:feedback][:day].present?
    @feedback = Feedback.find(params[:id])
    @feedback.update_attributes(feedback_params)
    @feedbacks = Feedback.where(:draft => 0,:property_id => @feedback.property_id).reverse_order
  end

  def feedback_visiting
    @feedback = Feedback.find(params[:id])
  end

  def feedback_destroy
    @feedback = Feedback.find(params[:id])
    @feedback.destroy
  end

  def feedback_report_new
    @feedback = Feedback.find(params[:id])
    @report = FeedbackReport.where(:feedback_id=>@feedback.id,:is_sent=>0).first_or_create
  end

  def feedback_report_send
    @report = FeedbackReport.find(params[:id])
    @report.update_attributes(report_params)
    if JSON.parse(@report.contact_1)['send'] == true
      ReportMailer.notification_to_prospect(@report).deliver_later
    end
    if JSON.parse(@report.contact_2)['send'] == true
      ReportMailer.notification_to_owner(@report).deliver_later
    end
    if JSON.parse(@report.contact_3)['send'] == true
      ReportMailer.notification_to_myself(@report).deliver_later
    end
    redirect_to dashboard_manage_feedback_path(@user.username,@report.feedback.property.secure_id)
  end




  ##============================================================##
  ## Visibility
  ##============================================================##
  def visibility_manage
    @property  = Property.where(:user_id => @user.id, :id=> params[:id]).first
    if params[:international].present?
      @portals = Portal.where(:country => "int", :status => 1)
    else
      @portals = Portal.where(:country => @user.profile.country_alpha2, :status => 1)
    end
    @my_vps = Array.new
    @portals.each do |p|
      vp = VisibilityPortal.where(:property_id=> @property.id,:portal_id =>p.id).last
      @my_vps << {:portal => p, :vp => vp.present? ? vp : VisibilityPortal.new}
    end
  end


  ##============================================================##
  ## Visibility Update
  ##============================================================##
  def visibility_update_portal
    @vp = VisibilityPortal.where(:property_id => visibility_portal_params["property_id"], :portal_id => visibility_portal_params["portal_id"]).first_or_create
    @vp.update_attributes(visibility_portal_params)
    @count = VisibilityPortal.joins(:portal).where(:portals => {:country =>@vp.portal.country,:status => 1}).where(:property_id=> @vp.property_id,:posting_status=>1).where("visibility_until > ?", Time.now).count
  end


  ##============================================================##
  ##
  ##============================================================##
  def visibility_show
    @property = Property.where(:user_id => @user.id, :id=> params[:id]).first
    @vps = VisibilityPortal.joins(:portal).where("visibility_until > ?", Time.now).where(
      :property_id    => @property.id,
      :posting_status => 1,
      :portals=>{:status =>1,:country => params[:international].present? ? "int" : @user.profile.country_alpha2}
      )
  end


  ##============================================================##
  ## Inventories
  ##============================================================##
  def inventory_manage
    @xml = Immopad.new(params[:id])
    @xml.create_xml_and_send_it_to_immopad
    @status = @xml.status
  end

  def inventories
    @inventories = params[:unit].present? ? Inventory.where(:user_id=>@user.id,:property_id=>params[:unit], :status => 1) : Inventory.where(:user_id=>@user.id, :status => 1)
  end

  def inventories_parameters
    @profile = @user.profile
    @parameter =  InventoryParameter.where(:user_id=>@user.id).first_or_create
  end

  def inventories_parameters_update
    @parameter = @user.inventory_parameter
    @parameter.update_attributes(inventory_parameter_params)
  end

  def inventories_new
    @inventory = Inventory.new
  end

  def inventories_show
    @inventory = Inventory.find(params[:id])
    respond_to do |format|
      format.pdf do
        data = {
          :inventory => @inventory,
          :company => @my_provider.name
        }
        builder = InventoryBuilder.new(data)
        send_data builder.build, filename: builder.filename,type: "application/pdf",disposition: "inline"
      end
    end
  end

  def inventories_create
    @inventory = Inventory.new(inventory_params)
    @inventory.save
    @inventories = Inventory.where(:user_id=>@user.id)
  end

  def inventories_destroy
    @inventory = Inventory.find(params[:id])
    Rails.logger.warn @inventory[:status]
    @inventory.status = 0
    @inventory.save(:validate => false)
    redirect_to dashboard_inventories_path()
  end

  def inventories_glossary
    @glossary = Glossary.where(:user_id=>@user.id).first_or_create
  end

  def inventories_glossary_new
    @glossary_element = GlossaryElement.new(glossary_element_params)
    @glossary_element.save
    @glossary = @glossary_element.glossary
  end

  def inventories_glossary_delete
    @glossary_element = GlossaryElement.find(params[:id])
    @glossary_element.destroy
  end

  def inventories_glossary_edit
    @glossary_element = GlossaryElement.find(params[:id])
  end

  def inventories_glossary_update
    @glossary_element = GlossaryElement.find(params[:id])
    @glossary_element.update_attributes(glossary_element_params)
    @glossary = @glossary_element.glossary
    render 'inventories_glossary_new'
  end

  def inventories_nomenclatures
    @nomenclatures = NomenclatureDefault.where("id NOT IN (?)", [9999999999]) +  Nomenclature.where(:user_id=>@user.id)
  end


  def inventories_nomenclature_new
    @nomenclature = Nomenclature.new(nomenclature_params)
    @nomenclature.save
    redirect_to dashboard_nomenclatures_path(@user.username)
  end

  def inventories_nomenclature_import
    @import = Nomenclature.import(params[:nomenclature].present? ? params[:nomenclature][:file] : nil ,@user.id)
    if @import.blank?
      flash[:danger] = t('errors.messages.invalid_xml')
    end
    redirect_to dashboard_nomenclatures_path(@user.username)
  end


  def inventories_nomenclature_delete
    @nomenclature = Nomenclature.find(params[:id])
    @nomenclature.nomenclature_elements.roots.destroy_all
    @nomenclature.destroy
    redirect_to dashboard_nomenclatures_path(@user.username)
  end

  def inventories_nomenclature_edit
    @nomenclature = Nomenclature.find(params[:id])
  end

  def inventories_nomenclature_update
    @nomenclature = Nomenclature.find(params[:id])
    @nomenclature.update_attributes(nomenclature_params)
    redirect_to dashboard_nomenclatures_path(@user.username)
  end

  def inventories_nomenclature_show
    @nomenclature = Nomenclature.find(params[:id])
    respond_to do |format|
      format.html do
        @my_list = [["none", nil]]
        @nomenclature.nomenclature_elements.where(:ancestry=>nil).order(:position).each do |node|
          @my_list.push ["*#{node.name}",node.id]
          make_nomenclature_options(node.descendants.arrange(:order => :position),@my_list)
        end
      end
      format.csv do
        send_data @nomenclature.to_csv(:col_sep => "\t"),
        :disposition => "attachment; filename=\"#{@nomenclature.name.gsub(/\s+/, "_").downcase}_nomenclature.csv\"",
        :type => 'text/csv'
      end
    end
  end


  def inventories_nomenclature_default_show
    @nomenclature = NomenclatureDefault.find(params[:id])
    render 'inventories_nomenclature_show'
  end


  def inventories_nomenclature_element_new
    @nomenclature_element = NomenclatureElement.new(nomenclature_element_params)
    @nomenclature_element.save
    @nomenclature = @nomenclature_element.nomenclature
    @my_list = [["none", nil]]
    @nomenclature.nomenclature_elements.where(:ancestry=>nil).order(:position).each do |node|
      @my_list.push ["*#{node.name}",node.id]
      make_nomenclature_options(node.descendants.arrange(:order => :position),@my_list)
    end
  end

  def inventories_nomenclature_element_sort
    @nomenclature_element = NomenclatureElement.find(params[:id])
    @nomenclature = @nomenclature_element.nomenclature
    if params[:direction] == "up"
      @nomenclature_element.move_higher
    elsif params[:direction] == "down"
      @nomenclature_element.move_lower
    end
    if params[:nomenclature_element].present?
      @nomenclature_element.update_attributes(nomenclature_element_params)
      @nomenclature.nomenclature_elements.where(:ancestry=>nil).where.not(:id=>@nomenclature_element.id).update_all(:is_the_standard_element=>0) if @nomenclature_element.is_the_standard_element == 1
    end

    @my_list = [["none", nil]]
    @nomenclature.nomenclature_elements.where(:ancestry=>nil).order(:position).each do |node|
      @my_list.push ["*#{node.name}",node.id]
      make_nomenclature_options(node.descendants.arrange(:order => :position),@my_list)
    end
    render "inventories_nomenclature_element_new"
  end

  def inventories_nomenclature_element_edit
    @nomenclature_element = NomenclatureElement.find(params[:id])
  end


  def inventories_building_property
    @property = Property.find(params[:property_id])
  end




  ##============================================================##
  ## Assets Property : CREATE + DELETE + SORT + UPDATE
  ##============================================================##
  def property_asset_create
    @property = Property.where(:user_id=>@user.id,:secure_id=>params[:secure_id]).first
    @property_asset = PropertyAsset.create(property_assets_params)
  end


  def property_asset_delete
    @asset = PropertyAsset.find(params[:asset_id])
    @asset.destroy
  end

  def property_assets_sort
    params[:my_order].each_with_index do |id, index|
      @asset = PropertyAsset.find(id)
      @asset.update_columns(:position =>index+1)
      @asset.save
    end
    render nothing: true
  end

  def property_videos_sort
    params[:my_order].each_with_index do |id, index|
      @property_video =PropertyAsset.find(id)
      @property_video.update_columns(:position =>index+1)
      @property_video.save
    end
    render nothing: true
  end




  ##============================================================##
  ## Assets Building : CREATE + DELETE + SORT + UPDATE
  ##============================================================##
  def create_building_asset
    @building = Building.where(:user_id=>@user.id,:secure_id=>params[:secure_id]).first
    @building_asset = BuildingAsset.create(building_assets_params)
  end


  def update_building_asset
    @asset = BuildingAsset.find(params[:asset_id])
    @asset.update_columns(:room_id=>params[:room_id])
    render :json => {:status =>true, :asset =>@asset}
  end


  def delete_building_asset
    @asset = BuildingAsset.find(params[:asset_id])
    @asset.destroy
  end

  def building_assets_sort
    params[:my_order].each_with_index do |id, index|
      @asset = BuildingAsset.find(id)
      @asset.update_columns(:position =>index+1)
      @asset.save
    end
    render nothing: true
  end



  ##============================================================##
  ## MENU WEBSITE THEMES
  ## ===================
  ##============================================================##
  def set_theme
    @user.theme_settings.update_all(:is_selected=>0)
    theme = Theme.find(params[:theme_id])
    ThemeSetting.where(:user_id => @user.id,:theme_id => theme.id).first_or_create.update_attribute(:is_selected,1)
    redirect_to dashboard_apps_path(@user.username,:website =>1)
  end

  def theme_set_colors
    @theme_setting = ThemeSetting.where(:user_id=>@user.id,:theme_id=> params[:theme_id]).first
  end

  def update_theme_setting
    @theme_setting = ThemeSetting.find(params[:theme_id])
    @theme_setting.update_attributes(theme_setting_params)
    flash[:success] = "Saved successfully"               if @theme_setting.errors.blank?
    flash[:danger] = @theme_setting.errors.full_messages if @theme_setting.errors.any?
    flash.discard
  end

  def theme_remove_cover
    @theme_setting = ThemeSetting.find(params[:theme_id])
    @theme_setting.website_cover           = nil  if params[:cover_type] == "website_cover"
    @theme_setting.website_cover_full_size = nil  if params[:cover_type] == "website_cover_full_size"
    @theme_setting.save
    render "update_theme_setting"
  end



  def cards
    @cards = @user.credit_cards
    if @user.stripe_customer_id.present?
      customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
      @default_card = customer.sources.retrieve(customer.default_source)
    end
  end

  def card_delete
    @card = CreditCard.where(:stripe_id=>params[:stripe_id]).first
    if @card.present?
      customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
      customer.sources.retrieve(params[:stripe_id]).delete
      @card.destroy
    end
  end

  def card_new
    @card = CreditCard.new
  end

  def card_default
    @card = CreditCard.find(params[:id])
    customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
    customer.default_source = @card.stripe_id
    customer.save
    redirect_to :back
  end

  def card_update
    @card = CreditCard.new(credit_card_params)
    if @user.stripe_customer_id.blank?
      customer = Stripe::Customer.create(:description => "User ##{@user.id}",:source => params["credit_card"]["stripeToken"])
      @user.update_attributes(:stripe_customer_id => customer.id)
      @card.update_attributes(
        :stripe_id => customer.sources.data[0].id,
        :last4 => customer.sources.data[0].last4,
        :name =>customer.sources.data[0].name,
        :brand => customer.sources.data[0].brand,
        :funding => customer.sources.data[0].funding,
        :exp_month => customer.sources.data[0].exp_month,
        :exp_year => customer.sources.data[0].exp_year,
        :country => customer.sources.data[0].country,
        :stripe_infos => customer.sources.data[0]
        )
    else
      customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
      card = customer.sources.create(:source =>  params["credit_card"]["stripeToken"])
      customer.default_source = card.id
      customer.save
      @card.update_attributes(
        :stripe_id => card.id,
        :last4 => card.last4,
        :name =>card.name,
        :brand => card.brand,
        :funding => card.funding,
        :exp_month => card.exp_month,
        :exp_year => card.exp_year,
        :country => card.country,
        :stripe_infos => card
        )
    end
    redirect_to :back
  end













  def hosting_detail
    @hosting = Hosting.find(params[:hosting])
  end

  def hosting_new
    @hosting = Hosting.new(hosting_params)
    @hosting.save(:validate=>false)
    redirect_to dashboard_apps_path(@user.username,:website =>1)
  end



  def hosting_update
    @hosting = Hosting.find(params[:id])
    @hosting.update(hosting_params)
    @hosting.save(:validate=>false)
    redirect_to dashboard_apps_path(@user.username,:website =>1)
  end


  def hosting_checker
    is_a_domain = Hosting.check_domain_name("domain","http://#{params[:value]}")
    @validation = {:status => false,:available => false}
    if is_a_domain == true
      @validation[:status] = true
      begin
        r = Whois.whois(params[:value])
        if r.available? == true
          @validation[:available] = true
        end
      rescue Whois::Error => e
      end
    end
    respond_to do |format|
      format.json do
        render :json => @validation
      end
    end
  end


  def delete_hosting
    @hosting = Hosting.find(params[:hosting])
    @hosting.destroy
  end





  ##============================================================##
  ## MENU PROFILE & User
  ## ==============
  ##============================================================##
  def profile
    @profile = @user.profile
    @profile_locale = LanguageList::LanguageInfo.find(@profile.default_locale)
  end

  def profile_update
    @profile = @user.profile
    country_1 = @profile.country
    @profile.update_attributes(profile_params)
    country_2 = @profile.country
    @portals_page = country_1 != country_2
    respond_to do |format|
      format.html do
        redirect_to dashboard_profile_path(@user.username,:locale =>@profile.default_locale)
      end
      format.js do
        flash[:success] = "Profile was saved successfully" if @profile.errors.blank?
        flash[:danger] = @profile.errors.full_messages if @profile.errors.any?
        flash.discard
      end
    end
  end

  def user_update
    @user.update_attributes(user_params)
  end

  def account
  end

  def profile_remove_picture
    @profile = @user.profile
    @profile.picture = nil
    @profile.save
    respond_to do |format|
      format.js{render :action => "profile_update"}
    end
  end


  def profile_remove_logo
    @profile = @user.profile
    @profile.logo = nil
    @profile.save
    respond_to do |format|
      format.js{render :action => "profile_update"}
    end
  end




  ##============================================================##
  ## Blog + testimonials
  ##============================================================##
  def blog
    @posts = Blog.where(:user_id=>@user.id).where.not(:draft=>1).order('created_at DESC').paginate(:page => params[:page], :per_page => 15)
  end

  def testimonials
    @testimonials = Testimonial.where(:user_id=>@user.id).reverse_order
  end

  def blog_new
    @post = Blog.where(:user_id=>@user.id,:draft=>1).first_or_create
    redirect_to dashboard_edit_blog_path(@user.username, @post.id)
  end

  def blog_edit
    @post = Blog.find(params[:id])
  end

  def blog_update
    @post = Blog.find(params[:id])
    @post.update_attributes(blog_params)
    respond_to do |format|
      format.html do
        redirect_to dashboard_blog_path(@user.username)
      end
      format.js do
        flash[:success] = "Post was saved successfully" if @post.errors.blank?
        flash[:danger] = @post.errors.full_messages if @post.errors.any?
        flash.discard
      end
    end
  end


  def blog_delete
    @post = Blog.find(params[:id])
    @post.destroy
    redirect_to dashboard_blog_path(@user.username)
  end

  def blog_remove_picture
    @post = Blog.find(params[:id])
    @post.cover = nil
    @post.save
  end

  def testimonials_delete
    @testimonial = Testimonial.find(params[:id])
    @testimonial.destroy
    redirect_to dashboard_testimonials_path(@user.username)
  end

  def testimonials_active
    @testimonial = Testimonial.find(params[:id])
    @testimonial.update_attribute(:status, 1)
    redirect_to dashboard_testimonials_path(@user.username)
  end




  ##============================================================##
  ## WYSIWYG picture
  ##============================================================##
  def wysiwyg_picture_manager
    @my_type = Blog.find(params[:id])     if params[:asset_type] == "blog"
    @my_type = Profile.find(params[:id])  if params[:asset_type] == "profile"
    @my_type = Building.find(params[:id]) if params[:asset_type] == "building"
    @my_type = Property.find(params[:id]) if params[:asset_type] == "property"
    @my_type = Post.find(params[:id])     if params[:asset_type] == "post"
    @asset = @my_type.wysiwyg_pictures.new(:picture=>params[:file])
    @asset.save
  end

  def wysiwyg_picture_delete
    @asset = WysiwygPicture.find(File.basename(params[:src]).split('_').first)
    @asset.destroy
    render :nothing => true
  end








  ##============================================================##
  ## Services
  ##============================================================##
  def services_home
    @services_activated = Array.new
    @services_inactivated = Array.new
    @my_provider.services.order(:position).each do |s|
      services_user = ServicesUser.where(:service_id => s.id, :user_id => @user.id).first
      if services_user.present?
        @services_activated << {:service =>s, :services_user => services_user}
      else
        @services_inactivated.push(s)
      end
    end
  end

  def services_display
    services_user = ServicesUser.find(params[:id])
    services_user.update_attributes(services_user_params)
    render nothing: true
  end


  def services_add_custom
    custom_service = CustomService.new(custom_service_params)
    custom_service.save
    redirect_to dashboard_services_home_path(@user.username)
  end

  def services_edit_custom
    custom_service = CustomService.find(params[:id])
    custom_service.update_attributes(custom_service_params)
    redirect_to dashboard_services_home_path(@user.username)
  end

  def services_delete_custom
    custom_service = CustomService.find(params[:id])
    custom_service.destroy
    redirect_to dashboard_services_home_path(@user.username)
  end


  def services_activation
    contact = Contact.new(contact_params)
    contact.save(:validate => false)
    BackOfficeMailer.new_service_activation_required(contact).deliver_later
    flash[:notice] = t('back_office.thank_you')
    redirect_to dashboard_services_home_path(@user.username)
  end


  def services_order
    @service = Service.find(params[:service_id])
    @service_order = ServiceOrder.where(:user_id=>@user.id, :property_id =>params[:id],:service_id =>@service.id,:status => 0).first_or_create
  end


  def services_order_update
    @service_order = ServiceOrder.find(params[:id])
    @service_order.update_attributes(service_order_params)
    flash[:notice] = t('back_office.thank_you')
    BackOfficeMailer.new_order_placed(@service_order).deliver_later
    redirect_to dashboard_services_path(@user.username,:service =>@service_order.service.slug)
  end


  def services
    @service = Service.where(:slug => params[:service]).first
    raise "Service not Found"     if @service.blank?
    raise "Service not actived"   unless @my_provider.services.map(&:id).include?(@service.id)
    ##============================================================##
    ## check Activation & Options
    ##============================================================##
    @service_user     = ServicesUser.where(:user_id => @user.id,:service_id => @service.id).first
    @service_options  = (@service_user.present? and @service_user.options.present?) ? @service_user.options : 0
    ##============================================================##
    ## Socials Networks
    ##============================================================##
    if @service.slug == "socials_networks"
      @twitter  = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'twitter'}).last
      @facebook = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'facebook'}).last
      @youtube  = Authentification.joins(:api_provider).where(:user_id=>@user.id,:api_providers => { :name =>'google_oauth2'}).last
    end
    if params[:ad].present?
      @properties= Property.where(:user_id=>@user.id,:status =>1,:id=>params[:ad]).paginate(:page => params[:page], :per_page => 15).reverse_order
    else
      @properties = Property.where(:user_id=>@user.id,:status =>1,:property_listing_id => @user.listing_limitation_id.present? ? @user.listing_limitation_id : PropertyListing.all.pluck(:id) ).where.not(:draft=>1).paginate(:page => params[:page], :per_page => 15).reverse_order
    end
    render "back_office/services/layout"
  end




  private


  def make_nomenclature_options(elements,my_array)
    elements.each do |e,children|
      my_array.push ["#{"-"*(e.depth)}#{e.name}",e.id]
      make_nomenclature_options(children,my_array) if children.present?
    end
    return my_array
  end

  def days_in_month(month, year = Time.now.year)
    return 29.0 if month == 2 && Date.gregorian_leap?(year)
    [nil, 31.0, 28.0, 31.0, 30.0, 31.0, 30.0, 31.0, 31.0, 30.0, 31.0, 30.0, 31.0][month]
  end



  def remove_subdomain
    if !request.subdomain.blank? and request.subdomain != 'www'
      redirect_to url_for(params.merge(:host => request.domain))
    end
  end


  def check_ability
    unless current_user.has_any_role? :admin, :manager
      redirect_to url_for(params.merge(:username => current_user.username)) if params[:username] != current_user.username
    end
  end

  def check_apps
    begin
      @user =  User.where(:username=>params[:username]).first || @user
      @website_activated  = ServicesUser.joins(:service).where(:user_id => @user.id,:services =>{:slug=>"website"}).first
      @website_options    = (@website_activated.present? and @website_activated.options.present?) ? @website_activated.options : 0
    rescue Exception => e
      flash[:danger] = "Error \n User not set"
      redirect_to root_path
    end
  end


  def set_store_menu
    begin
      if params[:store].present?
        store  = ApiProvider.where(:name => params[:store],:is_storeimmo_provider => 1).first
        raise "" if store.blank? || store.store_url.blank?
        portal = UserPortalProvider.where(:user_id => @user.id,:api_provider_id => store.id).first
        raise "" if portal.blank?
        menu = HTTParty.get("#{store.store_url}/api/v1/services/menu?client_id=#{portal.uid}?store=#{store.name}")
        raise "" if menu.code != 200
        @remoteMenu = JSON.parse(menu.body)
      end
    rescue Exception => e
      JulesLogger.info e.message
    end
  end



  def is_number?(param)
    true if Float(param) rescue false
  end


  def theme_setting_params
    params.require(:theme_setting).permit!
  end


  def api_import_params
    params.require(:authentification).permit(
      :user_id,
      :api_provider_id,
      :review_status,
      :custom_field_1,
      :custom_field_2,
      :custom_field_3
      )
  end

  def blog_params
    params.require(:blog).permit(
      :user_id,
      :blog_category_id,
      :draft,
      :locale,
      :title,
      :content,
      :cover
      )
  end

  def credit_card_params
    params.require(:credit_card).permit(:user_id)
  end


  def user_params
    params.require(:user).permit(
      :username
      )
  end

  def inclusions_params
    params.require(:property_inclusion).permit!
  end

  def document_attributes
    params.require(:document).permit!
  end


  def properties_params
    params.require(:property).permit(
      Property.globalize_attribute_names,
      :is_sync_with_api,
      :rhinov_link,
      :is_luxurious,
      :status,
      :auto_renewal,
      :draft,
      :status,
      :address,
      :street_number,
      :street_name,
      :sublocality,
      :locality,
      :zipcode,
      :latitude,
      :longitude,
      :administrative_area_level_1,
      :administrative_area_level_2,
      :country_short,
      :property_classification_id,
      :property_status_id,
      :property_listing_id,
      :property_flag_id,
      :building_id,
      :currency,
      :price,
      :price_with_tax,
      :price_from,
      :rent,
      :rent_frequency,
      :tax_1,
      :tax_2,
      :fees,
      :income,
      :unit,
      :year_of_built,
      :availability_date,
      :area_living,
      :area_land,
      :area_unit_id,
      :rooms,
      :bathrooms,
      :bedrooms,
      :withHalf,
      :level,
      :showerrooms,
      :property_url,
      :parking,
      :cadastre,
      :address_visibility,
      :area_balcony,
      :property_complement_attributes => [
        :france_dpe_indice,
        :france_dpe_value,
        :france_dpe_id,
        :france_ges_indice,
        :france_ges_value,
        :france_alur_is_condo,
        :france_alur_units,
        :france_alur_uninhabitables,
        :france_alur_spending,
        :france_alur_legal_action,
        :france_rent_honorary,
        :belgium_peb_indice,
        :belgium_peb_value,
        :belgium_peb_id,
        :belgium_building_permit,
        :belgium_done_assignments,
        :belgium_preemption_property,
        :belgium_subdivision_permits,
        :belgium_sensitive_flood_area,
        :belgium_delimited_flood_zone,
        :belgium_risk_area,
        :canada_mls
      ]
      )
end


def visibility_portal_params
  params.require(:visibility_portal).permit(
    :property_id,
    :portal_id,
    :posting_status,
    :visibility_start,
    :visibility_until
    )
end


def property_assets_params
  params.require(:property_asset).permit(
    :picture,
    :property_id,
    :position,
    :room_id
    )
end


def building_assets_params
  params.require(:building_asset).permit(
    :name,
    :picture,
    :building_id,
    :position,
    )
end

def videos_params
  params.require(:property_video).permit(:video_url,:property_id,:locale)
end

def profile_params
  params.require(:profile).permit!
end

def hosting_params
  params.require(:hosting).permit(
    :domain,
    :paying_domain,
    :property_id,
    :user_id
    )
end


def building_params
  params.require(:building).permit(
    Building.globalize_attribute_names,
    :name,
    :user_id,
    :number_of_units,
    :number_of_levels,
    :street_number,
    :street_name,
    :zipcode,
    :locality,
    :sublocality,
    :address,
    :administrative_area_level_1,
    :administrative_area_level_2,
    :country_short,
    :latitude,
    :longitude,
    :draft
    )
end

def portal_activation_params
  params.require(:portal_activation).permit(
    :login,
    :password,
    :limitation,
    :ftp
    )
end

def share_social_params
  params.require(:share_social).permit(
    :user_id,
    :property_id,
    :api_provider_id,
    :note
    )
end

def report_params
  params.require(:feedback_report).permit(
    :contact_1,
    :contact_2,
    :contact_3,
    :is_sent
    )
end

def feedback_params
  params.require(:feedback).permit(
    :visit_at,
    :draft,
    :advice,
    :full_name,
    :email,
    :phone,
    :reminder_phone,
    :reminder_email,
    :note_internal,
    :note_for_owner,
    :help_sms
    )
end

def glossary_element_params
  params.require(:glossary_element).permit(
    :content,
    :glossary_id,
    )
end

def nomenclature_params
  params.require(:nomenclature).permit(
    :name,
    :user_id,
    :description,
    )
end

def nomenclature_element_params
  params.require(:nomenclature_element).permit(
    :name,
    :value,
    :element_type,
    :parent_id,
    :status,
    :nomenclature_id,
    :is_the_standard_element
    )
end

def inventory_params
  params.require(:inventory).permit(
    :property_id,
    :designation,
    :xml,
    :user_id
    )
end


def inventory_parameter_params
  params.require(:inventory_parameter).permit(
    :nomenclature_id,
    :state1_name,
    :state1_label,
    :state2_name,
    :state2_label,
    :state3_name,
    :state3_label,
    :state4_name,
    :state4_label,
    :state5_name,
    :state5_label,
    :pdf_introduction_title,
    :pdf_introduction_content,
    :pdf_free_section_title,
    :pdf_free_section_content
    )
end

def services_user_params
  params.require(:services_user).permit(:on_marketing_page)
end

def custom_service_params
  params.require(:custom_service).permit!
end


def contact_params
  params.require(:contact).permit!
end

def service_order_params
  params.require(:service_order).permit(:status)
end

def property_contact_params
  params.require(:property_contact).permit!
end








end
