module PingHelper

  require 'net/ping'
  include Net
  Ping::TCP.service_check = true

  def check_ip_from_domain(domain)
    begin
      if Net::Ping::TCP.new(domain).ping? == true
        return true if IPSocket.getaddress(domain) == ENV['ip_prod']
      end
    rescue
      return false
    end
  end



end
