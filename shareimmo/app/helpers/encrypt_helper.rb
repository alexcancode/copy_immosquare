module EncryptHelper

  def encrypt(data)
    encrypted_data = AESCrypt.encrypt(data, "3137437538213489") if data.present?
  end

  def decrypt(data)
    AESCrypt.decrypt(data, "3137437538213489") if data.present?
  end
end
