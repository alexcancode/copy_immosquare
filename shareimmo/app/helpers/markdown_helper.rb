# encoding: utf-8

module MarkdownHelper



class HTMLwithPygments < Redcarpet::Render::HTML
    def block_code(code, lang)
        lang = lang && lang.split.first || "text"
        Pygments.highlight(code, lexer: lang.to_sym,options: {:lineseparator => "<br>",:linespans=>:'line_number'})
    end
  end

  class HTMLwithPygments_black < Redcarpet::Render::HTML
    def block_code(code, lang)
        lang = lang && lang.split.first || "text"
        Pygments.highlight(code, lexer: lang.to_sym,options: {:lineseparator => "<br>",:linespans=>:'line_number',:cssclass=>'highlight highlight_black'})
    end
  end


  def markdown(text,black_editor=false)
    if black_editor==false
      renderer = HTMLwithPygments.new(hard_wrap: true, filter_html: false,safe_links_only:true)
    else
      renderer = HTMLwithPygments_black.new(hard_wrap: true, filter_html: false,safe_links_only:true)
    end
    options = {
      no_intra_emphasis: false, #si false  foo_bar_baz est trancrit foo<em>bar</em>baz
      tables:true, # si true , les tableaux sont parsés et traduit en html
      fenced_code_blocks: true, #si true, tout ce qui se trouve entre ~~~ est parsés comme du code
      autolink: true,
      strikethrough: true, # si true, le texte entre deux ~~ est barré
      lax_spacing: false,
      space_after_headers:false,
      superscript:false, # exposant : this is the 2^(nd) time
      underline:true, #This is _underlined_ but this is still *italic*
      highlight:true, #This is ==highlighted==
      quote:true,
      footnotes:false,
      disable_indented_code_blocks:false,
    }
    Redcarpet::Markdown.new(renderer, options).render(text).html_safe
  end


  def colorize_pre(text,format)
    Pygments.highlight(text, :lexer =>format).html_safe if text.present?
  end

  def colorize_pre_file(file,format)
    Pygments.highlight(File.read(file), :lexer =>format).html_safe if File.exist?(file)
  end


end
