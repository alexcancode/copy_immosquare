module ApplicationHelper

  def view_is_devise?
    devise_controller = [ "sessions", "registrations", "confirmations", "unlocks","passwords" ];
    if devise_controller.include?(controller_name)
      unless controller.controller_name=="registrations" and (controller.action_name=="edit" or controller.action_name=="update")
        return true
      end
    end
  end

  def formated_phone_number(number = "")
    Phony.format(number.gsub(/\s+/, ""), :format => :international, :spaces => " ")
  end


  def hex_to_rgba(hex,opacity)
    hex = hex.gsub('#','')
    rgb = hex.scan(/../).map {|color| color.hex}
    "rgba(#{rgb[0].to_i},#{rgb[1].to_i},#{rgb[2].to_i},#{opacity})"
  end


  ##============================================================##
  ## Custom Button Class
  ##============================================================##
  def custom_button_class
    "btn-custom-primary-#{@my_provider.provider_id}"
  end

  def custom_button_class_secondary
    "btn-custom-secondary-#{@my_provider.provider_id}"
  end

  ##============================================================##
  ## Custom Bg Class
  ##============================================================##
  def custom_bg_class
    "bg-#{@my_provider.provider_id}-primary"
  end

    def custom_bg_class_secondary
    "bg-#{@my_provider.provider_id}-secondary"
  end


  ##============================================================##
  ## Custom Color Class
  ##============================================================##
  def custom_color_class
    "color-#{@my_provider.provider_id}-primary"
  end

  def custom_color_class_secondary
    "color-#{@my_provider.provider_id}-secondary"
  end

  ##============================================================##
  ## Custom Border Class
  ##============================================================##
  def custom_border_class
    "border-#{@my_provider.provider_id}-primary"
  end


  def custom_border_class_secondary
    "border-#{@my_provider.provider_id}-secondary"
  end


  def good_locality(property)
    property.sublocality.blank? ? "#{property.locality}" : "#{property.sublocality}"
  end


  def avatar_to_display(user,width=30)
    return if user.blank?
    if user.profile.picture?
      user.profile.picture.url(:medium)
    else
      gravatar_url(user.email,width)
    end
  end



  def gravatar_url(email, size=50,  secure=true, img_type='png')
    default_img ="mm"
    allow_rating="pg"
    gravatar_id = Digest::MD5.hexdigest(email)
    "http"+ (!!secure ? 's://secure.' : '://') +"gravatar.com/avatar/#{gravatar_id}.#{img_type}?s=#{size}&r=#{allow_rating}&d=#{default_img}"
  end


  def name_to_display(user)
    return if user.blank?
    unless user.profile.public_name.blank?
      user.profile.public_name
    else
      user.email
    end
  end


  def my_url_with_protocol(url,secure = false)
    return if url.blank?
    url = "http#{secure == true ? "s" : nil}://#{url}" unless url[/\Ahttp:\/\//] || url[/\Ahttps:\/\//]
    return url
  end




end
