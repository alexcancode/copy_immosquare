module MoneyHelper


  def currency_symbol
    content_tag(:span, Money.default_currency.symbol, :class => "currency_symbol")
  end


  def my_beautiful_price(property)
    property.property_listing.id == 1 ? humanized_money_with_symbol(property.price) : ("#{humanized_money_with_symbol(property.rent)}#{humanized_frequency(property.rent_frequency)}").gsub(" ","") if property.present?
  end



  def humanized_money(value, options={})
    if !options || !options.is_a?(Hash)
      warn "humanized_money now takes a hash of formatting options, please specify { :symbol => true }"
      options = { :symbol => options }
    end
    options = {:no_cents_if_whole => true,:symbol => false}.merge(options)
    if value.is_a?(Money)
      value.format(options)
    elsif value.respond_to?(:to_money)
      value.to_money.format(options)
    else
      ""
    end
  end




  def humanized_money_with_symbol(value, options={})
    humanized_money(value, options.merge(:symbol => true))
  end



  def money_without_cents(value, options={})
    if !options || !options.is_a?(Hash)
      warn "money_without_cents now takes a hash of formatting options, please specify { :symbol => true }"
      options = { :symbol => options }
    end
    options = {:no_cents => true,:no_cents_if_whole => false,:symbol => false}.merge(options)
    humanized_money(value, options)
  end

  def money_without_cents_and_with_symbol(value)
    money_without_cents(value, :symbol => true)
  end


  def humanized_frequency(frequency)
    case frequency
    when 1
      frequency = "/ #{I18n.t('datetime.prompts.day').downcase}"
    when 7
      frequency = "/ #{I18n.t('datetime.prompts.week').downcase}"
    when 90
      frequency = "/ 3 #{I18n.t('datetime.prompts.month').downcase}"
    when 180
      frequency = "/ 6 #{I18n.t('datetime.prompts.month').downcase}"
    when 360
      frequency = "/ #{I18n.t('datetime.prompts.year').downcase}"
    else
      frequency = "/ #{I18n.t('datetime.prompts.month').downcase}"
    end
  end




end
