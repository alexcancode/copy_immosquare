module MailerHelper


  def set_my_subject(subject)
    if Rails.env.development? || Rails.env.beta?
      subject = "###{Rails.env}## #{subject}"
    end
    return subject
  end

  def set_my_from(from = "contact")
    return "#{from}@#{@my_provider.host}"
  end


end
