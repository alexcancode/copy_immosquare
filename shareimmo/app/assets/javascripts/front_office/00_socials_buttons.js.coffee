$(document).ready ->


  popupCenter = (url, title) ->
    popupWidth   = 640
    popupHeight  = 320
    windowLeft   = window.screenLeft or window.screenX
    windowTop    = window.screenTop or window.screenY
    windowWidth  = window.innerWidth or document.documentElement.clientWidth
    windowHeight = window.innerHeight or document.documentElement.clientHeight
    popupLeft    = windowLeft+ windowWidth / 2 - (popupWidth / 2)
    popupTop     = windowTop + windowHeight / 2 - (popupHeight / 2)
    popup        = window.open(url, title, "scrollbars=yes,width=#{popupWidth},height=#{popupHeight},top=#{popupTop},left=#{popupLeft}")
    popup.focus()
    true

  $('button.share_twitter').on 'click',(e) ->
    e.preventDefault
    shareUrl = "https://twitter.com/intent/tweet?text=#{encodeURIComponent(document.title)}"+"&url=#{encodeURIComponent($(@).data('url'))}"
    popupCenter(shareUrl, "Twitter")

  $('button.share_facebook').on 'click',(e) ->
    e.preventDefault
    shareUrl = "https://www.facebook.com/sharer/sharer.php?u=#{encodeURIComponent($(@).data('url'))}"
    popupCenter(shareUrl, "Facebook")


  $('button.share_gplus').on 'click',(e) ->
    e.preventDefault
    shareUrl = "https://plus.google.com/share?url=#{encodeURIComponent($(@).data('url'))}"
    popupCenter(shareUrl, "Google")


  $('button.share_linkedin').on 'click',(e) ->
    e.preventDefault
    shareUrl = "https://www.linkedin.com/shareArticle?url=#{encodeURIComponent($(@).data('url'))}"
    popupCenter(shareUrl, "Google")

  $('button.share_pinterest').on 'click',(e) ->
    e.preventDefault
    shareUrl = "http://pinterest.com/pin/create/button/?url==#{encodeURIComponent($(@).data('url'))}"
    popupCenter(shareUrl, "Google")

