##============================================================##
## Ces fonctions servent dans la page d'édition du nom de
## domaines
##============================================================##
$('body.back_office.apps').ready ->
  delay = (->
    timer = 0
    (callback, ms) ->
      clearTimeout timer
      timer = setTimeout(callback, ms)
      return
  )()


  $('#DomainAdddModal').on 'show.bs.modal', (e) ->
    $("#addDomain").hide()
    $('#newDomainInput input').val('')


  $('#newDomainInput').on 'keyup', 'input',(event) ->
    container = $('#newDomainInput')
    value_to_check = $(event.currentTarget).val()
    delay ->
      if value_to_check.length
        container.removeClass('has-error has-success')
        container.find('span.form-control-feedback').removeClass('glyphicon-remove glyphicon-ok')
        container.find('i').addClass('hide')
        postData = new Object()
        postData.value = value_to_check
        $.ajax
          type: "POST"
          url: container.data('checkdomain')
          data: postData
          dataType: "json"
          success: (data) ->
            if data.status is true and data.available is true
              container.find('.text-success').removeClass('hide')
              container.find('.text-danger').addClass('hide')
              $("#addDomain").show()
            else
              container.find('.text-success').addClass('hide')
              container.find('.text-danger').removeClass('hide')
              $("#addDomain").hide()
    ,500



