$('body.back_office.theme_set_colors').ready ->

  $('.wistiaBtn').on 'click', (e) ->
    e.preventDefault()
    $('#removeWebsiteCoverFullSize').trigger('click')
    $('#theme_setting_wistia_id').val($(@).data('wistia'))
    $('#theme_setting_img_id').val($(@).data('img'))
    $('.labelActivBackground').addClass('hide')
    $(@).siblings('.labelActivBackground').removeClass('hide')
    $('#TemplateForm').submit()

  $('body.back_office.theme_set_colors').on 'change','#theme_setting_website_cover', ->
    $('form#TemplateForm').submit()


  $('body.back_office.theme_set_colors').on 'change','#theme_setting_website_cover_full_size', ->
    $('form#TemplateForm').submit()
