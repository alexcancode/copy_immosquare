$('body.back_office, body.posts.edit,body.admin').ready ->

  ##============================================================##
  ## Activation du plugin pophover de boostrap
  ##============================================================##
  $('[data-toggle="popover"]').popover()



  ##============================================================##
  ## Disable form submit with enter
  ##============================================================##
  $(window).keydown (event) ->
    if event.keyCode == 13
      event.preventDefault()
      return false
    return




  ##============================================================##
  ## Activation du plugin Pickadate
  ##============================================================##
  $('.pickadate').pickadate()

  ##============================================================##
  ## Setup du menu
  ##============================================================##
  $('#MyHeaderHeroNav .comp')
    .on 'mouseover', ->
      $($(@).find('.drop-down-menu')).show()
    .on 'mouseout', ->
      $($(@).find('.drop-down-menu')).hide()


  ##============================================================##
  ## Setup de l'éditeur Froala...
  ##============================================================##
  if typeof froala_init isnt 'undefined' and froala_init is true
    $('.froala').froalaEditor(
      theme: 'gray'
      height: '300'
      scrollableContainer: '.fr-box.fr-top'
      toolbarButtons: [
        'fullscreen'
        'bold'
        'italic'
        'underline'
        'strikeThrough'
        'subscript'
        'superscript'
        'fontFamily'
        'fontSize'
        '|'
        'color'
        'emoticons'
        'inlineStyle'
        'paragraphStyle'
        '|'
        'paragraphFormat'
        'align'
        'formatOL'
        'formatUL'
        'outdent'
        'indent'
        '-'
        'insertLink'
        'insertImage' if picture_module == true
        'insertVideo' if picture_module == true
        'insertTable'
        '|'
        'quote'
        'insertHR'
        'undo'
        'redo'
        'clearFormatting'
        'selectAll'
      ]
      imageUploadURL: froala_upload_url
      imageMaxSize: 1 * 1024 * 1024
      imageAllowedTypes: [
        'jpeg'
        'jpg'
        'png'
      ]
      inlineMode: false
      toolbarFixed: false).on 'froalaEditor.image.removed', (e, editor, $img) ->
      $.ajax(
        method: 'POST'
        url: frola_delete_url
        data: src: $img.attr('src')).done((data) ->
        console.log 'image was deleted'
        return
      ).fail ->
        console.log 'image delete problem'
        return
      return

