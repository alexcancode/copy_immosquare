$('body.back_office.profile,body.back_office.apps').ready ->


  ##============================================================##
  ## Chosen : sur les langues
  ##============================================================##
  $('#chosen-select-languages').on 'change', (evt,params) ->
    spoken = $('#profile_spoken_languages').val()
    if spoken.length == 0
      spoken = []
    else
      spoken = spoken.split(',');
    if params.selected
      spoken.push(params.selected)
    if params.deselected
      spoken.splice(jQuery.inArray(params.deselected,spoken),1);
    $('#profile_spoken_languages').val(spoken)



  ##============================================================##
  ## AutoSubmit : Picture
  ##============================================================##
  $('body.back_office.profile').on 'change','#profile_picture', ->
    $('form#form_setupPicture').submit()
  $('#changePictureLink').bind 'click', ->
    $('#profile_picture').click();


  ##============================================================##
  ## AutoSubmit : Logo
  ##============================================================##
  $('body.back_office.profile').on 'change','#profile_logo', ->
    $('form#form_setupLogo').submit()
  $('#changeLogoLink').bind 'click', ->
    $('#profile_logo').click();


  ##============================================================##
  ## Effect on each line
  ##============================================================##
  $('.setting_edit_line').on 'click', (e) ->
    $('.setting_edit_line').each ->
      if $(@).hasClass 'active'
        $(@).removeClass 'active'
    $(@).addClass 'active'
    ##============================================================##
    ##  CloseMenu
    ##============================================================##
    if $(e.target).hasClass('closeAction')
        e.preventDefault();
        $(e.currentTarget).removeClass('active')
    ##============================================================##
    ## Chosen
    ##============================================================##
    if $(@).data('form') is 'setupLanguages'
        $('#chosen-select-languages').chosen()
    if $(@).data('form') is 'setupCountry'
      $('#chosen-select-country').chosen(no_results_text: 'No results matched')


