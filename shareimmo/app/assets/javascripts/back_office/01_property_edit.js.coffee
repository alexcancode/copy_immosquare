$("body.back_office.property_edit, body.back_office.blog_edit, body.back_office.building_edit").ready ->

  ##============================================================##
  ## Menu
  ##============================================================##
  $('#sidebar_my_services a').on 'click', (e) ->
    e.preventDefault()
    $('#sidebar_my_services li').removeClass('active')
    $(@).closest('li').addClass('active')
    $('#annonce_form_right div.step').hide()
    $("#{$(@).attr('href')}").show()


  ##============================================================##
  ## Custom Integer Inputs with (-/+)
  ##============================================================##
  $('.bootstrapNumber').bootstrapNumber()

  ##============================================================##
  ## Autosave Inclusions
  ##============================================================##
  $('form.edit_property_inclusion').on 'change', 'input', ->
    $('form.edit_property_inclusion').submit()

  ##============================================================##
  ## Change les champs en fonction du type d'annonce
  ## À vendre ou à louer
  ##============================================================##
  $('body.back_office.property_edit #property_property_listing_id').ready ->
    if $('option:selected', this).attr('value') == '1'
      $('body.back_office.property_edit #general-sell').removeClass "hide"
      $('body.back_office.property_edit #general-rent').addClass "hide"
    else
      $('body.back_office.property_edit #general-rent').removeClass "hide"
      $('body.back_office.property_edit #general-sell').addClass "hide"
    $(@).change (e) ->
      if $('option:selected', this).attr('value') == '1'
        $('body.back_office.property_edit #general-sell').removeClass "hide"
        $('body.back_office.property_edit #general-rent').addClass "hide"
      else
        $('body.back_office.property_edit #general-rent').removeClass "hide"
        $('body.back_office.property_edit #general-sell').addClass "hide"

  $('body.back_office.property_edit #property_country_short').ready ->
    $("#complement_#{$(@).val()}").show()
    $(@).change (e) ->
      $('.complement_infos').hide()
      $("#complement_#{$(@).val()}").show()

  ##============================================================##
  ## Change le surfaces en fonction du select
  ##============================================================##
  $('body.back_office.property_edit select.surface_unit').ready ->
    $('.surface-disabled').html $('option:selected', this).html()
    $(@).on "change", (e) ->
      $('.surface-disabled').html $('option:selected', this).html()




  ##============================================================##
  ## Change les monnaies en fonction du select
  ##============================================================##
  $('body.back_office.property_edit select.currency').ready ->
    text  = $('option:selected',@).text()
    val   = $('option:selected',@).val()
    $('.currency-disabled').html(text)
    $(@).on 'change', (e) ->
      text = $('option:selected',@).text()
      val  = $('option:selected',@).val()
      $('.currency-disabled').html(text)
      $('select.currency').val(val)




  ##============================================================##
  ## Customisation du bouton d'upload d'images
  ##============================================================##
  $('#input_file_button').on 'click', (e) ->
    $('.input-file').trigger "click"



  ##============================================================##
  ## FileUpload Pictures
  ##============================================================##
  model = $('#PropertyGeneralForm').data('model')
  $("#new_#{model}_asset").fileupload
    dataType: 'script'
    dropZone: $('#dropzone')
    add: (e, data) ->
      types = /(\.|\/)(gif|jpe?g|png|mov|mpeg|mpeg4|avi)$/i
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        data.context = $(tmpl("template-upload", file))
        $("#new_#{model}_asset").append(data.context)
        data.submit()
      else
        swal("Error","#{file.name} is not a gif, jpg or png image file",'error')
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.progress-bar').css('width', progress + '%')
    done: (e,data) ->


  ##============================================================##
  ## Effect on dragover
  ##============================================================##
  $(document).bind 'dragover', (e) ->
    dropZone = $('#dropzone')
    timeout = window.dropZoneTimeout
    if !timeout
      dropZone.addClass 'in'
    else
      clearTimeout timeout
    found = false
    node = e.target
    loop
      if node == dropZone[0]
        found = true
        break
      node = node.parentNode
      unless node != null
        break
    if found
      dropZone.addClass 'hover'
    else
      dropZone.removeClass 'hover'
    window.dropZoneTimeout = setTimeout((->
      window.dropZoneTimeout = null
      dropZone.removeClass 'in hover'
      return
    ), 100)
    return



  ##============================================================##
  ## Sortable Pictures
  ##============================================================##
  if $('#my-ui-list-picture').length
    Sortable.create document.getElementById("my-ui-list-picture"),
      store:
        get: (sortable) ->
          order = ""
          if order then order.split('|') else []
        set: (sortable) ->
          $.ajax
            type: "POST"
            url: $('#my-ui-list-picture').data("sort_url")
            data:
              my_order: sortable.toArray()



  ##============================================================##
  ## Getion de l'autocomplète avec la carte Google associée
  ##============================================================##
  if google?
    geocoder = new google.maps.Geocoder()
    latlng = new google.maps.LatLng(model_lat, model_lng)
    mapOptions =
      styles:[{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}],
      maxZoom: 18,
      minZoom: 11,
      zoom:14,
      disableDefaultUI: true,
      draggable: true,
      panControl: false,
      zoomControl: true,
      streetViewControl:false,
      scrollwheel: false,
      scaleControl: true,
      center: latlng
      mapTypeId: google.maps.MapTypeId.ROADMAP
      zoomControlOptions:
        style: google.maps.ZoomControlStyle.SMALL
      mapTypeControlOptions:
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    map = new google.maps.Map(document.getElementById("mapContainer"), mapOptions)
    marker = new google.maps.Marker
      map: map
      position: latlng
      draggable: false


    autocomplete = new google.maps.places.Autocomplete(document.getElementById('gmaps-input-address'))
    autocomplete.bindTo('bounds', map)

    google.maps.event.addListener autocomplete, 'place_changed', ->
      performGeocoding(autocomplete.getPlace())


    $("#property_building_id").on 'change', ->
      request = {}
      request['address'] = $(@).find(':selected').data('address')
      geocoder.geocode  request , (results, status) ->
        if status is "OK"
          performGeocoding(results[0])
        else
          swal 'Geocode was not successful for the following reason: ' + status



    performGeocoding = (place) ->
      model = $('#PropertyGeneralForm').data('model')
      $("##{model}_street_number, ##{model}_street_name, ##{model}_sublocality,##{model}_locality,##{model}_administrative_area_level_1, ##{model}_administrative_area_level_2,##{model}_country_short,##{model}_zipcode,##{model}_latitude,##{model}_longitude").val("")
      marker.setVisible(false)

      $("#gmaps-input-address").val place.formatted_address

      if !place.geometry
        swal "Autocomplete's returned place contains no geometry"
        return

      if place.geometry.viewport
        map.fitBounds place.geometry.viewport
      else
        map.setCenter place.geometry.location
        map.setZoom 14

      marker.setPosition(place.geometry.location)
      marker.setVisible(true)

      $("##{model}_latitude").val(place.geometry.location.lat())
      $("##{model}_longitude").val(place.geometry.location.lng())



      componentForm =
        street_number:
          long_or_short:'short_name'
          my_input_id: 'street_number'
        route:
          long_or_short: 'long_name'
          my_input_id: 'street_name'
        neighborhood:
          long_or_short: 'long_name'
          my_input_id: 'sublocality'
        sublocality_level_1:
          long_or_short: 'long_name'
          my_input_id: 'sublocality_level_1'
        locality:
          long_or_short: 'long_name'
          my_input_id: 'locality'
        administrative_area_level_2:
          long_or_short: 'long_name'
          my_input_id: 'administrative_area_level_2'
        administrative_area_level_1:
          long_or_short: 'long_name'
          my_input_id: 'administrative_area_level_1'
        country:
          long_or_short: 'short_name'
          my_input_id: 'country_short'
        postal_code_prefix:
          long_or_short: 'short_name'
          my_input_id: 'zipcode'
        postal_code:
          long_or_short: 'short_name'
          my_input_id: 'zipcode'

      i = 0
      while i < place.address_components.length
        addressType = place.address_components[i].types[0]
        if componentForm[addressType]
          val = place.address_components[i][componentForm[addressType].long_or_short]
          # console.log "##{model}_#{componentForm[addressType].my_input_id} => #{val}"
          $("##{model}_#{componentForm[addressType].my_input_id}").val(val).trigger('change')
        i++


  ##============================================================##
  ## Property Contact Autocomplete
  ##============================================================##
  engine = new Bloodhound
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value')
    queryTokenizer: Bloodhound.tokenizers.whitespace
    local: property_contacts


  $('.typeahead')
    .typeahead null,
      name: 'best-pictures'
      display: "value"
      source: engine
    .bind 'typeahead:select', (ev, suggestion) ->
      $('#property_contact_property_contact_id').val(suggestion.id)



