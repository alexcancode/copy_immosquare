$('body.back_office.dashboard').ready ->

  ##============================================================##
  ## Filter : AutoSubmit Ajax
  ##============================================================##
  $('#filterForm select').on 'change', (event) ->
    $('#filterForm').submit()



  ##============================================================##
  ## Kangalou Dashboard
  ##============================================================##
  container = $(".kangalou-steps");
  height = 0;
  $('.kangalou-step',container).each ->
    if($(@).outerHeight() > height)
      height = $(@).outerHeight()
  $('.kangalou-step',@).each ->
    $(@).height(height + 40)

