# @cjsx React.DOM

React     = window.React    = require('react')
ReactDOM  = window.ReactDOM = require('react-dom')
Router        = require('react-router').Router
Route         = require('react-router').Route
Link          = require('react-router').Link
History       = require('react-router').browserHistory
MapContainer  = require('map/mapContainer')
LeftSide      = require('map/leftSide')



MapApp = React.createClass
  render: () ->
    return(
      <Router history={History}>
        <Route component={MainLayout}>
          <Route path="/map" component={MyApp} search={null} apiUrl={@props.data.api_url} mapboxToken={@props.data.mapboxtoken} mapTheme={@props.data.maptheme} mapThemes={@props.data.mapthemes}></Route>
        </Route>
      </Router>
    )


##============================================================##
## Layout
##============================================================##
MainLayout = React.createClass
  render: () ->
    return (
      <div id="appView" className="relative">
        <MainMenu></MainMenu>
        {this.props.children}
      </div>
    )


##============================================================##
##
##============================================================##
MainMenu = React.createClass
  render: () ->
    return(
      <menu id="MyHeaderHeroNav" className='mt0 mb0 pl0 clearfix'>
        <div className="comp pull-left">
          <a href="http://shareimmo.com/fr">
            <img className="img-responsive" id="logo" src="/assets/logos/shareimmo-10f4eb0cb84483b885c013b22c2ed5ad.png" alt="Shareimmo" />
          </a>
        </div>
        <div className="hidden-xs">
          <form id="searchFormFake"  className="simple_form search">
            <div className="comp pull-left search-bar-wrapper">
              <i className="fa fa-search fa-15x absolute"></i>
              <input id="gmaps-input-address" className="pl35" placeholder="Entrez une ville?" type="text" />
            </div>
          </form>
          <div className="comp pull-right block mr2 pointer">
            <a href="/fr/signin">Connexion</a>
          </div>
        </div>
      </menu>
    )




##============================================================##
##
##
##============================================================##
MyApp = React.createClass

  getInitialState: () ->
    console.log "React App: started"
    @props.route.search =
      price_from:               @props.location.query.price_from          || ""
      price_to:                 @props.location.query.price_to            || ""
      area_from:                @props.location.query.area_from           || ""
      area_to:                  @props.location.query.area_from           || ""
      reference:                @props.location.query.reference           || ""
      keywords:                 @props.location.query.keywords            || ""
      status:                   @props.location.query.status              || ""
      majortransaction:         @props.location.query.majortransaction    || ""
      majortype:                @props.location.query.majortype           || ""
      mimortype:                @props.location.query.mimortype           || ""
      bedrooms:                 @props.location.query.bedrooms            || ""
      bathrooms:                @props.location.query.bathrooms           || ""
      publication_dates:        @props.location.query.publication_dates   || ""
      bounds:                   @props.location.query.bounds              || ""
      custom_poly:              @props.location.query.custom_poly         || ""
      custom_circle:            @props.location.query.custom_circle       || ""
      zones:                    @props.location.query.zones               || ""
      page:                     @props.location.query.page                || 1
      user:                     @props.location.query.user                || ""


    return {
      apiResult: null
      listingsIds: []
      hightlight: null
    }


  ##============================================================##
  ## History.push lance un render, comme @setState
  ##============================================================##
  fetchApi: (args={}) ->
    if args.search?
      keys = _.keys(args.search)
      _.each keys, (key) =>
        @props.route.search[key] = args.search[key]


    newSearch = _.clone(@props.route.search)

    _.each newSearch, (v, k) ->
      delete newSearch[k] if !v

    apiParams = decodeURIComponent(jQuery.param(newSearch))
    History.push("/map?#{apiParams}")

    jQuery.ajax
      type: "get"
      url : "api/v2/search/geojson/properties?#{apiParams}&per_page=200"
      headers:
        "apiKey":"123immo",
        "apiToken":"square456"
      success: (api) =>
        ids = _.map api.results.features ,(feature) ->
            return feature.properties.id
          ids = ids.sort()

        if ids.toString() != @state.listingsIds.toString()
          @setState
            apiResult: api
            listingsIds: ids


  updateHightlight: (id) ->
    @setState(hightlight: id)


  render: () ->
    return(
      <div id='FilterAndMap' className="relative">
        <LeftSide apiResult={@state.apiResult} hightlight={@state.hightlight}></LeftSide>
        <MapContainer
          bounds={@props.route.search.bounds}
          apiResult={@state.apiResult}
          mapboxToken={@props.route.mapboxToken}
          themes={@props.route.mapThemes}
          theme={@props.route.mapTheme}
          hightlight={@updateHightlight}
          fetchApi={@fetchApi}>
        </MapContainer>
      </div>
    )










##============================================================##
##
##============================================================##
$("body.map.index").ready ->
  datas = $("#reactApp")
  ReactDOM.render(<MapApp data={datas.data()} />,datas[0])


