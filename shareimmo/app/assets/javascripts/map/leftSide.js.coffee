# @cjsx React.DOM

module.exports  = React.createClass
  getInitialState: ->
    return{
      modeList: false
    }

  componentDidUpdate: ->
    if @props.hightlight?
      list  = @refs.resultList
      $(list).scrollTop($(list).scrollTop()+$(list).find("#list_#{@props.hightlight}").offset().top-100)

  updateMode: ->
    @setState
      modeList: !@state.modeList

  render: () ->
    PropertiesList =
      if @props.apiResult? && @props.apiResult.results.features
          @props.apiResult.results.features.map (listing) =>
            if @state.modeList == true
              <PropertyList data={listing.properties} hightlight={@props.hightlight}  key={listing.properties.id}  />
            else
              <PropertyList2 data={listing.properties} hightlight={@props.hightlight}  key={listing.properties.id}  />
      else
        <div className="mt20 center">
          <i className="fa fa-spinner fa-3x fa-spin" />
        </div>

    Size =
      if @props.apiResult? && @props.apiResult.results.features
        "#{@props.apiResult.pagination.formated}"
      else
        "No result"


    return(
      <div id="leftSide">
        <div id="resultCount" className="center">
          {Size}
          <hr className="mt0 mb0"/>
          <ul className="list-inline a-right mb0">
            <li onClick={@updateMode}>
              <i className="fa fa-th pointer  #{if @state.modeList == false then 'color-secondary' else null }" />
            </li>
            <li onClick={@updateMode}>
              <i className="fa fa-list pointer  #{if @state.modeList == true then 'color-secondary' else null }" />
            </li>
          </ul>
        </div>
        <ul ref="resultList" className="mb0 list-unstyled resultList">
          {PropertiesList}
        </ul>
      </div>
    )





PropertyList = React.createClass
  render: ->
    divhightlight =
      backgroundColor: if @props.hightlight == @props.data.id then "yellow" else ""

    divStyle =
      backgroundImage: "url(#{@props.data.cover_url})"


    divStyle2 =
      color: if @props.data.property_listing.id == 1 then "#002663" else "#fd5c63"

    return(
      <li id="list_#{@props.data.id}" className="pl5 pt5 pb5 pr3 fs10 ln11" style={divhightlight}>
        <div className="thumb bg-cover pull-left mr5" style={divStyle}></div>
        <div>
          <div className="a-right bold" style={divStyle2}>{@props.data.property_listing.fr}</div>
          <div>{@props.data.locality}</div>
          <div>{@props.data.property_classification.fr}</div>
          <div className="a-right bold">{@props.data.price_formated}</div>
        </div>
        <div className="clear-block"></div>
      </li>
    )





PropertyList2 = React.createClass

  onClick: (id) ->
    console.log "#{id}"


  render: ->
    divhightlight =
      backgroundColor: if @props.hightlight == @props.data.id then "green" else ""
      backgroundImage: "url(#{@props.data.cover_url})"
      height: "200px"



    return(
      <li id="list_#{@props.data.id}" onClick={@onClick.bind(@,@props.data.id)} className="relative bg-cover fs11 ln11 pointer" style={divhightlight}>
        <div className="absolute absolute-bottom-left absolute-bottom-right color-white withBg">
          <div>
            <div className="a-right bold">{@props.data.property_listing.fr}</div>
            <div>{@props.data.locality}</div>
            <div>{@props.data.property_classification.fr}</div>
            <div className="a-right bold">{@props.data.price_formated}</div>
          </div>
        </div>
      </li>
    )
