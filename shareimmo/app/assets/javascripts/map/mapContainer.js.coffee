# @cjsx React.DOM


module.exports =  React.createClass
  getInitialState: ->
    return{
      searchOnMove: true
    }


  componentDidMount: ->
    mapboxgl.accessToken = @props.mapboxToken

    @listingsIds = []

    @emptyGeoJson =
      type: "FeatureCollection"
      features: []

    b = @props.bounds.split(',')
    if b.length is 4
      bounds = new mapboxgl.LngLatBounds([b[1],b[0]], [b[3],b[2]])
      center = bounds.getCenter()

    else
      center = [0,0]
      bounds = null

    @map =  new mapboxgl.Map
      container:  "mapHere"
      style:      "mapbox://styles/mapbox/#{@props.theme}-v9"
      center: center
      zoom: 1
      minZoom:1
      pitch: 30


    @map.addControl(new mapboxgl.Navigation())

    if bounds isnt null
      @map.fitBounds(bounds)

    @map.on 'load', () =>
      console.log "Map: loaded"

      popup = new mapboxgl.Popup
      current_location =
        x: null
        y: null
      current_marker = null

      ##============================================================##
      ## Source
      ##============================================================##
      @map.addSource "markers_source",
        type: "geojson"
        data: @emptyGeoJson

      ##============================================================##
      ##
      ##============================================================##
      @addLayers()

      ##============================================================##
      ## Fetch Datas For a first Time
      ##============================================================##
      @props.fetchApi()



      ##============================================================##
      ## Events
      ##============================================================##
      @map.on 'mousemove', (e) =>
        mouseOnMarker = @map.queryRenderedFeatures(e.point,layers:['markers_layer'])

        @map.getCanvas().style.cursor = if (mouseOnMarker.length and mouseOnMarker[0].properties.index?) then 'pointer' else ''

        if popup.isOpen() is true
          if mouseOnMarker.length and mouseOnMarker[0].properties.index isnt current_marker
            popup.remove()
            return

          distance =  Math.sqrt(Math.pow(Math.abs(e.point.x-current_location.x),2)+Math.pow(Math.abs(e.point.y-current_location.y),2))
          if distance > 80
            popup.remove()
            return
        else
          if mouseOnMarker.length and mouseOnMarker[0].properties.index?
            feature           = mouseOnMarker[0]
            current_location  = e.point
            current_marker    = feature.properties.index

            listing_grouped = JSON.parse(feature.properties.grouped)

            popUpContent = "<div class='fs12 pt0 pr10 pb5 pl5 bb border-light-grey bold ellipsis'>#{listing_grouped[0].address}</div> <ul id='property-list' class='mb0 list-unstyled'>"
            _.each listing_grouped, (listing) ->
              popUpContent +=
                "<li class='pl5 pt5 pb5 pr3 fs10 lh12 pointer' data-id='#{listing.id}'>
                  <div class='thumb bg-cover pull-left mr5' style='background-image:url(\"#{listing.cover_url}\")'></div>
                  <div>
                    <div class='a-right bold ellipsis'>#{listing.property_classification.fr}</div>
                    <div class='ellipsis'>#{listing.locality}</div>
                    <div class='ellipsis'>#{listing.property_listing.fr}</div>
                    <div class='a-right ellipsis'>#{listing.price_formated}</div>
                  </div>
                  <div class='clear-block'></div>
                </li>"

            popUpContent += "</ul>"

            div = $('<div></div>')
            div.html(popUpContent)
            $(div).on 'click','li', (evt) =>
              id = $(evt.currentTarget).data('id')
              @clickOnPopUp(id)

            popup
              .setLngLat(feature.geometry.coordinates)
              .setDOMContent(div[0])
              .addTo(@map)



      @map.on "dragend", (e) =>
        @fetchApi()


      @map.on "zoomend", (e) =>
        @fetchApi()



  clickOnPopUp: (id) ->
    @props.hightlight(id)

  ##============================================================##
  ## a = [sw_lng,sw_lat],[ne_lng,ne_lat]
  ##============================================================##
  fetchApi: ->
    if @state.searchOnMove is true
      a = @map.getBounds().toArray()
      @props.fetchApi
        search:
          bounds: "#{a[0][1]},#{a[0][0]},#{a[1][1]},#{a[1][0]}"


  ##============================================================##
  ##
  ##
  ##============================================================##
  addLayers: ->
    ##============================================================##
    ## Add marker layer Stroke
    ##============================================================##
    @map.addLayer
      id: 'markers_stroke_layer'
      source: 'markers_source'
      type: "circle"
      paint:
        'circle-radius':
          base: 7
          stops: [[12,10],[14,22],[20, 200]]
        'circle-color':
          property: "color"
          stops: [[1,"#000"],[2,"#000"]]


    ##============================================================##
    ## Add marker layer Stroke
    ##============================================================##
    @map.addLayer
      id: 'markers_stroke_layer'
      source: 'markers_source'
      type: "circle"
      paint:
        'circle-radius':
          base: 5
          stops: [[12,8],[14,16],[20, 190]]
        'circle-color':
          property: "color"
          stops: [[1,"#d7d7d8"],[2,"#d7d7d8"]]


    ##============================================================##
    ## Add marker layer
    ##============================================================##
    @map.addLayer
      id: 'markers_layer'
      source: 'markers_source'
      type: "circle"
      paint:
        'circle-radius':
          base: 3
          stops: [[12,6],[14,10],[20, 180]]
        'circle-color':
          property: "color"
          stops: [[1,"#002663"],[2,"#fd5c63"]]


  ##============================================================##
  ##
  ##============================================================##
  themeChanged: (theme) ->
    source = @map.getSource("markers_source")

    @map.setStyle("mapbox://styles/mapbox/#{theme}-v9")

    @map.on 'style.load', () =>
      console.log "Map style: loaded"

      ##============================================================##
      ## Source
      ##============================================================##
      if @map.getSource("markers_source")? is false
        @map.addSource "markers_source",
          type: "geojson"
          data: source._data

      @addLayers()

  ##============================================================##
  ##
  ##============================================================##
  updateCheckBox: ->
    @setState
      searchOnMove: !@state.searchOnMove


  ##============================================================##
  ##
  ##============================================================##
  render: () ->
    if @map?
      if @props.apiResult?
        geojson       = _.clone(@props.apiResult.results)
        newFeatures   = []

        g1 = _.groupBy geojson.features, (feature) ->
          return feature.properties.location_geohash

        _.each g1,(g2,location_geohash) ->
          datas  = []

          _.each g2, (g3) ->
            datas.push(g3.properties)

          newFeatures.push
            type: "Feature"
            geometry: g2[0].geometry
            properties:
              grouped: datas
              index: location_geohash
              color: g2[0].properties.property_listing.id

        geojson.features = newFeatures
        @map.getSource("markers_source").setData(geojson)



    return(
      <div id='MapContainer' className="relative">
        <div id="search-when-moved" className="absolute absolute-top-right mt15 mr50 bg-white z-index-1000">
           <label className="pt0 pb0 mt5 mb5 pl10 pr10 pointer">
            <input type="checkbox" onChange={@updateCheckBox}  checked={@state.searchOnMove}  />
            <span className="normal ml5 fs13 color-grey">Search when I move the map</span>
          </label>
        </div>
        <div className="absolute absolute-top-left z-index-1000">
          <MapStyleSwitcher themes={@props.themes} theme={@props.theme} onThemeChanged={@themeChanged}/>
        </div>
        <div id="mapHere"></div>
      </div>
    )






##============================================================##
## MapStyleSwitcher
##============================================================##
MapStyleSwitcher = React.createClass
  getInitialState: ->
    return{
      theme: @props.theme
    }

  changeStyle: (theme) ->
    if theme isnt @state.theme
      @setState(theme:theme)
      @props.onThemeChanged(theme)


  render: ->
    return(
      <div className="btn-group btn-group-sm mt5 ml5" role="group">
        {@props.themes.map((name) =>
          return <button type="button" onClick={@changeStyle.bind(@,name)} className="btn btn-default #{if @state.theme == name then 'bg-primary color-white bold' else ''}" key={name}>{name}</button>
        )}
      </div>
    )
