(function ($) {
  $.fn.sameHeights = function(){
    var maxHeight = [];
    $(this).each(function(){
      $(this).css('height','');
      maxHeight.push($(this).outerHeight());
    });
    maxHeight.sort(function(a, b){ return a - b });
    $(this).each(function(){
      $(this).css({'height': maxHeight[maxHeight.length - 1]});
    });
    return this;
  }
})(jQuery);
