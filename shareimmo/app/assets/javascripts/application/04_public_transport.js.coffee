$('body.tools.geocoder').ready ->

  if google?
    autocomplete = undefined
    autocomplete = new (google.maps.places.Autocomplete)(document.getElementById('gmaps-input-address'), types: [ 'geocode' ])
    google.maps.event.addListener autocomplete, 'place_changed', ->
      place = autocomplete.getPlace()
      $('#GmapsPlaceId').val(place.place_id)

