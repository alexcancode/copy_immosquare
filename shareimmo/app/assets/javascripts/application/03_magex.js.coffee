$("body.pages.magex").ready ->
  $("#yes-email a", @).on 'click', ->
    $("body.pages.magex #content1").fadeOut 300
    setTimeout (->
      $("body.pages.magex #content2").fadeIn()
      return
    ), 300
  $("#yes-new a", @).on 'click', ->
    $("body.pages.magex #content1").fadeOut 300
    setTimeout (->
      $("body.pages.magex #content3").fadeIn()
      return
    ), 300