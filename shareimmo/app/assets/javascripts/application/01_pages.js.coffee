$(document).ready ->


  ##===================================
  ## ToopTip
  ##===================================
  $('[data-toggle="tooltip"]').tooltip()

  $('body.pages.home').ready ->
    ##===================================
    ## Video FullScreen
    ##===================================
    $(window).on 'resize', ->
      $('#videoHeroContainer').css
        height: Math.floor($(window).height()*0.99)
    .trigger('resize')

    $('#videoHeroContainer').viewportChecker
      classToAdd: 'inView'
      classToRemove: 'inView'
      repeat: true

  $('body.pages.tour').ready ->
    ##============================================================##
    ## Menu ScroolSpy
    ## ==============
    ## Cette fonction permet de sélectionner le menu active
    ## lors du scroll de le page
    ##============================================================##
    sections = new Array
    navbar_a = $('#navbar-shareimmo-level2 .scrollSpy a')
    navbar_a.each ->
      sections.push $("##{$(@).data('section')}")

    navbar_a.click (event) ->
      event.preventDefault()
      $('html, body').animate
        scrollTop: $("##{$(@).data('section')}").offset().top - $('#navbar-shareimmo').outerHeight()

    $(window).scroll ->
      scrollTop = $(@).scrollTop()+ $(window).height()*0.6
      for i of sections
        section = sections[i]
        if scrollTop > section.offset().top
          scrolled_id = section.attr('id')
      id = false
      if scrolled_id isnt id
        id = scrolled_id
        navbar_a.removeClass('active')
        $("a[data-section='#{scrolled_id}']").addClass('active')


  $('body.pages.tour').ready ->
    ##============================================================##
    ## InViewPortEffect
    ##============================================================##
    $('.image-effect-left').addClass('opacity-0').viewportChecker
      classToAdd: 'opacity-1 animated fadeInLeftBig'
      classToRemove: 'opacity-0'
      repeat: true
      offset: 10



