var player;

function onYouTubeIframeAPIReady(){
  video_id = $('#myYoutubeIframe').data('video')
  player = new YT.Player('myYoutubeIframe', {
    videoId: video_id,
    height: '1080',
    width: '1920',
    playerVars: {
      color: 'white',
      showinfo: 0,
      fs:0
    },
    events: {
      'onStateChange': onPlayerStateChange
    }
  });


  function onPlayerStateChange(event) {
    switch(event.data){
      case 0:
        player.cueVideoById(video_id)
        $("#layer-bottom").fadeIn();
        $("#layer-top").fadeOut();
      break;
      case 1:
        $("#layer-image").fadeOut();
        $("#layer-bottom").fadeOut();
        $("#layer-top").fadeIn();
      break;
    }
  }

  $("#play_video_start, #play_video_end").click(function(){
    player.playVideo();
    $('div.alert').hide();
  });

  $("iframe#myYoutubeIframe, #layer-top").hover(function(){
    $("#layer-top").css("margin-top", "40px");
  }, function(){
    $("#layer-top").css("margin-top", 0);
  });
}
