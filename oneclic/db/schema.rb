# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161202150657) do

  create_table "blog_categories", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "blogs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "blog_category_id"
    t.integer  "draft",                            default: 1
    t.string   "locale"
    t.string   "title"
    t.string   "slug"
    t.text     "content",            limit: 65535
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.index ["blog_category_id"], name: "index_blogs_on_blog_category_id", using: :btree
    t.index ["user_id"], name: "index_blogs_on_user_id", using: :btree
  end

  create_table "broker_contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "by_sms"
    t.integer  "property_id"
    t.string   "ref"
    t.string   "full_name"
    t.string   "email"
    t.string   "phone"
    t.text     "message",     limit: 65535
    t.integer  "newsletter"
    t.integer  "user_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "domains", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "website_id"
    t.integer  "property_id"
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "font_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fonts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "link_url"
    t.integer  "font_type_id"
    t.string   "css_invok"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["font_type_id"], name: "index_fonts_on_font_type_id", using: :btree
  end

  create_table "profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "color1"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "phone"
    t.string   "email"
    t.string   "company_name"
    t.string   "company_email"
    t.string   "company_phone"
    t.string   "company_address"
    t.string   "company_address2"
    t.string   "company_country"
    t.string   "company_city"
    t.string   "company_province"
    t.string   "company_zipcode"
    t.integer  "blog_active",                                default: 0
    t.integer  "page_testimonial_active",                    default: 0
    t.integer  "page_team_active",                           default: 0
    t.string   "public_name"
    t.string   "job"
    t.text     "bio",                     limit: 65535
    t.boolean  "phone_visibility"
    t.string   "facebook"
    t.string   "twitter"
    t.string   "linkedin"
    t.string   "pinterest"
    t.string   "google_plus"
    t.string   "instagram"
    t.boolean  "phone_sms"
    t.string   "baseline"
    t.text     "spoken_languages",        limit: 4294967295
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "testimonials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "full_name"
    t.text     "content",    limit: 65535
    t.integer  "status",                   default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["user_id"], name: "index_testimonials_on_user_id", using: :btree
  end

  create_table "theme_settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "theme_id"
    t.integer  "is_selected"
    t.string   "wistia_id"
    t.string   "img_id"
    t.string   "website_cover_file_name"
    t.string   "website_cover_content_type"
    t.integer  "website_cover_file_size"
    t.datetime "website_cover_updated_at"
    t.string   "website_cover_full_size_file_name"
    t.string   "website_cover_full_size_content_type"
    t.integer  "website_cover_full_size_file_size"
    t.datetime "website_cover_full_size_updated_at"
    t.integer  "with_slim_button"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["theme_id"], name: "index_theme_settings_on_theme_id", using: :btree
    t.index ["user_id"], name: "index_theme_settings_on_user_id", using: :btree
  end

  create_table "theme_translations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "theme_id"
    t.string   "locale"
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["theme_id"], name: "index_theme_translations_on_theme_id", using: :btree
  end

  create_table "themes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "with_cover"
    t.integer  "with_background"
    t.text     "description",          limit: 65535
    t.integer  "with_cover_full_size"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uid"
    t.string   "username"
    t.string   "provider"
    t.string   "provider_host"
    t.string   "oauth_token"
    t.string   "oauth_refresh_token"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "websites", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "name"
    t.integer  "theme_id"
    t.integer  "font_1_id"
    t.integer  "font_2_id"
    t.string   "color_1"
    t.string   "color_2"
    t.string   "google_analytics"
    t.string   "default_locale"
    t.boolean  "testimonial_active"
    t.boolean  "blog_active"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "mandate_website",                  default: 0
    t.text     "css",                limit: 65535
  end

end
