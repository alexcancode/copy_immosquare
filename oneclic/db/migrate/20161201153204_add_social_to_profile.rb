class AddSocialToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :phone_visibility, :boolean
    add_column :profiles, :facebook, :string
    add_column :profiles, :twitter, :string
    add_column :profiles, :linkedin, :string
    add_column :profiles, :pinterest, :string
    add_column :profiles, :google_plus, :string
    add_column :profiles, :instagram, :string
  end
end
