class CreateTestimonials < ActiveRecord::Migration[5.0]
  def change
    create_table :testimonials do |t|
      t.references :user, index: true, foreign_key: false
      t.string :title
      t.string :full_name
      t.text :content
      t.integer :status, :default => 0
      t.timestamps 
    end

    add_column :profiles, :page_testimonial_active, :integer, :default => 0
    add_column :profiles, :page_team_active, :integer,  :default => 0
  end
end
