class AddBaselineToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :baseline, :string
  end
end
