class CreateBlogs < ActiveRecord::Migration[5.0]
  def change
    create_table :blogs do |t|
			t.references :user, index: true, foreign_key: false
      t.references :blog_category, index: true, foreign_key: false
      t.integer :draft, :default=> 1
      t.string :locale
      t.string :title
      t.string :slug
      t.text :content
      t.attachment :cover

      t.timestamps
    end

    create_table :blog_categories do |t|
      t.string :name
      t.string :slug

      t.timestamps 
    end

    add_column :profiles, :blog_active, :integer, :default => 0
  end
end
