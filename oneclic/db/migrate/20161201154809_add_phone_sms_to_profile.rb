class AddPhoneSmsToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :phone_sms, :boolean
  end
end
