class AddStuffToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :public_name, :string
    add_column :profiles, :job, :string
    add_column :profiles, :bio, :text
  end
end
