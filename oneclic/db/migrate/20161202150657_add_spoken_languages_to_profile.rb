class AddSpokenLanguagesToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :spoken_languages, :text, :limit => 4294967295
  end
end
