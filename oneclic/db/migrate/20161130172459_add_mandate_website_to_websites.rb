class AddMandateWebsiteToWebsites < ActiveRecord::Migration[5.0]
  def change
    add_column :websites, :mandate_website, :integer, default: 0
  end
end
