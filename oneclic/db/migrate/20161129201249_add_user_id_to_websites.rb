class AddUserIdToWebsites < ActiveRecord::Migration[5.0]
  def change
    add_column :websites, :user_id, :integer, after: :id
  end
end
