class CreateWebsites < ActiveRecord::Migration[5.0]
  def change
    create_table :websites do |t|
      t.integer :theme_id
      t.integer :font_1_id
      t.integer :font_2_id
      t.string :color_1
      t.string :color_2
      t.string :google_analytics
      t.string :default_locale
      t.boolean :testimonial_active
      t.boolean :blog_active
    t.timestamps
	end

    create_table :domains do |t|
    	t.integer :user_id
    	t.integer :website_id	
    	t.string :name
	    t.timestamps
    end

    create_table :themes do |t|
    	t.string :name
    	t.integer :with_cover	
    	t.integer :with_background
    	t.text :description
    	t.integer :with_cover_full_size
	    t.timestamps
    end

    create_table :theme_translations do |t|
    	t.references :theme, foreign_key: false
    	t.string :locale	
    	t.text :description
	    t.timestamps
    end

    create_table :theme_settings do |t|
    	t.references :user, foreign_key: false
    	t.references :theme, foreign_key: false
    	t.integer :is_selected
    	t.string :wistia_id
    	t.string :img_id
    	t.attachment :website_cover
    	t.attachment :website_cover_full_size
    	t.integer :with_slim_button
    	t.timestamps
    end

    create_table :font_types do |t|
    	t.string :name
    	t.timestamps
    end

    create_table :fonts do |t|
    	t.string :name
    	t.string :link_url
    	t.references :font_type, foreign_key: false
    	t.string :css_invok
    	t.timestamps
    end

  end

end
