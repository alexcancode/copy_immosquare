class AddCssToWebsite < ActiveRecord::Migration[5.0]
  def change
    add_column :websites, :css, :text
  end
end
