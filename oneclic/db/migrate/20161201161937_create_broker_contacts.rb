class CreateBrokerContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :broker_contacts do |t|
      t.integer :by_sms
      t.integer :property_id
      t.string :ref
      t.string :full_name
      t.string :email
      t.string :phone
      t.text :message
      t.integer :newsletter
      t.integer :user_id, after: :id

      t.timestamps
    end
  end
end
