class AddNameToWebsite < ActiveRecord::Migration[5.0]
  def change
  	add_column :websites, :name, :string, after: :id
  end
end
