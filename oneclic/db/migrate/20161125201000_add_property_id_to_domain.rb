class AddPropertyIdToDomain < ActiveRecord::Migration[5.0]
  def change
  	add_column :domains, :property_id, :integer, after: :website_id 
  end
end
