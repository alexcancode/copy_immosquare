set :application, "oneclic"
set :repo_url, "git@bitbucket.org:wgroupe/oneclic.git"
set :rvm_ruby_version, "2.3.3@oneclic"
set :deploy_to, "/srv/apps/oneclic"
set :bundle_roles, :all
set :linked_dirs, %w{log bin tmp/pids tmp/sockets}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "oneclic_#{fetch(:stage)}" }
