require 'sidekiq/web'

Rails.application.routes.draw do

  ##============================================================##
  ## Devise does not support scoping OmniAuth callbacks under a dynamic segment
  ##============================================================##
  devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}


  ##============================================================##
  ## Sidekiq
  ##============================================================##
  mount Sidekiq::Web => '/sidekiq'

  ##============================================================##
  ## SEO
  ##============================================================##
  constraints(BrokerApp) do
    match 'robots.txt'   => 'front_office#robots', via: [:get] ,:as => 'broker_robots',  defaults: { format: "txt" }
    match 'sitemap.xml'  => 'front_office#sitemap',via: [:get] ,:as => 'broker_sitemap', defaults: { format: "xml" }
  end
  ##============================================================##
  ## Doit se trouver après les robots.txt des brokers
  ##============================================================##
  get "robots.txt"         => "pages#robots", as: "robots", defaults: { format: "txt" }
  get "sitemap.xml"        => "pages#sitemap", as: "sitemap", defaults: { format: "xml"}




  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do


    ##============================================================##
    ## Front Office
    ##============================================================##
    constraints(BrokerApp) do
      get 'robots.txt'             ,:to => redirect { |params, request| "/robots.txt" }
      get 'sitemap.xml'            ,:to => redirect { |params, request| "/sitemap.xml" }
      match 'stylesheet'           => 'front_office#stylesheet',        :via => :get, :format => [:css]
      match ''                     => 'front_office#broker_home',       :via => [:get]
      match 'listings'             => 'front_office#listings',          :via => [:get],   :as => "broker_listing"
      match 'about'                => 'front_office#about',             :via => [:get],   :as => "broker_about"
      match 'blog'                 => 'front_office#blogs',             :via => [:get],   :as => "broker_blogs"
      match 'testimonial'          => 'front_office#testimonial',       :via => [:get],   :as => "broker_testimonial"
      match 'testimonial'          => 'front_office#testimonial_new',   :via => [:post],  :as => "broker_testimonial_new"
      match 'blog/cat/:id'         => 'front_office#blog_category',     :via => [:get],   :as => "broker_blog_category"
      match 'blog/:id'             => 'front_office#broker_blog',       :via => [:get],   :as => 'broker_blog'
      match 'contact/mail'         => 'front_office#contact_by_mail',   :via => [:post],  :as => 'contact_by_mail'
      match 'contact/sms'          => 'front_office#contact_by_sms',    :via => [:post],  :as => 'contact_by_sms'
      match ':id'                  => 'front_office#broker_property',   :via => [:get],   :as => 'broker_property'
    end


    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users, skip: :omniauth_callbacks


    ##============================================================##
    ## OAuth
    ##============================================================##
    match 'users/auth/:provider/callback'   => 'authentifications#create',   :via => [:get]
    match 'users/auth/failure'              => 'authentifications#failure',  :via => [:get]
    match 'users/auth/:id'                  => 'authentifications#destroy',  :via => [:delete] ,:as => 'dashboard_delete_app'

    ##============================================================##
    ## Dashboard
    ##============================================================##
    root to: "dashboard#index"
    match "dashboard"       => "dashboard#index",           :via => [:get], :as => :dashboard
    match 'profile'         => "dashboard#profile",         :via => :get,   :as => :dashboard_profile
    
    match 'domains'         => "dashboard#manage_domains",         :via => :get,   :as => :manage_domains
    match 'domains/:id/edit'         => "dashboard#edit_domain",    :via => :get,   :as => :edit_domain
    match 'domains/:id'         => "dashboard#update_domain",    :via => :patch,   :as => :update_domain
    match 'domains'         => "dashboard#create_domain",         :via => :post,   :as => :create_domain
        
    match 'website'         => "dashboard#new_website",         :via => :get,   :as => :new_website
    match 'websites'         => "dashboard#index_websites",         :via => :get,   :as => :index_websites
    match 'websites/:id/edit'         => "dashboard#edit_website",    :via => :get,   :as => :edit_website
    match 'websites/:id'         => "dashboard#update_website",    :via => :patch,   :as => :update_website
    match 'websites'         => "dashboard#create_website",         :via => :post,   :as => :create_website
    


    match 'profile/:id'     => "dashboard#profile_update",  :via => :patch, :as => :dashboard_profile_update


    ##============================================================##
    ## Blog + testiomonials
    ##============================================================##
    match 'blog'                          => 'dashboard#blog',via: [:get] ,:as => 'dashboard_blog'
    match 'blog/new'                      => 'dashboard#blog_new',via: [:get] ,:as => 'dashboard_new_blog'
    match 'blog/:id/edit'                 => 'dashboard#blog_edit',via: [:get] ,:as => 'dashboard_edit_blog'
    match 'blog/:id/'                     => 'dashboard#blog_update',via: [:patch] ,:as => 'dashboard_update_blog'
    match 'blog/:id/'                     => 'dashboard#blog_delete',via: [:delete] ,:as => 'dashboard_delete_blog'
    match 'blog/:id/picture'              => 'dashboard#blog_remove_picture',via: [:get] ,:as => 'dashboard_blog_remove_picture'
    match 'testimonials'                  => 'dashboard#testimonials',via: [:get] ,:as => 'dashboard_testimonials'
    match 'testimonials/:id'              => 'dashboard#testimonials_delete',via: [:delete] ,:as => 'dashboard_testimonials_delete'
    match 'testimonials/:id'              => 'dashboard#testimonials_active',via: [:post] ,:as => 'dashboard_testimonials_active'
    match 'testimonials/:id/edit'         => 'dashboard#testimonials_edit',via: [:get] ,:as => 'dashboard_testimonials_edit'
    match 'testimonials/:id'              => 'dashboard#testimonials_update',via: [:patch] ,:as => 'dashboard_testimonials_update'


    ##============================================================##
    ## oneclicOrder
    ##============================================================##
    match "oneclic/:id"    => "oneclic_orders#new",    :via => [:get],     :as => :oneclic_new
    match "oneclic"        => "oneclic_orders#create",    :via => [:post],    :as => :oneclic_create
    match "confirmation/:id"        => "oneclic_orders#confirmation",    :via => [:get],    :as => :confirmation

  end

end
