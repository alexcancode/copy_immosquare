module FrontOfficeHelper

  def split_name(public_name)
    spliter = public_name.split(' ',2)
    ("<span class='color-dark-grey'>#{spliter[0]}</span> #{spliter[1]}").html_safe
  end


  def split_name_mobile(public_name)
    public_name.split(' ',2).map {|spliter| spliter[0]}.join("")
  end


end
