module ApplicationHelper

	def my_url_with_protocol(url,secure = false)
    return if url.blank?
    url = "http#{secure == true ? "s" : nil}://#{url}" unless url[/\Ahttp:\/\//] || url[/\Ahttps:\/\//]
    return url
  end
end
