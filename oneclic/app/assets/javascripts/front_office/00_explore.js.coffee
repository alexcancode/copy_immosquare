$('body.front_office.explore, body.front_office.explore_ad').ready ->
  ##============================================================##
  ## Menu
  ##============================================================##
  $('#MyHeaderHeroNav .comp')
    .on 'mouseover', ->
      $($(@).find('.drop-down-menu')).show()
    .on 'mouseout', ->
      $($(@).find('.drop-down-menu')).hide()


  if google?
    autocomplete = undefined
    autocomplete = new (google.maps.places.Autocomplete)(document.getElementById('gmaps-input-address'), types: [ 'geocode' ])
    google.maps.event.addListener autocomplete, 'place_changed', ->
      place = undefined
      place = autocomplete.getPlace()
      if !place.geometry
        window.alert 'Autocomplete\'s returned place contains no geometry'
        return
      if place.geometry.viewport
        $('#search_bounds').val place.geometry.viewport.toUrlValue()
        $('#searchFormFake').submit()
      else
        console.log place.geometry.location
      return

  ##============================================================##
  ## New Craousel with Owl Carousel
  ##============================================================##
  syncPosition = (el) ->
    current = @currentItem
    $("#sync2").find(".owl-item").removeClass("synced").eq(current).addClass "synced"
    center current  if $("#sync2").data("owlCarousel") isnt `undefined`

  center = (number) ->
    sync2visible = sync2.data("owlCarousel").owl.visibleItems
    num = number
    found = false
    for i of sync2visible
      found = true  if num is sync2visible[i]
    if found is false
      if num > sync2visible[sync2visible.length - 1]
        sync2.trigger "owl.goTo", num - sync2visible.length + 2
      else
        num = 0  if num - 1 is -1
        sync2.trigger "owl.goTo", num
    else if num is sync2visible[sync2visible.length - 1]
      sync2.trigger "owl.goTo", sync2visible[1]
    else sync2.trigger "owl.goTo", num - 1  if num is sync2visible[0]

  sync1 = $("#sync1")
  sync2 = $("#sync2")

  sync1.owlCarousel
    singleItem: true
    slideSpeed: 1000
    navigation: false
    pagination: false
    afterAction: syncPosition
    responsiveRefreshRate: 200

  sync2.owlCarousel
    items: 8
    itemsDesktop: [
      1199
      8
    ]
    itemsDesktopSmall: [
      979
      6
    ]
    itemsTablet: [
      768
      4
    ]
    itemsMobile: [
      479
      4
    ]
    pagination: false
    responsiveRefreshRate: 100
    afterInit: (el) ->
      el.find(".owl-item").eq(0).addClass "synced"
      return

  $("#sync2").on "click", ".owl-item", (e) ->
    e.preventDefault()
    number = $(this).data("owlItem")
    sync1.trigger "owl.goTo", number

  $(".shareimmo_control_left").on 'click',(e) ->
    sync1.trigger('owl.prev')

  $(".shareimmo_control_right").on 'click', (e) ->
    sync1.trigger('owl.next');