class UserMailer < ApplicationMailer
 
  def welcome_email(oneclic_order)
    @oneclic_order = oneclic_order
    mail(from: 'no-reply@immosquare.com', to: 'alex@immosquare.com', subject: 'Thank you for your purchase')
  end

end
