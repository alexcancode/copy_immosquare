class FrontOfficeMailer < ApplicationMailer

  default :template_path => "mailers/front_office_mailer"


  def broker_contact(broker_contact)
    @broker_contact = broker_contact
    @title = "Demande d'information"
    mail(
      :to       => @broker_contact.user.email,
      :bcc      => "contact@immosquare.com" ,
      :from     => set_my_from('no-reply'),
      :subject  => set_my_subject(@title)
    )
  end

  def sms_send(broker_contact)
    @broker_contact = broker_contact
    @title = "SMS"
    mail(
      :to       => @broker_contact.user.email,
      :bcc      => "contact@immosquare.com",
      :from     => set_my_from('no-reply'),
      :subject  => set_my_subject(@title)
    )
  end


  def new_testimonial_notification(testimonial)
    @testimonial = testimonial
    @title = t('back_office.testimonial')
    mail(
      :to       => @testimonial.user.email,
      :bcc      => "contact@immosquare.com",
      :from     => set_my_from('no-reply'),
      :subject  => set_my_subject(@title)
    )
  end






end
