class Theme < ApplicationRecord
	has_many :theme_translations
	has_many :theme_settings
	has_many :websites
end