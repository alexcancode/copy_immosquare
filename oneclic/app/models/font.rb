class Font < ApplicationRecord
	belongs_to :font_type
	has_many :websites
end