class Website < ApplicationRecord
	has_many :domains
	belongs_to :theme
	belongs_to :font_1, class_name: "Font", foreign_key: "font_1_id"
	belongs_to :font_2, class_name: "Font", foreign_key: "font_2_id"
	belongs_to :font
	belongs_to :user
end
