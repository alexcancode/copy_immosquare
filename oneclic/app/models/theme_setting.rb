class ThemeSetting < ApplicationRecord
  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  belongs_to :theme



  ##============================================================##
  ## Paperclip : Website Cover
  ##============================================================##
  has_attached_file :website_cover,
  :path => "#{Rails.env}/brokers/:user_id_paperclip/website_cover_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :large => ["1900x430#", :jpg]
  },
  :convert_options => {
    :large => "-background white -flatten +matte -quality 90 -strip"
  },
  :default_url => "https://placehold.it/1900x430"
  validates_attachment_content_type :website_cover, :content_type => /\Aimage/
  validates_attachment_size :website_cover, :less_than=>2.megabytes



  ##============================================================##
  ## Paperclip : Website CoverFull
  ##============================================================##
  has_attached_file :website_cover_full_size,
  :path => "#{Rails.env}/brokers/:user_id_paperclip/website_cover_full_size_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :large => ["990x660#", :jpg]
  },
  :convert_options => {
    :large => "-background white -flatten +matte -quality 90 -strip"
  },
  :default_url => "https://placehold.it/990x660"
  validates_attachment_content_type :website_cover, :content_type => /\Aimage/
  validates_attachment_size :website_cover, :less_than=>2.megabytes


end