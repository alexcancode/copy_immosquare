class Testimonial < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user

  ##============================================================##
  ## Validations
  ##============================================================##
  validates :full_name, :presence => true
  validates :title, :presence => true
  validates :content, :length => { minimum: 4}
  validates :content, :length => { maximum: 2500}

end