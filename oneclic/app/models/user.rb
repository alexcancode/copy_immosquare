class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
   has_one :profile, dependent: :destroy
   has_many :domains
   has_many :websites
   has_many :testimonials
   has_many :blogs
   has_many :broker_contacts

	validates :username, :presence => true
  validates :username, :uniqueness => {:case_sensitive => false}


  after_create :make_default_site

   devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable,:omniauthable, :omniauth_providers => [:storeimmo,:gercopstore]

   def name_to_display
    (self.profile.first_name.present? || self.profile.last_name.present?) ?  "#{self.profile.first_name} #{self.profile.last_name}" : self.email
   end

   private


  def make_default_site
  	my_website = Website.create(user_id: self.id,
                                name: "default",
															  theme_id: 1,
						  								  font_1_id: 1,
						  								  font_2_id: 1,
						  								  color_1: "red",
						  								  color_2: "blue",
						  								  default_locale: "en"
									  						)

  	  	Domain.create(user_id: self.id,
		  								name: self.username,
		  								website_id: my_website.id,
		  								name: Rails.env.production? ? (self.username + ".oneclic.immo") : (self.username + ".lvh.me:4000")
		  								)

  end

end
