class FrontOfficeController < ApplicationController


 ##============================================================##
  ## Before Filters
  ##============================================================##
  before_filter :set_domain, :except => [:contact_by_mail,:contact_by_sms,:explore,:explore_ad]
  before_filter :set_properties, :only => [:broker_home,:listings]
  # alias_method :devise_current_user, :current_user
  skip_before_action :verify_authenticity_token, :only => [:contact_by_mail,:contact_by_sms,:testimonial_new]
  after_action :allow_iframe_requests

  def robots
    render :layout => false
  end



  def sitemap
    @domain = @main_domain.present? ?  @main_domain.domain : "#{@user.username}.1clic.immo"
    ##============================================================##
    ## Statics Pages Management
    ##============================================================##
    all_locales = ['fr','en']
    @static_urls = Array.new
    properties_url = Array.new
    all_urls_root = Array.new
    all_urls_listings = Array.new
    all_urls_about = Array.new
    all_urls_blogs = Array.new

    all_locales.each do |my_locale|
      all_urls_root       << [:locale=>my_locale,:hreflang=>my_locale,:url=>root_url(:domain=>@domain,:locale=>my_locale)]
      all_urls_listings   << [:locale=>my_locale,:hreflang=>my_locale,:url=>broker_listing_url(:domain=>@domain,:locale=>my_locale)]
      all_urls_about      << [:locale=>my_locale,:hreflang=>my_locale,:url=>broker_about_url(:domain=>@domain,:locale=>my_locale)]
      all_urls_blogs      << [:locale=>my_locale,:hreflang=>my_locale,:url=>broker_blogs_url(:domain=>@domain,:locale=>my_locale)]
    end


    all_locales.each do |my_locale|
      @static_urls << [:url=>root_url(:locale=>my_locale),:changefreq=>'monthly',:priority=>0.8,:all_urls=>all_urls_root]
      @static_urls << [:url=>broker_listing_url(:locale=>my_locale),:changefreq=>'monthly',:priority=>1,:all_urls=>all_urls_listings]
      @static_urls << [:url=>broker_about_url(:locale=>my_locale),:changefreq=>'monthly',:priority=>1,:all_urls=>all_urls_about]
      @static_urls << [:url=>broker_blogs_url(:locale=>my_locale),:changefreq=>'monthly',:priority=>1,:all_urls=>all_urls_blogs]
    end


    @properties = Property.where(:status=>1,:user_id=>@user.id).where.not(:draft=>1)
    @posts = Blog.where(:user_id=>@user.id).where.not(:draft=>1).order('created_at DESC')
    render :layout => false
  end


  def stylesheet
    render :layout => false
  end

  def broker_home
    @blogs =  Blog.where(:locale=>I18n.locale.to_s,:user_id=>@user.id).where.not(:draft=>1).limit(4).order('created_at DESC')
    @blogs =  Blog.where(:user_id=>@user.id).where.not(:draft=>1).limit(4).order('created_at DESC') if @blogs.blank?
    render template: "front_office/template_#{@website.theme_id}/home"
  end

  def broker_blog
    begin
      full_slug = params[:id].split('-')
      blog_id   = full_slug.pop
      slug      = full_slug.join('-')
      @blog     = Blog.where(:id=> blog_id,:user_id => @user.id).first
      redirect_to broker_blog_path("#{@blog.slug}-#{@blog.id}") if @blog.slug != slug
    rescue
      redirect_to broker_blogs_path
    end
  end

  def blog_category
    @cat = BlogCategory.find(params[:id])
    @blogs2 = Blog.joins(:blog_category).where(:locale=>I18n.locale.to_s,:user_id=>@user.id,:blog_categories => {:id =>@cat.id}).order('created_at DESC')
    render "blogs"
  end

  def about
  end

  def testimonial
  end

  def testimonial_new
    @testimonial = Testimonial.new(params_testimonial)
    FrontOfficeMailer.new_testimonial_notification(@testimonial).deliver_later if @testimonial.save
  end


  def blogs
    @blogs  = Blog.where(:user_id=>@user.id,:locale=>I18n.locale.to_s).where.not(:draft=>1).limit(4).order('created_at DESC')
    @blogs2 = Blog.where(:user_id=>@user.id,:locale=>I18n.locale.to_s).where.not(:draft=>1).order('created_at DESC').offset(4)
    @blogs  =  Blog.where(:user_id=>@user.id).where.not(:draft=>1).limit(4).order('created_at DESC') if @blogs.blank?
    @blogs2 = Blog.where(:user_id=>@user.id).where.not(:draft=>1).order('created_at DESC').offset(4) if @blogs2.blank?
  end


  def broker_property
    response      = HTTParty.get("#{@user.provider_host}/api/v1/properties/#{params["id"]}?access_token=#{@user.oauth_token}")
    @property     = JSON.parse(response.body) if response.code == 200

    ##============================================================##
    ## Inclusions
    ##============================================================##
    # @my_inclusion = @property["other"].format


    respond_to do |format|
      format.html do

        ##============================================================##
        ## Bus Stations
        ##============================================================##
        if @property["address"]["latitude"].present? and @property["address"]["longitude"].present?
          @client = GooglePlaces::Client.new(ENV["GOOGLE_API_KEY"])
          @transport_types  = ['subway_station',"bus_station","train_station"]
          @transport_google  = @client.spots(@property["address"]["latitude"],@property["address"]["longitude"],:multipage => true,:radius=>200,:types=> @transport_types)
        end
        ##============================================================##
        ## Map
        ##============================================================##


        ##============================================================##
        ## Agent
        ##============================================================##
        # @agent = @property.property_contacts.joins(:property_contact_type).where(:property_contact_types =>{:slug => "agent"}).first

        ##============================================================##
        ## Documents
        ##============================================================##
        # @documents = Document.where(:property_id => @property.id,:visible_on_front=>1)

        render template: "front_office/template_#{@theme.id}/property"
      end
      # format.pdf do
      #   begin
      #     @myPdf       = Pdf.first_or_create
      #     dictionary   = DictionaryPopulater.new(
      #       :dictionary   =>  @myPdf.dictionary,
      #       :property_id  =>  @property.id,
      #       :locale       =>  I18n.locale
      #       ).populate_dictionary
      #     pdf_original = "#{Rails.root}/tmp/pdfs/original/#{@myPdf.document_file_name}"
      #     pdf_result   = "#{Rails.root}/tmp/pdfs/results/listing_#{@property.id}.pdf"
      #     if !File.exists?(pdf_original)
      #       FileUtils.mkdir_p(File.dirname(pdf_original))  unless File.exists?(File.dirname(pdf_original))
      #       open(pdf_original, 'wb') do |file|
      #         file.write HTTParty.get(pdf.document.url).parsed_response
      #       end
      #     end
      #     Mypdflib::PdfWritter.new(:original => pdf_original,:result => pdf_result, :dictionary => dictionary).produce
      #     send_file(pdf_result, :filename => "listing_#{@property.id}.pdf", :type => "application/pdf",:disposition => :inline)
      #   rescue Exception => e
      #     JulesLogger.info e.message
      #     flash[:error] = e.message
      #     redirect_to broker_property_path(@property.id)
      #   end
      # end
    end
  end

  def listings
    @by_line = @theme.id == 3 ? 2 : 3
  end

  def contact_by_mail
    @broker_contact = BrokerContact.new(contact_params)
    respond_to do |format|
      if @broker_contact.save
        FrontOfficeMailer.broker_contact(@broker_contact).deliver_later
        format.js
      else
        format.js
      end
    end
  end

  def contact_by_sms
    @broker_contact = BrokerContact.new(contact_params)
    @broker_contact.message = "#{t('front_office.ad')} : ##{@broker_contact.property_id}\n#{t('front_office.name')} : #{@broker_contact.full_name}\n#{t('front_office.phone')} : #{@broker_contact.phone}\n#{t('front_office.email')} : #{@broker_contact.email}\n#{t('front_office.message')} : #{@broker_contact.message}"
    profile = @broker_contact.user.profile
    respond_to do |format|
      if @broker_contact.save
        @client = Twilio::REST::Client.new ENV['twilio_sid'], ENV['twilio_token']
        @client.messages.create(
          :from => ENV['twilio_number'],
          :to   => ("+1#{profile.phone}").gsub(/\s+/, ""),
          :body => @broker_contact.message
          )
        FrontOfficeMailer.sms_send(@broker_contact).deliver_later
        format.js
      else
        format.js
      end
    end
  end





  private




  def set_domain
    begin
      if request.subdomain.blank? or request.subdomain == "www"
        secondary_domain  = Domain.find_by_name(request.domain)
        @user = secondary_domain.user
        @main_domain = @user.domains.first
        @website = secondary_domain.website
      else
        @user = User.where(:username=>request.subdomain).first
        raise "User error" if @user.blank?
        @main_domain = @user.domains.first
        @website = @main_domain.website
        # return redirect_to "http://#{@main_domain.name}" + request.path, :status => :moved_permanently if @main_domain.present?
      end
      
      ##============================================================##
      ## À partir d'ici le site est défini..
      ##============================================================##
      @theme = ThemeSetting.where(:user_id =>@user.id,:is_selected=>1).first
      if @theme.blank?
        @theme = ThemeSetting.where(:user_id =>@user.id).first_or_create
        @theme.update_attributes(:is_selected=> 1,:theme_id =>3)
      end
      @profile = @user.profile
      @mandate_website = @website.mandate_website
      @menu_1 =  Blog.where(:locale=>I18n.locale.to_s,:user_id=>@user.id,:blog_category_id=>1).where.not(:draft=>1).count
      @menu_2 =  Blog.where(:locale=>I18n.locale.to_s,:user_id=>@user.id,:blog_category_id=>2).where.not(:draft=>1).count
      @menu_3 =  Blog.where(:locale=>I18n.locale.to_s,:user_id=>@user.id,:blog_category_id=>3).where.not(:draft=>1).count
      @menu_4 =  Blog.where(:locale=>I18n.locale.to_s,:user_id=>@user.id,:blog_category_id=>4).where.not(:draft=>1).count
    rescue Exception => e
      render 'available_website'
    end
  end

  def set_properties
    url = "#{@user.provider_host}/api/v1/properties?access_token=#{@user.oauth_token}"
    response  = HTTParty.get(url)
    @response     = JSON.parse(response.body).deep_symbolize_keys! if response.code == 200
    @properties = @response[:results][:features].map { |p| p[:properties] }

    response = HTTParty.get(url+"&flag=4&per_page=10&order_by=price&order=desc")
    @properties_favorites = JSON.parse(response.body)
    @properties_favorites = @properties_favorites["results"]["features"].map { |p| p[:properties] }
    JulesLogger.info @properties_favorites
    # JulesLogger.info @properties_favorites["results"]["features"].map { |p| p[:properties] }

  end



  ##============================================================##
  ## Il faut autoriser les iframe pour que les fiches shareIMMO
  ## puisse être joué via la passerelle Centris
  ##============================================================##
  def allow_iframe_requests
    response.headers.except! 'X-Frame-Options'
  end



  # def current_user
  #   my_user = devise_current_user
  #   unless my_user.blank?
  #     if !request.subdomain.to_s.blank? and request.subdomain.to_s != "www"
  #       my_new_user = User.where(:username=>request.subdomain.to_s).select(:id).first
  #       if my_new_user
  #         if my_new_user.id == my_user.id
  #           my_user.is_owner=true
  #         end
  #       end
  #     end
  #   end
  #   my_user
  # end


  def contact_params
    params.require(:broker_contact).permit!
  end

  def params_testimonial
    params.require(:testimonial).permit(:full_name,:user_id,:title,:content)
  end


end