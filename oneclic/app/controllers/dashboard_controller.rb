class DashboardController < ApplicationController


  include ClientAuthorized

  def index
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties?access_token=#{current_user.oauth_token}")
    @response       = JSON.parse(response.body).deep_symbolize_keys! if response.code == 200
    # @oneclic_orders      = OneclicOrder.where(user_id: current_user.id)
  end


  def profile
    @profile = current_user.profile
  end

  def blog
    # return redirect_to "http://google.com"
  end

  def testimonial

  end

  ############################################
  ###Domains
  ############################################
  def manage_domains
    @domain = Domain.new
    @domains = Domain.where(user_id: current_user.id)
  end

  def edit_domain
    @domain = Domain.find(params[:id])
  end

  def update_domain
   @domain = Domain.find(params[:id])
   if @domain.update(domain_params)
    redirect_to manage_domains_path
   end
  end

  def create_domain
    @domain = Domain.new(domain_params)
    @domain.website_id = 1
    if @domain.save

      redirect_to new_website_path(id: @domain.id)
    end
  end

  ############################################
  ###Websites
  ############################################
  def index_websites
    @websites = Website.where(user_id: current_user.id)
  end

  def new_website
    @domain = Domain.find(params[:id])
    @website = Website.new
    @websites = Website.all

    @themes           = Theme.all
    @selected_theme   = ThemeSetting.where(:is_selected=>1, user_id: current_user.id).pluck(:theme_id).first
  end

  def edit_website
    @website = Website.find(params[:id])

    @themes           = Theme.all
    @selected_theme   = ThemeSetting.where(:is_selected=>1, user_id: current_user.id).pluck(:theme_id).first
    JulesLogger.info @selected_theme
  end

  def update_website
   @website = Website.find(params[:id])
   if @website.update(website_params)
    redirect_to root_path
   end
  end

  def create_website
    @domain = Domain.find(params[:domain_id])
    @website = Website.new(website_params)
    if @website.save
      @domain.website_id = @website.id
      @domain.save
      redirect_to root_path
    end
  end

  def settings_update
    @website = Website.new(website_params)
    if @website.save
      redirect_to root_path, notice: "thanks"
    end
  end

  def profile_update
    @profile = Profile.find(params[:id])
    @profile.update_attributes(profile_params)
    flash[:message] = t('app.saved')
    redirect_to dashboard_path
  end

  ##============================================================##
  ## Blog + testimonials
  ##============================================================##
  def blog
    @posts = Blog.where(:user_id=>current_user.id).where.not(:draft=>1).order('created_at DESC')
    # @posts = Blog.where(:user_id=>current_user.id).where.not(:draft=>1).order('created_at DESC').paginate(:page => params[:page], :per_page => 15)
  end

  def blog_new
    @post = Blog.where(:user_id=>current_user.id,:draft=>1).first_or_create
    redirect_to dashboard_edit_blog_path(current_user.username, @post.id)
  end

  def blog_edit
    @post = Blog.find(params[:id])
  end

  def blog_update
    @post = Blog.find(params[:id])
    @post.update_attributes(blog_params)
    respond_to do |format|
      format.html do
        redirect_to dashboard_blog_path
      end
      format.js do
        flash[:success] = "Post was saved successfully" if @post.errors.blank?
        flash[:danger] = @post.errors.full_messages if @post.errors.any?
        flash.discard
      end
    end
  end

  def blog_delete
    @post = Blog.find(params[:id])
    @post.destroy
    redirect_to dashboard_blog_path
  end

  def blog_remove_picture
    @post = Blog.find(params[:id])
    @post.cover = nil
    @post.save
  end

  def testimonials
    @testimonials = Testimonial.where(:user_id=>current_user.id).reverse_order
  end

  def testimonials_delete
    @testimonial = Testimonial.find(params[:id])
    @testimonial.destroy
    redirect_to dashboard_testimonials_path
  end

  def testimonials_active
    @testimonial = Testimonial.find(params[:id])
    @testimonial.update_attribute(:status, 1)
    redirect_to dashboard_testimonials_path
  end

  def testimonials_edit
    @testimonial = Testimonial.find(params[:id])
  end

  def testimonials_update
    @testimonial = Testimonial.find(params[:id])
    @testimonial.update(testimonial_params)
    redirect_to dashboard_testimonials_path
  end



  private

  def profile_params
    params.require(:profile).permit!
  end

  def website_params
    params.require(:website).permit!
  end

  def domain_params
    params.require(:domain).permit!
  end

  def testimonial_params
    params.require(:testimonial).permit!
  end

  def blog_params
    params.require(:blog).permit!
  end

end
