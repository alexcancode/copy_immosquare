class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController


  def gercopstore
    auth    = request.env['omniauth.auth'].to_hash.deep_symbolize_keys!
    store   = auth[:extra][:raw_info][:store]
    client  = auth[:extra][:raw_info][:client]
    company = auth[:extra][:raw_info][:company]


    ##============================================================##
    ## On create
    ##============================================================##
      my_username = client[:first_name] + client[:last_name]
      i = 1
      while User.exists?(username: my_username)
        my_username = client[:first_name] + client[:last_name] + i.to_s 
        i += 1
      end

    @user = User.where(:provider => auth[:provider], :uid => auth[:uid]).first_or_create do |user|
      user.email                    = "#{auth[:uid]}@#{auth[:provider]}.com"
      user.password                 = Devise.friendly_token[0,20]
      user.username                 = my_username



    end

    profile = Profile.where(:user_id => @user.id).first_or_create do |profile|
      profile.first_name       =  client[:first_name]
      profile.last_name        =  client[:last_name]
      profile.public_name      =  client[:first_name] + " " + client[:last_name]
      profile.email            =  client[:email]
      profile.phone            =  client[:phone]
      profile.avatar           =  open(client[:image])
      profile.company_name     =  company[:name]
      profile.company_email    =  company[:email]
      profile.company_phone    =  company[:phone]
      profile.company_address  =  company[:address]
      profile.company_address2 =  company[:address2]
      profile.company_country  =  company[:country]
      profile.company_city     =  company[:city]
      profile.company_province =  company[:province]
      profile.company_zipcode  =  company[:zipcode]
      profile.logo             =  open(company[:logo])
    end


    ##============================================================##
    ## On create/update
    ##============================================================##
    @user.update_attributes(
      :oauth_token      => auth[:credentials][:token],
      :provider_host    => store[:url]
      )



    ##============================================================##
    ## Sign in @user
    ##============================================================##
    if @user.persisted?
      sign_in_and_redirect(@user, :event => :authentication)
      set_flash_message(:notice, :success, :kind => auth[:provider]) if is_navigational_format?
    else
      session["devise.storeimmo_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end




  def failure
    flash[:message] = env["omniauth.error"]
    redirect_to root_path
  end
end
