xml.instruct!
xml.urlset(:xmlns=>"http://www.sitemaps.org/schemas/sitemap/0.9", "xmlns:xhtml"=>"http://www.w3.org/1999/xhtml") do


  @static_urls.each do |static|
    xml.url do
      xml.loc(static[0][:url])
      static[0][:all_urls].each do |tab|
        xml.tag!("xhtml:link",:rel=>'alternate',:hreflang=>tab[0][:hreflang].to_s,:href=>tab[0][:url])
      end
      xml.changefreq(static[0][:changefreq])
      xml.priority(static[0][:priority])
    end
  end

  I18n.available_locales.each do |my_locale_1|
    @properties.each do |p|
      xml.url do
        xml.loc(broker_property_url(p.id,:domain=>@domain,:locale=>my_locale_1))
        I18n.available_locales.each do |my_locale_2|
          xml.tag!("xhtml:link",:rel=>'alternate',:hreflang=>my_locale_2,:href=>broker_property_url(p.id,:domain=>@domain,:locale=>my_locale_2))
        end
        xml.changefreq('daily')
        xml.lastmod(p.updated_at.strftime("%Y-%m-%d"))
        xml.priority(1)
      end
    end
  end

    @posts.each do |p|
      xml.url do
        xml.loc(blog_show_url("#{p.slug}-#{p.id}",:locale=>p.locale,:domain=>@domain))
        xml.changefreq('monthly')
        xml.lastmod(p.updated_at.strftime("%Y-%m-%d"))
        xml.priority(0.8)
      end
    end


  end
