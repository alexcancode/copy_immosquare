json.posts do
  json.array! @blogs.each do |blog|
    json.id blog.id
    json.title blog.title
    json.cover blog.cover.url(:large)
    json.link  broker_blog_url("#{blog.slug}-#{blog.id}")
    json.date  I18n.localize blog.updated_at.to_time,:format =>("%d %b %Y")
  end
end
