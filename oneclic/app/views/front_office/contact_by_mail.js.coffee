$('.simple_form.new_broker_contact').find("div.form-group").each ->
  $(@).removeClass "has-error has-success"
  $(@).children('p.hint').remove()


<% if @broker_contact.errors.any? %>
errors = jQuery.parseJSON('<%= raw j(@broker_contact.errors.to_json) %>')
$.each errors, (key, value) ->
  $(".broker_contact_#{key}")
    .addClass("has-error")
    .append("<p class='hint'>#{value[0]}</p>")


<% else %>
$('#new_broker_contact')[0].reset()
$('#myContactModalMail').modal('hide')
$('body').prepend(
  '<div class="flashMessage">\
      <div class="container">\
        <div class="alert alert-success" role="alert">\
          <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>\
          Merci\
          </div>\
      </div>\
    </div>'
  )
<% end %>

