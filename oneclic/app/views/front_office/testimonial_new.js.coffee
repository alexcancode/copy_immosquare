<% if @testimonial.errors.any? %>
  $('#testionalForm').html("<%= j render :partial =>'front_office/partials/testimonial_form', :locals => {:testimonial => @testimonial,:user => @user} %>")
<% else %>
  $('body').prepend(
    '<div class="flashMessage">\
      <div class="container">\
        <div class="alert alert-success" role="alert">\
          <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>\
          Merci, votre témoignage devra être validé avant d’être affiché.\
          </div>\
      </div>\
    </div>'
  )
  $('#testionalForm').html("<%= j render :partial =>'front_office/partials/testimonial_form', :locals => {:testimonial => Testimonial.new,:user => @user} %>")
<% end %>
