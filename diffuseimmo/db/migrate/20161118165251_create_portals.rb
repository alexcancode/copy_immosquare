class CreatePortals < ActiveRecord::Migration[5.0]
  def change
    
    create_table :portal_types do |t|
    	t.string "name"
    	t.integer "position"
    	t.string "slug"
      t.timestamps
    end

    create_table :portals do |t|
    	t.integer  "portal_type_id"
	    t.string   "name"               
	    t.string   "slug"               
	    t.string   "url"                
	    t.string   "access_key"         
	    t.integer  "status"             
	    t.string   "portal_ad_type"     
	    t.integer  "free"               
	    t.string   "country"            
	    t.integer  "with_user_connexion"
      t.timestamps
    end

	create_table "visibility_portals" do |t|
		t.integer  "property_id"
		t.integer  "portal_id"
		t.integer  "posting_status"
		t.integer  "count", default: 0
		t.date     "visibility_start"
		t.date     "visibility_until"
		t.string   "backlink_id"
		t.string   "backlink_url"
		t.text     "history"
		t.timestamps
	end

  end
end
