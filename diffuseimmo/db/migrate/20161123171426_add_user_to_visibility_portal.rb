class AddUserToVisibilityPortal < ActiveRecord::Migration[5.0]
  def change
  	add_column :visibility_portals, :user_id, :integer, after: :id
  	add_column :visibility_portals, :picture_url, :string, after: :history
  end
end
