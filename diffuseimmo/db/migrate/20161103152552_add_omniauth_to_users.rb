class AddOmniauthToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :uid, :string, :after => :id
    add_column :users, :provider, :string, :after => :uid

  end
end
