class DashboardController < ApplicationController


  include ClientAuthorized

  def index
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties?access_token=#{current_user.oauth_token}")
    @response       = JSON.parse(response.body).deep_symbolize_keys! if response.code == 200
    # @visibility_portals = VisibilityPortal.where(user_id: current_user.id)
    @visibility_portals = VisibilityPortal.all
  end


  def profile
    @profile = current_user.profile
  end

  def profile_update
    @profile = Profile.find(params[:id])
    @profile.update_attributes(profile_params)
    flash[:message] = t('app.saved')
    redirect_to dashboard_path
  end


  ##============================================================##
  ## Visibility
  ##============================================================##
  def visibility_manage
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{params["id"]}?access_token=#{current_user.oauth_token}")
    @property = JSON.parse(response.body) if response.code == 200
    # JulesLogger.info @property["id"]
    # # @property  = Property.where(:user_id => @user.id, :id=> params[:id]).first
    if params[:international].present?
      @portals = Portal.where(:country => "int", :status => 1)
    else
      @portals = Portal.where(:country => current_user.profile.company_country, :status => 1)
    end
    @my_vps = Array.new
    @portals.each do |p|
      vp = VisibilityPortal.where(:property_id=> @property["id"],:portal_id =>p.id).last
      @my_vps << {:portal => p, :vp => vp.present? ? vp : VisibilityPortal.new}
    end
    JulesLogger.info @my_vps
  end

  ##============================================================##
  ## Visibility Show
  ##============================================================##
  def visibility_show
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{params["id"]}?access_token=#{current_user.oauth_token}")
    @property = JSON.parse(response.body) if response.code == 200
    # # @property = Property.where(:user_id => @user.id, :id=> params[:id]).first
    @vps = VisibilityPortal.joins(:portal).where("visibility_until > ?", Time.now).where(
      :property_id    => @property["id"],
      :posting_status => 1,
      :portals=>{:status =>1,:country => params[:international].present? ? "int" : current_user.profile.company_country}
      )
    JulesLogger.info @vps
  end

  ##============================================================##
  ## Visibility Update
  ##============================================================##
  def visibility_update_portal
    @vp = VisibilityPortal.where(:property_id => visibility_portal_params["property_id"], :portal_id => visibility_portal_params["portal_id"]).first_or_create
    @vp.update_attributes(visibility_portal_params)
    @count = VisibilityPortal.joins(:portal).where(:portals => {:country =>@vp.portal.country,:status => 1}).where(:property_id=> @vp.property_id,:posting_status=>1).where("visibility_until > ?", Time.now).count
  end


  ##============================================================##
  ## All Visibility
  ##============================================================##
  def all_visibility_manage
    @property_ids = params["array_of_id"]
    @portals = Portal.where(:country => current_user.profile.company_country, :status => 1)
    @my_vps = Array.new
    @property_ids.each do |prop_id|
      @portals.each do |p|
        vp = VisibilityPortal.where(:property_id=> prop_id,:portal_id =>p.id).last
        @my_vps << {:portal => p, :vp => vp.present? ? vp : VisibilityPortal.new, prop_id: prop_id}
      end
    end
    # JulesLogger.info @my_vps
  end

  ##============================================================##
  ## All Visibility Show
  ##============================================================##
  def all_visibility_show
    @property_ids = params["array_of_id"].map(&:to_i)
      @vps = VisibilityPortal.joins(:portal).where("visibility_until > ?", Time.now).where(
        :property_id    => @property_ids,
        :posting_status => 1,
        :portals=>{:status =>1,:country => params[:international].present? ? "int" : current_user.profile.company_country}
        )
  end  

  ##============================================================##
  ## All Visibility Update
  ##============================================================##
  def all_visibility_update_portal
    @property_ids = params["toto"]["property_id"].split(' ')
    @property_ids.each do |prop_id|
      response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{prop_id}?access_token=#{current_user.oauth_token}")
      this_property = JSON.parse(response.body) if response.code == 200
      @vp = VisibilityPortal.where(:property_id => prop_id, :portal_id => params["toto"]["portal_id"]).first_or_create
      @vp.update_attributes(property_id: prop_id, 
                            portal_id: params["toto"]["portal_id"],
                            visibility_start: params["toto"]["visibility_start"],
                            visibility_until: params["toto"]["visibility_until"],
                            posting_status: params["toto"]["posting_status"],
                            user_id: params["toto"]["user_id"],
                            picture_url: this_property["pictures"][0]["medium"]
                            )
      @count = VisibilityPortal.joins(:portal).where(:portals => {:country =>@vp.portal.country,:status => 1}).where(:property_id=> @vp.property_id,:posting_status=>1).where("visibility_until > ?", Time.now).count
    end
  end



  private

  def profile_params
    params.require(:profile).permit!
  end


  def visibility_portal_params
    params.require(:visibility_portal).permit!
  end
end