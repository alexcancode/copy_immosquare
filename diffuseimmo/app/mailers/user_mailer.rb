class UserMailer < ApplicationMailer
 
  def welcome_email(diffuseimmo_order)
    @diffuseimmo_order = diffuseimmo_order
    mail(from: 'no-reply@immosquare.com', to: 'alex@immosquare.com', subject: 'Thank you for your purchase')
  end

end
