class PortalType < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :portals
end