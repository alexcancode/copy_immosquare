class Portal < ApplicationRecord
  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :portal_type

  ##============================================================##
  ## portal_ad_type : Type of ad available for this portal as list separating by coma
  ## Available : sell, rent
  ##============================================================##
  def portal_ad_types
  	self.portal_ad_type.split(",")
  end
end
