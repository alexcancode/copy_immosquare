set :application, "diffuseimmo"
set :repo_url, "git@bitbucket.org:wgroupe/diffuseimmo.git"
set :bundle_roles, :all
set :rvm_ruby_version, "2.3.3@diffuseimmo"
set :deploy_to, "/srv/apps/diffuseimmo"
set :linked_dirs, %w{log bin tmp/pids tmp/sockets}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "diffuseimmo_#{fetch(:stage)}" }
