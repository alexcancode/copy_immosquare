require 'sidekiq/web'

Rails.application.routes.draw do

  ##============================================================##
  ## Devise does not support scoping OmniAuth callbacks under a dynamic segment
  ##============================================================##
  devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}


  ##============================================================##
  ## Sidekiq
  ##============================================================##
  mount Sidekiq::Web => '/sidekiq'



  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do

    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users, skip: :omniauth_callbacks


    ##============================================================##
    ## OAuth
    ##============================================================##
    match 'users/auth/:provider/callback'   => 'authentifications#create',   :via => [:get]
    match 'users/auth/failure'              => 'authentifications#failure',  :via => [:get]
    match 'users/auth/:id'                  => 'authentifications#destroy',  :via => [:delete] ,:as => 'dashboard_delete_app'

    ##============================================================##
    ## Dashboard
    ##============================================================##
    root to: "dashboard#index"
    match "dashboard"       => "dashboard#index",           :via => [:get], :as => :dashboard
    match 'profile'         => "dashboard#profile",         :via => :get,   :as => :dashboard_profile
    match 'profile/:id'     => "dashboard#profile_update",  :via => :patch, :as => :dashboard_profile_update



    ##============================================================##
    ## diffuseimmoOrder
    ##============================================================##
    # match "diffuseimmo/:id"    => "diffuseimmo_orders#new",    :via => [:get],     :as => :diffuseimmo_new
    # match "diffuseimmo"        => "diffuseimmo_orders#create",    :via => [:post],    :as => :diffuseimmo_create
    # match "confirmation/:id"        => "diffuseimmo_orders#confirmation",    :via => [:get],    :as => :confirmation
    # match "history"      => "diffuseimmo_orders#history", :via => [:get],     :as => :diffuseimmo_history


    ##============================================================##
    ## Visibility
    ##============================================================##
    match 'visibility/:id'      => 'dashboard#visibility_manage',         via: [:get],          :as => 'dashboard_visibility_manage'
    match 'visibility/:id/s'    => 'dashboard#visibility_show',           via: [:get],          :as => 'dashboard_visibility_show'
    match 'visibility/portal'   => 'dashboard#visibility_update_portal',  via: [:post,:patch],  :as => 'dashboard_visibility_update_portal'

    ##============================================================##
    ## All Visibility
    ##============================================================##
    match 'all_visibility/'      => 'dashboard#all_visibility_manage',         via: [:get],          :as => 'dashboard_all_visibility_manage'
    match 'all_visibility/s'    => 'dashboard#all_visibility_show',           via: [:get],          :as => 'dashboard_all_visibility_show'
    match 'all_visibility/portal'   => 'dashboard#all_visibility_update_portal',  via: [:post,:patch],  :as => 'dashboard_all_visibility_update_portal'

  end

end
