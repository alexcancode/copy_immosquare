Sidekiq.configure_server do |config|
  config.redis = {:namespace => "diffuseimmo_#{Rails.env}" }
end

Sidekiq.configure_client do |config|
  config.redis = {:namespace => "diffuseimmo_#{Rails.env}"}
end
