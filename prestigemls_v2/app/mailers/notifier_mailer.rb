class NotifierMailer < ApplicationMailer

  default :template_path => "mailers/notifier_mailer"

  def send_payment_link(user, payment)
    @user = user
    @payment = payment
    @title = "Payment link"
    mail(
      :from     => "noreply@prestige-mls.com",
      :to       => @user.email,
      :cc       => "guillermo@prestige-mls.com",
      :subject  => set_my_subject(@title)
      )
  end

end
