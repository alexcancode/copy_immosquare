class Payment < ApplicationRecord

  attr_accessor :number, :recurrence

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :invoice

  ##============================================================##
  ## Money
  ##============================================================##
  monetize :amount_cents, :with_model_currency  => :currency

end
