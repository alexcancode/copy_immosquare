class Invoice < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  has_many :payments, dependent: :destroy


  ##============================================================##
  ## Money
  ##============================================================##
  monetize :amount_cents, :with_model_currency  => :currency

  def is_paid?
    self.payments.each do |p|
      return false if p.is_paid == 0
    end
    return true
  end


  ##============================================================##
  ## Paperclip
  ##============================================================##
  has_attached_file :file, :path => "#{Rails.env}/invoices/:user_id/invoice_:id.:extension"
  validates_attachment_file_name :file, matches: [/png\z/, /jpe?g\z/, /mpeg\z/, /mpeg4\z/, /pdf\z/, /doc?x\z/, /txt\z/, /odt\z/]
  validates_attachment_size :file, :less_than=>5.megabytes

  ##============================================================##
  ## Paperclip Interpolates
  ##============================================================##
  Paperclip.interpolates('user_id') do |attachment, style|
    attachment.instance.user_id
  end

end