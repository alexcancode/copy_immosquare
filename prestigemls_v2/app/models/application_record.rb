class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # Returns an array of all currency id
  def self.currency_codes
    my_currencies = [:eur, :cad]
    currencies = []
    my_currencies.each do |c|
      currencies << Money::Currency.find(c)
    end
    currencies
  end
end
