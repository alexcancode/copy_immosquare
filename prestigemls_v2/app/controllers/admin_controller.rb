class AdminController < ApplicationController
  include ActionView::Helpers::DateHelper

  before_filter :authenticate_user!
  before_filter :check_admin

  layout "admin"


  ##============================================================##
  ## Invoices
  ##============================================================##
  def invoices
    @invoices = Invoice.where(:draft => 0)
  end

  def invoice_new
    invoice = Invoice.where(:draft => 1).first_or_create
    redirect_to admin_invoices_edit_path(invoice.id)
  end

  def invoice_edit
    @invoice  = Invoice.find(params[:id])
  end

  def invoice_update
    @invoice = Invoice.find(params[:id])
    @invoice.update_attributes(invoices_params)
    flash[:success] = "Invoice was saved"
    redirect_to admin_invoices_edit_path(@invoice.id)
  end

  def invoice_delete
    @invoice = Invoice.find(params[:id])
    @invoice.destroy
    flash[:success] = "Invoice was deleted"
    redirect_to admin_invoices_path
  end


  ##============================================================##
  ## Payment
  ##============================================================##
  def payments
    @payments = Payment.all.order(:is_paid, :due_date)
  end

  def payment_new
    @invoice = Invoice.find(params[:invoice_id])
    @payment = Payment.new
  end

  def payment_edit
    @invoice = Invoice.find(params[:invoice_id])
    @payment  = Payment.find(params[:id])
  end

  def payment_update
    if params[:id].present?
      payment = Payment.find(params[:id])
      payment.update_attributes(payments_params)
    else
      duplicate_number  = params[:payment][:number].present? ? params[:payment][:number].to_i : 1
      original_due_date = params[:payment][:due_date].present? ? DateTime.parse(params[:payment][:due_date]) : DateTime.now
      (1..duplicate_number).each do |n|
        due_date = case params[:payment][:recurrence]
        when "year"
          original_due_date+(n-1).year
        when "month"
          original_due_date+(n-1).month
        else
          original_due_date
        end
        payment = Payment.new
        payment.update_attributes(payments_params)
        payment.update_attributes(:secure_id => Payment.generate_unique_secure_token, :due_date => due_date)
      end
    end
    flash[:success] = "Payment was saved"
    redirect_to admin_invoices_edit_path(payment.invoice_id)
  end

  def payment_send
    @invoice = Invoice.find(params[:invoice_id])
    @payment = Payment.find(params[:id])

    NotifierMailer.send_payment_link(@invoice.user, @payment).deliver_now

    flash[:success] = "Payment was successfuly sent"
    redirect_to admin_invoices_path
  end

  def payment_delete
    @payment = Payment.find(params[:id])
    invoice_id = @payment.invoice_id
    @payment.destroy
    flash[:success] = "Payment was deleted"
    redirect_to admin_invoices_edit_path(invoice_id)
  end

  ##============================================================##
  ## Leads
  ##============================================================##
  def leads
    url = root_url + "/api/v1/search/leads?apiKey=123&apiToken=456"
    response = HTTParty.get(url)
    @leads = JSON.parse(response.body)
    @leads["myleads"] = @leads["myleads"].sort_by {|l| l["created_at"]}.reverse.group_by {|x| time_ago_in_words(x["created_at"])}
  end


  private

  def check_admin
    redirect_to root_path if !current_user.has_any_role? :admin
  end

  def invoices_params
    params.require(:invoice).permit!
  end

  def payments_params
    params.require(:payment).permit!
  end


end
