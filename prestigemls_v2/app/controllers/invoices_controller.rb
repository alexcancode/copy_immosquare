class InvoicesController < ApplicationController

  before_filter :authenticate_user!

  def index
    @invoices = Invoice.where(:user_id => current_user.id, :draft => 0)
  end

  def payments
    @payments = Payment.includes(:invoice).where(:invoices => {:user_id => current_user.id}).order(:is_paid, :due_date)
  end


  def make_payment
    begin
      @payment = Payment.where(:secure_id => params[:id]).first
      raise "Payment not found" if @payment.blank?

      return redirect_to payment_made_path(@payment.secure_id) if @payment.is_paid == 1

      if @payment.invoice.user.stripe_id.present?
        customer      = Stripe::Customer.retrieve(@payment.invoice.user.stripe_id)
        @cards        = customer.sources.all(:object => "card")
        @default_card = customer.default_source
      end
    rescue Exception => e
      flash[:danger] = e.message
      redirect_to root_path
    end
  end

  def payment_made
    begin
      @payment = Payment.where(:secure_id => params[:id]).first
      raise "Payment not found" if @payment.blank?

    rescue Exception => e
      flash[:danger] = e.message
      redirect_to root_path
    end
  end


  def stripe
    begin
      @payment = Payment.where(:secure_id => params[:id]).first
      raise "Payment not found" if @payment.blank?

      customer = Stripe::Customer.retrieve(@payment.invoice.user.stripe_id)

      charge = Stripe::Charge.create(
        :amount       => @payment.amount_cents,
        :currency     => @payment.currency,
        :source       => params[:pay][:stripe_id],
        :customer     => customer.id,
        :description => "Prestige MLS - Invoice #{@payment.invoice.invoice_number} - Payment #{@payment.id}"
        )

      raise "Error Charge" if charge.id.blank?

      @payment.update_attributes(:is_paid => 1, :stripe_id => charge.id, :payment_date => DateTime.now)

      flash[:notice] = "Thank you"
      redirect_to payment_made_path(@payment.secure_id)

    rescue Stripe::CardError => e
      flash[:danger] = e.message
      redirect_to root_path
    rescue Exception => e
      flash[:danger] = e.message
      redirect_to root_path
    end
  end

end
