module Api
  module V1
    class SearchsController < Api::V1::ApplicationController


      def leads
        begin
          raise "Your ApiKey/ApiToken are not authorized for search"  if @api_app.search_authorized_users.blank?
          params[:user] = @api_app.search_authorized_users            if @api_app.search_authorized_users != "*"
          
          if params[:id].present?
            @leads = LeadInstapage.where("id > ?", params[:id])
          else
            @leads = LeadInstapage.all
          end
           
          # @response = LeadInstapage.my_search_elastic(params)
          # @index = "listings"
          # @leads = LeadInstapage.all
          render 'api/v1/leads/leads'
        rescue Exception => e
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end



      def leads_show
        begin
          raise "Your ApiKey/ApiToken are not authorized for search" if @api_app.search_authorized_users.blank?
          @lead = LeadInstapage.find(params[:id])
          if @api_app.search_authorized_users != "*"
          #   raise "Not authorized" if !@api_app.search_authorized_users.split(',').include?(@lead.user_id.to_s)
          end
          render 'api/v1/leads/retrieve'
        rescue Exception => e
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end

    end
  end
end
