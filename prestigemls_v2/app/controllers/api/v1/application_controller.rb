module Api
  module V1
    class ApplicationController < ActionController::Base


      ##============================================================##
      ## Before & After
      ##============================================================##
      before_action :check_credentials_in_header_or_url


      def check_credentials_in_header_or_url
        begin
          if request.headers["apiKey"].blank? or request.headers["apiToken"].blank?
            raise "No Credentials Found" if(params["apiKey"].blank? or params["apiToken"].blank?)
            @api_app = ApiApp.where(:key=>params["apiKey"],:token=>params["apiToken"]).first
            raise "Not Authorized" if @api_app.blank?
          else
            @api_app = ApiApp.where(:key=>request.headers["apiKey"],:token=>request.headers["apiToken"]).first
            raise "Not Authorized" if @api_app.blank?
          end
        rescue Exception => e
          return render :json =>{:errors=>e.message}, :status=> 402
        end
      end





    end
  end
end
