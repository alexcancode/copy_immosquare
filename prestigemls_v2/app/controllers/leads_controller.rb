class LeadsController < ApplicationController
  include ActionView::Helpers::DateHelper

  layout "leads"


  def home
  end



  def referrals_market
  end

  def details_referral
  end

  def details_referral_open
  end

  def acceptation_referral
  end

  def my_referrals
  end

  def my_referrals_sent
  end

  def send_referral
  end

  def send_referral_step2
  end

  def send_referral_step3
  end

  def referral_sent
  end

  def details_leads
  end

  def leads_credit
  end


  def index

  end

  def show
    url = root_url + "/api/v1/search/leads/#{params[:id]}?apiKey=123&apiToken=456"
    response = HTTParty.get(url)
    @lead = JSON.parse(response.body)
  end

  def show_signedin
    url = root_url + "/api/v1/search/leads/#{params[:id]}?apiKey=123&apiToken=456"
    response = HTTParty.get(url)
    @lead = JSON.parse(response.body)
  end

  #####################################################
  ## Function to save leads from instapage.
  ## How to use :
  ## 1. Create a form with instapage redirecting to this page : "http://gateway.opus44.com/"
  ## 2. Add a hidden field named "redirect_url" (typo is important) and with the value of the page you would like to land on after this treatment.
  ## 4. These fields are mandatory : first_name, last_name, email, phone
  ## 3. Enjoy
  #####################################################
  def new
    begin
      submission_id = params["submission"]
      url = "https://app.instapage.com/api/1/submission/#{submission_id}"

      @response = HTTParty.get(url, :headers => {"TOKEN" => ENV["instapage_token"],"ACCESS" => ENV["instapage_access_token"]})

      datas = {}
      @response["data"]["fields"].map { |f| datas[f["name"]] = f["value"] }

      @lead = LeadInstapage.where(:uid => submission_id).first_or_create

      datas.keys.each do |key|
        datas.keys.each do |k|
          if key == datas[k]
            datas[k] = datas.delete(key)
          end
        end
      end

      @lead.update_attributes(:datas => datas)

      if datas["redirect_url"].present?
        redirect_to datas["redirect_url"]
      else
        redirect_to :back
      end

    rescue Exception => e
      puts "error #{e.message}"
      JulesLogger.info e.message
      redirect_to :back
    end
  end

end
