$("body.credit_cards").ready ->

  ##============================================================##
  ## On CreditCard Modal show
  ##============================================================##
  $('#new_credit_card_modal').on 'hide.bs.modal', (e) ->
    form = $("#stripeCardForm")
    form[0].reset()
    form.find('.payment-errors').addClass('hide').removeClass('animated ').html('')


  ##============================================================##
  ## Validate
  ##============================================================##
  Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'))
  $('#stripeCardForm').on 'submit',(event) ->
    event.preventDefault()
    form = $(event.currentTarget)
    form.find('.payment-errors').addClass('hide').removeClass('animated ').html('')
    form.find('input[type="submit"]').prop('disabled',true)
    Stripe.card.createToken(form, stripeResponseHandler)

  stripeResponseHandler = (status, response)->
    form = $('#stripeCardForm')
    if response.error
      form.find('input[type="submit"]').prop('disabled',false)
      form.find('.payment-errors').removeClass('hide').addClass('animated').html(response.error.message)
    else
      $('#credit_card_stripeToken').val(response.id)
      form[0].submit()


