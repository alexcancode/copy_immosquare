class RedoInvoiceTable < ActiveRecord::Migration[5.0]
  def change
    drop_table :payments
    drop_table :invoices

    create_table :invoices do |t|
      t.integer :user_id
      t.integer :draft, :default => 1
      t.string :invoice_number
      t.date :invoice_date
      t.string :currency
      t.money :amount, currency: { present: false }

      t.timestamps
    end

    create_table :payments do |t|
      t.integer :invoice_id
      t.string :stripe_id
      t.date :payment_date
      t.integer :is_paid, :default => 0
      t.string :currency
      t.money :amount, currency: { present: false }

      t.timestamps
    end
  end
end
