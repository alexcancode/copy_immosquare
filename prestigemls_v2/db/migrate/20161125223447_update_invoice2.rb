class UpdateInvoice2 < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :is_paid, :integer, :default => 0, :after => :currency
  end
end
