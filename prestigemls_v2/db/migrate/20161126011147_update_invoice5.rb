class UpdateInvoice5 < ActiveRecord::Migration[5.0]
  def change
        add_column :payments,  :next_payment_date, :date
  end
end
