class AddDueDateToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :due_date, :date, :after => :stripe_id
  end
end
