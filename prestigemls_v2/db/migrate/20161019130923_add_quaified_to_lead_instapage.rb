class AddQuaifiedToLeadInstapage < ActiveRecord::Migration[5.0]
  def change
    add_column :lead_instapages, :qualified?, :boolean, after: :uid
  end
end
