class AddViewAllLeadsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :view_all_leads?, :boolean, after: :email
  end
end
