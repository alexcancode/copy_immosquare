class AddSecureIdToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :secure_id, :string, :after => :id
  end
end
