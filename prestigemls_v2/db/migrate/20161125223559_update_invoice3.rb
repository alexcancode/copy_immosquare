class UpdateInvoice3 < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :invoice_number, :string, :after => :is_paid
  end
end
