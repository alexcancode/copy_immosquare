class AddDraftToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :draft, :integer, :default => 1, :after => :id
  end
end
