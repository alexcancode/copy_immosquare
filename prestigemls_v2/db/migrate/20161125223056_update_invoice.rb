class UpdateInvoice < ActiveRecord::Migration[5.0]
  def change
    change_column :invoices, :amount_cents, :integer, :after => :id
  end
end
