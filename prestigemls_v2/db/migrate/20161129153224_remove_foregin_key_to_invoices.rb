class RemoveForeginKeyToInvoices < ActiveRecord::Migration[5.0]

  def change
    remove_reference :invoices, :user, :foreign_key => true
    add_column :invoices, :user_id, :integer, :after => :id
  end
end
