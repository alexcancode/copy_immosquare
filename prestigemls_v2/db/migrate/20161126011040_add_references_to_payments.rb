class AddReferencesToPayments < ActiveRecord::Migration[5.0]
  def change
    add_reference :payments, :invoice, foreign_key: false, :after => :id
  end
end
