class RenameTablesInvocies < ActiveRecord::Migration[5.0]
  def change
    rename_table :invoices, :payments
  end
end
