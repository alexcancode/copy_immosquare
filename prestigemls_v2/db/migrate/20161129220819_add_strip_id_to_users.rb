class AddStripIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :stripe_id, :string, :after => :email
  end
end
