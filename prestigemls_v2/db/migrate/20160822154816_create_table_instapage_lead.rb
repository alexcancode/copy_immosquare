class CreateTableInstapageLead < ActiveRecord::Migration[5.0]
  def change
    create_table(:lead_instapages) do |t|
      t.integer :uid
      t.text :datas
      t.text :clean_datas

      t.timestamps
    end
  end
end