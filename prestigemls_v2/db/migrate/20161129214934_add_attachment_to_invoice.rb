class AddAttachmentToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_attachment :invoices, :file
  end
end
