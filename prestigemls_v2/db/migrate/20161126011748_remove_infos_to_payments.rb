class RemoveInfosToPayments < ActiveRecord::Migration[5.0]
  def change
    remove_column :payments, :name, :string
    remove_column :payments, :description, :name
    rename_column :payments, :next_payment_date, :payment_date
  end
end
