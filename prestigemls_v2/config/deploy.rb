set :application, "prestigemls"
set :rvm_ruby_version, "2.3.2@prestigemls"
set :repo_url, "git@bitbucket.org:wgroupe/prestigemls_v2.git"
set :bundle_roles, :all
set :deploy_to, "/srv/apps/prestigemls"
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

