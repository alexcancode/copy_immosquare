Rails.application.routes.draw do

  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do

    ##============================================================##
    ## Admin
    ##============================================================##
    scope "admin" do
      match "payments"            => "admin#payments",        :via => [:get],           :as => "admin_payments"
      scope "invoices" do
        match ""                  => "admin#invoices",        :via => [:get],           :as => "admin_invoices"
        match "/new"              => "admin#invoice_new",     :via => [:get],           :as => "admin_invoices_new"
        match "/:id"              => "admin#invoice_edit",    :via => [:get],           :as => "admin_invoices_edit"
        match "/:id"              => "admin#invoice_update",  :via => [:patch],         :as => "admin_invoices_update"
        match "/:id"              => "admin#invoice_delete",  :via => [:delete],        :as => "admin_invoices_delete"

        scope ":invoice_id/payments" do
          match "/new"            => "admin#payment_new",     :via => [:get],           :as => "admin_payment_add"
          match "/:id"            => "admin#payment_edit",    :via => [:get],           :as => "admin_payment_edit"
          match "/(:id)"          => "admin#payment_update",  :via => [:post, :patch],  :as => "admin_payment_update"
          match "/send/:id"       => "admin#payment_send",    :via => [:get],           :as => "admin_payment_send"
          match "/:id"            => "admin#payment_delete",  :via => [:delete],        :as => "admin_payment_delete"
        end
      end
      scope "leads" do
        match ''                  => 'admin#leads',           :via => :get,             :as => :lead_instapages
      end
    end

    ##============================================================##
    ## Payment
    ##============================================================##
    constraints(subdomain: "payment") do
      match "/pay/:id"      => "invoices#make_payment",  :via => [:get],    :as => "make_payment"
      match "/thanks/:id"   => "invoices#payment_made",  :via => [:get],    :as => "payment_made"
      match "/pay/:id"      => "invoices#stripe",        :via => [:post],   :as => "payment_stripe"
    end


    match "/invoices"         => "invoices#index",         :via => [:get],    :as => "my_invoices"
    match "/payments"         => "invoices#payments",      :via => [:get],    :as => "my_payments"


    ##============================================================##
    ## Lead
    ##============================================================##
    constraints(subdomain: "lead") do
      match ''                        => 'leads#home',                     :via => :get, :as => :instapage_2
      match '/referrals_market'       => 'leads#referrals_market',         :via => :get, :as => :referrals_market
      match '/details_referral'       => 'leads#details_referral',         :via => :get, :as => :details_referral
      match '/details_referral_open'  => 'leads#details_referral_open',    :via => :get, :as => :details_referral_open
      match '/acceptation_referral'   => 'leads#acceptation_referral',     :via => :get, :as => :acceptation_referral
      match '/my_referrals'           => 'leads#my_referrals',             :via => :get, :as => :my_referrals
      match '/my_referrals_sent'      => 'leads#my_referrals_sent',        :via => :get, :as => :my_referrals_sent
      match '/send_referral'          => 'leads#send_referral',            :via => :get, :as => :send_referral
      match '/send_referral_step2'    => 'leads#send_referral_step2',      :via => :get, :as => :send_referral_step2
      match '/send_referral_step3'    => 'leads#send_referral_step3',      :via => :get, :as => :send_referral_step3
      match '/referral_sent'          => 'leads#referral_sent',            :via => :get, :as => :referral_sent
      match '/details_leads'          => 'leads#details_leads',            :via => :get, :as => :details_leads
      match '/leads_credit'           => 'leads#leads_credit',             :via => :get, :as => :leads_credit
      match '/new'                    => 'leads#new',                      :via => :get, :as => :instapage
      match '/:id'                    => 'leads#show',                     :via => :get, :as => :show_lead
      match '/signedin/:id'           => 'leads#show_signedin',            :via => :get, :as => :show_signedin
    end


    ##============================================================##
    ## Credit cards
    ##============================================================##
    scope "credit_cards" do
      match "/"                 => "credit_cards#index",          :via => [:get],    :as => "credit_cards"
      match "card"              => "credit_cards#card_new",       :via => [:post],   :as => "credit_cards_new"
      match "card/:stripe_id"   => "credit_cards#card_default",   :via => [:post],   :as => "credit_cards_default"
      match "card/:stripe_id"   => "credit_cards#card_delete",    :via => [:delete], :as => "credit_cards_delete"
    end

    ##============================================================##
    ## API
    ##============================================================##
    namespace :api, defaults: {format: 'json'} do
      namespace :v1 do
        match 'search/leads'       => 'searchs#leads',        :via => :get
        match 'search/leads/:id'   => 'searchs#leads_show',   :via => :get
      end
    end

    root "beta#beta"


    ##============================================================##
    ## Devise
    ##============================================================##
    resource :user, only: [:edit] do
      collection do
        get "edit"
        patch 'update_password'
      end
    end
    devise_for :users



  end

end
