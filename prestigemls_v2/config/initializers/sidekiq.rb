Sidekiq.configure_server do |config|
  config.redis = {:namespace => "mcg_#{Rails.env}" }
end

Sidekiq.configure_client do |config|
  config.redis = {:namespace => "mcg_#{Rails.env}"}
end
