module MailerBefore
  def before(hash)
    hash.keys.each do |key|
      define_method key.to_sym do
        eval " @#{key} = hash[key] "
      end
    end
  end
end

class ActionMailer::Base
  extend MailerBefore
end

class ActionController::Base
  before_filter :mailer_knows
  def mailer_knows
    ActionMailer::Base.before({
      :request => request
    })
  end
end
