# encoding: utf-8

require 'uri'
require 'open-uri'
require 'net/imap'
require 'capybara/poltergeist'
require 'selenium-webdriver'

Encoding.default_external = Encoding::UTF_8

  ##============================================================##
  ## Setup #1 Capybara with selenium_remote_firefox
  ##============================================================##
  Capybara.register_driver :selenium_remote_firefox do |app|

    profile = Selenium::WebDriver::Firefox::Profile.new
    # profile['general.useragent.override'] = "ipad"

    profile['capability.policy.default.Window.QueryInterface'] = "allAccess"
    profile['capability.policy.default.Window.frameElement.get'] = "allAccess"
    # profile['app.update.enabled'] = false
    # profile.native_events = true
    # profile.assume_untrusted_certificate_issuer = false

    # if @scraper[:is_js_disabled] == true
    profile['javascript.enabled'] = false # dÃ©sactive le javascript quand le scraper est configurÃ© pour fonctionner sans
    # end

    # Taken from https://code.google.com/p/selenium/wiki/AdvancedUserInteractions#Native_events_versus_synthetic_events
    # Native events versus synthetic events
    # In WebDriver advanced user interactions are provided by either simulating the Javascript events directly (i.e. synthetic events) or by letting
    # the browser generate the Javascript events (i.e. native events). Native events simulate the user interactions better whereas synthetic events
    # are platform independent, which can be important in Linux when alternative window managers are used, see native events on Linux. Native events
    # should be used whenever it is possible.
    # These are some examples where native events behave different to synthetic events:
    # - With synthetic events it is possible to click on elements which are hidden behind other elements. With native events the browser sends the
    # click event to the top most element at the given location, as it would happen when the user clicks on the specific location.
    # - When a user presses the 'tab' key the focus jumps from the current element to the next element. This is done by the browser.
    # With synthetic events the browser does not know that the 'tab' key is pressed and therefore won't change the focus. With native events the
    # browser will behave as expected.

    # Apparently the cache breaks loading jquery from google on some occasions... let's disable it
    # profile['browser.cache.disk.enable'] = false
    # profile['browser.cache.memory.enable'] = false
    # profile['browser.cache.offline.enable'] = false
    # profile['network.http.use-cache'] = false
    # caps with user_agents

    # try to use adblock on duproprio
    # if @message_body[:url] == "http://duproprio.com"
    #   profile.add_extension('/tmp/adblockplusfirefox.xpi')
    #   profile["extensions.blocklist.enabled"] = false # cf: https://github.com/jfirebaugh/capybara-firebug/blob/master/lib/capybara/firebug.rb
    #   profile["extensions.adblockplus.enabled"] = true
    # end

    # disable firefox automatic updates
    # profile["app.update.auto"] = false
    # profile["app.update.enabled"] = false

    caps = Selenium::WebDriver::Remote::Capabilities.firefox(:proxy => Selenium::WebDriver::Proxy.new(:http => "#{ENV['SCRAPER.PROXY_URL']}"), :firefox_profile => profile, :accept_ssl_certs => true)
    # caps = Selenium::WebDriver::Remote::Capabilities.firefox(:firefox_profile => profile, :accept_ssl_certs => true)
    #caps without user_agents
    #caps = Selenium::WebDriver::Remote::Capabilities.firefox(:proxy => Selenium::WebDriver::Proxy.new(:http => proxy_url))
    Capybara::Selenium::Driver.new(app,
     :browser => :remote,
     :desired_capabilities => caps,
     :url => ENV['SCRAPER.GRID_HUB_URL'])
  end

#   Capybara.register_driver :chrome do |app|
#     profile = Selenium::WebDriver::Chrome::Profile.new
#     profile['extensions.password_manager_enabled'] = false

#     options = Selenium::WebDriver::Chrome::Options.new
#     options.AddExtension("#{Rails.root}/resources/rapportive.crx") # Path.GetFullPath("local/path/to/extension.crx"));

# Capybara::Selenium::Driver.new(app, :browser => :chrome, profile: profile, options: options)
# end


  ##============================================================##
  ## Setup #2 Capybara with poltergeist
  ##============================================================##
  puts "#{ENV['SCRAPER.PROXY_URL']}"
  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, { :phantomjs_options => ["--proxy={ENV['SCRAPER.PROXY_URL']}"], :js_errors => false})
  end


  # Capybara.javascript_driver  = :selenium_remote_firefox
  # Capybara.default_driver     = :selenium_remote_firefox
  Capybara.javascript_driver  = :poltergeist
  Capybara.default_driver     = :poltergeist
  
  browser = Capybara.current_session

  screenshots_path        = File.join(Rails.root,'tmp','linkedin','screenshots')




  namespace :lead do

    task :import => :environment do

      begin
        puts "lead:import - instapage started"    
        url = "https://app.instapage.com/submissions/index/1754712/1/100"

        browser.visit(url)
        browser.all(:xpath, '//*[@id="email"]').first.set("contact@immosquare.com")
        sleep(rand(2))

        browser.all(:xpath, '//*[@id="password"]').first.set("M@gexTechno")
        sleep(rand(2))
        browser.all(:xpath, '//*[@type="submit"]').first.click
        sleep(3)

        browser.visit(url)
        sleep(3)

        @submissions = []

        @next_button = browser.all(:xpath, '//*[@class="i-page-next"]')

        i = 0
        while @next_button.present?
          @next_button = browser.all(:xpath, '//*[@class="i-page-next"]')

          entries = browser.all(:xpath, '//*[@id="submissions-tbl"]/tbody/tr')
          entries.each do |tr|
            submission_id = tr[:id].gsub("email_", "").gsub("_content", "")
            next if submission_id.nil?

            @submissions << submission_id         
          end if entries.present?
          puts "#{@submissions.count} submissions"
          @next_button.first.click if @next_button.first.present?
          sleep(2)
          i+=1    
          puts i
        end

        @submissions.each do |submission_id|
          browser.visit "http://gateway.lvh.me:4600?submission_id=#{submission_id}"
        end

        screenshot = File.join(screenshots_path,"#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.png")      
        browser.save_screenshot(screenshot)

        puts "lead:import - instapage ended"

      rescue Exception => e
        puts "error"
        screenshot = File.join(screenshots_path,"error_#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.png")      
        browser.save_screenshot(screenshot)
        DevLogger.info e
      end

    end

    task :clean => :environment do
      begin
        @geoip = GeoIP.new(Rails.root.join("resources/GeoLiteCity.dat"))
        @corresponding_fields = { "VOTRE ADRESSE COURRIEL..." => "email", "Prénom" => "first_name", "Nom" => "first_name", "Téléphone" => "phone", "Langue" => "language", "Last Name" => "last_name", "First Name" => "first_name", "Phone Number" => "phone", "YOUR EMAIL ADDRESS..." => "email" }

        puts "lead:clean - scrape started"
        puts "Robot IP : #{check_proxy(browser)}"





        # browser.visit("https://mail.google.com/mail/u/0/#inbox")
        # sleep(2)
        # browser.all(:xpath, '//*[@id="Email"]').first.set("quentin.degrange")
        # browser.all(:xpath, '//*[@id="next"]').first.click
        # sleep(2)
        # browser.all(:xpath, '//*[@id="Passwd"]').first.set("Quent1250793")
        # browser.all(:xpath, '//*[@id="signIn"]').first.click
        # sleep(2)

        # browser.all(:xpath, '//div[@class="aic"]/*/div').first.click
        # sleep(5)

        # browser.all(:xpath, '//td[@class="ok"]/div[@class="o1"]/span').first.click


        # browser.all(:xpath, '//textarea[@name="to"]').first.set("quentin.degrange@gmail.com")
        # sleep(2)

        # screenshot = File.join(screenshots_path,"#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.png")
        # browser.save_screenshot(screenshot)





        # Add linkedin
        browser.visit("https://www.linkedin.com")

        browser.all(:xpath, '//*[@id="login-email"]').first.set("emilie.tygreat@gmail.com")
        browser.all(:xpath, '//*[@id="login-password"]').first.set("Immo460Immo")
        browser.all(:xpath, '//*[@name="submit"]').first.click

        @leads = LeadInstapage.where("updated_at > :date and clean_datas is null", {:date => Time.now-24.hours}).limit(10)
        @leads.each do |lead|
          clean_datas = { :custom_fields => {}, :socials => [], :geographic => {} }
          puts "Test lead #{lead.id} for uid #{lead.uid}"

          JSON.parse(lead.datas).each do |key, value|
            ( @corresponding_fields[key].present? ? clean_datas[@corresponding_fields[key]] = value : clean_datas[:custom_fields][key] = value )
          end if lead.datas.present?
          puts clean_datas["email"]

          # Add full_contact
          clean_datas[:full_contact] = get_fullcontact_datas(clean_datas)

          # Get socials
          clean_datas[:full_contact][:socials].each do |social|
            clean_datas[:socials] << social
          end if clean_datas[:full_contact][:socials].present?

          clean_datas[:geographic][:country_name] = clean_datas[:full_contact][:demographics][:country_name] if clean_datas[:full_contact][:demographics][:country_name].present?
          clean_datas[:geographic][:country_code] = clean_datas[:full_contact][:demographics][:country_code].downcase if clean_datas[:full_contact][:demographics][:country_code].present?
          clean_datas[:geographic][:continent] = clean_datas[:full_contact][:demographics][:continent].downcase if clean_datas[:full_contact][:demographics][:continent].present?
          clean_datas[:geographic][:city_name] = clean_datas[:full_contact][:demographics][:city_name].downcase if clean_datas[:full_contact][:demographics][:city_name].present?
          clean_datas[:geographic][:ip] = clean_datas[:custom_fields]["ip"] if clean_datas[:custom_fields]["ip"].present?         

          if clean_datas[:geographic][:ip].present?
            ipinfo = @geoip.city(clean_datas[:geographic][:ip])
            if ipinfo.present?
              clean_datas[:geographic][:country_name] = (ipinfo.country_name.present? ? ipinfo.country_name : nil ) unless clean_datas[:geographic][:country_name].present?
              clean_datas[:geographic][:country_code2] = (ipinfo.country_code2.present? ? ipinfo.country_code2 : nil ) unless clean_datas[:geographic][:country_code2].present?
              clean_datas[:geographic][:city_name] = (ipinfo.city_name.present? ? ipinfo.city_name : nil ) unless clean_datas[:geographic][:city_name].present?
            end
          end

          # Check Linkedin if not existing
          unless clean_datas[:socials].any?{|social| social[:type_id] == "linkedin" }
            linkedin_account = get_linkedin_account(browser, clean_datas)
            clean_datas[:socials] << linkedin_account if linkedin_account.present?
          end

          # Check Twitter if not existing
          unless clean_datas[:socials].any?{|social| social[:type_id] == "twitter" }
            twitter_account = get_twitter_account(browser, clean_datas)
            clean_datas[:socials] << twitter_account if twitter_account.present?
          end

          # Check Facebook if not existing
          unless clean_datas[:socials].any?{|social| social[:type_id] == "facebook" }
            facebook_account = get_facebook_account(browser, clean_datas)
            clean_datas[:socials] << facebook_account if facebook_account.present?
          end

          # screenshot = File.join(screenshots_path,"#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.png")
          # browser.save_screenshot(screenshot)

          lead.update_attributes(:clean_datas => clean_datas.to_json)
          DevLogger.info clean_datas

          # check conditions and do the corresponding action
          case clean_datas[:geographic][:country_name]
          when "France"
            # LeadHelper.send_email
          when "Australie"
            # LeadHelper.send_email
          else
            # LeadHelper.create_pdf
          end

        end

      rescue Exception => e
        puts "error #{e}"
        screenshot = File.join(screenshots_path,"error_#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.png")      
        browser.save_screenshot(screenshot)
        DevLogger.info e
      end

      puts "lead:clean - scrape end"
    end

    task :test, [:submission_id] => [:environment] do |t, args|
      browser.visit "http://gateway.lvh.me:4600?submission_id=#{args[:submission_id]}"
    end

    task :check, [] => [:environment] do |t, args|
      LeadInstapage.where.not(:clean_datas => nil).each do |lead|
        @lead = JSON.parse(lead.clean_datas)
        JulesLogger.info @lead["socials"]
      end
    end


    def get_fullcontact_datas(clean_datas)
      datas = {}

      begin

        if clean_datas["email"].present?
          request = HTTParty.get("https://api.fullcontact.com/v2/person.json?email=#{clean_datas['email']}&apiKey=66b8041a0dc23d0d")
          response = JSON.parse(request.body)
          datas[:pictures] = []
          datas[:generic] = {}
          datas[:demographics] = {}
          datas[:socials] = []

          response["photos"].each do |pic|
            datas[:pictures] << { :type_id => pic["typeId"], :url => pic["url"] }
          end if response["photos"].present?

          if response["contactInfo"].present?
            puts response["contactInfo"]["familyName"]
            datas[:generic][:family_name] = response["contactInfo"]["familyName"] if response["contactInfo"]["familyName"].present?
            datas[:generic][:full_name] = response["contactInfo"]["fullName"] if response["contactInfo"]["fullName"].present?
            datas[:generic][:given_name] = response["contactInfo"]["givenName"] if response["contactInfo"]["givenName"].present?
          end 

          datas[:generic][:sex] << response["gender"] if response["gender"].present?
          datas[:generic][:location] << response["locationGeneral"] if response["locationGeneral"].present?

          if response["demographics"].present?
            demographics = response["demographics"]
            if demographics["country"].present?
              datas[:demographics][:country_name] = demographics["country"]["name"] if demographics["country"]["name"].present?
              datas[:demographics][:country_code] = demographics["country"]["code"] if demographics["country"]["code"].present?
              datas[:demographics][:continent] = demographics["continent"]["name"] if demographics["continent"]["name"].present?
            end
            if demographics["city"].present?
              datas[:demographics][:city_name] = demographics["city"]["name"] if demographics["city"]["name"].present?
            end
          end

          response["socialProfiles"].each do |profile|
            datas[:socials] << { :type_id => profile["typeId"], :url => profile["url"], :id => profile["id"], :username => profile["username"] }
          end if response["socialProfiles"].present?
        end

      rescue Exception => e
        JulesLogger.info e
      ensure
        return datas
      end
    end

    def get_linkedin_account(browser, clean_datas)
      keywords = []
      keywords << clean_datas["first_name"]
      keywords << clean_datas["last_name"]
      keywords << clean_datas[:geographic][:country_name] if clean_datas[:geographic][:country_name].present?

      linkedin_profile = {}

      while linkedin_profile[:url].nil? || keywords.count >= 2

        JulesLogger.info keywords

        browser.visit("https://www.linkedin.com/vsearch/f?type=all&keywords=#{keywords.join(', ')}")

        @search_button = browser.all(:xpath, '//*[@class="search-results"]/li/a').first
        @search_button.click if @search_button.present?
        sleep(2)

        profile = browser.all(:xpath, '//*[@class="view-public-profile"]["href"]').first

        keywords.pop

        if profile.present?
          linkedin_profile[:type_id] = "linkedin"
          linkedin_profile[:url] = browser.all(:xpath, '//*[@class="view-public-profile"]["href"]').first.text
          linkedin_profile[:job] = browser.all(:xpath, '//*[@id="headline"]/*[@class="title"]').first.text.force_encoding("UTF-8")

          JulesLogger.info linkedin_profile
        end
      end

      return ( linkedin_profile[:username].present? ? linkedin_profile : nil  )
    end

    def get_twitter_account(browser, clean_datas)
      keywords = []
      keywords << clean_datas["last_name"]
      keywords << clean_datas["first_name"]
      keywords << I18n.transliterate(clean_datas[:geographic][:country_name]) if clean_datas[:geographic][:country_name].present?

      JulesLogger.info keywords

      twitter_profile = {}

      while twitter_profile[:username].nil? || keywords.count >= 2
        browser.visit("https://twitter.com/search?f=users&vertical=default&q=#{keywords.join(",")}&src=typd")
        sleep(2)
        profile = browser.all(:xpath, '//*[contains(@class, "ProfileCard-screenname")]/*[contains(@class, "u-linkComplex-target")]').first

        keywords.pop

        if profile.present?
          twitter_profile[:type_id] = "twitter"
          twitter_profile[:username] = profile.text
          twitter_profile[:url] = "https://www.twitter.com/#{profile.text}"

          JulesLogger.info twitter_profile
        end
      end

      return ( twitter_profile[:username].present? ? twitter_profile : nil  )
    end

    def get_facebook_account(browser, clean_datas)
      keywords = []
      keywords << clean_datas["first_name"]
      keywords << clean_datas["last_name"]
      keywords << I18n.transliterate(clean_datas[:geographic][:city_name]) if clean_datas[:geographic][:city_name].present?

      facebook_profile = {}

      JulesLogger.info keywords

      while facebook_profile[:url].nil? || keywords.count >= 2

        browser.visit("https://www.facebook.com/search/people/?q=#{keywords.join(",")}")
        sleep(2)
        profile = browser.all(:xpath, '//*[contains(@id, "all_search_results")]').first

        profile.click if profile.present?

        keywords.pop

        if profile.present?
          facebook_profile[:type_id] = "facebook"
          facebook_profile[:url] = browser.current_url

          JulesLogger.info facebook_profile
        end
      end

      return ( facebook_profile[:username].present? ? facebook_profile : nil  )
    end

  end