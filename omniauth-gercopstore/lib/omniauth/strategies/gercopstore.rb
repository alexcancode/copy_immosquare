require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Gercopstore < OmniAuth::Strategies::OAuth2
      option :name, :gercopstore


      option :client_options, {
        :site           => Rails.env.development? ? "http://gercop.lvh.me:3010" : 'http://store.gercop.com',
        :authorize_path => 'oauth/authorize',
        :token_path     => 'oauth/token'
      }


      uid do
        raw_info["id"]
      end

      info do
        {
          :name         => raw_info["name"],
          :email        => raw_info["email"],
          :nickname     => raw_info["nickname"],
          :first_name   => raw_info["first_name"],
          :last_name    => raw_info["last_name"],
          :location     => raw_info["location"],
          :description  => raw_info["description"],
          :image        => raw_info["image"],
          :phone        => raw_info["phone"],
          :urls         => raw_info["urls"],
        }
      end


      extra do
        {
          :raw_info => raw_info["extra"]
        }
      end


      def raw_info
        @raw_info ||= access_token.get('/api/v1/me').parsed
      end


    end
  end
end
