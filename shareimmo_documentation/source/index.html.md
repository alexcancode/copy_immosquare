---
title:  shareIMMO API Reference

language_tabs:
  - curl

toc_footers:
  - <a href='https://shareimmo.com' target="_blank">back to shareIMMO</a>

search: false
---

# API V2 Reference

The **shareimmo** API is organized around <a href='http://en.wikipedia.org/wiki/Representational_State_Transfer' target='_blank'>REST</a>. Our API is designed to have predictable, resource-oriented URLs and to use HTTP response codes to indicate API errors. We use built-in HTTP features, like HTTP authentication and HTTP verbs, which can be understood by off-the-shelf HTTP clients, and we support <a href='http://en.wikipedia.org/wiki/Cross-origin_resource_sharing' target='_blank'>cross-origin resource sharing</a> to allow you to interact securely with our API from a client-side web application (though you should remember that you should never expose your secret API key in any public website's client-side code). <a href='http://www.json.org/' target='_blank'>JSON</a> will be returned in all responses from the API, including errors (though if you're using API bindings, we will convert the response to the appropriate language-specific object)

# Authentication

<blockquote>Definition</blockquote>

```
POST /path
Content-Type: application/json
ApiKey: xz1232xzd213213er213213dczxcc3loizxzs12dczx
ApiToken: xz1287xzdwer213213dczxcas12dczx

GET /path
ApiKey: xz1232xzd213213er213213dczxcc3loizxzs12dczx
ApiToken: xz1287xzdwer213213dczxcas12dczx
```
You authenticate to the **shareimmo** API by providing your secret API key & API token in the request. Your API keys & tokens carry many privileges, so be sure to keep them secret!<br><br>All API requests must be made over HTTPS. Calls made over plain HTTP will fail. You must authenticate for all requests.<br><br>All calls to the API must contain  API key & API token in the header<br><br>To obtain your credentials, please contact our technical team at +1 (514) 700-1963 or <a href='mailto:'contact@shareimmo.com''>contact@shareimmo.com</a>

# Errors
```
HTTP Status Code Summary
------------------------------------ --------------------------------------------
200 - OK                             Everything worked as expected.
400 - Bad Request                    Often missing a required parameter.
401 - Unauthorized                   No valid API key provided.
402 - Request Failed                 Parameters were valid but request failed.
404 - Not Found                      The requested item doesn’t exist.
429 - Too Many Requests              Too many requests hit the API too quickly.
500, 502, 503, 504 - Server Errors   Something went wrong
------------------------------------ --------------------------------------------
```

**Shareimmo** uses conventional HTTP response codes to indicate success or failure of an API request. In general, codes in the 2xx range indicate success, codes in the 4xx range indicate an error that resulted from the provided information (e.g. a required parameter was missing, a charge failed, etc.), and codes in the 5xx range indicate an error with shareimmmo's servers.





# Accounts
This is an object representing your **shareimmo** account.

## Create an account
<blockquote>Definition</blockquote>
>[POST] https://shareimmo.com/api/v2/accounts

<blockquote class="request_class">Exemple Request</blockquote>
```json
{
  "uid": "123",
  "email" : "cyrielle@shareimmo.com",
  "password": "your-password"
}
```

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 2,
  "uid" : "123",
  "email" : "cyrielle@shareimmo.com",
  "message": "User was created"
}
```

Attributes | &nbsp;
---------- | -------
uid <br> **string** <p style='color:red'>required</p> | The identifier of the account in your system <br>(must be unique)
email <br> **string** <p style='color:red'>required</p> | login email <br>(must be unique)
password <br> **string** <p style='color:red'>required</p> | password




## Update an account

<blockquote>Definition</blockquote>
>[PUT] https://shareimmo.com/api/v2/accounts/123

<blockquote class="request_class">Exemple Request</blockquote>
```json
{
  "country": "FR",
  "public_email": "cyrielle_public@shareimmo.com",
  "first_name": "Cyrielle",
  "last_name": "Bocquet",
  "phone": "514 568-6780",
  "phone_sms": "514 568-6780",
  "phone_visibility": 1,
  "website": "www.wgroupe.com",
  "spoken_languages": "fr,en",
  "picture_url": "http://lorempicsum.com/futurama/255/200/5",
  "facebook_url": "https://www.facebook.com/jules.welsch",
  "twitter_url": "https://twitter.com/jules_welsch",
  "linkedin_url": "https://ca.linkedin.com/pub/jules-welsch/91/463/991",
  "pinterest_url": "https://www.pinterest.com/juleswelsch/",
  "google_plus_url": "https://plus.google.com/",
  "instagram_url": "https://instagram.com/cyrielle_bocquet/",
  "company_name": "Wgroupe",
  "company_address": "460 rue sainte catherine Ouest #300",
  "company_city": "Montréal",
  "company_zipcode": "H3B1A8",
  "company_administrative_area": "Québec",
  "company_logo_url": "http://lorempicsum.com/futurama/350/200/6",
  "corporate_account": 1,
  "color1": "#f54029",
  "color2": "#f54029",
  "presentation_text": {
    "fr": "professionnel de l'immobilier de luxe",
    "en": "Luxury real estate broker",
    "de": "Luxus Immobilien Berufs",
    "nl": "Luxe onroerend goed professional",
    "it": "Immobili di lusso professionale",
    "es": "Lujo profesional de bienes raíces",
    "pt": "profissional Imóveis de luxo"
  },
  "bio": {
    "fr": "<h1>Bonjour</h1>,<p>je m'appelle ...</p>",
    "en": "<h1>Hello</h1><p> I'm ...</p>",
    "de": "<h1>Hallo</h1><p> Ich bin ...</p>",
    "nl": "<h1>Hallo</h1><p> Ik ben ...</p>",
    "it": "<h1>Hello</h1><p> Sono ...</p>",
    "es": "<h1>Hola</h1><p> Estoy ...</p>",
    "pt": "<h1>Hello</h1><p> Eu sou ...</p>"
  },
  "baseline": {
    "fr": "la meilleur place pour acheter",
    "en": "the best place to buy",
    "de": "der beste Ort, um zu kaufen",
    "nl": "de beste plek om te kopen",
    "it": "il posto migliore per comprare",
    "es": "el mejor lugar para comprar",
    "pt": "o melhor lugar para comprar"
  }
}
```

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 2,
  "uid": "123",
  "email": "contact@immosquare.com",
  "username": "immosquare",
  "sign_in_count": 12,
  "country": "FR",
  "public_email": "cyrielle_public@shareimmo.com",
  "first_name": "Cyrielle",
  "last_name": "Bocquet",
  "phone": "514 568-6780",
  "phone_sms": "514 568-6780",
  "phone_visibility": 1,
  "picture_url": "https://s3.amazonaws.com/shareimmo/development/brokers/2/picture_original.",
  "website": "www.wgroupe.com",
  "spoken_languages": "fr,en",
  "facebook": "https://www.facebook.com/jules.welsch",
  "twitter": "https://twitter.com/jules_welsch",
  "linkedin": "https://ca.linkedin.com/pub/jules-welsch/91/463/991",
  "pinterest": "https://www.pinterest.com/juleswelsch/",
  "google_plus": "https://plus.google.com/",
  "instagram": "https://instagram.com/cyrielle_bocquet/",
  "color1": "#f54029",
  "color2": "#f54029",
  "company_name": "Wgroupe",
  "company_address": "460 rue sainte catherine Ouest #300",
  "company_city": "Montréal",
  "company_zipcode": "H3B1A8",
  "company_administrative_area": "Québec",
  "company_logo_url": "https://s3.amazonaws.com/shareimmo/development/brokers/2/logo_original.",
  "corporate_account": 1,
  "presentation_text": {
    "fr": "professionnel de l'immobilier de luxe",
    "en": "Luxury real estate broker",
    "de": "Luxus Immobilien Berufs",
    "nl": "Luxe onroerend goed professional",
    "it": "Immobili di lusso professionale",
    "es": "Lujo profesional de bienes raíces",
    "pt": "profissional Imóveis de luxo"
  },
  "bio": {
    "fr": "<h1>Bonjour</h1>,<p>je m'appelle ...</p>",
    "en": "<h1>Hello</h1><p> I'm ...</p>",
    "de": "<h1>Hallo</h1><p> Ich bin ...</p>",
    "nl": "<h1>Hallo</h1><p> Ik ben ...</p>",
    "it": "<h1>Hello</h1><p> Sono ...</p>",
    "es": "<h1>Hola</h1><p> Estoy ...</p>",
    "pt": "<h1>Hello</h1><p> Eu sou ...</p>"
  },
  "baseline": {
    "fr": "la meilleur place pour acheter",
    "en": "the best place to buy",
    "de": "der beste Ort, um zu kaufen",
    "nl": "de beste plek om te kopen",
    "it": "il posto migliore per comprare",
    "es": "el mejor lugar para comprar",
    "pt": "o melhor lugar para comprar"
  },
  "created_at": "2014-06-14T13:01:49.000-04:00",
  "updated_at": "2016-09-14T15:50:57.694-04:00",
  "uids": [
    {
      "uid": "G5805",
      "provider": "centris"
    },
    {
      "uid": "123",
      "provider": "kangalou"
    }
  ]
}

```


Attributes | &nbsp;
---------- | -------
uid <br> **string** <p style='color:red'>required</p> | Your identifier of the account to be retrieved
country <br> **string** <p style='color:red'>required</p> | The country attached to the account with <a href='https://en.wikipedia.org/wiki/ISO_3166-1'>ISO 3166-1 alpha-2 standard</a>
email <br> **string** <p style='color:red'>required</p> | User login email (Only for XML API)
public_email <br> **string**  | &nbsp;
first_name <br> **string**  | &nbsp;
last_name <br> **string**  | &nbsp;
phone <br> **string**  | Phone number connected to the account without the international prefix
phone_sms <br> **string**  | Phone number for sms reception connected to the account without the international prefix
phone_visibility <br> **string** <br> &nbsp; &nbsp; &bull; 0 - no <br> &nbsp; &nbsp; &bull; 1 - yes  | if 0 any phone numbers are displayed on ads
website  <br> **string** | The url of the account's website (without http://)
spoken_languages  <br> **string** | spoken languages separated by commas with <a href='https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes'>ISO 3166-1 alpha-2 standard</a>
picture_url  <br> **string** | Max: 2Mo Square format for the best result (500x500)
facebook_url  <br> **string** | Facebook page of the account
twitter_url  <br> **string** | Twitter page of the account
linkedin_url  <br> **string** | Linkedin page of the account
pinterest_url  <br> **string** | Pinterest page of the account
google_plus_url  <br> **string** | Google + page of the account
instagram_url  <br> **string** | Instagram page of the account
company_name  <br> **string** | &nbsp;
company_address  <br> **string** | &nbsp;
company_city  <br> **string** | &nbsp;
company_administrative_area  <br> **string** | &nbsp;
company_logo_url  <br> **string** | Max: 2Mo Square format for the best result (500x500)
corporate_account  <br> **integer** | &nbsp; &nbsp; &bull; 0 - no <br> &nbsp; &nbsp; &bull; 1 - yes
color1  <br> **string** | Color primary of the account with <a href="https://en.wikipedia.org/wiki/Web_colors">hexadecimal standard</a>
color2  <br> **string** | Color primary of the account with <a href="https://en.wikipedia.org/wiki/Web_colors">hexadecimal standard</a>
presentation_text  <br> **string** | Small presentation text of the account
bio  <br> **text** | Complete Presentation text of the account (html accepted)
baseline  <br> **string** | Commercial hook



## Retrieve an account

<blockquote>Definition</blockquote>
>[GET] https://shareimmo.com/api/v2/accounts/:uid

<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/accounts/123

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 2,
  "uid": "123",
  "email": "contact@immosquare.com",
  "username": "immosquare",
  "sign_in_count": 12,
  "country": "FR",
  "public_email": "cyrielle_public@shareimmo.com",
  "first_name": "Cyrielle",
  "last_name": "Bocquet",
  "phone": "514 568-6780",
  "phone_sms": "514 568-6780",
  "phone_visibility": 1,
  "picture_url": "https://s3.amazonaws.com/shareimmo/development/brokers/2/picture_original.",
  "website": "www.wgroupe.com",
  "spoken_languages": "fr,en",
  "facebook": "https://www.facebook.com/jules.welsch",
  "twitter": "https://twitter.com/jules_welsch",
  "linkedin": "https://ca.linkedin.com/pub/jules-welsch/91/463/991",
  "pinterest": "https://www.pinterest.com/juleswelsch/",
  "google_plus": "https://plus.google.com/",
  "instagram": "https://instagram.com/cyrielle_bocquet/",
  "color1": "#f54029",
  "color2": "#f54029",
  "company_name": "Wgroupe",
  "company_address": "460 rue sainte catherine Ouest #300",
  "company_city": "Montréal",
  "company_zipcode": "H3B1A8",
  "company_administrative_area": "Québec",
  "company_logo_url": "https://s3.amazonaws.com/shareimmo/development/brokers/2/logo_original.",
  "corporate_account": 1,
  "presentation_text": {
    "fr": "professionnel de l'immobilier de luxe",
    "en": "Luxury real estate broker",
    "de": "Luxus Immobilien Berufs",
    "nl": "Luxe onroerend goed professional",
    "it": "Immobili di lusso professionale",
    "es": "Lujo profesional de bienes raíces",
    "pt": "profissional Imóveis de luxo"
  },
  "bio": {
    "fr": "<h1>Bonjour</h1>,<p>je m'appelle ...</p>",
    "en": "<h1>Hello</h1><p> I'm ...</p>",
    "de": "<h1>Hallo</h1><p> Ich bin ...</p>",
    "nl": "<h1>Hallo</h1><p> Ik ben ...</p>",
    "it": "<h1>Hello</h1><p> Sono ...</p>",
    "es": "<h1>Hola</h1><p> Estoy ...</p>",
    "pt": "<h1>Hello</h1><p> Eu sou ...</p>"
  },
  "baseline": {
    "fr": "la meilleur place pour acheter",
    "en": "the best place to buy",
    "de": "der beste Ort, um zu kaufen",
    "nl": "de beste plek om te kopen",
    "it": "il posto migliore per comprare",
    "es": "el mejor lugar para comprar",
    "pt": "o melhor lugar para comprar"
  },
  "created_at": "2014-06-14T13:01:49.000-04:00",
  "updated_at": "2016-09-14T15:50:57.694-04:00",
  "uids": [
    {
      "uid": "G5805",
      "provider": "centris"
    },
    {
      "uid": "123",
      "provider": "kangalou"
    }
  ]
}
```

Attributes | &nbsp;
---------- | -------
uid <br> **string** <p style='color:red'>required</p> | Your identifier of the account to be retrieved


## Delete an account

<blockquote>Definition</blockquote>
>[DELETE] https://shareimmo.com/api/v2/accounts/:uid

<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/accounts/123

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "uid": 123,
  "message": "Deletion was done"
}
```

Attributes | &nbsp;
---------- | -------
uid <br> **string** <p style='color:red'>required</p> | The identifier of the account in your system <br> (must be unique)





# Buildings

This is an object representing your shareimmo buildings

## Create & Update a building

<blockquote>Définition</blockquote>
>[POST|PUT] https://shareimmo.com/api/v2/buildings


<blockquote class="request_class">Example Request</blockquote>
```json
{
  "uid": "3",
  "account_uid": "123",
  "name": "Le nordelec",
  "units": 250,
  "levels": 8,
  "address": {
    "address_formatted": "1740 Rue St-Patrick, Montréal, QC H3K 2H2, Canada",
    "street_number": "1740",
    "street_name": "Rue Saint-Patrick",
    "zipcode": "H3K 2H2",
    "locality": "Montréal",
    "sublocality": "Pointe-Saint-Charles",
    "administrative_area_level1": "Québec",
    "administrative_area_level2": "Communauté-Urbaine-de-Montréal",
    "country": "CA",
    "latitude": 45.4854,
    "longitude": -73.5629
  },
  "description_short": {
    "fr": "Construit en 1913, le Nordelec demeure un exemple remarquable...",
    "en": "Built in 1913, the Nordelec remains an outstanding example ...",
    "de": "Erbaut im Jahr 1913, bleibt die Nordelec ein hervorragendes Beispiel ...",
    "nl": "Gebouwd in 1913, de Nordelec blijft een uitstekend voorbeeld ...",
    "it": "Costruito nel 1913, il Nordelec rimane un esempio eccezionale ...",
    "es": "Construido en 1913, el Nordelec sigue siendo un ejemplo sobresaliente ...",
    "pt": "Construído em 1913, o Nordelec continua a ser um exemplo notável ..."
  },
  "description": {
    "fr": "<p><span>Un des beaux avantages de la vie aux condos Nordelec est l’accès exclusif à des espaces communs peu ordinaires</span></p>",
    "en": "<p><span>One of the beautiful advantages of living in the condos is Nordelec exclusive access to unusual public areas</span></p>",
    "de": "<p><span>Einer der schönsten Vorteile in den Eigentumswohnungen leben, ist Nordelec exklusiven Zugang zu ungewöhnlichen öffentlichen Bereichen</span></p>",
    "nl": "<p><span>Een van de prachtige voordelen van het wonen in de appartementen is Nordelec exclusieve toegang tot ongebruikelijke openbare ruimten</span></p>",
    "it": "<p><span>Una delle belle vantaggi di vivere in condomini è l'accesso esclusivo a Nordelec insolite aree pubbliche</span></p>",
    "es": "<p><span>Una de las hermosas ventajas de vivir en los condominios es Nordelec acceso exclusivo a las zonas comunes inusuales</span></p>",
    "pt": "<p><span>Uma das belas vantagens de viver em condomínios é de acesso exclusivo Nordelec para espaços públicos incomuns</span></p>"
  },
  "pictures": [
    { "url": "http://lorempicsum.com/nemo/627/300/9"},
    { "url": "http://lorempicsum.com/nemo/627/300/2"},
    { "url": "http://lorempicsum.com/nemo/627/300/1"}
  ]
}

```

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 12,
  "uid": "3",
  "account_id": 2,
  "account_uid": "123",
  "name": "Le nordelec",
  "units": 250,
  "levels": 8,
  "address": {
    "address_formatted": "1740 Rue St-Patrick, Montréal, QC H3K 2H2, Canada",
    "street_number": "1740",
    "street_name": "Rue Saint-Patrick",
    "zipcode": "H3K 2H2",
    "locality": "Montréal",
    "sublocality": "Pointe-Saint-Charles",
    "administrative_area_level1": "Québec",
    "administrative_area_level2": "Communauté-Urbaine-de-Montréal",
    "country": "CA",
    "latitude": 45.4854,
    "longitude": -73.5629
  },
  "description_short": {
    "fr": "Construit en 1913, le Nordelec demeure un exemple remarquable...",
    "en": "Built in 1913, the Nordelec remains an outstanding example ...",
    "de": "Erbaut im Jahr 1913, bleibt die Nordelec ein hervorragendes Beispiel ...",
    "nl": "Gebouwd in 1913, de Nordelec blijft een uitstekend voorbeeld ...",
    "it": "Costruito nel 1913, il Nordelec rimane un esempio eccezionale ...",
    "es": "Construido en 1913, el Nordelec sigue siendo un ejemplo sobresaliente ...",
    "pt": "Construído em 1913, o Nordelec continua a ser um exemplo notável ..."
  },
  "description": {
    "fr": "<p><span>Un des beaux avantages de la vie aux condos Nordelec est l’accès exclusif à des espaces communs peu ordinaires</span></p>",
    "en": "<p><span>One of the beautiful advantages of living in the condos is Nordelec exclusive access to unusual public areas</span></p>",
    "de": "<p><span>Einer der schönsten Vorteile in den Eigentumswohnungen leben, ist Nordelec exklusiven Zugang zu ungewöhnlichen öffentlichen Bereichen</span></p>",
    "nl": "<p><span>Een van de prachtige voordelen van het wonen in de appartementen is Nordelec exclusieve toegang tot ongebruikelijke openbare ruimten</span></p>",
    "it": "<p><span>Una delle belle vantaggi di vivere in condomini è l'accesso esclusivo a Nordelec insolite aree pubbliche</span></p>",
    "es": "<p><span>Una de las hermosas ventajas de vivir en los condominios es Nordelec acceso exclusivo a las zonas comunes inusuales</span></p>",
    "pt": "<p><span>Uma das belas vantagens de viver em condomínios é de acesso exclusivo Nordelec para espaços públicos incomuns</span></p>"
  },
  "pictures": {
    "in_progess": true
  }
}
```

You can use the same url to create and update a property
In order to continuously have more impact in the display of your properties, it is possible to associate several properties to a single building.


Attributes | &nbsp;
---------- | -------
uid <br> **string**  <p style='color:red'>required</p> | In order to continuously have more impact in the display of your ads. It is possible to associate several ads to a single building
account_uid <br> **string** <p style='color:red'>required</p> | This entry is used to allocate a building to a user
name <br> **string**  <p style='color:red'>required for creation (POST)</p> | Building name
units <br> **integer** | Number of units in the building
levels <br> **integer** | Number of floors in the building
address.address_formatted <br> **string** | property's full address (can be geocoded)
address.street_number <br> **string** | Street number
address.street_name <br> **string** | Street name (without street number)
address.zipcode <br> **string** | Zipcode
address.locality <br> **string** | Town
address.sublocality <br> **string** | Neighborhood
address.administrative_area_level1 <br> **string** | Region/province
address.administrative_area_level2 <br> **string** | Municipality/county
address.country <br> **string** | &nbsp;
address.latitude <br> **float** | &nbsp;
address.longitude <br> **float** | &nbsp;
description_short <br> **text** | Building description in different languages (can NOT use html content)
description <br> **text** | Building description in different languages (can use html content)
pictures <br> **string** | max:10Mo Use 1920x1080px to obtain the best results
address_visibility <br> **integer** <br> &nbsp; &nbsp; &bull; 0 - no <br> &nbsp; &nbsp; &bull; 1 - yes | Do you wish your address to be visible?




## Retrieve a building

<blockquote>Définition</blockquote>
>[GET] https://shareimmo.com/api/v2/building/:uid

<blockquote class="request_class">Exemple Request</blockquote>
>https://shareimmo.com/api/v2/building/3

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 12,
  "uid": "3",
  "account_id": 2,
  "account_uid": "123",
  "name": "Le nordelec",
  "units": 250,
  "levels": 8,
  "address": {
    "address_formatted": "1740 Rue St-Patrick, Montréal, QC H3K 2H2, Canada",
    "street_number": "1740",
    "street_name": "Rue Saint-Patrick",
    "zipcode": "H3K 2H2",
    "locality": "Montréal",
    "sublocality": "Pointe-Saint-Charles",
    "administrative_area_level1": "Québec",
    "administrative_area_level2": "Communauté-Urbaine-de-Montréal",
    "country": "CA",
    "latitude": 45.4854,
    "longitude": -73.5629
  },
  "description_short": {
    "fr": "Construit en 1913, le Nordelec demeure un exemple remarquable...",
    "en": "Built in 1913, the Nordelec remains an outstanding example ...",
    "de": "Erbaut im Jahr 1913, bleibt die Nordelec ein hervorragendes Beispiel ...",
    "nl": "Gebouwd in 1913, de Nordelec blijft een uitstekend voorbeeld ...",
    "it": "Costruito nel 1913, il Nordelec rimane un esempio eccezionale ...",
    "es": "Construido en 1913, el Nordelec sigue siendo un ejemplo sobresaliente ...",
    "pt": "Construído em 1913, o Nordelec continua a ser um exemplo notável ..."
  },
  "description": {
    "fr": "<p><span>Un des beaux avantages de la vie aux condos Nordelec est l’accès exclusif à des espaces communs peu ordinaires</span></p>",
    "en": "<p><span>One of the beautiful advantages of living in the condos is Nordelec exclusive access to unusual public areas</span></p>",
    "de": "<p><span>Einer der schönsten Vorteile in den Eigentumswohnungen leben, ist Nordelec exklusiven Zugang zu ungewöhnlichen öffentlichen Bereichen</span></p>",
    "nl": "<p><span>Een van de prachtige voordelen van het wonen in de appartementen is Nordelec exclusieve toegang tot ongebruikelijke openbare ruimten</span></p>",
    "it": "<p><span>Una delle belle vantaggi di vivere in condomini è l'accesso esclusivo a Nordelec insolite aree pubbliche</span></p>",
    "es": "<p><span>Una de las hermosas ventajas de vivir en los condominios es Nordelec acceso exclusivo a las zonas comunes inusuales</span></p>",
    "pt": "<p><span>Uma das belas vantagens de viver em condomínios é de acesso exclusivo Nordelec para espaços públicos incomuns</span></p>"
  },
  "pictures": [
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_hd.jpg"
    }
  ]
}
```

Attributes | &nbsp;
---------- | -------
uid <br> **string** | Your identifier of the building to be retrieved


## Delete a building
<blockquote>Définition</blockquote>
>[DELETE] https://shareimmo.com/api/v2/building/:uid

<blockquote class="request_class">Exemple Request</blockquote>
>https://shareimmo.com/api/v2/building/3

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 12,
  "uid": "3",
  "account_id": 2,
  "account_uid": "123",
  "message": "Building deletion was launched0"
}

```

Attributes | &nbsp;
---------- | -------
uid <br> **string** | Your identifier of the building to be deleted




#Properties

This is an object representing your **shareimmo** properties

##Create & Update a property

In order to continuously have more impact in the display of your properties, it is possible to associate several properties to a single building.

<blockquote>Définition</blockquote>
>[POST|PUT] https://shareimmo.com/api/v2/properties

<blockquote class="request_class">Exemple Request</blockquote>
```json
{
  "uid": "50",
  "master": 1,
  "account_uid": "123",
  "building_uid": "3",
  "online": 1,
  "classification_id": 201,
  "status_id": 1,
  "listing_id": 2,
  "unit": "Appt #603",
  "level": 6,
  "cadastre": "nordelec-603",
  "flag_id": 1,
  "year_of_built": 2012,
  "availability_date": "2017-01-16",
  "is_luxurious": 1,
   "address": {
    "address_visibility": 1,
    "address_formatted": "1740 Rue St-Patrick, Montréal, QC H3K 2H2, Canada",
    "street_number": "1740",
    "street_name": "Rue Saint-Patrick",
    "zipcode": "H3K 2H2",
    "locality": "Montréal",
    "sublocality": "Pointe-Saint-Charles",
    "administrative_area_level1": "Québec",
    "administrative_area_level2": "Communauté-Urbaine-de-Montréal",
    "country": "CA",
    "latitude": 45.4854,
    "longitude": -73.5629
  },
  "area": {
    "living": 1000,
    "balcony": 20,
    "land": null,
    "unit_id": 2
  },
  "rooms": {
    "rooms": 3,
    "bathrooms": 1,
    "bedrooms": 1,
    "showerrooms": null,
    "toilets": 1
  },
  "title": {
    "fr": "Superbe Appartement",
    "en": "superb Apartment",
    "de": "Superb Wohnung",
    "nl": "schitterend Appartement",
    "it": "schitterend Appartement",
    "es": "Excelente Apartamento",
    "pt": "Excelente Apartamento"
  },
  "description_short": {
    "fr": "Construit en 1913, le Nordelec demeure un exemple remarquable...",
    "en": "Built in 1913, the Nordelec remains an outstanding example ...",
    "de": "Erbaut im Jahr 1913, bleibt die Nordelec ein hervorragendes Beispiel ...",
    "nl": "Gebouwd in 1913, de Nordelec blijft een uitstekend voorbeeld ...",
    "it": "Costruito nel 1913, il Nordelec rimane un esempio eccezionale ...",
    "es": "Construido en 1913, el Nordelec sigue siendo un ejemplo sobresaliente ...",
    "pt": "Construído em 1913, o Nordelec continua a ser um exemplo notável ..."
  },
  "description": {
    "fr": "<p><span>Un des beaux avantages de la vie aux condos Nordelec est l’accès exclusif à des espaces communs peu ordinaires</span></p>",
    "en": "<p><span>One of the beautiful advantages of living in the condos is Nordelec exclusive access to unusual public areas</span></p>",
    "de": "<p><span>Einer der schönsten Vorteile in den Eigentumswohnungen leben, ist Nordelec exklusiven Zugang zu ungewöhnlichen öffentlichen Bereichen</span></p>",
    "nl": "<p><span>Een van de prachtige voordelen van het wonen in de appartementen is Nordelec exclusieve toegang tot ongebruikelijke openbare ruimten</span></p>",
    "it": "<p><span>Una delle belle vantaggi di vivere in condomini è l'accesso esclusivo a Nordelec insolite aree pubbliche</span></p>",
    "es": "<p><span>Una de las hermosas ventajas de vivir en los condominios es Nordelec acceso exclusivo a las zonas comunes inusuales</span></p>",
    "pt": "<p><span>Uma das belas vantagens de viver em condomínios é de acesso exclusivo Nordelec para espaços públicos incomuns</span></p>"
  },
  "url": {
    "fr": "http://www.exemple.com/fr/55",
    "en": "http://www.exemple.com/en/55",
    "de": "http://www.exemple.com/de/55",
    "nl": "http://www.exemple.com/nl/55",
    "it": "http://www.exemple.com/it/55",
    "es": "http://www.exemple.com/es/55",
    "pt": "http://www.exemple.com/pt/55"
  },
  "financial": {
    "price": 0,
    "price_with_tax": 1,
    "rent": 1150,
    "fees": 145,
    "rent_frequency": 30,
    "tax1_annual": 1500,
    "tax2_annual": 2541,
    "income_annual": "0",
    "deposit": null,
    "currency": "CAD"
  },
  "pictures": [
    { "url": "http://lorempicsum.com/nemo/627/300/9"},
    { "url": "http://lorempicsum.com/nemo/627/300/2"},
    { "url": "http://lorempicsum.com/nemo/627/300/1"}
  ],
  "canada": {
    "mls": 123456
  },
  "france":{
    "dpe_indice": null,
    "dpe_value": null,
    "dpe_id": null,
    "ges_indice": null,
    "ges_value": null,
    "alur_is_condo": null,
    "alur_units": null,
    "alur_uninhabitables": null,
    "alur_spending": null,
    "alur_legal_action": null,
    "rent_honorary": null,
    "mandat_exclusif": null
  },
  "belgium":{
    "peb_indice": null,
    "peb_value": null,
    "peb_id": null,
    "building_permit": null,
    "done_assignments": null,
    "preemption_property":null,
    "subdivision_permits":null,
    "sensitive_flood_area": null,
    "delimited_flood_zone":null,
    "risk_area" :null
  },
  "other": {
    "orientation":{
      "north": 1,
      "south": 0,
      "east": 0,
      "west": 1
    },
    "inclusion": {
      "air_conditioning": 1,
      "hot_water": 1,
      "heated": 1,
      "electricity": 0,
      "furnished": 0,
      "fridge": 1,
      "cooker": 1,
      "dishwasher": 1,
      "dryer": 1,
      "washer": 1,
      "microwave": 0
    },
    "detail": {
      "elevator": 1,
      "laundry": 0,
      "garbage_chute": 1,
      "common_space": 1,
      "janitor": 1,
      "gym": 1,
      "golf": 1,
      "tennis": 1,
      "sauna": 1,
      "spa": 1,
      "inside_pool": 0,
      "outside_pool": 1,
      "inside_parking": 1,
      "outside_parking": 1,
      "parking_on_street": 1,
      "garagebox": 0
    },
    "half_furnished": {
      "living_room": 0,
      "bedrooms": 0,
      "kitchen": 0,
      "other": 0
    },
    "indoor": {
      "attic": 0,
      "attic_convertible": 0,
      "attic_converted": 0,
      "cellar": 0,
      "central_vacuum": 0,
      "entries_washer_dryer": 1,
      "entries_dishwasher": 1,
      "fireplace": 0,
      "storage": 1,
      "walk_in": 1,
      "no_smoking": 1,
      "double_glazing": 0,
      "triple_glazing": 1
    },
    "floor": {
      "carpet": 0,
      "wood": 1,
      "floating": 0,
      "ceramic": 0,
      "parquet": 0,
      "cushion": 0,
      "vinyle": 0,
      "lino": 0,
      "marble": 0
    },
    "exterior": {
      "land_access": 0,
      "back_balcony": 0,
      "front_balcony": 1,
      "private_patio": 1,
      "storage": 1,
      "terrace": 1,
      "veranda": 0,
      "garden": 0,
      "sea_view": 0,
      "mountain_view": 0
    },
    "accessibility": {
      "elevator": 0,
      "balcony": 0,
      "grab_bar": 0,
      "wider_corridors": 0,
      "lowered_switches": 0,
      "ramp": 0
    },
    "senior": {
      "autonomy": 0,
      "half_autonomy": 0,
      "no_autonomy": 0,
      "meal": 0,
      "nursery": 0,
      "domestic_help": 0,
      "community": 0,
      "activities": 0,
      "validated_residence": 0,
      "housing_cooperative": 0
    },
    "pet": {
      "allow": 1,
      "cat": 1,
      "dog": 1,
      "little_dog": 1,
      "cage_aquarium": 1,
      "not_allow": 0
    },
    "service": {
      "internet": 1,
      "tv": 1,
      "tv_sat": 1,
      "phone": 1
    },
    "composition": {
      "bar": 0,
      "living": 0,
      "dining": 0,
      "separate_toilet": 0,
      "open_kitchen": 1
    },
    "heating": {
      "electric": 1,
      "solar": 0,
      "gaz": 0,
      "condensation": 0,
      "fuel": 0,
      "heat_pump": 0,
      "floor_heating": 0
    },
    "transport": {
      "bicycle_path": 1,
      "public_transport": 1,
      "public_bike": 1,
      "subway": 1,
      "train_station": 1
    },
    "security": {
      "guardian": 1,
      "alarm": 1,
      "intercom": 1,
      "camera": 1,
      "smoke_dectector": 1
    }
  }
}

```



<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 149,
  "uid": "50",
  "master": 1,
  "account_id": 2,
  "account_uid": "123",
  "building": {
    "id": 12,
    "uid": "3",
    "name": "Le nordelec",
    "cover_url": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_697_large.jpg",
    "cover_url_hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_697_hd.jpg",
  },
  "online": 1,
  "classification_id": 201,
  "classification": {
    "fr": "Appartement",
    "en": "Apartement",
    "category": {
      "id": 2,
      "fr": "Appartement",
      "en": "Apartement"
    }
  },
  "status_id": 1,
  "status": {
    "fr": "Neuf",
    "en": "New"
  },
  "listing_id": 2,
  "listing": {
    "fr": "À Louer",
    "en": "For Rent"
  },
  "unit": "Appt #603",
  "level": 6,
  "cadastre": "nordelec-603",
  "flag_id": 1,
  "flag": {
    "fr": "Nouveauté",
    "en": "Newness"
  },
  "year_of_built": 2012,
  "availability_date": "2017-01-16",
  "is_luxurious": 1,
  "address": {
    "address_visibility": 1,
    "address_formatted": "1740 Rue St-Patrick, Montréal, QC H3K 2H2, Canada",
    "street_number": "1740",
    "street_name": "Rue Saint-Patrick",
    "zipcode": "H3K 2H2",
    "locality": "Montréal",
    "sublocality": "Pointe-Saint-Charles",
    "administrative_area_level1": "Québec",
    "administrative_area_level2": "Communauté-Urbaine-de-Montréal",
    "country": "CA",
    "latitude": 45.4854,
    "longitude": -73.5629
  },
  "area": {
    "living": 1000,
    "balcony": 20,
    "land": null,
    "unit_id": 2
  },
  "rooms": {
    "rooms": 3,
    "bathrooms": 1,
    "bedrooms": 1,
    "showerrooms": null
  },
  "title": {
    "fr": "Superbe Appartement",
    "en": "superb Apartment",
    "de": "Superb Wohnung",
    "nl": "schitterend Appartement",
    "it": "schitterend Appartement",
    "es": "Excelente Apartamento",
    "pt": "Excelente Apartamento"
  },
  "description_short": {
    "fr": "Construit en 1913, le Nordelec demeure un exemple remarquable...",
    "en": "Built in 1913, the Nordelec remains an outstanding example ...",
    "de": "Erbaut im Jahr 1913, bleibt die Nordelec ein hervorragendes Beispiel ...",
    "nl": "Gebouwd in 1913, de Nordelec blijft een uitstekend voorbeeld ...",
    "it": "Costruito nel 1913, il Nordelec rimane un esempio eccezionale ...",
    "es": "Construido en 1913, el Nordelec sigue siendo un ejemplo sobresaliente ...",
    "pt": "Construído em 1913, o Nordelec continua a ser um exemplo notável ..."
  },
  "description": {
    "fr": "<p><span>Un des beaux avantages de la vie aux condos Nordelec est l’accès exclusif à des espaces communs peu ordinaires</span></p>",
    "en": "<p><span>One of the beautiful advantages of living in the condos is Nordelec exclusive access to unusual public areas</span></p>",
    "de": "<p><span>Einer der schönsten Vorteile in den Eigentumswohnungen leben, ist Nordelec exklusiven Zugang zu ungewöhnlichen öffentlichen Bereichen</span></p>",
    "nl": "<p><span>Een van de prachtige voordelen van het wonen in de appartementen is Nordelec exclusieve toegang tot ongebruikelijke openbare ruimten</span></p>",
    "it": "<p><span>Una delle belle vantaggi di vivere in condomini è l'accesso esclusivo a Nordelec insolite aree pubbliche</span></p>",
    "es": "<p><span>Una de las hermosas ventajas de vivir en los condominios es Nordelec acceso exclusivo a las zonas comunes inusuales</span></p>",
    "pt": "<p><span>Uma das belas vantagens de viver em condomínios é de acesso exclusivo Nordelec para espaços públicos incomuns</span></p>"
  },
  "url": {
    "fr": "http://www.exemple.com/fr/55",
    "en": "http://www.exemple.com/en/55",
    "de": "http://www.exemple.com/de/55",
    "nl": "http://www.exemple.com/nl/55",
    "it": "http://www.exemple.com/it/55",
    "es": "http://www.exemple.com/es/55",
    "pt": "http://www.exemple.com/pt/55"
  },
  "financial":{
    "price_with_tax": 1,
    "price_from": 0,
    "rent_frequency": 30,
    "currency": "CAD",
    "price_formated": "$1,150/month",
    "price": null,
    "rent": 1150,
    "fees": 140,
    "tax1_annual": 1500,
    "tax2_annual": 2541,
    "income_annual": null,
    "formated":{
      "price": null,
      "rent": "$1,150",
      "fees": "$140",
      "tax1_annual": "$1,500",
      "tax2_annual": "$2,541",
      "income_annual": null
    }
  },
  "cover_url": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_large.jpg",
  "cover_url_hd": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_hd.jpg",
  "pictures": {
    "in_progess": true
  },
  "canada": {
    "mls": "123456"
  },
  "france": {
    "dpe_indice": null,
    "dpe_value": null,
    "dpe_id": null,
    "ges_indice": null,
    "ges_value": null,
    "alur_is_condo": null,
    "alur_units": null,
    "alur_uninhabitables": null,
    "alur_spending": null,
    "alur_legal_action": null
  },
  "belgium": {
    "peb_indice": null,
    "peb_value": null,
    "peb_id": null,
    "building_permit": null,
    "done_assignments": null,
    "preemption_property": null,
    "subdivision_permits": null,
    "sensitive_flood_area": null,
    "delimited_flood_zone": null,
    "risk_area": null
  },
  "videos": [],
  "other": {
    "orientation": {
      "north": 1,
      "south": 0,
      "east": 0,
      "west": 1
    },
    "inclusion": {
      "air_conditioning": 1,
      "hot_water": 1,
      "heated": 1,
      "electricity": 0,
      "furnished": 0,
      "fridge": 1,
      "cooker": 1,
      "dishwasher": 1,
      "dryer": 1,
      "washer": 1,
      "microwave": 0
    },
    "detail": {
      "elevator": 1,
      "laundry": 0,
      "garbage_chute": 1,
      "common_space": 1,
      "janitor": 1,
      "gym": 1,
      "golf": 1,
      "tennis": 1,
      "sauna": 1,
      "spa": 1,
      "inside_pool": 0,
      "outside_pool": 1,
      "inside_parking": 1,
      "outside_parking": 1,
      "parking_on_street": 1,
      "garagebox": 0
    },
    "half_furnsihed": {
      "living_room": 0,
      "bedrooms": 0,
      "kitchen": 0,
      "other": 0
    },
    "indoor": {
      "attic": 0,
      "attic_convertible": 0,
      "attic_converted": 0,
      "cellar": 0,
      "central_vacuum": 0,
      "entries_washer_dryer": 1,
      "entries_dishwasher": 1,
      "fireplace": 0,
      "storage": 1,
      "walk_in": 1,
      "no_smoking": 1,
      "double_glazing": 0,
      "triple_glazing": 1
    },
    "floor": {
      "carpet": 0,
      "wood": 1,
      "floating": 0,
      "ceramic": 0,
      "parquet": 0,
      "cushion": 0,
      "vinyle": 0,
      "lino": 0,
      "marble": 0
    },
    "exterior": {
      "land_access": 0,
      "back_balcony": 0,
      "front_balcony": 1,
      "private_patio": 1,
      "storage": 1,
      "terrace": 1,
      "veranda": 0,
      "garden": 0,
      "sea_view": 0,
      "mountain_view": 0
    },
    "accessibility": {
      "elevator": 0,
      "balcony": 0,
      "grab_bar": 0,
      "wider_corridors": 0,
      "lowered_switches": null,
      "ramp": 0
    },
    "senior": {
      "autonomy": 0,
      "half_autonomy": 0,
      "no_autonomy": 0,
      "meal": 0,
      "nursery": 0,
      "domestic_help": 0,
      "community": 0,
      "activities": 0,
      "validated_residence": 0,
      "housing_cooperative": 0
    },
    "pet": {
      "allow": 1,
      "cat": 1,
      "dog": 1,
      "little_dog": 1,
      "cage_aquarium": 1,
      "not_allow": 0
    },
    "service": {
      "internet": 1,
      "tv": 1,
      "tv_sat": 1,
      "phone": 1
    },
    "composition": {
      "bar": 0,
      "living": 0,
      "dining": 0,
      "separe_toilet": 0,
      "open_kitchen": 1
    },
    "heating": {
      "electric": 1,
      "solar": 0,
      "gaz": 0,
      "condensation": 0,
      "fuel": 0,
      "heat_pump": 0,
      "floor_heating": 0
    },
    "transport": {
      "bicycle_path": 1,
      "public_transport": 1,
      "public_bike": 1,
      "subway": 1,
      "train_station": 1
    },
    "security": {
      "guardian": 1,
      "alarm": 1,
      "intercom": 1,
      "camera": 1,
      "smoke_dectector": 1
    }
  }
}
```

Attributes | &nbsp;
---------- | -------
uid <br> **string** <p style='color:red'>required</p> | The unique idetifier of the property in your system (must be unique)
master <br> **integer** | Property is orginal version ?<ul><li>0 - Duplicate property</li><li>1 - Original property</li></ul>
account_uid <br> **string** <p style='color:red'>required</p> | This entry is used to allocate a property to a user
building_uid  | This entry is used to allocate a property to a building
online <br> **integer** <br> | Property status <ul><li>0 - Offline</li><li>1 - Online</li></ul>
classification_id <br> **integer** <p style='color:red'>required for creation (POST)</p>| <a href="#classification_list"> See classification list below</a>
status_id <br> **integer** <br> <p style='color:red'>required for creation (POST)</p>| In what state is the property?<ul><li>1 - New</li><li>2 - Used</li></ul>
listing_id <br> **integer** <br> <p style='color:red'>required for creation (POST)</p>| Is the property for sale or for rent?<ul><li>1 - For sale</li><li>2 - For rent</li></ul>
unit <br> **string**  | Unit number of the property
level <br> **integer**  | Floor number in the building
cadastre <br> **string**  | The property's land register number
flag_id <br> **integer** | <ul><li>1 -New</li><li>2 - Price reduction</li><li>3- Open house</li><li>4 - Favorite</li><li>5 - Sold/Rent</li></ul>
year_of_built <br> **integer** | Year when the property was built
availability_date <br> **datetime** | Availability date of the property
is_luxurious <br> **integer**| Is it luxurious ad?<ul><li>0 - no</li><li>1- yes</li></ul>
address.address_visibility <br> **integer** | Do you wish your address to be visible?<ul><li>0 - no</li><li>1- yes</li></ul>
address.address_formatted <br> **string** | property's full address (can be geocoded)
address.street_number <br> **string** | Street number
address.street_name <br> **string** | Street name (without street number)
address.zipcode <br> **string** | Zipcode
address.locality <br> **string** | Town
address.sublocality <br> **string** | Neighborhood
address.administrative_area_level1 <br> **string** | Region/province
address.administrative_area_level2 <br> **string** | Municipality/county
address.country <br> **string** | &nbsp;
address.latitude <br> **float** | &nbsp;
address.longitude <br> **float** | &nbsp;
area.living <br> **integer** | Living surface of the property
area.balcony <br> **integer** | Balcony surface
area.land <br> **integer** | Land surface
area.unit_id <br> **integer** | <ul><li>1 - square meter (m2)</li><li>2 - square meters (sq)</li></ul>
rooms.rooms <br> **integer** | Total number of rooms
rooms.bathrooms <br> **integer** | Number of bathrooms
rooms.bedrooms <br> **integer** | Number of bedrooms
rooms.showerrooms <br> **integer** | Number of shower rooms
rooms.toilets <br> **integer** | Number of toilets
title <br> **string** | Title of the ad in different languages
description_short <br> **text** | Ad description in different languages (can NOT use html content)
description <br> **text** | description of the ad in different languages (can use html content)
url <br> **text** | Url of the original ad in different languages
financial.price <br> **integer** | Property price
financial.price_with_tax <br> **integer** | Property price contains tax?<ul><li>0 - no</li><li>1- yes</li></ul>
financial.rent <br> **integer** | Rent amount
financial.fees <br> **integer** | Amount of monthly property fees
financial.rent_frequency <br> **integer** | Rent Frequency<ul><li>1 - per day</li><li>7 - per week</li><li>30 - per month</li><li>90 - per 3 months</li><li>180 - per 6 months</li><li>360 - per year</li></ul>
financial.deposit <br> **integer** | Rent deposit
financial.tax1_annual <br> **integer** | Tax #1 annually
financial.tax2_annual <br> **integer** | Tax #2 annually
financial.income_annual <br> **integer** | Annual income for an income property
financial.currency <br> **string** | currency with <a href="https://en.wikipedia.org/wiki/ISO_4217">ISO 4217 standard</a> (CAD, USD, EUR...)
pictures <br> **string** | max:10Mo Use 900x600px to obtain the best results
canada.mls <br> **string** | MLS identifier of the property
france.dpe_indice <br> **string** | France DPE Indice<ul><li>A</li><li>B</li><li>C</li><li>D</li><li>E</li><li>F</li><li>G</li></ul>
france.dpe_value <br> **string** | France DPE Value (KWh/m<sup>2</sup>)
france.ges_indice <br> **string**| France GES (Gazes à effet de serre) indice<ul><li>A</li><li>B</li><li>C</li><li>D</li><li>E</li><li>F</li><li>G</li><li>H</li><li>I</li></ul>
france.ges_value <br> **string** | France GES Value (CO<sup>2</sup>/m<sup>2</sup>/an)
france.alur_is_condo <br> **string** | LOI ALUR - property is on co-ownership?<ul><li>0 - no</li><li>1- yes</li></ul>
france.alur_units <br> **integer** | LOI ALUR - number of units
france.alur_uninhabitables <br> **integer** | LOI ALUR - number of uninhabitable units
france.alur_spending <br> **integer** | LOI ALUR - Annual Spending
france.alur_legal_action <br> **integer** | LOI ALUR - Syndic d'immeuble en procédure judiciaire?<ul><li>0 - no</li><li>1- yes</li></ul>
france.rent_honorary <br> **integer** | Honoraires de location
france.mandat_exclusif <br> **integer** | Mandat Exclusif<ul><li>0 - no</li><li>1- yes</li></ul>
belgium.peb_indice <br> **string** | Belgium peb Indice<ul><li>A</li><li>B</li><li>C</li><li>D</li><li>E</li><li>F</li><li>G</li></ul>
belgium.peb_value <br> **string** | Belgium peb Value (KWh/m<sup>2</sup>)
belgium.peb_id <br> **string** | Belgium peb Identification
belgium.building_permit <br> **integer** | building permit<ul><li>0 - no</li><li>1- yes</li></ul>
belgium.done_assignments <br> **integer** | Done assignments<ul><li>0 - no</li><li>1- yes</li></ul>
belgium.preemption_property <br> **integer** | Preemption on this property<ul><li>0 - no</li><li>1- yes</li></ul>
belgium.subdivision_permits <br> **integer** | Subdivision permits<ul><li>0 - no</li><li>1- yes</li></ul>
belgium.sensitive_flood_area <br> **integer** | Sensitive floods area<ul><li>0 - no</li><li>1- yes</li></ul>
belgium.delimited_flood_zone <br> **integer** | Delimited flood zone<ul><li>0 - no</li><li>1- yes</li></ul>
belgium.risk_area <br> **integer** | Area with risk<ul><li>0 - no</li><li>1- yes</li></ul>
orientation.north <br> **integer** | North orientation?<ul><li>0 - no</li><li>1- yes</li></ul>
orientation.south <br> **integer** | south orientation?<ul><li>0 - no</li><li>1- yes</li></ul>
orientation.east <br> **integer** | east orientation?<ul><li>0 - no</li><li>1- yes</li></ul>
orientation.west <br> **integer** | west orientation?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.air_conditioning <br> **integer** | Property equipped with air conditionning?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.hot_water <br> **integer** | Heated water included in rent?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.heated <br> **integer** | Heating included in rent?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.electricity <br> **integer** | Electricity included in rent?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.furnished <br> **integer** | Is the property furnished?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.fridge <br> **integer** | Property equipped with a fridge?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.cooker <br> **integer** | Property equipped with a cooker?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.dishwasher <br> **integer** | Property equipped with a dishwasher?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.dryer <br> **integer** | Property equipped with a dryer?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.washer <br> **integer** | Property equipped with a washer?<ul><li>0 - no</li><li>1- yes</li></ul>
inclusion.microwave <br> **integer** | Property equipped with a microwave?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.elevator <br> **integer** | Building equipped with a elevator?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.laundry <br> **integer** | Property/Building equipped with a laundry?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.garbage_chute <br> **integer** | Building equipped with a garbage chute?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.common_space <br> **integer** | Common spaces in the building?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.janitor <br> **integer** | Janitorial service in the building?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.gym <br> **integer** | Gym service in the building?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.golf <br> **integer** | Property equipped with private golf course?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.tennis <br> **integer** | Property equipped with private tennis court?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.sauna <br> **integer** | Property equipped with sauna?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.spa <br> **integer** | Property equipped with spa?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.inside_pool <br> **integer** | Property equipped with inside pool?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.outside_pool <br> **integer** | Property equipped with outside pool?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.inside_parking <br> **integer** | Property equipped with inside parking?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.outside_parking <br> **integer** | Property equipped with outside parking?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.parking_on_street <br> **integer** | Parking only possible on street?<ul><li>0 - no</li><li>1- yes</li></ul>
detail.garagebox <br> **integer** | Garabox Available<ul><li>0 - no</li><li>1- yes</li></ul>
half_furnished.living_room <br> **integer** | Only the living room is furnished<ul><li>0 - no</li><li>1- yes</li></ul>
half_furnished.bedrooms <br> **integer** | Only the bedrooms are furnished<ul><li>0 - no</li><li>1- yes</li></ul>
half_furnished.kitchen <br> **integer** | Only the kitchen is furnished<ul><li>0 - no</li><li>1- yes</li></ul>
half_furnished.other <br> **integer** | Only another room is furnished<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.attic <br> **integer** | Property equipped with attic?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.attic_convertible <br> **integer** | Property equipped with convertible attic?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.attic_converted <br> **integer** | Property equipped with converted attic?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.cellar <br> **integer** | Property equipped with cellar?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.central_vacuum <br> **integer** | Property equipped with central vacuum?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.entries_washer_dryer <br> **integer** | Property equipped with washer/dryer?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.entries_washer_dishwasher <br> **integer** | Property equipped with dishwasher entry?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.fireplace <br> **integer** | Property equipped with fireplace?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.storage <br> **integer** | Property equipped with storage?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.walk_in <br> **integer** | Property equipped with walkin?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.no_smoking <br> **integer** | smoking is forbidden in the building?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.double_gazing <br> **integer** | Property equipped with double glazing?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.triple_glazing <br> **integer** | Property equipped with triple glazing?<ul><li>0 - no</li><li>1- yes</li></ul>
indoor.carpet <br> **integer** | Carpet flooring in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
floor.wood <br> **integer** | Wood floor in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
floor.floating <br> **integer** | Floating floor in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
floor.ceramic <br> **integer** | Ceramic floor in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
floor.parquet <br> **integer** | Parquet floor in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
floor.cushion <br> **integer** | Cushion floor in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
floor.vinyle <br> **integer** | Vynile floor in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
floor.marble <br> **integer** | Marble floor in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.land_access <br> **integer** | Access to exterior land in the property?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.back_balcony <br> **integer** | Property equipped with a back balcony?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.front_balcony <br> **integer** | Property equipped with a front balcony?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.private_patio <br> **integer** | Property equipped with a private patio?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.storage <br> **integer** | Property equipped with exterior storage?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.terrace <br> **integer** | Property equipped with a terrace?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.veranda <br> **integer** | Property equipped with a veranda?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.garden <br> **integer** | Property equipped with a garden?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.sea_view <br> **integer** | Property has a sea view?<ul><li>0 - no</li><li>1- yes</li></ul>
exterior.mountain_view <br> **integer** | Property has a mountain view?<ul><li>0 - no</li><li>1- yes</li></ul>
accessibility.elevator <br> **integer** | Elevator adapted for use by disabled people<ul><li>0 - no</li><li>1- yes</li></ul>
accessibility.balcony <br> **integer** | Balcony adapted for use by disabled people<ul><li>0 - no</li><li>1- yes</li></ul>
accessibility.grab_bar <br> **integer** | Property equipped with brab bars for disabled people<ul><li>0 - no</li><li>1- yes</li></ul>
accessibility.wider_corridors <br> **integer** | Corridors widened for use by disabled people<ul><li>0 - no</li><li>1- yes</li></ul>
accessibility.lowered_switches <br> **integer** | Switches lowered for use by disabled people<ul><li>0 - no</li><li>1- yes</li></ul>
accessibility.ramp <br> **integer** | Property equipped with access ramp<ul><li>0 - no</li><li>1- yes</li></ul>
senior.autonomy <br> **integer** | Retirement home for autonomous seniors<ul><li>0 - no</li><li>1- yes</li></ul>
senior.half_autonomy <br> **integer** | Retirement home for semi-autonomous seniors<ul><li>0 - no</li><li>1- yes</li></ul>
senior.no_autonomy <br> **integer** | Retirement home for non-autonomous seniors<ul><li>0 - no</li><li>1- yes</li></ul>
senior.meal <br> **integer** | Retirement home prepared meals<ul><li>0 - no</li><li>1- yes</li></ul>
senior.nursery <br> **integer** | Retirement home with nursery<ul><li>0 - no</li><li>1- yes</li></ul>
senior.domestic_help <br> **integer** | Retirement home with domestic help<ul><li>0 - no</li><li>1- yes</li></ul>
senior.community <br> **integer** | Retirement home with community spaces<ul><li>0 - no</li><li>1- yes</li></ul>
senior.activities <br> **integer** | Retirement home with community activities<ul><li>0 - no</li><li>1- yes</li></ul>
senior.validated_residence <br> **integer** | Certified retirement home<ul><li>0 - no</li><li>1- yes</li></ul>
senior.housing_cooperative <br> **integer** | Housing cooperative<ul><li>0 - no</li><li>1- yes</li></ul>
pet.allow <br> **integer** | Pets allowed in property<ul><li>0 - no</li><li>1- yes</li></ul>
pet.not_allow <br> **integer** | Pets not allowed in property<ul><li>0 - no</li><li>1- yes</li></ul>
pet.cat <br> **integer** | Cats allowed in property<ul><li>0 - no</li><li>1- yes</li></ul>
pet.dog <br> **integer** | Dogs allowed in property<ul><li>0 - no</li><li>1- yes</li></ul>
pet.little_dog <br> **integer** | Little dogs allowed in property<ul><li>0 - no</li><li>1- yes</li></ul>
pet.cage_aquarium <br> **integer** | Aquariums allowed in property<ul><li>0 - no</li><li>1- yes</li></ul>
service.internet <br> **integer** | Internet subscription included in rent<ul><li>0 - no</li><li>1- yes</li></ul>
service.tv <br> **integer** | TV subscription included in rent<ul><li>0 - no</li><li>1- yes</li></ul>
service.tv_sat <br> **integer** | Sat TV subscription included in rent<ul><li>0 - no</li><li>1- yes</li></ul>
service.phone <br> **integer** | Phone subscription included in rent<ul><li>0 - no</li><li>1- yes</li></ul>
composition.bar <br> **integer** | Property equipped with bar<ul><li>0 - no</li><li>1- yes</li></ul>
composition.living <br> **integer** | Property equipped with living room<ul><li>0 - no</li><li>1- yes</li></ul>
composition.dining <br> **integer** | Property equipped with dining room<ul><li>0 - no</li><li>1- yes</li></ul>
composition.separate_toilet <br> **integer** | Property equipped with separate toilet<ul><li>0 - no</li><li>1- yes</li></ul>
composition.open_kitchen <br> **integer** | Property equipped with open kitchen<ul><li>0 - no</li><li>1- yes</li></ul>
heating.electric <br> **integer** | Heating is electric<ul><li>0 - no</li><li>1- yes</li></ul>
heating_solar <br> **integer** | Heating is solar<ul><li>0 - no</li><li>1- yes</li></ul>
heating.gaz <br> **integer** | Heating with gas<ul><li>0 - no</li><li>1- yes</li></ul>
heating.condensation <br> **integer** | Heating with condensation<ul><li>0 - no</li><li>1- yes</li></ul>
heating.fuel <br> **integer** | Heating with fuel<ul><li>0 - no</li><li>1- yes</li></ul>
heating.heat_pump <br> **integer** | Heating with heatpump<ul><li>0 - no</li><li>1- yes</li></ul>
heating.floor_heating <br> **integer** | Floor heating<ul><li>0 - no</li><li>1- yes</li></ul>
transport.bicycle_path <br> **integer** | Bicycle path in proximity to property<ul><li>0 - no</li><li>1- yes</li></ul>
transport.public_transport <br> **integer** | Public transport in proximity to property<ul><li>0 - no</li><li>1- yes</li></ul>
transport.public_bike <br> **integer** | Public bike in proximity to property<ul><li>0 - no</li><li>1- yes</li></ul>
transport.subway <br> **integer** | Subway in proximity to property<ul><li>0 - no</li><li>1- yes</li></ul>
transport.train_station <br> **integer** | Train station path in proximity to property<ul><li>0 - no</li><li>1- yes</li></ul>
security.guardian <br> **integer** | Guard services in property<ul><li>0 - no</li><li>1- yes</li></ul>
security.alarm <br> **integer** | Property equipped with alarm<ul><li>0 - no</li><li>1- yes</li></ul>
security.intercom <br> **integer** | Property equipped with alarm<ul><li>0 - no</li><li>1- yes</li></ul>
security.camera <br> **integer** | Property equipped with camera<ul><li>0 - no</li><li>1- yes</li></ul>
security.smoke_detector <br> **integer** | Property equipped with smoke detector<ul><li>0 - no</li><li>1- yes</li></ul>


<p id="classification_list">List of available subclassifications :</p>
<h3>House</h3>
<ul>
    <li>101 - House</li>
    <li>102 - Twin house</li>
    <li>103 - Attached house</li>
    <li>104 - Mansion</li>
    <li>105 - Individual house</li>
    <li>106 - House to renovate</li>
    <li>107 - Farm</li>
    <li>108 - Farmhouse</li>
    <li>109 - Villa</li>
    <li>110 - Bungalow</li>
    <li>111 - Castle</li>
    <li>112 - Monastery</li>
    <li>113 - Cottage</li>
    <li>114 - Town house</li>
    <li>115 - Village house</li>
    <li>116 - House without land</li>
    <li>117 - Property</li>
    <li>118 - Two-family house</li>
    <li>119 - Various</li>
</ul>
<h3>Appartement</h3>
<ul>
    <li>201 - Apartment</li>
    <li>202 - Room</li>
    <li>203 - Studio</li>
    <li>204 - Duplex</li>
    <li>205 - Triplex</li>
    <li>206 - Penthouse</li>
    <li>207 - Loft</li>
    <li>208 - Furnished apartment</li>
    <li>209 - Plex</li>
    <li>210 - Condo</li>
</ul>
<h3>Garage</h3>
<ul>
    <li>301 - Garage</li>
    <li>302 - Parking</li>
    <li>303 - Covered parking</li>
    <li>304 - Open parking</li>
    <li>305 - Box</li>
</ul>
<h3>Land</h3>
<ul>
    <li>401 - Land</li>
    <li>402 - Construction land</li>
    <li>403 - Agricultural land</li>
    <li>404 - Land for leisure</li>
    <li>405 - Industrial land</li>
</ul>
<h3>Income property</h3>
<ul>
    <li>501 - Income property</li>
    <li>502 - Income land</li>
</ul>
<h3>Allotment</h3>
<ul>
    <li>601 - Allotment</li>
    <li>602 - Future project</li>
    <li>603 - Residential development</li>
    <li>604 - Land to build</li>
</ul>
<h3>Residence</h3>
<ul>
    <li>701 - Residence</li>
    <li>702 - Construction project</li>
    <li>703 - New construction</li>
    <li>704 - Residence under construction</li>
</ul>
<h3>Commercial use</h3>
<ul>
    <li>801 - Commercial use</li>
    <li>802 - Office</li>
    <li>803 - Virtual office</li>
    <li>804 - Commercial</li>
    <li>805 - Warehouse</li>
    <li>806 - Storehouse</li>
    <li>807 - Restaurant</li>
    <li>808 - Hotel</li>
    <li>809 - Liberal profession office</li>
    <li>810 - Hangar</li>
    <li>811 - Industrial</li>
    <li>812 - Parking</li>
    <li>813 - Residential</li>
    <li>814 - Retail</li>
    <li>815 - Other</li>
</ul>


##Retrieve a property

<blockquote class="request_class">Definition</blockquote>
>[GET] https://shareimmo.com/api/v2/properties/:uid


<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/properties/50

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 149,
  "uid": "50",
  "master": 1,
  "account_id": 2,
  "account_uid": "123",
  "building": {
    "id": 12,
    "uid": "3",
    "name": "Le nordelec",
    "cover_url": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_697_large.jpg",
    "cover_url_hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_697_hd.jpg"
  },
  "online": 1,
  "classification_id": 201,
  "classification": {
    "fr": "Appartement",
    "en": "Apartement",
    "category": {
      "id": 2,
      "fr": "Appartement",
      "en": "Apartement"
    }
  },
  "status_id": 1,
  "status": {
    "fr": "Neuf",
    "en": "New"
  },
  "listing_id": 2,
  "listing": {
    "fr": "À Louer",
    "en": "For Rent"
  },
  "unit": "Appt #603",
  "level": 6,
  "cadastre": "nordelec-603",
  "flag_id": 1,
  "flag": {
    "fr": "Nouveauté",
    "en": "Newness"
  },
  "year_of_built": 2012,
  "availability_date": "2017-01-16",
  "is_luxurious": 1,
  "address": {
    "address_visibility": 1,
    "address_formatted": "1740 Rue St-Patrick, Montréal, QC H3K 2H2, Canada",
    "street_number": "1740",
    "street_name": "Rue Saint-Patrick",
    "zipcode": "H3K 2H2",
    "locality": "Montréal",
    "sublocality": "Pointe-Saint-Charles",
    "administrative_area_level1": "Québec",
    "administrative_area_level2": "Communauté-Urbaine-de-Montréal",
    "country": "CA",
    "latitude": 45.4854,
    "longitude": -73.5629
  },
  "area": {
    "living": 1000,
    "balcony": 20,
    "land": null,
    "unit_id": 2
  },
  "rooms": {
    "rooms": 3,
    "bathrooms": 1,
    "bedrooms": 1,
    "showerrooms": null
  },
  "title": {
    "fr": "Superbe Appartement",
    "en": "superb Apartment",
    "de": "Superb Wohnung",
    "nl": "schitterend Appartement",
    "it": "schitterend Appartement",
    "es": "Excelente Apartamento",
    "pt": "Excelente Apartamento"
  },
  "description_short": {
    "fr": "Construit en 1913, le Nordelec demeure un exemple remarquable...",
    "en": "Built in 1913, the Nordelec remains an outstanding example ...",
    "de": "Erbaut im Jahr 1913, bleibt die Nordelec ein hervorragendes Beispiel ...",
    "nl": "Gebouwd in 1913, de Nordelec blijft een uitstekend voorbeeld ...",
    "it": "Costruito nel 1913, il Nordelec rimane un esempio eccezionale ...",
    "es": "Construido en 1913, el Nordelec sigue siendo un ejemplo sobresaliente ...",
    "pt": "Construído em 1913, o Nordelec continua a ser um exemplo notável ..."
  },
  "description": {
    "fr": "<p><span>Un des beaux avantages de la vie aux condos Nordelec est l’accès exclusif à des espaces communs peu ordinaires</span></p>",
    "en": "<p><span>One of the beautiful advantages of living in the condos is Nordelec exclusive access to unusual public areas</span></p>",
    "de": "<p><span>Einer der schönsten Vorteile in den Eigentumswohnungen leben, ist Nordelec exklusiven Zugang zu ungewöhnlichen öffentlichen Bereichen</span></p>",
    "nl": "<p><span>Een van de prachtige voordelen van het wonen in de appartementen is Nordelec exclusieve toegang tot ongebruikelijke openbare ruimten</span></p>",
    "it": "<p><span>Una delle belle vantaggi di vivere in condomini è l'accesso esclusivo a Nordelec insolite aree pubbliche</span></p>",
    "es": "<p><span>Una de las hermosas ventajas de vivir en los condominios es Nordelec acceso exclusivo a las zonas comunes inusuales</span></p>",
    "pt": "<p><span>Uma das belas vantagens de viver em condomínios é de acesso exclusivo Nordelec para espaços públicos incomuns</span></p>"
  },
  "url": {
    "fr": "http://www.exemple.com/fr/55",
    "en": "http://www.exemple.com/en/55",
    "de": "http://www.exemple.com/de/55",
    "nl": "http://www.exemple.com/nl/55",
    "it": "http://www.exemple.com/it/55",
    "es": "http://www.exemple.com/es/55",
    "pt": "http://www.exemple.com/pt/55"
  },
  "financial":{
    "price_with_tax": 1,
    "price_from": 0,
    "rent_frequency": 30,
    "currency": "CAD",
    "price_formated": "$1,150/month",
    "price": null,
    "rent": 1150,
    "fees": 140,
    "tax1_annual": 1500,
    "tax2_annual": 2541,
    "income_annual": null,
    "formated":{
      "price": null,
      "rent": "$1,150",
      "fees": "$140",
      "tax1_annual": "$1,500",
      "tax2_annual": "$2,541",
      "income_annual": null
    }
  },
  "cover_url": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_large.jpg",
  "cover_url_hd": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_hd.jpg",
  "pictures": [
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_697_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_698_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_698_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_698_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_698_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_698_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_698_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_699_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_699_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_699_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_699_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_699_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/149/shareimmo_149_699_hd.jpg"
    }
  ],
  "canada": {
    "mls": "123456"
  },
  "france": {
    "dpe_indice": null,
    "dpe_value": null,
    "dpe_id": null,
    "ges_indice": null,
    "ges_value": null,
    "alur_is_condo": null,
    "alur_units": null,
    "alur_uninhabitables": null,
    "alur_spending": null,
    "alur_legal_action": null
  },
  "belgium": {
    "peb_indice": null,
    "peb_value": null,
    "peb_id": null,
    "building_permit": null,
    "done_assignments": null,
    "preemption_property": null,
    "subdivision_permits": null,
    "sensitive_flood_area": null,
    "delimited_flood_zone": null,
    "risk_area": null
  },
  "videos": [],
  "other": {
    "orienttation": {
      "north": 1,
      "south": 0,
      "east": 0,
      "west": 1
    },
    "inclusion": {
      "air_conditioning": 1,
      "hot_water": 1,
      "heated": 1,
      "electricity": 0,
      "furnished": 0,
      "fridge": 1,
      "cooker": 1,
      "dishwasher": 1,
      "dryer": 1,
      "washer": 1,
      "microwave": 0
    },
    "detail": {
      "elevator": 1,
      "laundry": 0,
      "garbage_chute": 1,
      "common_space": 1,
      "janitor": 1,
      "gym": 1,
      "golf": 1,
      "tennis": 1,
      "sauna": 1,
      "spa": 1,
      "inside_pool": 0,
      "outside_pool": 1,
      "inside_parking": 1,
      "outside_parking": 1,
      "parking_on_street": 1,
      "garagebox": 0
    },
    "half_furnsihed": {
      "living_room": 0,
      "bedrooms": 0,
      "kitchen": 0,
      "other": 0
    },
    "indoor": {
      "attic": 0,
      "attic_convertible": 0,
      "attic_converted": 0,
      "cellar": 0,
      "central_vacuum": 0,
      "entries_washer_dryer": 1,
      "entries_dishwasher": 1,
      "fireplace": 0,
      "storage": 1,
      "walk_in": 1,
      "no_smoking": 1,
      "double_glazing": 0,
      "triple_glazing": 1
    },
    "floor": {
      "carpet": 0,
      "wood": 1,
      "floating": 0,
      "ceramic": 0,
      "parquet": 0,
      "cushion": 0,
      "vinyle": 0,
      "lino": 0,
      "marble": 0
    },
    "exterior": {
      "land_access": 0,
      "back_balcony": 0,
      "front_balcony": 1,
      "private_patio": 1,
      "storage": 1,
      "terrace": 1,
      "veranda": 0,
      "garden": 0,
      "sea_view": 0,
      "mountain_view": 0
    },
    "accessibility": {
      "elevator": 0,
      "balcony": 0,
      "grab_bar": 0,
      "wider_corridors": 0,
      "lowered_switches": null,
      "ramp": 0
    },
    "senior": {
      "autonomy": 0,
      "half_autonomy": 0,
      "no_autonomy": 0,
      "meal": 0,
      "nursery": 0,
      "domestic_help": 0,
      "community": 0,
      "activities": 0,
      "validated_residence": 0,
      "housing_cooperative": 0
    },
    "pet": {
      "allow": 1,
      "cat": 1,
      "dog": 1,
      "little_dog": 1,
      "cage_aquarium": 1,
      "not_allow": 0
    },
    "service": {
      "internet": 1,
      "tv": 1,
      "tv_sat": 1,
      "phone": 1
    },
    "composition": {
      "bar": 0,
      "living": 0,
      "dining": 0,
      "separe_toilet": 0,
      "open_kitchen": 1
    },
    "heating": {
      "electric": 1,
      "solar": 0,
      "gaz": 0,
      "condensation": 0,
      "fuel": 0,
      "heat_pump": 0,
      "floor_heating": 0
    },
    "transport": {
      "bicycle_path": 1,
      "public_transport": 1,
      "public_bike": 1,
      "subway": 1,
      "train_station": 1
    },
    "security": {
      "guardian": 1,
      "alarm": 1,
      "intercom": 1,
      "camera": 1,
      "smoke_dectector": 1
    }
  }
}

```

Attributes | &nbsp;
---------- | -------
uid <br> **string** | Your identifier of the property to be retrieved


##Delete a property

<blockquote class="request_class">Definition</blockquote>
>[DELETE] https://shareimmo.com/api/v2/properties/:uid


<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/properties/50

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 149,
  "uid": "50",
  "account_id": 2,
  "account_uid": "123",
  "message": "Property deletion was launched"
}
```

Attributes | &nbsp;
---------- | -------
uid <br> **string** | Your identifier of the property to be deleted




# Contacts

This is an object representing your shareimmo contacts



## Create & Update a contact

<blockquote class="request_class">Definition</blockquote>
>[POST|PUT] https://shareimmo.com/api/v2/contacts


<blockquote class="request_class">Example Request</blockquote>
```json
{
  "uid": "4",
  "account_uid": "123",
  "email": "cyrielle@bienvenue.com",
  "civility": 3,
  "last_name": "Cyrielle",
  "first_name": "Bocquet",
  "type": 2,
  "phone": "+1 514 568 6780",
  "mobile": "+1 514 568 6780",
  "iban": "FR01 2345 6789 8765 4321 0123 456",
  "address": {
    "street_name": "460, rue Sainte-Catherine Ouest",
    "city": "Montréal",
    "zipcode": "h3E 0A8",
    "country": "CA"
  },
  "picture_url": "https://randomuser.me/api/portraits/men/80.jpg"
}
```

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 35,
  "uid": "4",
  "account_uid": "123",
  "email": "cyrielle@bienvenue.com",
  "civility": 3,
  "first_name": "Bocquet",
  "last_name": "Cyrielle",
  "phone": "+1 514 568 6780",
  "mobile": "+1 514 568 6780",
  "type": 2,
  "iban": "FR01 2345 6789 8765 4321 0123 456",
  "address": {
    "street_name": "460, rue Sainte-Catherine Ouest",
    "city": "Montréal",
    "zipcode": "h3E 0A8",
    "country": "CA"
  },
  "picture_url": "https://randomuser.me/api/portraits/men/80.jpg"
}
```


Attributes | &nbsp;
---------- | -------
uid <br> **string** <br><p style='color:red'>required</p>| The unique identifier of the contacts in your system (must be unique)
account_uid <br> **string** <br><p style='color:red'>required</p> | This entry is used to allocate a contact to a user
email  <br> **string**  <br><p style='color:red'>required</p>| Contact email
civility  <br> **integer** | Contact civility <ul><li>1 - Mister</li><li>2- Madam</li><li>3- Miss</li></ul>
first_name <br> **string** | Contact first name
last_name <br> **string** | Contact last name
phone <br> **string** | Contact phone
mobile <br> **string** | Contact mobile
type  <br> **integer** | Contact type <ul><li>1 - owner</li><li>2- tenant</li><li>3- agent</li><li>4- manager</li><li>5 - Co-worker</li></ul>
iban <br> **string** | Contact International Bank Account Number
address.street_name <br> **string** | &nbsp;
address.city <br> **string** | &nbsp;
address.locality <br> **string** | &nbsp;
address.zipcode <br> **string** |  &nbsp;
address.country <br> **string** | The country attached to the account with <a href='https://en.wikipedia.org/wiki/ISO_3166-1' target='_blank'>ISO 3166-1 alpha-2 standard</a>
picture_url  <br> **string** | Max: 2Mo Square format for the best result (500x500)



## Retrieve a contact
<blockquote class="request_class">Definition</blockquote>
>[GET] https://shareimmo.com/api/v2/contacts/:uid


<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/contacts/4

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 35,
  "uid": "4",
  "account_uid": "123",
  "email": "cyrielle@bienvenue.com",
  "civility": 3,
  "first_name": "Bocquet",
  "last_name": "Cyrielle",
  "phone": "+1 514 568 6780",
  "mobile": "+1 514 568 6780",
  "type": 2,
  "iban": "FR01 2345 6789 8765 4321 0123 456",
  "address": {
    "street_name": "460, rue Sainte-Catherine Ouest",
    "city": "Montréal",
    "zipcode": "h3E 0A8",
    "country": "CA"
  },
  "picture_url": "https://randomuser.me/api/portraits/men/80.jpg"
}
```

Attributes | &nbsp;
---------- | -------
uid <br> **string** | Your identifier of the contact to be retrieved


## Delete a contact
<blockquote class="request_class">Definition</blockquote>
>[DELETE] https://shareimmo.com/api/v2/contacts/:uid


<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/contacts/4

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 35,
  "uid": "4",
  "account_id": 2,
  "account_uid": "123",
  "message": "Contact deletion was launched"
}
```

Attributes | &nbsp;
---------- | -------
uid <br> **string** | Your identifier of the contact to be deleted



## Associate a contact to a property

<blockquote class="request_class">Definition</blockquote>
>[POST] https://shareimmo.com/api/v2/contacts/property



<blockquote class="request_class">Example Request</blockquote>
```json
{
  "contact_uid": "4",
  "property_uid": "50"
}
```


<blockquote class="request_class">Example Response</blockquote>
```json
{
  "uid": "4",
  "property_uid": "50",
  "message": "Contact was associated to property"
}
```



Attributes | &nbsp;
---------- | -------
contact_uid <br> **string** | Your identifier of the contact
property_uid <br> **string** | Your identifier of the property



# Searches
<aside class="warning" style="color:#FFFFFF;">
By defaults api Key/Token are not authorized on search API. To activated them, please contact our technical team at +1 (514) 700-1963 or <a style="color:#FFFFFF;font-weight:bold" href='mailto:'contact@shareimmo.com''>contact@shareimmo.com</a>
</aside>


## Buildings
<blockquote class="request_class">Definition</blockquote>
>[GET] https://shareimmo.com/api/v2/search/buildings


<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/seach/buildings?per_page=1&page=2&country=CA,US,FR

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "took": 4,
  "pagination": {
    "total": 5,
    "current_page": 2,
    "previous_page": 1,
    "next_page": 3,
    "length": 1,
    "offset": 1,
    "from": 2,
    "to": 2,
    "formated": "2-2 sur 5"
  },
  "buildings": [
    {
      "_index": "shareimmo_development_buildings",
      "_type": "building",
      "_id": "12",
      "_score": 0,
      "_source": {
        "id": 12,
        "uid": "3",
        "draft": 0,
        "api_provider_id": 21,
        "user_id": 2,
        "address": "1740 Rue St-Patrick, Montréal, QC H3K 2H2, Canada",
        "location_geohash": "f25dy035sjn",
        "locality": "Montréal",
        "country": "CA",
        "cover_url": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_medium.jpg",
        "updated_at": "2016-09-14",
        "units": 8,
        "name": "Le nordelec",
        "location": {
          "lat": 45.4854,
          "lon": -73.5629
        },
        "description_short": {
          "fr": "Construit en 1913, le Nordelec demeure un exemple remarquable...",
          "en": "Built in 1913, the Nordelec remains an outstanding example ...",
          "de": "Erbaut im Jahr 1913, bleibt die Nordelec ein hervorragendes Beispiel ...",
          "nl": "Gebouwd in 1913, de Nordelec blijft een uitstekend voorbeeld ...",
          "it": "Costruito nel 1913, il Nordelec rimane un esempio eccezionale ...",
          "es": "Construido en 1913, el Nordelec sigue siendo un ejemplo sobresaliente ...",
          "pt": "Construído em 1913, o Nordelec continua a ser um exemplo notável ..."
        }
      }
    }
  ]
}

```


Filters attributes | &nbsp;
---------- | -------
bounds <br> **polygon** | Polygon of coordinates separated by commas which corresponds to a square containing the properties. The first coordinate is the South-West corner, and the second is the North-East corner <br><ul><li>Example : 46.533614,-72.788911,46.574281,-72.72883</li></ul>
custom_circle <br> **polygon** | Polygon of coordinates separated by commas which corresponds to a circle containing the properties. The first coordinate is the center, and the second is the radius<br><ul><li>Example : 46.56966,-72.77421,1290.172</li></ul>
custom_poly <br> **polygon** | Polygon of coordinates separated by commas which corresponds to a custom polygon containing the properties<br><ul><li>Example : 46.58022059737275,-72.76528358459473,46.56464375460338,-72.79034614562988,46.553430379448706,-72.75472640991211,46.5681253311492,-72.75429725646973,46.5700135503882,-72.7694034576416</li></ul>
keywords <br> **string** | The keywords to look up for properties which contain this keyword anywhere in their description (you can add several separated by commas). This parameter isn't discriminating
reference <br> **string** | The number which represents the reference of your property in Shareimmo, without the # at the beginning
id <br> **string** | ids of buildings (you can add several separated by commas)
country <br> **string** | countries of properties ISO 3166 (ex: CA,US,FR)(you can add several separated by commas)


Options attributes | &nbsp;
---------- | -------
page <br> **integer** | The number of the page of properties you are currently looking at on the website
per_page <br> **integer** | The number of the page of properties you are currently looking at on the website
order_by <br> **string** | The parameter you want to order the properties by. Can be any of the building search parameters
order <br> **string** | The order of the buildings (default is 'asc') <ul><li>asc - Ascendant</li><li>desc - Descendant</li></ul>


## Building

This method returns an object building

<blockquote class="request_class">Definition</blockquote>
>[GET] https://shareimmo.com/api/v2/search/buildings/:id


<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/seach/buildings/12

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 12,
  "uid": "3",
  "account_id": 2,
  "account_uid": "123",
  "name": "Le nordelec",
  "units": 250,
  "levels": 8,
  "address": {
    "address_formatted": "1740 Rue St-Patrick, Montréal, QC H3K 2H2, Canada",
    "street_number": "1740",
    "street_name": "Rue Saint-Patrick",
    "zipcode": "H3K 2H2",
    "locality": "Montréal",
    "sublocality": "Pointe-Saint-Charles",
    "administrative_area_level1": "Québec",
    "administrative_area_level2": "Communauté-Urbaine-de-Montréal",
    "country": "CA",
    "latitude": 45.4854,
    "longitude": -73.5629
  },
  "description_short": {
    "fr": "Construit en 1913, le Nordelec demeure un exemple remarquable...",
    "en": "Built in 1913, the Nordelec remains an outstanding example ...",
    "de": "Erbaut im Jahr 1913, bleibt die Nordelec ein hervorragendes Beispiel ...",
    "nl": "Gebouwd in 1913, de Nordelec blijft een uitstekend voorbeeld ...",
    "it": "Costruito nel 1913, il Nordelec rimane un esempio eccezionale ...",
    "es": "Construido en 1913, el Nordelec sigue siendo un ejemplo sobresaliente ...",
    "pt": "Construído em 1913, o Nordelec continua a ser um exemplo notável ..."
  },
  "description": {
    "fr": "<p><span>Un des beaux avantages de la vie aux condos Nordelec est l’accès exclusif à des espaces communs peu ordinaires</span></p>",
    "en": "<p><span>One of the beautiful advantages of living in the condos is Nordelec exclusive access to unusual public areas</span></p>",
    "de": "<p><span>Einer der schönsten Vorteile in den Eigentumswohnungen leben, ist Nordelec exklusiven Zugang zu ungewöhnlichen öffentlichen Bereichen</span></p>",
    "nl": "<p><span>Een van de prachtige voordelen van het wonen in de appartementen is Nordelec exclusieve toegang tot ongebruikelijke openbare ruimten</span></p>",
    "it": "<p><span>Una delle belle vantaggi di vivere in condomini è l'accesso esclusivo a Nordelec insolite aree pubbliche</span></p>",
    "es": "<p><span>Una de las hermosas ventajas de vivir en los condominios es Nordelec acceso exclusivo a las zonas comunes inusuales</span></p>",
    "pt": "<p><span>Uma das belas vantagens de viver em condomínios é de acesso exclusivo Nordelec para espaços públicos incomuns</span></p>"
  },
  "pictures": [
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_56_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_55_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_53_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_40_hd.jpg"
    }
  ]
}
```

Attributes | &nbsp;
---------- | -------
id <br> **string** <p style='color:red'>required</p> | the shareimmo's id of the building



## Properties
<blockquote class="request_class">Definition</blockquote>
>[GET] https://shareimmo.com/api/v2/search/properties


<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/seach/properties?per_page=1&page=2&listing=1

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "took": 8,
  "pagination": {
    "total": 10,
    "current_page": 1,
    "previous_page": null,
    "next_page": 2,
    "length": 1,
    "offset": 0,
    "from": 1,
    "to": 1,
    "formated": "1-1 sur 10"
  },
  "properties": [
    {
      "_index": "shareimmo_development_properties",
      "_type": "property",
      "_id": "73",
      "_score": 0,
      "_source": {
        "id": 73,
        "uid": "19793816",
        "draft": 0,
        "api_provider_id": 9,
        "user_id": 2,
        "status": 1,
        "address": "psssrout",
        "price": 665000,
        "price_formated": "$665,000",
        "price_with_tax": 0,
        "price_from": 0,
        "location_geohash": "f25dw5cuvvf",
        "cover_url":  "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_medium.jpg",
        "cover_url_hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_hd_m.jpg",
        "area": 1367,
        "updated_at": "2016-09-01",
        "link": "/fr/ad/73",
        "rooms": 5,
        "bedrooms": 3,
        "bathrooms": 2,
        "showerrooms": 2,
        "locality": "Montréal",
        "country": "CA",
        "unit": "1101",
        "location": {
          "lat": 45.4609,
          "lon": -73.5618
        },
        "property_listing": {
          "id": 1,
          "fr": "À Vendre",
          "en": "For Sale"
        },
        "property_status": {
          "id": 2,
          "fr": "Ancien",
          "en": "Used property"
        },
        "property_flag": {
          "id": 1,
          "fr": "Nouveauté",
          "en": "Newness"
        },
        "property_classification": {
          "id": 101,
          "fr": "Maison",
          "en": "House",
          "category": {
            "id": 1,
            "fr": "Maison",
            "en": "House"
          }
        },
        "title": {
          "fr": "Appartement - 30 Allée des Brises-du-Fleuve",
          "en": "Apartment - 30 Allée des Brises-du-Fleuve",
          "de": "",
          "nl": "",
          "it": "",
          "es": "",
          "pt": ""
        },
        "description_short": {
          "fr": "",
          "en": "",
          "de": "",
          "nl": "",
          "it": "",
          "es": "",
          "pt": ""
        },
        "building": {
          "id": 2,
          "name": "Le Vistal",
          "units": 250,
          "cover_url": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_697_medium.jpg",
          "cover_url_hd": "https://s3.amazonaws.com/shareimmo/development/buildings/12/shareimmo_12_697_hd_m.jpg"
        }
      }
    }
  ]
}
```



Filters attributes | &nbsp;
---------- | -------
price_from <br> **integer** | The minimum price of the properties
price_to <br> **integer** | The maximum price of the properties
area_from <br> **integer** | The minimum surface of the properties
area_to <br> **integer** | The maximum surface of the properties
classification <br> **integer** | Numbers from 1 to 8 representing the type of property (you can add several separated by commas)","List of available classifications : <ul><li>1 - House</li><li>2 - Condo</li><li>3 - Garage</li><li>4 - Land</li><li>5 - Building</li><li>6 - Allotment</li><li>7 - Residence</li><li>8 - Commercial</li></ul>
subclassification <br> **integer** | (you can add several separated by commas) <a href="#classification_list"> See classification list above</a>
listing <br> **integer** | Wether your property is for sale or for rent (you can add several separated by commas) <ul><li>1 - For Sale</li><li>2 - For Rent</li></ul>
status <br> **integer** | Wether you property is new or not (you can add several separated by commas) <ul><li>1 - New</li><li>2 - Not new</li></ul>
rooms <br> **integer** | The minimum number of rooms
bedrooms <br> **integer** | The minimum number of bedrooms
bedrooms <br> **integer** | The minimum number of bathrooms
keywords <br> **string** | The keywords to look up for properties which contain this keyword anywhere in their description (you can add several separated by commas). This parameter isn't discriminating
flag <br> **integer** | The flags on the properties you want to receive (you can add several separated by commas) <ul><li>1 - Newness</li><li>2 - Price Reduction</li><li>3 - Open House</li><li>4 - Favorite</li><li>5 - Sold/Rented</li></ul>
reference <br> **integer** | The number which represents the reference of your property in Shareimmo, without the # at the beginning
user <br> **integer** | The user who posted the properties (you can add several separated by commas)
id <br> **integer** | ids of properties (you can add several separated by commas)
country <br> **string** | countries of properties ISO 3166 (ex: CA,US,FR)(you can add several separated by commas)
building <br> **integer** | ids of buildings (you can add several separated by commas)
bounds <br> **polygon** | Polygon of coordinates separated by commas which corresponds to a square containing the properties. The first coordinate is the South-West corner, and the second is the North-East corner <br><ul><li>Example : 46.533614,-72.788911,46.574281,-72.72883</li></ul>
custom_circle <br> **polygon** | Polygon of coordinates separated by commas which corresponds to a circle containing the properties. The first coordinate is the center, and the second is the radius<br><ul><li>Example : 46.56966,-72.77421,1290.172</li></ul>
custom_poly <br> **polygon** | Polygon of coordinates separated by commas which corresponds to a custom polygon containing the properties<br><ul><li>Example : 46.58022059737275,-72.76528358459473,46.56464375460338,-72.79034614562988,46.553430379448706,-72.75472640991211,46.5681253311492,-72.75429725646973,46.5700135503882,-72.7694034576416</li></ul>



Options attributes | &nbsp;
---------- | -------
page <br> **integer** | The number of the page of properties you are currently looking at on the website
per_page <br> **integer** | The number of the page of properties you are currently looking at on the website
order_by <br> **string** | The parameter you want to order the properties by. Can be any of the building search parameters execpt bounds,custom_circle,custom_poly
order <br> **string** | The order of the propeties (default is 'asc') <ul><li>asc - Ascendant</li><li>desc - Descendant</li></ul>




## Property

This method returns an object building

<blockquote class="request_class">Definition</blockquote>
>[GET] https://shareimmo.com/api/v2/search/properties/:id


<blockquote class="request_class">Example Request</blockquote>
>https://shareimmo.com/api/v2/seach/properties/73

<blockquote class="request_class">Example Response</blockquote>
```json
{
  "id": 73,
  "uid": "19793816",
  "master":1,
  "account_id": 2,
  "account_uid": "123",
  "building": {
    "id": 2,
    "uid": null,
    "name": "Le Vistal",
    "cover_url":    "https://s3.amazonaws.com/shareimmo/development/buildings/2/shareimmo_2_254_large.jpg",
    "cover_url_hd": "https://s3.amazonaws.com/shareimmo/development/buildings/2/shareimmo_2_254_hd.jpg",
  },
  "online": 1,
  "classification_id": 101,
  "classification": {
    "fr": "Maison",
    "en": "House",
    "category": {
      "id": 1,
      "fr": "Maison",
      "en": "House"
    }
  },
  "status_id": 2,
  "status": {
    "fr": "Ancien",
    "en": "Used property"
  },
  "listing_id": 1,
  "listing": {
    "fr": "À Vendre",
    "en": "For Sale"
  },
  "unit": "1101",
  "level": 10,
  "cadastre": "2 217 773-2 217 687 ",
  "flag_id": 1,
  "flag": {
    "fr": "Nouveauté",
    "en": "Newness"
  },
  "year_of_built": 2000,
  "availability_date": null,
  "is_luxurious": 0,
  "address": {
    "address_visibility": 1,
    "address_formatted": "psssrout",
    "street_number": "30",
    "street_name": "Allée des Brises-du-Fleuve",
    "zipcode": "H4G3M7",
    "locality": "Montréal",
    "sublocality": "Verdun/Île-des-Soeurs ",
    "administrative_area_level1": "QC",
    "administrative_area_level2": "Communauté-Urbaine-de-Montréal",
    "country": "CA",
    "latitude": 45.4609,
    "longitude": -73.5618
  },
  "area": {
    "living": 1367,
    "balcony": null,
    "land": null,
    "unit_id": 2
  },
  "rooms": {
    "rooms": 5,
    "bathrooms": 2,
    "bedrooms": 3,
    "showerrooms": null
  },
  "title": {
    "fr": "Appartement - 30 Allée des Brises-du-Fleuve",
    "en": "Apartment - 30 Allée des Brises-du-Fleuve",
    "de": "",
    "nl": "",
    "it": "",
    "es": "",
    "pt": ""
  },
  "description_short": {
    "fr": "",
    "en": "",
    "de": "",
    "nl": "",
    "it": "",
    "es": "",
    "pt": ""
  },
  "description": {
    "fr": "<p>*** RARE SUR LE MARCHE *** Situ&eacute; proche de tout, ce Penthouse de 3 chambres, avec vue panoramique sur le fleuve et la ville offre espace et luminosit&eacute; avec son aire ouverte regroupant un salon, une cuisine fonctionnelle adjacente &agrave; la salle &agrave; manger et une terrasse magnifique. Voir l&apos;ADDENDA pour tous les d&eacute;tails. UNE VALEUR SURE.</p><p><br></p><p><br></p><p>***VOICI UN APER&Ccedil;U DE DE QUE CE PENTHOUSE VOUS OFFRE***</p><p><br></p><p>A L&apos;INTERIEUR</p><p>- Un penthouse avec une vue &eacute;poustouflante et une tranquillit&eacute; de vie au dernier &eacute;tage d&apos;une copropri&eacute;t&eacute; calme et paisible.</p><p>- Une aire ouverte sur l&apos;espace de vie.</p><p>- 3 grandes chambres &agrave; coucher dont la chambre des ma&icirc;tres avec un vaste Walk-In et salle de bain attenante.</p><p>- 2 salles de bains (La deuxi&egrave;me avec douche).</p><p>- 1 foyer au bois dans le salon pour une ambiance feutr&eacute;e et un maximum de confort par temps froid.</p><p>- de nombreux rangements.</p><p>- une terrasse de plus de 100 pc avec vue imprenable sur le fleuve, la ville et les monts avoisinants.</p><p><br></p><p><br></p><p>A L&apos;EXTERIEUR</p><p>- Un garage en tandem (no 26)</p><p>- un locker grillag&eacute;</p><p>- un espace de rangement privatif ferm&eacute; (10 x 10)</p><p>- une piscine creus&eacute;e</p><p><br></p><p>L&apos;ENVIRONNEMENT</p><p>- Acc&egrave;s par voie calme et sans issue.</p><p>- Acc&egrave;s direct aux pistes cyclables en bas de l&apos;immeuble</p><p>- Proche h&ocirc;pital et commodit&eacute;s (m&eacute;tro, transports en commun).</p><p><br></p><p>AUTRE</p><p>Immeuble administr&eacute; par un gestionnaire professionnel reconnu.</p><p><br></p><p>Attention : sur certaines photos, l&apos;am&eacute;nagement de la pi&egrave;ce principale a &eacute;t&eacute; r&eacute;alis&eacute; au moyen d&apos;un proc&eacute;d&eacute; de &quot;r&eacute;alit&eacute; augment&eacute;e&quot; afin de d&eacute;montrer le potentiel de l&apos;espace transform&eacute; dans une ambiance et un style diff&eacute;rent.</p><p><br></p><p>A VOIR sur tablette num&eacute;rique, cette &quot;r&eacute;alit&eacute; augment&eacute;e&quot; en 360 sur place.</p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p>",
    "en": "<p><br></p><p><br></p>",
    "de": "",
    "nl": "",
    "it": "",
    "es": "",
    "pt": ""
  },
  "url": {
    "fr": "http://passerelle.centris.ca/redirect.aspx?CodeDest=VIACAPITALE%26NoMLS=MT19793816",
    "en": "http://passerelle.centris.ca/redirect.aspx?CodeDest=VIACAPITALE%26NoMLS=MT19793816",
    "de": "",
    "nl": "",
    "it": "",
    "es": "",
    "pt": ""
  },
  "financial":{
    "price_with_tax": 1,
    "price_from": 0,
    "rent_frequency": 30,
    "currency": "CAD",
    "price_formated": "$1,150/month",
    "price": null,
    "rent": 1150,
    "fees": 140,
    "tax1_annual": 1500,
    "tax2_annual": 2541,
    "income_annual": null,
    "formated":{
      "price": null,
      "rent": "$1,150",
      "fees": "$140",
      "tax1_annual": "$1,500",
      "tax2_annual": "$2,541",
      "income_annual": null
    }
  },
  "cover_url":    "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_large.jpg",
  "cover_url_hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_hd.jpg",
  "pictures": [
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_254_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_260_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_260_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_260_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_260_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_260_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_260_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_266_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_266_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_266_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_266_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_266_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_266_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_270_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_270_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_270_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_270_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_270_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_270_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_278_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_278_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_278_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_278_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_278_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_278_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_279_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_279_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_279_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_279_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_279_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_279_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_284_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_284_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_284_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_284_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_284_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_284_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_288_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_288_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_288_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_288_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_288_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_288_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_291_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_291_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_291_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_291_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_291_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_291_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_294_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_294_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_294_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_294_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_294_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_294_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_325_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_325_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_325_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_325_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_325_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_325_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_327_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_327_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_327_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_327_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_327_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_327_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_329_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_329_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_329_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_329_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_329_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_329_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_331_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_331_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_331_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_331_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_331_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_331_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_332_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_332_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_332_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_332_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_332_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_332_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_333_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_333_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_333_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_333_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_333_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_333_hd.jpg"
    },
    {
      "small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_334_small.jpg",
      "medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_334_medium.jpg",
      "large": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_334_large.jpg",
      "hd_small": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_334_hd_s.jpg",
      "hd_medium": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_334_hd_m.jpg",
      "hd": "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_334_hd.jpg"
    }
  ],
  "canada": {
    "mls": ""
  },
  "france": {
    "dpe_indice": "",
    "dpe_value": "",
    "dpe_id": "",
    "ges_indice": "",
    "ges_value": "",
    "alur_is_condo": null,
    "alur_units": null,
    "alur_uninhabitables": null,
    "alur_spending": null,
    "alur_legal_action": null
  },
  "belgium": {
    "peb_indice": "",
    "peb_value": "",
    "peb_id": "",
    "building_permit": null,
    "done_assignments": null,
    "preemption_property": null,
    "subdivision_permits": null,
    "sensitive_flood_area": null,
    "delimited_flood_zone": null,
    "risk_area": null
  },
  "videos": [],
  "other": {
    "orientation": {
      "north": null,
      "south": null,
      "east": null,
      "west": null
    },
    "inclusion": {
      "air_conditioning": null,
      "hot_water": null,
      "heated": null,
      "electricity": null,
      "furnished": null,
      "fridge": null,
      "cooker": null,
      "dishwasher": null,
      "dryer": null,
      "washer": null,
      "microwave": null
    },
    "detail": {
      "elevator": null,
      "laundry": null,
      "garbage_chute": null,
      "common_space": null,
      "janitor": null,
      "gym": null,
      "golf": 0,
      "tennis": 0,
      "sauna": null,
      "spa": null,
      "inside_pool": null,
      "outside_pool": null,
      "inside_parking": null,
      "outside_parking": null,
      "parking_on_street": null,
      "garagebox": null
    },
    "half_furnsihed": {
      "living_room": null,
      "bedrooms": null,
      "kitchen": null,
      "other": null
    },
    "indoor": {
      "attic": null,
      "attic_convertible": 0,
      "attic_converted": 0,
      "cellar": null,
      "central_vacuum": null,
      "entries_washer_dryer": null,
      "entries_dishwasher": null,
      "fireplace": null,
      "storage": null,
      "walk_in": null,
      "no_smoking": null,
      "double_glazing": null,
      "triple_glazing": null
    },
    "floor": {
      "carpet": null,
      "wood": null,
      "floating": null,
      "ceramic": null,
      "parquet": null,
      "cushion": null,
      "vinyle": null,
      "lino": null,
      "marble": null
    },
    "exterior": {
      "land_access": null,
      "back_balcony": null,
      "front_balcony": null,
      "private_patio": null,
      "storage": null,
      "terrace": null,
      "veranda": null,
      "garden": null,
      "sea_view": 0,
      "mountain_view": 0
    },
    "accessibility": {
      "elevator": null,
      "balcony": null,
      "grab_bar": null,
      "wider_corridors": null,
      "lowered_switches": null,
      "ramp": null
    },
    "senior": {
      "autonomy": null,
      "half_autonomy": null,
      "no_autonomy": null,
      "meal": null,
      "nursery": null,
      "domestic_help": null,
      "community": null,
      "activities": null,
      "validated_residence": null,
      "housing_cooperative": null
    },
    "pet": {
      "allow": null,
      "cat": null,
      "dog": null,
      "little_dog": null,
      "cage_aquarium": null,
      "not_allow": null
    },
    "service": {
      "internet": null,
      "tv": null,
      "tv_sat": null,
      "phone": null
    },
    "composition": {
      "bar": null,
      "living": null,
      "dining": null,
      "separe_toilet": null,
      "open_kitchen": null
    },
    "heating": {
      "electric": null,
      "solar": null,
      "gaz": null,
      "condensation": null,
      "fuel": null,
      "heat_pump": null,
      "floor_heating": null
    },
    "transport": {
      "bicycle_path": null,
      "public_transport": null,
      "public_bike": null,
      "subway": null,
      "train_station": null
    },
    "security": {
      "guardian": null,
      "alarm": null,
      "intercom": null,
      "camera": null,
      "smoke_dectector": null
    }
  }
}
```

Attributes | &nbsp;
---------- | -------
id <br> **string** <p style='color:red'>required</p> | the shareimmo's id of the property


