//============================================================
//  JQUERY
//============================================================
//= require jquery
//= require jquery_ujs
//= require jquery.minicolors
//= require jquery.minicolors.simple_form


//============================================================
//  OHTER LIB
//============================================================
//= require plugins/jquery.readySelector
//= require textcounter.min
//= require twitter-text
//= require bootstrap-sprockets
//= require underscore
//= require caman.full.min
//= require caman_plugin_insta



//============================================================
// Application
//============================================================
//= require application/00_for_all_pages

