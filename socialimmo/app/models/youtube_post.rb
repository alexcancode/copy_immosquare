class YoutubePost < ApplicationRecord
  belongs_to :user
  has_many :histories, as: :historable, dependent: :destroy
  serialize :pictures, JSON
  serialize :backlink, JSON
end
