class History < ApplicationRecord
  belongs_to :historable, polymorphic: true
  belongs_to :user

  def user
    self.historable.user
  end

  def icon
    if self.historable_type == "YoutubePost"
      return "youtube"
    elsif self.historable_type == "FacebookPost"
      return "facebook"
    elsif self.historable_type == "TwitterPost"
      return "twitter"
    end
  end


end
