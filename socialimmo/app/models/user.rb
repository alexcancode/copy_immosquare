class User < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :authentifications, dependent: :destroy
  has_many :youtube_posts, dependent: :destroy
  has_many :twitter_posts, dependent: :destroy
  has_many :facebook_posts, dependent: :destroy
  has_many :histories, dependent: :destroy

  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable,:omniauthable, :omniauth_providers => [:storeimmo,:gercopstore]




end
