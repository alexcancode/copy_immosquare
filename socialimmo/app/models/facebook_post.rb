class FacebookPost < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  has_many :histories, as: :historable, dependent: :destroy


  ##============================================================##
  ## Validation
  ##============================================================##
  validates :user_id, presence: true
  validate :user_id_or_pages_ids_can_be_set
  validates :link, presence: true

  serialize :pages_ids
  serialize :backlink, JSON


  def user_id_or_pages_ids_can_be_set
    if user_id.blank? && pages_ids.blank?
      errors.add(:user_id, "or one page must be selected")
    end
  end


end
