class TwitterPost < ApplicationRecord
  belongs_to :user
  has_many :histories, as: :historable, dependent: :destroy
  serialize :backlink, JSON
end
