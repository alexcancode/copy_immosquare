var system  = require('system');
var args    = system.args;


var url_to_screen = args[1];
var width         = args[2];
var height        = args[3];
var final_image   = args[4];


var page = require('webpage').create();


page.viewportSize = { width: width, height: height };
page.clipRect     = { top: 0, left: 0, width: width, height: height };


page.open(url_to_screen, function(){
  page.render(final_image);
  phantom.exit();
});
