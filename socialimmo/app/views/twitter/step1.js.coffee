$("#twitterModal .modal-dialog").html("<%= j (render 'twitter/modal_step1') %>")
$("#twitterModal").modal('show')

# update remaining characters and disable input if too many
$('#tweetLimit').html 140 - twttr.txt.getTweetLength($('#my_tweet').val())
$('#my_tweet').keyup ->
  remainingCharacters = 140 - twttr.txt.getTweetLength($(this).val())
  $('#tweetLimit').html remainingCharacters
  if remainingCharacters >= 0
    $('#tweeter_submit').prop 'disabled', false
  else
    $('#tweeter_submit').prop 'disabled', true
  return

$('li.image-ad').on 'click', ->
  # add or withdraw blue border on selected images
  if $(@).hasClass('bg-twitter')
    $(@).removeClass('bg-twitter')
  else if $('li.image-ad.bg-twitter').length < 4
    $(@).addClass('bg-twitter')

  # clean and get picture urls
  $("#twitter_post_picture_url1").val("")
  $("#twitter_post_picture_url2").val("")
  $("#twitter_post_picture_url3").val("")
  $("#twitter_post_picture_url4").val("")

  selected_images = []
  _.each $("li.image-ad.bg-twitter"), (a) ->
    selected_images.push($(a).data("url"))

  _.each selected_images, (url, index) ->
    $("#twitter_post_picture_url#{index+1}").val(url)

  # update selected images counter
  $('#twitterpictureLimit').html $('li.image-ad.bg-twitter').length


    

  


