<% if @twitter_post.errors.any? %>

$("#twitterStep1 div.alert").show().html("<%= j (render 'twitter/modal_errors') %>")

<% else %>

Pusher.logToConsole = false
pusher  = new Pusher("<%= ENV['pusher_key'] %>", encrypted: true)
channel = pusher.subscribe('<%= "twitter_channel_#{@twitter_post.id}" %>')

$('#twitterStep1').remove()
$("#twitterModal .modal-dialog").html("<%= j (render 'twitter/modal_step2') %>")

channel.bind 'twitter_event', (data) ->
  console.log data
  if data.status isnt true
    $("#twitterStatus").html(data.message)

<% end %>
