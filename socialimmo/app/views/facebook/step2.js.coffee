<% if @fb_post.errors.any? %>
$("#facebookStep1 div.alert").show().html("<%= j (render 'facebook/modal_errors') %>")

<% else %>

Pusher.logToConsole = false
pusher  = new Pusher("<%= ENV['pusher_key'] %>", encrypted: true)
channel = pusher.subscribe('<%= "facebook_channel_#{@fb_post.id}" %>')

$('#facebookStep1').remove()
$("#facebookModal .modal-dialog").html("<%= j (render 'facebook/modal_step2') %>")

channel.bind 'facebook_event', (data) ->
  console.log data
  if data.status isnt true
    $("#facebookStatus").html(data.message)

<% end %>
