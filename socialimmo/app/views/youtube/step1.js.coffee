$("#youtubeModal .modal-dialog").html("<%= j (render 'youtube/modal_step1') %>")
$("#youtubeModal").modal('show')

$('li.image-ad').on 'click', ->
  # red border on selected images
  if $(@).hasClass('bg-youtube')
    $(@).removeClass('bg-youtube')
  else if $('li.image-ad.bg-youtube').length < 10
    $(@).addClass('bg-youtube')

  # images selection
  $("#youtube_post_pictures").val("")

  selected_images = []
  _.each $("li.image-ad.bg-youtube"), (a) ->
    selected_images.push($(a).data("url"))

  $("#youtube_post_pictures").val(selected_images)
  $('#youtubepictureLimit').html $('li.image-ad.bg-youtube').length

# Prepare color input
$('#youtube_post_video_color').minicolors() theme: 'bootstrap'
    

  


