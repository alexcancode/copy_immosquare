<% if @youtube_post.errors.any? %>

$("#youtubeStep1 div.alert").show().html("<%= j (render 'youtube/modal_errors') %>")

<% else %>

Pusher.logToConsole = false
pusher  = new Pusher("<%= ENV['pusher_key'] %>", encrypted: true)
channel = pusher.subscribe('<%= "youtube_channel_#{@youtube_post.id}" %>')

$('#youtubeStep1').remove()
$("#youtubeModal .modal-dialog").html("<%= j (render 'youtube/modal_step2') %>")

channel.bind 'youtube_event', (data) ->
  console.log data
  if data.status isnt true
    $("#youtubeStatus").html(data.message)
  if data.value 
    $('.progress-bar').css 
      width: data.value + "%"
    $('.progress-bar').html "#{data.value}%" 

<% end %>
