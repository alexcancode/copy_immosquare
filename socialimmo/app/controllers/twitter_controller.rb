class TwitterController < ApplicationController


  include ClientAuthorized


  def step1
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{params["id"]}?access_token=#{current_user.oauth_token}")
    @response = JSON.parse(response.body) if response.code == 200
    @twitter = Authentification.where(:user_id => current_user.id,:provider=>"twitter").last
    @twitter_post  = TwitterPost.new
  end

  def step2
    @twitter_post = TwitterPost.new(twitter_params)
    if @twitter_post.save
      TwitterPublisherWorker.perform_async(@twitter_post.id)
    end
  end


  private

  def twitter_params
    params.require(:twitter_post).permit!
  end

end
