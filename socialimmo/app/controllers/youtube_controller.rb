class YoutubeController < ApplicationController

  include ClientAuthorized

  def step1
    @property_id = params["id"]
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{@property_id}?access_token=#{current_user.oauth_token}")
    @response = JSON.parse(response.body) if response.code == 200
    @google = Authentification.where(:user_id => current_user.id,:provider=>"google_oauth2").last
    @youtube_post  = YoutubePost.new
  end

  def step2
    @youtube_post = YoutubePost.new(youtube_params)
    if @youtube_post.save
      YoutubePublisherWorker.perform_async(@youtube_post.id)
    end
  end

  private

  def youtube_params
    params.require(:youtube_post).permit!
  end

end