class FacebookController < ApplicationController


  include ClientAuthorized


  def step1
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{params["id"]}?access_token=#{current_user.oauth_token}")
    @response = JSON.parse(response.body) if response.code == 200
    @facebook = Authentification.where(:user_id => current_user.id,:provider=>"facebook").last
    graph     = Koala::Facebook::API.new(@facebook.token)
    @pages    = graph.get_connections("me","accounts")
    @fb_post  = FacebookPost.new
  end

  def step2
    @fb_post = FacebookPost.create(facebook_params)
    unless params[:base64].empty?
      data =  params[:base64]
      image_data = Base64.decode64(data['data:image/png;base64,'.length .. -1])
      facebook_folder = "#{Rails.root}/tmp/facebook_post/#{@fb_post.id}"
      FileUtils.mkdir_p(facebook_folder) unless File.exists?(facebook_folder)
      File.open("#{facebook_folder}/#{File.basename(@fb_post.picture_url)}.png", 'wb') do |f|
        f.write image_data
      end
    end
    FacebookPublisherWorker.perform_async(@fb_post.id) if @fb_post.save
  end


  private

  def facebook_params
    params.require(:facebook_post).permit!
  end

end
