class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  before_action :set_locale
  before_action :set_filters
  before_action :caman



  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {locale: I18n.locale}
  end


  def set_locale
    I18n.locale  = params[:locale]
  end




  def after_sign_in_path_for(user)
    dashboard_path
  end

  def set_filters
    headers['Access-Control-Allow-Origin'] = '*'
    @filters = ["vintage","lomo","clarity","sinCity","sunrise","crossProcess","orangePeel","love","grungy","jarques","pinhole","oldBoot","glowingSun","multiply","softLight","hazyDays","herMajesty","nostalgia","hemingway","concentrate","1977"]
    @filters2 = [ "","aden","_1977","brooklyn","clarendon","earlybird","gingham","hudson","inkwell","lark","lofi","mayfair","moon","nashville","perpetua","reyes","rise","slumber","toaster","walden","willow","xpro2"]
  end

  def caman
    headers['Access-Control-Allow-Origin'] = '*'
  end


end
