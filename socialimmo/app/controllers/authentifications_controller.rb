class AuthentificationsController < ApplicationController

  before_action :authenticate_user!


  def create
    my_params = request.env["omniauth.params"]
    auth      = request.env['omniauth.auth']
    Authentification.where(
      :provider     =>  auth["provider"],
      :uid          =>  auth["uid"],
      :user_id      =>  my_params["user_id"]
      ).first_or_create.tap do |u|
      if u
        if auth["provider"] == 'facebook'
          u.update_attributes(
            :expires  => auth["credentials"]["expires"],
            :token    => auth["credentials"]["token"],
            :email    => auth["extra"]["raw_info"]["email"],
            :name     => auth["extra"]["raw_info"]["name"],
            :image    => auth["info"]["image"]
            )
        elsif auth["provider"] == 'twitter'
          u.update_attributes(
            :token          => auth["credentials"]["token"],
            :token_secret   => auth["credentials"]["secret"],
            :expires        => false,
            :nickname       => auth["info"]["nickname"],
            :name           => auth["info"]["name"],
            :image          => auth["info"]["image"],
            :location       => auth["info"]["location"],
            :link           => auth["info"]["urls"]['Twitter']
            )
        elsif auth["provider"] == 'instagram'
          u.update_attributes(
            :token      => auth["credentials"]["token"],
            :expires    => false,
            :nickname   => auth["info"]["nickname"],
            :name       => auth["info"]["name"],
            :image      => auth["info"]["image"],
            )
        elsif auth["provider"] == 'pinterest'
          u.update_attributes(
            :token      => auth["credentials"]["token"],
            :expires    => false,
            :nickname   => auth["info"]["nickname"],
            :name       => auth["info"]["name"],
            :image      => auth["info"]["image"],
            :first_name => auth["info"]["first_name"],
            :last_name  => auth["info"]["last_name"],
            :link       => auth["info"]["url"]
            )
        elsif auth["provider"] == 'google_oauth2'
          u.update_attributes(
            :name           => auth["info"]["name"],
            :email          => auth["info"]["email"],
            :first_name     => auth["info"]["first_name"],
            :last_name      => auth["info"]["last_name"],
            :token          => auth["credentials"]["token"],
            :refresh_token  => auth["credentials"]["refresh_token"],
            :expires_at     => auth["credentials"]["expires_at"],
            :expires        => auth["credentials"]["expires"],
            :image          => auth["info"]["image"],
            )
        end
      end
    end
    redirect_to dashboard_settings_path
  end


  def destroy
    begin
      authentification = Authentification.find(params[:id])
      raise t("app.not_authorized") if authentification.user_id != current_user.id
      authentification.destroy
      redirect_to dashboard_settings_path
    rescue Exception => e
      redirect_to dashboard_settings_path
    end
  end


  def failure
    flash[:notice] = "Auth Failed #{params[:message]}"
    redirect_to dashboard_settings_path
  end
end

