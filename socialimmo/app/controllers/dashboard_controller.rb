class DashboardController < ApplicationController


  include ClientAuthorized

  def index
    @is_authorized = @store_response.present? && @store_response["authorizations"]["plans"].length > 0 && @store_response["authorizations"]["plans"][0]["status"] == "active"
    if @is_authorized
      response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties?access_token=#{current_user.oauth_token}")
      @response       = JSON.parse(response.body).deep_symbolize_keys! if response.code == 200
      @facebook       = Authentification.where(:user_id => current_user.id,:provider=>"facebook").last
      @twitter        = Authentification.where(:user_id => current_user.id,:provider=>"twitter").last
      @google         = Authentification.where(:user_id => current_user.id,:provider=>"google_oauth2").last
      @have_1_social  = @facebook.present? || @twitter.present? || @google.present?
      @histories      = History.where(user_id: current_user.id).order(:created_at => "desc").limit(10)
    end
  end



  def settings
    @facebook = Authentification.where(:user_id => current_user.id,:provider=>"facebook").last
    @twitter  = Authentification.where(:user_id => current_user.id,:provider=>"twitter").last
    @google   = Authentification.where(:user_id => current_user.id,:provider=>"google_oauth2").last
  end


  def history
    @histories = History.where(user_id: current_user.id).order(:created_at => "desc")
  end



  private



end
