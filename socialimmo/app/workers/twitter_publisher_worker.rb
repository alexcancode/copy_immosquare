class TwitterPublisherWorker

  include Twitter::Validation
  include Twitter::Autolink
  include Sidekiq::Worker
  sidekiq_options :queue => :critical

  
  def perform(twitter_post_id)
    begin
      twitter_post  = TwitterPost.find(twitter_post_id)
      @channel        = "twitter_channel_#{twitter_post.id}"
      auth_twitter  = Authentification.where(:user_id =>twitter_post.user_id,:provider=>"twitter").last
      twitter_limit = 120
      tweet         = twitter_post.tweet

      pusher_and_log(I18n.t("twitter.worker.preparing"))
      ##============================================================##
      ## Si @twitter_limit > 120... on recoit une erreur de Twitter
      ## Status is over 140 characters...
      ## surement à cause des photos  du type pic.twitter.com/3VZjlZQecb
      ##============================================================##
      len = tweet_length(tweet)
      while len >= twitter_limit
        len = len-1
        tweet = tweet[0..len]
      end

      pic_array = []
      pic_array.push(twitter_post.picture_url1) if twitter_post.picture_url1.present?
      pic_array.push(twitter_post.picture_url2) if twitter_post.picture_url2.present?
      pic_array.push(twitter_post.picture_url3) if twitter_post.picture_url3.present?
      pic_array.push(twitter_post.picture_url4) if twitter_post.picture_url4.present?

      
      if auth_twitter.present?

        ##============================================================##
        ## Config Twitter Client
        ##============================================================##
        pusher_and_log(I18n.t("twitter.worker.connecting"))
        client = Twitter::REST::Client.new do |config|
          config.consumer_key        = ENV['twitter_api_key']
          config.consumer_secret     = ENV['twitter_api_secret']
          config.access_token        = auth_twitter.token
          config.access_token_secret = auth_twitter.token_secret
        end

        ##============================================================##
        ## Send picture to Tweeter servers
        ##============================================================##
        media_ids = Array.new
        amazon_pic = ""
        if pic_array.size > 0
          twitter_folder = "#{Rails.root}/tmp/twitter_post/#{twitter_post.id}"
          FileUtils.mkdir_p(twitter_folder) unless File.exists?(twitter_folder)
          pic_array.each_with_index do |picture_url, i|
            twitter_image = "#{twitter_folder}/#{File.basename(picture_url)}"
            open(twitter_image,'wb') { |file| file.write HTTParty.get(picture_url).parsed_response }

            pusher_and_log(I18n.t("twitter.worker.uploading_s3"))
            s3      = Aws::S3::Resource.new
            bucket  = s3.bucket(ENV['aws_bucket'])
            bucket  = s3.create_bucket(ENV['aws_bucket']) unless bucket.exists?
            obj     = bucket.object("#{Rails.env}/twitter_post/#{twitter_post.id}_#{i}.png")
            obj.upload_file(twitter_image, {acl: 'public-read'})
            amazon_pic = obj.public_url.to_s if i == 0

            t_image = client.upload(File.new(twitter_image))
            media_ids << t_image
          end
          FileUtils.rm_rf(twitter_folder) if File.directory?(twitter_folder)
        end

        ##============================================================##
        ## Send Tweet with pictures
        ##============================================================##
        pusher_and_log(I18n.t("twitter.worker.sending"))
        uid = client.update(tweet,:media_ids => media_ids.join(',')).id
      end

      twitter_post.update_attributes(backlinks: "#{auth_twitter.link}/status/#{uid}")

      History.create(historable_type: "TwitterPost", historable_id: twitter_post.id, backlink: "#{auth_twitter.link}/status/#{uid}", user_id: auth_twitter.user_id, name: auth_twitter.nickname, url_avatar: auth_twitter.image, uid: uid, caption: amazon_pic )
      
      pusher_and_log(I18n.t("app.done"))

    rescue Exception => e
      pusher_and_log(e.message)
    end
  end

  private

  def pusher_and_log(message)
    JulesLogger.info message
    Pusher.trigger(@channel,'twitter_event',{:status => false, :message => message})
  end

end
