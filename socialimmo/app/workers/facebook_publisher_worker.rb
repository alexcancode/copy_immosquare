# encoding: utf-8

class FacebookPublisherWorker

  include Sidekiq::Worker
  sidekiq_options :queue => :critical

  def perform(facebook_post_id)
    begin
      @fb_post        = FacebookPost.find(facebook_post_id)
      @channel        = "facebook_channel_#{@fb_post.id}"
      @auth_facebook  = Authentification.where(:user_id =>@fb_post.user_id,:provider=>"facebook").last

      ##============================================================##
      ## Préparation de l'espace de travail
      ##============================================================##
      pusher_and_log(I18n.t("facebook.worker.preparing"))
      
      facebook_folder = "#{Rails.root}/tmp/facebook_post/#{@fb_post.id}"
      FileUtils.mkdir_p(facebook_folder) unless File.exists?(facebook_folder)

      tmp_img = "#{facebook_folder}/#{File.basename(@fb_post.picture_url)}"
      open(tmp_img,'wb') { |file| file.write HTTParty.get(@fb_post.picture_url).parsed_response }

      pusher_and_log(I18n.t("facebook.worker.uploading_s3"))
      s3      = Aws::S3::Resource.new
      bucket  = s3.bucket(ENV['aws_bucket'])
      bucket  = s3.create_bucket(ENV['aws_bucket']) unless bucket.exists?
      obj     = bucket.object("#{Rails.env}/facebook_post/#{@fb_post.id}.png")
      obj.upload_file(tmp_img, {acl: 'public-read'})
      @fb_post.update_column(:picture_url, obj.public_url.to_s)

      FileUtils.rm_rf(facebook_folder) if File.directory?(facebook_folder) && obj.exists?

      ##============================================================##
      ## Post on Facebook on user's page
      ##============================================================##
      pusher_and_log(I18n.t("facebook.worker.sending"))

      user_graph = Koala::Facebook::API.new(@auth_facebook.token)
      if @auth_facebook.present? &&  @fb_post.user_uid.present?
        fb = user_graph.put_connections("me", "feed",
          :message      => @fb_post.message,
          :link         => @fb_post.link,
          :caption      => @fb_post.caption,
          :description  => @fb_post.description,
          :picture      => @fb_post.picture_url
          )

        raise I18n.t("facebook.worker.error") if fb["id"].blank?

        History.create(
          :historable_type  => "FacebookPost",
          :historable_id    => @fb_post.id,
          :name             => @auth_facebook.name,
          :url_avatar       => @auth_facebook.image,
          :user_id          => @fb_post.user_id,
          :uid              => fb["id"],
          :backlink         => "https://www.facebook.com/#{fb["id"].split("_")[0]}/posts/#{fb["id"].split("_")[1]}",
          :caption          => @fb_post.picture_url
          )
      end

      ##============================================================##
      ## Post on Facebook on every other page
      ##============================================================##
      if @auth_facebook.present? &&  @fb_post.pages_ids.present?
        @fb_post.pages_ids.each do |page_id|
          page_token = user_graph.get_page_access_token(page_id.to_i)
          page_infos = user_graph.get_object(page_id)
          page_graph = Koala::Facebook::API.new(page_token)
          fb = page_graph.put_connections(page_id, 'feed',
            :message      => @fb_post.message,
            :link         => @fb_post.link,
            :caption      => @fb_post.caption,
            :description  => @fb_post.description,
            :picture      => @fb_post.picture_url
            )

          raise I18n.t("facebook.worker.error") if fb["id"].blank?

          History.create(
            :historable_type  => "FacebookPost",
            :historable_id    => @fb_post.id,
            :name             => page_infos["name"],
            :url_avatar       => "http://graph.facebook.com/#{page_id}/picture",
            :user_id          => @fb_post.user_id,
            :uid              => fb["id"],
            :backlink         => "https://www.facebook.com/#{fb["id"].split("_")[0]}/posts/#{fb["id"].split("_")[1]}",
            :caption          => @fb_post.picture_url
            )
        end
      end

      pusher_and_log(I18n.t("app.done"))

    rescue Exception => e
      JulesLogger.info e.backtrace
      pusher_and_log(e.message)
    end
  end


  private

  def pusher_and_log(message)
    JulesLogger.info message
    Pusher.trigger(@channel,'facebook_event',{:status => false, :message => message})
  end

end