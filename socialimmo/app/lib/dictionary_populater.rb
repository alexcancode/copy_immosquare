class DictionaryPopulater


  def initialize(dictionary, user, property)
    @dictionary   = dictionary.deep_symbolize_keys!
    @user         = user
    @property     = property
  end


  ##============================================================##
  ## Send a Populated dictionary
  ##============================================================##
  def populate_dictionary

    # @dictionary[:document][:primary_color] = "#000000"  if @dictionary[:document].present?

    @dictionary[:blocks].each do |b|
      case b[:type]
      when :text
        b[:content] = decompose_definition(b[:name].clone)
      when :image
        b[:url] = self.send(b[:name].to_s)
      end
    end if @dictionary[:blocks].present?
    @dictionary
  end

  def decompose_definition(name)
    # indicates if we want a comma at the end
    with_slash    = name.slice!('_with_slash').present?

    # indicates if we want a comma at the end
    with_comma    = name.slice!('_with_comma').present?

    # indicates if we want a new line at the end
    with_newline  = name.slice!('_with_newline').present?

    # get all methods we want to call here (separated by "_and_")
    methods       = name.split('_and_')

    # for each method, we're gonna call it and collect the result
    methods.collect! do |meth|
      self.send(meth)
    end

    # Here we add a comma at the end if needed
    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth}, "
      end
    end if with_comma

     # Here we add a slash at the end if needed
     methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth} / "
      end
    end if with_slash

    # Here we add a newline at the end if needed
    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth}\n"
      end
    end if with_newline

    # Finally, we concatenate everything
    final = methods.join((with_comma || with_newline)  ? "" : " ")
    return final
  end



  ##============================================================##
  ## When Missing property is called
  ##============================================================##
  def method_missing(sym, *args, &block)
    JulesLogger.info "Method #{sym} called on the DictionaryPopulater object but nil was returned"
    return nil
  end




##============================================================##
  ## DICTONNARY BELOW
  ##============================================================##

  ##============================================================##
  ## B
  ##============================================================##
  def broker_agency_name
    @user[:company_name]
  end

  def broker_photo
    @user[:picture_url]
  end

  def broker_logo
    @user[:company_logo_url]
  end

  def broker_email
    @user[:email]
  end

  def broker_job
    @user[:presentation_text][I18n.locale]
  end

  def broker_name
    "#{@user[:first_name]} #{@user[:last_name]}"
  end

  def broker_phone
    @user[:phone]
  end

  def broker_phone_plus_label
    "#{I18n.t('dico.phone')} : #{@user[:phone]}" if @user[:phone].present?
  end

  def broker_phonemobile_plus_label
    "#{I18n.t('dico.mobile')} : #{@user[:phone_sms]}" if @user[:phone_sms].present?
  end

  def broker_website
    @user[:website]
  end


##============================================================##
  ## P
  ##============================================================##
  def property_address1
    "#{@property[:address][:street_number]} #{@property[:address][:street_name]}"
  end

  def property_address_2_lines
    line_1 = "#{@property[:address][:street_number]} #{@property[:address][:street_name]}"
    line_2 = "#{@property[:address][:zipcode]} #{@property[:address][:locality]}"
    "#{line_1.present? ? "#{line_1}\n" : nil }#{line_2}"
  end

  def property_title
    @property[:title][I18n.locale]
  end

  def property_map
    "https://api.mapbox.com/v4/mapbox.streets/pin-m-marker+e3a21d(#{@property[:address][:longitude]},#{@property[:address][:latitude]})/#{@property[:address][:longitude]},#{@property[:address][:latitude]},14/500x500@2x.png?access_token=#{ENV['MAPBOX_API_TOKEN']}"
  end

  def property_cover
    @property[:cover_url]
  end

  def property_address
    "#{@property[:address][:street_number]} #{@property[:address][:street_name]} #{@property[:address][:zipcode]} #{@property[:address][:locality]}"
  end

  def property_price
    @property[:financial][:price_formated]
  end

  def property_classification_listing
    "#{@property[:classification][I18n.locale]} #{@property[:listing][I18n.locale]}"
  end

  def property_classification
    @property[:classification][I18n.locale]
  end

  def property_listing
    @property[:listing][I18n.locale]
  end

  def property_status
    @property[:status][I18n.locale]
  end

  def property_available_date
    "#{I18n.t('app.availability')} : #{@property[:availability_date].present? ? I18n.localize(DateTime.parse(@property[:availability_date]), :format=>("%d %B %Y")) : I18n.t('app.immediately')}"
  end

  def property_link
    @property[:url].present? && @property[:url][I18n.locale].present? ? @property[:url][I18n.locale] : "https://www.shareimmo.com/ad/#{@property[:id]}"
  end

  def property_description
    convert_to_text(@property[:description][I18n.locale])
  end

  def property_description_short
    convert_to_text(@property[:description_short][I18n.locale])
  end

  def property_cover_url
    @property[:cover_url]
  end

  def property_locality
    @property[:address][:locality]
  end

  def property_sublocality
    @property[:address][:sublocality]
  end

    def property_reference
    @property[:uid].present? ? "##{@property[:uid]}" : "##{@property[:id]}"
  end

end