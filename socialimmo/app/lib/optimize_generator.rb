# encoding: utf-8

class OptimizeGenerator

  require "mp3info"


  def initialize(args)
    @order_id           = args[:order_id]
    @channel            = args[:channel]
    @infos              = args[:infos].deep_symbolize_keys!
    @visibility_start   = args[:visibility_start] || Time.now
    @visibility_until   = args[:visibility_until] || Time.now+1.month
    @fps                = 25
    @size               = "1920x1080"
    @scale              = "1920:1080"
    @vcodec             = "libx264"
    @pix_format         = "yuv420p"
    @start              = Time.now
  end




  def perform
    JulesLogger.info "Sidekiq Action Start for ad # #{@order_id}"
    begin
      intro_url         = "https://s3.amazonaws.com/socialimmo/shared/youtube/v1/openning/video_#{rand(1..8)}.mp4"
      audio_url         = "https://s3.amazonaws.com/socialimmo/shared/youtube/v1/soundtrack/soundtrack_1.mp3"
      # intro_overlay_url = "https://s3.amazonaws.com/socialimmo/shared/youtube/v1/template_pdf/1/intro.pdf"
      intro_overlay_url = "http://apps.shareimmo.com/video_test/Template1_video.pdf"
      # video_overlay_url = "https://s3.amazonaws.com/socialimmo/shared/youtube/v1/template_pdf/1/video.pdf"
      video_overlay_url = "http://apps.shareimmo.com/video_test/Template2_video.pdf"
      outro_pdf_url     = "https://s3.amazonaws.com/socialimmo/shared/youtube/v1/template_pdf/1/outro.pdf"
      images            = Array.new
      duration          = 5
      cmd_1             = ""
      cmd_2             = ""
      video_folder      = "#{Rails.root}/tmp/video_generator/#{@order_id}"
      concat_list       = "#{video_folder}/mylist.txt"



      ##============================================================##
      ## Préparation de l'espace de travail
      ##============================================================##
      pusher_and_log("Préparation de l'espace de travail",@channel)
      FileUtils.rm_rf(video_folder) if File.directory?(video_folder)
      FileUtils.mkdir_p(video_folder)



      #============================================================##
      # Download Intro
      # Extraire la bande video de la séquence d'ouverture + sélectionner la zone de départ et de fin + convertir à la bonne dimension, codecs
      # ffmpeg -i opening.mp4 -ss 02 -t 10 -an -s hd720 -vcodec libx264 -r 30 -pix_fmt yuv420p  step1.mp4
      # -i         : input de départ
      # -ss        : Pour déterminer le point de départ. Ici on commence à deux seconde du début
      # -t         : Durée à partir de ce point de début. Ici 10 secondes. donc on garde de 2 à 12 secondes de la video initial
      # -an        : pour dire que l'on veut garder que la bande vidéo, on retire le son
      # -r         : frame pour seconde pour le fichier de rend
      # -vf scale  : Resize de la viédo ( scale vs size ?)
      # -s         : Taille de la vidéo de sortie ( scale vs size ?)
      # -vcodec    : codec d'encodage de la vidéo
      # -pix_fmt   : taille du pixel.. pas trop compris l'utilité mais cela semble obligatoire
      #============================================================##
      pusher_and_log("Download + encode intro",@channel)
      intro1_path   = "#{video_folder}/intro_original#{File.extname(intro_url)}"
      intro2_path   = "#{video_folder}/intro_step1.mp4"
      intro3_path   = "#{video_folder}/intro_final.mp4"
      open(intro1_path,'wb'){|file| file.write HTTParty.get(intro_url).parsed_response }
      system("ffmpeg -i #{intro1_path} -ss 00 -t 10 -an -r #{@fps} -vf scale=#{@scale} -s #{@size} -vcodec #{@vcodec} -preset ultrafast -pix_fmt #{@pix_format}  #{intro2_path}")


      ##============================================================##
      ## Add Overlay on Intro
      ## Download
      ## Pdf to png
      ##============================================================##
      pusher_and_log("Download Overlay",@channel)
      intro_overlay_pdf_path        = "#{video_folder}/#{File.basename(intro_overlay_url)}"
      intro_overlay_pdf_path_final  = "#{video_folder}/intro_overlay.pdf"
      intro_overlay_png_path        = "#{video_folder}/intro_overlay.png"
      intro_overlay_png_path_resize = "#{video_folder}/intro_overlay_resize.png"
      open(intro_overlay_pdf_path,'wb'){|file| file.write HTTParty.get(intro_overlay_url).parsed_response }


      dictionary  = Mypdflib::PdfParser.new(:pdf=>intro_overlay_pdf_path).parse
      dictionary  = DictionaryPopulater.new(dictionary,@infos).populate_dictionary
      pdf         = Mypdflib::PdfWritter.new(:original => intro_overlay_pdf_path,:result => intro_overlay_pdf_path_final, :dictionary =>dictionary).produce
      raise pdf if pdf != true

      pdf_to_png  = "gs -dUseCropBox  -sDEVICE=pngalpha -r300 -o #{intro_overlay_png_path}  #{intro_overlay_pdf_path_final}"
      system(pdf_to_png)
      system("ffmpeg -i #{intro_overlay_png_path} -preset ultrafast -vf scale=#{@scale} #{intro_overlay_png_path_resize}")

      pusher_and_log("Match Intro + overlay",@channel)
      system("ffmpeg -i #{intro2_path} -i #{intro_overlay_png_path_resize} -preset ultrafast -filter_complex \"overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2 \" -codec:a copy #{intro3_path}")
      File.open(concat_list, 'a') { |file| file.puts("file '#{intro3_path}'")}




      ##============================================================##
      ## Téléchargement des images localement
      ##============================================================##
      pusher_and_log("Téléchargement des images",@channel)
      @infos[:pictures].each_with_index do |pic,i|
        if i <= 5
          image = "#{video_folder}/img_#{i+1}#{File.extname(pic[:url])}"
          images << image
          open(image,'wb'){|file| file.write HTTParty.get(pic[:url]).parsed_response }
        end
      end if @infos[:pictures].present?


      ##============================================================##
      ## Création de la vidéo
      ## See : https://ffmpeg.org/ffmpeg-filters.html
      ## -y : Overwrite output files without asking.
      ## -i : input
      ## -s : Set the output image size
      ## trim=duration=x Keep only the x seconds
      ## scale = 8960x5040 pour éviter les tremblements du zoom
      ## mais vu que nous avons défini s,le format final est 1920*1080
      ##============================================================##
      step2_0_path  = "#{video_folder}/zoom_original.mp4"
      step2_1_path  = "#{video_folder}/zoom_final.mp4"
      pusher_and_log("Création de la vidéo",@channel)
      cmd_outpout = "ffmpeg -y -framerate #{@fps}"
      images.each_with_index do |img,i|
        cmd_outpout += " -loop 1 -i #{img}"
        factor_x     = rand()
        factor_y     = rand()
        zoom         = "zoompan=z='min(zoom+0.0015,1.5)':s=#{@size}:d=#{@fps*duration}:x='iw*#{factor_x}-(iw/zoom*#{factor_x})':y='ih*#{factor_y}-(ih/zoom*#{factor_y})'"
        trim         = "trim=duration=#{duration}"
        fade_in      = "fade=t=in:st=0:d=1"
        fade_out     = "fade=t=out:st=#{duration-1}:d=1"
        cmd_1       += "[#{i}:v]scale=9600x5400,#{zoom},#{trim},#{fade_in},#{fade_out}[v#{i}];"
        cmd_2       += "[v#{i}]"
      end

      cmd_outpout += " -filter_complex \" #{cmd_1} #{cmd_2} concat=n=#{images.size}:v=1:a=0 , format=#{@pix_format}[v] \" "
      cmd_outpout += " -map '[v]' -preset ultrafast #{step2_0_path}"
      system(cmd_outpout)


      ##============================================================##
      ## Add Overlay on Zoom
      ## Download
      ## Pdf to png
      ##============================================================##
      # pusher_and_log("Download Overlay",@channel)
      # video_overlay_pdf_path  = "#{video_folder}/#{File.basename(video_overlay_url)}"
      # video_overlay_png_path  = "#{video_folder}/video_overlay.png"
      # open(video_overlay_pdf_path,'wb'){|file| file.write HTTParty.get(video_overlay_url).parsed_response }

      # pdf_to_png  = "gs -dUseCropBox  -sDEVICE=pngalpha -r300 -o #{video_overlay_png_path}  #{video_overlay_pdf_path}"
      # system(pdf_to_png)
      
      # pusher_and_log("Match video + overlay",@channel)
      # system("ffmpeg -i #{step2_0_path} -i #{video_overlay_png_path} -preset ultrafast -filter_complex \"overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2 \" -codec:a copy #{step2_1_path}")
      # File.open(concat_list, 'a') { |file| file.puts("file '#{step2_1_path}'")}






      pusher_and_log("Download Overlay",@channel)
      video_overlay_pdf_path        = "#{video_folder}/#{File.basename(video_overlay_url)}"
      video_overlay_pdf_path_final  = "#{video_folder}/video_overlay.pdf"
      video_overlay_png_path        = "#{video_folder}/video_overlay.png"
      video_overlay_png_path_resize = "#{video_folder}/video_overlay_resize.png"
      open(video_overlay_pdf_path,'wb'){|file| file.write HTTParty.get(video_overlay_url).parsed_response }


      dictionary  = Mypdflib::PdfParser.new(:pdf=>video_overlay_pdf_path).parse
      dictionary  = DictionaryPopulater.new(dictionary,@infos).populate_dictionary
      pdf         = Mypdflib::PdfWritter.new(:original => video_overlay_pdf_path,:result => video_overlay_pdf_path_final, :dictionary =>dictionary).produce
      raise pdf if pdf != true

      pdf_to_png  = "gs -dUseCropBox  -sDEVICE=pngalpha -r300 -o #{video_overlay_png_path}  #{video_overlay_pdf_path_final}"
      system(pdf_to_png)
      system("ffmpeg -i #{video_overlay_png_path} -preset ultrafast -vf scale=#{@scale} #{video_overlay_png_path_resize}")

      pusher_and_log("Match Intro + overlay",@channel)
      system("ffmpeg -i #{step2_0_path} -i #{video_overlay_png_path_resize} -preset ultrafast -filter_complex \"overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2 \" -codec:a copy #{step2_1_path}")
      File.open(concat_list, 'a') { |file| file.puts("file '#{step2_1_path}'")}






      ##============================================================##
      ##  Outro
      ##  ToDo Alex
      ##============================================================##
      pusher_and_log("Download Outro",@channel)
      outro_pdf_path  = "#{video_folder}/#{File.basename(outro_pdf_url)}"
      outro_png_path  = "#{video_folder}/outro.png"
      open(outro_pdf_path,'wb'){|file| file.write HTTParty.get(outro_pdf_url).parsed_response }

      pdf_to_png  = "gs -dUseCropBox  -sDEVICE=pngalpha -r300 -o #{outro_png_path}  #{outro_pdf_path}"
      system(pdf_to_png)

      #Create outro_video from outro.png
      outro_video_path = "#{video_folder}/final_frame.mp4"
      system "ffmpeg -framerate 1/5  -i #{File.join(video_folder,'outro.png')} -s #{@size} -preset ultrafast -vcodec #{@vcodec} -r #{@fps} -pix_fmt #{@pix_format} #{File.join(video_folder,'final_frame.mp4')}"
      File.open(concat_list, 'a') { |file| file.puts("file '#{outro_video_path}'")}


      ##============================================================##
      ## Création du fichie audio
      ##  1/ Download
      ##  2/ Convert to mp3
      ##  3/ Loop mp3 for ajust duration
      ##  4/ Normalize db
      ##============================================================##
      pusher_and_log("Audio Management",@channel)
      audio_1 = "#{video_folder}/#{File.basename(audio_url)}"
      audio_2 = "#{video_folder}/audio_convert.mp3"
      audio_3 = "#{video_folder}/audio_looped.mp3"
      open(audio_1,'wb'){|file| file.write HTTParty.get(audio_url).parsed_response }
      system("sox #{audio_1} #{audio_2}")
      mp3info = Mp3Info.open(audio_2)
      repeat = (((images.size+1)*duration)/mp3info.length).ceil
      system "sox #{audio_2} #{audio_3} repeat #{repeat}"
      system "mp3gain -r #{audio_3}"


      ##============================================================##
      ## Concat
      ##============================================================##
      pusher_and_log("Concat",@channel)
      final0 = "#{video_folder}/final_0.mp4"
      final1 = "#{video_folder}/final_1.mp4"
      system "ffmpeg -f concat -safe 0 -i #{concat_list} -c copy #{final0}"


      ##============================================================##
      ## Soundtrack
      ##============================================================##
      pusher_and_log("Soundtrack",@channel)
      system "ffmpeg -i #{final0} -i #{audio_3} -preset ultrafast -acodec aac -strict experimental  -shortest #{final1}"


      pusher_and_log("Done",@channel)
    rescue Exception => e
      pusher_and_log("Error : #{e.message}",@channel)
      JulesLogger.info e.backtrace
    end
  end




  private

  def pusher_and_log(message,channel)
    JulesLogger.info "#{message} (#{(Time.now-@start).ceil} s)"
  end









end
