class RemoveInfosToFacebookPosts < ActiveRecord::Migration[5.0]
  def change
    remove_column :facebook_posts, :post_id, :string
    remove_column :facebook_posts, :backlinks, :text
  end
end
