class AddBacklinkToTwitterPost < ActiveRecord::Migration[5.0]
  def change
    add_column :twitter_posts, :backlink, :string
  end
end
