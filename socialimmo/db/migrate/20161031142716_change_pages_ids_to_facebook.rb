class ChangePagesIdsToFacebook < ActiveRecord::Migration[5.0]
  def change
    change_column :facebook_posts, :pages_ids, :text
    rename_column :facebook_posts, :backlink, :backlinks
    change_column :facebook_posts, :backlinks, :text, :after => :post_id
  end
end
