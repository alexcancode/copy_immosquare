class ChangeTweetFormat < ActiveRecord::Migration[5.0]
  def up
    change_column :twitter_posts, :tweet, :text
  end

  def down
    change_column :twitter_posts, :tweet, :string
  end
end
