class CreateFacebookPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :facebook_posts do |t|
      t.references :user, foreign_key: false
      t.string :user_uid
      t.string :pages_ids
      t.string :post_id
      t.string :message
      t.string :link
      t.string :caption
      t.text :description
      t.string :picture_url
      t.string :filter
      t.string :flag

      t.timestamps
    end
  end
end
