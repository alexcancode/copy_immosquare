class AddInfoToHistory < ActiveRecord::Migration[5.0]
  def change
    add_column :histories, :url_avatar, :string
    add_column :histories, :backlink, :string
    add_column :histories, :uid, :string
    add_reference :histories, :user, foreign_key: true
  end
end
