class UpdateBacklinkOfYoutubePost < ActiveRecord::Migration[5.0]
  def change
    change_column :youtube_posts, :backlink, :text
    change_column :twitter_posts, :backlink, :text
    change_column :facebook_posts, :backlink, :text
  end
end
