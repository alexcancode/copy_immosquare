class CreateAuthentifications < ActiveRecord::Migration[5.0]
  def change
    create_table :authentifications do |t|
      t.references :user, foreign_key: false
      t.string :provider
      t.string :uid
      t.string :token
      t.string :refresh_token
      t.string :token_secret
      t.integer :expires_at
      t.boolean :expires
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :nickname
      t.string :image
      t.string :location
      t.string :link
      t.string :name
      t.timestamps
    end
  end
end
