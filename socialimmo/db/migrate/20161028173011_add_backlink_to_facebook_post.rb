class AddBacklinkToFacebookPost < ActiveRecord::Migration[5.0]
  def change
    add_column :facebook_posts, :backlink, :string
  end
end
