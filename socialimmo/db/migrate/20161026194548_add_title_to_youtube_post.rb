class AddTitleToYoutubePost < ActiveRecord::Migration[5.0]
  def change
    add_column :youtube_posts, :video_title, :string
    add_column :youtube_posts, :video_description, :text
  end
end
