class AddVisibilityToYoutubePost < ActiveRecord::Migration[5.0]
  def change
    add_column :youtube_posts, :visibility_start, :date
    add_column :youtube_posts, :visibility_until, :date
  end
end
