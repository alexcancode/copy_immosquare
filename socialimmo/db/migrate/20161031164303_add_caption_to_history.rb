class AddCaptionToHistory < ActiveRecord::Migration[5.0]
  def change
    add_column :histories, :caption, :string
  end
end
