class ChangePagesIdsToTwitter < ActiveRecord::Migration[5.0]
  def change
    rename_column :twitter_posts, :backlink, :backlinks
    rename_column :youtube_posts, :backlink, :backlinks
  end
end
