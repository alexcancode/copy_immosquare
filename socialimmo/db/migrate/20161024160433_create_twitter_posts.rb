class CreateTwitterPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :twitter_posts do |t|
      t.references :user, foreign_key: false
      t.string :user_uid
      t.string :tweet_id
      t.string :tweet
      t.string :picture_url1
      t.string :picture_url2
      t.string :picture_url3
      t.string :picture_url4
      t.string :filter
      t.string :flag

      t.timestamps
    end
  end
end
