class AddPropertyIdToYoutubePosts < ActiveRecord::Migration[5.0]
  def change
    add_column :youtube_posts, :property_id, :integer, :after => :youtube_id
  end
end
