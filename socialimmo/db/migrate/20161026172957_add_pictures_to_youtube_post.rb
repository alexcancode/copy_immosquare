class AddPicturesToYoutubePost < ActiveRecord::Migration[5.0]
  def change
    add_column :youtube_posts, :pictures, :text
  end
end
