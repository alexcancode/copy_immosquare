class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: false
      t.string :first_name
      t.string :last_name
      t.string :color1

      t.timestamps
    end
  end
end
