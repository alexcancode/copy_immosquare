class CreateHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :histories do |t|
      t.string :name
      t.references :historable, polymorphic: true

      t.timestamps
    end
    add_index :histories, :name
  end
end
