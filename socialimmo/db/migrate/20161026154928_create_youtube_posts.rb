class CreateYoutubePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :youtube_posts do |t|
      t.references :user, foreign_key: false
      t.string :user_uid
      t.string :youtube_id
      t.string :video_color
      t.string :link

      t.timestamps
    end
  end
end