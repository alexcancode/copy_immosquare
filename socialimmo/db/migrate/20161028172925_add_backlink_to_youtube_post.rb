class AddBacklinkToYoutubePost < ActiveRecord::Migration[5.0]
  def change
    add_column :youtube_posts, :backlink, :string
  end
end
