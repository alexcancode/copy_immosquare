# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161230215040) do

  create_table "authentifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "token"
    t.string   "refresh_token"
    t.string   "token_secret"
    t.integer  "expires_at"
    t.boolean  "expires"
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "nickname"
    t.string   "image"
    t.string   "location"
    t.string   "link"
    t.string   "name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["user_id"], name: "index_authentifications_on_user_id", using: :btree
  end

  create_table "facebook_posts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "user_uid"
    t.text     "pages_ids",   limit: 65535
    t.string   "message"
    t.string   "link"
    t.string   "caption"
    t.text     "description", limit: 65535
    t.string   "picture_url"
    t.string   "filter"
    t.string   "flag"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["user_id"], name: "index_facebook_posts_on_user_id", using: :btree
  end

  create_table "histories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "historable_type"
    t.integer  "historable_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "url_avatar"
    t.string   "backlink"
    t.string   "uid"
    t.integer  "user_id"
    t.string   "caption"
    t.index ["historable_type", "historable_id"], name: "index_histories_on_historable_type_and_historable_id", using: :btree
    t.index ["name"], name: "index_histories_on_name", using: :btree
    t.index ["user_id"], name: "index_histories_on_user_id", using: :btree
  end

  create_table "twitter_posts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "user_uid"
    t.string   "tweet_id"
    t.text     "tweet",        limit: 65535
    t.string   "picture_url1"
    t.string   "picture_url2"
    t.string   "picture_url3"
    t.string   "picture_url4"
    t.string   "filter"
    t.string   "flag"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.text     "backlinks",    limit: 65535
    t.index ["user_id"], name: "index_twitter_posts_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uid"
    t.string   "provider"
    t.string   "provider_host"
    t.string   "oauth_token"
    t.string   "oauth_refresh_token"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "youtube_posts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "user_uid"
    t.string   "youtube_id"
    t.integer  "property_id"
    t.string   "video_color"
    t.string   "link"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.text     "pictures",          limit: 65535
    t.date     "visibility_start"
    t.date     "visibility_until"
    t.string   "video_title"
    t.text     "video_description", limit: 65535
    t.text     "backlinks",         limit: 65535
    t.index ["user_id"], name: "index_youtube_posts_on_user_id", using: :btree
  end

  add_foreign_key "histories", "users"
end
