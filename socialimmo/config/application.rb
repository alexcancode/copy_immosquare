require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Diffuseimmo
  class Application < Rails::Application

    config.autoload_paths << Rails.root.join('lib')

    ##============================================================##
    ## The default locale is :en and all translations
    ## from config/locales/*.rb,yml are auto loaded.
    ##============================================================##
    config.i18n.default_locale = 'fr'
    config.i18n.available_locales = ['fr','en', 'es']
    config.i18n.fallbacks = {:fr => :en, :es => :en, :en => :fr}


    ##============================================================##
    ## Config action_mailer
    ##============================================================##
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :address        => "smtp-relay.sendinblue.com",
      :port           => 587,
      :authentication => :plain,
      :user_name      => ENV['sendinblue_username'],
      :password       => ENV['sendingblue_key']
    }


    ##============================================================##
    ## Config paperclip S3_storage
    ##============================================================##
    config.paperclip_defaults = {
      :storage => :s3,
      :s3_protocol => :https,
      :s3_credentials => {
        :bucket             => ENV['aws_bucket'],
        :access_key_id      => ENV['aws_acces_key_id'],
        :secret_access_key  => ENV['aws_secret_acces_key']
      },
      :s3_region => ENV["aws_region"]
    }



  end
end




