set :stage, :production
set :rvm_ruby_version, "2.3.3@socialimmo"
set :deploy_to, "/srv/apps/socialimmo"
server 'web03.immosquare.com', {:user=>'root', :roles => %w{web app db}}
