set :application, "socialimmo"
set :rvm_ruby_version, "2.3.2@socialimmo"
set :repo_url, "git@bitbucket.org:wgroupe/socialimmo.git"
set :bundle_roles, :all
set :deploy_to, "/srv/apps/socialimmo"
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

