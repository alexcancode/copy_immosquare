Rails.application.config.middleware.use OmniAuth::Builder do

  ##============================================================##
  ## Facebook
  ##============================================================##
  provider :facebook, ENV['facebook_app_id'], ENV['facebook_secret_id'],
  {
    :scope            => 'email,publish_actions,manage_pages,publish_pages',
    :display          => 'page',
    :image_size       => 'square',
    :secure_image_url =>  true
  }


  ##============================================================##
  ## Twitter
  ##============================================================##
  provider :twitter, ENV['twitter_api_key'], ENV['twitter_api_secret'],
  {
    :secure_image_url => true,
    :image_size       => 'bigger',
  }


  ##============================================================##
  ## Google/Youtube
  ##============================================================##
  provider :google_oauth2, ENV['google_client_id'], ENV['google_client_secret'],
  {
    :scope                  => "email,profile,youtube",
    :access_type            => "offline",
    :image_aspect_ratio     => "square",
    :image_size             => 50,
    :prompt                 => "consent",
    :include_granted_scopes => true
  }

  ##============================================================##
  ## Instagram
  ##============================================================##
  provider :instagram, ENV['instagram_client_id'], ENV['instagram_client_secret']


  ##============================================================##
  ## Pinterest
  ##============================================================##
  provider :pinterest, ENV['pinterest_client_id'], ENV['pinterest_client_secret']

end


OmniAuth.config.on_failure do |env|
  error_type = env['omniauth.error.type']
  new_path = "#{env['SCRIPT_NAME']}#{OmniAuth.config.path_prefix}/failure?message=#{error_type}"
  [301, {'Location' => new_path, 'Content-Type' => 'text/html'}, []]
end
