Mypdflib.setup do |config|
  config.license        = Rails.env.production? ? ENV['pdflib_license'] : nil
  config.stuff_folder   = "#{Rails.root}/pdfs"
  config.ppi            = 250
end
