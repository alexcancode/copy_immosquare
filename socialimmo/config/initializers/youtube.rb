Yt.configure do |config|
  config.client_id = ENV['google_client_id']
  config.client_secret = ENV['google_client_secret']
  config.api_key = ENV['google_api_key']
  config.log_level = :debug
end
