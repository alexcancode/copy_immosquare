require 'sidekiq/web'

Rails.application.routes.draw do

  ##============================================================##
  ## Devise does not support scoping OmniAuth callbacks under a dynamic segment
  ##============================================================##
  devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}


  ##============================================================##
  ## Sidekiq
  ##============================================================##
  mount Sidekiq::Web => '/sidekiq'



  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do

    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users, skip: :omniauth_callbacks

    ##============================================================##
    ## Pages
    ##============================================================##
    match "/policy" => "pages#policy" , :via => :get, :as => :policy

    ##============================================================##
    ## OAuth
    ##============================================================##
    match 'users/auth/:provider/callback'   => 'authentifications#create',   :via => [:get]
    match 'users/auth/failure'              => 'authentifications#failure',  :via => [:get]
    match 'users/auth/:id'                  => 'authentifications#destroy',  :via => [:delete] ,:as => 'dashboard_delete_app'

    ##============================================================##
    ## Dashboard
    ##============================================================##
    root to: "pages#home"
    match "dashboard"       => "dashboard#index",           :via => [:get], :as => :dashboard
    match 'settings'        => "dashboard#settings",        :via => :get,   :as => :dashboard_settings
    match 'history'         => "dashboard#history",         :via => :get,   :as => :dashboard_history




    ##============================================================##
    ## Facebook
    ##============================================================##
    match "facebook/:id"    => "facebook#step1",    :via => [:get],     :as => :facebook_step1
    match "facebook"        => "facebook#step2",    :via => [:post],    :as => :facebook_step2

    ##============================================================##
    ## Twitter
    ##============================================================##
    match "twitter/:id"    => "twitter#step1",    :via => [:get],     :as => :twitter_step1
    match "twitter"        => "twitter#step2",    :via => [:post],    :as => :twitter_step2

    ##============================================================##
    ## Youtube
    ##============================================================##
    match 'youtube/:id'    => 'youtube#step1',       via: [:get],        :as => 'youtube_step1'
    match 'youtube'        => 'youtube#step2',       via: [:post],        :as => 'youtube_step2'

    match "insta_filter" => "pages#insta_filter", :via => [:get],  :as => :insta_filter
    match "caman"         => "pages#caman", :via => [:get],  :as => :caman

  end

end
