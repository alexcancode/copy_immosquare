## Pour Ubuntu
  sudo apt-get install libav-tools
  sudo apt-get install sox
  sudo apt-get install mp3gain
  sudo apt-get install libsox-fmt-mp3
  sudo apt-get ffmpeg

## for Mac os
  brew install imagemagick
  brew install gs
  brew install sox --with-lame
  brew install ffmpeg


# Test génération de vidéos
## Étape #1 :Extraire la bande video de la séquence d'ouverture + sélectionner la zone de départ et de fin + convertir à la bonne dimension, codecs
ffmpeg -ss 02 -t 10 -i opening.mp4  -an -s hd720 -vcodec libx264 -r 30 -pix_fmt yuv420p  step1.mp4

-ss      : Pour déterminer le point de départ. Ici on commence à deux seconde du début
-t       : Durée à partir de ce point de début. Ici 10 secondes. donc on garde de 2 à 12 secondes de la video initial
-i       : input de départ
-an      : pour dire que l'on veut garder que la bande vidéo, on retire le son
-r       : frame pour seconde pour le fichier de rendu
-vcodec  : codec d'encodage de la vidéo
-pix_fmt : taille du pixel.. pas trop compris l'utilité mais cela semble obligatoire



## Étape 2 et 3  Créer une video à partir d'image
ffmpeg -framerate 1/5  -i img%03d.jpg   -s hd720 -vcodec libx264 -r 30 -pix_fmt yuv420p   step2.mp4
ffmpeg -framerate 1/5  -i final%01d.png -s hd720 -vcodec libx264 -r 30 -pix_fmt yuv420p   step3.mp4

-framerate  : durée de chaque séquence 1/5 pour 5 secondes
-i          : img%03d.jpg les images sources. Le 3 correspond au nombre de chiffre qui compose l'index des fichiers donc ici il faut que les index soit img001. img002....
-s          : dimension du fichier final. (taille en pixel, ou format classique)
-r          : frame pour seconde pour le fichier de rendu
-vcodec     : codec d'encodage de la vidéo
-pix_fmt    : taille du pixel.. pas trop compris l'utilité mais cela semble obligatoire



## Concatenate nos 3 videos MP4(https://trac.ffmpeg.org/wiki/Concatenate#samecodec)
ffmpeg -i step1.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts
ffmpeg -i step2.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate2.ts
ffmpeg -i step3.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate3.ts
ffmpeg -i "concat:intermediate1.ts|intermediate2.ts|intermediate3.ts" -c copy -bsf:a aac_adtstoasc step4.mp4



## Créer un long fichier audio (sox)
sox  audio.mp3 looped_audio.mp3 repeat 30


## Ajouter de la musique au fichier final
ffmpeg -i step4.mp4 -i looped_audio.mp3 -shortest step5.mp4
