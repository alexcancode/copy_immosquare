namespace :test do


  task :phantomjs => :environment do
    system "phantomjs #{Rails.root}/phantomjs/screenshot.js"
  end

  task :alex => :environment do
    image_url   = "https://s3.amazonaws.com/shareimmo/development/properties/73/shareimmo_73_331_medium.jpg?1476302887"
    filter_url  = "http://lvh.me:4000/insta_filter?picture_url=#{image_url}&filter=_1977"
    size        = FastImage.size(image_url)
    tmp_img     = "#{Rails.root}/tmp/alex/test.png"
    system "phantomjs #{Rails.root}/app/phantomjs/screenshot.js '#{filter_url}' #{size[0]} #{size[1]} #{tmp_img}"
  end


  task :jules => :environment do
    id        = 23873
    response  = HTTParty.get("http://shareimmo.com/api/v2/youtube/#{id}")
    prop      = JSON.parse(response.body)
    infos     = {
      :pictures => prop["pictures"],
      :texts    => {
        :name       => "Jules Welsch",
        :phone      => "+ 1 514 568 6780",
        :email      => "jules@immosquare.com",
        :reference  => "#12356",
        :locality   => "Montreal",
        :type       => "Appartement",
        :price      => " 1 200 000 $"
        },
        :logo   => "https://s3.amazonaws.com/shareimmo/production/brokers/1/logo_large.png",
        :avatar => "https://s3.amazonaws.com/shareimmo/production/brokers/1/picture_large.png"
      }
      OptimizeGenerator.new(
        :order_id           => id,
        :channel            => "my_channel",
        :infos              => infos,
        :visibility_start   => Time.now,
        :visibility_until   => Time.now+1.month
        ).perform
    end


    task :jules2 => :environment do
      YmlTranslation.new(:file => "#{Rails.root}/config/locales/fr.yml").to_db
      p
    end



    task :db_yml => :environment do
      @final = Hash.new
      Translation.all.each_with_index do |t,i|
        l = t.locale.to_s
        k = t.key.split(".")
        k.unshift(l)
        v = t.value.to_s
        e = DbYml.new.export(k, v)
        toto(@final, e)
      end
      File.open("#{Rails.root}/config/locales/dbdump.yml", 'w') { |file| file.write(@final.to_yaml) }
    end



    def toto(final, e)
      @final = final.deep_merge(e)
    end

  end
