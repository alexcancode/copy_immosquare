$("body.cedreo_orders.new").ready ->
	
	console.log "toto"
	$('#part2').hide()
	$('#part3').hide()
	$('#part4').hide()

	# form steps
	
	$('#button1').on "click", (e) ->
		e.preventDefault()
		$('#part1').hide()
		$('#part2').show()
		$('#part3').hide()
		$('#part4').hide()	
	
	$('#button2').on "click", (e) ->
		e.preventDefault()
		$('#part1').show()
		$('#part2').hide()
		$('#part3').hide()
		$('#part4').hide()	
	
	$('#button3').on "click", (e) ->
		e.preventDefault()
		$('#part1').hide()
		$('#part2').hide()
		$('#part3').show()
		$('#part4').hide()

	$('#button4').on "click", (e) ->
		e.preventDefault()
		$('#part1').hide()
		$('#part2').show()
		$('#part3').hide()
		$('#part4').hide()

	$('#button5').on "click", (e) ->
		e.preventDefault()
		$('#part1').hide()
		$('#part2').hide()
		$('#part3').hide()
		$('#part4').show()

	$('#button6').on "click", (e) ->
		e.preventDefault()
		$('#part1').hide()
		$('#part2').hide()
		$('#part3').show()
		$('#part4').hide()

	$('li.image-ad').on 'click', ->
		url = $(@).data("url")
		$('li.image-ad').removeClass('bg-twitter')
		$(@).addClass('bg-twitter')
		$('#cedreo_order_picture_url').val(url)
		$('#filterPreview img').attr('src', url)

	# decoration style selection
	$('.style-images').on 'click', ->
		$('.style-images > p').removeClass('color-green')
		$(@).children("p:first").addClass('color-green')
		chosen = $(@).data("style")
		$('#cedreo_order_decoration_style').val(chosen)


