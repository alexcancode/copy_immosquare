class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # before_action :set_locale

  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {locale: I18n.locale}
  end

  def set_locale
    I18n.locale  = params[:locale]
  end

  def after_sign_in_path_for(user)
    Profile.where(:user_id => user.id).first_or_create if user.profile.blank?
    dashboard_path
  end
end
