class CedreoOrdersController < ApplicationController

  include ClientAuthorized


  def new
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{params["id"]}?access_token=#{current_user.oauth_token}")
    @response = JSON.parse(response.body) if response.code == 200
    @cedreo_order = CedreoOrder.new
  end

  def create
    @cedreo_order = CedreoOrder.new(cedreo_order_params)
    if @cedreo_order.save
      UserMailer.welcome_email(@cedreo_order).deliver_later
      reponse   = HTTParty.get("#{current_user.provider_host}/api/v1/services/cedreo?access_token=#{current_user.oauth_token}&api_key=#{ENV["storeimmo_api_key"]}&api_token=#{ENV["storeimmo_api_token"]}")
      debit = reponse["authorizations"]["tokens"].first["debit_url"]
      token_debited = 1
      debit_post = HTTParty.post("#{debit}/#{token_debited}?access_token=#{current_user.oauth_token}&api_key=#{ENV["storeimmo_api_key"]}&api_token=#{ENV["storeimmo_api_token"]}")
      redirect_to confirmation_path(@cedreo_order)
    end
  end

  def confirmation
    @cedreo_order = CedreoOrder.find(params[:id])
  end

  private

  def cedreo_order_params
    params.require(:cedreo_order).permit!
  end

end