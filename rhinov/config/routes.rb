Rails.application.routes.draw do

  ##============================================================##
  ## Devise does not support scoping OmniAuth callbacks under a dynamic segment
  ##============================================================##
  devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}


  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do

    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users, skip: :omniauth_callbacks

    ##============================================================##
    ## OAuth
    ##============================================================##
    match 'users/auth/:provider/callback'   => 'authentifications#create',   :via => [:get]
    match 'users/auth/failure'              => 'authentifications#failure',  :via => [:get]
    match 'users/auth/:id'                  => 'authentifications#destroy',  :via => [:delete] ,:as => 'dashboard_delete_app'

    ##============================================================##
    ## Dashboard
    ##============================================================##
    root to: "dashboard#index"
    match "dashboard"       => "dashboard#index",           :via => [:get], :as => :dashboard
    match 'profile'         => "dashboard#profile",         :via => :get,   :as => :dashboard_profile
    match 'profile/:id'     => "dashboard#profile_update",  :via => :patch, :as => :dashboard_profile_update


    ##============================================================##
    ## RhinovOrder
    ##============================================================##
    match "rhinov/:id"        => "rhinov_orders#new",             :via => [:get],     :as => :rhinov_new
    match "rhinov"            => "rhinov_orders#create",          :via => [:post],    :as => :rhinov_create
    match "confirmation/:id"  => "rhinov_orders#confirmation",    :via => [:get],     :as => :confirmation

  end

end
