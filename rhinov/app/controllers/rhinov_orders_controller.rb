class RhinovOrdersController < ApplicationController

  include ClientAuthorized


  def new
    response      = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{params["id"]}?access_token=#{current_user.oauth_token}")
    @response     = JSON.parse(response.body) if response.code == 200
    @rhinov_order = RhinovOrder.new
    @styles       = [
      ['decorateur',"https://api.rhinov.fr/resources/styles/Je_fais_confiance.jpg"],
      ["Campagne","https://api.rhinov.fr/resources/styles/campagne.jpg"],
      ["design","https://api.rhinov.fr/resources/styles/design.jpg"],
      ["industriel","https://api.rhinov.fr/resources/styles/industriel.jpg"],
      ["scandinave","https://api.rhinov.fr/resources/styles/scandinave.jpg"]
    ]
  end

  def create
    @rhinov_order = RhinovOrder.new(rhinov_order_params)
    if @rhinov_order.save
    #   UserMailer.welcome_email(@rhinov_order).deliver_later
    #   reponse   = HTTParty.get("#{current_user.provider_host}/api/v1/services/rhinov?access_token=#{current_user.oauth_token}&api_key=#{ENV["storeimmo_api_key"]}&api_token=#{ENV["storeimmo_api_token"]}")
    #   debit = reponse["authorizations"]["tokens"].first["debit_url"]
    #   token_debited = 1
    #   debit_post = HTTParty.post("#{debit}/#{token_debited}?access_token=#{current_user.oauth_token}&api_key=#{ENV["storeimmo_api_key"]}&api_token=#{ENV["storeimmo_api_token"]}")
    end
    redirect_to confirmation_path(@rhinov_order.id)
  end



  def confirmation
    @rhinov_order = RhinovOrder.find(params[:id])
  end

  private

  def rhinov_order_params
    params.require(:rhinov_order).permit!
  end

end
