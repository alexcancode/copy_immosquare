class DashboardController < ApplicationController


  include ClientAuthorized

  def index
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties?access_token=#{current_user.oauth_token}")
    @response       = JSON.parse(response.body).deep_symbolize_keys! if response.code == 200
    @rhinov_orders      = RhinovOrder.where(user_id: current_user.id)
  end


  def profile
    @profile = current_user.profile
  end

  def profile_update
    @profile = Profile.find(params[:id])
    @profile.update_attributes(profile_params)
    flash[:message] = t('app.saved')
    redirect_to dashboard_path
  end


  private

  def profile_params
    params.require(:profile).permit!
  end
end