module ClientAuthorized
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_user!
    before_action :check_ability
  end



  def check_ability
    store_url   = "#{current_user.provider_host}/api/v1/services/rhinov?access_token=#{current_user.oauth_token}&api_key=#{ENV["storeimmo_api_key"]}&api_token=#{ENV["storeimmo_api_token"]}"
    httpparty   = HTTParty.get(store_url)
    # JulesLogger.info store_url
    @testApi    = JSON.parse(httpparty.body) if httpparty.code == 200
  end

end
