class UserMailer < ApplicationMailer
 
  def welcome_email(rhinov_order)
    @rhinov_order = rhinov_order
    mail(from: 'no-reply@immosquare.com', to: 'alex@immosquare.com', subject: 'Thank you for your purchase')
  end

end
