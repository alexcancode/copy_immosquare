# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161104140705) do

  create_table "profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "color1"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "phone"
    t.string   "email"
    t.string   "company_name"
    t.string   "company_email"
    t.string   "company_phone"
    t.string   "company_address"
    t.string   "company_address2"
    t.string   "company_country"
    t.string   "company_city"
    t.string   "company_province"
    t.string   "company_zipcode"
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "rhinov_orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "user_uid"
    t.string   "picture_url"
    t.string   "room_type"
    t.text     "room_type_comments", limit: 65535
    t.string   "decoration_style"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "reference_number"
    t.string   "address"
    t.string   "r_first_name"
    t.string   "r_last_name"
    t.string   "r_tel"
    t.string   "r_email"
    t.string   "r_company_name"
    t.string   "r_company_email"
    t.text     "r_notes",            limit: 65535
    t.index ["user_id"], name: "index_rhinov_orders_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uid"
    t.string   "provider"
    t.string   "provider_host"
    t.string   "oauth_token"
    t.string   "oauth_refresh_token"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
