class CreateRhinovOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :rhinov_orders do |t|
      t.references :user, foreign_key: false
      t.string :user_uid
      t.string :picture_url
      t.string :room_type
      t.text :room_type_comments
      t.string :decoration_style


      t.timestamps
    end
  end
end
