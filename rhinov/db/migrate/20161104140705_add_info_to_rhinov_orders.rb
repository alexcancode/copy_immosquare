class AddInfoToRhinovOrders < ActiveRecord::Migration[5.0]
  def change
  	add_column :rhinov_orders, :reference_number, :string
  	add_column :rhinov_orders, :address, :string
    add_column :rhinov_orders, :r_first_name, :string
  	add_column :rhinov_orders, :r_last_name, :string
  	add_column :rhinov_orders, :r_tel, :string
  	add_column :rhinov_orders, :r_email, :string
  	add_column :rhinov_orders, :r_company_name, :string
  	add_column :rhinov_orders, :r_company_email, :string
  	add_column :rhinov_orders, :r_notes, :text
  end
end

