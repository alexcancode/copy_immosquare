class AddTokensToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :oauth_token, :string, :after => :provider
    add_column :users, :oauth_refresh_token, :string, :after => :oauth_token
  end
end
