class AddInfoToOuiflashOrders < ActiveRecord::Migration[5.0]
  def change
  	add_column :ouiflash_orders, :reference_number, :string
    add_column :ouiflash_orders, :address, :string
    add_column :ouiflash_orders, :address_complement, :string
    add_column :ouiflash_orders, :picture_url, :string
    add_column :ouiflash_orders, :surface, :string
  	add_column :ouiflash_orders, :type_de_bien, :string
    add_column :ouiflash_orders, :r_first_name, :string
  	add_column :ouiflash_orders, :r_last_name, :string
  	add_column :ouiflash_orders, :r_tel, :string
  	add_column :ouiflash_orders, :r_email, :string
  	add_column :ouiflash_orders, :r_company_name, :string
  	add_column :ouiflash_orders, :r_company_email, :string
  	add_column :ouiflash_orders, :r_notes, :text
  end
end

