class CreateOuiflashOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :ouiflash_orders do |t|
      t.references :user, foreign_key: false
      t.string :user_uid
      t.references :reportage, foreign_key: false
      t.date :report_date
      t.time :report_time
      t.text :report_comments

      t.timestamps
    end
  end
end
