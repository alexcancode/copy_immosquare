class CreateReportages < ActiveRecord::Migration[5.0]
  def change
    create_table :reportages do |t|
      t.string :name

      t.timestamps
    end
  end
end
