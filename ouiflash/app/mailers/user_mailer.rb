class UserMailer < ApplicationMailer
 
  def welcome_email(ouiflash_order)
    @ouiflash_order = ouiflash_order
    mail(from: 'no-reply@immosquare.com', to: 'alex@immosquare.com', subject: 'Thank you for your purchase')
  end

end
