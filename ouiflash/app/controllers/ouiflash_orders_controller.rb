class OuiflashOrdersController < ApplicationController

  include ClientAuthorized


  def new
    response  = HTTParty.get("#{current_user.provider_host}/api/v1/properties/#{params["id"]}?access_token=#{current_user.oauth_token}")
    @response = JSON.parse(response.body) if response.code == 200
    @photo_report = Reportage.where(:id => @testApi["authorizations"]["tokens"].select {|auth| auth["tokens"] > 0}.pluck("sku") ) 
    # @photo_report = []
    @ouiflash_order = OuiflashOrder.new
  end

  def create
    @ouiflash_order = OuiflashOrder.new(ouiflash_order_params)
    if @ouiflash_order.save
      UserMailer.welcome_email(@ouiflash_order).deliver_later
      reponse   = HTTParty.get("#{current_user.provider_host}/api/v1/services/ouiflash?access_token=#{current_user.oauth_token}&api_key=#{ENV["storeimmo_api_key"]}&api_token=#{ENV["storeimmo_api_token"]}")
      debit = reponse["authorizations"]["tokens"].select {|auth| auth["sku"] == @ouiflash_order.reportage.id.to_s}.first["debit_url"]
      token_debited = 1
      debit_post = HTTParty.post("#{debit}/#{token_debited}?access_token=#{current_user.oauth_token}&api_key=#{ENV["storeimmo_api_key"]}&api_token=#{ENV["storeimmo_api_token"]}")
      redirect_to confirmation_path(@ouiflash_order)
    end
  end

  def confirmation
    @ouiflash_order = OuiflashOrder.find(params[:id])
  end

  private

  def ouiflash_order_params
    params.require(:ouiflash_order).permit!
  end

end