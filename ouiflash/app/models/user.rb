class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
   has_one :profile, dependent: :destroy
   has_many :ouiflash_orders, dependent: :destroy

   devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable,:omniauthable, :omniauth_providers => [:storeimmo,:gercopstore]

   def name_to_display
    (self.profile.first_name.present? || self.profile.last_name.present?) ?  "#{self.profile.first_name} #{self.profile.last_name}" : self.email
   end

end
