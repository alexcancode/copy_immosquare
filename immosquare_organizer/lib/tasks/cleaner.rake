namespace :cleaner do 

  task :reset_tracks => :environment do
    # RAZ du poids et des dates de lecture des morceaux.
    tracks = Track.all
    now = DateTime.now

    tracks.each do |t|
      t.update_attributes(:weight => 0, :last_played => now, :count_play => 0)
    end
  end

end