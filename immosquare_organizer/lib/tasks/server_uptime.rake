namespace :server_uptime do

  Pusher.app_id = ENV["PUSHER_APP_ID"]
  Pusher.key = ENV["PUSHER_KEY"]
  Pusher.secret = ENV["PUSHER_SECRET"]
  Pusher.logger = Rails.logger
  Pusher.encrypted = true

  task :task do
    # Ici, toutes les minutes, on va faire une requete HTTP pour aller chercher les infos de uptimerobot avec HTTParty.
    # Ensuite on va publier les resultats sur un channel avec Pusher
    # Le channel sera lu sur le client qui pourra afficher les infos a l'ecran
    response = HTTParty.get("http://api.uptimerobot.com/getMonitors?apiKey=#{ENV['UPTIMEROBOT_API_KEY']}&format=json")

    rep = response.body

    rep = rep.gsub("jsonUptimeRobotApi(", "")
      rep = rep.gsub(")", "")

      Pusher.trigger('server_channel', 'update', {
        message: rep,
        time: Time.now.strftime("%Y-%m-%d %H:%M:%S")
        })

      @redis = Redis.new(:host => 'localhost', :port => 6379)
      @redis.set('server_uptime', rep)
      @redis.set('server_uptime_time_last', Time.now.strftime("%Y-%m-%d %H:%M:%S"))
    end

  end
