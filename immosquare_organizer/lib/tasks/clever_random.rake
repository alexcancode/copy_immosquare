namespace :clever_random do 

  task :update_tracks => :environment do
    # On va passer dans cette tâche pour mettre à jour le poids des morceaux.
    # On calcule un poids en fonction de son nombre d'ecoute, de sa date d'ajout,
    # et de la dernière date à laquelle il a été lu.
    # Le poids le plus haut est celui devant être lu
    tracks = Track.all
    now = Time.now

    tracks.each do |t|
      hours_last_played =  t.last_played.blank? ? 0 : ((now - Time.parse(t.last_played.to_s)) / 3600).round
      w = hours_last_played - t.count_play + (t.weight.abs / 2)
      t.update_attribute(:weight, w)
    end
  end

  task :reset => :environment do
    tracks = Track.all
    now = Time.now

    tracks.each do |t|
      t.update_attributes(:weight => 0, :count_play => 0, :last_played => now)
    end
  end

end