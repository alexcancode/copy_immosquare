
every 1.month, :at => '2:30 am' do
  rake "stats:compile"
  rake "clever_random:reset"
  rake "clever_random:update_tracks"
end

every 5.minutes do
  rake "server_uptime:task"
end

every 4.hours do
  rake "clever_random:update_tracks"
end
