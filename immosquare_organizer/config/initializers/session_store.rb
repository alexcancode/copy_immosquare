if Rails.env.production? or Rails.env.beta?
  Rails.application.config.session_store :cookie_store, key: 'immosquare_organizer_session',:domain => :all
else
  Rails.application.config.session_store :cookie_store, :key => 'immosquare_organizer_session', :domain => "lvh.me"
end
