Rails.application.routes.draw do


  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do
    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users, :controllers => { :registrations => :registrations }
    # root  "pages#home"
    root  "dashboard#index", :via => :all


    ##============================================================##
    ## Dashboard
    ##============================================================##
    match 'dashboard/manage_direct'                    => "dashboard#admin_management",            :via => :get,       :as => :dashboard_manage
    match "dashboard/(:mode)"                          => "dashboard#index",                       :via => :get,       :as => :dashboard

    ##============================================================##
    ## Tracks
    ##============================================================##
    match 'tracks'              => "tracks#index",            :via => :get,     :as => :tracks
    match 'track/create'        => "tracks#create",           :via => :post,     :as => :track_create
    match 'track/:id'           => "tracks#destroy",          :via => :delete,     :as => :track_destroy
    match 'track/edit/:id'      => "tracks#update",        :via => :post,     :as => :track_edit
    # match 'track/add/:id'       => "tracks#add_to_playlist",        :via => :post,     :as => :track_add_to_playlist
    match 'track/remove/:id'    => "tracks#remove_from_playlist",        :via => :post,     :as => :track_remove_from_playlist

    ##============================================================##
    ## PLaylists
    ##============================================================##
    match 'playlists'              => "playlists#index",            :via => :get,     :as => :playlists
    match 'playlist/create'        => "playlists#create",           :via => :post,     :as => :playlist_create
    match 'playlist/:id'           => "playlists#destroy",          :via => :delete,   :as => :playlist_destroy
    match 'playlist/details/:id'   => "playlists#details",          :via => :post,   :as => :playlist_details

    ##============================================================##
    ## Users
    ##============================================================##
    match 'user/:id'      => "users#edit",        :via => :get,     :as => :user_edit
    match 'user/:id'      => "users#update",        :via => :patch,     :as => :user_update

    ##============================================================##
    ## Team
    ##============================================================##
    match 'teams'         => "teams#index",       :via => :get,     :as => :teams
    match 'team/create'   => "teams#create",      :via => :post,     :as => :team_create
    match 'team/:id'      => "teams#edit",        :via => :get,     :as => :team_edit
    match 'team/:id'      => "teams#destroy",      :via => :delete,  :as => :team_delete
    match 'team/:id'      => "teams#update",      :via => :patch,   :as => :team_update
    match 'team/add_user'      => "teams#add_user",      :via => :post,  :as => :team_add_user
    match 'team/remove_user/:user_id/:team_id'      => "teams#remove_user",      :via => :delete,   :as => :team_remove_user

    ##============================================================##
    ## Tasks
    ##============================================================##
    match 'tasks'         => "tasks#index",       :via => :get,     :as => :tasks
    match 'task'          => "tasks#create",      :via => :post,    :as => :task_create
    # match 'task/:id'      => "tasks#edit",        :via => :get,     :as => :task_edit
    match 'task/over/:id'      => "tasks#over",     :via => :post,  :as => :task_over
    match 'task/:id'      => "tasks#destroy",     :via => :delete,  :as => :task_destroy
    match 'task/:id'      => "tasks#update",      :via => :post,   :as => :task_update
    match 'tasks/remote'      => "tasks#remote_get",      :via => :post,   :as => :task_remote_get

    ##============================================================##
    ## API
    ##============================================================##
    namespace :api, defaults: {format: 'json'} do
      namespace :v1 do

        scope 'tracks' do
          match 'playlist/:playlist_id'       => 'tracks#retrieve_playlist',        :via => :get
          match 'direct/create'               => 'tracks#create_redis_list',        :via => :get
          match 'direct/get_list'             => 'tracks#get_redis_list',       :via => :get
          match 'direct/add'                  => 'tracks#add_to_direct_mode',       :via => :post
          match 'direct/remove'               => 'tracks#remove_from_queue',  :via => :post
          match 'direct/admin_remove/:uid'    => 'tracks#admin_remove_from_direct_mode',  :via => :delete
          match 'direct/get_random'           => 'tracks#get_random_track',  :via => :get
          match 'volume/:value'               => 'tracks#volume',  :via => :post
        end

        scope 'opus' do
          match 'monitoring/update'           => 'opus#monitoring_update',  :via => :post
        end
      end

      scope 'opus' do
        match 'monitoring/update'                 => 'opus#monitoring_update', :via => :post
      end


    end
  end
end
