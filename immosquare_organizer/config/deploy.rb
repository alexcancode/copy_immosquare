set :application, 'immosquare_organizer'
set :rvm_ruby_version, "2.3.2@immosquare_music"
set :repo_url, 'git@bitbucket.org:wgroupe/immosquare_organizer.git'
set :bundle_roles, :all
set :deploy_to, "/srv/apps/immosquare_organizer"
set :linked_dirs, %w{log bin tmp/pids tmp/cache tmp/sockets}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
