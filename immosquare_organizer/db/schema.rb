# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160504143710) do

  create_table "playlists", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "playlists", ["user_id"], name: "index_playlists_on_user_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string "name", limit: 255
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "task_dependencies", id: false, force: :cascade do |t|
    t.integer "task_id",        limit: 4
    t.integer "task_needed_id", limit: 4
  end

  add_index "task_dependencies", ["task_id"], name: "index_task_dependencies_on_task_id", using: :btree
  add_index "task_dependencies", ["task_needed_id"], name: "index_task_dependencies_on_task_needed_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "name",       limit: 255
    t.text     "content",    limit: 65535
    t.integer  "status",     limit: 4
    t.integer  "priority",   limit: 4
    t.integer  "progress",   limit: 4
    t.datetime "due_date"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "tasks", ["user_id"], name: "index_tasks_on_user_id", using: :btree

  create_table "tasks_projects", id: false, force: :cascade do |t|
    t.integer "task_id",    limit: 4
    t.integer "project_id", limit: 4
  end

  add_index "tasks_projects", ["project_id"], name: "index_tasks_projects_on_project_id", using: :btree
  add_index "tasks_projects", ["task_id"], name: "index_tasks_projects_on_task_id", using: :btree

  create_table "teams", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tracks", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "url",         limit: 255
    t.string   "video_id",    limit: 255
    t.integer  "user_id",     limit: 4
    t.integer  "playlist_id", limit: 4
    t.integer  "count_play",  limit: 4,   default: 0
    t.datetime "last_played",             default: '2016-05-04 15:05:52'
    t.integer  "weight",      limit: 4,   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tracks", ["playlist_id"], name: "index_tracks_on_playlist_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "username",               limit: 255
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "users_teams", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "team_id", limit: 4
  end

  add_index "users_teams", ["team_id"], name: "index_users_teams_on_team_id", using: :btree
  add_index "users_teams", ["user_id"], name: "index_users_teams_on_user_id", using: :btree

end
