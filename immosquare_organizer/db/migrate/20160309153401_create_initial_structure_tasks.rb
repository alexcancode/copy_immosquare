class CreateInitialStructureTasks < ActiveRecord::Migration
	def change

  	# table teams 
  	change_table :teams do |t|
  		t.remove :first_name
  		t.remove :last_name
  		t.remove :email
  		t.remove :avatar_file_name
  		t.remove :avatar_content_type
  		t.remove :avatar_file_size
  		t.remove :avatar_updated_at
  		t.remove :draft
  		t.string :name, :after => :id
  	end

  	# add references users/teams N:M
  	remove_reference :teams, :user, index: true, foreign_key: true

  	create_table :users_teams, id: false do |t|
  		t.references :user, index: true
  		t.references :team, index: true
  	end


  	# table tasks
  	change_table :tasks do |t|
  		t.remove :finish
  		t.integer :status, :after => :content
  		t.integer :priority, :after => :status
  		t.datetime :due_date, :after => :priority
  		t.integer :progress, :after => :priority
  	end

  	remove team_id, add references users
  	remove_column :tasks, :team_id
  	add_reference :tasks, :user, :index => true, :after => :id


  	# table task_dependencies
  	create_table :task_dependencies, id: false do |t|
  		t.references :task, index: true
  		t.references :task_needed, index: true
  	end


  	# table projects
  	create_table :projects do |t|
  		t.string :name
  	end


  	# table projects_tasks
  	create_table :tasks_projects, id: false do |t|
  		t.references :task, index: true
  		t.references :project, index: true
  	end
  end
end
