class AddCounttoTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :count_play, :integer, :after => :playlist_id, :default => 0
    add_column :tracks, :last_played, :datetime, :after => :count_play, :default => Time.now
    add_column :tracks, :weight, :integer, :after => :last_played, :default => 0
  end
end
