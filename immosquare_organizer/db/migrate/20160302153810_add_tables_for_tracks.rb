class AddTablesForTracks < ActiveRecord::Migration
	def change
		create_table :tracks do |t|
			t.string :name
			t.string :url
			t.references :user

			t.timestamps
		end

		create_table :playlists do |t|
			t.string :name
			t.timestamps
		end

	end
end
