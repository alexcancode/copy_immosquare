class AddUrlIdForTrack < ActiveRecord::Migration
  def change
  	add_column :tracks, :video_id, :string, :after => :url
  end
end
