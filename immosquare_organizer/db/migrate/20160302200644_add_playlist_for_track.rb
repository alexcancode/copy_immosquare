class AddPlaylistForTrack < ActiveRecord::Migration
  def change
  	add_reference :tracks, :playlist, :index => true, :after => :user_id
  end
end
