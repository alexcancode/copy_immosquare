class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :team, index: true, foreign_key: false
      t.string :name
      t.text :content
      t.integer :finish, :default => 0
      t.timestamps null: false
    end
  end
end
