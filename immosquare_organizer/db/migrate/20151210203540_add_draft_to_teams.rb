class AddDraftToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :draft, :integer, :default => 0
  end
end
