class AddUserForPlaylist < ActiveRecord::Migration
  def change
  	add_reference :playlists, :user, :index => true, :after => :name
  end
end
