# Application

## 1. Servers

| Name  | Dns | Type | IP | Description | Memory | CPU | More |
--------|-----|------|----|-------------|--------|-----|------|
| vana | vana.prestige-mls.com | ??? | 94.23.62.197 | Backend + Database | 16Go | 4C | All prestige environment (production + staging) |



## 2. Application

### Environment

| Service | Version | Description |
|---------|---------|-------------|
| PHP | 5.4.* | Main language |
| MySQL | 5.5 | Database engine |
|  |  |  |

### Applications
| Name | Server | Description | More |
|------|--------|-------------|------|
|Application|prestige-mls.com| Website||


### Technologies

* Framework : Symfony 2.1


