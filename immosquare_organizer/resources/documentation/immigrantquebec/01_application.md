# Application

## Ads management

Ads are managed with Ad Rotate plugin

### Flow

* 1. Add banner in medias
* 2. Add Linker in WP with real link
* 3. Create link in bitly with Linker link
* 4. Create ad with Ad Rotate (use bitly link for statistics)

## Multilanguages sites

All contents comes from v1 website. Periodically, v1 database is imported in v2 system.


## Guides management

From "Immigrer 2016", download process was updated. When click to download, a modal appears with contact guide form.

Flow :
* 1. Form send an email with admin address (admin@immigrantquebec.com). Format is json
* 2. Periodically, a cron get all new emails and add them in database (contact_guides)


## Contact guides

### Aweber import
* 1. Get file with all address [here](http://apps.immigrantquebec.com/guide-emails.csv)
* 2. Go to aweber - login
* 3. When logged, Subscribers => Add subscribers
* 4. Clic to "Add subscribers" button
* 5. Import more 10 Subscribers
* 6. Choose "immigrant - guides" and put file

### Statistics

Get statistics file [here](http://apps.immigrantquebec.com/stats-guide.csv)

