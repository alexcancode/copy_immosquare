# Infrastructure

## 1. Servers

| Name  | Dns | Type | IP | Description | Memory | CPU | More |
--------|-----|------|----|-------------|--------|-----|------|
| v1 - HM Infomaniak |  | HM | bhee.ftp.infomaniak.com | All | - | - | Mutualized Infomaniak |
| v2 - web02 |  | HM | bhee.ftp.infomaniak.com | All | - | - | Mutualized Infomaniak |
| v2 - web02 |  | | - | Version 2 backend  | - | - | Mutualized Immosquare server |
| v2 - db01 |  | | - | Version 2 database  | - | - | Mutualized Immosquare server |



## 2. Application

### Environment

| Version | Service | Version | Description |
|---------|---------|---------|-------------|
| 1 | PHP | 5.4.* | Main language |
| 1 | MySQL | 5.5 | Database engine |
| 2 | Ruby | 2.2.3 | Backend |
| 2 | MySQL | 5.5 | Database engine |

### Applications

| Name | Server | Description | More |
|------|--------|-------------|------|
|Application - v1 |www.immigrantquebec.com| Website| Currently, french version is in version 1|
|Application - v2 |[es,en].immigrantquebec.com| Website| Multi languages version is with version 2|


### Technologies

* CMS : Wordpress 4.2.7
* Framework : Ruby-on-Rails 4.2.5

