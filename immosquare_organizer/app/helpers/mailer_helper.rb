module MailerHelper


  def set_my_subject(subject)
    if Rails.env.development? || Rails.env.beta?
      subject = "###{Rails.env}## #{subject}"
    end
    return subject
  end


end
