
json.status 200

json.monitoring do
  json.last_update @opus_monitoring_last_update
  json.scraping do
    json.hours @opus_monitoring_scraping_hours
    json.day @opus_monitoring_scraping_day
    json.by_strategy @opus_monitoring_by_strategy
  end
end