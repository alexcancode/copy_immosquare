
json.status 1

json.monitoring do
  json.24 @opus_scraping_monitoring_scraping_24 if @opus_scraping_monitoring_scraping_24.present?
  json.2 @opus_scraping_monitoring_scraping_2 if @opus_scraping_monitoring_scraping_2.present?
end