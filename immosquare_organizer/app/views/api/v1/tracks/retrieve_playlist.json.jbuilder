json.tracks @tracks do |t|
	json.id t.id
	json.name t.name
	json.video_id t.video_id
	json.username t.user.username
end

json.track_video_ids @track_video_ids