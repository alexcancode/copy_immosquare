class PlaylistsController < ApplicationController

	before_filter :authenticate_user!
	before_filter :check_ability

	def index
    @playlists = Playlist.all.paginate(:page => params[:page], :per_page => 30).order('user_id = ' + current_user.id.to_s + ' desc, id desc')

  end

  def details
    @playlist = Playlist.find(params[:id])
  end

  def create
  	@playlist = Playlist.new(playlist_params)
  	@playlist.user_id = current_user.id
  	if @playlist.save
  		flash[:success] = t("playlists.create.success_message")
  	else
  		flash[:danger] = t("playlists.create.error_message", @playlist.errors.first[1])
  	end
  	redirect_to playlists_path
  end

  def destroy
  	@playlist = Playlist.find(params[:id])

    if @playlist.user_id != current_user.id
      flash[:danger] = t("playlists.destroy.right_message")
      return redirect_to playlists_path
    end

    # we remove each track from this playlist before deleting it
    @playlist.tracks.each do |t|
      @track = Track.find(t.id)
      @track.update_attribute(:playlist_id, nil)
    end

    if @playlist.destroy
      flash[:success] = t("playlists.destroy.success_message")
    else
      flash[:danger] = t("playlists.destroy.error_message")
    end
    redirect_to playlists_path
  end




  private

  def check_ability
  	redirect_to root_path if !current_user.has_any_role? :user, :admin, :team
  end

  def playlist_params
    params.require(:playlist).permit!
  end


end
