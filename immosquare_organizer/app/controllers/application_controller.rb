class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception



  ##============================================================##
  ## CANCANCAN : Handle Unauthorized Access
  ##============================================================##
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end


  def after_sign_in_path_for(resource)
    root_path
  end








  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {locale: I18n.locale}
  end






end
