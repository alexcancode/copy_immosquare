class Api::V1::TracksController < Api::V1::ApplicationController

  Pusher.app_id = ENV["PUSHER_APP_ID"]
  Pusher.key = ENV["PUSHER_KEY"]
  Pusher.secret = ENV["PUSHER_SECRET"]
  Pusher.logger = Rails.logger
  Pusher.encrypted = true

  ##============================================================##
  ## GET /api/v1/tracks/playlist/:playlist_id
  ##============================================================##
  # def retrieve_playlist
  #   # Getting all the tracks in the playlist playlist_id
  #   begin
  #     @tracks = Track.where(:playlist_id => params[:playlist_id]).shuffle
  #     @track_video_ids = ""
  #     @tracks.each do |t|
  #       unless t.video_id.nil? || t.video_id.blank?
  #         @track_video_ids += t.video_id + ","
  #       end
  #     end
  #     @track_video_ids = @track_video_ids[0...-1] unless @track_video_ids.empty?
  #     return render :json =>{:errors=>"Not Found tracks in playlist #{params[:playlist_id]}" }, :status=>402 if @tracks.blank?
  #     return render :json =>{:errors=>"Not Found IDs in playlist #{params[:playlist_id]}" }, :status=>402 if @track_video_ids.blank?

  #   rescue Exception => e
  #     return render :json =>{:errors=>e.message }, :status=> 402
  #   end
  # end

  def create_redis_list
    add_tracks
    return render :json =>{:message=>"OK"}, :status=>200
  end

  def get_redis_list
    if Redis.current.llen(ENV["REDIS_PLAYER_QUEUE_KEY"]) < ENV['REDIS_PLAYER_QUEUE_SIZE'].to_i
      create_redis_list
      get_redis_list
    else
      @playlist = Redis.current.lrange(ENV["REDIS_PLAYER_QUEUE_KEY"], 0, ENV['REDIS_PLAYER_QUEUE_SIZE'].to_i - 1).map{ |m| JSON.parse(m) }
      @playlist_ids = @playlist.map{ |m| m["video_id"]}.join(',')

      Pusher.trigger('next_channel', 'update', {
        playlist: Redis.current.lrange(ENV["REDIS_PLAYER_QUEUE_KEY"], 1, ENV['REDIS_PLAYER_QUEUE_SIZE'].to_i - 1).map{ |m| JSON.parse(m) }
        })

      Pusher.trigger(ENV["PLAYING_NOW_CHANNEL"], 'update', {
        name: @playlist[0]["name"],
        username: @playlist[0]["username"]
        })

      Pusher.trigger(ENV["NEXT_CHANNEL"], 'update', {
        playlist: @playlist[1..ENV['REDIS_PLAYER_QUEUE_SIZE'].to_i - 1]
        })

      return render :json =>{:data=> @playlist, :video_ids => @playlist_ids}, :status=>200
    end
  end

  # def add_to_direct_mode
  #   # adding a video to the queue in direct mode
  #   begin
  #     id = params[:url].split("watch?v=")[1].split("&")[0]

  #     response = HTTParty.get("http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v="+id+"&format=json")
  #     name = JSON.parse(response.body)['title']

  #     @timestamp = Time.now.to_s
  #     @o = {:uid => Digest::MD5.hexdigest(@timestamp), :video_id => id, :username => current_user.username, :name => name, :timestamp => @timestamp}.to_json
  #     raise "Error in insertion" unless Redis.current.rpush(ENV["REDIS_PLAYER_QUEUE_KEY"], @o)
  #   rescue Exception => e
  #     return render :json =>{:errors=> e.message}, :status=>402
  #   end
  #   return render :json =>{:data=> JSON.parse(@o), :message => "Morceau ajouté avec succes!"}, :status=>200
  # end

  # def add_to_direct_mode
  #   # adding a video to the queue in direct mode
  #   begin
  #     id = params[:url].split("watch?v=")[1].split("&")[0]

  #     response = HTTParty.get("http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v="+id+"&format=json")
  #     name = JSON.parse(response.body)['title']

  #     @timestamp = Time.now.to_s
  #     @o = {:uid => Digest::MD5.hexdigest(@timestamp), :video_id => id, :username => current_user.username, :name => name, :timestamp => @timestamp}.to_json
  #     raise "Error in insertion" unless Redis.current.rpush(ENV["REDIS_PLAYER_QUEUE_KEY"], @o)
  #   rescue Exception => e
  #     return render :json =>{:errors=> e.message}, :status=>402
  #   end
  #   Pusher.trigger('direct_mode_channel', 'update', {:data=> JSON.parse(@o), message: "Morceau ajouté avec succes!"})
  #   return render :json =>{:message => "Morceau ajouté avec succes!"}, :status=>200
  # end

  def remove_from_queue
    # removing a video from the queue
    Redis.current.lpop(ENV["REDIS_PLAYER_QUEUE_KEY"])

    # add another one
    add_tracks

    return render :json =>{:message=>"Video removed successfuly"}, :status=>200
  end

  def admin_remove_from_direct_mode
    # an admin can choose which track to remove from the queue in direct mode
    for i in 0..Redis.current.llen(ENV["REDIS_PLAYER_QUEUE_KEY"])-1
      @val = Redis.current.lindex(ENV["REDIS_PLAYER_QUEUE_KEY"], i)
      @t = JSON.parse(@val)
      if @t["uid"] == params[:uid]
        Redis.current.lrem(ENV["REDIS_PLAYER_QUEUE_KEY"], 1, @val)

        # add another one
        add_tracks

        return render :json =>{:message=>"Video removed successfuly", :uid => params[:uid]}, :status=>200
      end
    end
    return render :json =>{:message=>"Video not found", :uid => params[:uid]}, :status=>404
  end

  def volume
    Redis.current.set(ENV["REDIS_PLAYER_VOLUME_KEY"], params[:value])
    Pusher.trigger(ENV["VOLUME_CHANNEL"], 'update', {
      message: params[:value]
      })
    return render :json =>{:message=>"OK"}, :status=>200
  end


  private
  def getTrackCleverRandom
    # here, we select a track within the 5 that have the higher weights

    # @track = Track.where("video_id <> '' AND video_id is not null").shuffle.first

    @tracks = Track.where('weight >= ?', Track.order('weight DESC').limit(5).pluck(:weight).min)
    @track = @tracks.shuffle.first
    @tracks.each do |t|
      if t.count_play == 0
        # id it's a new track, we wanna play it now
        @track = t
        break
      end
    end
    @track.update_attributes(:weight => 0, :count_play => @track.count_play + 1, :last_played => DateTime.now)
    return @track
  end

  def add_tracks
    for i in Redis.current.llen(ENV["REDIS_PLAYER_QUEUE_KEY"])..(ENV['REDIS_PLAYER_QUEUE_SIZE'].to_i - 1)
      @track = getTrackCleverRandom
      @timestamp = Time.now.to_s
      @o = {:uid => Digest::MD5.hexdigest(@timestamp), :video_id => @track.video_id, :username => @track.user.username, :name => @track.name, :timestamp => @timestamp}.to_json
      Redis.current.rpush(ENV["REDIS_PLAYER_QUEUE_KEY"], @o)
    end
  end

end