class Api::V1::OpusController < Api::V1::ApplicationController

  ##============================================================##
  ## POST /api/v1/opus/monitoring/update
  ##============================================================##
  def monitoring_update
    begin
      JulesLogger.info params
      raise "missing scraping params"  if params[:scraping].nil?

      @opus_monitoring_by_strategy = Redis.current.set(ENV["REDIS_OPUS_MONITORING_SCRAPING_BY_STRATEGY"], params[:scraping][:by_strategy]) if params[:scraping][:by_strategy].present?
      @opus_monitoring_scraping_day = Redis.current.set(ENV["REDIS_OPUS_MONITORING_SCRAPING_DAY_KEY"], params[:scraping][:day].to_i) if params[:scraping][:day].present?
      @opus_monitoring_scraping_hours = Redis.current.set(ENV["REDIS_OPUS_MONITORING_SCRAPING_HOURS_KEY"], params[:scraping][:hours].to_i) if params[:scraping][:hours].present?
  	  @opus_monitoring_last_update = Redis.current.set(ENV["REDIS_OPUS_MONITORING_LAST_UPDATE"], Time.now)


    rescue Exception => e
      return render :json =>{:errors=>e.message }, :status=> 402
    end



  end

  
end
