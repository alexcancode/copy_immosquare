class UsersController < ApplicationController

	before_filter :authenticate_user!
	before_filter :check_ability

	def edit
		unless current_user.id == params[:id].to_i
			flash[:danger] = t("users.edit.right_message")
			redirect_to root_path
		end
		@user = User.find(params[:id])
	end

	def update
		unless current_user.id == params[:id].to_i
			flash[:danger] = t("users.update.right_message")
			redirect_to root_path
		end
		@user = User.find(params[:id])
		if @user.update_attributes(user_params)
			flash[:success] = t("tracks.update.success_message")
		else
			flash[:danger] = t("tracks.update.error_message", @user.errors.first[1])
		end
		redirect_to root_path
	end

	private

	def check_ability
		redirect_to root_path if !current_user.has_any_role? :user, :admin, :team
	end

	def user_params
		params.require(:user).permit!
	end

end
