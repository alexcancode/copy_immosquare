class TasksController < ApplicationController

	before_filter :authenticate_user!
	before_filter :check_ability

	def index
		@tasks = Task.where(:user_id => current_user.id).paginate(:page => params[:page], :per_page => 20).order('status DESC, priority ASC, due_date ASC')
		# @tasks = Task.where("tasks.status > ? AND tasks.user_id = ?", 0, current_user.id).paginate(:page => params[:page], :per_page => 20).order('due_date ASC')
		@users = User.all
	end

	def create
		@task = Task.new(task_params)
		@task.status = 1 # en cours
		if @task.save
			flash[:success] = t("tasks.create.success_message")

			# Pusher.app_id = ENV["PUSHER_APP_ID"]
			# Pusher.key = ENV["PUSHER_KEY"]
			# Pusher.secret = ENV["PUSHER_SECRET"]
			# Pusher.logger = Rails.logger
			# Pusher.encrypted = true
			# Pusher.trigger('task_channel', 'update', {message: @task.to_json})
		else
			flash[:danger] = t("tasks.create.error_message", @task.errors.first[1])
		end
		redirect_to tasks_path
	end

	def update
		@task = Task.find(params[:id])
		if @task.update_attributes(task_params)
			flash[:success] = t("tasks.update.success_message")
		else
			flash[:danger] = t("tasks.update.error_message")
		end
		redirect_to tasks_path
	end

	def over
		@task = Task.find(params[:id])
		if @task.update_attribute(:status, 0)
			flash[:success] = t("tasks.over.success_message")
		else
			flash[:danger] = t("tasks.over.error_message")
		end
		redirect_to tasks_path
	end

	def destroy
		@task = Task.find(params[:id])

		# Pusher.app_id = ENV["PUSHER_APP_ID"]
		# Pusher.key = ENV["PUSHER_KEY"]
		# Pusher.secret = ENV["PUSHER_SECRET"]
		# Pusher.logger = Rails.logger
		# Pusher.encrypted = true
		# Pusher.trigger('task_channel', 'update', {message: @task})

		@task.destroy
		redirect_to tasks_path
	end

	private

	def check_ability
		redirect_to root_path if !current_user.has_any_role? :user, :admin, :team
	end

	def task_params
		params.require(:task).permit!
	end

end
