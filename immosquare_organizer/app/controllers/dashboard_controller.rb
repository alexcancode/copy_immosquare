class DashboardController < ApplicationController

  before_filter :authenticate_user!
  before_filter :check_ability
  before_filter :check_admin, :only => [:admin_management]


  def index
    if params[:page].present?
      @page = params[:page]
    else
      @page = 1
    end

    @mode = params[:mode] if params[:mode].present?

    # Opus monitoring
    @opus = {}
    @opus[:monitoring] = {}
    @opus[:monitoring][:last_update] = Redis.current.get(ENV["REDIS_OPUS_MONITORING_LAST_UPDATE"])
    @opus[:monitoring][:day] = Redis.current.get(ENV["REDIS_OPUS_MONITORING_SCRAPING_DAY_KEY"]).to_i
    @opus[:monitoring][:hours] = Redis.current.get(ENV["REDIS_OPUS_MONITORING_SCRAPING_HOURS_KEY"]).to_i
    @opus[:monitoring][:by_strategy] = Redis.current.get(ENV["REDIS_OPUS_MONITORING_BY_STRATEGY"])
    @opus[:monitoring][:down] = []
    @opus[:monitoring][:down] << "node01.test.fr"

    @number_tasks_per_page = 5
    JulesLogger.info @opus[:monitoring][:by_strategy]

    respond_to do |format|

      format.js {
        if params[:priority].blank?
          if params[:task].blank? || params[:task][:u_id].blank?
            @tasks = Task.where(:status => 1).paginate(:page => @page, :per_page => @number_tasks_per_page).order('status DESC, priority ASC, due_date ASC')
          else
            @tasks = Task.where(:status => 1).where(:user_id => params[:task][:u_id]).paginate(:page => @page, :per_page => @number_tasks_per_page).order('status DESC, priority ASC, due_date ASC')
          end
        else
          if params[:task].blank? || params[:task][:u_id].blank?
            @tasks = Task.where(:status => 1).where(:priority => params[:priority].map(&:to_i)).paginate(:page => @page, :per_page => @number_tasks_per_page).order('status DESC, priority ASC, due_date ASC')
          else
            @tasks = Task.where(:status => 1).where(:user_id => params[:task][:u_id]).where(:priority => params[:priority].map(&:to_i)).paginate(:page => @page, :per_page => @number_tasks_per_page).order('status DESC, priority ASC, due_date ASC')
          end
        end

        @users = User.all
      }

      format.html {
        @teams = Team.where(:draft =>0)

        # getting the saved server uptime infos so that we don't have to wait 1 minute
        @server_uptime = Redis.current.get('server_uptime')
        @time_last = Redis.current.get('server_uptime_time_last')

        @playlists = Playlist.all

        # tasks management
        @tasks = Task.joins(:user).where(:status => 1).paginate(:page => params[:page], :per_page => @number_tasks_per_page).order('status DESC, priority ASC, due_date ASC')

        @users = User.all
      }
    end
  end


  def admin_management
    @playlist_redis = Redis.current.lrange(ENV["REDIS_PLAYER_QUEUE_KEY"], 0, ENV['REDIS_PLAYER_QUEUE_SIZE'].to_i-1).map{ |m| JSON.parse(m) }
  end

  private
  def check_ability
    redirect_to root_path if !current_user.has_any_role? :user,:admin, :team
  end

  def check_admin
    redirect_to root_path if !current_user.has_any_role? :admin
  end

end
