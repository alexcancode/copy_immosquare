class TeamsController < ApplicationController

	before_filter :authenticate_user!
	before_filter :check_ability

	def index
		@teams = Team.all
	end

	def create
		@team = Team.new(team_params)
		if @team.save
			flash[:success] = t("teams.create.success_message")
		else
			flash[:danger] = t("teams.create.error_message", @team.errors.first[1])
		end
		redirect_to teams_path
	end

	def edit
		@team = Team.find(params[:id])

		@users = User.all
	end

	def update
		@team = Team.find(params[:id])
		@team.update_attributes(team_params)
		redirect_to teams_path
	end

	def destroy
		@team = Team.find(params[:id])
		@team.destroy
		redirect_to teams_path
	end

	def add_user
		@team = Team.find(params[:user][:team_id])
		@user = User.find(params[:user][:user_id])

		if @team.users.include?(@user)
			flash[:danger] = t("teams.add_user.include_message")
			return redirect_to team_edit_path(@team)
		else
			res = @team.users << @user
		end

		if res
			flash[:success] = t("teams.add_user.success_message")
		else
			flash[:danger] = t("teams.add_user.error_message")
		end
		return redirect_to team_edit_path(@team)
	end

	def remove_user
		@team = Team.find(params[:team_id])
		@user = User.find(params[:user_id])

		if @team.users.include?(@user)
			res = @team.users.delete(@user)
		else
			flash[:danger] = t("teams.remove_user.include_message")
			return redirect_to team_edit_path(@team)
		end

		if res
			flash[:success] = t("teams.add_user.success_message")
		else
			flash[:danger] = t("teams.add_user.error_message")
		end

		return redirect_to team_edit_path(@team)
	end


	private
	def check_ability
		redirect_to root_path if !current_user.has_any_role? :user, :admin, :team
	end

	def team_params
		params.require(:team).permit!
	end
end
