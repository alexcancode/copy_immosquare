class TracksController < ApplicationController

	before_filter :authenticate_user!
	before_filter :check_ability

	def index
		@tracks = Track.all.paginate(:page => params[:page], :per_page => 30).order('user_id = ' + current_user.id.to_s + ' desc, id desc')
		@playlists = Playlist.where(:user_id=>current_user.id)
	end

  def create
  	@track = Track.new(track_params)
  	@track.user_id = current_user.id
    @track.count_play = 0
    @track.weight = Track.maximum(:weight)
    if @track.save
      flash[:success] = t("tracks.create.success_message")
      render :js => "window.location = '/tracks'"
    elsif @track.errors[:track_id_not_split]
      # Here we'll go in the create.js.erb file
    else
      flash[:danger] = t("tracks.create.error_message", @track.errors.first[1])
      render :js => "window.location = '/tracks'"
    end
  end

  def update
    @track = Track.find(params[:id])
    if @track.user_id != current_user.id
      flash[:danger] = t("tracks.update.right_message")
      return redirect_to tracks_path
    end
    if @track.update_attributes(track_params)
      flash[:success] = t("tracks.update.success_message")
    else
      flash[:danger] = t("tracks.update.error_message", @track.errors.first[1])
    end
    redirect_to tracks_path
  end

  def destroy
  	@track = Track.find(params[:id])
    if @track.user_id != current_user.id
      flash[:danger] = t("tracks.destroy.right_message")
      return redirect_to tracks_path
    end
  	if @track.destroy
  		flash[:success] = t("tracks.destroy.success_message")
  	else
  		flash[:danger] = t("tracks.destroy.error_message")
  	end
  	redirect_to tracks_path
  end

  # def add_to_playlist
  #   @track = Track.find(params[:id])
  #   if @track.user_id != current_user.id
  #     flash[:danger] = "You don't have the right to update that track"
  #     return redirect_to tracks_path
  #   end
  #   if @track.update_attributes(track_params)
  #     flash[:success] ="Track added"
  #   else
  #     flash[:danger] = "Error"
  #   end
  #   redirect_to tracks_path
  # end

  def remove_from_playlist
    @track = Track.find(params[:id])
    if @track.user_id != current_user.id
      flash[:danger] = t("tracks.remove_from_playlist.right_message")
      return redirect_to playlists_path
    end
    if @track.update_attribute(:playlist_id, nil)
      flash[:success] = t("tracks.remove_from_playlist.success_message")
    else
      flash[:danger] = t("tracks.remove_from_playlist.error_message")
    end
    redirect_to playlists_path
  end



  private

  def check_ability
    redirect_to root_path if !current_user.has_any_role? :user, :admin, :team
  end

  def track_params
    params.require(:track).permit!
  end

end
