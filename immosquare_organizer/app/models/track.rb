class Track < ActiveRecord::Base
  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  belongs_to :playlist

  ##============================================================##
  ## Validation
  ##============================================================##
  validates_presence_of :url, :message => "URL can't be empty" 

  before_create :split_url

  def split_url
  	begin
  		self.video_id = self.url.split("watch?v=")[1].split("&")[0] if self.video_id.nil? || self.video_id.empty?
  	rescue
  		# can't find ID ... Il faut le rentrer a la main
      errors.add(:track_id_not_split, "Id couldn't be found from URL ...")
    end

    if self.video_id.nil? || self.video_id.empty?
      response = HTTParty.get("http://www.youtube.com/oembed?url="+self.url+"&format=json")
      self.name = JSON.parse(response.body)['title']
    else 
      begin
      response = HTTParty.get("http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v="+self.video_id+"&format=json")
      self.name = JSON.parse(response.body)['title']
      rescue
        self.name = "???"
      end
    end
  end
end