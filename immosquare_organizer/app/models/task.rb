class Task < ActiveRecord::Base
	belongs_to :user
	has_and_belongs_to_many :projects, join_table: "tasks_projects"
	has_and_belongs_to_many :tasks, join_table: "task_dependencies"
end
