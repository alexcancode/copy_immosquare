class Project < ActiveRecord::Base
	has_and_belongs_to_many :tasks, join_table: "tasks_projects"
end
