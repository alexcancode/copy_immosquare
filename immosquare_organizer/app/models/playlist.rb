class Playlist < ActiveRecord::Base
  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  has_many :tracks

  ##============================================================##
  ## Validation
  ##============================================================##
  # validates_presence_of :url, :message => "URL can't be empty" 

end
