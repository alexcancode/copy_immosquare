class Team < ActiveRecord::Base
  ##============================================================##
  ## Associations
  ##============================================================##
  has_and_belongs_to_many :users, join_table: "users_teams"

  validates_presence_of :name, :message => "Name can't be empty" 


end
