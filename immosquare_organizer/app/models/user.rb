class User < ActiveRecord::Base


  ##============================================================##
  ## Association
  ##============================================================##
  rolify
  has_many :estimates, dependent: :nullify
  has_one :profile, dependent: :destroy
  has_and_belongs_to_many :teams, join_table: "users_teams"
  has_many :tasks
  has_many :playlists


  ##============================================================##
  ## Active Record Callbacks
  ##============================================================##
  before_validation :check_username
  after_create :assign_default_role


   ##============================================================##
  ## Paperclip : Website avatar
  ##============================================================##
  has_attached_file :avatar,
  :path => "#{Rails.env}/users/:id/avatar_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :large => ["500x500#", :jpg]
  },
  :convert_options => {
    :large => "-background white -flatten +matte -quality 90 -strip"
  },
  :default_url => "http://placehold.it/500x500"
  validates_attachment_content_type :avatar, :content_type => /\Aimage/
  validates_attachment_size :avatar, :less_than=>2.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_avatar


  def rename_avatar
    extension = File.extname(avatar_file_name).downcase
    self.avatar.instance_write :file_name, "shareimmo_hook_avatar_#{Time.now.to_i.to_s}#{extension}"
  end




  ##============================================================##
  ## Devise
  ##============================================================##
  devise :database_authenticatable, :rememberable, :trackable


  private

  def check_username
    self.username = "#{(self.email.split('@').first.gsub('.','').gsub('&','').gsub('+','')).downcase}#{Random.new.bytes(4).bytes.join[0,4]}" if self.username.blank? and self.email.present?
  end

  def assign_default_role
    add_role(:user)
  end




end
