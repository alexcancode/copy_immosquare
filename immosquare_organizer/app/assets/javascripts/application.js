
//= require jquery
//= require jquery_ujs
//= require jquery-ui


//============================================================
//  OHTER LIB
//============================================================
//= require bootstrap-sprockets
//= require chosen-jquery



//============================================================
//  FROALA
//============================================================
//= require froala_editor.min.js
//= require plugins/align.min.js
//= require plugins/char_counter.min.js
//= require plugins/colors.min.js
//= require plugins/entities.min.js
//= require plugins/file.min.js
//= require plugins/font_family.min.js
//= require plugins/font_size.min.js
//= require plugins/fullscreen.min.js
//= require plugins/image.min.js
//= require plugins/inline_style.min.js
//= require plugins/line_breaker.min.js
//= require plugins/link.min.js
//= require plugins/lists.min.js
//= require plugins/paragraph_format.min.js
//= require plugins/paragraph_style.min.js
//= require plugins/quote.min.js
//= require plugins/table.min.js
//= require plugins/url.min.js
//= require plugins/video.min.js
//= require languages/ro.js



//============================================================
//  JQUERY PLUGINS
//============================================================
//= require base/jquery.readyselector
//= require jquery.remotipart



//= require application/00_for_all_pages



