$(document).ready ->

  $('.chosen').chosen()

  $('[data-toggle="popover"]').popover()


  ##============================================================##
  ## Prevent submit before Geoloc
  ##============================================================##
  $('#homeForm').on "keypress", (e) ->
    return false if e.keyCode == 13



  if typeof froala_init isnt 'undefined' and froala_init is true
    ##============================================================##
    ## WYSIWYG
    ##============================================================##
    $('.froala').froalaEditor(
      theme: 'gray'
      minHeight: '500'
      toolbarButtons: [
        'fullscreen'
        'bold'
        'italic'
        'underline'
        'strikeThrough'
        'subscript'
        'superscript'
        'fontFamily'
        'fontSize'
        '|'
        'color'
        'emoticons'
        'inlineStyle'
        'paragraphStyle'
        '|'
        'paragraphFormat'
        'align'
        'formatOL'
        'formatUL'
        'outdent'
        'indent'
        '-'
        'insertLink'
        'insertImage' if picture_module == true
        'insertVideo' if picture_module == true
        'insertTable'
        '|'
        'quote'
        'insertHR'
        'undo'
        'redo'
        'clearFormatting'
        'selectAll'
      ]
      imageUploadURL: froala_upload_url
      imageMaxSize: 1 * 1024 * 1024
      imageAllowedTypes: [
        'jpeg'
        'jpg'
        'png'
      ]
      inlineMode: false
      toolbarFixed: false).on 'froalaEditor.image.removed', (e, editor, $img) ->
      $.ajax(
        method: 'POST'
        url: frola_delete_url
        data: src: $img.attr('src')).done((data) ->
        console.log 'image was deleted'
        return
      ).fail ->
        console.log 'image delete problem'
        return
      return






  if google?
    ##============================================================##
    ## Google Autocomplete Management
    ##============================================================##
    autocomplete = new (google.maps.places.Autocomplete)(document.getElementById('gmaps-input-address'), types: [ 'geocode' ])
    google.maps.event.addListener autocomplete, 'place_changed', ->
      fillInAddress()


    componentForm =
      street_number:
        api_ref:'short_name'
        input_ref: 'street_number'
      route:
        api_ref: 'long_name'
        input_ref: 'street_name'
      neighborhood:
        api_ref: 'long_name'
        input_ref: 'neighborhood'
      sublocality_level_1:
        api_ref: 'long_name'
        input_ref: 'sublocality'
      locality:
        api_ref: 'long_name'
        input_ref: 'locality'
      administrative_area_level_2:
        api_ref: 'long_name'
        input_ref: 'administrative_area_level_2'
      administrative_area_level_1:
        api_ref: 'short_name'
        input_ref: 'administrative_area_level_1'
      country:
        api_ref: 'short_name'
        input_ref: 'country'
      postal_code_prefix:
        api_ref: 'short_name'
        input_ref: 'zipcode'
      postal_code:
        api_ref: 'short_name'
        input_ref: 'zipcode'

    $("#gmaps-input-address").on 'focus', ->
      if navigator.geolocation
        navigator.geolocation.getCurrentPosition (position) ->
          geolocation = new (google.maps.LatLng)(position.coords.latitude, position.coords.longitude)
          circle = new (google.maps.Circle)
            center: geolocation
            radius: position.coords.accuracy
          autocomplete.setBounds(circle.getBounds())


    fillInAddress = ->
      place = autocomplete.getPlace()
      i = 0
      while i < place.address_components.length
        addressType = place.address_components[i].types[0]
        if componentForm[addressType]
          val = place.address_components[i][componentForm[addressType].api_ref]
          # console.log "#{addressType} => #{val}"
          $("#estimate_#{componentForm[addressType].input_ref}").val(val)
        i++
      $('#estimate_zipcode').trigger('change')
      $("#estimate_latitude").val(place.geometry.location.lat())
      $("#estimate_longitude").val(place.geometry.location.lng())
      $("#estimate_formated_address").val(place.formatted_address)
      $("#estimate_place_id").val(place.place_id)
      $('#imageMap img').attr('src',"https://maps.googleapis.com/maps/api/staticmap?center=#{place.geometry.location.lat()},#{place.geometry.location.lng()}&zoom=13&size=600x300&maptype=roadmap&markers=#{place.geometry.location.lat()},#{place.geometry.location.lng()}")

