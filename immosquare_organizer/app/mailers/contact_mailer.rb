class ContactMailer < ActionMailer::Base

  default :template_path => "mailers/contact_mailer"
  layout 'mailer'
  include MailerHelper

  def contact(contact)
    @contact = contact
    @title = "Demande information - formulaire contact"
    mail(
     :to => "contact@immosquare.com",
     :from => 'no-reply@meilleurprix.immo',
     :subject => set_my_subject(@title)
     )
  end

end
