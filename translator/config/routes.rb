Rails.application.routes.draw do
  root 'fileupload#index'
  match '/'                => 'fileupload#index',        :via => :get,             :as => :fileupload
  match '/upload'                => 'fileupload#upload',        :via => :post,             :as => :fileupload_upload
  match '/modify'                => 'fileupload#modify',        :via => :post,             :as => :fileupload_modify
  match '/download'                => 'fileupload#download',        :via => :post,             :as => :fileupload_download
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
