set :application, 'immosquare_translate'
set :rvm_ruby_version, "2.3.2@immosquare_translate"
set :repo_url, 'git@bitbucket.org:wgroupe/translator.git'
set :bundle_roles, :all
set :deploy_to, "/srv/apps/immosquare_translate"
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
