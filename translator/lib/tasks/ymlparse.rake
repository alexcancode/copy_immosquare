namespace :ymlparse do

  task :split => :environment do
    YmlTranslation.new(:file => "#{Rails.root}/tmp/uploaded_file/app.en.yml").to_db
  end

  task :db_yml => :environment do
    @final = Hash.new
    File.open("#{Rails.root}/tmp/downloaded_file/dload.txt").each_line do |line|
      l = line.split("|")[0].strip
      k = line.split("|")[1].split(".")
      k.unshift(l)
      v = line.split("|")[2].chomp.to_s
      e = DbYml.new.export(k, v)
      toto(@final, e)
    end
    File.open("#{Rails.root}/tmp/formatted_downloaded_file/final.yml", 'w') { |file| file.write(@final.to_yaml) }
  end

  task :remove => :environment do
    FileUtils.rm_rf(Dir.glob('tmp/downloaded_file/*'))
    FileUtils.rm_rf(Dir.glob('tmp/formatted_downloaded_file/*'))
    FileUtils.rm_rf(Dir.glob('tmp/formatted_uploaded_file/*'))
    FileUtils.rm_rf(Dir.glob('tmp/uploaded_file/*'))
  end

  def toto(final, e)
    @final = final.deep_merge(e)
  end

end

