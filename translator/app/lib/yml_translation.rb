class YmlTranslation

  # def initialize(args)
  #   begin
  #     @file = args[:file]
  #     raise "No file found" if !File.exist?(@file)
  #   rescue Exception => e
  #     # JulesLogger.info e.message
  #   end
  # end

  # def to_db
  #   yml = YAML.load_file(File.open(@file))
  #   yml.keys.each do |locale|
  #     deep_parser(locale,yml[locale])
  #   end if yml.is_a?(Hash)
  # end



  # def deep_parser(locale,hash,key=nil)
  #   if hash.is_a?(Hash)
  #     hash.keys.each do |k|
  #       deep_parser(locale, hash[k],"#{key}#{key.present? ? "." : nil }#{k}")
  #     end
  #   else
  #     open("#{Rails.root}/tmp/formatted_uploaded_file/test.txt", 'a') do |f|
  #       f << "#{locale},#{key},#{hash}\n"
  #     end
  #   end
  # end


  def initialize(args)
    begin
      @file = args[:file]
      raise "No file found" if !File.exist?(@file)
    rescue Exception => e
      # JulesLogger.info e.message
    end
  end

  def to_db
    yml = YAML.load_file(File.open(@file))
    yml.keys.each do |locale|
      deep_parser(locale,yml[locale],@file.split('/').last)
    end if yml.is_a?(Hash)
  end



  def deep_parser(locale,hash,filename,key=nil)
    if hash.is_a?(Hash)
      hash.keys.each do |k|
        deep_parser(locale, hash[k],filename,"#{key}#{key.present? ? "." : nil }#{k}")
      end
    else
      open("#{Rails.root}/tmp/formatted_uploaded_file/#{filename}", 'a') do |f|
        f << "#{locale}|#{key}|#{hash}\n"
      end
    end
  end


end