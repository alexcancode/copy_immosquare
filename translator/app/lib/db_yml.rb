class DbYml

  def initialize
    @h = {}
  end

  def export(array, value)
    @h = array.reverse.inject(value) {|a, n| {n => a} }
  end

end