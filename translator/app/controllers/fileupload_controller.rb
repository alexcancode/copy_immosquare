class FileuploadController < ApplicationController
  def index
    @name = params[:base_file]
    @modify = params[:modify_file]
  end

  def upload
    begin
      raise "Please Upload File" if params[:upload].nil?
      @name = SecureRandom.hex + "_" + params[:upload][:file].original_filename
      
      folder = "tmp/uploaded_file" 
      unless File.directory?(folder)
        FileUtils.mkdir_p(folder)
      end
      path = File.join(folder, @name)
      File.open(path, "wb") { |f| f.write(params[:upload][:file].read) }
      folder_2 = "tmp/formatted_uploaded_file"
      unless File.directory?(folder_2)
        FileUtils.mkdir_p(folder_2)
      end
      YmlTranslation.new(:file => "#{Rails.root}/tmp/uploaded_file/#{@name}").to_db
      redirect_to root_path(:base_file => @name)
    rescue Exception => e 
      flash[:danger] = e.message
      redirect_to root_path
    end
  end

  def modify
    begin
      raise "Please Upload File" if params[:modify].nil?
      @name = params[:modify][:base_file]
      @modify = SecureRandom.hex + "_" + params[:modify][:file].original_filename
      folder = "tmp/uploaded_file"
      path = File.join(folder, @modify)
      File.open(path, "wb") { |f| f.write(params[:modify][:file].read) }
      YmlTranslation.new(:file => "#{Rails.root}/tmp/uploaded_file/#{@modify}").to_db
      redirect_to root_path(:base_file => @name, modify_file: @modify)
    rescue Exception => e 
      flash[:danger] = e.message
      redirect_to :back
    end
  end

  def download
    folder_3 = "tmp/downloaded_file"
    unless File.directory?(folder_3)
      FileUtils.mkdir_p(folder_3)
    end
    folder_4 = "tmp/formatted_downloaded_file"
    unless File.directory?(folder_4)
      FileUtils.mkdir_p(folder_4)
    end
    @name = params[:base_file]
    file = "tmp/downloaded_file/#{@name}"
    if File.exist?(file)
      FileUtils.rm_rf(file)
    end
    File.open("#{Rails.root}/tmp/formatted_uploaded_file/#{@name}", "r").each_line do |line|
      work = params[:new_translation]["#{line.split('|')[1]}".to_sym]
      p work
      work = work.gsub("\r\n", "<br>")
      while work[-4..-1] == "<br>"
        work = work[0..-5]
      end
      open("#{Rails.root}/tmp/downloaded_file/#{@name}", 'a') do |f|
        f << "#{params[:new_translation][:locale].downcase}|#{line.split('|')[1]}|#{work}\n"
      end      
    end
    @final = Hash.new
    File.open("#{Rails.root}/tmp/downloaded_file/#{@name}").each_line do |line|
      l = line.split("|")[0].strip
      k = line.split("|")[1].split(".")
      k.unshift(l)
      v = line.split("|")[2].chomp.to_s
      e = DbYml.new.export(k, v)
      toto(@final, e)
    end
    File.open("#{Rails.root}/tmp/formatted_downloaded_file/#{@name}", 'w') { |file| file.write(@final.to_yaml(line_width: -1)) }
    send_file(Rails.root.join("tmp", "formatted_downloaded_file", "#{@name}"), disposition: 'inline', filename: 'your_translation.yml')
  end

  private

  def toto(final, e)
    @final = final.deep_merge(e)
  end

end