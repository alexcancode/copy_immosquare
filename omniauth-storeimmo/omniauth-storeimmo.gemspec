# -*- encoding: utf-8 -*-
require File.expand_path('../lib/omniauth-storeimmo/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Jules Welsch"]
  gem.email         = ["jules@immosquare.com"]
  gem.description   = %q{Official OmniAuth strategy for Storeimmo.}
  gem.summary       = %q{Official OmniAuth strategy for Storeimmo.}
  gem.homepage      = "https://storeimmo.com/intridea/omniauth-storeimmo"
  gem.license       = "MIT"

  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.name          = "omniauth-storeimmo"
  gem.require_paths = ["lib"]
  gem.version       = OmniAuth::Storeimmo::VERSION

  gem.add_dependency 'omniauth', '~> 1.0'
  # Nothing lower than omniauth-oauth2 1.1.1
  # http://www.rubysec.com/advisories/CVE-2012-6134/
  gem.add_dependency 'omniauth-oauth2', '1.3.1'
end
