require 'omniauth-oauth2'

module OmniAuth

 module Strategies
    class Storeimmo < OmniAuth::Strategies::OAuth2
      option :name, :storeimmo


      option :client_options, {
        site: Rails.env.development? ? "http://gercop.lvh.me:3010" : 'http://store.gercop.com',
        authorize_path: 'oauth/authorize',
        token_path: 'oauth/token'
      }

      uid do
        raw_info['id']
      end


      def raw_info
        @raw_info ||= access_token.get('/api/v1/me').parsed
      end
    end
  end
end
