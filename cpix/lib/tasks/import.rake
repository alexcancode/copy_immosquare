namespace :import do

  require 'roo'

  ##============================================================##
  ## 1 : CPIX_Office
  ##============================================================##
  task :offices => :environment do
    begin
      ods = Roo::Spreadsheet.open("#{Rails.root}/import/CPIX_Property_20160914_Actifs.xlsx")
      ods.sheet('CPIX_Office').each(id: 'OfficeID', city: 'City', email: 'Email',fax: "Fax",office_name: "OfficeName",:zipcode => "PostCode",:country => "State",:street1=>"Street1",:street2 => "Street2",:website => "Website",:office_type =>"OfficeType",:phone_ext => "PhoneExt",:office_manager_id => "OfficeManagerID",:cls => "CLS",:firm_code =>"FirmCode") do |office|
        o           = Office.where(:uid =>office[:id]).first_or_create
        o.client_id = 1
        o.name      = office[:office_name]
        o.email     = office[:email]
        if o.save
          JulesLogger.info o
        else
          JulesLogger.info o.errors.full_messages
        end
      end
    rescue Exception => e
      JulesLogger.info e.message
    end
  end


  ##============================================================##
  ## 2 : CPIX_User
  ##============================================================##
  task :users => :environment do
    begin
      ods = Roo::Spreadsheet.open("#{Rails.root}/import/CPIX_Property_20160914_Actifs.xlsx")
      ods.sheet('CPIX_User').each(id: 'UserID', office_id: 'OfficeID', permission: 'Permission',email:'Email',fist_name: "FName",last_name: "LName",phone: "BusinessPhone",mobile_phone:"MobilePhone",member_type:"MemberType",cls:"CLS",board:"Board") do |user|
        full_name = "#{user[:first_name]}#{user[:last_name]}".gsub(" ", "")
        email = user[:email].present? ? user[:email] : "#{full_name}#{user[:id]}@realtor.com"
        u = User.where(:email =>email).first_or_create

        office = Office.where(:uid => user[:office_id]).first

        u.uid                     = user[:id]
        u.client_id               = 1
        u.password                = "12345"
        u.password_confirmation   = "12345"
        u.office_id               = office.present? ? office.id : nil
        u.first_name              = user[:fist_name]
        u.last_name               = user[:last_name]
        u.business_phone          = user[:phone]
        u.mobile_phone            = user[:mobile_phone]
        # u.picture                 = open("https://randomuser.me/api/portraits/men/83.jpg")
        if u.save
        else
          JulesLogger.info u.errors.full_messages
        end
      end
    rescue Exception => e
      JulesLogger.info e.message
    end
  end


  ##============================================================##
  ## 3 : CPIX_Main
  ##============================================================##
  task :mains => :environment do
    begin
      ods = Roo::Spreadsheet.open("#{Rails.root}/import/CPIX_Property_20160914_Actifs.xlsx")
      ods.sheet('CPIX_Main').each(
        :cpix_id          => "CPIXNum",
        :area             => "CPIXArea",
        :sp1              => "SP1",
        :sp2              => "SP2",
        :street_direction => "StDirect",
        :street_type     => "StType",
        :city             => "City",
        :zipcode          => "PostalCode",
        :province         => "Province",
        :street_name      => "StName",
        :street_number    => "StNum",
        :longitude        => "Latitude",
        :latitude         => "Longitude",
        :status           => "Status"
        ) do |main|

        next if main[:status] != "A"

        ##============================================================##
        ## Broker
        ##============================================================##
        user = User.where(:uid =>main[:sp1]).first
        user = User.where(:uid =>main[:sp2]).first if user.blank?
        next if user.blank?


        ##============================================================##
        ## Listing
        ##============================================================##
        listing           =  Listing.where(:uid => main[:cpix_id]).first
        next if listing.blank?

        listing.user_id   = user.id
        listing.draft     = 0
        listing.save

        if listing.save
          # JulesLogger.info listing
        else
          JulesLogger.info listing.errors.full_messages
        end


        ##============================================================##
        ## Area
        ##============================================================##
        area    = Area.where(:slug =>main[:area]).first

        ##============================================================##
        ## Address
        ##============================================================##
        address = Address.where(:street_type => main[:street_type],:street_number => main[:street_number],:street_name =>main[:street_name], :locality => main[:city],:zipcode =>main[:zipcode]).first_or_create
        address.latitude                    = main[:latitude]
        address.longitude                   = main[:longitude]
        address.sublocality                 = nil
        address.administrative_area_level_2 = nil
        address.administrative_area_level_1 = main[:province]
        address.country                     = "CA"
        address.save

        JulesLogger.info "="*30
        JulesLogger.info address.id


        ##============================================================##
        ## Building
        ##============================================================##
        if listing.building.blank?
          building = Building.new
          building.save(:validate =>false)
          listing.update_attribute(:building_id,building.id)
        end
        building = listing.building

        building.area_id    = area.present? ? area.id : nil
        building.address_id = address.id
        if building.save
            # JulesLogger.info building
        else
          JulesLogger.info building.errors.full_messages
        end




      end
    rescue Exception => e
      JulesLogger.info e.message
      JulesLogger.info e.backtrace
    end
  end

  ##============================================================##
  ## 5 : CPIX_SubTable
  ##============================================================##
  task :submains => :environment do
    begin
      all_listings      = Listing.all.pluck(:id)
      listings_to_keep  = []

      ##============================================================##
      ##
      ##============================================================##
      ods = Roo::Spreadsheet.open("#{Rails.root}/import/CPIX_Property_20160914_Actifs.xlsx")
      ods.sheet('CPIX_SubTable').each(
        :uid          => 'CPIXNum',
        :classID      => "ClassID",
        :lease_form   => "LeaseForm",
        :status       => "Status",
        :type_id      => "TypeID",
        :zoning       => "Zoning",
        :assessYear   => "AssessYear",
        :busname      => "BusName",
        :floorNum     => "FloorNum",
        :grossBldg    => "GrossBldg",
        :landSq       => "LandSq",
        :leaseExpiry  => "LeaseExpiry",
        :leaseRate    => "LeaseRate",
        :listPrice    => "ListPrice",
        :opYears      => "OpYears",
        :pRemarks     => "PRemarks",
        :rRemarks     => "RRemarks"
        ) do |subtype|


        next if subtype[:status] != "A"

        listing   = Listing.where(:uid =>subtype[:uid]).first
        next if listing.blank?

        listings_to_keep << listing.id
        building = listing.building

        ##============================================================##
        ## Transaction
        ##============================================================##
        t = TransactionType.where(:slug => subtype[:classID]).first
        listing.transaction_type_id =  t.present? ? t.id : nil

        ##============================================================##
        ## LeaseType
        ##============================================================##
        l = LeaseType.where(:slug => subtype[:lease_form]).first
        listing.lease_type_id     = l.present? ? l.id : nil


        listing.listing_status_id = 1

        ##============================================================##
        ## MajorType
        ##============================================================##
        majortype = ListingMajorType.where(:slug => subtype[:type_id]).first || ListingMajorType.where(:slug => "Other").first
        listing.listing_major_type_id = majortype.present? ? majortype.id : nil


        ##============================================================##
        ## Zoning
        ##============================================================##
        z = Zoning.where(:name => subtype[:zoning]).first
        building.zoning_id = z.present? ? z.id : nil


        ##============================================================##
        ## assessYear
        ##============================================================##
        building.building_assessment_year = subtype[:assessYear]


        ##============================================================##
        ## business_name
        ##============================================================##
        listing.business_name = subtype[:busname]


        ##============================================================##
        ## nb_of_floor
        ##============================================================##
        building.nb_of_floor = subtype[:floorNum]


        ##============================================================##
        ## grossBldg
        ##============================================================##
        building.total_floor_area = subtype[:grossBldg]

        ##============================================================##
        ## landSq
        ##============================================================##
        building.land_total_size = subtype[:landSq]

        ##============================================================##
        ##
        ##============================================================##
        listing.current_lease_expiry = subtype[:leaseExpiry]

        ##============================================================##
        ## lease_price
        ##============================================================##
        listing.lease_price = subtype[:leaseRate]

        ##============================================================##
        ## listPrice
        ##============================================================##
        listing.list_price  = subtype[:listPrice]

        ##============================================================##
        ## opYears
        ##============================================================##
        listing.operating_since  = 2016 - subtype[:opYears].to_i  if subtype[:opYears].present?

        ##============================================================##
        ## pRemarks
        ##============================================================##
        listing.public_remarks   = subtype[:pRemarks]
        listing.private_remarks  = subtype[:rRemarks]


        if listing.save and building.save
          JulesLogger.info listing
        else
          JulesLogger.info listing.errors.full_messages   if listing.errors.present?
          JulesLogger.info building.errors.full_messages  if building.errors.present?
        end
      end

      ##============================================================##
      ## Remove olds
      ##============================================================##
      to_remove = all_listings-listings_to_keep
      JulesLogger.info "to_remove => #{to_remove}"
      to_remove.each do |id|
        list = Listing.where(:id => id).first
        list.delete if list.present?
      end

    rescue Exception => e
      JulesLogger.info e.message
      JulesLogger.info e.backtrace
    end
  end

  ##============================================================##
  ## 6 : Pictures
  ##============================================================##
  task :pictures => :environment do
    begin
      pic_folder = "#{Rails.root}/import/Images"

      Dir.foreach(pic_folder) do |folder|
        f = "#{pic_folder}/#{folder}"
        next if !File.directory?(f)

        JulesLogger.info "========"
        Dir.foreach(f) do |item|
          file = "#{f}/#{item}"
          next if item == '.' or item == '..' or ![".jpg","png"].include?(File.extname(file))

          basename  = File.basename(file,".*")
          uid       = basename[0..basename.length-2]
          listing   = Listing.where(:uid => uid).first
          if listing.present?
            p           = listing.photos.new
            p.image     = File.open(file)
            if p.save
              JulesLogger.info p
            else
              JulesLogger.info p.errors.full_messages
            end
          end
        end
      end
    rescue Exception => e
      JulesLogger.info e.message
      JulesLogger.info e.backtrace
    end



  end


end
