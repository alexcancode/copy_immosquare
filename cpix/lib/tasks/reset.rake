namespace :import do

  require 'roo'

  task :reset => :environment do
    puts "accesses"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE accesses")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/AccessType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      Access.create(:name=>row[1])
      puts row
    end

    puts "appliances"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE appliances")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Appliance.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Appliance.create(:name=>row[1])
      puts row
    end

    puts "architecture style"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE architecture_styles")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/ArchitectureStyle.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      ArchitectureStyle.create(:name=>row[1])
      puts row
    end

    puts "attached style"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE attached_styles")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/AttachedStyle.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      AttachedStyle.create(:name=>row[1])
      puts row
    end

    puts "basement development"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE basement_developments")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/BasementDevelopment.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      BasementDevelopment.create(:name=>row[1])
      puts row
    end

    puts "basement type"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE basement_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/BasementType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      BasementType.create(:name=>row[1])
      puts row
    end

    puts "boma"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE bomas")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/BOMA.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Boma.create(:name=>row[1])
      puts row
    end

    puts "BuildingType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE building_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/BuildingType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      BuildingType.create(:name=>row[1])
      puts row
    end

    puts "BuildingAmenity"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE building_amenities")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/BuildingAmenities.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      BuildingAmenity.create(:name=>row[1])
      puts row
    end

    puts "Ceiling"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE ceilings")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Ceiling.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Ceiling.create(:name=>row[1])
      puts row
    end

    puts "Communication"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE communications")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Communications.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Communication.create(:name=>row[1])
      puts row
    end

    puts "ConstructionStatus"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE construction_statuses")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/ConstructionStatus.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      ConstructionStatus.create(:name=>row[1])
      puts row
    end

    puts "ConstructionType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE construction_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/ConstructionType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      ConstructionType.create(:name=>row[1])
      puts row
    end

    puts "Cooling"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE coolings")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Cooling.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Cooling.create(:name=>row[1])
      puts row
    end

    puts "DocumentAvailability"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE document_availabilities")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/DocumentAvailability.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      DocumentAvailability.create(:name=>row[1])
      puts row
    end

    puts "Documents"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE document_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Documents.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      DocumentType.create(:name=>row[1])
      puts row
    end

    puts "Elevators"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE elevators")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Elevators.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Elevator.create(:name=>row[1])
      puts row
    end

    puts "ExteriorFinish"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE exterior_finishes")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/ExteriorFinish.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      ExteriorFinish.create(:name=>row[1])
      puts row
    end

    # puts "FarmType"
    # ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    # ActiveRecord::Base.connection.execute("TRUNCATE farm_types")
    # ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    # csv_text = File.read("#{Rails.root}/tmp/access/FarmType.csv")
    # csv = CSV.parse(csv_text, :headers => true)
    # csv.each do |row|
    #   ##============================================================##
    #   ## Method #1
    #   ##============================================================##
    #   FarmType.create(:name=>row[1])
    #   puts row
    # end

    puts "Fence"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE fence_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Fence.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      FenceType.create(:name=>row[1])
      puts row
    end

    puts "FirePlaceFuel"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE fireplace_fuels")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/FirePlaceFuel.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      FireplaceFuel.create(:name=>row[1])
      puts row
    end

    puts "FirePlaceType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE fireplace_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/FirePlaceType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      FireplaceType.create(:name=>row[1])
      puts row
    end

    puts "Flooring"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE floorings")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Flooring.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Flooring.create(:name=>row[1])
      puts row
    end

    puts "Foundation"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE foundations")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Foundation.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Foundation.create(:name=>row[1])
      puts row
    end

    puts "FranchiseType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE franchise_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/FranchiseType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      FranchiseType.create(:name=>row[1])
      puts row
    end

    puts "Frontson"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE frontsons")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Frontson.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Frontson.create(:name=>row[1])
      puts row
    end

    puts "GreenBuilding"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE greens")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/GreenBuilding.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Green.create(:name=>row[1])
      puts row
    end

    puts "HeatingFuel"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE heating_fuels")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/HeatingFuel.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      HeatingFuel.create(:name=>row[1])
      puts row
    end

    puts "HeatingType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE heating_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/HeatingType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      HeatingType.create(:name=>row[1])
      puts row
    end

    puts "Irrigation"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE irrigation_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Irrigation.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      IrrigationType.create(:name=>row[1])
      puts row
    end

    puts "LandAmenities"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE land_amenities")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LandAmenities.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      LandAmenity.create(:name=>row[1])
      puts row
    end

    puts "LandRights"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE landrights")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LandRights.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Landright.create(:name=>row[1])
      puts row
    end

    puts "Landscaping"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE landscapings")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Landscaping.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Landscaping.create(:name=>row[1])
      puts row
    end

    puts "LandType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE land_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LandType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      LandType.create(:name=>row[1])
      puts row
    end

    puts "LeaseAdditionalCosts"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE lease_additional_costs")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LeaseAdditionalCosts.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      LeaseAdditionalCost.create(:name=>row[1])
      puts row
    end

    puts "LeaseIncludes"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE lease_includes")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LeaseIncludes.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      LeaseInclude.create(:name=>row[1])
      puts row
    end

    puts "LeaseTerm"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE lease_terms")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LeaseTerm.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      LeaseTerm.create(:name=>row[1])
      puts row
    end

    puts "LeaseType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE lease_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LeaseType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      LeaseType.create(:name=>row[1])
      puts row
    end

    puts "Leed"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE leeds")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LEEDCategory.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Leed.create(:name=>row[1])
      puts row
    end

    puts "LEEDWELL"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE leedwells")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LEEDWELL.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Leedwell.create(:name=>row[1])
      puts row
    end

    puts "LEEDWELL"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE leedwells")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LEEDWELL.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Leedwell.create(:name=>row[1])
      puts row
    end

    puts "ListingMajorType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE listing_major_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/ListingMajorType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    major_type = Array.new #this will be used for ListingMinorType
    csv.each do |row|
      major_type << {:uid => row[0].to_i, name: row[1]} #this will be used for ListingMinorType
      ##============================================================##
      ## Method #1
      ##============================================================##
      ListingMajorType.create(:name=>row[1])
      puts row
    end
    # puts major_type

    puts "ListingMinorType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE listing_minor_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/ListingMinorType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      m = major_type.select { |major| major[:uid] == row[0].to_i }
      major = ListingMajorType.where(:name => m.first[:name]).first
      ListingMinorType.create(:listing_major_type_id=>major.id,:name => row[2])
      puts row
    end


    puts "ListingSubType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE listing_sub_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/ListingSubType.csv")
    csv_text_mt = File.read("#{Rails.root}/tmp/access/ListingMinorType.csv")
    mino = Array.new
    csv = CSV.parse(csv_text, :headers => true)
    csv_mt = CSV.parse(csv_text_mt, :headers => true)
    csv_mt.each { |row| mino << {:uid => row[1].to_i, name: row[2]}}
    p mino
    csv.each do |row|
      mi = mino.select { |min| min[:uid] == row [0].to_i }
      minor = ListingMinorType.where(:name => mi.first[:name]).first
      ##============================================================##
      ## Method #1
      ##============================================================##
      ListingSubType.create(name: row[2], listing_minor_type_id: minor.id)
      puts row
    end

    puts "LoadingType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE loading_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/LoadingType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      LoadingType.create(:name=>row[1])
      puts row
    end

    puts "Municipality"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE municipalities")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Municipality.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Municipality.create(name: row[1])
      puts row
    end

    puts "Parking"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE parkings")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Parking.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Parking.create(:name=>row[1])
      puts row
    end

    puts "Possession_OccupancyCategory"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE possession_occupations")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Possession_OccupancyCategory.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      PossessionOccupation.create(:name=>row[1])
      puts row
    end

    puts "Power"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE powers")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Power.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Power.create(:name=>row[1])
      puts row
    end

    puts "ProtectionSystems"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE protection_systems")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/ProtectionSystems.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      ProtectionSystem.create(:name=>row[1])
      puts row
    end

    puts "RoadType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE road_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/RoadType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      RoadType.create(:name=>row[1])
      puts row
    end

    puts "Roof"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE roofs")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Roof.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Roof.create(name: row[1])
      puts row
    end

    puts "Rooms"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE rooms")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Rooms.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Room.create(:name=>row[1])
      puts row
    end

    puts "SaleType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE sale_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/SaleType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      SaleType.create(:name=>row[1])
      puts row
    end

    puts "Sewage"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE sewages")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Sewage.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Sewage.create(:name=>row[1])
      puts row
    end

    puts "Signage"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE signages")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Signage.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Signage.create(:name=>row[1])
      puts row
    end

    puts "SiteFeatures"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE site_features")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/SiteFeatures.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      SiteFeature.create(:name=>row[1])
      puts row
    end

    puts "Storage"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE storages")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Storage.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Storage.create(:name=>row[1])
      puts row
    end

    puts "StoreFront"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE storefronts")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/StoreFront.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Storefront.create(:name=>row[1])
      puts row
    end

    puts "StructureType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE structure_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/StructureType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      StructureType.create(:name=>row[1])
      puts row
    end

    puts "TitleType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE title_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/TitleType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      TitleType.create(:name=>row[1])
      puts row
    end

    puts "TransactionType"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE transaction_types")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/TransactionType.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      TransactionType.create(:name=>row[1])
      puts row
    end

    puts "UFFI"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE uffis")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/UFFI.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Uffi.create(:name=>row[1])
      puts row
    end

    puts "Utility"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE utilities")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/UtilityAvailability.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Utility.create(:name=>row[1])
      puts row
    end

    puts "View"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE views")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/View.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      View.create(:name=>row[1])
      puts row
    end

    puts "Water"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE waters")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Water.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Water.create(:name=>row[1])
      puts row
    end

    puts "Waterfront"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE waterfronts")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Waterfront.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      ##============================================================##
      ## Method #1
      ##============================================================##
      Waterfront.create(:name=>row[1])
      puts row
    end

    puts "Zoning"
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 0")
    ActiveRecord::Base.connection.execute("TRUNCATE zonings")
    ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS = 1")
    csv_text = File.read("#{Rails.root}/tmp/access/Zoning.csv")
    csv_text_m = File.read("#{Rails.root}/tmp/access/Municipality.csv")
    muni = Array.new
    csv = CSV.parse(csv_text, :headers => true)
    csv_m = CSV.parse(csv_text_m, :headers => true)
    csv_m.each { |row| muni << {:uid => row[0].to_i, name: row[1]}}
    csv.each do |row|
      mu = muni.select { |mun| mun[:uid] == row [0].to_i }
      munic = Municipality.where(:name => mu.first[:name]).first
      ##============================================================##
      ## Method #1
      ##============================================================##
      Zoning.create(name: row[2], municipality_id: munic.id)
      puts row
    end
  end




end
