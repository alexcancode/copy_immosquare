class ContactMailerPreview < ActionMailer::Preview

  def send_contact_mail
    ContactMailer.send_contact_mail(Contact.first)
  end

  def error
    ContactMailer.error("ici", "lala")
  end

end