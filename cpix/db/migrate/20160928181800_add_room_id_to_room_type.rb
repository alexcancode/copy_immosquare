class AddRoomIdToRoomType < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :rooms, { :column => :room_type_id }
    remove_column :rooms, :room_type_id
    add_reference :room_types, :room, index: true
    add_foreign_key :room_types, :rooms
  end
end
