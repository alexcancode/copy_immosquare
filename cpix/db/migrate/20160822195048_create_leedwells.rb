class CreateLeedwells < ActiveRecord::Migration[5.0]
  def change
    create_table :leedwells do |t|
      t.text :name

      t.timestamps
    end
  end
end
