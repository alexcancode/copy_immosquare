class CreateTaxes < ActiveRecord::Migration[5.0]
  def change
    create_table :taxes do |t|
      t.string :tax_roll_number
      t.integer :tax_year
      t.integer :building_assessment_year
      t.money :building_assessment_value
      t.money :building_annual_tax
      t.integer :land_assessment_year
      t.money :land_assessment_value
      t.money :land_annual_tax
      t.text :building_tax_description

      t.timestamps
    end
  end
end
