class CreateZonings < ActiveRecord::Migration[5.0]
  def change
    create_table :zonings do |t|
      t.text :name

      t.timestamps
    end
  end
end
