class CreateCeilings < ActiveRecord::Migration[5.0]
  def change
    create_table :ceilings do |t|
      t.text :name

      t.timestamps
    end
  end
end
