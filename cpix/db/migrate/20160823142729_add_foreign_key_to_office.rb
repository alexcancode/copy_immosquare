class AddForeignKeyToOffice < ActiveRecord::Migration[5.0]
  def change
  	add_reference :offices, :address, index: true
  	add_foreign_key :offices, :addresses
  end
end