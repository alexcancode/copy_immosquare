class AddForeignKeyToBuildingPartThree < ActiveRecord::Migration[5.0]
  def change
  	add_reference :buildings, :area, index: true
  	add_foreign_key :buildings, :areas
  	add_reference :buildings, :zoning, index: true
  	add_foreign_key :buildings, :zonings
	add_reference :buildings, :access, index: true
  	add_foreign_key :buildings, :accesses
	add_reference :buildings, :landright, index: true
  	add_foreign_key :buildings, :landrights
  	add_reference :buildings, :waterfront, index: true
  	add_foreign_key :buildings, :waterfronts
	add_reference :buildings, :fence_type, index: true
  	add_foreign_key :buildings, :fence_types
	add_reference :buildings, :road_type, index: true
  	add_foreign_key :buildings, :road_types
  	add_reference :buildings, :land_type, index: true
  	add_foreign_key :buildings, :land_types
	add_reference :buildings, :utility, index: true
  	add_foreign_key :buildings, :utilities
	add_reference :buildings, :view, index: true
  	add_foreign_key :buildings, :views
	add_reference :buildings, :irrigation_type, index: true
  	add_foreign_key :buildings, :irrigation_types
	add_reference :buildings, :building_amenity, index: true
  	add_foreign_key :buildings, :building_amenities
	add_reference :buildings, :landscaping, index: true
  	add_foreign_key :buildings, :landscapings
	add_reference :buildings, :document, index: true
  	add_foreign_key :buildings, :documents
	add_reference :buildings, :photo, index: true
  	add_foreign_key :buildings, :photos
	add_reference :buildings, :land_amenity, index: true
  	add_foreign_key :buildings, :land_amenities
  end
end
