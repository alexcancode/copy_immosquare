class AddForeignKeyToBuildingPartFour < ActiveRecord::Migration[5.0]
  def change
    add_reference :buildings, :communication, index: true
    add_foreign_key :buildings, :communications
  end
end
