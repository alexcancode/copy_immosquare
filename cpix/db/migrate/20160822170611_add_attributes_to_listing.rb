class AddAttributesToListing < ActiveRecord::Migration[5.0]
  def change
    add_money :listings, :list_price
    add_money :listings, :sales_tax
 	add_column :listings, :inventory_included, :boolean
 	add_column :listings, :inventory_description, :text
 	add_money :listings, :inventory_value
 	add_column :listings, :franchisor_name, :string
 	add_column :listings, :franchise_name, :string
 	add_column :listings, :public_remarks, :text
 	add_column :listings, :private_remarks, :text
 	add_column :listings, :finance_agreement_for_sale, :boolean
 	add_column :listings, :finance_assume_with_qualification, :boolean
 	add_column :listings, :finance_clear_title, :boolean
 	add_column :listings, :finance_lease_with_option_to_buy, :boolean
 	add_column :listings, :finance_mortgage_must_be_assumed, :boolean
 	add_column :listings, :finance_mortgage_must_be_paid_off, :boolean
 	add_column :listings, :finance_option_to_renew, :boolean
 	add_column :listings, :finance_buyer_to_finance, :boolean
 	add_column :listings, :finance_sale_lease_back, :boolean
 	add_column :listings, :finance_sale_lease_option, :boolean
 	add_column :listings, :finance_seller_may_buy_down_mortgage, :boolean
 	add_column :listings, :finance_seller_may_carry_mortgage, :boolean
 	add_column :listings, :finance_trade_or_exchange, :boolean
 	add_column :listings, :finance_vendor_take_back, :boolean
 	add_column :listings, :finance_notes, :text
 	add_column :listings, :ddf_distribution, :boolean
 	add_column :listings, :wordl_prop_display, :boolean
 	add_column :listings, :public_display_address, :string
 	add_column :listings, :feature_sheet_url, :string
 	add_column :listings, :virtual_tour_url, :string
 	add_column :listings, :sale_brochure_url, :string
 	add_column :listings, :video_tour_url, :string
 	add_column :listings, :prop_map_url, :string
 	add_column :listings, :listing_measurement_unit, :integer
 	add_column :listings, :current_lease_expiry, :date
 	add_money :listings, :lease_price
 	add_column :listings, :payment_frequency, :integer
 	add_column :listings, :lease_area_size, :decimal
 	add_money :listings, :total_monthly_lease
 	add_column :listings, :lease_details, :text
 	add_column :listings, :leasehold_improvements_details, :text
 	add_money :listings, :additional_costs_amount
 	add_column :listings, :additional_costs_payment_freq, :string
 	add_column :listings, :additional_costs_details, :text
 	add_column :listings, :landlord, :string
 	add_column :listings, :property_mngt_company, :string
 	add_column :listings, :property_mngt_contact, :string
 	add_column :listings, :property_mngt_address, :string
 	add_column :listings, :property_mngt_phone, :string
  end
end
