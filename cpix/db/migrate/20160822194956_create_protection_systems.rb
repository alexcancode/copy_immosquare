class CreateProtectionSystems < ActiveRecord::Migration[5.0]
  def change
    create_table :protection_systems do |t|
      t.text :name

      t.timestamps
    end
  end
end
