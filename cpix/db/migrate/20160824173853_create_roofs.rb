class CreateRoofs < ActiveRecord::Migration[5.0]
  def change
    create_table :roofs do |t|
      t.text :name

      t.timestamps
    end
  end
end
