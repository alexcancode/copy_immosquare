class AddForeignKeyToSeller < ActiveRecord::Migration[5.0]
  def change
  	add_reference :sellers, :listing, index: true
  	add_foreign_key :sellers, :listings
  	add_reference :sellers, :address, index: true
  	add_foreign_key :sellers, :addresses
  end
end
