class AddAttributesToBuilding < ActiveRecord::Migration[5.0]
  def change
    add_column :buildings, :prop_legal_address_lot, :string
    add_column :buildings, :prop_legal_address_block, :string
    add_column :buildings, :prop_legal_address_plan, :string
    add_column :buildings, :prop_legal_address_meridian, :string
    add_column :buildings, :prop_legal_address_range, :string
    add_column :buildings, :prop_legal_address_township, :string
    add_column :buildings, :prop_legal_address_section, :string
    add_column :buildings, :prop_legal_address_quarter, :string
    add_column :buildings, :prop_legal_address_subdivision, :string
    add_column :buildings, :prop_legal_address_freeform, :text
    add_column :buildings, :area_sub_area, :string
    add_column :buildings, :zoning_description, :text
    add_column :buildings, :mapping_lat, :float
    add_column :buildings, :mapping_long, :float
    add_column :buildings, :utilities_availability, :boolean
    add_column :buildings, :irrigation_coverage, :decimal
    add_column :buildings, :land_total_size, :decimal
    add_column :buildings, :land_depth, :decimal
    add_column :buildings, :land_width, :decimal
    add_column :buildings, :land_size_description, :text
    add_column :buildings, :land_frontage_measurement, :decimal
    add_column :buildings, :land_use, :text
    add_column :buildings, :land_amount, :integer
    add_column :buildings, :fenced_area_measurement, :decimal
    add_column :buildings, :land_assessment_year, :integer
    add_money :buildings, :land_assessment_value
    add_money :buildings, :land_annual_tax
  end
end
