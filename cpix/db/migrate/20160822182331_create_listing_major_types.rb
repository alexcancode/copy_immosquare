class CreateListingMajorTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :listing_major_types do |t|
      t.text :name

      t.timestamps
    end
  end
end
