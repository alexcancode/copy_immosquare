class CreateRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :rooms do |t|
      t.integer :listing_id
      t.text :name
      t.decimal :room_size
      t.decimal :room_length
      t.decimal :room_width
      t.integer :room_level

      t.timestamps
    end
  end
end
