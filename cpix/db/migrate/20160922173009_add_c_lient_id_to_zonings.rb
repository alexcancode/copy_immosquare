class AddCLientIdToZonings < ActiveRecord::Migration[5.0]
  def change
    add_column :zonings, :client_id, :integer, :after =>:id
  end
end
