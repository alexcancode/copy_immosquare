class AddForeignKeyToBuildingPartOne < ActiveRecord::Migration[5.0]
  def change
  	add_reference :buildings, :address, index: true
  	add_foreign_key :buildings, :addresses
  	add_reference :buildings, :architecture_style, index: true
  	add_foreign_key :buildings, :architecture_styles
  	add_reference :buildings, :structure_type, index: true
  	add_foreign_key :buildings, :structure_types
  	add_reference :buildings, :attached_style, index: true
  	add_foreign_key :buildings, :attached_styles
  	add_reference :buildings, :construction_status, index: true
  	add_foreign_key :buildings, :construction_statuses
  	add_reference :buildings, :construction_type, index: true
  	add_foreign_key :buildings, :construction_types
  	add_reference :buildings, :elevator, index: true
  	add_foreign_key :buildings, :elevators
  	add_reference :buildings, :signage, index: true
  	add_foreign_key :buildings, :signages
  	add_reference :buildings, :parking, index: true
  	add_foreign_key :buildings, :parkings
  end
end
