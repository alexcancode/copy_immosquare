class CreateAttachedStyles < ActiveRecord::Migration[5.0]
  def change
    create_table :attached_styles do |t|
      t.text :name

      t.timestamps
    end
  end
end
