class CreateDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :documents do |t|
      t.integer :documentable_id
      t.string :documentable_type
      t.datetime :document_date
      t.attachment :document_file

      t.timestamps
    end
  add_index :documents, [:documentable_id, :documentable_type]
  end
end
