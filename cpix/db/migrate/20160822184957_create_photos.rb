class CreatePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :photos do |t|
      t.integer :photoable_id
      t.integer :photoable_type
      t.string :name
      t.text :description
      t.boolean :primary
      t.attachment :image

      t.timestamps
    end

    add_index :photos, [:photoable_id, :photoable_type]
  end
end

# A Second way of doing polymorphic association
  # def change
  #   create_table :photos do |t|
  #     t.string :name
  #     t.references :photoable, polymorphic: true, index: true
  #     t.text :name
  #     t.boolean :primary
  #     t.attachment :image

  #     t.timestamps
  #   end
  # end
