class ChangeDocColType < ActiveRecord::Migration[5.0]
def up
    change_column :documents, :document_date, :date
end

def down
    # This might cause trouble if you have strings longer
    # than 255 characters.
    change_column :documents, :document_date, :datetime
end
end
