class CreateFloorings < ActiveRecord::Migration[5.0]
  def change
    create_table :floorings do |t|
      t.text :name

      t.timestamps
    end
  end
end
