class AddColorsToListingsMajorTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :listing_major_types, :color, :string, :default => "#000000",:after => :name
  end
end
