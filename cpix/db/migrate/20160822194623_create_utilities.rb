class CreateUtilities < ActiveRecord::Migration[5.0]
  def change
    create_table :utilities do |t|
      t.text :name

      t.timestamps
    end
  end
end
