class RemoveTaxFromBuilding < ActiveRecord::Migration[5.0]
  def change
    remove_column :buildings, :building_tax_roll_number
    remove_column :buildings, :building_tax_year
    remove_column :buildings, :building_tax_description
    remove_column :buildings, :building_description
    remove_column :buildings, :building_assessment_year
    remove_column :buildings, :building_annual_tax_cents
    remove_column :buildings, :building_annual_tax_currency
    remove_column :buildings, :building_assessment_value_cents
    remove_column :buildings, :building_assessment_value_currency
    remove_column :buildings, :land_assessment_year
    remove_column :buildings, :land_assessment_value_cents
    remove_column :buildings, :land_assessment_value_currency
    remove_column :buildings, :land_annual_tax_cents
    remove_column :buildings, :land_annual_tax_currency
  end
end
