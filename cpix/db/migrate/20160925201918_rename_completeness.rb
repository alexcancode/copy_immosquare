class RenameCompleteness < ActiveRecord::Migration[5.0]
  def change
    rename_column :listings, :listing_completness, :listing_completeness
  end
end
