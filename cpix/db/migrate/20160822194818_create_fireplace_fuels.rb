class CreateFireplaceFuels < ActiveRecord::Migration[5.0]
  def change
    create_table :fireplace_fuels do |t|
      t.text :name

      t.timestamps
    end
  end
end
