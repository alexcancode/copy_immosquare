class CreateListings < ActiveRecord::Migration[5.0]
  def change
    create_table :listings do |t|
      t.text :description
      t.string :business_name
      t.date :operating_since
      t.date :list_date
      t.date :listing_expiry_date
      t.string :sellers_interest
      t.string :sellers_right_reserved
      t.string :limited_service_contract
      t.date :possession_occupation_date 
      t.decimal :selling_commission_rate 
      t.money :selling_commission_amount

      t.timestamps
    end
  end
end
