class CreateOffices < ActiveRecord::Migration[5.0]
  def change
    create_table :offices do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.integer :phone_ext

      t.timestamps
    end
  end
end
