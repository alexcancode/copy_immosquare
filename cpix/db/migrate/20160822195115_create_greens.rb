class CreateGreens < ActiveRecord::Migration[5.0]
  def change
    create_table :greens do |t|
      t.text :name

      t.timestamps
    end
  end
end
