class UpdateListingOrders2 < ActiveRecord::Migration[5.0]
  def change
    remove_reference :listings, :address
    remove_column :listings, :room_id
    remove_column :listings, :document_id
    change_column :listings, :list_price_cents,  :integer ,:after => :currency
    change_column :listings, :lease_price_cents,  :integer ,:after => :currency
    change_column :listings, :lease_type_id,  :integer ,:after => :transaction_type_id
  end
end
