class CreateMajorTransactionTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_major_types do |t|
      t.string :name
      t.timestamps
    end
  end
end
