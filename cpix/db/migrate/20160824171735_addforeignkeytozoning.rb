class Addforeignkeytozoning < ActiveRecord::Migration[5.0]
  def change
  	add_reference :zonings, :municipality, index: true
    add_foreign_key :zonings, :municipalities
  end
end
