class CreateFrontsons < ActiveRecord::Migration[5.0]
  def change
    create_table :frontsons do |t|
      t.text :name

      t.timestamps
    end
  end
end
