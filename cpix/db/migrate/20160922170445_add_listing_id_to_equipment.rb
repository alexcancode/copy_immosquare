class AddListingIdToEquipment < ActiveRecord::Migration[5.0]
  def change
    add_reference :equipment, :listing, index: true
    add_foreign_key :equipment, :listings
  end
end
