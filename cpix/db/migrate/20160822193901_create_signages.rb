class CreateSignages < ActiveRecord::Migration[5.0]
  def change
    create_table :signages do |t|
      t.text :name

      t.timestamps
    end
  end
end
