class RenameColumnToBuildings < ActiveRecord::Migration[5.0]
  def change
    rename_column :buildings, :numeric_of_floor, :nb_of_floor
  end
end
