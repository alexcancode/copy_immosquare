class CreateLeaseTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :lease_types do |t|
      t.text :name

      t.timestamps
    end
  end
end
