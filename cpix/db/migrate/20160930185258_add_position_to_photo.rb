class AddPositionToPhoto < ActiveRecord::Migration[5.0]
  def change
    add_column :photos, :position, :integer, after: :id
  end
end
