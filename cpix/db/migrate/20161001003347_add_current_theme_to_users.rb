class AddCurrentThemeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :current_map_theme, :string, :default => :dark
  end
end
