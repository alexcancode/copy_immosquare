class AddSlugToLeaseTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :lease_types, :slug, :string, :after => :id
  end
end
