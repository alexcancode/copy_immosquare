class AddSLugToTransactionType < ActiveRecord::Migration[5.0]
  def change
    add_column :transaction_types, :slug, :string, :after => :id
  end
end
