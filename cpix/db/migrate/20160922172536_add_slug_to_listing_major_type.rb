class AddSlugToListingMajorType < ActiveRecord::Migration[5.0]
  def change
    add_column :listing_major_types, :slug, :string, :after => :id
  end
end
