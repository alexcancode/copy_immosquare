class CreateSiteFeatures < ActiveRecord::Migration[5.0]
  def change
    create_table :site_features do |t|
      t.text :name

      t.timestamps
    end
  end
end
