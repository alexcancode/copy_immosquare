class CreateParkings < ActiveRecord::Migration[5.0]
  def change
    create_table :parkings do |t|
      t.text :name

      t.timestamps
    end
  end
end
