class AddDraftToListings < ActiveRecord::Migration[5.0]
  def change
    add_column :listings, :draft, :integer, :default => 1
  end
end
