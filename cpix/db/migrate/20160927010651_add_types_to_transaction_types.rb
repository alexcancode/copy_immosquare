class AddTypesToTransactionTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :transaction_types, :major_type, :integer, :default => 1, :after =>:slug
  end
end
