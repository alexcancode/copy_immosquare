class AddListingCompletenessToListings < ActiveRecord::Migration[5.0]
  def change
    add_column :listings, :listing_completness, :integer, :default => 0
  end
end
