class AddEquipmentToListing < ActiveRecord::Migration[5.0]
  def change
  	add_reference :listings, :equipment, index: true
  	add_foreign_key :listings, :equipment
  end
end