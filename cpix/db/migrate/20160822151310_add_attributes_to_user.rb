class AddAttributesToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :business_phone, :string
    add_column :users, :business_phone_ext, :string
    add_column :users, :mobile_phone, :string
    add_column :users, :linkedin_link, :string
  end
end