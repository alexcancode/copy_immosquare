class AddRoomToListing < ActiveRecord::Migration[5.0]
  def change
  	add_reference :listings, :room, index: true
  	add_foreign_key :listings, :rooms
  end
end
