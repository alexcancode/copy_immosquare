class AddUidtoListings < ActiveRecord::Migration[5.0]
  def change
    add_column :listings, :uid, :string, :after => :id
    add_column :listings, :client_id, :integer, :after => :uid , :default => 1
  end
end
