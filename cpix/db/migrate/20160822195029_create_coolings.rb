class CreateCoolings < ActiveRecord::Migration[5.0]
  def change
    create_table :coolings do |t|
      t.text :name

      t.timestamps
    end
  end
end
