class CreateWaterfronts < ActiveRecord::Migration[5.0]
  def change
    create_table :waterfronts do |t|
      t.text :name

      t.timestamps
    end
  end
end
