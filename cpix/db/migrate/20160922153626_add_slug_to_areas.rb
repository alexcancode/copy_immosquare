class AddSlugToAreas < ActiveRecord::Migration[5.0]
  def change
    add_column :areas, :slug, :string, :after => :id
  end
end
