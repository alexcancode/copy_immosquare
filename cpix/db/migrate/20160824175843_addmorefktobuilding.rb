class Addmorefktobuilding < ActiveRecord::Migration[5.0]
  def change
  	add_reference :buildings, :exterior_finish, index: true
  	add_foreign_key :buildings, :exterior_finishes
  	add_reference :buildings, :building_type, index: true
  	add_foreign_key :buildings, :building_types
  	add_reference :buildings, :foundation, index: true
  	add_foreign_key :buildings, :foundations
  	add_reference :buildings, :municipality, index: true
  	add_foreign_key :buildings, :municipalities
  	add_reference :buildings, :roof, index: true
  	add_foreign_key :buildings, :roofs
  end
end
