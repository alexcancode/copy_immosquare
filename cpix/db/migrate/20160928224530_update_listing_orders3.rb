class UpdateListingOrders3 < ActiveRecord::Migration[5.0]
  def change
    change_column :listings, :currency, :string, :default => "CAD"
    change_column :listings, :lease_price_cents, :integer, :default => nil
    change_column :listings, :list_price_cents, :integer, :default => nil
    change_column :listings, :selling_commission_amount_cents, :integer, :default => nil
    change_column :listings, :sales_tax_cents, :integer, :default => nil, :after => :selling_commission_amount_cents
    change_column :listings, :inventory_value_cents, :integer, :default => nil, :after => :sales_tax_cents
    change_column :listings, :total_monthly_lease_cents, :integer, :default => nil, :after => :inventory_value_cents
    change_column :listings, :additional_costs_amount_cents, :integer, :default => nil, :after => :total_monthly_lease_cents




  end
end
