class Addlistingsubtypetolistingminortype < ActiveRecord::Migration[5.0]
  def change
  	add_reference :listing_sub_types, :listing_minor_type, index: true, :after => :id
  	add_foreign_key :listing_sub_types, :listing_minor_types
  end
end
