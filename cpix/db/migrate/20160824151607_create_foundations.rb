class CreateFoundations < ActiveRecord::Migration[5.0]
  def change
    create_table :foundations do |t|
      t.text :name

      t.timestamps
    end
  end
end
