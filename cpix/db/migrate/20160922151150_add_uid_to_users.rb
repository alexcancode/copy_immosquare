class AddUidToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :uid, :string, :after => :id
    add_column :users, :client_id, :integer, :after => :uid , :default => 1
  end
end
