class AddForeignKeyToListingPartOne < ActiveRecord::Migration[5.0]
  def change
  	add_reference :listings, :address, index: true
  	add_foreign_key :listings, :addresses
  	add_reference :listings, :building, index: true
  	add_foreign_key :listings, :buildings
  	add_reference :listings, :listing_major_type, index: true
  	add_foreign_key :listings, :listing_major_types
  	add_reference :listings, :listing_minor_type, index: true
  	add_foreign_key :listings, :listing_minor_types
  	add_reference :listings, :listing_sub_type, index: true
  	add_foreign_key :listings, :listing_sub_types
  	add_reference :listings, :transaction_type, index: true
  	add_foreign_key :listings, :transaction_types
  	add_reference :listings, :user, index: true
  	add_foreign_key :listings, :users
  	add_reference :listings, :sale_type, index: true
  	add_foreign_key :listings, :sale_types
  	add_reference :listings, :possession_occupation, index: true
  	add_foreign_key :listings, :possession_occupations
  	add_reference :listings, :title_type, index: true
  	add_foreign_key :listings, :title_types
  	add_reference :listings, :franchise_type, index: true
  	add_foreign_key :listings, :franchise_types
  	add_reference :listings, :listing_status, index: true
  	add_foreign_key :listings, :listing_statuses
  	add_reference :listings, :document, index: true
  	add_foreign_key :listings, :documents
  	add_reference :listings, :lease_type, index: true
  	add_foreign_key :listings, :lease_types
  	add_reference :listings, :lease_term, index: true
  	add_foreign_key :listings, :lease_terms
  	add_reference :listings, :lease_include, index: true
  	add_foreign_key :listings, :lease_includes
  	add_reference :listings, :lease_additional_cost, index: true
  	add_foreign_key :listings, :lease_additional_costs
  end
end
