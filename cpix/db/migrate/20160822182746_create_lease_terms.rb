class CreateLeaseTerms < ActiveRecord::Migration[5.0]
  def change
    create_table :lease_terms do |t|
      t.text :name

      t.timestamps
    end
  end
end
