class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
	  t.string :civic
      t.string :address_one
      t.string :address_two
      t.string :postal_code
      t.string :municipality
      t.string :state_province
      t.string :country
      
      t.timestamps
    end
  end
end
