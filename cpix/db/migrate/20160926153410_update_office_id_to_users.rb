class UpdateOfficeIdToUsers < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :office_id, :integer, :after => :client_id
  end
end
