class UpdateOperatingSinceToListings < ActiveRecord::Migration[5.0]
  def change
    change_column :listings, :operating_since, :integer
  end
end
