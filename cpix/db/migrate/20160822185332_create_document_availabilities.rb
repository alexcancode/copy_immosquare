class CreateDocumentAvailabilities < ActiveRecord::Migration[5.0]
  def change
    create_table :document_availabilities do |t|
      t.text :name

      t.timestamps
    end
  end
end
