class AddUidToOffices < ActiveRecord::Migration[5.0]
  def change
    add_column :offices, :uid, :string, :after => :id
    add_column :offices, :client_id, :integer, :after => :uid, :default => 1
  end
end
