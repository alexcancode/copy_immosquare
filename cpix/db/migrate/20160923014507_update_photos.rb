class UpdatePhotos < ActiveRecord::Migration[5.0]
  def change
    change_column :photos, :photoable_type, :string
  end
end
