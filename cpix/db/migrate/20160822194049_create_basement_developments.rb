class CreateBasementDevelopments < ActiveRecord::Migration[5.0]
  def change
    create_table :basement_developments do |t|
      t.text :name

      t.timestamps
    end
  end
end
