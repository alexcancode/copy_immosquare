class AddMajorTransactionTypeToTransactionTypes < ActiveRecord::Migration[5.0]
  def change
    add_reference :transaction_types, :transaction_major_type, foreign_key: false, :after => :id, :default=> 1
    remove_column :transaction_types, :major_type
  end
end
