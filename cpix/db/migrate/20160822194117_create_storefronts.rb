class CreateStorefronts < ActiveRecord::Migration[5.0]
  def change
    create_table :storefronts do |t|
      t.text :name

      t.timestamps
    end
  end
end
