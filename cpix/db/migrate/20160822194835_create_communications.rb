class CreateCommunications < ActiveRecord::Migration[5.0]
  def change
    create_table :communications do |t|
      t.text :name

      t.timestamps
    end
  end
end
