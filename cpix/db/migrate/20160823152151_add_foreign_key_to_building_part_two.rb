class AddForeignKeyToBuildingPartTwo < ActiveRecord::Migration[5.0]
  def change
  	add_reference :buildings, :uffi, index: true
    add_foreign_key :buildings, :uffis
    add_reference :buildings, :boma, index: true
    add_foreign_key :buildings, :bomas
    add_reference :buildings, :leed, index: true
    add_foreign_key :buildings, :leeds
    add_reference :buildings, :leedwell, index: true
    add_foreign_key :buildings, :leedwells
    add_reference :buildings, :green, index: true
    add_foreign_key :buildings, :greens
    add_reference :buildings, :frontson, index: true
    add_foreign_key :buildings, :frontsons
    add_reference :buildings, :basement_development, index: true
    add_foreign_key :buildings, :basement_developments
    add_reference :buildings, :basement_type, index: true
    add_foreign_key :buildings, :basement_types
    add_reference :buildings, :ceiling, index: true
    add_foreign_key :buildings, :ceilings
    add_reference :buildings, :cooling, index: true
    add_foreign_key :buildings, :coolings
    add_reference :buildings, :appliance, index: true
    add_foreign_key :buildings, :appliances
    add_reference :buildings, :protection_system, index: true
    add_foreign_key :buildings, :protection_systems
  	add_reference :buildings, :flooring, index: true
  	add_foreign_key :buildings, :floorings
  	add_reference :buildings, :heating_type, index: true
  	add_foreign_key :buildings, :heating_types
  	add_reference :buildings, :heating_fuel, index: true
  	add_foreign_key :buildings, :heating_fuels
  	add_reference :buildings, :loading_type, index: true
  	add_foreign_key :buildings, :loading_types
  	add_reference :buildings, :storage, index: true
  	add_foreign_key :buildings, :storages
  	add_reference :buildings, :power, index: true
  	add_foreign_key :buildings, :powers
  	add_reference :buildings, :sewage, index: true
  	add_foreign_key :buildings, :sewages
  	add_reference :buildings, :water, index: true
  	add_foreign_key :buildings, :waters
	add_reference :buildings, :site_feature, index: true
  	add_foreign_key :buildings, :site_features
	add_reference :buildings, :storefront, index: true
  	add_foreign_key :buildings, :storefronts
	add_reference :buildings, :fireplace_fuel, index: true
  	add_foreign_key :buildings, :fireplace_fuels
	add_reference :buildings, :fireplace_type, index: true
  	add_foreign_key :buildings, :fireplace_types
  end
end
    