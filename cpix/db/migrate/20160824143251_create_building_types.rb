class CreateBuildingTypes < ActiveRecord::Migration[5.0]
	def change
		create_table :building_types do |t|
			t.text :name

			t.timestamps
			
		end
	end
end
