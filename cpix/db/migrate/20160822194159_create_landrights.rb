class CreateLandrights < ActiveRecord::Migration[5.0]
  def change
    create_table :landrights do |t|
      t.text :name

      t.timestamps
    end
  end
end
