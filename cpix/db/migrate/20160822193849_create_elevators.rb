class CreateElevators < ActiveRecord::Migration[5.0]
  def change
    create_table :elevators do |t|
      t.text :name

      t.timestamps
    end
  end
end
