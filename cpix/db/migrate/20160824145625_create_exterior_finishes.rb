class CreateExteriorFinishes < ActiveRecord::Migration[5.0]
  def change
    create_table :exterior_finishes do |t|
      t.text :name

      t.timestamps
    end
  end
end
