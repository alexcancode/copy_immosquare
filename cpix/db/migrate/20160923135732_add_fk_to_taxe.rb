class AddFkToTaxe < ActiveRecord::Migration[5.0]
  def change
    add_reference :taxes, :building, index: true
    add_foreign_key :taxes, :buildings
  end
end
