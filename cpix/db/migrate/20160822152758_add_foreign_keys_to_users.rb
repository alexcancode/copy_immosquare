class AddForeignKeysToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_reference :users, :office, index: true
  	add_foreign_key :users, :offices
  	add_reference :users, :language, index: true
  	add_foreign_key :users, :languages
  end
end
