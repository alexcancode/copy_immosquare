class AddInfosToAddresses < ActiveRecord::Migration[5.0]
  def change
    remove_column :addresses, :civic
    remove_column :addresses, :address_one
    remove_column :addresses, :address_two
    remove_column :addresses, :postal_code
    remove_column :addresses, :municipality
    remove_column :addresses, :state_province
    remove_column :addresses, :country
    add_column :addresses, :latitude, :float
    add_column :addresses, :longitude, :float
    add_column :addresses, :zipcode, :string
    add_column :addresses, :street_number, :string
    add_column :addresses, :street_type, :string
    add_column :addresses, :street_name, :string
    add_column :addresses, :sublocality, :string
    add_column :addresses, :locality, :string
    add_column :addresses, :administrative_area_level_2, :string
    add_column :addresses, :administrative_area_level_1, :string
    add_column :addresses, :country, :string
    add_column :addresses, :address, :string
  end
end
