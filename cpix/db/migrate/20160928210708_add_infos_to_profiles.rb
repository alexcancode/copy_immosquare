class AddInfosToProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :company_name, :string
    add_column :users, :company_phone, :string
    add_column :users, :company_website, :string
    add_column :users, :company_email, :string
    add_column :users, :prefix, :string
    add_column :users, :title, :string
    add_column :users, :suffix, :string
    add_column :users, :personal_corporate_name, :string
    add_column :users, :phone_toll_free, :string
    add_column :users, :website, :string
    add_column :users, :languages, :string
    add_column :users, :qualifications, :text
    add_column :users, :specialties, :text
    add_column :users, :trading_areas, :string
  end
end
