class CreateListingStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :listing_statuses do |t|
      t.text :listing_status

      t.timestamps
    end
  end
end
