class CreateLeaseAdditionalCosts < ActiveRecord::Migration[5.0]
  def change
    create_table :lease_additional_costs do |t|
      t.text :name

      t.timestamps
    end
  end
end
