class CreateBuildings < ActiveRecord::Migration[5.0]
  def change
    create_table :buildings do |t|
      t.string :building_name
      t.integer :year_build
      t.decimal :total_floor_area
      t.decimal :numeric_of_floor
      t.decimal :elevator_capacity
      t.string :parking_nb_space
      t.boolean :parking_included_in_sale
      t.decimal :garage_size
      t.decimal :garage_door_size
      t.string :garage_nb_space
      t.text :green_certification_name
      t.text :present_use
      t.boolean :vehicle_access
      t.text :basement_name
      t.decimal :ceiling_height
      t.decimal :ceiling_clearance
      t.integer :loading_qty
      t.decimal :door_height
      t.decimal :door_width
      t.decimal :storage_size
      t.decimal :aperage
      t.integer :total_units
      t.integer :building_assessment_year
      t.money :building_annual_tax
      t.money :building_assessment_value

      t.timestamps
    end
  end
end
