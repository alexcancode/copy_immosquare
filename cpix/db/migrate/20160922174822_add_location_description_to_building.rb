class AddLocationDescriptionToBuilding < ActiveRecord::Migration[5.0]
  def change
    add_column :listings, :location_description, :text
  end
end
