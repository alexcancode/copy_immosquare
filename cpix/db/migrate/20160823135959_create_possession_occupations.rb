class CreatePossessionOccupations < ActiveRecord::Migration[5.0]
  def change
    create_table :possession_occupations do |t|
      t.string :name

      t.timestamps
    end
  end
end
