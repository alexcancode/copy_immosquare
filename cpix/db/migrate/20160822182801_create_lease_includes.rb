class CreateLeaseIncludes < ActiveRecord::Migration[5.0]
  def change
    create_table :lease_includes do |t|
      t.text :name

      t.timestamps
    end
  end
end
