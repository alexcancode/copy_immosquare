class AddForeignKeyToDocuments < ActiveRecord::Migration[5.0]
  def change
  	add_reference :documents, :document_type, index: true
  	add_foreign_key :documents, :document_types
  	add_reference :documents, :document_availability, index: true
  	add_foreign_key :documents, :document_availabilities
  end
end
