class UpdateListingOrders < ActiveRecord::Migration[5.0]
  def change
    add_column    :listings, :currency, :string,:after => :client_id
    remove_column :listings, :selling_commission_amount_currency
    remove_column :listings, :list_price_currency
    remove_column :listings, :sales_tax_currency
    remove_column :listings, :inventory_value_currency
    remove_column :listings, :lease_price_currency
    remove_column :listings, :total_monthly_lease_currency
    remove_column :listings, :additional_costs_amount_currency
    change_column :listings, :address_id,  :integer ,:after => :client_id
    change_column :listings, :building_id,  :integer ,:after => :address_id
    change_column :listings, :listing_major_type_id,  :integer ,:after => :building_id
    change_column :listings, :listing_minor_type_id,  :integer ,:after => :listing_major_type_id
    change_column :listings, :listing_sub_type_id,  :integer ,:after => :listing_minor_type_id
    change_column :listings, :transaction_type_id,  :integer ,:after => :listing_sub_type_id
    change_column :listings, :user_id,  :integer ,:after => :client_id
    change_column :listings, :sale_type_id,  :integer ,:after => :transaction_type_id
    change_column :listings, :listing_status_id,  :integer ,:after => :sale_type_id
    change_column :listings, :draft,  :integer ,:after => :client_id





  end
end
