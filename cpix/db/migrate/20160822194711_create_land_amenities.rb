class CreateLandAmenities < ActiveRecord::Migration[5.0]
  def change
    create_table :land_amenities do |t|
      t.text :name

      t.timestamps
    end
  end
end
