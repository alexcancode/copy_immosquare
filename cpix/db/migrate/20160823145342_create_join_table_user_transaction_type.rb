class CreateJoinTableUserTransactionType < ActiveRecord::Migration[5.0]
  def change
    create_join_table :users, :transaction_types do |t|
      t.index [:user_id, :transaction_type_id]
      t.index [:transaction_type_id, :user_id]
    end
  end
end
