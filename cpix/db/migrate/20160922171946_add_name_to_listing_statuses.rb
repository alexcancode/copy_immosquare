class AddNameToListingStatuses < ActiveRecord::Migration[5.0]
  def change
    remove_column :listing_statuses, :listing_status, :string
    add_column :listing_statuses, :name, :string, :after => :id
  end
end
