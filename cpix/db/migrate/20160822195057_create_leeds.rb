class CreateLeeds < ActiveRecord::Migration[5.0]
  def change
    create_table :leeds do |t|
      t.text :name

      t.timestamps
    end
  end
end
