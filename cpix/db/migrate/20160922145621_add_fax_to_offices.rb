class AddFaxToOffices < ActiveRecord::Migration[5.0]
  def change
    add_column :offices, :fax, :string, :after => :phone_ext
  end
end
