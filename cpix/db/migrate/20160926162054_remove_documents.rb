class RemoveDocuments < ActiveRecord::Migration[5.0]
  def change
    remove_attachment :documents, :document_file
    add_attachment :documents, :document
  end
end
