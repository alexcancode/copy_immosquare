class CreateSellers < ActiveRecord::Migration[5.0]
  def change
    create_table :sellers do |t|
      t.string :s_company
      t.string :s_legal_rep
      t.string :s_phone
      t.string :s_email
      t.boolean :s_canadian_resident
      t.string :o_company
      t.string :o_legal_rep
      t.string :o_phone
      t.string :o_email

      t.timestamps
    end
  end
end
