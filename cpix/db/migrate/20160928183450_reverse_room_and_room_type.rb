class ReverseRoomAndRoomType < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :room_types, { :column => :room_id }
    remove_column :room_types, :room_id
    add_reference :rooms, :room_type, index: true
    add_foreign_key :rooms, :room_types
  end
end
