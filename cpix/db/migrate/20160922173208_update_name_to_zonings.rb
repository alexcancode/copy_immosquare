class UpdateNameToZonings < ActiveRecord::Migration[5.0]
  def change
    change_column :zonings, :name, :string
  end
end
