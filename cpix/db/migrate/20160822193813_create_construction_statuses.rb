class CreateConstructionStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :construction_statuses do |t|
      t.text :name

      t.timestamps
    end
  end
end
