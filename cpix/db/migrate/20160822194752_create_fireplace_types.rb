class CreateFireplaceTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :fireplace_types do |t|
      t.text :name

      t.timestamps
    end
  end
end
