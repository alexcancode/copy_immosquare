class AddBuildingLocationDescToBuilding < ActiveRecord::Migration[5.0]
  def change
    add_column :buildings, :building_location_description, :text
  end
end
