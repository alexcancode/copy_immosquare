class Addlistingmajortypetolistingminortype < ActiveRecord::Migration[5.0]
  def change
	add_reference :listing_minor_types, :listing_major_type, index: true, :after => :id
  	add_foreign_key :listing_minor_types, :listing_major_types
  end
end
