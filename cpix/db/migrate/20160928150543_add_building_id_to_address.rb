class AddBuildingIdToAddress < ActiveRecord::Migration[5.0]
  def change
    add_reference :addresses, :building, index: true
    add_foreign_key :addresses, :buildings
  end
end
