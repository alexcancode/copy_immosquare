class AddTaxToBuilding < ActiveRecord::Migration[5.0]
  def change
    add_column :buildings, :building_tax_roll_number, :string
    add_column :buildings, :building_tax_year, :integer
    add_column :buildings, :building_tax_description, :text
  end
end
