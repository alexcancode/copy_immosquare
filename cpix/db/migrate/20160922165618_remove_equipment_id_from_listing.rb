class RemoveEquipmentIdFromListing < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :listings, { :column => :equipment_id }
    remove_column :listings, :equipment_id
  end
end
