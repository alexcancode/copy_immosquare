class FixMajorType < ActiveRecord::Migration[5.0]
  def change
    change_column :accesses, :name, :string
    change_column :appliances, :name, :string
    change_column :architecture_styles, :name, :string
    change_column :attached_styles, :name, :string
    change_column :basement_developments, :name, :string
    change_column :basement_types, :name, :string
    change_column :bomas, :name, :string
    change_column :building_amenities, :name, :string
    change_column :building_types, :name, :string
    change_column :ceilings, :name, :string
    change_column :communications, :name, :string
    change_column :construction_statuses, :name, :string
    change_column :construction_types, :name, :string
    change_column :coolings, :name, :string
    change_column :document_availabilities, :name, :string
    change_column :document_types, :name, :string
    change_column :elevators, :name, :string
    change_column :exterior_finishes, :name, :string
    change_column :fence_types, :name, :string
    change_column :fireplace_fuels, :name, :string
    change_column :fireplace_types, :name, :string
    change_column :floorings, :name, :string
    change_column :foundations, :name, :string
    change_column :franchise_types, :name, :string
    change_column :frontsons, :name, :string
    change_column :greens, :name, :string
    change_column :heating_fuels, :name, :string
    change_column :heating_types, :name, :string
    change_column :irrigation_types, :name, :string
    change_column :land_amenities, :name, :string
    change_column :land_types, :name, :string
    change_column :landrights, :name, :string
    change_column :landscapings, :name, :string
    change_column :lease_additional_costs, :name, :string
    change_column :lease_includes, :name, :string
    change_column :lease_terms, :name, :string
    change_column :lease_types, :name, :string
    change_column :leeds, :name, :string
    change_column :leedwells, :name, :string
    change_column :listing_major_types, :name, :string
    change_column :listing_minor_types, :name, :string
    change_column :listing_sub_types, :name, :string
    change_column :loading_types, :name, :string
    change_column :parkings, :name, :string
    change_column :powers, :name, :string
    change_column :protection_systems, :name, :string
    change_column :road_types, :name, :string
    change_column :roofs, :name, :string
    change_column :room_types, :name, :string
    change_column :rooms, :name, :string
    change_column :sale_types, :name, :string
    change_column :sewages, :name, :string
    change_column :signages, :name, :string
    change_column :site_features, :name, :string
    change_column :storages, :name, :string
    change_column :storefronts, :name, :string
    change_column :structure_types, :name, :string
    change_column :uffis, :name, :string
    change_column :utilities, :name, :string
    change_column :views, :name, :string
    change_column :waterfronts, :name, :string
    change_column :waters, :name, :string







  end
end
