# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161001021005) do

  create_table "accesses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.float    "latitude",                    limit: 24
    t.float    "longitude",                   limit: 24
    t.string   "zipcode"
    t.string   "street_number"
    t.string   "street_type"
    t.string   "street_name"
    t.string   "sublocality"
    t.string   "locality"
    t.string   "administrative_area_level_2"
    t.string   "administrative_area_level_1"
    t.string   "country"
    t.string   "address"
    t.integer  "building_id"
    t.index ["building_id"], name: "index_addresses_on_building_id", using: :btree
  end

  create_table "api_apps", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "key"
    t.string   "token"
    t.integer  "api_provider_id"
    t.string   "search_authorized_users"
    t.string   "internal_note"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["api_provider_id"], name: "index_api_apps_on_api_provider_id", using: :btree
  end

  create_table "api_provider", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
  end

  create_table "appliances", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "architecture_styles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "areas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "slug"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attached_styles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "basement_developments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "basement_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bomas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "building_amenities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "building_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "buildings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "building_name"
    t.integer  "year_build"
    t.decimal  "total_floor_area",                             precision: 10
    t.decimal  "nb_of_floor",                                  precision: 10
    t.decimal  "elevator_capacity",                            precision: 10
    t.string   "parking_nb_space"
    t.boolean  "parking_included_in_sale"
    t.decimal  "garage_size",                                  precision: 10
    t.decimal  "garage_door_size",                             precision: 10
    t.string   "garage_nb_space"
    t.text     "green_certification_name",       limit: 65535
    t.text     "present_use",                    limit: 65535
    t.boolean  "vehicle_access"
    t.text     "basement_name",                  limit: 65535
    t.decimal  "ceiling_height",                               precision: 10
    t.decimal  "ceiling_clearance",                            precision: 10
    t.integer  "loading_qty"
    t.decimal  "door_height",                                  precision: 10
    t.decimal  "door_width",                                   precision: 10
    t.decimal  "storage_size",                                 precision: 10
    t.decimal  "aperage",                                      precision: 10
    t.integer  "total_units"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.string   "prop_legal_address_lot"
    t.string   "prop_legal_address_block"
    t.string   "prop_legal_address_plan"
    t.string   "prop_legal_address_meridian"
    t.string   "prop_legal_address_range"
    t.string   "prop_legal_address_township"
    t.string   "prop_legal_address_section"
    t.string   "prop_legal_address_quarter"
    t.string   "prop_legal_address_subdivision"
    t.text     "prop_legal_address_freeform",    limit: 65535
    t.string   "area_sub_area"
    t.text     "zoning_description",             limit: 65535
    t.float    "mapping_lat",                    limit: 24
    t.float    "mapping_long",                   limit: 24
    t.boolean  "utilities_availability"
    t.decimal  "irrigation_coverage",                          precision: 10
    t.decimal  "land_total_size",                              precision: 10
    t.decimal  "land_depth",                                   precision: 10
    t.decimal  "land_width",                                   precision: 10
    t.text     "land_size_description",          limit: 65535
    t.decimal  "land_frontage_measurement",                    precision: 10
    t.text     "land_use",                       limit: 65535
    t.integer  "land_amount"
    t.decimal  "fenced_area_measurement",                      precision: 10
    t.integer  "address_id"
    t.integer  "architecture_style_id"
    t.integer  "structure_type_id"
    t.integer  "attached_style_id"
    t.integer  "construction_status_id"
    t.integer  "construction_type_id"
    t.integer  "elevator_id"
    t.integer  "signage_id"
    t.integer  "parking_id"
    t.integer  "uffi_id"
    t.integer  "boma_id"
    t.integer  "leed_id"
    t.integer  "leedwell_id"
    t.integer  "green_id"
    t.integer  "frontson_id"
    t.integer  "basement_development_id"
    t.integer  "basement_type_id"
    t.integer  "ceiling_id"
    t.integer  "cooling_id"
    t.integer  "appliance_id"
    t.integer  "protection_system_id"
    t.integer  "flooring_id"
    t.integer  "heating_type_id"
    t.integer  "heating_fuel_id"
    t.integer  "loading_type_id"
    t.integer  "storage_id"
    t.integer  "power_id"
    t.integer  "sewage_id"
    t.integer  "water_id"
    t.integer  "site_feature_id"
    t.integer  "storefront_id"
    t.integer  "fireplace_fuel_id"
    t.integer  "fireplace_type_id"
    t.integer  "area_id"
    t.integer  "zoning_id"
    t.integer  "access_id"
    t.integer  "landright_id"
    t.integer  "waterfront_id"
    t.integer  "fence_type_id"
    t.integer  "road_type_id"
    t.integer  "land_type_id"
    t.integer  "utility_id"
    t.integer  "view_id"
    t.integer  "irrigation_type_id"
    t.integer  "building_amenity_id"
    t.integer  "landscaping_id"
    t.integer  "document_id"
    t.integer  "photo_id"
    t.integer  "land_amenity_id"
    t.integer  "exterior_finish_id"
    t.integer  "building_type_id"
    t.integer  "foundation_id"
    t.integer  "municipality_id"
    t.integer  "roof_id"
    t.integer  "communication_id"
    t.text     "building_description",           limit: 65535
    t.text     "building_location_description",  limit: 65535
    t.index ["access_id"], name: "index_buildings_on_access_id", using: :btree
    t.index ["address_id"], name: "index_buildings_on_address_id", using: :btree
    t.index ["appliance_id"], name: "index_buildings_on_appliance_id", using: :btree
    t.index ["architecture_style_id"], name: "index_buildings_on_architecture_style_id", using: :btree
    t.index ["area_id"], name: "index_buildings_on_area_id", using: :btree
    t.index ["attached_style_id"], name: "index_buildings_on_attached_style_id", using: :btree
    t.index ["basement_development_id"], name: "index_buildings_on_basement_development_id", using: :btree
    t.index ["basement_type_id"], name: "index_buildings_on_basement_type_id", using: :btree
    t.index ["boma_id"], name: "index_buildings_on_boma_id", using: :btree
    t.index ["building_amenity_id"], name: "index_buildings_on_building_amenity_id", using: :btree
    t.index ["building_type_id"], name: "index_buildings_on_building_type_id", using: :btree
    t.index ["ceiling_id"], name: "index_buildings_on_ceiling_id", using: :btree
    t.index ["communication_id"], name: "index_buildings_on_communication_id", using: :btree
    t.index ["construction_status_id"], name: "index_buildings_on_construction_status_id", using: :btree
    t.index ["construction_type_id"], name: "index_buildings_on_construction_type_id", using: :btree
    t.index ["cooling_id"], name: "index_buildings_on_cooling_id", using: :btree
    t.index ["document_id"], name: "index_buildings_on_document_id", using: :btree
    t.index ["elevator_id"], name: "index_buildings_on_elevator_id", using: :btree
    t.index ["exterior_finish_id"], name: "index_buildings_on_exterior_finish_id", using: :btree
    t.index ["fence_type_id"], name: "index_buildings_on_fence_type_id", using: :btree
    t.index ["fireplace_fuel_id"], name: "index_buildings_on_fireplace_fuel_id", using: :btree
    t.index ["fireplace_type_id"], name: "index_buildings_on_fireplace_type_id", using: :btree
    t.index ["flooring_id"], name: "index_buildings_on_flooring_id", using: :btree
    t.index ["foundation_id"], name: "index_buildings_on_foundation_id", using: :btree
    t.index ["frontson_id"], name: "index_buildings_on_frontson_id", using: :btree
    t.index ["green_id"], name: "index_buildings_on_green_id", using: :btree
    t.index ["heating_fuel_id"], name: "index_buildings_on_heating_fuel_id", using: :btree
    t.index ["heating_type_id"], name: "index_buildings_on_heating_type_id", using: :btree
    t.index ["irrigation_type_id"], name: "index_buildings_on_irrigation_type_id", using: :btree
    t.index ["land_amenity_id"], name: "index_buildings_on_land_amenity_id", using: :btree
    t.index ["land_type_id"], name: "index_buildings_on_land_type_id", using: :btree
    t.index ["landright_id"], name: "index_buildings_on_landright_id", using: :btree
    t.index ["landscaping_id"], name: "index_buildings_on_landscaping_id", using: :btree
    t.index ["leed_id"], name: "index_buildings_on_leed_id", using: :btree
    t.index ["leedwell_id"], name: "index_buildings_on_leedwell_id", using: :btree
    t.index ["loading_type_id"], name: "index_buildings_on_loading_type_id", using: :btree
    t.index ["municipality_id"], name: "index_buildings_on_municipality_id", using: :btree
    t.index ["parking_id"], name: "index_buildings_on_parking_id", using: :btree
    t.index ["photo_id"], name: "index_buildings_on_photo_id", using: :btree
    t.index ["power_id"], name: "index_buildings_on_power_id", using: :btree
    t.index ["protection_system_id"], name: "index_buildings_on_protection_system_id", using: :btree
    t.index ["road_type_id"], name: "index_buildings_on_road_type_id", using: :btree
    t.index ["roof_id"], name: "index_buildings_on_roof_id", using: :btree
    t.index ["sewage_id"], name: "index_buildings_on_sewage_id", using: :btree
    t.index ["signage_id"], name: "index_buildings_on_signage_id", using: :btree
    t.index ["site_feature_id"], name: "index_buildings_on_site_feature_id", using: :btree
    t.index ["storage_id"], name: "index_buildings_on_storage_id", using: :btree
    t.index ["storefront_id"], name: "index_buildings_on_storefront_id", using: :btree
    t.index ["structure_type_id"], name: "index_buildings_on_structure_type_id", using: :btree
    t.index ["uffi_id"], name: "index_buildings_on_uffi_id", using: :btree
    t.index ["utility_id"], name: "index_buildings_on_utility_id", using: :btree
    t.index ["view_id"], name: "index_buildings_on_view_id", using: :btree
    t.index ["water_id"], name: "index_buildings_on_water_id", using: :btree
    t.index ["waterfront_id"], name: "index_buildings_on_waterfront_id", using: :btree
    t.index ["zoning_id"], name: "index_buildings_on_zoning_id", using: :btree
  end

  create_table "ceilings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "communications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "construction_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "construction_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.text     "message",     limit: 65535
    t.integer  "property_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "coolings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "document_availabilities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "document_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "documentable_id"
    t.string   "documentable_type"
    t.date     "document_date"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "document_type_id"
    t.integer  "document_availability_id"
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
    t.index ["document_availability_id"], name: "index_documents_on_document_availability_id", using: :btree
    t.index ["document_type_id"], name: "index_documents_on_document_type_id", using: :btree
    t.index ["documentable_id", "documentable_type"], name: "index_documents_on_documentable_id_and_documentable_type", using: :btree
  end

  create_table "elevators", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "equipment", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "listing_id"
    t.index ["listing_id"], name: "index_equipment_on_listing_id", using: :btree
  end

  create_table "exterior_finishes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fence_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fireplace_fuels", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fireplace_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "floorings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "foundations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "franchise_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "frontsons", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "greens", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "heating_fuels", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "heating_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "irrigation_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "land_amenities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "land_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "landrights", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "landscapings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "languages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "lease_additional_costs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lease_includes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lease_terms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lease_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "slug"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "leeds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "leedwells", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "listing_major_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "slug"
    t.string   "name"
    t.string   "color",      default: "#000000"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "listing_minor_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "listing_major_type_id"
    t.string   "name"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["listing_major_type_id"], name: "index_listing_minor_types_on_listing_major_type_id", using: :btree
  end

  create_table "listing_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "listing_sub_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "listing_minor_type_id"
    t.string   "name"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["listing_minor_type_id"], name: "index_listing_sub_types_on_listing_minor_type_id", using: :btree
  end

  create_table "listings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uid"
    t.integer  "client_id",                                                         default: 1
    t.integer  "draft",                                                             default: 1
    t.integer  "user_id"
    t.integer  "building_id"
    t.integer  "listing_major_type_id"
    t.integer  "listing_minor_type_id"
    t.integer  "listing_sub_type_id"
    t.integer  "transaction_type_id"
    t.integer  "lease_type_id"
    t.integer  "sale_type_id"
    t.integer  "listing_status_id"
    t.string   "currency",                                                          default: "CAD"
    t.integer  "lease_price_cents",                                                                 null: false
    t.integer  "list_price_cents",                                                                  null: false
    t.text     "description",                          limit: 65535
    t.string   "business_name"
    t.integer  "operating_since"
    t.date     "list_date"
    t.date     "listing_expiry_date"
    t.string   "sellers_interest"
    t.string   "sellers_right_reserved"
    t.string   "limited_service_contract"
    t.date     "possession_occupation_date"
    t.decimal  "selling_commission_rate",                            precision: 10
    t.integer  "selling_commission_amount_cents",                                                   null: false
    t.integer  "sales_tax_cents",                                                                   null: false
    t.integer  "inventory_value_cents",                                                             null: false
    t.integer  "total_monthly_lease_cents",                                                         null: false
    t.integer  "additional_costs_amount_cents",                                                     null: false
    t.datetime "created_at",                                                                        null: false
    t.datetime "updated_at",                                                                        null: false
    t.boolean  "inventory_included"
    t.text     "inventory_description",                limit: 65535
    t.string   "franchisor_name"
    t.string   "franchise_name"
    t.text     "public_remarks",                       limit: 65535
    t.text     "private_remarks",                      limit: 65535
    t.boolean  "finance_agreement_for_sale"
    t.boolean  "finance_assume_with_qualification"
    t.boolean  "finance_clear_title"
    t.boolean  "finance_lease_with_option_to_buy"
    t.boolean  "finance_mortgage_must_be_assumed"
    t.boolean  "finance_mortgage_must_be_paid_off"
    t.boolean  "finance_option_to_renew"
    t.boolean  "finance_buyer_to_finance"
    t.boolean  "finance_sale_lease_back"
    t.boolean  "finance_sale_lease_option"
    t.boolean  "finance_seller_may_buy_down_mortgage"
    t.boolean  "finance_seller_may_carry_mortgage"
    t.boolean  "finance_trade_or_exchange"
    t.boolean  "finance_vendor_take_back"
    t.text     "finance_notes",                        limit: 65535
    t.boolean  "ddf_distribution"
    t.boolean  "wordl_prop_display"
    t.string   "public_display_address"
    t.string   "feature_sheet_url"
    t.string   "virtual_tour_url"
    t.string   "sale_brochure_url"
    t.string   "video_tour_url"
    t.string   "prop_map_url"
    t.integer  "listing_measurement_unit"
    t.date     "current_lease_expiry"
    t.integer  "payment_frequency"
    t.decimal  "lease_area_size",                                    precision: 10
    t.text     "lease_details",                        limit: 65535
    t.text     "leasehold_improvements_details",       limit: 65535
    t.string   "additional_costs_payment_freq"
    t.text     "additional_costs_details",             limit: 65535
    t.string   "landlord"
    t.string   "property_mngt_company"
    t.string   "property_mngt_contact"
    t.string   "property_mngt_address"
    t.string   "property_mngt_phone"
    t.integer  "possession_occupation_id"
    t.integer  "title_type_id"
    t.integer  "franchise_type_id"
    t.integer  "lease_term_id"
    t.integer  "lease_include_id"
    t.integer  "lease_additional_cost_id"
    t.text     "location_description",                 limit: 65535
    t.integer  "listing_completeness",                                              default: 0
    t.index ["building_id"], name: "index_listings_on_building_id", using: :btree
    t.index ["franchise_type_id"], name: "index_listings_on_franchise_type_id", using: :btree
    t.index ["lease_additional_cost_id"], name: "index_listings_on_lease_additional_cost_id", using: :btree
    t.index ["lease_include_id"], name: "index_listings_on_lease_include_id", using: :btree
    t.index ["lease_term_id"], name: "index_listings_on_lease_term_id", using: :btree
    t.index ["lease_type_id"], name: "index_listings_on_lease_type_id", using: :btree
    t.index ["listing_major_type_id"], name: "index_listings_on_listing_major_type_id", using: :btree
    t.index ["listing_minor_type_id"], name: "index_listings_on_listing_minor_type_id", using: :btree
    t.index ["listing_status_id"], name: "index_listings_on_listing_status_id", using: :btree
    t.index ["listing_sub_type_id"], name: "index_listings_on_listing_sub_type_id", using: :btree
    t.index ["possession_occupation_id"], name: "index_listings_on_possession_occupation_id", using: :btree
    t.index ["sale_type_id"], name: "index_listings_on_sale_type_id", using: :btree
    t.index ["title_type_id"], name: "index_listings_on_title_type_id", using: :btree
    t.index ["transaction_type_id"], name: "index_listings_on_transaction_type_id", using: :btree
    t.index ["user_id"], name: "index_listings_on_user_id", using: :btree
  end

  create_table "loading_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "municipalities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uid"
    t.integer  "client_id",  default: 1
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.integer  "phone_ext"
    t.string   "fax"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "address_id"
    t.index ["address_id"], name: "index_offices_on_address_id", using: :btree
  end

  create_table "parkings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "photos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "position"
    t.integer  "photoable_id"
    t.string   "photoable_type"
    t.string   "name"
    t.text     "description",        limit: 65535
    t.boolean  "primary"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["photoable_id", "photoable_type"], name: "index_photos_on_photoable_id_and_photoable_type", using: :btree
  end

  create_table "possession_occupations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "powers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "protection_systems", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "road_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "roofs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "room_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rooms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "listing_id"
    t.string   "name"
    t.decimal  "room_size",    precision: 10
    t.decimal  "room_length",  precision: 10
    t.decimal  "room_width",   precision: 10
    t.integer  "room_level"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "room_type_id"
    t.index ["room_type_id"], name: "index_rooms_on_room_type_id", using: :btree
  end

  create_table "sale_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sellers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "s_company"
    t.string   "s_legal_rep"
    t.string   "s_phone"
    t.string   "s_email"
    t.boolean  "s_canadian_resident"
    t.string   "o_company"
    t.string   "o_legal_rep"
    t.string   "o_phone"
    t.string   "o_email"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "listing_id"
    t.integer  "address_id"
    t.index ["address_id"], name: "index_sellers_on_address_id", using: :btree
    t.index ["listing_id"], name: "index_sellers_on_listing_id", using: :btree
  end

  create_table "sewages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "signages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "site_features", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "storages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "storefronts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "structure_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "taxes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "tax_roll_number"
    t.integer  "tax_year"
    t.integer  "building_assessment_year"
    t.integer  "building_assessment_value_cents",                  default: 0,     null: false
    t.string   "building_assessment_value_currency",               default: "CAD", null: false
    t.integer  "building_annual_tax_cents",                        default: 0,     null: false
    t.string   "building_annual_tax_currency",                     default: "CAD", null: false
    t.integer  "land_assessment_year"
    t.integer  "land_assessment_value_cents",                      default: 0,     null: false
    t.string   "land_assessment_value_currency",                   default: "CAD", null: false
    t.integer  "land_annual_tax_cents",                            default: 0,     null: false
    t.string   "land_annual_tax_currency",                         default: "CAD", null: false
    t.text     "building_tax_description",           limit: 65535
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.integer  "building_id"
    t.index ["building_id"], name: "index_taxes_on_building_id", using: :btree
  end

  create_table "title_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transaction_major_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transaction_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "transaction_major_type_id", default: 1
    t.string   "slug"
    t.string   "name"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["transaction_major_type_id"], name: "index_transaction_types_on_transaction_major_type_id", using: :btree
  end

  create_table "transaction_types_users", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id",             null: false
    t.integer "transaction_type_id", null: false
    t.index ["transaction_type_id", "user_id"], name: "index_transaction_types_users_on_transaction_type_id_and_user_id", using: :btree
    t.index ["user_id", "transaction_type_id"], name: "index_transaction_types_users_on_user_id_and_transaction_type_id", using: :btree
  end

  create_table "uffis", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "uid"
    t.integer  "client_id",                             default: 1
    t.integer  "office_id"
    t.string   "email",                                 default: "",     null: false
    t.string   "encrypted_password",                    default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "business_phone"
    t.string   "business_phone_ext"
    t.string   "mobile_phone"
    t.string   "linkedin_link"
    t.integer  "language_id"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.string   "company_name"
    t.string   "company_phone"
    t.string   "company_website"
    t.string   "company_email"
    t.string   "prefix"
    t.string   "title"
    t.string   "suffix"
    t.string   "personal_corporate_name"
    t.string   "phone_toll_free"
    t.string   "website"
    t.string   "languages"
    t.text     "qualifications",          limit: 65535
    t.text     "specialties",             limit: 65535
    t.string   "trading_areas"
    t.string   "current_map_theme",                     default: "dark"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["language_id"], name: "index_users_on_language_id", using: :btree
    t.index ["office_id"], name: "index_users_on_office_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  create_table "utilities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "views", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "waterfronts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "waters", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "zonings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.string   "name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "municipality_id"
    t.index ["municipality_id"], name: "index_zonings_on_municipality_id", using: :btree
  end

  add_foreign_key "addresses", "buildings"
  add_foreign_key "buildings", "accesses"
  add_foreign_key "buildings", "addresses"
  add_foreign_key "buildings", "appliances"
  add_foreign_key "buildings", "architecture_styles"
  add_foreign_key "buildings", "areas"
  add_foreign_key "buildings", "attached_styles"
  add_foreign_key "buildings", "basement_developments"
  add_foreign_key "buildings", "basement_types"
  add_foreign_key "buildings", "bomas"
  add_foreign_key "buildings", "building_amenities"
  add_foreign_key "buildings", "building_types"
  add_foreign_key "buildings", "ceilings"
  add_foreign_key "buildings", "communications"
  add_foreign_key "buildings", "construction_statuses"
  add_foreign_key "buildings", "construction_types"
  add_foreign_key "buildings", "coolings"
  add_foreign_key "buildings", "documents"
  add_foreign_key "buildings", "elevators"
  add_foreign_key "buildings", "exterior_finishes"
  add_foreign_key "buildings", "fence_types"
  add_foreign_key "buildings", "fireplace_fuels"
  add_foreign_key "buildings", "fireplace_types"
  add_foreign_key "buildings", "floorings"
  add_foreign_key "buildings", "foundations"
  add_foreign_key "buildings", "frontsons"
  add_foreign_key "buildings", "greens"
  add_foreign_key "buildings", "heating_fuels"
  add_foreign_key "buildings", "heating_types"
  add_foreign_key "buildings", "irrigation_types"
  add_foreign_key "buildings", "land_amenities"
  add_foreign_key "buildings", "land_types"
  add_foreign_key "buildings", "landrights"
  add_foreign_key "buildings", "landscapings"
  add_foreign_key "buildings", "leeds"
  add_foreign_key "buildings", "leedwells"
  add_foreign_key "buildings", "loading_types"
  add_foreign_key "buildings", "municipalities"
  add_foreign_key "buildings", "parkings"
  add_foreign_key "buildings", "photos"
  add_foreign_key "buildings", "powers"
  add_foreign_key "buildings", "protection_systems"
  add_foreign_key "buildings", "road_types"
  add_foreign_key "buildings", "roofs"
  add_foreign_key "buildings", "sewages"
  add_foreign_key "buildings", "signages"
  add_foreign_key "buildings", "site_features"
  add_foreign_key "buildings", "storages"
  add_foreign_key "buildings", "storefronts"
  add_foreign_key "buildings", "structure_types"
  add_foreign_key "buildings", "uffis"
  add_foreign_key "buildings", "utilities"
  add_foreign_key "buildings", "views"
  add_foreign_key "buildings", "waterfronts"
  add_foreign_key "buildings", "waters"
  add_foreign_key "buildings", "zonings"
  add_foreign_key "documents", "document_availabilities"
  add_foreign_key "documents", "document_types"
  add_foreign_key "equipment", "listings"
  add_foreign_key "listing_minor_types", "listing_major_types"
  add_foreign_key "listing_sub_types", "listing_minor_types"
  add_foreign_key "listings", "buildings"
  add_foreign_key "listings", "franchise_types"
  add_foreign_key "listings", "lease_additional_costs"
  add_foreign_key "listings", "lease_includes"
  add_foreign_key "listings", "lease_terms"
  add_foreign_key "listings", "lease_types"
  add_foreign_key "listings", "listing_major_types"
  add_foreign_key "listings", "listing_minor_types"
  add_foreign_key "listings", "listing_statuses"
  add_foreign_key "listings", "listing_sub_types"
  add_foreign_key "listings", "possession_occupations"
  add_foreign_key "listings", "sale_types"
  add_foreign_key "listings", "title_types"
  add_foreign_key "listings", "transaction_types"
  add_foreign_key "listings", "users"
  add_foreign_key "offices", "addresses"
  add_foreign_key "rooms", "room_types"
  add_foreign_key "sellers", "addresses"
  add_foreign_key "sellers", "listings"
  add_foreign_key "taxes", "buildings"
  add_foreign_key "users", "languages"
  add_foreign_key "users", "offices"
  add_foreign_key "zonings", "municipalities"
end
