


##============================================================##
## Filters
##============================================================##
module.exports = React.createClass
  getInitialState: ->
    return {
      mode: "input"
      area_unit: "sqft"
      advanced: true
      price_from: @props.search.price_from
      price_to: @props.search.price_to
      area_from: @props.search.area_from
      area_to: @props.search.area_to
      nbfloors_from: @props.search.nbfloors_from
      nbfloors_to: @props.search.nbfloors_to
      yearbuilt_from:  @props.search.yearbuilt_from
      yearbuilt_to:  @props.search.yearbuilt_to
    }

  handleFilterChange: (changed_params) ->
    @props.onFiltersChanged(changed_params)

  handleChange: (name,e) ->
    change = {}
    change[name] = e.target.value
    @setState(change)

  lunchSearch: ->
    @props.onFiltersChanged(@state)

  updateAreaUnit:(unit) ->
    @setState(area_unit: unit)

  updateAdvancedSearch: ->
    @setState(advanced: !@state.advanced)

  render: ->

    price_container =
      if @state.mode == "input"
        <div className='row'>
          <div className='col-xs-6'>
            <input onChange={@handleChange.bind(@,"price_from")} className="form-control" value={@state.priceMin} placeholder="min" />
          </div>
          <div className='col-xs-6'>
            <input onChange={@handleChange.bind(@,"price_to")} className="form-control" value={@state.priceMax} placeholder="max" />
          </div>
        </div>
      else
        <RangeSlider type="double" name="price" grid=false min="0" max="3000000" from="0" to="1000000" postfix="$" max-postfix="+" onFilterChanged={@handleFilterChange} />


    area_container =
      if @state.mode == "input"
        <div className='row'>
          <div className='col-xs-6'>
            <input onChange={@handleChange.bind(@,"area_from")} className="form-control" value={@state.areaMin} placeholder="min" />
          </div>
          <div className='col-xs-6'>
            <input onChange={@handleChange.bind(@,"area_to")} className="form-control" value={@state.areaMax} placeholder="max" />
          </div>
        </div>
      else
        <RangeSlider type="double" name="area" grid=false min="0" max="5000" from="0" to="3000" postfix="sq" max-postfix="+" onFilterChanged={@handleFilterChange} />

    yearbuilt_container =
      if @state.mode == "input"
        <div className="col-sm-3">
          <h3 className="fs13 bold color-white mb10 mt-15">Year built</h3>
            <div className='row'>
              <div className='col-xs-6'>
                <input onChange={@handleChange.bind(@,"yearbuilt_from")} className="form-control" value={@state.yearbuiltMin} placeholder="min" />
              </div>
              <div className='col-xs-6'>
                <input onChange={@handleChange.bind(@,"yearbuilt_to")} className="form-control" value={@state.yearbuiltMax} placeholder="max" />
              </div>
            </div>
        </div>
      else
            <RangeSlider type="double" name="yearbuilt" grid=false min="0" max="{Date.getFullYear()}" from="0" to="{Date.getFullYear()}" postfix="yrs" max-postfix="+" onFilterChanged={@handleFilterChange} />

    nbfloors_container =
      if @state.mode == "input"
        <div className="col-sm-3">
          <h3 className="fs13 bold color-white mb10 mt-15">Number of floors</h3>
            <div className='row'>
              <div className='col-xs-6'>
                <input onChange={@handleChange.bind(@,"nbfloors_from")} className="form-control" value={@state.nbfloorMin} placeholder="min" />
              </div>
              <div className='col-xs-6'>
                <input onChange={@handleChange.bind(@,"nbfloors_to")} className="form-control" value={@state.nbfloorsMax} placeholder="max" />
              </div>
            </div>
        </div>
      else
            <RangeSlider type="double" name="nbfloors" grid=false min="0" max="100" from="0" to="100" postfix="yrs" max-postfix="+" onFilterChanged={@handleFilterChange} />

    AdvancedSearch =
      if @state.advanced is true
        <div className="row mt20 text-center more-filters">
          <button onClick={@updateAdvancedSearch} className="button-secondary brbl0 brbr0 pt10 pb10 pl25 pr25 uppercase fs13">Advanced Search<span className="icon inline-block ml10 lh17"><i className="fa fa-plus color-primary"></i></span></button>
        </div>
      else
        <div>
          <div className="row mt30 same-height-col">
            <div className="col-sm-5">
              <h3 className="fs13 bold color-white mb10 mt-15">Keywords</h3>
                <div className='row'>
                  <div className='col-xs-12'>
                    <input className="form-control" placeholder="keywords" />
                  </div>
                </div>
            </div>
            {yearbuilt_container}
            {nbfloors_container}
          </div>
          <div className="row mt20 same-height-col">
            <div className="col-sm-11">
              <div className="pointer" onClick={@updateAdvancedSearch}>Close advanced search</div>
            </div>
          </div>
        </div>

    return(
      <section id="search-filters" className="bg-secondary color-white">
        <div className="container pt10 relative">
          <div className="row mt20 same-height-col">
            <div className="col-sm-2">
              <Select value={@props.search.majortransaction} data={@props.majortransaction} title="For Sale / For Lease" name="majortransaction" onFilterChanged={@handleFilterChange} />
            </div>
            <div className="col-sm-3">
              <Select value={@props.search.majortype} data={@props.majorType} title="Type" name="majortype" onFilterChanged={@handleFilterChange} />
            </div>
            <div className="col-sm-3">
              <h3 className="fs13 bold color-white mb10 mt-15">Price</h3>
              {price_container}
            </div>
            <div className="col-sm-3">
              <h3 className="fs13 bold color-white mb10 mt-15">Size(
                {["sqft","sqm"].map((unit) =>
                  return(<span key={unit} onClick={@updateAreaUnit.bind(@,unit)} className="#{if unit == @state.area_unit then 'color-primary bold' else 'color-white'} pointer mr2 ml2">{unit}</span>)
                )}
              )</h3>
              {area_container}
            </div>
            <div className="col-sm-1">
              <i onClick={@lunchSearch} className="fa fa-search color-primary pointer fa-2x mt10"></i>
            </div>
          </div>
          {AdvancedSearch}
          <div className="absolute absolute-bottom-right ml15">
            <div>{@props.size} results</div>
          </div>
        </div>
      </section>
    )



##============================================================##
## Select Class
##============================================================##
Select = React.createClass
  getInitialState: ->
    return{
      value: @props.value
      name: @props.name
      isAllSelected: @props.isAllSelected
    }


  renderSelect: () ->
    $(@refs.SelectFilter)
      .on 'mouseover', ->
        $($(@).find('.filterInputDropdown')).show()
      .on 'mouseout', ->
        $($(@).find('.filterInputDropdown')).hide()


  componentDidMount:() ->
    @renderSelect()


  updateSelect: (id,toggle) ->
    my_array = _.without((_.map @state.value.split(','), (num) -> num * 1),0)
    if toggle
      new_value =  (_.without(my_array,id)).join(",")
    else
      my_array.push(id)
      new_value = (_.sortBy(_.uniq(_.compact(my_array)))).join(",")

    @setState(value: new_value)
    @props.onFilterChanged({"#{@state.name}": new_value})

  updateSelectAll: () ->
    my_array = []

    filt =   $(@refs.SelectFilter)
    if (filt.find('.selectAll').hasClass('fa-square-o')) ##Est decoche on coche
      filt.find('.listitem').removeClass('fa-square-o').addClass('fa-check-square')
      ## Place toute les valeurs dans le array
      @props.data.map((d) => my_array.push(d.id))
      new_value =  my_array.join(",")
    else
      filt.find('.listitem').removeClass('fa-check-square').addClass('fa-square-o')
      new_value = '9999'

    @setState(value: new_value)
    @props.onFilterChanged({"#{@state.name}": new_value})


  define_title: (title,data,value) ->
    t = []
    data.map((d) =>
      toggle =  _.contains((_.map value.split(','), (num) -> num * 1),d.id)
      if toggle
        t.push d.name
    )
    if t.length == 0
      title
    else if t.length == 1
      t[0]
    else
      "#{t[0]} + #{t.length-1}..."

  render: () ->

    return(
      <div ref="SelectFilter" className="filterInput relative">
        <h3 className="fs13 bold color-white mb10 mt-15">{@props.title}</h3>
        <div className="filterInputPlaceholder">{@define_title(@props.title,@props.data,@props.value)}</div>
        <div className="filterInputDropdown">
          <ul className="list-unstyled mb0">
          <li key="0" ><i className="fa mr5 pull-left fs10 mt5 fa-check-square listitem selectAll " onClick={@updateSelectAll}></i>Select All</li>
            {if @props.data? then @props.data.map((d) =>
              toggle =  _.contains((_.map @state.value.split(','), (num) -> num * 1),d.id)
              divStyle =  
                color: d.color
              return(
                <li key={d.id} onClick={@updateSelect.bind(@,d.id,toggle)}>
                  <i className="fa mr5 pull-left fs10 mt5  #{if toggle then 'fa-check-square'  else 'fa-square-o' } listitem"></i>{d.name}<i className="fa fa-square ml5 fs10" style={divStyle}></i>
                </li>
              )
            ) else null }
          </ul>
        </div>
      </div>
    )



##============================================================##
## RangeSlider
##============================================================##
RangeSlider = React.createClass
  getInitialState: ->
    return {}

  renderSlider: () ->
    $(@refs.RefIonRangeSlider).ionRangeSlider
      type:             @props.type
      grid:             @props.grid
      min:              @props.min
      max:              @props.max
      from:             @props.from
      to:               @props.to
      prefix:           @props.prefix
      postfix:          @props.postfix
      step:             @props.step
      prettify_enabled: true
      force_edges:      true
      max_postfix:      @props.maxPostfix
      prettify:         @props.prettify
      onFinish:(data) =>
        filter_name = $(data.input).data('name')
        new_data = {
          "#{filter_name}_from": data.from,
          "#{filter_name}_to": data.to,
        }
        @props.onFilterChanged(new_data)


  componentDidMount:() ->
    @renderSlider()


  render: () ->
    return(
      <div ref="RefIonRangeSlider" data-name={@props.name}></div>
    )

