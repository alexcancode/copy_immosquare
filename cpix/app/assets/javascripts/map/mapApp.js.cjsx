Router        = require('react-router').Router
Route         = require('react-router').Route
Link          = require('react-router').Link
History       = require('react-router').browserHistory
Filters       = require('map/filters')
Map           = require('map/map')
ProperiesList = require('map/properiesList')


module.exports = React.createClass
  render: () ->
    return(
      <Router history={History}>
        <Route component={MainLayout} logo={@props.data.logo} currentUser={@props.data.currentUser}>
          <Route path="/en/map" component={ReactMapApp} apiUrl={@props.data.api_url} mapboxToken={@props.data.mapboxtoken} mapTheme={@props.data.maptheme} mapThemes={@props.data.mapthemes} majorType={@props.data.majortype} majortransaction={@props.data.majortransaction} ></Route>
        </Route>
      </Router>
    )



MainLayout = React.createClass
  render: () ->
    return (
      <div>
        <nav  id="main-nav" className="navbar navbar-default bg-white mb0">
          <div className="container-fluid">
            <div className="navbar-header full-height">
              <a className="navbar-brand  pl30 pb0 pt0 pr0" href="/en">
                <img id="logoMap" className="img-responsive mt5" src={@props.route.logo} alt="Logo Bee Commercial"/>
              </a>
            </div>
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <LoginMenu currentUser={@props.route.currentUser} />
            </div>
          </div>
        </nav>
        {this.props.children}
      </div>
    )



LoginMenu = React.createClass
  render: () ->
    if @props.currentUser is true
      return(
        <ul className="nav navbar-nav navbar-right">
          <li><a className="bold" href="/en">Home</a></li>
          <li><a className="bold" href="/en/dashboard">Dashboard</a></li>
          <li><a className="bold" rel="nofollow" data-method="delete" href="/en/users/sign_out">Sign out</a></li>
      </ul>
      )
    else
      return(
        <ul className="nav navbar-nav navbar-right">
          <li><a href="/" className="bold">Home</a></li>
          <li><a className="bold" href="/en/users/sign_in">Sign in</a></li>
        </ul>
      )



##============================================================##
## Page Behevior
##============================================================##
ReactMapApp = React.createClass
  getInitialState: () ->
    console.log "React App: started"

    @colors = []
    _.each @props.route.majorType, (color) =>
      @colors.push([color.id,color.color])


    @search = {
      price_from:               @props.location.query.price_from          || ""
      price_to:                 @props.location.query.price_to            || ""
      area_from:                @props.location.query.area_from           || ""
      area_to:                  @props.location.query.area_from           || ""
      yearbuilt_from:           @props.location.query.yearbuilt_from      || ""
      yearbuilt_to:             @props.location.query.yearbuilt_to        || ""
      nbfloors_from:            @props.location.query.nbfloors_from       || ""
      nbfloors_to:              @props.location.query.nbfloors_to         || ""
      reference:                @props.location.query.reference           || ""
      keywords:                 @props.location.query.keywords            || ""
      status:                   @props.location.query.status              || ""
      majortransaction:         @props.location.query.majortransaction    || ""
      majortype:                @props.location.query.majortype           || ""
      mimortype:                @props.location.query.mimortype           || ""
      bedrooms:                 @props.location.query.bedrooms            || ""
      bathrooms:                @props.location.query.bathrooms           || ""
      publication_dates:        @props.location.query.publication_dates   || ""
      bounds:                   @props.location.query.bounds              || ""
      custom_poly:              @props.location.query.custom_poly         || ""
      custom_circle:            @props.location.query.custom_circle       || ""
      zones:                    @props.location.query.zones               || ""
      page:                     @props.location.query.page                || 1
      user:                     @props.location.query.user                || ""
    }
    return {
      apiResult: null
      apiResultSize: 0
      mode: @props.location.query.mode || "grid"
      switcher:  @props.location.query.switcher || "center"
    }

  mapReady: ->
    @loadDataFromApi(@search)


  loadDataFromApi: (search) ->
    ##============================================================##
    ## Étape 1 : on clone @state.search et on enlève les valeurs
    ## vides pour avoir une belle url
    ##============================================================##
    search_url = _.clone(search)

    _.each search_url , (v, k) ->
      delete search_url[k] if !v

    # console.log(decodeURIComponent(jQuery.param(search_url)))

    jQuery.ajax
      type: "get"
      url : "api/v1/search/listings?#{decodeURIComponent(jQuery.param(search_url))}&per_page=10000"
      headers:
        "apiKey":"123cpix",
        "apiToken":"jlr456"
      success: (api) =>
        @setState(
          apiResult: api
          apiResultSize: api.results.features.length
        )


  handleFiltersChanged: (params) ->
    ##============================================================##
    ## We remove all ads...so spin loader backs
    ##============================================================##
    @setState(apiResult: null)

    #============================================================##
    # We updated @search
    #============================================================##
    keys = _.keys(params)
    _.each keys, (key) =>
      @search[key] = params[key]

    ##============================================================##
    ## we update url
    ##============================================================##
    search_url = _.clone(@search)
    _.each search_url , (v, k) ->
      delete search_url[k] if !v
    History.push("/en/map?#{decodeURIComponent(jQuery.param(search_url))}")

    ##============================================================##
    ## we send data to API
    ##============================================================##
    @loadDataFromApi(@search)

  modeChanged: (mode) ->
    @setState(mode:mode)



  leftSwitch: ->
    sw =  if @state.switcher == "center" then "list" else "center"
    @setState(switcher: sw )

  rightSwitch: ->
    sw =  if @state.switcher == "center" then "map" else "center"
    @setState(switcher: sw)

  render: ->
    leftClass =
      switch @state.switcher
        when "map"   then "col-md-12"
        when "center" then "col-md-6"
        when "list"  then "hidden-xs hidden-sm hidden-md hidden-lg"

    rightClass =
      switch @state.switcher
        when "map"   then "hidden-xs hidden-sm hidden-md hidden-lg"
        when "center" then "col-md-6"
        when "list"  then "col-md-12"


    return(
      <div>
        <Filters size={@state.apiResultSize} search={@search} majorType={@props.route.majorType} majortransaction={@props.route.majortransaction} onFiltersChanged={@handleFiltersChanged} />
        <section className="page-content row ml0 mr0">
          <div ref="MapSection" id="map-section" className="#{leftClass}  full-height pl0 pr0">
             <Map apiResult={@state.apiResult} mapboxToken={@props.route.mapboxToken} mapThemes={@props.route.mapThemes} mapTheme={@props.route.mapTheme}  mapReady={@mapReady} colors={@colors} />
          </div>
          <Inveser leftSwitch={@leftSwitch} rightSwitch={@rightSwitch} />
          <div className="#{rightClass} list-section full-height pl0 pr0">
            <div className="section-head bb border-light-grey">
              <div className="tabs-ads-display pull-left mt0 ml25 mt5 mb-2">
                <TabsNav mode={@state.mode} onModeChanged={@modeChanged} />
              </div>
            </div>
            {if @state.switcher != "map" then <ProperiesList apiResult={@state.apiResult} mode={@state.mode} switcher={@state.switcher} /> }
          </div>
        </section>
      </div>
    )



##============================================================##
## Inveser
##============================================================##
Inveser = React.createClass
  leftSwitch: ->
    @props.leftSwitch()

  rightSwitch: ->
    @props.rightSwitch()

  render: () ->
    return(
      <div className="middle-separator br border-light-grey centered-content relative">
        <div className="col-switch relative">
          <div onClick={@leftSwitch} className="switch-left-wrap inline-block">
            <div className="switch-left button-secondary pl10 pull-left">
              <i className="fa fa-chevron-left color-white"></i>
            </div>
          </div>
          <div onClick={@rightSwitch} className="switch-right-wrap inline-block">
            <div className="switch-right button-secondary pr10 text-right pull-right">
              <i className="fa fa-chevron-right color-white"></i>
            </div>
          </div>
          <div className="separator"></div>
        </div>
      </div>
    )


##============================================================##
## TabsNav
##============================================================##
TabsNav = React.createClass
  getInitialState: ->
    return{
      mode: @props.mode
    }
  
  updateMode: (mode) ->
    if mode isnt @state.mode
      @setState(mode:mode)
      @props.onModeChanged(mode)

  render: () ->
    return(
      <ul className="list-inline mb0">
        <li><div onClick={@updateMode.bind(@,'grid')}   className="inline-block pl20 pr20 pointer #{if @state.mode == 'grid' then 'bg-light-grey bAll border-grey' else 'bg-secondary color-white'}"><i className="fa fa-th mr5 color-primary"></i><span className="uppercase fs13">Grid</span></div></li>
        <li><div onClick={@updateMode.bind(@,'list')}   className="inline-block pl20 pr20 pointer #{if @state.mode == 'list' then 'bg-light-grey bAll border-grey' else 'bg-secondary color-white'}"><i className="fa fa-list-ul mr5 color-primary"></i><span className="uppercase fs13">List</span></div></li>
        <li><div onClick={@updateMode.bind(@,'table')}  className="inline-block pl20 pr20 pointer #{if @state.mode == 'table' then 'bg-light-grey bAll border-grey' else 'bg-secondary color-white'}"><i className="fa fa-calculator mr5 color-primary"></i><span className="uppercase fs13">Table</span></div></li>
      </ul>
    )












