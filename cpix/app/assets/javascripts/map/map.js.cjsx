
##============================================================##
## Map
##============================================================##
module.exports = React.createClass

  getInitialState: () ->
    @geoComparator = null
    return{
      theme: @props.mapTheme
    }

  componentDidMount: ->

    mapboxgl.accessToken = @props.mapboxToken

    @map =  new mapboxgl.Map
      container:  "MapBoxView"
      style:      "mapbox://styles/mapbox/#{@props.mapTheme}-v9"
      center: [-97.13917,49.89944]
      zoom: 3
      minZoom:3
      pitch: 45

    @map.addControl(new mapboxgl.Navigation())

    @map.on 'load', () =>
      console.log "Map: loaded"
      @setupMap()
      @props.mapReady()


  themeChanged: (theme) ->
    @map.setStyle("mapbox://styles/mapbox/#{theme}-v9")
    @map.on 'style.load', () =>
      "Map style: loaded"
      @setupMap()
      @setState
        theme: theme

  setupMap: (withFitMethod) ->
    ##============================================================##
    ## Add a Global Source
    ##============================================================##
    if @map.getSource("markers_source")? is false
      @map.addSource "markers_source",
        type: "geojson"
        data:
          type: "FeatureCollection"
          features: []
        cluster: true
        clusterMaxZoom: 10
        clusterRadius: 30


    ##============================================================##
    ## Add marker layer Stroke
    ##============================================================##
    @map.addLayer
      id: 'markers_layer_stroke'
      source: 'markers_source'
      type: "circle"
      paint:
        'circle-radius':
          base: 5
          stops: [[12,8],[14,16],[20, 190]]
        'circle-color': "#281D23"


    ##============================================================##
    ## Add marker layer
    ##============================================================##
    @map.addLayer
      id: 'markers_layer'
      source: 'markers_source'
      type: "circle"
      paint:
        'circle-radius':
          base: 3
          stops: [[12,6],[14,10],[20, 180]]
        'circle-color':
          property: "majortype"
          stops: @props.colors


    ##============================================================##
    ## Add Cluster Layer
    #============================================================##
    layers = [[150, '#FF614C',17], [20, '#FF614C',13],[0, '#FF614C',10]]
    _.each (layers),(layer,i) =>
      @map.addLayer
        id:     "cluster_layer_#{i}"
        type:   "circle"
        source: "markers_source"
        paint:
          'circle-color': layer[1]
          'circle-radius': layer[2]
        filter:
          if i == 0 then ['>=','point_count',layer[0]]
          else [
            'all'
            [ '>=','point_count',layer[0]]
            ['<','point_count',layers[i-1][0]]
          ]


    ##============================================================##
    ## Add Cluster legend
    ##============================================================##
    @map.addLayer
      id:     'cluster_number_layer'
      source: 'markers_source'
      type:   'symbol'
      layout:
        'text-field': "{point_count}"
        'text-font': ['DIN Offc Pro Medium','Arial Unicode MS Bold']
        'text-size': 10


    ##============================================================##
    ## PopUp
    ##============================================================##
    popup = new mapboxgl.Popup


    ##============================================================##
    ## Add Popup
    ##============================================================##
    current_location =
        x: null
        y: null
    current_marker = null
    current_propertieId = null
    @map.on 'mousemove', (e) =>
      mouseOnMarker = @map.queryRenderedFeatures(e.point,layers:['markers_layer'])

      @map.getCanvas().style.cursor = if (mouseOnMarker.length and mouseOnMarker[0].properties.index?) then 'pointer' else ''


      ##console.log('current_propertieId_' + current_propertieId?)       

      if popup.isOpen() is true

        if mouseOnMarker.length and mouseOnMarker[0].properties.index isnt current_marker
          popup.remove()
          return       

        distance =  Math.sqrt(Math.pow(Math.abs(e.point.x-current_location.x),2)+Math.pow(Math.abs(e.point.y-current_location.y),2))
        if distance > 80 
          popup.remove()
          return

        ##old_marker = current_marker
      else
        if mouseOnMarker.length and mouseOnMarker[0].properties.propertieId?
          @highlightListing(mouseOnMarker[0].properties.propertieId)

        if mouseOnMarker.length and mouseOnMarker[0].properties.index?
          feature           = mouseOnMarker[0]
          current_location  = e.point
          current_marker    = feature.properties.index
          current_propertieId = feature.properties.propertieId
          popup
            .setLngLat(feature.geometry.coordinates)
            .setHTML(feature.properties.html_popup)
            .addTo(@map)

    ##============================================================##
    ## Highlight and Scroll to top the listing on selected properties
    ## Not in a object du to a call from Mapbox Pointer 
    ##============================================================##
  highlightListing: (propertieId) ->
    selectedElement = $("#griditem_#{propertieId}")
    container = $('#listProperties')
    scrollTo = selectedElement

    ##console.log($("#griditem_#{propertieId}"))
    ##console.log('propertieId_' + propertieId)
    ##console.log('itemPosition_' + selectedElement?.position().top + '_itemOffset_' + selectedElement?.offset().top + '_height()_' + selectedElement?.height())
    ##console.log('containerPosition_' + container.scrollTop() + '_containerOffset_' + container.offset().top + '_height()_' + container?.height())
    container.animate({
        scrollTop: scrollTo.offset()?.top - container.offset()?.top + container.scrollTop()
    });    

    ##$('#listProperties').animate({
    ##  scrollTop: selectedElement.position().top
    ##}, 500); 
    $('.highlightItem').removeClass('highlightItem')
    selectedElement.addClass('highlightItem')

  render: ->

    if @props.apiResult?

    ##============================================================##
      ## Update GeoJson
      ##============================================================##
      if @map and @map.getSource("markers_source")? is true
        geojson = _.clone(@props.apiResult.results)

        if geojson.features.length > 0
          markersBounds   = new mapboxgl.LngLatBounds()
          newFeatures     = []
          colors          = {}

          g1  = _.groupBy geojson.features, (feature) ->
            return feature.properties.location_geohash
##{highlightListing(g3.properties.id)}
##return (function(){ var propertieId = #{g3.properties.id}; alert(propertieId); return true;})
          that = this;
          index = 0
          _.each g1, (g2) ->
            popUpContent =
              "<div class='color-grey fs14 pt0 pr10 pb5 pl10 bb border-light-grey bold ellipsis'>#{g2[0].properties.address}</div>
              <div class='property-list'>
              <table class='table table-condensed mb0'>"
            _.each g2, (g3) ->
              popUpContent +=
                "<tr id='popupCover_#{g3.properties.id}'>
                  <td class='popupCover' >
                    <a class='bg-cover block pointer' href='/en/ad/#{g3.properties.id}' target='_blank' style='background-image:url(#{g3.properties.cover_url});'></a>                      
                  </td>
                  <td class='popupContent'>
                    <table class='table mt0 mb0 table-condensed table-no-bordered'>
                      <tr>
                        <td colspan='2' class='center pr5 bold color-primary'>#{g3.properties.transaction_major_type.en}</td>
                      </tr>
                      <tr>
                        <td>Type</td>
                        <td class='a-right pr5 bold color-primary'>#{g3.properties.listing_major_type.en}</td>
                      </tr>
                      <tr>
                        <td>Area</td>
                        <td class='a-right pr5 bold color-primary'>#{g3.properties.area_formated}</td>
                      </tr>
                      <tr>
                        <td>Price</td>
                        <td class='a-right pr5 bold color-primary'>#{g3.properties.price_formated}</td>
                      </tr>
                    </table>
                  </td>
                </tr>"
            popUpContent +=
              "</table>
              </div>"

            colors[g2[0].properties.listing_major_type.id] = g2[0].properties.listing_major_type.color

            index = index+1
            newData =
              type: "Feature"
              geometry: g2[0].geometry
              properties:
                majortype: g2[0].properties.listing_major_type.id
                html_popup: popUpContent
                index: index
                propertieId: g2[0].properties.id


            markersBounds.extend(newData.geometry.coordinates)
            newFeatures.push(newData)

          geojson.features = newFeatures

          isTheSame   = JSON.stringify(@geoComparator) is JSON.stringify(geojson)
          @geoComparator  = geojson


          @map.getSource("markers_source").setData(geojson)


          if isTheSame is false
            @map.fitBounds(markersBounds,{padding:60})

    else
      if @map and @map.getSource("markers_source")? is true
        @map.getSource("markers_source").setData
          type: "FeatureCollection"
          features: []

    ##============================================================##
    ## We need to resize map when we switch view(left/right)
    ##============================================================##
    if @map
      @map.resize()

    return(
      <div className="map full-width relative">
        <div className="absolute absolute-top-left z-index-1000">
          <MapStyleSwitcher themes={@props.mapThemes}  theme={@props.mapTheme} onThemeChanged={@themeChanged}/>
        </div>
        <div className="search-when-moved absolute absolute-top-right mt15 mr50 bg-white z-index-1000">
           <label className="pt7 pb7 pl10 pr10 pointer">
            <input type="checkbox" id="search-when-moved-checkbox" />
            <span className="normal ml5 fs13 color-grey">Search when I move the map</span>
          </label>
        </div>
        <div id="MapBoxView"></div>
      </div>
    )

##============================================================##
## MapStyleSwitcher
##============================================================##
MapStyleSwitcher = React.createClass
  getInitialState: ->
    return{
      theme: @props.theme
    }

  changeStyle: (theme) ->
    if theme isnt @state.theme
      @setState(theme:theme)
      @props  ThemeChanged(theme)

  render: ->
    return(
      <div className="btn-group btn-group-sm mt5 ml5" role="group">
        {@props.themes.map((name) =>
          return <button type="button" onClick={@changeStyle.bind(@,name)} className="btn btn-default #{if @state.theme == name then 'bg-primary color-white bold' else ''}" key={name}>{name}</button>
        )}
      </div>
    )