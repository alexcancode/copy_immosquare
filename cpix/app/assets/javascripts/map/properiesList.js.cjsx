
##============================================================##
## List
##============================================================##
module.exports = React.createClass
  getInitialState: ->
    return{
      viewport:
        top: 0
        height: 0
    }



  onScroll: ->
    v=
      top: $(@refs.listContainer).scrollTop()
      height: $(@refs.listContainer).innerHeight()

    # console.log v
    # @setState(viewport:v)


  render: ->
    PropertiesNodes =
      if @props.apiResult? && @props.apiResult.results.features
        if @props.mode == "grid"
          @props.apiResult.results.features.map((listing) =>
            <PropertyGrid data={listing.properties} switcher={@props.switcher} key={listing.properties.id}  />
          )
        else if @props.mode == "list"
          @props.apiResult.results.features.map((listing) =>
            <PropertyList data={listing.properties} key={listing.properties.id}  />
          )
        else if @props.mode == "table"
          <PropertyTable data={@props.apiResult.results.features} />

      else
        <i className="fa fa-spinner fa-3x fa-spin" />

    return(
      <div ref="listContainer" id='listContainer' onScroll={@onScroll} className="ads-display border-light-grey pl25 pt20 pb10 bt full-width">
        <div id='listProperties' className="properties #{@props.mode}">
          {PropertiesNodes}
        </div>
      </div>
    )


##============================================================##
## Grid Detail
##============================================================##
PropertyGrid = React.createClass
  render: ->
    return(
      <div className="#{if @props.switcher == 'list' then 'col-lg-2 col-md-3 col-sm-4' else 'col-md-4 col-sm-6'}">
        <article id="griditem_#{@props.data.id}"  className="property mb30 relative bAll2 border-light-grey">
          <div className="absolute absolute-top-left">
            <div className="bg-white mt2 ml2 pl5 pr5 pt2 pb2 bold color-primary">
              {@props.data.transaction_major_type.en}
            </div>
          </div>
          <a href="/en/ad/#{@props.data.id}">
            <img src={@props.data.cover_url} className="img-responsive"/>
          </a>
          <div className="title centered-content pt10 pb5">
            <div className="price">{@props.data.price_formated}</div>
            <div className="full-width">
              <div className="ellipsis h4 fs14">{@props.data.address}</div>
              <div className="ellipsis h5 bold mt3">{@props.data.locality}</div>
            </div>
          </div>
          <footer className="pt15 pb10 pl15 pr15 bg-white fs13 bAll border-light-grey">
            <div className="row">
              <div className="col-xs-6">
                <div className="ellipsis">Property Type</div>
              </div>
              <div className="col-xs-6">
                <div className="ellipsis">{@props.data.listing_major_type.en}</div>
              </div>
            </div>
            <hr className="mt4 mb4"/>
            <div className="row">
              <div className="col-xs-6">
                <div className="ellipsis">Total floor area</div>
              </div>
              <div className="col-xs-6">
                <div className="ellipsis">{@props.data.area_formated}</div>
              </div>
            </div>
          </footer>
        </article>
      </div>
    )


##============================================================##
## List Detail
##============================================================##
PropertyList = React.createClass

  render: ->
    return(
      <div id="griditem_#{@props.data.id}" className="property bAll2 border-grey">
        <table className="table table-no-bordered table-condensed mb0">
          <tbody>
            <tr>
              <td className="listImage relative">
                <div className="absolute absolute-top-left">
                  <div className="bg-white mt4 ml5 pl5 pr5 pt2 pb2 bold color-primary">
                    {@props.data.transaction_major_type.en}
                  </div>
                </div>
                <a href="/en/ad/#{@props.data.id}">
                  <img src={@props.data.cover_url} className="img-responsive"/>
                </a>
              </td>
              <td className="">
                <div className="pl15">
                  <div className="pull-right h4 color-primary bold">{@props.data.price_formated}</div>
                  <div className="h4">{@props.data.address}, {@props.data.locality}</div>
                  <div className="row">
                    <div className="col-md-6">
                      <table className="table table-condensed table-bordered mb0 mt5">
                        <tbody>
                          <tr>
                            <td className="pt0 pb0">Listing #</td>
                            <td className="pt0 pb0">{@props.data.id}</td>
                          </tr>
                          <tr>
                            <td className="pt0 pb0">Property Type</td>
                            <td className="pt0 pb0">{@props.data.listing_major_type.en}</td>
                          </tr>
                          <tr>
                            <td className="pt0 pb0">Sub Type</td>
                            <td className="pt0 pb0">-</td>
                          </tr>
                          <tr>
                            <td className="pt0 pb0">Total floor area</td>
                            <td className="pt0 pb0">{@props.data.area_formated}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )


##============================================================##
## List Detail
##============================================================##
PropertyTable = React.createClass
  componentDidMount: ->
    sorttable.makeSortable($(@refs.sorttable)[0])

  render: ->
    return(
      <table ref="sorttable" className="table table-striped table-bordered full-width bAll2 border-light-grey sortable table-valign-middle">
        <thead className="fs13 bg-secondary color-white">
          <tr>
            <th className="pl10 pr10 pt5 pb5">Listing ID</th>
            <th className="pl10 pr10 pt5 pb5">Transaction Type</th>
            <th className="pl10 pr10 pt5 pb5">Address</th>
            <th className="pl10 pr10 pt5 pb5">Price/Lease</th>
            <th className="pl10 pr10 pt5 pb5">Property Type</th>
            <th className="pl10 pr10 pt5 pb5">Building area</th>
          </tr>
        </thead>
        <tbody className="bg-white">
          {@props.data.map((feature) ->
            return(
              <tr key={feature.properties.id} id="griditem_#{feature.properties.id}">
                <td className="pl10 pr10 pt5 pb5" data-sorttable_customkey={feature.properties.id} ><a href="/en/ad/#{feature.properties.id}">{feature.properties.id}</a></td>
                <td className="pl10 pr10 pt5 pb5" data-sorttable_customkey={feature.properties.transaction_major_type.en} ><a href="/en/ad/#{feature.properties.id}">{feature.properties.transaction_major_type.en}</a></td>
                <td className="pl10 pr10 pt5 pb5" data-sorttable_customkey={feature.properties.locality}><a href="/en/ad/#{feature.properties.id}">{feature.properties.address}<br/>{feature.properties.locality}</a></td>
                <td className="pl10 pr10 pt5 pb5" data-sorttable_customkey={feature.properties.price}><a href="/en/ad/#{feature.properties.id}">{feature.properties.price_formated}</a></td>
                <td className="pl10 pr10 pt5 pb5" data-sorttable_customkey={feature.properties.listing_major_type.en}><a href="/en/ad/#{feature.properties.id}">{feature.properties.listing_major_type.en}</a></td>
                <td className="pl10 pr10 pt5 pb5"><a href="/en/ad/#{feature.properties.id}">{feature.properties.area_formated}</a></td>
              </tr>
            )
          )}
        </tbody>
      </table>
    )
