$('body.listing.listing_location').ready ->

  if google? and mapboxgl? and $('#googleMapGeocoder').length
    div_geocoder = $('#googleMapGeocoder')

    mapboxgl.accessToken =  div_geocoder.data("mapboxtoken")
    map = new mapboxgl.Map
      container: 'map'
      style: 'mapbox://styles/mapbox/streets-v9'
      center: [div_geocoder.data("lng"),div_geocoder.data("lat")]
      zoom: 9


    map.on 'load', () ->
      console.log "Map loaded"

      if div_geocoder.data("marker") is true
        el = document.createElement('div')
        el.className = 'block'
        el.style.backgroundImage = 'url(/map_icons/1.png)'
        el.style.width = '26px'
        el.style.height = '42px'

        @marker = new mapboxgl.Marker(el, {offset: [-13, -21]})
          .setLngLat([div_geocoder.data("lng"),div_geocoder.data("lat")])
          .addTo(map)

      autocomplete = new google.maps.places.Autocomplete div_geocoder[0],
        types: [ 'geocode' ]
        componentRestrictions: {country: "ca"}

      google.maps.event.addListener autocomplete, 'place_changed', ->
        performGeocoding(autocomplete.getPlace())

      performGeocoding = (place) ->
        if !place.geometry
          alert 'Autocomplete\'s returned place contains no geometry'
        else
          lat = place.geometry.location.lat()
          lng = place.geometry.location.lng()
          lng_lat = [lng,lat]

          el = document.createElement('div')
          el.className = 'block'
          el.style.backgroundImage = 'url(/map_icons/1.png)'
          el.style.width = '26px'
          el.style.height = '42px'

          if @marker
            @marker.setLngLat(lng_lat)
          else
            @marker = new mapboxgl.Marker(el, {offset: [-13, -21]})
              .setLngLat(lng_lat)
              .addTo(map)


          map.flyTo
            center: lng_lat


          # Step #2 form attributes
          $("#building_address_latitude").val(lat)
          $("#building_address_longitude").val(lng)


          componentForm =
            street_number:
              long_or_short:'short_name'
              my_input_id: 'street_number'
            route:
              long_or_short: 'long_name'
              my_input_id: 'street_name'
            neighborhood:
              long_or_short: 'long_name'
              my_input_id: 'sublocality'
            locality:
              long_or_short: 'long_name'
              my_input_id: 'locality'
            administrative_area_level_2:
              long_or_short: 'long_name'
              my_input_id: 'administrative_area_level_2'
            administrative_area_level_1:
              long_or_short: 'long_name'
              my_input_id: 'administrative_area_level_1'
            country:
              long_or_short: 'short_name'
              my_input_id: 'country'
            postal_code_prefix:
              long_or_short: 'short_name'
              my_input_id: 'zipcode'
            postal_code:
              long_or_short: 'short_name'
              my_input_id: 'zipcode'

          i = 0
          while i < place.address_components.length
            addressType = place.address_components[i].types[0]
            if componentForm[addressType]
              val = place.address_components[i][componentForm[addressType].long_or_short]
              $("#building_address_#{componentForm[addressType].my_input_id}").val(val).trigger('change')
            i++
