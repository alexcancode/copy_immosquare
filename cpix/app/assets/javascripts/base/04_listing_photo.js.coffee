$('body.listing.listing_photo').ready ->

  $('#input_file_button').on 'click', (e) ->
    $('.input-file').trigger "click"
 
  ##============================================================##
  ## FileUpload Pictures
  ##============================================================##
  $("#new_photo").fileupload
    dataType: 'script'
    dropZone: $('#dropzone')
    add: (e, data) ->
      types = /(\.|\/)(gif|jpe?g|png|mov|mpeg|mpeg4|avi)$/i
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        data.context = $(tmpl("template-upload", file))
        $("#new_photo").append(data.context)
        data.submit()
      else
        swal("Error","#{file.name} is not a gif, jpg or png image file",'error')
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.progress-bar').css('width', progress + '%')
    done: (e,data) ->


  ##============================================================##
  ## Effect on dragover
  ##============================================================##
  $(document).bind 'dragover', (e) ->
    dropZone = $('#dropzone')
    timeout = window.dropZoneTimeout
    if !timeout
      dropZone.addClass 'in'
    else
      clearTimeout timeout
    found = false
    node = e.target
    loop
      if node == dropZone[0]
        found = true
        break
      node = node.parentNode
      unless node != null
        break
    if found
      dropZone.addClass 'hover'
    else
      dropZone.removeClass 'hover'
    window.dropZoneTimeout = setTimeout((->
      window.dropZoneTimeout = null
      dropZone.removeClass 'in hover'
      return
    ), 100)
    return


  #============================================================##
  # Sortable Pictures
  #============================================================##
  if $('#my-ui-list-picture').length
    Sortable.create document.getElementById("my-ui-list-picture"),
      store:
        get: (sortable) ->
          order = ""
          if order then order.split('|') else []
        set: (sortable) ->
          $.ajax
            type: "POST"
            url: $('#my-ui-list-picture').data("sort_url")
            data:
              my_order: sortable.toArray()


