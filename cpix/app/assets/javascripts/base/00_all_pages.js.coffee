##============================================================##
## General Setup
##============================================================##
$(document).ready ->
  $('.date').pickadate()


$("body.front_office.home").ready ->

  ##============================================================##
  ## Google Autocomplete for search
  ##============================================================##
  if google? and $('#googleMapGeocoder').length
    autocomplete = new google.maps.places.Autocomplete $('#googleMapGeocoder')[0],
      types: ['geocode']
      componentRestrictions: {country: "ca"}

    google.maps.event.addListener autocomplete, 'place_changed', ->
      place = autocomplete.getPlace()
      if !place.geometry
        alert 'Autocomplete\'s returned place contains no geometry'
      if place.geometry.viewport
        $('#search_bounds').val place.geometry.viewport.toUrlValue()
      else
        console.log place.geometry.location
      return




$(".front_office").ready ->
  ##============================================================##
  ## Expand card
  ##============================================================##
  $(".expandable-card").each ->
    card = $(@)
    $(".expandable-button", @).on "click", ->
      $(".expandable-area", card).slideToggle()
      $(@).toggleClass 'opened'



  ##============================================================##
  ## Disable form submit with enter
  ##============================================================##
  $("form").keydown (event) ->
    if event.keyCode == 13
      event.preventDefault()
      return false

  ##============================================================##
  ## New Carousel with Owl Carousel
  ##============================================================##
  syncPosition = (el) ->
    current = @currentItem
    $("#sync2").find(".owl-item").removeClass("synced").eq(current).addClass "synced"
    center current  if $("#sync2").data("owlCarousel") isnt `undefined`

  center = (number) ->
    sync2visible = sync2.data("owlCarousel").owl.visibleItems
    num = number
    found = false
    for i of sync2visible
      found = true  if num is sync2visible[i]
    if found is false
      if num > sync2visible[sync2visible.length - 1]
        sync2.trigger "owl.goTo", num - sync2visible.length + 2
      else
        num = 0  if num - 1 is -1
        sync2.trigger "owl.goTo", num
    else if num is sync2visible[sync2visible.length - 1]
      sync2.trigger "owl.goTo", sync2visible[1]
    else sync2.trigger "owl.goTo", num - 1  if num is sync2visible[0]

  sync1 = $("#sync1")
  sync2 = $("#sync2")

  sync1.owlCarousel
    singleItem: true
    slideSpeed: 1000
    navigation: false
    pagination: false
    afterAction: syncPosition
    responsiveRefreshRate: 200

  sync2.owlCarousel
    items: 4
    itemsDesktop: [
      1199
      3
    ]
    itemsDesktopSmall: [
      979
      6
    ]
    itemsTablet: [
      768
      5
    ]
    itemsMobile: [
      479
      4
    ]
    pagination: false
    responsiveRefreshRate: 100
    afterInit: (el) ->
      el.find(".owl-item").eq(0).addClass "synced"
      return

  $("#sync2").on "click", ".owl-item", (e) ->
    e.preventDefault()
    number = $(this).data("owlItem")
    sync1.trigger "owl.goTo", number

  $(".shareimmo_control_left").on 'click',(e) ->
    sync1.trigger('owl.prev')

  $(".shareimmo_control_right").on 'click', (e) ->
    sync1.trigger('owl.next');

  $('#sync1').magnificPopup
    delegate: 'a'
    type: 'image'
    gallery: enabled: true


  ##============================================================##
  ## Mobile menu deployment
  ##============================================================##
  menu_button = $('.mobile-nav-button')
  menu_button.on 'click', ->
    $('#main-nav').toggleClass 'opened'
  # Close the menu
  $('#main-nav').on 'click', (e) ->
    if e.target != this
      return
    $(@).removeClass 'opened'
    return

$('.front_office.home').ready ->
  ##============================================================##
  ## Home search tabs switching
  ##============================================================##
  search = $('#advanced-search')
  $('.tabs li', search).on 'click', ->
    $('.tabs li', search).removeClass('bg-primary').addClass('bg-secondary')
    $(@).addClass('bg-primary').removeClass('bg-secondary')
    tab         = $(@).data('tab')
    transaction = $(@).data('majortransaction')
    $("#search_majortransaction").val(transaction)
