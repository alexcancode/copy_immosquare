$('body.dashboard').ready ->


  if $("#mapDashboard").length

    ##============================================================##
    ## Update GeoJson for map use
    ##============================================================##
    datas           = $('#mapDashboard').data()
    markersBounds   = new mapboxgl.LngLatBounds()
    newFeatures     = []
    colors          = {}
    mapTheme        = datas.maptheme

    g1  = _.groupBy datas.geojson.features, (feature) ->
      return feature.properties.location_geohash

    index = 0
    _.each g1,(g2) ->
      popUpContent =
        "<div class='color-grey fs14 pt0 pr10 pb5 pl10 bb border-light-grey bold ellipsis'>#{g2[0].properties.address}</div>
        <div class='property-list'>
        <table class='table table-condensed mb0'>"
      _.each g2, (g3) ->
        popUpContent +=
          "<tr>
            <td class='popupCover'>
              <a class='bg-cover block pointer' href='/en/ad/#{g3.properties.id}' target='_blank' style='background-image:url(#{g3.properties.cover_url});'></a>
            </td>
            <td class='popupContent'>
              <table class='table mt0 mb0 table-condensed table-no-bordered'>
                <tr>
                  <td colspan='2' class='center pr5 bold color-primary'>#{g3.properties.transaction_major_type.en}</td>
                </tr>
                <tr>
                  <td>Type</td>
                  <td class='a-right pr5 bold color-primary'>#{g3.properties.listing_major_type.en}</td>
                </tr>
                <tr>
                  <td>Area</td>
                  <td class='a-right pr5 bold color-primary'>#{g3.properties.area_formated}</td>
                </tr>
                <tr>
                  <td>Price</td>
                  <td class='a-right pr5 bold color-primary'>#{g3.properties.price_formated}</td>
                </tr>
              </table>
            </td>
          </tr>"
      popUpContent +=
        "</table>
        </div>"

      colors[g2[0].properties.listing_major_type.id] = g2[0].properties.listing_major_type.color

      index = index+1
      newData =
        type: "Feature"
        geometry: g2[0].geometry
        properties:
          majortype: g2[0].properties.listing_major_type.id
          html_popup: popUpContent
          index: index


      markersBounds.extend(newData.geometry.coordinates)
      newFeatures.push(newData)


    datas.geojson.features = newFeatures


    ##============================================================##
    ## Map Setup
    ##============================================================##
    mapboxgl.accessToken = datas.token
    map = new mapboxgl.Map
      container: 'mapDashboard'
      style: "mapbox://styles/mapbox/#{mapTheme}-v9"
      center: [0,0]
      zoom: 4
      pitch: 45

    map.addControl(new mapboxgl.Navigation())


    ##============================================================##
    ## Map Event & Handlers
    ##============================================================##
    $('#mapStyleSwitcher button').on 'click', () ->
      theme = $(@).data('style')
      if mapTheme != theme
        mapTheme = theme
        $("#mapStyleSwitcher button").removeClass('bg-primary color-white bold')
        $(@).addClass('bg-primary color-white bold')
        map.setStyle("mapbox://styles/mapbox/#{theme}-v9")
        map.on 'style.load', () ->
          setup_map()



    ##============================================================##
    ## First map Init
    ##============================================================##
    map.on 'load', () ->
      setup_map()
      map.fitBounds(markersBounds)



    setup_map = () ->
      ##============================================================##
      ## Add a Global Source
      ##============================================================##
      if map.getSource("markers_source")? is false
        map.addSource "markers_source",
          type: "geojson"
          data: datas.geojson
          cluster: true
          clusterMaxZoom: 16
          clusterRadius: 30


      ##============================================================##
      ## Add marker layer Stroke
      ##============================================================##
      map.addLayer
        id: 'markers_layer_stroke'
        source: 'markers_source'
        type: "circle"
        paint:
          'circle-radius':
            base: 5
            stops: [[12,8],[14,16],[20, 190]]
          'circle-color': "#281D23"

      ##============================================================##
      ## Add marker layer
      ##============================================================##
      map.addLayer
        id: 'markers_layer'
        source: 'markers_source'
        type: "circle"
        paint:
          'circle-radius':
            base: 3
            stops: [[12,6],[14,10],[22, 180]]
          'circle-color':
            property: "majortype"
            stops: _.pairs(colors)


      #============================================================##
      # Add Cluster Layer
      #============================================================##
      layers = [[150, '#FF614C',17], [20, '#FF614C',13],[0, '#FF614C',10]]
      _.each (layers),(layer,i) ->
        map.addLayer
          id:     "cluster_layer_#{i}"
          type:   "circle"
          source: "markers_source"
          paint:
            'circle-color': layer[1]
            'circle-radius': layer[2]
          filter:
            if i == 0 then ['>=','point_count',layer[0]]
            else [
              'all'
              [ '>=','point_count',layer[0]]
              ['<','point_count',layers[i-1][0]]
            ]



      ##============================================================##
      ## Add Cluster legend
      ##============================================================##
      map.addLayer
        id:     'cluster_number_layer'
        source: 'markers_source'
        type:   'symbol'
        layout:
          'text-field': "{point_count}"
          'text-font': ['DIN Offc Pro Medium','Arial Unicode MS Bold']
          'text-size': 10


      ##============================================================##
      ## PopUp
      ##============================================================##
      popup = new mapboxgl.Popup


      ##============================================================##
      ## Add Popup
      ##============================================================##
      current_location =
          x: null
          y: null
      current_marker = null
      map.on 'mousemove', (e) ->
        mouseOnMarker = map.queryRenderedFeatures(e.point,layers:['markers_layer'])

        map.getCanvas().style.cursor = if (mouseOnMarker.length and mouseOnMarker[0].properties.index?) then 'pointer' else ''

        if popup.isOpen() is true
          if mouseOnMarker.length and mouseOnMarker[0].properties.index isnt current_marker
            popup.remove()
            return

          distance =  Math.sqrt(Math.pow(Math.abs(e.point.x-current_location.x),2)+Math.pow(Math.abs(e.point.y-current_location.y),2))
          if distance > 80
            popup.remove()
            return
        else
          if mouseOnMarker.length and mouseOnMarker[0].properties.index?
            feature           = mouseOnMarker[0]
            current_location  = e.point
            current_marker    = feature.properties.index
            popup
              .setLngLat(feature.geometry.coordinates)
              .setHTML(feature.properties.html_popup)
              .addTo(map)


