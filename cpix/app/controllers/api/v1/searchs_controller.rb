module Api
  module V1
    class SearchsController < Api::V1::ApplicationController


      def listings
        begin
          raise "Your ApiKey/ApiToken are not authorized for search"  if @api_app.search_authorized_users.blank?
          params[:user] = @api_app.search_authorized_users            if @api_app.search_authorized_users != "*"
          @response = Listing.my_search_elastic(params)
          @index = "listings"
          render 'api/v1/searchs/elastic'
        rescue Exception => e
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end



      def listings_show
        begin
          raise "Your ApiKey/ApiToken are not authorized for search" if @api_app.search_authorized_users.blank?
          @listing = Listing.find(params[:id])
          if @api_app.search_authorized_users != "*"
            raise "Not authorized" if !@api_app.search_authorized_users.split(',').include?(@listing.user_id.to_s)
          end
          render 'api/v1/listings/retrieve'
        rescue Exception => e
          return render :json =>{:errors=>e.message}, :status=> 400
        end
      end









    end
  end
end
