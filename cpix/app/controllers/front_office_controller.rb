class FrontOfficeController < ApplicationController

  def home
    api_url   = "#{@apiBaseUrl}/search/listings?apiKey=#{ENV['jlr_api_key']}&apiToken=#{ENV['jlr_api_token']}&per_page=4&user=8440"
    response  = HTTParty.get(api_url)
    @response = JSON.parse(response.body).deep_symbolize_keys if response.code == 200
  end

  def search_cleaner
    new_params = {}
    params[:search].each do |p|
      next if !["majortype","bounds","majortransaction"].include?(p) or params[:search][p].blank?
      new_params[p] = params[:search][p]
    end if params[:search].present?
    new_params[:majortype] = ListingMajorType.all.pluck(:id).join(',') if params[:majortype].blank?
    redirect_to "#{front_office_map_path}?#{URI.unescape(new_params.to_query)}"
  end

  def map
    @datas = {
      :mapboxtoken      =>  ENV['MAPBOX_API_TOKEN'],
      :majortype        =>  ListingMajorType.all.select(:id,:name,:color),
      :majortransaction =>  TransactionMajorType.all.select(:id,:name),
      :logo             =>  ActionController::Base.helpers.asset_path("logo-secondary.png"),
      :current_user     =>  current_user.present?,
      :maptheme         =>  (current_user.present? and current_user.current_map_theme.present?) ? current_user.current_map_theme : 'bright',
      :mapthemes        =>  @mapTheme,
    }
  end



  def show
    respond_to do |format|
      api_url     = "#{@apiBaseUrl}/search/listings/#{params[:id]}?apiKey=#{ENV['jlr_api_key']}&apiToken=#{ENV['jlr_api_token']}"
      response    = HTTParty.get(api_url)
      @listing    = JSON.parse(response.body).deep_symbolize_keys
      format.html do
        response    = HTTParty.get("#{@apiBaseUrl}/search/listings?apiKey=#{ENV['jlr_api_key']}&apiToken=#{ENV['jlr_api_token']}&per_page=3&user=8440")
        @listings   = JSON.parse(response.body).deep_symbolize_keys
      end
      format.pdf do
        pdf         = "#{Rails.root}/app/pdfs/listing.pdf"
        pdf_result  = "#{Rails.root}/tmp/pdfs/listing_#{params[:id]}.pdf"
        dictionary  = Mypdflib::PdfParser.new(:pdf =>pdf).parse
        dictionary  = DictionaryPopulater.new(dictionary,@listing).populate_dictionary
        pdf         = Mypdflib::PdfWritter.new(:original =>pdf,:result => pdf_result, :dictionary =>dictionary).produce
        send_file(pdf_result, :filename => "listing_#{params[:id]}.pdf", :type => "application/pdf",:disposition => params[:print].present? ? :inline : :attachment )
      end
    end
  end




  def contact_mail
    # response = HTTParty.get("https://www.google.com/recaptcha/api/siteverify?secret=#{ENV['RECAPTCHA_SECRET_KEY']}&response=#{params['g-recaptcha-response']}")
    contact = Contact.new(contact_params)
    # if JSON.parse(response.body)["success"] == true
      if contact.save
        flash[:notice] = t("contact.success_message")
        ContactMailer.send_contact_mail(contact).deliver_later
      else
        flash[:danger] = t("contact.error_message", :error => contact.errors.full_messages.join(", "))
      end
    # else
      # flash[:danger] = t("contact.error_message_captcha")
    # end
    redirect_to :back
  end

  private
  def contact_params
    params.require(:contact).permit!
  end

end
