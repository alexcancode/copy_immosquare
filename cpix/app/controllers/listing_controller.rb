class ListingController < ApplicationController

  ##============================================================##
  ##
  ##============================================================##
  before_action :authenticate_user!
  before_action :listing_completeness

  ##============================================================##
  ##Listing
  ##============================================================##
  def listing_new
    listing = Listing.where(:draft => 1,:user_id => current_user.id).first_or_create
    redirect_to  dashboard_listing_edit_path(listing.id)
  end

  def listing_edit
    @listing = Listing.find(params[:id])
    @listing.rooms.build
  end

  def listing_update
    @listing = Listing.find(params[:id])
    @listing.update(listing_params)
    redirect_to params[:redirect]
  end
  ##============================================================##
  ##Location
  ##============================================================##
  def listing_location
    @listing = Listing.find(params[:id])
    if @listing.building.blank?
      building = Building.new
      building.save(:validate =>false)
      @listing.update_attribute(:building_id, building.id)
    end
    @building = @listing.building
    @building.build_address
  end

  ##============================================================##
  ##Building
  ##============================================================##
  def listing_building
    @listing = Listing.find(params[:id])
    if @listing.building.blank?
      building = Building.new
      building.save(:validate =>false)
      @listing.update_attribute(:building_id, building.id)
    end
    @building = @listing.building
  end

  def listing_building_update
    @building = Building.find(params[:id])
    @building.update(building_params)
    redirect_to params[:redirect]
  end

  ##============================================================##
  ##Broker
  ##============================================================##
  def listing_broker
    @listing = Listing.find(params[:id])
  end


  ##============================================================##
  ##Photo
  ##============================================================##
  def listing_photo
    @listing = Listing.find(params[:id])
    @photos = @listing.photos
  end

  def listing_photo_create
    @listing = Listing.find(params[:id])
    @photo = Photo.new(photo_params)
    @photo.save
  end

  def listing_photo_delete
    @listing = Listing.find(params[:id])
    @photo = Photo.find(params[:pic_id])
    @photo.destroy
  end

  def listing_photo_sort
    params[:my_order].each_with_index do |id, index|
      @photo = Photo.find(id)
      @photo.update_column(:position, index+1)
    end
    render nothing: true
  end

  ##============================================================##
  ##Document
  ##============================================================##
  def listing_document
    @listing = Listing.find(params[:id])
    @documents = @listing.documents
  end

  def listing_document_download
    @listing = Listing.find(params[:id])
    @document = Document.find(params[:doc_id])
    send_file(@document.document.url, :filename => @document.document_file_name, :type => @document.document_content_type)
  end

  def listing_document_create
    @listing = Listing.find(params[:id])
    @document = Document.new(document_params)
    if @document.save
      redirect_to dashboard_listing_document_path(@listing)
    else
      JulesLogger.info @document.errors.full_messages
    end
    
  end

  def listing_document_edit
    @listing = Listing.find(params[:id])
    @document = Document.find(params[:doc_id])
  end

  def listing_document_update
    @listing = Listing.find(params[:id])
    @document = Document.find(params[:doc_id])
    @document.update(document_params)
    redirect_to dashboard_listing_document_path(@listing)
  end

  def listing_document_delete
    @listing = Listing.find(params[:id])
    @document = Document.find(params[:doc_id])
    @document.destroy
    redirect_to dashboard_listing_document_path(@listing)
  end

  ##============================================================##
  ##Tax
  ##============================================================##
  def listing_taxes
    @listing = Listing.find(params[:id])
    if @listing.building.blank?
      building = Building.new
      building.save(:validate =>false)
      @listing.update_attribute(:building_id, building.id)
    end
    @building = @listing.building
    @taxes = @building.taxes
  end

  def listing_tax_create
    @listing = Listing.find(params[:id])
    @tax = Tax.new(tax_params)
    @tax.save
    redirect_to dashboard_listing_taxes_path(@listing)
  end

  def listing_tax_edit
    @listing = Listing.find(params[:id])
    @building = @listing.building
    @tax = Tax.find(params[:tax_id])
  end

  def listing_tax_update
    @listing = Listing.find(params[:id])
    @tax = Tax.find(params[:tax_id])
    @tax.update(tax_params)
    redirect_to dashboard_listing_taxes_path(@listing)
  end

  def listing_tax_delete
    @listing = Listing.find(params[:id])
    @tax = Tax.find(params[:tax_id])
    @tax.destroy
    redirect_to dashboard_listing_taxes_path(@listing)
  end

  ##============================================================##
  ##Seller
  ##============================================================##
  def listing_seller
    @listing = Listing.find(params[:id])
  end

  ##============================================================##
  ##Equipment
  ##============================================================##
  def listing_equipment
    @listing = Listing.find(params[:id])
    @equipment = @listing.equipment 
  end

  def listing_equipment_create
    @equipment = Equipment.new(equipment_params)
    @equipment.save
    redirect_to dashboard_listing_equipment_path(@equipment.listing_id)
  end

  def listing_equipment_update
    @listing = Listing.find(params[:id])
    @equipment = Equipment.find(params[:e_id])
    @equipment.update(equipment_params)
    redirect_to dashboard_listing_equipment_path(@listing)
  end

  def listing_equipment_edit
    @listing = Listing.find(params[:id])
    @equipment = Equipment.find(params[:e_id])
  end

  def listing_equipment_delete
    @listing = Listing.find(params[:id])
    @equipment = Equipment.find(params[:e_id])
    @equipment.destroy
    redirect_to dashboard_listing_equipment_path(@listing)
  end

  ##============================================================##
  ##Room
  ##============================================================##
  def listing_room
    @listing = Listing.find(params[:id])
    @rooms = @listing.rooms
  end

  def listing_room_create
    @room = Room.new(room_params)
    @room.save
    redirect_to dashboard_listing_room_path(@room.listing_id)
  end

  def listing_room_update
    @listing = Listing.find(params[:id])
    @room = Room.find(params[:r_id])
    @room.update(room_params)
    redirect_to dashboard_listing_room_path(@listing)
  end

  def listing_room_edit
    @listing = Listing.find(params[:id])
    @room = Room.find(params[:r_id])
  end

  def listing_room_delete
    @listing = Listing.find(params[:id])
    @room = Room.find(params[:r_id])
    @room.destroy
    redirect_to dashboard_listing_room_path(@listing)
  end

  ##============================================================##
  ##Financing
  ##============================================================##
  def listing_financing
    @listing = Listing.find(params[:id])
  end

  ##============================================================##
  ##Lease
  ##============================================================##
  def listing_lease
    @listing = Listing.find(params[:id])
  end


  private

  def set_listing
    @listing = Listing.find(params[:id])
  end

  def set_building
    if @listing.building.blank?
      building = Building.new
      building.save(:validate =>false)
      @listing.update_attribute(:building_id, building.id)
    end
    @building = @listing.building
  end

  def listing_params
    params.require(:listing).permit!
  end

  def building_params
    params.require(:building).permit(:building_location_description, :prop_legal_address_lot, :prop_legal_address_block, :prop_legal_address_plan, :prop_legal_address_meridian, :prop_legal_address_range, :prop_legal_address_township, :prop_legal_address_section, :prop_legal_address_quarter, :prop_legal_address_subdivision, :prop_legal_address_freeform, :area, :area_sub_area, :zoning, :zoning_description, :address_attributes => [:id, :address, :street_number, :street_name, :sublocality, :locality, :country, :zipcode, :administrative_area_level_1, :administrative_area_level_2, :latitude, :longitude])
  end

  def address_params
    params.require(:address).permit!
  end

  def equipment_params
    params.require(:equipment).permit!
  end

  def room_params
    params.require(:room).permit!
  end

  def document_params
    params.require(:document).permit!
  end

  def photo_params
    params.require(:photo).permit!
  end

  def tax_params
    params.require(:tax).permit!
  end

  def listing_completeness
    if @listing.present? && @listing.attributes.present?
      @listing.listing_completeness = 100*(@listing.attributes.keys.length - @listing.attributes.values.select(&:nil?).count)/ @listing.attributes.keys.length
    end
  end

end
