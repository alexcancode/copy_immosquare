class DashboardController  < ApplicationController

  ##============================================================##
  ## Before Filters
  ##============================================================##
  before_action :authenticate_user!



  ##============================================================##
  ## This mehtod clean params from search form and redirect
  ## with cleaned params
  ##============================================================##
  def search_cleaner
    cleaned_params = {}
    params[:search].each do |p|
      next if !["keywords", "sort"].include?(p) or params[:search][p].blank?
      cleaned_params[p] = params[:search][p]
    end if params[:search].present?
    redirect_to "#{params[:redirect]}?view=#{params[:view].present? ? params[:view] : "grid"}&#{cleaned_params.to_query}"
  end


  ##============================================================##
  ## Lisintgs
  ##============================================================##
  def index
    sort = ""
    if params[:sort].present?
      sort = case params[:sort]
      when "pa" then "&order_by=price&order=asc"
      when "pd" then "&order_by=price&order=desc"
      when "type" then "&order_by=type&order=asc"
      end
    end
    api_url   = "#{@apiBaseUrl}/search/listings?apiKey=#{ENV['jlr_api_key']}&apiToken=#{ENV['jlr_api_token']}&user=#{current_user.id}#{params[:keywords].present? ? "&keywords=#{params[:keywords]}" : nil }#{sort}&per_page=10000"
    response  = HTTParty.get(api_url)
    @response = JSON.parse(response.body).deep_symbolize_keys if response.code == 200
    @brand    = "#{t('app.listing.my_listings')} (#{@response.present? ? @response[:results][:features].size : "0"})"
  end

  def offices
    p = "&user=#{current_user.id}"
    p = "&office=#{current_user.office.id}" if current_user.office.present?
    sort = ""
    if params[:sort].present?
      sort = case params[:sort]
      when "pa" then "&order_by=price&order=asc"
      when "pd" then "&order_by=price&order=desc"
      when "type" then "&order_by=type&order=asc"
      end
    end
    api_url   = "#{@apiBaseUrl}/search/listings?apiKey=#{ENV['jlr_api_key']}&apiToken=#{ENV['jlr_api_token']}#{p}#{params[:keywords].present? ? "&keywords=#{params[:keywords]}" : nil }#{sort}&per_page=10000"
    response  = HTTParty.get(api_url)
    @response = JSON.parse(response.body).deep_symbolize_keys if response.code == 200
    @brand    = "#{t('app.listing.office_listings')} (#{@response.present? ? @response[:results][:features].size : "0"})"
    render 'index'
  end

  def all_listings
    sort = ""
    if params[:sort].present?
      sort = case params[:sort]
      when "pa" then "&order_by=price&order=asc"
      when "pd" then "&order_by=price&order=desc"
      when "type" then "&order_by=type&order=asc"
      end
    end
    api_url   = "#{@apiBaseUrl}/search/listings?apiKey=#{ENV['jlr_api_key']}&apiToken=#{ENV['jlr_api_token']}#{params[:keywords].present? ? "&keywords=#{params[:keywords]}" : nil }#{sort}&per_page=10000"
    response  = HTTParty.get(api_url)
    @response = JSON.parse(response.body).deep_symbolize_keys if response.code == 200
    @brand    = "#{t('app.listing.all_listings')} (#{@response.present? ? @response[:results][:features].size : "0"})"
    render 'index'
  end


  ##============================================================##
  ## Profile
  ##============================================================##
  def profile
    @brand    = t('app.my_profile')
    @user     = current_user
  end

  def profile_update
    begin
      @user = User.find(params[:id])
      raise "Not Authorized" if @user != current_user
      if @user.update_attributes(params_user)
        redirect_to dashboard_path
      else
        @brand  = t('app.my_profile')
        render 'profile'
      end
    rescue Exception => e
      redirect_to dashboard_path
    end
  end



  private


  def params_user
    params.require(:user).permit!
  end




end
