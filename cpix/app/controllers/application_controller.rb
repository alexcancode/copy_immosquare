class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_action :set_locale
  before_action :set_app_name


  def after_sign_in_path_for(resource)
    dashboard_path
  end

  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {locale: I18n.locale}
  end


  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def set_app_name
    @appName    = "JLR"
    @apiBaseUrl = "#{root_url}/api/v1"
    @mapTheme   = ["streets","bright","light","dark"]
  end



end
