<% if @photo.errors.any? %>
swal("Failed to upload picture: <%= j @photo.errors.full_messages.join(', ').html_safe %>");
<% else %>
$('#new_photo')[0].reset()
$('#my-ui-list-picture').append('<%= j render :partial => "photo", locals: {photo: @photo}  %>')
<% end %>