# encoding: utf-8

@building = @listing.building  || Building.new
@address  = @building.address || Address.new
@user     = @listing.user || User.new

if @listing.transaction_type.present? and @listing.transaction_type.transaction_major_type_id == 2
  price           = @listing.lease_price_cents/100
  price_formated  = "#{humanized_money_with_symbol(@listing.lease_price)}/ft²"
  subtype         = @listing.lease_type.present? ? @listing.lease_type.name : nil
else
  price           = @listing.list_price_cents/100.00
  price_formated  = humanized_money_with_symbol(@listing.list_price)
  subtype         = @listing.sale_type.present? ? @listing.sale_type.name : nil
end




json.id @listing.id
json.address do
  json.latitude                     @address.latitude
  json.longitude                    @address.longitude
  json.zipcode                      @address.zipcode
  json.street_number                @address.street_number
  json.street_type                  @address.street_type
  json.street_name                  @address.street_name
  json.sublocality                  @address.sublocality
  json.locality                     @address.locality
  json.administrative_area_level_2  @address.administrative_area_level_2
  json.administrative_area_level_1  @address.administrative_area_level_1
  json.country                      @address.country
  json.map_static                   "https://api.mapbox.com/v4/mapbox.streets/pin-m-marker+e3a21d(#{@address.longitude},#{@address.latitude})/#{@address.longitude},#{@address.latitude},14/500x500@2x.png?access_token=#{ENV['MAPBOX_API_TOKEN']}"
end
json.transaction_type do
  json.id   @listing.transaction_type_id
  json.en   @listing.transaction_type.present? ? @listing.transaction_type.name : "-"
end
json.classification do
  json.major_type do
    json.id   @listing.listing_major_type_id
    json.en   @listing.listing_major_type.present? ? @listing.listing_major_type.name : "-"
  end
end
json.financial do
  json.price            price
  json.price_formated   price_formated
end
json.cover @listing.photos.present? ? @listing.photos[0].image.url(:large,:timestamp => false) : Photo.new.image.options[:default_url]
json.pictures do
  if @picture_in_progress.present? && @picture_in_progress == 1
    json.in_progess true
  else
    json.array! @listing.photos do |asset|
      json.small        asset.image? ? asset.image.url(:small,:timestamp => false)  : Photo.new.image.options[:default_url]
      json.medium       asset.image? ? asset.image.url(:medium,:timestamp => false) : Photo.new.image.options[:default_url]
      json.large        asset.image? ? asset.image.url(:large,:timestamp => false)  : Photo.new.image.options[:default_url]
      json.hd_small     asset.image? ? asset.image.url(:hd_s,:timestamp => false)   : Photo.new.image.options[:default_url]
      json.hd_medium    asset.image? ? asset.image.url(:hd_m,:timestamp => false)   : Photo.new.image.options[:default_url]
      json.hd           asset.image? ? asset.image.url(:hd,:timestamp => false)     : Photo.new.image.options[:default_url]
    end
  end
end
json.description     @listing.public_remarks
json.area do
  json.area          @building.land_total_size
  json.unit          "pc"
  json.area_formated @building.land_total_size.present? ? "#{number_with_delimiter(@building.land_total_size)} ft²" : ""
end
json.availability  @listing.list_date

json.user do
  json.uid                @user.uid
  json.first_name         @user.first_name
  json.last_name          @user.last_name
  json.business_phone     @user.business_phone
  json.business_phone_ext @user.business_phone_ext
  json.mobile_phone       @user.mobile_phone
  json.picture            @user.picture.url(:medium)
end
json.building do
  json.architecture_style       "Contempory"
  json.attached_style           "Detached"
  json.structure_type           "Distribution Warehouse"
  json.roof_construction        "Tar and gravel"
  json.number_of_floor          "12"
  json.parking_type             "Undergroud"
  json.building_amenities       "Common Area Indoors, Shopping Area, Separate Heating Controls, Separate Hydro Meters"
  json.green_building           "EnerGuide, ENERGY STAR, Envirohome, R-2000"
  json.cooling_type             "Central Air, Air Exchanger"
  json.ceiling_type             "Suspended, Open Beam"
  json.ceiling_height           "12 ft"
  json.ceiling_clearance        "11 ft"
  json.basement_type            "Full, Crawl Space"
  json.loading_type             "Loading Dock, Ramp Door"
  json.storage_type             "Storage in Basement, Outside Storage"
  json.year_build               "1978"
  json.construction_type        "Concrete - Block, Steel Frame"
  json.fondation                "Poured Concrete"
  json.signage                  "Signband, Pylon"
  json.elevators                "Hydrolic, Escalator"
  json.parking_spaces           "48"
  json.protection_systems       "Alarm System, Security guard, Full Sprinkler, Fire Alarm system, Smoke Detectors"
  json.leedwell                 "Platinum"
  json.flooring                 "Tile, Linoleum, Engineered hardwood"
  json.heating_type             "Radiant/Infra-red Heat, Cool Fan"
  json.heating_fuel             "Oil, Natural gas"
  json.exterior_finish          "Wood shingles, Brick, Sheathing brick paper"
  json.basement_development     "Finished"
  json.loading_quantity         "4"
  json.storeFront               "Street Level - Font, Street Level - Rear"
end
