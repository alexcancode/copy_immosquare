class ApplicationMailer < ActionMailer::Base
  default from: "jules@wgoupe.com"
  layout 'mailer'
end
