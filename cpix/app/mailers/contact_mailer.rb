class ContactMailer < ApplicationMailer

  default :template_path => "mailers/contact_mailer"

  def send_contact_mail(contact)
    @contact = contact
    mail(:to => ["contact@immosquare.com", contact.listing.user.email], :from => "admin@immosquare.com", :subject => "Contact cPix")
  end

end