class DictionaryPopulater

  include HtmlToPlainTextHelper
  include MoneyHelper

  def initialize(dictionary,listing)
    @dictionary   = dictionary.deep_symbolize_keys
    @listing      = listing.deep_symbolize_keys
  end


  ##============================================================##
  ## Send a Populated dictionary
  ## Il faut utiliser .clone sinon on change text[:name]
  ## dans decompose_defintion avec slice!
  ##============================================================##
  def populate_dictionary
    if @dictionary[:document].present?
      @dictionary[:document][:primary_color] = "#FF614C"
    end

    ##============================================================##
    ## Setup Text
    ##============================================================##
    @dictionary[:text].each do |text|
      text[:content] = decompose_defintion(text[:name].clone)
    end if @dictionary[:text].present?

    ##============================================================##
    ## Setup Image
    ##============================================================##
    @dictionary[:image].each do |image|
      image[:url] = self.send(image[:name])
    end if @dictionary[:image].present?


    ##============================================================##
    ## Setup Graphics
    ##============================================================##
    @dictionary[:graphics].each do |graphic|
      graphic[:content] = self.send(graphic[:name])
    end if @dictionary[:graphics].present?

    @dictionary
  end


  ##============================================================##
  ## _with_comma
  ## _with_newline
  ## _and_
  ##============================================================##
  def decompose_defintion(name)
    with_comma    = name.slice!('_with_comma').present?
    with_newline  = name.slice!('_with_newline').present?
    methods       = name.split('_and_')

    methods.collect! { |meth| self.send(meth) }

    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth}, "
      end
    end if with_comma == true


    methods.collect!.with_index do |meth,i|
      if meth == methods.last
        meth
      else
        methods[i+1].blank? ? meth : "#{meth}\n"
      end
    end if with_newline == true

    final = methods.join((with_comma == true || with_newline == true)  ? "" : " ")
    return final
  end

  ##============================================================##
  ## When Missing property is called
  ##============================================================##
  def method_missing(sym, *args, &block)
    JulesLogger.info "method missing => #{sym}"
    return nil
  end


  def broker_name
    "#{@listing[:user][:first_name]} #{@listing[:user][:last_name]}"
  end

  def broker_phone
    @listing[:user][:mobile_phone]
  end

  def building_description
    tab = []
    @listing[:building].to_a.in_groups_of(2,false).each do |prop|
      tab << [
        {:type => "string", :content => prop[0][0]},
        {:type => "string", :content => prop[0][1]},
        {:type => "string", :content => prop[1][0]},
        {:type => "string", :content => prop[1][1]}
      ]
    end
    tab
  end

  def property_description
    convert_to_text(@listing[:description])
  end

  def property_disponibility
    @listing[:availability]
  end

  def property_address
    "#{@listing[:address][:street_number]} #{@listing[:address][:street_name]}, #{@listing[:address][:zipcode]}"
  end


  def property_listing
    @listing[:transaction_type][:en]
  end

  def property_living_area
    @listing[:area][:area_formated]
  end

  def property_location
    @listing[:address][:locality]
  end

  def property_price
    @listing[:financial][:price_formated]
  end

  def property_reference
    "##{@listing[:id]}"
  end

  def property_room_count
    @listing[:building][:number_of_floor]
  end

  def property_sublocation
    @listing[:address][:sublocality]
  end

  def property_type
    @listing[:classification][:major_type][:en]
  end

  def image_1
    @listing[:cover]
  end

  def broker_photo
    @listing[:user][:picture]
  end

  def property_map
    @listing[:address][:map_static]
  end



end
