class BasementType < ApplicationRecord
	has_one :building, dependent: :nullify
end
