class Document < ApplicationRecord
	belongs_to :document_type
	belongs_to :document_availability
	belongs_to :documentable, polymorphic: true

  #============================================================##
  ## Paperclip : Document
  ##============================================================##
  has_attached_file :document,
  :path => "#{Rails.env}/documents/:id/:filename",
  :hash_secret => "some_secret",
  :default_url => "https://placehold.it/250X250"

  validates_attachment_content_type :document, :content_type => ['application/pdf', 'application/msword', 'text/plain']
end
