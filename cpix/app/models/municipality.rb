class Municipality < ApplicationRecord
	has_one :building, dependent: :nullify
	has_many :zoning, dependent: :nullify
end
