class DocumentAvailability < ApplicationRecord
	has_many :documents, dependent: :nullify
end
