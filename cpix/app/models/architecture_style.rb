class ArchitectureStyle < ApplicationRecord
	has_one :building, dependent: :nullify
end
