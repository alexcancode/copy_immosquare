class FireplaceType < ApplicationRecord
	has_many :buildings, dependent: :nullify
end
