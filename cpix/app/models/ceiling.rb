class Ceiling < ApplicationRecord
	has_many :buildings, dependent: :nullify
end
