class SiteFeature < ApplicationRecord
	has_many :buildings, dependent: :nullify
end
