class Appliance < ApplicationRecord
	has_one :building, dependent: :nullify
end
