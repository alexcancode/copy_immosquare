class ListingSubType < ApplicationRecord
	has_one :listing, dependent: :nullify
	belongs_to :listing_minor_type
end
