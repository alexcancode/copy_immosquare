class Contact < ApplicationRecord

  ##============================================================##
  ## Validations
  ##============================================================##
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  validates :message, presence: true

  belongs_to :listing, foreign_key: 'property_id'
end
