class SaleType < ApplicationRecord
	has_one :listing, dependent: :nullify
end
