class StructureType < ApplicationRecord
	has_one :building, dependent: :nullify
end
