class Area < ApplicationRecord

  ##============================================================##
  ## Association
  ##============================================================##
	has_one :building, dependent: :nullify
end
