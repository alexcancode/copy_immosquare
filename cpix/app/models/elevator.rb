class Elevator < ApplicationRecord
	has_one :building, dependent: :nullify
end
