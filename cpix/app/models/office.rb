class Office < ApplicationRecord
  has_one :user, dependent: :nullify
  belongs_to :address
end
