class Photo < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
	belongs_to :photoable, polymorphic: true

  acts_as_list scope: [:photoable_id, :photoable_type]

  ##============================================================##
  ## PaperClip Interoplation
  ##============================================================##
  Paperclip.interpolates :photoable_type do |attachment, style|
    attachment.instance.photoable_type.downcase
  end

  Paperclip.interpolates :photoable_id do |attachment, style|
    attachment.instance.photoable_id
  end

  ##============================================================##
  ## Paperclip : Property_asset
  ##============================================================##
  has_attached_file :image,
  :path => "#{Rails.env}/:photoable_type/:photoable_id/:style_:filename",
  :styles => {
    :small    => ["480x270#",   :jpg],
    :medium   => ["960x540#",   :jpg],
    :large    => ["1920x1080#", :jpg],
    :hd_s     => ["480x270",    :jpg],
    :hd_m     => ["960x540",    :jpg],
    :hd       => ["1920x1080",  :jpg],
    },
    :convert_options => {
      :small      => "-strip -quality 70",
      :medium     => "-strip -quality 80",
      :large      => "-strip -quality 100",
      :hd_s       => "-background black -gravity center -extent 480x270     -quality 70",
      :hd_m       => "-background black -gravity center -extent 960x540     -quality 80",
      :hd         => "-background black -gravity center -extent 1920x1080   -quality 100"
    },
    :default_url => "https://placehold.it/1920X1080"
    validates_attachment_content_type :image, :content_type => /\Aimage/
    validates_attachment_size :image, :less_than=>11.megabytes



end
