class LandType < ApplicationRecord
	has_one :building, dependent: :nullify
end
