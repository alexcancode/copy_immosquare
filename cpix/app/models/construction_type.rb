class ConstructionType < ApplicationRecord
	has_one :building, dependent: :nullify
end
