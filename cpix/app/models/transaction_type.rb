class TransactionType < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :major_transaction_type
  has_and_belongs_to_many :users, :join_table => :transaction_types_users
  has_one :listing, dependent: :nullify
end
