class Roof < ApplicationRecord
	has_one :building, dependent: :nullify
end
