class HeatingType < ApplicationRecord
	has_many :buildings, dependent: :nullify
end
