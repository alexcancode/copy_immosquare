class Building < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :photos, as: :photoable, dependent: :destroy
  has_many :documents, as: :documentable, dependent: :destroy
  has_many :taxes

  belongs_to :address
  has_one :listing

  belongs_to :structure_type
  belongs_to :attached_style
  belongs_to :construction_type
  belongs_to :construction_status
  belongs_to :architecture_style
  belongs_to :elevator
  belongs_to :signage
  belongs_to :parking
  belongs_to :uffi
  belongs_to :boma
  belongs_to :frontson
  belongs_to :appliance
  belongs_to :basement_development
  belongs_to :basement_type
  belongs_to :storefront
  belongs_to :area
  belongs_to :zoning
  belongs_to :landright
  belongs_to :waterfront
  belongs_to :site_feature
  belongs_to :fence_type
  belongs_to :road
  belongs_to :access
  belongs_to :view
  belongs_to :landscaping
  belongs_to :irrigation_type
  belongs_to :utility
  belongs_to :land_amenity
  belongs_to :building_amenity
  belongs_to :land_type
  belongs_to :fireplace_type
  belongs_to :fireplace_fuel
  belongs_to :communication
  belongs_to :water
  belongs_to :heating_fuel
  belongs_to :heating_type
  belongs_to :protection_system
  belongs_to :flooring
  belongs_to :cooling
  belongs_to :ceiling
  belongs_to :leedwell
  belongs_to :leed
  belongs_to :green
  belongs_to :loading_type
  belongs_to :storage
  belongs_to :power
  belongs_to :sewage
  belongs_to :building_type
  belongs_to :exterior_finish
  belongs_to :municipality
  belongs_to :foundation
  belongs_to :roof
end
