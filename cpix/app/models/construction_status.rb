class ConstructionStatus < ApplicationRecord
	has_one :building, dependent: :nullify
end
