class User < ApplicationRecord


  ##============================================================##
  ## Association
  ##============================================================##
  rolify
  has_many :my_searches, dependent: :nullify
  has_many :listings, dependent: :destroy
  belongs_to :language
  belongs_to :office
  has_and_belongs_to_many :transaction_types, :join_table => :transaction_types_users


  ##============================================================##
  ## Devise
  ##============================================================##
  devise :database_authenticatable,:recoverable, :rememberable, :trackable, :validatable



  #============================================================##
  ## Paperclip : Profil Picture
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/brokers/:id/picture_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small    => ["100x100", :png],
    :medium   => ["200x200", :png],
    :large    => ["450x450", :png]
  },
  :convert_options => {
    :small    => "-background transparent -gravity center -extent 100x100   -quality 70",
    :medium   => "-background transparent -gravity center -extent 200x200   -quality 80",
    :large    => "-background transparent -gravity center -extent 450x450",
  },
  :default_url => "https://randomuser.me/api/portraits/#{rand(0..1)== 0 ? "women" : "men"}/#{rand(1..99)}.jpg"

  validates_attachment_content_type :picture, :content_type => /\Aimage/
  validates_attachment_size :picture, :less_than=>10.megabytes



  def avatar_to_display
    if self.picture?
      self.picture.url(:small)
    else
      size          = 50
      default_img   = "mm"
      allow_rating  = "pg"
      gravatar_id   = Digest::MD5.hexdigest(self.email)
      "https://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&r=#{allow_rating}&d=#{default_img}"
    end
  end

  def name_to_display
    self.first_name.present? ? "#{self.first_name} #{self.last_name}" : self.email
  end









end
