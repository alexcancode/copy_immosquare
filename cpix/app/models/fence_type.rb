class FenceType < ApplicationRecord
	has_one :building, dependent: :nullify
end
