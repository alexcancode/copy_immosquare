class Communication < ApplicationRecord
	has_many :buildings, dependent: :nullify
end
