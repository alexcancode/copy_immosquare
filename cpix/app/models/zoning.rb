class Zoning < ApplicationRecord
	has_one :building, dependent: :nullify
	belongs_to :municipality
end
