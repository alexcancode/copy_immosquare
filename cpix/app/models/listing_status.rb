class ListingStatus < ApplicationRecord
	has_one :listing, dependent: :nullify
end
