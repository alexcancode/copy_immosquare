class Foundation < ApplicationRecord
	has_one :building, dependent: :nullify
end
