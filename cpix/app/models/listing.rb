# encoding: utf-8

class Listing < ApplicationRecord

  ##============================================================##
  ## includes router helper
  ##============================================================##
  include ActionView::Helpers
  include MoneyHelper
  require 'elasticsearch/model'


  ##============================================================##
  ## ASSOCIATIONS
  ##============================================================##
  has_many :photos, as: :photoable, dependent: :destroy
  has_many :documents, as: :documentable, dependent: :destroy
  belongs_to :user
  belongs_to :building
  belongs_to :sale_type
  belongs_to :transaction_type
  belongs_to :title_type
  belongs_to :franchise_type
  has_many    :sellers
  belongs_to :listing_status
  has_many :rooms
  belongs_to :listing_major_type
  belongs_to :listing_minor_type
  belongs_to :listing_sub_type
  belongs_to :possession_occupation
  belongs_to :lease_type
  belongs_to :lease_term
  belongs_to :lease_include
  belongs_to :lease_additional_cost
  has_many :equipment

  ##============================================================##
  ## Money
  ##============================================================##
  monetize :lease_price_cents,  with_model_currency: :currency
  monetize :list_price_cents,   with_model_currency: :currency



  ##============================================================##
  ## Elasticseach
  ##============================================================##
  include Elasticsearch::Model
  index_name "#{Rails.application.class.parent_name.underscore}_#{Rails.env}_listings"
  document_type "listing"

  after_save     { ElasticIndexer.new(:index, self.class.name, self.id)}
  after_destroy  { ElasticIndexer.new(:delete,self.class.name, self.id)}


  ##============================================================##
  ## Pour définir l'indexation de chaque champs....
  ## Si dynamic == true le bloc sert à overwrite et le reste est automatique
  ## Si dynamic == false, on définie quels sont à indexer manuellement
  ##============================================================##
  settings index: { number_of_shards:5, number_of_replicas:0 } do
    settings :analysis => {
      analyzer: {
        description_analyzer: {
          type: 'custom',
          tokenizer: 'standard',
          filter: ['asciifolding', 'lowercase']
        }
      }
    }
    mappings dynamic: true do
      indexes :location, :type => 'geo_point'
    end
  end

  ##============================================================##
  ## Pour définir manuellement les champs présent dans les
  ## sources
  ## Geohash ne sert pas pour elasticsearch, mais pour grouper
  ## les résultats par la suite dans la réponse de l'api
  ##============================================================##
  def as_indexed_json(options={})
    building  = self.building || Building.new
    user      = self.user     || User.new
    address   = (building.present? and building.address.present?) ? building.address :  Address.new
    geo_hash  = (address.latitude.present? and address.longitude.present?) ? GeoHash.encode(address.latitude,address.longitude,11) : ""


    if self.transaction_type.present? and self.transaction_type.transaction_major_type_id == 2
      price           = self.lease_price_cents/100
      price_formated  = "#{humanized_money_with_symbol(self.lease_price)}/ft²"
      subtype         = self.lease_type.present? ? self.lease_type.name : nil
    else
      price           = self.list_price_cents/100.00
      price_formated  = humanized_money_with_symbol(self.list_price)
      subtype         = self.sale_type.present? ? self.sale_type.name : nil
    end


    my_infos  = Jbuilder.encode do |json|
      json.id                 self.id
      json.uid                self.uid
      json.draft              self.draft
      json.client_id          self.client_id
      json.user_id            self.user_id
      json.office_id          user.office_id
      json.status             self.listing_status_id
      json.year_build         building.year_build
      json.price              price
      json.price_formated     price_formated
      json.area               building.land_total_size
      json.area_formated      building.land_total_size.present? ? "#{number_with_delimiter(building.land_total_size)} ft²" : ""
      json.nb_of_floor        building.nb_of_floor
      json.year_build         building.year_build
      json.completeness       self.listing_completeness
      json.location_geohash   geo_hash
      json.cover_url          self.photos.present? ? self.photos[0].image.url(:medium,:timestamp=>false) : Photo.new.image.options[:default_url]
      json.updated_at         I18n.localize self.updated_at.to_time,:format =>("%Y-%m-%d")
      json.locality           address.locality
      json.country            address.country
      json.address            "#{address.street_number} #{address.street_name}#{address.zipcode.present? ? ", #{address.zipcode}" : nil}"
      if(address.latitude.present? && address.longitude.present?)
        json.location do
          json.lat   address.latitude
          json.lon   address.longitude
        end
      end
      json.listing_major_type do
        if self.listing_major_type.present?
          json.id          self.listing_major_type_id
          json.en          self.listing_major_type.name
          json.color       self.listing_major_type.color
        end
      end
      json.transaction_major_type do
        if self.transaction_type.present?
          json.id           self.transaction_type.transaction_major_type_id
          json.en           self.transaction_type.name
          json.subtype do
            json.en   subtype
          end
        end
      end
    end
    return JSON.parse(my_infos)
  end




  ##============================================================##
  ## ~ : recherche partielle sur le mot
  ## must     : The clause (query) must appear in matching documents and will contribute to the score.
  ## filter   : The clause (query) must appear in matching documents. However unlike must the score of the query will be ignored.
  ## should   : The clause (query) should appear in the matching document. In a boolean query with no must clauses, one or more should clauses must match a document. The minimum number of should clauses to match can be set using the minimum_should_match parameter.
  ## must_not : The clause (query) must not appear in the matching documents.
  ##============================================================##
  def self.my_search_elastic(params)
    filter  = []
    should  = []
    must    = []
    sort    = []

    ##============================================================##
    ## Default Settings
    ##============================================================##
    filter << { term: { draft: 0 }}
    filter << { term: { status: 1 }}
    filter << { exists: { field: "location" }}
    filter << { exists: { field: "country" }}

    ##============================================================##
    ##
    ##============================================================##
    filter << { terms: { client_id: params[:provider].split(',') }}         if params[:client].present?
    filter << { terms: { user_id:   params[:user].split(',') }}             if params[:user].present?
    filter << { terms: { office_id: params[:office].split(',') }}           if params[:office].present?
    filter << { terms: { id:        params[:id].split(',') }}               if params[:id].present?

    #filter << { terms: { nb_of_floor: params[:floor].split(',') }}        if params[:floor].present?
    #filter << { terms: { year_build:  params[:yearbuild].split(',') }}    if params[:yearbuild].present?
    
    ##============================================================##
    ## Slider
    ##============================================================##
    filter << { range: { price: { from: params[:price_from] }}}   if params[:price_from].present?
    filter << { range: { price: { to:   params[:price_to]   }}}   if params[:price_to].present?
    filter << { range: { area:  { from: params[:area_from]  }}}   if params[:area_from].present?
    filter << { range: { area:  { to:   params[:area_to]    }}}   if params[:area_to].present?
    filter << { range: { year_build:  { from:   params[:yearbuilt_from] }}}  if params[:yearbuilt_from].present?
    filter << { range: { year_build:  { to:   params[:yearbuilt_to]   }}}  if params[:yearbuilt_to].present?
    filter << { range: { nb_of_floor:   { from:   params[:nbfloors_from]  }}}  if params[:nbfloors_from].present?
    filter << { range: { nb_of_floor:   { to:   params[:nbfloors_to]    }}}  if params[:nbfloors_to].present?

    ##============================================================##
    ## Geoloc
    ## bounds = lat_sw,lng_sw,lat_ne,lng_ne
    ##============================================================##
    if params[:bounds].present? and params[:custom_circle].blank? and params[:custom_poly].blank?
      bounds = params[:bounds].split(',')
      filter << { geo_bounding_box: {location: { bottom_left: { lat: bounds[0], lon: bounds[1]},top_right:{ lat: bounds[2], lon: bounds[3]}}}}
    end
    if params[:custom_circle].present?
      circle = params[:custom_circle].split(',')
      filter << {geo_distance: {location: {lat: circle[0], lon: circle[1]},:distance => circle[2]}}
    end
    if params[:custom_poly].present?
      points = Array.new
      params[:custom_poly].split(',').in_groups_of(2) do |p|
        points << {lat: p[0], lon:p[1]}
      end
      filter << { geo_polygon: { location: { points: points}}}
    end


    ##============================================================##
    ## Dropdowns
    ##============================================================##
    filter << {      terms: { "listing_major_type.id":  params[:majortype].split(',') }}     if params[:majortype].present?
    filter << {not:{ terms: { "listing_major_type.id":  params[:majortype!].split(',') }}}   if params[:majortype!].present?


    filter << {       terms: { "transaction_major_type.id":  params[:majortransaction].split(',') }}    if params[:majortransaction].present?
    filter << {not: { terms: { "transaction_major_type.id":  params[:majortransaction!].split(',') }}}  if params[:majortransaction!].present?


    ##============================================================##
    ## Référence
    ##============================================================##
    filter << { query: { multi_match: { query: params[:reference].downcase, operator: :or, fields: %w(id uid) }}} if params[:reference].present?
    filter << { query: { match: { "country": params[:country] }}}                                                 if params[:country].present?

    ##============================================================##
    ## Keywords
    ##============================================================##
    should << {"query_string":{"query": params[:keywords]}}       if params[:keywords].present?


    ##============================================================##
    ## Sort
    ##============================================================##
    sort << {"#{params[:order_by]}": { "order": ['asc','desc'].include?(params['order']) ? params['order'] : 'asc'  }} if params[:order_by].present?

    ##============================================================##
    ## Search
    ##============================================================##
    @response = Listing.__elasticsearch__.search(
    {
      "query": {
        "bool": {
          "filter": filter,
          "should": should,
          "must":   must,
        }
        },
        "sort": sort
      }
      ).paginate(:page => params[:page], :per_page => params[:per_page])
  end

  ##============================================================##
  ## Fin Elasticsearh
  ##============================================================##





end
