class Flooring < ApplicationRecord
	has_many :buildings, dependent: :nullify
end
