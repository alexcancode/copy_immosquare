class ListingMinorType < ApplicationRecord
	has_one :listing, dependent: :nullify
	belongs_to :listing_major_type
	has_many :listing_sub_types
end
