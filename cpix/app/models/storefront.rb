class Storefront < ApplicationRecord
	has_one :building, dependent: :nullify
end
