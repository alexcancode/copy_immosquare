class FireplaceFuel < ApplicationRecord
	has_many :buildings, dependent: :nullify
end
