class LeaseType < ApplicationRecord
	has_one :listing, dependent: :nullify
end
