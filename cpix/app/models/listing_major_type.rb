class ListingMajorType < ApplicationRecord
	has_one :listing, dependent: :nullify
	has_many :listing_minor_types, dependent: :nullify
end
