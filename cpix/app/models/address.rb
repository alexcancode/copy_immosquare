class Address < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  has_one :office, dependent: :nullify
  has_many :buildings
  has_one :seller, dependent: :nullify
  # has_one :building, dependent: :nullify

  ##============================================================##
  ##
  ##
  ##============================================================##
  after_save :compile_address


  def compile_address
    ## TODO
  end

end
