class DocumentType < ApplicationRecord
	has_one :document, dependent: :nullify
end
