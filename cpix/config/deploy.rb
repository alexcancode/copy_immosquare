set :application, 'cpix'
set :rvm_ruby_version, "2.3.2@cpix"
set :repo_url, 'git@bitbucket.org:wgroupe/cpix.git'
set :bundle_roles, :all
set :deploy_to, "/srv/apps/cpix"
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }



