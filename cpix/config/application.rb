require File.expand_path('../boot', __FILE__)

require "rails"
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"

Bundler.require(*Rails.groups)

module Cpix
  class Application < Rails::Application

    ##============================================================##
    ## add app/assets/fonts to the asset path
    ##============================================================##
    config.assets.paths << Rails.root.join("app", "assets", "fonts")

    ##============================================================##
    ##
    ##============================================================##
    config.browserify_rails.commandline_options = "-t cjsxify --extension=\".js.cjsx\""
    # config.browserify_rails.commandline_options = "-t coffeeify --extension=\".js.jsx\""


    ##============================================================##
    ## Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    ## Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    ##============================================================##
    config.time_zone = 'Central Time (US & Canada)'

    ##============================================================##
    ## The default locale is :en and all translations
    ## from config/locales/*.rb,yml are auto loaded.
    ##============================================================##
    ##============================================================##
    config.i18n.default_locale = 'en'
    config.i18n.available_locales = ['en','fr']
    config.i18n.fallbacks = true


    ##============================================================##
    ## Config action_mailer
    ##============================================================##
    config.action_mailer.default_url_options = { :host => ENV["HOST"] }
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :address        => "smtp-relay.sendinblue.com",
      :port           => 587,
      :authentication => :plain,
      :user_name      => ENV["SENDINBLUE_USERNAME"],
      :password       => ENV["SENDINBLUE_KEY"]
    }




    ##============================================================##
    ## Config paperclip S3_storage
    ##============================================================##
    config.paperclip_defaults = {
      :storage => :s3,
      :s3_protocol => :https,
      :s3_credentials => {
        :bucket             => ENV['AWS_BUCKET'],
        :access_key_id      => ENV['AWS_ACCESS_KEY_ID'],
        :secret_access_key  => ENV['AWS_SECRET_ACCESS_KEY']
        },
        :s3_region => ENV["aws_region"]
      }


    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
  end
end
