Geocoder.configure(
  ##============================================================##
  ## geocoding service
  ##============================================================##
  lookup: :google,

  # geocoding service request timeout (in seconds)
  timeout: 3,
  :http_proxy => 'zurbia.com:7777',
# Geocoder::Configuration.http_proxy = ""

  # default units
  units: :km
)
