# encoding: utf-8

MoneyRails.configure do |config|

  # To set the default currency
  config.default_currency = :cad

  # Set default bank object
  config.default_bank = EuCentralBank.new

  #
  config.include_validations = true


  config.amount_column = { prefix: '', postfix: '_cents',column_name: nil, type: :integer, present: true, null: false, default: 0}
  config.currency_column = { prefix: '',postfix: '_currency', column_name: nil, type: :string, present: true, null: false, default: 'CAD'}


end

euro = {
  :priority             => 1,
  :iso_code             => "EUR",
  :iso_numeric          => "978",
  :name                 => "Euro",
  :symbol               => "€",
  :symbol_first         => false,
  :subunit_to_unit      => 100,
  :subunit              => "Cent",
  :html_entity          => '&#x20AC;',
  :thousands_separator  => ' ',
  :separator            => '.',
  :delimiter            => ','
}
cad = {
  :priority             => 5,
  :iso_code             => "CAD",
  :iso_numeric          => "124",
  :name                 => "Canadian Dollar",
  :symbol               => "$",
  :symbol_first         => false,
  :subunit_to_unit      => 100,
  :subunit              => "Cent",
  :html_entity          => '$',
  :decimal_mark         => '.',
  :thousands_separator  => ',',
  :separator            => '.',
  :delimiter            => ',',
  :exponent             => 2.0,
}
Money::Currency.register(euro)
Money::Currency.register(cad)
