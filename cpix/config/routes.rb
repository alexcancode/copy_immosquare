Rails.application.routes.draw do



  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users

    ##============================================================##
    ## FrontOffice
    ##============================================================##
    root 'front_office#home'
    match 'map'                 => 'front_office#map',              :via => :get,   :as => :front_office_map
    match 'search/cleaner'      => 'front_office#search_cleaner',   :via => :get,   :as => :front_office_search_cleaner
    match 'ad/:id'              => 'front_office#show',             :via => :get,   :as => :front_office_show
    match "contact_mail"        => "front_office#contact_mail",     :via => :post,  :as => :contact_mail


    ##============================================================##
    ## Dashboard
    ##============================================================##
    match 'dashboard'                 => 'dashboard#index',           :via => :get,      :as => :dashboard
    match 'dashboard/offices'         => 'dashboard#offices',         :via => :get,      :as => :dashboard_offices
    match 'dashboard/all'             => 'dashboard#all_listings',    :via => :get,      :as => :dashboard_all_listings
    match 'dashboard/profile'         => 'dashboard#profile',         :via => :get,      :as => :dashboard_profile
    match 'dashboard/profile/:id'     => 'dashboard#profile_update',  :via => :patch,    :as => :dashboard_profile_update
    match 'dashboard/search/cleaner'  => 'dashboard#search_cleaner',  :via => :post,     :as => :dashboard_search_cleaner


    ##============================================================##
    ## Listing Edit
    ##============================================================##
    match 'dashboard/listing/new'                     => 'listing#listing_new',         :via => :get,             :as => :dashboard_listing_new
    match 'dashboard/listing/:id'                     => 'listing#listing_edit',        :via => :get,             :as => :dashboard_listing_edit
    match 'dashboard/listing/:id'                     => 'listing#listing_update',      :via => :patch,           :as => :dashboard_listing_update

    match 'dashboard/listing/:id/broker'              => 'listing#listing_broker',      :via => :get,             :as => :dashboard_listing_broker
    match 'dashboard/listing/:id/financing'           => 'listing#listing_financing',   :via => :get,             :as => :dashboard_listing_financing
    match 'dashboard/listing/:id/lease'               => 'listing#listing_lease',       :via => :get,             :as => :dashboard_listing_lease
    match 'dashboard/listing/:id/location'            => 'listing#listing_location',    :via => :get,             :as => :dashboard_listing_location
    match 'dashboard/listing/:id/seller'              => 'listing#listing_seller',      :via => :get,             :as => :dashboard_listing_seller


    ##============================================================##
    ## Building
    ##============================================================##
    match 'dashboard/listing/:id/building'            => 'listing#listing_building',            :via => :get,             :as => :dashboard_listing_building
    match 'dashboard/listing/:id/building/edit/:id'   => 'listing#listing_building_update',     :via => [:patch,:post],   :as => :dashboard_building_update

    ##============================================================##
    ## Taxs
    ##============================================================##
    match 'dashboard/listing/:id/taxes'                => 'listing#listing_taxes',        :via => :get,             :as => :dashboard_listing_taxes
    match 'dashboard/listing/:id/tax/create'         => 'listing#listing_tax_create',  :via => [:get, :post],    :as => :dashboard_tax_create
    match 'dashboard/listing/:id/tax/edit/:tax_id'     => 'listing#listing_tax_edit',  :via => [:get],        :as => :dashboard_tax_edit
    match 'dashboard/listing/:id/tax/delete/:tax_id'   => 'listing#listing_tax_delete',       :via => [:delete],          :as => :dashboard_tax_delete
    match 'dashboard/listing/:id/tax/update/:tax_id'   => 'listing#listing_tax_update',     :via => [:patch,:post],   :as => :dashboard_tax_update

    ##============================================================##
    ## Rooms
    ##============================================================##
    match 'dashboard/listing/:id/rooms'                => 'listing#listing_room',        :via => :get,             :as => :dashboard_listing_room
    match 'dashboard/listing/:id/rooms/create'         => 'listing#listing_room_create',  :via => [:get, :post],    :as => :dashboard_room_create
    match 'dashboard/listing/:id/rooms/edit/:r_id'     => 'listing#listing_room_edit',  :via => [:get],        :as => :dashboard_room_edit
    match 'dashboard/listing/:id/rooms/delete/:r_id'   => 'listing#listing_room_delete',       :via => [:delete],          :as => :dashboard_room_delete
    match 'dashboard/listing/:id/rooms/update/:r_id'   => 'listing#listing_room_update',     :via => [:patch,:post],   :as => :dashboard_room_update

    ##============================================================##
    ## Equipements
    ##============================================================##
    match 'dashboard/listing/:id/equipment'            => 'listing#listing_equipment',           :via => :get,             :as => :dashboard_listing_equipment
    match 'dashboard/listing/:id/equipment/create'     => 'listing#listing_equipment_create',    :via => [:get, :post],    :as => :dashboard_equipment_create
    match 'dashboard/listing/:id/equipment/edit/:e_id'    => 'listing#listing_equipment_edit',    :via => [:get],   :as => :dashboard_equipment_edit
    match 'dashboard/listing/:id/equipment/delete/:e_id'    => 'listing#listing_equipment_delete',    :via => [:delete],   :as => :dashboard_equipment_delete
    match 'dashboard/listing/:id/equipment/update/:e_id'    => 'listing#listing_equipment_update',    :via => [:patch,:post],   :as => :dashboard_equipment_update

    ##============================================================##
    ## Documents
    ##============================================================##
    match 'dashboard/listing/:id/documents'               => 'listing#listing_document',            :via => :get,             :as => :dashboard_listing_document
    match 'dashboard/listing/:id/documents/create'        => 'listing#listing_document_create',     :via => [:get, :post],    :as => :dashboard_document_create
    match 'dashboard/listing/:id/documents/edit/:doc_id'   => 'listing#listing_document_edit',       :via => [:get],          :as => :dashboard_document_edit
    match 'dashboard/listing/:id/documents/delete/:doc_id'   => 'listing#listing_document_delete',       :via => [:delete],          :as => :dashboard_document_delete
    match 'dashboard/listing/:id/documents/update/:doc_id'   => 'listing#listing_document_update',     :via => [:patch,:post],   :as => :dashboard_document_update
    match 'dashboard/listing/:id/documents/:doc_id'       => 'listing#listing_document_download',     :via => [:get, :post],    :as => :dashboard_document_download

    ##============================================================##
    ## Photos
    ##============================================================##
    match 'dashboard/listing/:id/photos'                => 'listing#listing_photo',               :via => :get,             :as => :dashboard_listing_photo
    match 'dashboard/listing/:id/photos/create'         => 'listing#listing_photo_create',        :via => [:get, :post],    :as => :dashboard_photo_create
    match 'dashboard/listing/:id/photos/edit/:id'       => 'listing#listing_photo_update',        :via => [:patch,:post],   :as => :dashboard_photo_update
    match 'dashboard/listing/:id/photos'                => 'listing#listing_photo_sort',          :via => [:post],          :as => :dashboard_listing_photo_sort
    match 'dashboard/listing/:id/photos/delete/:pic_id'   => 'listing#listing_photo_delete',       :via => [:delete],          :as => :dashboard_photo_delete







    ##============================================================##
    ## API
    ##============================================================##
    namespace :api, defaults: {format: 'json'} do
      namespace :v1 do
        ##============================================================##
        ## Map
        ##============================================================##
        match 'map/setup'      => 'maps#setup',          :via => :get
        ##============================================================##
        ## Search
        ##============================================================##
        match 'search/listings'       => 'searchs#listings',        :via => :get
        match 'search/listings/:id'   => 'searchs#listings_show',   :via => :get
        match 'search/buildings'      => 'searchs#buildings',       :via => :get
        match 'search/buildings/:id'  => 'searchs#buildings_show',  :via => :get
    end
end




end
end
