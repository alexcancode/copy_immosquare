module GravatarHelper

  def gravatar_url(email, size=50,  secure=true, img_type='png')
    default_img ="mm"
    allow_rating="pg"
    gravatar_id = Digest::MD5.hexdigest(email)
    "http"+ (!!secure ? 's://secure.' : '://') +"gravatar.com/avatar/#{gravatar_id}.#{img_type}?s=#{size}&r=#{allow_rating}&d=#{default_img}"
  end

end
