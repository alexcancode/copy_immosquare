class DictionaryPopulater

  def initialize(args)
    @dictionary = args[:dictionary].deep_symbolize_keys
    @bill       = args[:bill]
    @store      = args[:store]
  end

  ##============================================================##
  ## Send a Populated dictionary
  ##============================================================##
  def populate_dictionary
    @dictionary[:document][:primary_color] = @store.color2 if @store.color2.present?

    @dictionary[:blocks].each do |b|
      case b[:type].to_s
      when "text"
        b[:content] = self.send(b[:name])
      when "image"
        b[:content] = self.send(b[:name])
      when "graphics"
        b[:content] = self.send(b[:name])
      end
    end if @dictionary[:blocks].present?

    @dictionary
  end

  ##============================================================##
  ## When Missing property is called
  ##============================================================##
  def method_missing(sym, *args, &block)
    JulesLogger.info "Method #{sym} called on the DictionaryPopulater object but nil was returned"
    return nil
  end




  def store_logo
    directory_name = "#{Rails.root}/tmp/bill_images/"
    Dir.mkdir(directory_name) unless Dir.exists?(directory_name)
    IO.copy_stream(open(@store.logo.url(:medium)), "#{Rails.root}/tmp/bill_images/#{@store.logo_file_name}") unless File.file?("#{Rails.root}/tmp/bill_images/#{@store.logo_file_name}")

    "#{Rails.root}/tmp/bill_images/#{@store.logo_file_name}"
  end

  def invoice_title
    "#{I18n.t("billing.title").upcase}"
  end




  def bill_to
    country = ISO3166::Country[@bill.client.profile.billing_address_country]

    "#{I18n.t("billing.bill_to_title").upcase}\n
#{@bill.client.profile.company_name.present? ? @bill.client.profile.company_name : nil}
#{@bill.client.profile.first_name.present? ? @bill.client.profile.first_name : nil} #{@bill.client.profile.last_name.present? ? @bill.client.profile.last_name : nil}
#{@bill.client.profile.billing_address_address1}
#{@bill.client.profile.billing_address_city}, #{@bill.client.profile.billing_address_zipcode}
#{country.translations[I18n.locale.to_s] || country.name}"
  end

  def ship_to
    if @bill.shippable
      country = ISO3166::Country[@bill.client.profile.shipping_address_country]

      "#{I18n.t("billing.ship_to_title").upcase}\n
#{@bill.client.profile.company_name.present? ? @bill.client.profile.company_name : nil}
#{@bill.client.profile.first_name.present? ? @bill.client.profile.first_name : nil} #{@bill.client.profile.last_name.present? ? @bill.client.profile.last_name : nil}
#{@bill.client.profile.shipping_address_address1}
#{@bill.client.profile.shipping_address_city}, #{@bill.client.profile.shipping_address_zipcode}
#{country.translations[I18n.locale.to_s] || country.name}"
    else
      nil
    end
  end



  def invoice_payment_method
    "#{I18n.t("billing.payment_method_title")}"
  end

  def invoice_payment_details
    "#{@bill.payment_data["source"]["brand"]}\n**** **** **** #{@bill.payment_data["source"]["last4"]}" if @bill.payment_data.present?
  end




  def invoice_date_title
    I18n.t("billing.date_title")
  end

  def invoice_date_content
    @bill.date.strftime("%d/%m/%Y")
  end

  def invoice_number_title
    I18n.t("billing.number_title")
  end

  def invoice_number_content
    "# #{@bill.number}"
  end

  def client_number_title
    I18n.t("billing.client_title")
  end

  def client_number_content
    "# #{@bill.client.id}"
  end

  def invoice_total_title
    I18n.t("billing.total_title")
  end

  def invoice_total_content
    ActionController::Base.helpers.humanized_money_with_symbol(@bill.total_price)
  end




  def store_infos
    tax1 = @store.tax.present? ? "#{@store.tax.tax1_name} #{@store.tax.tax1_number}" : ""
    tax2 = @store.tax.present? && @store.tax.tax2_name.present? ? "#{@store.tax.tax2_name} #{@store.tax.tax2_number}" : ""
    "#{@store.name} - #{@store.address.gsub("\r\n", ", ")}\n\n#{@store.company_registration_number} - #{tax1} - #{tax2}"
  end

  def store_contact
    "#{@store.contact_email}\n\n#{@store.contact_phone}"
  end

end
