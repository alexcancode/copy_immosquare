$('body.back_office.products_edit, body.back_office.products_update').ready ->

  ##============================================================##
  ## Chosen
  ##============================================================##
  $(".chosen-select").chosen({no_results_text: nothing_found})

  ##============================================================##
  ## Spinner
  ##============================================================##
  $(".duplicate_link").on "click", (e) ->
    spin(true)


  ##============================================================##
  ## Sortable Pictures
  ##============================================================##
  if $('#uploaded_pictures').length
    Sortable.create document.getElementById("uploaded_pictures"),
      store:
        get: (sortable) ->
          order = ""
          if order then order.split('|') else []
        set: (sortable) ->
          $.ajax
            type: "POST"
            url: $('#uploaded_pictures').data("sort_url")
            data:
              my_order: sortable.toArray()


  ##============================================================##
  ## Image Upload for Product Assets
  ##============================================================##
  # Customize le bouton choisir image
  $('#input_file_button').on 'click', (e) ->
    $('#input-file').trigger "click"

  # FileUpload Pictures
  $("#new_product_asset").fileupload
    dataType: 'script'
    dropZone: $('#dropzone')
    add: (e, data) ->
      types = /(\.|\/)(gif|jpe?g|png|mov|mpeg|mpeg4|avi)$/i
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        data.context = $("#upload_progess_bar")
        data.context.append('<div class="progress"><div id="progress_bar_'+file.lastModified+'" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div></div>')
        data.context.find('#progress_bar_'+file.lastModified).text(file.name)
        data.submit()
      else
        swal("Error","#{file.name} is not a gif, jpg or png image file",'error')
    progress: (e, data) ->
      if data.context
        file = data.files[0]
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('#progress_bar_'+file.lastModified).css('width', progress + '%')
    done: (e,data) ->
      file = data.files[0]
      data.context.find('#progress_bar_'+file.lastModified).css('width', '0%').parent().remove()

  # Effect on dragover
  $(document).bind 'dragover', (e) ->
    dropZone = $('#dropzone')
    timeout = window.dropZoneTimeout
    if !timeout
      dropZone.addClass 'in'
    else
      clearTimeout timeout
    found = false
    node = e.target
    loop
      if node == dropZone[0]
        found = true
        break
      node = node.parentNode
      unless node != null
        break
    if found
      dropZone.addClass 'hover'
    else
      dropZone.removeClass 'hover'
    window.dropZoneTimeout = setTimeout((->
      window.dropZoneTimeout = null
      dropZone.removeClass 'in hover'
      return
    ), 100)
    return



$('body.back_office.products').ready ->

  ##============================================================##
  ## Spinner
  ##============================================================##
  $(".duplicate_link").on "click", (e) ->
    spin(true)