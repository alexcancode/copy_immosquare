$('body.back_office.pricings, body.back_office.pricings_update').ready ->

  ##============================================================##
  ## Sortable Pictures
  ##============================================================##
  if $('#pricings-table').length
    Sortable.create document.getElementById("pricings-table"),
      store:
        get: (sortable) ->
          order = ""
          if order then order.split('|') else []
        set: (sortable) ->
          $.ajax
            type: "POST"
            url: $('#pricings-table').data("sort_url")
            data:
              my_order: sortable.toArray()

  $(".duplicate_link").on "click", (e) ->
    spin(true)