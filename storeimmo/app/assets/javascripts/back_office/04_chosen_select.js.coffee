$('body.back_office.visibilities').ready ->

  # activating chosen
  $('#chosen-select-client').chosen {no_results_text: "Oops, nothing found!"}
  $('#chosen-select-store').chosen {no_results_text: "Oops, nothing found!"}



$('body.back_office.sales').ready ->

  # activating chosen
  $('#chosen-select-client').chosen {no_results_text: "Oops, nothing found!", allow_single_deselect: true}
  $('#chosen-select-store').chosen {no_results_text: "Oops, nothing found!", allow_single_deselect: true}

  # Same thing for inside the modal
  $('#modal_edit_sale').on 'shown.bs.modal', () ->
    # activating chosen
    $('#chosen-select-client-modal').chosen {no_results_text: "Oops, nothing found!", allow_single_deselect: true}
    $('#chosen-select-store-modal').chosen {no_results_text: "Oops, nothing found!", allow_single_deselect: true}