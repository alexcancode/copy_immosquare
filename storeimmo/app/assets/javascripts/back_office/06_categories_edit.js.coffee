$('body.back_office.categories').ready ->

  Sortable.create $("#categoriesList")[0],
    store:
      get: (sortable) ->
        order = ""
        if order then order.split('|') else []
      set: (sortable) ->
        $.ajax
          type: "POST"
          url: $('#categoriesList').data("sort_url")
          data:
            my_order: sortable.toArray()
