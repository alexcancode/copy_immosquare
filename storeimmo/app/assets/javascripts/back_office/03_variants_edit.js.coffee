$('body.back_office.variants').ready ->

  ##============================================================##
  ## Sortable Pictures
  ##============================================================##
  if $('#variantsTable').length
    Sortable.create document.getElementById("variantsTable"),
      store:
        get: (sortable) ->
          order = ""
          if order then order.split('|') else []
        set: (sortable) ->
          $.ajax
            type: "POST"
            url: $('#variantsTable').data("sort_url")
            data:
              my_order: sortable.toArray()