$('body.back_office.visibilities').ready ->

  # activating chosen
  $('#chosen-select-client').chosen {no_results_text: "Oops, nothing found!"}
  $('#chosen-select-store').chosen {no_results_text: "Oops, nothing found!"}

  # default value
  $('#product_user_store_client_id').val($('#chosen-select-client').val())
  $('#product_user_store_store_id').val($('#chosen-select-client').val())

  # change value on change
  $('#chosen-select-client').on 'change', () ->
    $('#product_user_store_client_id').val($('#chosen-select-client').val())

  $('#chosen-select-store').on 'change', () ->
    $('#product_user_store_store_id').val($('#chosen-select-store').val())