$('body.back_office').ready ->

  $('.bootstrapNumber').bootstrapNumber()

  $('[data-toggle="tooltip"]').tooltip()

  ##============================================================##
  ## Disable form submit with enter
  ##============================================================##
  $("form").keydown (event) ->
      e = e or event
      txtArea = /textarea/i.test((e.target or e.srcElement).tagName)
      txtArea or (e.keyCode or e.which or e.charCode or 0) != 13


  ##============================================================##
  ## Setup de l'éditeur Froala...
  ##============================================================##
  if typeof froala_init isnt 'undefined' and froala_init is true
    $('.froala').froalaEditor(
      theme: 'gray'
      height: '300'
      scrollableContainer: '.fr-box.fr-top'
      toolbarButtons: [
        'fullscreen'
        'bold'
        'italic'
        'underline'
        'strikeThrough'
        'subscript'
        'superscript'
        'fontFamily'
        'fontSize'
        '|'
        'color'
        'emoticons'
        'inlineStyle'
        'paragraphStyle'
        '|'
        'paragraphFormat'
        'align'
        'formatOL'
        'formatUL'
        'outdent'
        'indent'
        '-'
        'insertLink'
        'insertImage' if picture_module == true
        'insertVideo' if picture_module == true
        'insertTable'
        '|'
        'quote'
        'insertHR'
        'undo'
        'redo'
        'clearFormatting'
        'selectAll'
      ]
      imageUploadURL: froala_upload_url
      imageMaxSize: 1 * 1024 * 1024
      imageAllowedTypes: [
        'jpeg'
        'jpg'
        'png'
        'gif'
      ]
      inlineMode: false
      toolbarFixed: false).on 'froalaEditor.image.removed', (e, editor, $img) ->
      $.ajax(
        method: 'POST'
        url: frola_delete_url
        data: src: $img.attr('src')).done((data) ->
        console.log 'image was deleted'
        return
      ).fail ->
        console.log 'image delete problem'
        return
      return

##============================================================##
## Création d'un systeme de flash messages pour JS
##============================================================##
@flash = (level, message) =>
  $("#flashMessageContainer").append("<div class='flash-#{level} animated bounceInRight'><a class='close' data-dismiss='alert' href='#' aria-hidden='true'>×</a>#{message}</div>")

##============================================================##
## Création d'un systeme de spinner
##============================================================##
@spin = (on_off) =>
  if on_off
    $("#layer-full").show()
  else
    $("#layer-full").hide()
