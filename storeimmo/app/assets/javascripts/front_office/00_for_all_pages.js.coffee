##============================================================##
## Création d'un systeme de flash messages pour JS
##============================================================##
@flash = (level, message) =>
  $("#flashMessageContainer").append("<div class='alert flash-#{level} animated bounceInRight'><a class='close' data-dismiss='alert' href='#' aria-hidden='true'>×</a>#{message}</div>")


##============================================================##
## Spinner
##============================================================##
@spin = (on_off) =>
  if on_off
    $("#layer-full").show()
  else
    $("#layer-full").hide()

##============================================================##
## Delay
##============================================================##
@delay = (->
    timer = 0
    (callback, ms) ->
      clearTimeout timer
      timer = setTimeout(callback, ms)
      return
  )()









$(document).ready ->

  $('.bootstrapNumber').bootstrapNumber()
  $('[data-toggle="tooltip"]').tooltip()

  ##============================================================##
  ## Remove flash Notice
  ##============================================================##
  delay ->
    $('.flash-notice').fadeOut()
  ,8000

  ##============================================================##
  ## Disable form submit with enter
  ##============================================================##
  $("form").keydown (event) ->
      e = e or event
      txtArea = /textarea/i.test((e.target or e.srcElement).tagName)
      txtArea or (e.keyCode or e.which or e.charCode or 0) != 13


  ##=============================================================##
  ## Smooth scrolling to ankers
  ##=============================================================##
  $ ->
    $('a[href*="#"]:not([href="#"]):not([data-toggle="collapse"])').click ->
      if location.pathname.replace(/^\//, '') == @pathname.replace(/^\//, '') and location.hostname == @hostname
        target = $(@hash)
        target = if target.length then target else $('[name=' + @hash.slice(1) + ']')
        if target.length
          $('html, body').animate { scrollTop: target.offset().top - 60 }, 1000
          return false
      return
    return





