$('body.product_show').ready ->

  ##============================================================##
  ## Same height for all price-card
  ##============================================================##
  setTimeout ->
    sameHeightPriceCard()
    $(window).on 'resize', ->
      sameHeightPriceCard()
    500


  ##============================================================##
  ## Init image gallery
  ##============================================================##
  $('#image-gallery', @).magnificPopup
    delegate: 'a'
    type: 'image'
    gallery: enabled:true


  ##============================================================##
  ## Css Effect
  ##============================================================##
  initial_scroll = $('#product-header').position().top
  $(window).on 'scroll', ->
    scroll = $(@).scrollTop()
    if(scroll > initial_scroll - $('#main-navbar').outerHeight())
      $('#product-header').addClass('fixed')
      $('#fake-product-header').removeClass('hide')
    else
      $('#product-header').removeClass('fixed')
      $('#fake-product-header').addClass('hide')



  ##============================================================##
  ## Ads autocomplete for print
  ##============================================================##
  if clients_ads?
    new_ads = []
    _.each clients_ads,(client) ->
      new_ads.push(client._source)

    engine1 = new Bloodhound
      local: new_ads
      identify: (obj) ->
        return obj.id
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('id','address','locality','price','uid','unit')
      queryTokenizer: Bloodhound.tokenizers.whitespace


    source_with_no_values = (q,sync) ->
      if q == ""
        sync(engine1.get(_.pluck(new_ads,"id")))
      else
        engine1.search(q, sync)


    $('.typeaheadPrint')
      .typeahead
        hint: true
        highlight: true
        minLength: 0
      ,
        source: source_with_no_values
        display: (suggestion) ->
         return "##{suggestion.id} - #{suggestion.address}"
        templates:
          empty: [
            '<div class="center">',
              'Unable to find any ads that match the current query',
            '</div>'
          ].join('\n'),
          suggestion: Handlebars.compile(
            [
              '<div class="a-left">',
              '<table class="table mb0">',
                '<tr>'
                  '<td style="width:80px;" class="valignMiddle">',
                    '<div class="bg-secondary">',
                      '<img src="{{cover_url}}" class="img-responsive">',
                    '</div>',
                  '</td>',
                  '<td>',
                    '<div class="ellipsis fs11 lh12">
                      <strong>#{{id}}</strong> - {{property_listing.fr}}<br>{{address}}<br><span class="color-primary">#{{price_formated}}</span>',
                    '</div>',
                  '</td>',
                '</tr>',
              '</table>',
              '</div>'
            ].join('\n')
          )

    # PDF selection
    $("#property_pdf_id").on "change", (ev) ->
      select = $("#property_pdf_id")[0]
      window.myprint_pdf_id = select.options[select.selectedIndex].value
      window.n = $('#property_pdf_id option:selected').attr('data-count')

      # We hide or show only the right number of properties
      for i in [1..max_properties]
        if i <= n
          $('.typeaheadPrint[data-i='+i+']').removeClass("hide")
        else
          $('.typeaheadPrint[data-i='+i+']').addClass("hide")

      # then we format the property ids to be sent to myprint
      link_uids = ""
      for i in [1..n]
        if myprint_uids[i] isnt undefined
          link_uids += myprint_uids[i] + ","
      link_uids = link_uids.slice(0,-1)

      # and we update the link
      link = "#{myprint_server}/app/preview?uids=#{link_uids}&pdf=#{myprint_pdf_id}&redirect_uri=#{store_url}"
      $("#linkPreview").attr('href',link)


    # Listing selection
    $('.typeaheadPrint').bind 'typeahead:select', (ev, suggestion) ->
      window.myprint_uids[$(this).attr("data-i")] = suggestion.id

      # We format the property ids to be sent to myprint
      link_uids = ""
      for i in [1..n]
        if myprint_uids[i] isnt undefined
          link_uids += myprint_uids[i] + ","
      link_uids = link_uids.slice(0,-1)

      # and we update the link
      link = "#{myprint_server}/app/preview?uids=#{link_uids}&pdf=#{myprint_pdf_id}&redirect_uri=#{store_url}"
      $("#linkPreview").attr('href',link)
  ##============================================================##
  ## End Ads autocomplete for print
  ##============================================================##



  ##============================================================##
  ## Publipostage
  ## Si la quantité est supérieur aux max prévu dans le pricing
  ## on prend le tarif de la quantité maximum
  ##============================================================##
  $("#RoadsResults").on "click", ".publipostage_checkbox_road, .publipostage_checkbox_type", (e) ->
    $('#publipostageBtn').hide()
    types = {}
    $('#RoadsResults .publipostage_checkbox_type').each  ->
      if $(@).is(":checked")
        types[$(@).data('type')] = 0

    $('#RoadsResults .publipostage_checkbox_road').each  ->
      if $(@).is(":checked")
        data = $(@).data()
        _.each types, (k,v) ->
          types[v] += data[v]

    total = 0
    _.each types, (k,v) ->
      total += types[v]

    if total isnt 0
      $("#my_total").html(total)
      my_price        = 0
      form            = $('#publipostageBtn').closest('form')[0]
      form_datas      = $(form).data()
      prices          = form_datas.prices
      currency        = form_datas.currency
      currencyFormat  = wNumb
        prefix: ""
        postfix: " #{currency.symbol}"
        decimals: 2
        thousand: ' '

      if total > prices[prices.length-1].max
        my_price = prices[prices.length-1].price/100
      else
        _.each prices,(price,index) ->
          if price.min <= total && total < price.max
            my_price = prices[index].price/100

      $("#my_custom_quantity")
        .show()
        .find('input')
          .attr("value", total)
          .prop("checked",true)
          .trigger('change')
      $('#my_custom_value').html(total)
      $("#my_custom_price").html(currencyFormat.to(my_price))
    else
      $("#my_total").html("")
      $("#my_custom_quantity")
        .show()
        .find('input')
          .attr("value",0)
          .prop("checked",false)
          .trigger('change')
      $('#my_custom_value').html("")
      $("#my_custom_price").html("")
    setTimeout ->
      sameHeightPriceCard()
      $(window).on 'resize', ->
        sameHeightPriceCard()
      500





$('body.product_show, body.dashboard.tokens, body.checkouts').ready ->

  $("#prices_container").on "change", "form.pricing_cart_form :input", (e) ->
    form                = $(e.target.form)
    form_data           = form.data()
    prices              = form_data.prices
    checkout_type       = form_data.checkoutType
    currency            = form_data.currency
    combinations        = form_data.combinations
    quantityEditable    = form_data.quantityEditable
    displayRangePrice   = form_data.displayRangePrice
    displayUnitPrice    = form_data.displayUnitPrice
    sale                = form_data.sale
    isAPrintProduct     = form_data.isAPrintProduct
    printOrderId        = form_data.printOrderId
    submit_button       = form.find('.add-cart')


    total_price   = 0
    currencyFormat = wNumb
      prefix: ""
      postfix: " #{currency.symbol}"
      decimals: 2
      thousand: ' '

    ##============================================================##
    ## Total Quantity
    ## Dans le cas du publipostage la quantité peut être supérieur
    ## au max prévu par le pricing..
    ##============================================================##
    input_type = form.find("input[name='pricing[quantity]']").attr("type")
    if checkout_type == "plan"
      quantity = 1
    else
      quantity = if input_type == "radio" then form.find("input[name='pricing[quantity]']:checked").val() else form.find("input[name='pricing[quantity]']").val()
      quantity = parseInt(quantity)

    ##============================================================##
    ## Submit button Display
    ##============================================================##
    if (quantity? and isAPrintProduct isnt true) || (quantity? and isAPrintProduct is true and printOrderId? )
      submit_button.show()
    else
      submit_button.hide()

    ##============================================================##
    ## calculate total price
    ##============================================================##
    if checkout_type == "plan"
      if form.find("input.switchbox-input:checked").val() == "on"
        # check hidden checkbox
        form.find("input[name='pricing[plan_type]']").prop("checked", true);
        total_price += prices[0].price_year
      else
        # uncheck hidden checkbox
        form.find("input[name='pricing[plan_type]']").prop("checked", false);
        total_price += prices[0].price_month
    else
      if prices[prices.length-1].max isnt null and quantity > prices[prices.length-1].max
        total_price = prices[prices.length-1].price * quantity
      else
        _.each prices, (price,index) ->
          if (quantity >= price.min && quantity <= price.max) or (quantity >= price.min && price.max is null)
            if quantity is price.max
              total_price += price.price * quantity
            else
              if quantityEditable is false
                total_price += prices[Math.max(0, index-1)].price * quantity
              else
                total_price += prices[index].price * quantity


    ##============================================================##
    ## combinations
    ##============================================================##
    selected_variants = []
    prices_clones = _.map(prices, _.clone)
    $.each form.find("select.variants"), (index, variant) ->
      selected_variants.push variant.options[variant.selectedIndex].value
    _.each combinations,(comb) ->
      name_match = _.every comb.name.split("|*|"), (name) -> return _.contains(selected_variants, name)
      if name_match
        total_price *= 1.0 + (comb.price_percentage / 100.0)
        _.each prices_clones, (p) ->
          p.price *= 1.0 + (comb.price_percentage / 100.0)
        return true


    ##============================================================##
    ## sale
    ##============================================================##
    unless _.isEmpty(sale)
      if sale.type == "amount"
        total_price -= (sale.value * quantity)
      else
        total_price *= 1.0 - (sale.value / 100.0)


    ##============================================================##
    ## Extras
    ##============================================================##
    extras = 0
    _.each form.find("input[data-type='extra']:checked"),(extra) ->
      qte = parseInt(form.find("[name='pricing[extras][#{$(extra).data("id")}][qte]']").val())
      if checkout_type == "plan" && form.find("input.switchbox-input:checked").val() == "on"
        price = $(extra).data("price2")
      else
        price = $(extra).data("price")
      extras += ((if $(extra).data().isLinkedToPricingQuantity then quantity else qte) * price / $(extra).data().priceQuantity)
    total_price += extras


    ##============================================================##
    ## Display
    ## prices_clones[index] peut ne pas être défini
    ## lors du publipostage car on rajoute une ligne supplémentaire
    ##============================================================##
    _.each form.find('.quantity-options .option'), (tr,index) ->
      if prices_clones[index]
        price = (prices_clones[index].price)/100
        if quantityEditable is false and displayRangePrice is true and displayUnitPrice is false and Number.isInteger(prices_clones[index].max)
          price *= prices_clones[index].max
        $(tr).find('.quantity-price').html(currencyFormat.to(price))


    if total_price isnt 0
      form.find(".final-price").html(currencyFormat.to(total_price/100))
      form.find(".singlePrice").html(currencyFormat.to(prices_clones[0].price/100))



  ##============================================================##
  ## Init Prices forms
  ##============================================================##
  _.each $('form.pricing_cart_form'), (form) ->
    $(form).find('input:first').trigger('change')


@sameHeightPriceCard = (offMargin) ->
  $('#prices_container .container .row').each ->
    $('.price-card', @).css "height", ""
    maxheight = Math.max.apply(null, $('.price-card', @).map(->
      $(@).innerHeight()
    ).get())
    $('.price-card', @).each ->
      $(@).height(maxheight - 80)
