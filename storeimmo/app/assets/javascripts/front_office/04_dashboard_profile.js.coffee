$(window).load ->
  $("input#profile_shipping_same_as_billing").on "change", () ->
    if $(@).prop("checked")
      $(".other_shipping_address").hide()
    else
      $(".other_shipping_address").show()
