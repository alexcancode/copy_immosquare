$(window).load ->

  ##============================================================##
  ## Show new delegate form
  ##============================================================##
  $('.add-new-delegate').on 'click', ->
    $(@).hide()
    $('.new-delegate').removeClass 'hide'

  ##============================================================##
  ## On blur => email verification
  ##============================================================##
  $('#team_member_emails').on 'blur',(e) ->
    value = $(@).val()
    $('#teamMessage').html('')
    $(@).val('')
    if validateEmail(value) is false
      $('#teamMessage').html("#{value} : #{$(@).data('error1')}")
    else
      if $.inArray(value,team_members) == -1 and $.inArray(value,emails_to_add_to_team) == -1
        emails_to_add_to_team.push(value)
        $('#team_member_emails_to_add_to_team').val(emails_to_add_to_team)
        $('#teamMemberPending').append("<li><div class='btn btn-custom-primary'>#{value}<span data-value='#{value}' class='pointer delete_pending_member'><i class='fa fa-times ml5'></span></div</li>")
      else
        $('#teamMessage').html("#{value} : #{$(@).data('error2')}")


  ##============================================================##
  ## Remove From List
  ##============================================================##
  $('#my_team_member_form_container').on 'click', ".delete_pending_member", (e) ->
    e.preventDefault()
    emails_to_add_to_team.splice($.inArray($(@).data('value'),emails_to_add_to_team),1)
    $('#team_member_emails_to_add_to_team').val(emails_to_add_to_team)
    $(@).closest('li').remove()

  ##============================================================##
  ## Simulate Blur on Enter
  ##============================================================##
   $("#team_member_emails").keyup (e) ->
      if (e.which == 13)
        $(@).blur()

  validateEmail = (email) ->
    re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
