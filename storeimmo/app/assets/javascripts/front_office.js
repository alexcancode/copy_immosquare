//============================================================
//  JQUERY
//============================================================
//= require jquery
//= require jquery_ujs

//============================================================
//  My LIB
//============================================================
//= require plugins/jquery.readySelector

//============================================================
//  OHTER LIB
//============================================================
//= require bootstrap-sprockets
//= require sweetalert
//= require sweet-alert-confirm
//= require bootstrap-number-input
//= require selectize
//= require underscore
//= require wNumb
//= require jquery.magnific-popup.min
//= require typeahead.bundle.min
//= require handlebars
//= require sorttable



//============================================================
//  Front_office
//============================================================
//= require front_office/00_for_all_pages
//= require front_office/02_product_show
//= require front_office/03_checkouts_infos
//= require front_office/04_dashboard_profile
//= require front_office/05_credit_cards
//= require front_office/06_teams


