//============================================================
//  JQUERY
//============================================================
//= require jquery
//= require jquery_ujs

//============================================================
//  My LIB
//============================================================
//= require plugins/jquery.readySelector

//============================================================
//  OHTER LIB
//============================================================
//= require bootstrap-sprockets
//= require sweetalert
//= require sweet-alert-confirm
//= require bootstrap-number-input
//= require chosen-jquery
//= require jquery.plugin
//= require selectize
//= require Sortable.min
//= require underscore
//= require wNumb
//= require jquery.datepick
//= require typeahead.bundle.min
//= require handlebars
//= require sorttable



//============================================================
//  FROALA
//============================================================
//= require froala_editor.min.js
//= require plugins/align.min.js
//= require plugins/char_counter.min.js
//= require plugins/colors.min.js
//= require plugins/entities.min.js
//= require plugins/file.min.js
//= require plugins/font_family.min.js
//= require plugins/font_size.min.js
//= require plugins/fullscreen.min.js
//= require plugins/image.min.js
//= require plugins/inline_style.min.js
//= require plugins/line_breaker.min.js
//= require plugins/link.min.js
//= require plugins/lists.min.js
//= require plugins/paragraph_format.min.js
//= require plugins/paragraph_style.min.js
//= require plugins/quote.min.js
//= require plugins/table.min.js
//= require plugins/url.min.js
//= require plugins/video.min.js



//============================================================
//  IMAGE UPLOAD
//============================================================
//= require jquery.remotipart
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl


//============================================================
//  Back_office
//============================================================
//= require back_office/00_for_all_pages
//= require back_office/01_products_edit
//= require back_office/02_pricings_edit
//= require back_office/03_variants_edit
//= require back_office/04_chosen_select
//= require back_office/05_visibilities
//= require back_office/06_categories_edit


