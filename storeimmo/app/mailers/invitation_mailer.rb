class InvitationMailer < ApplicationMailer

  default :template_path => "mailers/invitation_mailer"

  ##============================================================##
  ## On overwrite le token de base par le notre
  ##============================================================##
  def invite_to_team(team_member)
    @team_member  = team_member
    mail(
      :to       => team_member.client.email,
      :from     => set_my_from('no-reply'),
      :subject  => set_my_subject("Join Team")
     )
  end

end


