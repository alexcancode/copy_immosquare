class NotificationMailer < ApplicationMailer

  default :template_path => "mailers/notification_mailer"

  ##============================================================##
  ## On overwrite le token de base par le notre
  ##============================================================##
  def send_bill(bill, store, file)
    @bill = bill
    @client = bill.client
    @store = store
    send_to = [@client.email]
    send_to << "quentin@immosquare.com"
    send_to << @store.contact_email if @store.contact_email.present?
    attachments[File.basename(file)] = File.read(file)
    mail(
      :to       => send_to,
      :from     => set_my_from('no-reply'),
      :subject  => set_my_subject("Facture")
      )
  end

end


