$("#modal_refill .modal-body").html("<%= j render 'pages/price_card', :pricing => @pricing, :path => cart_add_or_update_path %>")
$("#modal_refill").modal("show")

# Display well the price_card
$('.bootstrapNumber').bootstrapNumber()
$('form.pricing_cart_form').find('input:first').trigger('change')