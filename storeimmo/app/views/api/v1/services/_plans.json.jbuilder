json.id                 auth.id
json.sku                auth.pricing.sku
json.product_sku        auth.product_sku
json.status             auth.status
json.name               auth.item_data["pricing_name"]
json.purchase_date      auth.purchase_date
json.last_bill_date     auth.last_bill_date
json.next_bill_date     auth.next_bill_date
json.limit_date         auth.limit_date
json.mode               auth.mode
json.extras             auth.item_data["extras"] do |extra|
  json.name   extra["sku"]
end
