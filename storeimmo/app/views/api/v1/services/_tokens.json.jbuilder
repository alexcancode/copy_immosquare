json.id                 auth.id
json.sku                auth.pricing.sku
json.product_sku        auth.product_sku
json.tokens             auth.tokens
json.updated            updated
json.debit_url          "#{request.base_url}/api/v1/services/debit/#{auth.id}"
json.refill_link        "#{dashboard_tokens_url}"
json.limit_date         auth.limit_date
json.extras             auth.item_data["extras"] do |extra|
  json.name               extra["sku"]
end
