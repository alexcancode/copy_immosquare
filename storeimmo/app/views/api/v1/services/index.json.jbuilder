json.partial! 'api/v1/oauth/client', client: @client

json.menu  raw render(:partial => 'layouts/header.html.erb', :locals => {:company=>@company})

json.authorizations do
  json.plans @plans do |auth|
    json.partial! 'api/v1/services/plans', auth: auth
  end
  json.tokens @tokens do |auth|
    json.partial! 'api/v1/services/tokens', auth: auth, updated: @updated
  end
end
