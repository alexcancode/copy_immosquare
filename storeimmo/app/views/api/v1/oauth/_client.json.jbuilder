profile = client.profile || Profile.new

json.id           client.id
json.name         client.name
json.email        client.email
json.first_name   profile.first_name
json.last_name    profile.last_name
json.location     nil
json.description  nil
json.image        profile.avatar.url(:large,:timestamp=>false)
json.phone        profile.phone
json.urls         nil


json.extra do
  json.store do
    json.id     client.store_id
    json.name   client.store.name
    json.url    "http://#{client.store.store_domain.domain}"
  end
  json.client do
    json.email        client.email
    json.first_name   profile.first_name
    json.last_name    profile.last_name
    json.phone        profile.phone
    json.image        profile.avatar.url(:large,:timestamp=>false)
  end
  json.company do
    json.name       profile.company_name
    json.email      profile.company_email
    json.address    profile.billing_address_address1
    json.address2   profile.billing_address_address2
    json.country    profile.billing_address_country
    json.city       profile.billing_address_city
    json.province   profile.billing_address_province
    json.zipcode    profile.billing_address_zipcode
    json.logo       profile.logo.url(:large,:timestamp=>false)
  end
end


