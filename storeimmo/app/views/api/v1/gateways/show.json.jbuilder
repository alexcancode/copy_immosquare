json.gateways @gateways do |gateway|
  json.name       gateway.name
  json.activations gateway.gateway_activations do |activation|
    json.id                   activation.id
    json.client_id            activation.client_id
    json.username             activation.username
    json.password             activation.password
    json.shareimmo_api_key    activation.store.shareimmo_api_key
    json.shareimmo_api_token  activation.store.shareimmo_api_token
  end
end