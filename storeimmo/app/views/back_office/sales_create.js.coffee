<% if @sale.errors.any? %>
  $('#modal_edit_sale').html("<%= j render('back_office/partials/sales_edit_form') %>").modal('show');
<% else %>
  $("#salesTable").html("<%= j render 'back_office/partials/sales' %>")
  $('#modal_edit_sale').modal('hide')
<% end %>
