<% if @product_asset.errors.any? %>
swal("Failed to upload picture: <%= j @product_asset.errors.full_messages.join(', ').html_safe %>");
<% else %>
$('#new_product_asset')[0].reset()
$('#uploaded_pictures').append('<%= j render :partial => "back_office/partials/product_asset", locals: {product_asset: @product_asset} %>')
<% end %>
