<% if @pricing_plan.errors.any? %>
  $("#pricingPlanModal .modal-dialog").html('<%= j render "back_office/partials/pricing_plan_edit" %>')
  $('#pricingPlanModal').modal('show')
<% else %>
  $("#pricingPlanTable").html("<%= j render 'back_office/partials/pricing_plan' %>")
  $('#pricingPlanModal').modal('hide')
<% end %>
