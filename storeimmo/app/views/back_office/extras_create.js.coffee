<% if @extra.errors.any? %>
  $("#extraModal .modal-dialog").html('<%= j render "back_office/partials/extra" %>')
  $('#extraModal').modal('show')
<% else %>
  $("#extrasTable").html("<%= j render 'back_office/partials/extras' %>")
  $('#extraModal').modal('hide')
<% end %>
