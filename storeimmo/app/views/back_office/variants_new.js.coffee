$("#variantModal .modal-dialog").html('<%= j render "back_office/partials/variant" %>')
$('#variantModal').modal('show')

$('.selectized').last().selectize
    plugins: ['remove_button'],
    persist: false,
    create: (input) ->
      return {value: input,text: input}