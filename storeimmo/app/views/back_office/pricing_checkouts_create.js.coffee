<% if @pricing_checkout.errors.any? %>
  $("#pricingCheckoutModal .modal-dialog").html('<%= j render "back_office/partials/pricing_checkout" %>')
  $('#pricingCheckoutModal').modal('show')
<% else %>
  $("#pricingCheckoutTable").html("<%= j render 'back_office/partials/pricing_checkouts' %>")
  $('#pricingCheckoutModal').modal('hide')
<% end %>
