<% if @category.errors.any? %>
$("#variantModal .modal-dialog").html('<%= j render "back_office/partials/variant" %>')
$('#variantModal').modal('show')
<% else %>
$("#variantsTable").html("<%= j render 'back_office/partials/variants' %>")
$("#combinationsTable").html("<%= j render 'back_office/partials/combinations' %>")
$('#variantModal').modal('hide')
<% end %>