class DashboardController < ApplicationController

  include ClientCart

  before_action :authenticate_client!

  def flow
  end

  ##============================================================##
  ## Authorizations
  ##============================================================##
  def index
    @authorizations = Authorization.where(:client_id => current_client.id,:status => "active").reverse_order
  end

  def plans
    @authorizations = Authorization.where(:client_id => current_client.id,:status => "active").where.not(:mode => ["tokens", "purchase"]).reverse_order
    render "index"
  end

  def tokens
    @authorizations = Authorization.where(:client_id => current_client.id, :mode => "tokens",:status => "active").reverse_order
    render "index"
  end

  def purchases
    @authorizations = Authorization.where(:client_id => current_client.id, :mode => "purchase").reverse_order
    render "index"
  end

  def old_authorizations
    @authorizations = Authorization.where(:client_id => current_client.id).where.not(:status => "active").reverse_order
    render "index"
  end


  def checkout_edit
    @pricing = Pricing.find(params[:pricing_id])
  end

  def cancel
    auth = Authorization.find(params[:id])
    auth.update_attribute(:limit_date, auth.next_bill_date)
    flash[:info] = t("dashboard.cancel.info_message", :date => I18n.localize(auth.next_bill_date, :format =>("%d %b %Y")))
    redirect_to :back
  end

  ##============================================================##
  ## Shareimmo
  ##============================================================##
  def shareimmo_signin
    call1 = HTTParty.get("#{@store.shareimmo_api_url}/accounts/#{current_client.id}/token",{:headers => @api_header})
    call1 = JSON.parse(call1.body)
    redirect_to "#{@store.shareimmo_api_url}/accounts/#{current_client.id}/connect?apiKey=#{@store.shareimmo_api_key}&apiToken=#{@store.shareimmo_api_token}&token=#{call1["token"]}&redirect=#{params[:type]}&locale=#{I18n.locale}&store=#{@store.slug}"
  end

  ##============================================================##
  ## Profile
  ##============================================================##
  def profile
    @profile = current_client.profile
  end

  def profile_update
    @profile = Profile.find(params[:id])

    @profile.company_name              = params[:profile][:company_name]
    @profile.company_email             = params[:profile][:company_email]
    @profile.billing_address_address1  = params[:profile][:billing_address_address1]
    @profile.billing_address_address2  = params[:profile][:billing_address_address2]
    @profile.billing_address_city      = params[:profile][:billing_address_city]
    @profile.billing_address_province  = params[:profile][:billing_address_province]
    @profile.billing_address_country   = params[:profile][:billing_address_country]
    @profile.billing_address_zipcode   = params[:profile][:billing_address_zipcode]

    if params[:profile][:shipping_same_as_billing] == "1"
      @profile.shipping_same_as_billing   = 1
      @profile.shipping_address_address1  = params[:profile][:billing_address_address1]
      @profile.shipping_address_address2  = params[:profile][:billing_address_address2]
      @profile.shipping_address_city      = params[:profile][:billing_address_city]
      @profile.shipping_address_province  = params[:profile][:billing_address_province]
      @profile.shipping_address_country   = params[:profile][:billing_address_country]
      @profile.shipping_address_zipcode   = params[:profile][:billing_address_zipcode]
    else
      @profile.shipping_same_as_billing   = 0
      @profile.shipping_address_address1  = params[:profile][:shipping_address_address1]
      @profile.shipping_address_address2  = params[:profile][:shipping_address_address2]
      @profile.shipping_address_city      = params[:profile][:shipping_address_city]
      @profile.shipping_address_province  = params[:profile][:shipping_address_province]
      @profile.shipping_address_country   = params[:profile][:shipping_address_country]
      @profile.shipping_address_zipcode   = params[:profile][:shipping_address_zipcode]
    end

    if @profile.save
      flash[:info] = t('app.saved')
    else
      flash[:danger] = @profile.errors
    end
    redirect_to dashboard_profile_path
  end

  ##============================================================##
  ## Gateway
  ##============================================================##
  def gateway
    @gateways = current_client.gateway_activations
  end

  def gateway_create
    activation = GatewayActivation.new(gateway_params)
    if activation.save
      flash[:success] = t("dashboard.gateway.check_success")
      render "gateway_update"
    else
      flash[:danger]  = t("dashboard.gateway.check_error")
      flash.discard
      render 'flash_message'
    end
  end


  def gateway_update
    activation = GatewayActivation.find(params[:id])
    if activation.update_attributes(gateway_params)
      flash[:info]    = t("dashboard.gateway.check_success")
    else
      flash[:danger]  = t("dashboard.gateway.check_error")
      flash.discard
      render 'flash_message'
    end
  end


  def gateway_delete
    gateway = GatewayActivation.find(params[:id])
    gateway.destroy
    redirect_to dashboard_gateway_path
  end

  def gateway_sync
    activation = GatewayActivation.find(params[:id])
    response = HTTParty.get("#{ENV['shareimmo_api_url']}/api/v1/#{activation.gateway.name.downcase}/sync/#{activation.client_id}")
    if response.code == 200
      flash[:success] = t("dashboard.gateway.sync_success")
    else
      flash[:danger] = t("dashboard.gateway.sync_error")
    end
    redirect_to dashboard_gateway_path
  end


  ##============================================================##
  ## Team
  ##============================================================##
  def team
    @team = Team.where(:client_id => current_client.id).first_or_create
  end

  def team_member_create
    params[:team_member][:emails_to_add_to_team].split(',').each do |email|
      client      = Client.where(:email=>email,:store_id=> @store.id).first
      if client.blank?
        random_password = Client.random_password
        client = Client.new(
          :email                        => email,
          :store_id                     => @store.id,
          :password                     => random_password,
          :password_confirmation        => random_password,
          :passowrd_generated_randomly  => 1
          )
        client.save
      end
      team_member = TeamMember.new(
        :team_id          => current_client.team.id,
        :client_id        => client.id,
        :invitation_token => random_password
        )
      team_member.save
      InvitationMailer.invite_to_team(team_member).deliver_now
    end
    redirect_to dashboard_team_path
  end

  def team_member_resend
    team_member = TeamMember.find(params[:id])
    InvitationMailer.invite_to_team(team_member).deliver_now
    flash[:info] = "Invitation send"
    redirect_to dashboard_team_path
  end

  def team_member_delete
    team_member = TeamMember.find(params[:id])
    team_member.destroy
    redirect_to dashboard_team_path
  end

  def change_my_team
    authorized_clients_ids = current_client.teams.pluck(:client_id).push(current_client.id)
    current_client.update_attribute(:working_as_client_id,params[:id]) if authorized_clients_ids.include?(params[:id].to_i)
    redirect_to dashboard_path
  end


  ##============================================================##
  ## Billing
  ##============================================================##
  def billing
    @profile = current_client.profile
    @grouped_bills = Bill.where(:client_id => current_client.id).order(:date => :desc).group_by { |b| t("calendar.#{b.date.to_date.strftime("%B").downcase}") }
  end

  def redirect_to_bill
    s3      = Aws::S3::Resource.new
    object  = s3.bucket(ENV['aws_bucket']).object("#{Rails.env}/bills/bill_#{params[:id]}.pdf")
    return redirect_to object.presigned_url(:get, :expires_in => 10)
  end

  def regenerate_bill
    PdfBillGenerator.new.perform(params[:id], false)
    s3      = Aws::S3::Resource.new
    object  = s3.bucket(ENV['aws_bucket']).object("#{Rails.env}/bills/bill_#{params[:id]}.pdf")
    return redirect_to object.presigned_url(:get, :expires_in => 10)
  end



  private

  def teams_params
    params.require(:team).permit!
  end

  def gateway_params
    params.require(:gateway_activation).permit!
  end


end
