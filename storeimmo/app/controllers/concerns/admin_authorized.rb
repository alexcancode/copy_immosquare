module AdminAuthorized
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_user!
    before_action :check_ability, :except => [:bills]
  end


  def check_ability
    redirect_to root_path if !current_user.has_any_role? :admin
  end


end
