module ClientAuthorized
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_client!, :if => proc{!@store.pay_without_connection}
  end

end
