module ClientCart
  extend ActiveSupport::Concern

  included do
    before_action :find_cart
  end


  ##============================================================##
  ## Client is signed in => get or create his cart in the database
  ## Client isnot signed and he have a session => try and find the cart
  ## if Cart couldn't be found (it must have been deleted because it was too old) => we create a new one
  ## There is no session, we create a new cart
  ## Add currency to Card with the currenc store currency
  ##============================================================##
  def find_cart
    begin
      if client_signed_in?
        @cart = Order.where(:status => "cart", :client => current_client).first_or_create
      else
        if session[:order_id].present?
          @cart = Order.where(:id => session[:order_id], :status => "cart").first
          if @cart.blank?
            @cart = Order.create(:status => "cart")
            session[:order_id] = @cart.id
          end
        else
          @cart = Order.create(:status => "cart")
          session[:order_id] = @cart.id
        end
      end
      @cart.update_attribute(:currency, @store.country.currency) if @cart.currency.blank?
      @cart.update_attribute(:shipping_address_country, @store.country.code) if @cart.shipping_address_country.blank?
      raise "" if @cart.blank?
    rescue Exception => e
      JulesLogger.info e.message
    end
  end

end
