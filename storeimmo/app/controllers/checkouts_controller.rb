class CheckoutsController < ApplicationController

  include ClientAuthorized
  include ClientCart

  before_action :config_stripe, :only => [:infos, :checkout]


  ##============================================================##
  ## Display cart page
  ## Update prices first, just to be sure
  ##============================================================##
  def view_cart
    unless @cart.shipping_id.present?
      countries = Country.where(:code => @store.country.code)
      shippings = []
      if countries.present?
        country = countries.first
        shipping_modes = ShippingMode.where(:store => @store.id)
        # get all the shippings available for this country AND belonging to the modes available for this store
        shippings = Shipping.where(:shipping_mode_id => shipping_modes.pluck(:id), :country => country).map(&:id)
      end
      unless shippings.empty?
        @cart.shipping_id = shippings.first
        @cart.save
      end
    end

    @linked_products = []
    @cart.cart_items.each do |item|
      @linked_products.concat(item.pricing.product.associations)
    end
    @linked_products = @linked_products.uniq.shuffle[0..5] if @linked_products.present?

    @cart.update_subtotal
    @cart.calculate_shipping
    @cart.calculate_taxes(@store.tax)
    @cart.calculate_total
    flash[:danger] = t("orders.add_or_update.duplicate_service_error2") if @cart.multiple_plans_on_cart?
  end

  ##============================================================##
  ## Display infos page
  ## Pre-populate addresses
  ## Find corresponding shipping and update prices
  ##============================================================##
  def infos
    begin
      if @cart.multiple_plans_on_cart?
        flash[:danger] = t("orders.add_or_update.duplicate_service_error2")
        return redirect_to checkout_cart_path
      elsif @cart.is_empty?
        flash[:danger] = t("cart.your_cart_is_empty")
        return redirect_to checkout_cart_path
      end

      if client_signed_in?
        @client = current_client
      else
        @client = Client.new
        @cart.shipping_same_as_billing = 1
      end
      @client.profile = Profile.new if @client.profile.blank?

      # pre-fill the form
      @cart.billing_address_company_name  = @client.profile.company_name             if @cart.billing_address_company_name.blank?
      @cart.billing_address_last_name     = @client.profile.last_name                if @cart.billing_address_last_name.blank?
      @cart.billing_address_first_name    = @client.profile.first_name               if @cart.billing_address_first_name.blank?
      @cart.billing_address_address1      = @client.profile.billing_address_address1 if @cart.billing_address_address1.blank?
      @cart.billing_address_address2      = @client.profile.billing_address_address2 if @cart.billing_address_address2.blank?
      @cart.billing_address_country       = @client.profile.billing_address_country  if @cart.billing_address_country.blank?
      @cart.billing_address_city          = @client.profile.billing_address_city     if @cart.billing_address_city.blank?
      @cart.billing_address_province      = @client.profile.billing_address_province if @cart.billing_address_province.blank?
      @cart.billing_address_zipcode       = @client.profile.billing_address_zipcode  if @cart.billing_address_zipcode.blank?

      if @cart.shipping_same_as_billing.present? && @cart.shipping_same_as_billing == 1
        @cart.shipping_address_last_name    = @cart.billing_address_last_name
        @cart.shipping_address_first_name   = @cart.billing_address_first_name
        @cart.shipping_address_address1     = @cart.billing_address_address1
        @cart.shipping_address_address2     = @cart.billing_address_address2
        @cart.shipping_address_country      = @cart.billing_address_country
        @cart.shipping_address_city         = @cart.billing_address_city
        @cart.shipping_address_province     = @cart.billing_address_province
        @cart.shipping_address_zipcode      = @cart.billing_address_zipcode
      else
        @cart.shipping_same_as_billing      = 0
        @cart.shipping_address_last_name    = @client.profile.last_name                  if @cart.shipping_address_last_name.blank?
        @cart.shipping_address_first_name   = @client.profile.first_name                 if @cart.shipping_address_first_name.blank?
        @cart.shipping_address_address1     = @client.profile.shipping_address_address1  if @cart.shipping_address_address1.blank?
        @cart.shipping_address_address2     = @client.profile.shipping_address_address2  if @cart.shipping_address_address2.blank?
        @cart.shipping_address_country      = @client.profile.shipping_address_country   if @cart.shipping_address_country.blank?
        @cart.shipping_address_city         = @client.profile.shipping_address_city      if @cart.shipping_address_city.blank?
        @cart.shipping_address_province     = @client.profile.shipping_address_province  if @cart.shipping_address_province.blank?
      end

      @cart.save

      # find shippings
      @shipping_modes = ShippingMode.where(:store => @store.id)
      countries = Country.where(:code => (@cart.billing_address_country.present? ? @cart.billing_address_country : "").downcase)
      if countries.present?
        @shippings = Shipping.where(:shipping_mode_id => @shipping_modes.pluck(:id), :country => countries.first)
      else
        @shippings = []
      end

      # find client cards
      if @client.stripe_customer_id.present?
        customer      = Stripe::Customer.retrieve(@client.stripe_customer_id)
        @cards        = customer.sources.all(:object => "card")
        @default_card = customer.default_source
      end

      if (@cart.shipping_id.blank? || (@cart.shipping_id.present? && !@shippings.pluck(:id).include?(@cart.shipping_id))) && !@shippings.empty?
        @cart.shipping_id = @shippings.first[:id]

        @cart.update_subtotal
        @cart.calculate_shipping
        @cart.calculate_taxes(@store.tax)
        @cart.calculate_total
      end

    rescue Stripe::StripeError => e
      flash[:danger] = e.json_body[:error][:message]
    rescue Exception => e
      flash[:danger] = e.message
    end
  end

  ##============================================================##
  ## Proceed to checkout
  ##============================================================##
  def checkout
    begin

      # If at least one product is shippable and we have no shipping method, we tell the user he can't ship to this country
      raise t("orders.checkout.no_shipping_error") if @cart.will_be_shipped? && !@cart.shipping_id.present?

      ##============================================================##
      ## Save the shipping and billing addresses
      ##============================================================##
      @cart.billing_address_company_name    = params[:order][:billing_address_company_name]
      @cart.billing_address_last_name       = params[:order][:billing_address_last_name]
      @cart.billing_address_first_name      = params[:order][:billing_address_first_name]
      @cart.billing_address_address1        = params[:order][:billing_address_address1]
      @cart.billing_address_address2        = params[:order][:billing_address_address2]
      @cart.billing_address_country         = params[:order][:billing_address_country]
      @cart.billing_address_city            = params[:order][:billing_address_city]
      @cart.billing_address_province        = params[:order][:billing_address_province]
      @cart.billing_address_zipcode         = params[:order][:billing_address_zipcode]

      if params[:order][:shipping_same_as_billing] == "1"
        @cart.shipping_same_as_billing    = 1
        @cart.shipping_address_last_name  = params[:order][:billing_address_last_name]
        @cart.shipping_address_first_name = params[:order][:billing_address_first_name]
        @cart.shipping_address_address1   = params[:order][:billing_address_address1]
        @cart.shipping_address_address2   = params[:order][:billing_address_address2]
        @cart.shipping_address_country    = params[:order][:billing_address_country]
        @cart.shipping_address_city       = params[:order][:billing_address_city]
        @cart.shipping_address_province   = params[:order][:billing_address_province]
        @cart.shipping_address_zipcode    = params[:order][:billing_address_zipcode]
      else
        @cart.shipping_same_as_billing    = 0
        @cart.shipping_address_last_name  = params[:order][:shipping_address_last_name]
        @cart.shipping_address_first_name = params[:order][:shipping_address_first_name]
        @cart.shipping_address_address1   = params[:order][:shipping_address_address1]
        @cart.shipping_address_address2   = params[:order][:shipping_address_address2]
        @cart.shipping_address_country    = params[:order][:shipping_address_country]
        @cart.shipping_address_city       = params[:order][:shipping_address_city]
        @cart.shipping_address_province   = params[:order][:shipping_address_province]
        @cart.shipping_address_zipcode    = params[:order][:shipping_address_zipcode]
      end

      @cart.save

      ##============================================================##
      ## Get client (or create new account)
      ##============================================================##
      if client_signed_in?
        client = current_client
      else
        raise t("orders.checkout.no_email_error") if params[:order][:email].blank?
        clients = Client.where(:email => params[:order][:email])

        if clients.present?
          client = clients.first
        else
          pass = SecureRandom.urlsafe_base64
          client = Client.create(:email => params[:order][:email], :password => pass, :password_confirmation => pass, :store_id => @store.id)
          client.profile = Profile.create if client.profile.blank?
        end

        @cart.update_attributes(:client_id => client.id)
      end

      ##============================================================##
      ## Update client addresses if he doesn't have them already
      ##============================================================##
      profile = client.profile

      profile.company_name              = @cart.billing_address_company_name  unless profile.company_name.present?
      profile.last_name                 = @cart.billing_address_last_name     unless profile.last_name.present?
      profile.first_name                = @cart.billing_address_first_name    unless profile.first_name.present?
      profile.billing_address_address1  = @cart.billing_address_address1      unless profile.billing_address_address1.present?
      profile.billing_address_address2  = @cart.billing_address_address2      unless profile.billing_address_address2.present?
      profile.billing_address_country   = @cart.billing_address_country       unless profile.billing_address_country.present?
      profile.billing_address_city      = @cart.billing_address_city          unless profile.billing_address_city.present?
      profile.billing_address_province  = @cart.billing_address_province      unless profile.billing_address_province.present?
      profile.billing_address_zipcode   = @cart.billing_address_zipcode       unless profile.billing_address_zipcode.present?

      profile.shipping_address_address1 = @cart.shipping_address_address1   unless profile.shipping_address_address1.present?
      profile.shipping_address_address2 = @cart.shipping_address_address2   unless profile.shipping_address_address2.present?
      profile.shipping_address_country  = @cart.shipping_address_country    unless profile.shipping_address_country.present?
      profile.shipping_address_city     = @cart.shipping_address_city       unless profile.shipping_address_city.present?
      profile.shipping_address_province = @cart.shipping_address_province   unless profile.shipping_address_province.present?
      profile.shipping_address_zipcode  = @cart.shipping_address_zipcode    unless profile.shipping_address_zipcode.present?

      profile.save

      ##============================================================##
      ## Find Stripe infos
      ##============================================================##
      if client.stripe_customer_id.present?
        customer = Stripe::Customer.retrieve(client.stripe_customer_id)
      else
        customer = Stripe::Customer.create(:description => params[:order][:credit_card][:name])
        client.update_attribute(:stripe_customer_id, customer.id) if client_signed_in?
      end

      ##============================================================##
      ## Save stripe card
      ##============================================================##
      if params[:order][:stripe_card_id] == "-1" || params[:order][:stripe_card_id].blank?
        # it's a new card
        card_id = customer.sources.create(:source => params[:order][:credit_card][:stripeToken]).id
        if customer.default_source == nil
          customer.default_source = card_id
          customer.save
        end
      else
        # it's an existing card
        card_id = params[:order][:stripe_card_id]
      end

      ##============================================================##
      ## Check errors and save order if correct
      ##============================================================##
      message = "<ul>#{t("orders.checkout.error")}"
      error_flag = false

      if @cart.billing_address_first_name.blank? || @cart.billing_address_first_name.blank? || ((@cart.shipping_address_first_name.blank? || @cart.shipping_address_first_name.blank?) && @cart.will_be_shipped?)
        message += "<li>#{t("orders.checkout.name_error")}</li>"
        error_flag = true
      end
      if @cart.billing_address_address1.blank? || @cart.billing_address_city.blank? || @cart.billing_address_country.blank? || @cart.billing_address_zipcode.blank?
        message += "<li>#{t("orders.checkout.billing_address_error")}</li>"
        error_flag = true
      end
      if (@cart.shipping_address_address1.blank? || @cart.shipping_address_city.blank? || @cart.shipping_address_country.blank? || @cart.shipping_address_zipcode.blank?) && @cart.will_be_shipped?
        message += "<li>#{t("orders.checkout.shipping_address_error")}</li>"
        error_flag = true
      end

      message += "</ul>"

      raise message if error_flag

      # the order is correct, let's calculate the prices again with the new informations
      @cart.update_subtotal
      @cart.calculate_taxes(@store.tax)
      @cart.calculate_total
      @cart.calculate_shipping

      ##============================================================##
      ## Process payments * new *
      ##============================================================##
      items_data = []

      @cart.cart_items.each do |item|
        mode = ""
        if item.pricing.is_a_plan? && item.chosen_plan == "month"
          mode = "plan_month"
        elsif item.pricing.is_a_plan? && item.chosen_plan == "year"
          mode = "plan_year"
        elsif item.pricing.quantity.present? && item.pricing.quantity > 0
          mode = "tokens"
        else
          mode = "purchase"
        end

        sales = []
        item.cart_item_sales.each do |s|
          sale_data = {
            :code             => s.sale.code,
            :name             => s.sale.name,
            :start            => s.sale.start,
            :end              => s.sale.end,
            :amount_cents     => s.sale.amount_cents,
            :amount_formatted => "-#{ActionController::Base.helpers.humanized_money_with_symbol(s.sale.amount)}",
            :sale_type        => s.sale.sale_type
          }
          sales << sale_data
        end

        item_data = {
          :item_id                      => item.id,
          :pricing_id                   => item.pricing_id,
          :company_id                   => item.pricing.product.company_id,
          :pricing_name                 => item.pricing.name,
          :logo_url                     => item.pricing.product.logo.url(:medium),
          :logo_name                    => item.pricing.product.logo_file_name,
          :mode                         => mode,
          :product_name                 => item.pricing.product.name,
          :price_before_sale_cents      => item.price_before_sale_cents,
          :price_before_sale_formatted  => ActionController::Base.helpers.humanized_money_with_symbol(item.price_before_sale),
          :unit_price_cents             => item.unit_price_cents,
          :unit_price_formatted         => ActionController::Base.helpers.humanized_money_with_symbol(item.unit_price),
          :total_price_cents            => item.total_price_cents,
          :total_price_formatted        => ActionController::Base.helpers.humanized_money_with_symbol(item.total_price),
          :quantity                     => item.quantity,
          :total_weight                 => item.pricing.shippable == 1 && item.pricing.weight.present? ? item.quantity * item.pricing.weight : 0,
          :limit_date                   => item.pricing.product.validity_duration_month.present? ? DateTime.now + item.pricing.product.validity_duration_month.month : nil,
          :tokens                       => item.pricing.quantity.present? ? item.pricing.quantity * item.quantity : 0,
          :is_a_service                 => item.pricing.is_a_service?,
          :extras                       => item.extras_data,
          :sales                        => sales
        }

        items_data << item_data
      end

      bill = Bill.create(:items_data => items_data, :date => DateTime.now, :stripe_card_id => card_id, :currency => @cart.currency, :client_id => client.id,
        :shipping_address_country => (@cart.shipping_address_country.present? ? @cart.shipping_address_country : (@cart.client.profile.shipping_address_country.present? ? @cart.client.profile.shipping_address_country : nil)),
        :shipping_address_first_name => @cart.shipping_address_first_name, :shipping_address_last_name => @cart.shipping_address_last_name, :shipping_address_address1 => @cart.shipping_address_address1, :shipping_address_address2 => @cart.shipping_address_address2, :shipping_address_zipcode => @cart.shipping_address_zipcode, :shipping_address_city => @cart.shipping_address_city,
        :subtotal_cents => @cart.subtotal_cents, :total_price_cents => @cart.total_cents,
        :tax1_name => @cart.tax1_name, :tax1_cents => @cart.tax1_cents, :tax2_name => @cart.tax2_name, :tax2_cents => @cart.tax2_cents,
        :shipping_id => @cart.shipping_id, :shipping_unit_price_cents => @cart.shipping_unit_price_cents, :shipping_cents => @cart.shipping_cents,
        :origin_type => "Order", :origin_id => @cart.id, :shippable => @cart.will_be_shipped?)

      # little trick for us not to pay in production
      unless ((Rails.env == "production" || Rails.env == "gercop") && (@store.payment_api_key.split("_")[1] == "live" || @store.payment_api_token.split("_")[1] == "live") && client_signed_in? && (current_client.email == "quentin@immosquare.com" || current_client.email == "contact@immosquare.com"))
        charge = Stripe::Charge.create(
          :amount       => @cart.total_cents,
          :currency     => :cad, #TODO
          :source       => card_id,
          :customer     => customer.id,
          :description  => "Pay bill ##{bill.id}"
          )

        bill.update_attributes(:payment_data => charge)
      end

      bill.activate_services

      PdfBillGenerator.perform_async(bill.id, true)

      ##============================================================##
      ## Mark order as finished
      ##============================================================##
      @cart.update_attribute(:status, "finished")

      flash[:success] = t("thanks.title")
      redirect_to thanks_path
    rescue Stripe::StripeError => e

      # find shippings
      @shipping_modes = ShippingMode.where(:store => @store.id)
      countries = Country.where(:code => (@cart.billing_address_country.present? ? @cart.billing_address_country : "").downcase)
      @shippings = countries.present? ? Shipping.where(:shipping_mode_id => @shipping_modes.pluck(:id), :country => countries.first) : []

      JulesLogger.info e.json_body[:error][:message]
      flash[:danger] = e.json_body[:error][:message]
      redirect_to :back
    rescue Exception => e

      # find shippings
      @shipping_modes = ShippingMode.where(:store => @store.id)
      countries = Country.where(:code => (@cart.billing_address_country.present? ? @cart.billing_address_country : "").downcase)
      @shippings = countries.present? ? Shipping.where(:shipping_mode_id => @shipping_modes.pluck(:id), :country => countries.first) : []

      JulesLogger.info e.message
      JulesLogger.info e.backtrace
      flash[:danger] = e.message.html_safe
      redirect_to :back
    end
  end

end
