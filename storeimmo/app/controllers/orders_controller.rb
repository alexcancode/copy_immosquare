class OrdersController < ApplicationController

  include ClientCart



  ##============================================================##
  ##
  ##============================================================##
  def add_or_update
    begin
      @pricing = Pricing.find(params[:pricing][:pricing_id])

      total_item_price  = 0
      price_before_sale = 0
      quantity = params[:pricing][:quantity].to_i

      raise t("orders.add_or_update.no_quantity_error") if @pricing.is_a_checkout? && (quantity.blank? || quantity == 0)

      ##============================================================##
      ## Base price
      ##============================================================##
      chosen_plan = nil
      if @pricing.is_a_plan?
        chosen_plan = params[:pricing][:plan_type] == "on" ? "year" : "month"
        if params[:pricing][:plan_type] == "on"
          total_item_price += @pricing.pricing_plan.price_year_cents
        else
          total_item_price += @pricing.pricing_plan.price_month_cents
        end
      else
        if @pricing.pricing_checkouts.last.quantity_max.present? && quantity > @pricing.pricing_checkouts.last.quantity_max
          total_item_price = @pricing.pricing_checkouts.last.price_cents * quantity
        else
          @pricing.pricing_checkouts.each_with_index do |pc,index|
            if (quantity >= pc.quantity_min && pc.quantity_max.blank?) || (quantity >= pc.quantity_min && quantity <= pc.quantity_max)
              if quantity == pc.quantity_max
                total_item_price += pc.price_cents * quantity
              else
                total_item_price += @pricing.pricing_checkouts[index].price_cents * quantity
              end
              break
            end
          end
        end
      end

      ##============================================================##
      ## Variants and combinations
      ##============================================================##
      selected_variants = []
      if params[:pricing][:variants].present?
        params[:pricing][:variants].each do |k, v|
          selected_variants << v[:name]
        end
        my_combination = nil
        @pricing.variant_categories.first.variant_combinations.each do |comb|
          name_match = comb.name.split("|*|").all? {|name| selected_variants.include?(name)}
          if name_match
            my_combination = comb
            total_item_price *= 1.0 + (comb.price_percentage / 100.0)
            break
          end
        end
      end

      price_before_sale = total_item_price

      ##============================================================##
      ## Sale
      ##============================================================##
      sale_data = {}
      if params[:pricing][:sale_id].present?
        sale = Sale.find(params[:pricing][:sale_id])
        sale_data = {:start => sale.start, :end => sale.end, :sale_type => sale.sale_type, :percentage => sale.percentage, :amount_cents => sale.amount_cents, :amount_currency => sale.amount_currency, :code => sale.code, :client_id => sale.client_id}
        if sale.sale_type == "amount"
          total_item_price -= (sale.amount_cents * quantity)
        elsif sale.sale_type == "percentage"
          total_item_price *= 1.0 - (sale.percentage / 100.0)
        end
      end

      ##============================================================##
      ## Extras
      ##============================================================##
      extras_id = []
      extras_data = []
      if params[:pricing][:extras].present?
        params[:pricing][:extras].each do |k, v|
          if v[:checked] == "1"
            e = Extra.find(k)
            qte = e.is_linked_to_pricing_quantity && @pricing.is_a_checkout? ? quantity : v[:qte].to_i
            extras_id << k
            extras_data << {:id => e.id, :name => e.name, :price => e.price_cents, :price2 => e.price2_cents, :currency => e.currency, :is_linked_to_pricing_quantity => e.is_linked_to_pricing_quantity, :quantity => qte, :price_quantity => e.price_quantity,:obligatory => e.obligatory}
            if @pricing.is_a_plan? && params[:pricing][:plan_type] == "on"
              price_before_sale += e.price2_cents * qte
              total_item_price += e.price2_cents * qte
            else
              price_before_sale += (e.price_cents * qte / (e.price_quantity.present? ? e.price_quantity : 1)).ceil
              total_item_price += (e.price_cents * qte / (e.price_quantity.present? ? e.price_quantity : 1)).ceil
            end
          end
        end
      end



      if params[:cart_item_id].present?
        # update
        @item     = CartItem.find(params[:cart_item_id])
        old_price = @item.total_price_cents
        @item.update_attributes(:quantity => quantity,
          :variant_combination_id   => my_combination.present? ? my_combination.id : "",
          :variant_combination_data => my_combination.present? ? my_combination : "",
          :pricing_id               => @pricing.id,
          :pricing_data             => @pricing,
          :chosen_plan              => chosen_plan,
          :extras_id                => extras_id,
          :extras_data              => extras_data,
          :price_before_sale_cents  => price_before_sale,
          :total_price_cents        => total_item_price,
          :currency                 => @pricing.currency
          )

        @cart.update_attribute(:subtotal_cents, @cart.subtotal_cents - old_price + total_item_price)

        @cart.update_subtotal
        @cart.calculate_taxes(@store.tax)
        @cart.calculate_total

        flash[:success] = t('app.saved')
        render "update"
      else
        # create
        @item = CartItem.create(
          :order_id                 => @cart.id,
          :quantity                 => quantity,
          :variant_combination_id   => my_combination.present? ? my_combination.id : "",
          :variant_combination_data => my_combination.present? ? my_combination : "",
          :pricing_id               => @pricing.id,
          :pricing_data             => @pricing,
          :chosen_plan              => chosen_plan,
          :extras_id                => extras_id,
          :extras_data              => extras_data,
          :price_before_sale_cents  => price_before_sale,
          :total_price_cents        => total_item_price,
          :currency                 => @pricing.currency
          )

        ##============================================================##
        ## It's a new item we're adding, not an update.
        ## So we want to check if there isn't another service of the same product in the cart or in authorizations
        ## if item.pricing.product_id == @pricing.product_id && @pricing.is_a_service? && item.pricing.is_a_service?
        ## The user can't add this item. He should choose between the 2 pricings of the same product.
        ##============================================================##
        if @item.is_already_used?(client_signed_in? ? current_client : nil, @cart)
          @item.destroy
          raise t("orders.add_or_update.duplicate_service_error")
        end

        CartItemSale.create(
          :cart_item_id => @item.id,
          :sale_id      => params[:pricing][:sale_id],
          :sale_data    => sale_data
          ) if params[:pricing][:sale_id].present?

        @cart.cart_items << @item
        @cart.update_attribute(:subtotal_cents, @cart.subtotal_cents + @item.total_price_cents)

        flash[:success] = "added"
        flash.discard
        render "add"
      end

    rescue Exception => e
      @error_message = e.message
      render "error"
    end
  end


  def add_sale
    begin
      code = params[:sale][:code]
      item_id = params[:cart_item_id]

      item = @cart.cart_items.find(item_id)
      sale = Sale.where(:code => code).first

      # if code doesn't exist or doesn't belong to this client
      raise I18n.t("cart.sales.add.error_not_found") if sale.blank? || (sale.client_id.present? && sale.client_id != current_client.id)

      # if code doesn't belong to the product
      raise I18n.t("cart.sales.add.error_product") unless sale.pricing.blank? || item.pricing_id == sale.pricing_id

      # if item already has a sale
      raise I18n.t("cart.sales.add.already_has_sale") if item.cart_item_sales.present?

      price_before_sale = item.total_price_cents
      total_item_price = item.total_price_cents

      sale_data = {:start => sale.start, :end => sale.end, :sale_type => sale.sale_type, :percentage => sale.percentage, :amount_cents => sale.amount_cents, :amount_currency => sale.amount_currency, :code => sale.code, :client_id => sale.client_id}
      if sale.sale_type == "amount"
        total_item_price -= (sale.amount_cents * item.quantity)
      elsif sale.sale_type == "percentage"
        total_item_price *= 1.0 - (sale.percentage / 100.0)
      end

      CartItemSale.create(:cart_item_id => item.id, :sale_id => sale.id, :sale_data => sale_data)

      item.update_attributes(:price_before_sale_cents => price_before_sale, :total_price_cents => total_item_price)

      @cart.update_subtotal
      @cart.calculate_taxes(@store.tax)
      @cart.calculate_total

      flash[:success] = I18n.t("cart.sales.add.success_message")
      redirect_to checkout_cart_path
    rescue Exception => e
      flash[:danger] = e.message
      redirect_to checkout_cart_path
    end
  end



  def remove
    @item = CartItem.find(params[:cart_item_id])
    @cart.cart_items.delete(@item)
    @cart.update_attribute(:subtotal_cents, @cart.subtotal_cents - @item.total_price_cents)

    @cart.order_sales.destroy_all if @cart.is_empty?

    # delete sales if corresponding item is removed
    @cart.order_sales.each do |sale|
      sale.destroy if sale.sale.pricing.present? && @item.pricing.id == sale.sale.pricing.id
    end

    @item.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.js {  }
    end
  end

  def empty
    @cart.clear

    @cart.update_subtotal
    @cart.calculate_taxes(@store.tax)
    @cart.calculate_total

    return redirect_to :back
  end

  def display_update
    @item = CartItem.find(params[:cart_item_id])
    @pricing = @item.pricing
  end

end
