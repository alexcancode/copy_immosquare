class Clients::SessionsController < Devise::SessionsController

  include ClientCart

  skip_before_filter :verify_authenticity_token
  after_action :get_create_cart, :only => :create


end
