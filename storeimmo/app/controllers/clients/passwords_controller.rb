class Clients::PasswordsController < Devise::PasswordsController

  include ClientCart


  skip_before_filter :verify_authenticity_token


end
