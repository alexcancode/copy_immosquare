class Clients::RegistrationsController < Devise::RegistrationsController

  include ClientCart

  skip_before_filter :verify_authenticity_token

  def after_update_path_for(resource)
    dashboard_profile_path
  end

  after_action :get_create_cart, :only => :create
end
