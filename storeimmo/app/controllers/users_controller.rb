class UsersController < ApplicationController

  before_action :authenticate_user!

  def edit_password
    if current_user.sign_in_count == 1
      @user = current_user
      @minimum_password_length = 6
    else
      redirect_to root_path
    end
  end

  def update_password
    if current_user.sign_in_count == 1
      @user = User.find(current_user.id)
      if @user.update(user_params)
        # Sign in the user by passing validation in case their password changed
        sign_in @user, :bypass => true
        flash[:success] = t("user.password_updated")
        redirect_to root_path
      else
        render "edit"
      end
    else
      redirect_to root_path
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end


end