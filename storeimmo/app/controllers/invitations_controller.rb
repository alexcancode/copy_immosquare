class InvitationsController < ApplicationController


  before_filter :check_not_connected


  ##============================================================##
  ## GET clients/invitation/accept?invitation_token=xxx&team=yy
  ##============================================================##
  def invitation
    @team_member = TeamMember.where(:invitation_token => params[:invitation_token]).first
    @team_member.update_attributes(:status => 1)
  end


  def invitation_accept
    begin
      @team_member  = TeamMember.where(:invitation_token => params[:team_member][:invitation_token]).first
      client        = @team_member.client
      raise "Passwords doesnt match" if params[:team_member][:password] != params[:team_member][:password_confirmation]
      client.password               =  params[:team_member][:password]
      client.password_confirmation  =  params[:team_member][:password]
      client.save!
      client.update_attribute(:passowrd_generated_randomly,0)
      sign_in(:client,client)
      redirect_to dashboard_path
    rescue Exception => e
      flash[:danger] = e.message
      redirect_to invitation_path(:invitation_token=>params[:team_member][:invitation_token])
    end
  end



  private


  def check_not_connected
    if current_client.present?
      flash[:danger] = "Already SignIn"
      redirect_to root_path
    end
  end




end

