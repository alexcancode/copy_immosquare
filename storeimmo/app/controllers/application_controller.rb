class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  before_action :set_store
  before_action :set_profile


  skip_before_filter :verify_authenticity_token, :only => [:wysiwyg_picture_manager]



  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {locale: I18n.locale}
  end

  ##============================================================##
  ## Setup store base on request url
  ##============================================================##
  def set_store
    if !request.path.include?("/choose-store")
      if params[:store_id].present? and controller_name == "back_office"
        @store = Store.find(params[:store_id])
      else
        domain_with_port  = "#{request.subdomain.present? ? "#{request.subdomain}." : nil}#{request.domain}#{(request.port.present? and Rails.env.development?) ? ":#{request.port}" : nil}"
        @store            = Store.joins(:store_domain).where(:store_domains  =>{:domain => domain_with_port}).first
      end
      if @store.present?
        I18n.locale  = params[:locale].present? ? params[:locale] : @store.default_language
        @api_header  = {"apiKey" => @store.shareimmo_api_key,"apiToken" => @store.shareimmo_api_token}
      else
        redirect_to choose_your_store_path
      end
    end
  end


  ##============================================================##
  ## Set profile via shareIMMO
  ##============================================================##
  def set_profile
    begin
      @remoteProfile = {:picture_url => "", :first_name => "", :last_name => ""}
      if current_client.present?
        if @store.shareimmo_api_url.present? && current_client.shareimmo_user_id.present?
          profile   = HTTParty.get("#{@store.shareimmo_api_url}/search/accounts/#{current_client.shareimmo_user_id}",{:headers => @api_header})
          raise "" if profile.code != 200
          @remoteProfile  = JSON.parse(profile.body).deep_symbolize_keys
        end
      end
    rescue
      @remoteProfile = {:picture_url => "", :first_name => "", :last_name => ""}
    end
  end




  def after_sign_in_path_for(ressource)
    if ressource.model_name.name == "Client"
      client = ressource

      ##============================================================##
      ## Step # 1 : Check Profile
      ##============================================================##
      if client.profile.blank?
        p = params[:profile].present? ? Profile.create(:client_id =>client.id, :first_name => params[:profile][:first_name], :last_name => params[:profile][:last_name], :phone => params[:profile][:phone]) : Profile.create(:client_id =>client.id)
        p.update_attribute(:email,client.email)
      end

      ##============================================================##
      ## Step # 2 : Check working_as_client_id
      ## Si la liaison est vide ou non conforme, on lui assigne l'id
      ## l'équipier créateur de la 1ère équipe que le client à rejoins
      ## ou son propre id si il n'a pas d'équipe
      ##============================================================##
      authorized_clients_ids = current_client.teams.pluck(:client_id).push(current_client.id)
      current_client.update_attribute(:working_as_client_id, authorized_clients_ids.first ) if !authorized_clients_ids.include?(current_client.working_as_client_id)


      ##============================================================##
      ## Liaison avec shareIMMO
      ## si call1.code != 200, le user n'existe pas dans shareimmo
      ## Il peut y avoir des stores qui n'utilisent pas ShareImmo, donc on vérifie que la shareimmo_api_url est bien présente
      ##============================================================##
      if @store.present? && @store.shareimmo_api_url.present? && client.shareimmo_user_id.blank?
        call1 = HTTParty.get("#{@store.shareimmo_api_url}/accounts/#{client.id}",{:headers => @api_header})
        if call1.code == 200
          call1 = JSON.parse(call1.body)
          client.update_attribute(:shareimmo_user_id,call1["id"])
        else
          body = {
            :uid      => client.id,
            :email    => client.email,
            :password => SecureRandom.urlsafe_base64
          }
          call2 = HTTParty.post("#{@store.shareimmo_api_url}/accounts",{:headers => @api_header, :body => body})
          call2 = JSON.parse(call2.body)
          client.update_attribute(:shareimmo_user_id,call2["id"])
        end
      end


      ##============================================================##
      ## Step # 3 : path
      ## We have to create a new variable 'path' because stored_location_for
      ## is emptied when called
      ##============================================================##
      path = stored_location_for(client)
      path = params[:client][:redirect_uri] if params[:client].present? && params[:client][:redirect_uri].present?
      path.present? ? path : root_path
    elsif ressource.model_name.name == "User"
      if ressource.sign_in_count == 1
        edit_password_path
      else
        back_office_path
      end
    else
      root_path
    end
  end



  ##============================================================##
  ## WYSIWYG picture
  ##============================================================##
  def wysiwyg_picture_manager
    @my_type = Product.find(params[:id])     if params[:asset_type] == "product"
    @asset = @my_type.wysiwyg_assets.new(:picture=>params[:file])
    @asset.save
  end

  def wysiwyg_picture_delete
    @asset = WysiwygAsset.find(File.basename(params[:src]).split('_').first)
    @asset.destroy
    render :nothing => true
  end

  def sum_my_array(array)
    sum = 0
    array.each {|num| sum += num.to_i }
    return sum
  end

  protected
  def get_create_cart

    if session[:order_id].present?
      # There is a session ...
      tmp_cart = Order.find(session[:order_id])
      if tmp_cart.present?
        # The cart exists in the database ...
        @old_cart = Order.where(:status => "cart", :client => current_client).first
        if @old_cart.present?
          # And the client already has another cart => merge the carts
          tmp_cart.cart_items.each do |item|
            item.order_id = @old_cart.id
            item.save
          end
          @old_cart.update_subtotal
          tmp_cart.delete
        else
          # That is the only cart for that client => give it to that client
          tmp_cart.update_attribute(:client_id, current_client.id)
          @cart = tmp_cart
        end
      else
        # The cart doesn't exist in the database anymore => get or create the cart in the database
        @cart = Order.where(:status => "cart", :client => current_client).first_or_create
      end
    else
      # There is no session => get or create the cart in the database
      @cart = Order.where(:status => "cart", :client => current_client).first_or_create
    end
    @cart.update_attribute(:shipping_address_country, @store.country.code) if @cart.shipping_address_country.blank?

  end

  def config_stripe
    if @store.present? && @store.payment_api_type == "stripe"
      Rails.configuration.stripe = {
        :publishable_key => @store.payment_api_key,
        :secret_key      => @store.payment_api_token
      }

      Stripe.api_key    = @store.payment_api_token
    end
  end

end
