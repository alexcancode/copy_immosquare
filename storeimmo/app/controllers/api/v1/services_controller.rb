class Api::V1::ServicesController < Api::V1::ApplicationController

  before_action :doorkeeper_authorize!, :except => [:menu]
  respond_to :json

  def menu
    begin
      @client = Client.find(params[:client_id])
      @store          = @client.store
      sign_in(:client,@client)
      call1   = HTTParty.get("#{@store.shareimmo_api_url}/search/accounts/#{current_client.shareimmo_user_id}",{:headers => {"apiKey" => @store.shareimmo_api_key,"apiToken" => @store.shareimmo_api_token}})
      raise "" if call1.code != 200
      @remoteProfile  = JSON.parse(call1.body).deep_symbolize_keys
    rescue Exception => e
      return render :json =>{:errors => e.message }, :status=> 401
    end
  end

  ##============================================================##
  ## Cette fonction sert à récupererer les propriétés shareimmo
  ## liées à un compte...
  ## http://lvh.me:3000/api/v1/services?access_token=:access_token&api_key=:key&api_token=:token&sku=:sku
  ##============================================================##
  def index
    begin
      raise "No Credentials Found"  if params[:api_key].blank? || params[:api_token].blank?
      raise "No sku Found"          if params[:sku].blank?
      @company = Company.where(:api_key => params[:api_key], :api_token => params[:api_token]).first
      raise "company not set" if @company.blank?
      @client  = Client.find(doorkeeper_token.resource_owner_id)
      raise "Not client found" if @client.blank?
      @store          = @client.store
      @authorizations = Authorization.where(:client_id => @client.id, :company_id => @company.id, :status => "active",:product_sku => params[:sku]).order("purchase_date DESC")
      @tokens         = @authorizations.where(:mode => "tokens")
      @plans          = @authorizations.where(:mode => ["plan_month", "plan_year"])
      @updated        = false
      sign_in(:client,@client)
      call1   = HTTParty.get("#{@store.shareimmo_api_url}/search/accounts/#{current_client.shareimmo_user_id}",{:headers => {"apiKey" => @store.shareimmo_api_key,"apiToken" => @store.shareimmo_api_token}})
      raise "" if call1.code != 200
      @remoteProfile  = JSON.parse(call1.body).deep_symbolize_keys
    rescue Exception => e
      return render :json =>{:errors => e.message }, :status=> 401
    end
  end


  ##============================================================##
  ## Withdraw tokens from the auth
  ## http://lvh.me:3000/api/v1/services/:slug/debit/:id/:quantity?access_token=:access_token&api_key=:key&api_token=:token
  ##============================================================##
  def debit
    begin
      raise "No Credentials Found" if params[:api_key].blank? || params[:api_token].blank?
      @company = Company.where(:api_key => params[:api_key], :api_token => params[:api_token]).first
      raise "company not set" if @company.blank?

      @client  = Client.find(doorkeeper_token.resource_owner_id)
      raise "Not client found" if @client.blank?

      # find auth
      @authorization = Authorization.find(params[:id])
      @authorization.debit_token(params[:quantity])

      # for the view :
      @store = @client.store
      @authorizations = Authorization.where(:client_id => @client.id, :company_id => @company.id, :status => "active")
      @tokens = @authorizations.where(:mode => "tokens")
      @plans = @authorizations.where(:mode => ["plan_month", "plan_year"])
      @updated = true
      render "index"
    rescue Exception => e
      return render :json =>{:errors => e.message }, :status=> 402
    end
  end



end
