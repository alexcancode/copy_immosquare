class Api::V1::CheckoutsController < Api::V1::ApplicationController

  include ClientCart

  ##============================================================##
  ## GET /api/v1/cart/update_shipping
  ##============================================================##
  def update_shipping
    begin
      @cart.shipping_address_country = params[:shipping_country]
      @cart.billing_address_country = params[:billing_country]

      weight = @cart.weight

      countries = Country.where(:code => params[:shipping_country])
      if countries.present? 
        country = countries.first
        shipping_modes = ShippingMode.where(:store => params[:store_id])
        # get all the shippings available for this country AND belonging to the modes available for this store
        shippings1 = Shipping.where(:shipping_mode_id => shipping_modes.pluck(:id), :country => country).order(:max_weight).group_by(&:shipping_mode_id)

        # get the shippings corresponding to the weight (we take only one shipping per shipping mode)
        my_shippings = []
        shippings1.each do |shipping_mode, shippings|
          shippings.each do |s|
            if s.max_weight >= weight
              my_shippings << s
              break
            end
          end
        end

        shippings = my_shippings.map {|s| {id: s.id, name: "#{s.shipping_mode.name} - #{ActionController::Base.helpers.humanized_money_with_symbol(s.price)}"}}
      end

      shippings = [{id: "", name: I18n.t("cart.shipping_no_country")}] if shippings.blank? || shippings.empty? || countries.empty?

      @cart.shipping_id = params[:shipping_id].present? ? params[:shipping_id] : shippings.first[:id]

      @cart.calculate_shipping
      @cart.calculate_taxes(Store.find(params[:store_id]).tax)
      @cart.calculate_total

      return render :json =>{:shippings => shippings, :tax1 => ActionController::Base.helpers.humanized_money_with_symbol(@cart.tax1), :tax2 => ActionController::Base.helpers.humanized_money_with_symbol(@cart.tax2), :price_shipping => ActionController::Base.helpers.humanized_money_with_symbol(@cart.shipping), :price_total => ActionController::Base.helpers.humanized_money_with_symbol(@cart.total)}, :status=>200
    rescue Exception => e
      return render :json =>{:errors=>e.message}, :status=> 402
    end
  end
end