class Api::V1::OauthController < Api::V1::ApplicationController

  before_action :doorkeeper_authorize!

  def me
    @client = Client.find(doorkeeper_token.resource_owner_id)
  end

end
