class Api::V1::GatewaysController < Api::V1::ApplicationController

 before_filter :check_credentials_in_header_or_url

  ##============================================================##
  ## GET /api/v1/gateways/:name
  ##============================================================##
  def show
    begin
      raise "name is missing" if params[:name].blank?
      if params[:client_id].present?
        @gateways = Gateway.includes(:gateway_activations).where(:name => params[:name], :gateway_activations => {:client_id => params[:client_id]})
      else
        @gateways = Gateway.where(:name => params[:name])
      end
    rescue Exception => e
      return render :json =>{:errors=>e.message}, :status=> 402
    end
  end

  ##============================================================##
  ## GET /api/v1/gateways/:activation_id
  ##============================================================##
  def track
    begin
      raise "activation_id is missing" if params[:activation_id].blank?
      ImportTracking.create(:gateway_activation_id => params[:activation_id], :nb_success => params[:nb_success], :nb_error => params[:nb_errors], :date => DateTime.now)
      return render :json =>{:message => "OK"}, :status=> 200
    rescue Exception => e
      return render :json =>{:errors => e.message}, :status=> 402
    end
  end
end