class Api::V1::PropertiesController < Api::V1::ApplicationController

  before_action :doorkeeper_authorize!
  respond_to :json

  ##============================================================##
  ## Cette fonction sert à récupererer les proprités shareimmo
  ## liées à un compte...
  ## http://lvh.me:3000/api/v1/properties?access_token=:access_token
  ##============================================================##
  def index
    api_params = params.reject do |test|
       ["access_token","format","controller","action"].include?(test)
    end
    api_params = api_params.to_hash.to_query
    @client   = Client.find(doorkeeper_token.resource_owner_id)
    store     = @client.store
    response  = HTTParty.get("#{store.shareimmo_api_url}/search/geojson/properties?user=#{@client.shareimmo_user_id}&apiKey=#{store.shareimmo_api_key}&apiToken=#{store.shareimmo_api_token}&#{api_params}")
    respond_with  JSON.parse(response.body)
  end

  ##============================================================##
  ## Cette fonction sert à récupererer les proprités shareimmo
  ##============================================================##
  def show
    @client   = Client.find(doorkeeper_token.resource_owner_id)
    store     = @client.store
    response  = HTTParty.get("#{store.shareimmo_api_url}/search/properties/#{params[:id]}?user=#{@client.shareimmo_user_id}&apiKey=#{store.shareimmo_api_key}&apiToken=#{store.shareimmo_api_token}")
    respond_with  JSON.parse(response.body)
  end



end
