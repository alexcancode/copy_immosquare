class BackOfficeController < ApplicationController

  include AdminAuthorized

  def index
  end


  ##============================================================##
  ## Store
  ##============================================================##
  def store_edit
  end

  def store_update
    @store.update_attributes(store_params)
    redirect_to back_office_stores_edit_path(@store.id)
  end


  ##============================================================##
  ## Taxes
  ##============================================================##
  def taxes
    @taxes = @store.taxes
  end

  def taxes_new
    @tax    = Tax.new(taxes_params)
    @tax.save
    redirect_to back_office_taxes_path(@store)
  end

  def taxes_update
    @tax    = Tax.find(params[:id])
    @tax.update_attributes(taxes_params)
    redirect_to back_office_taxes_path(@store)
  end

  def taxes_delete
    @tax    = Tax.find(params[:id])
    @tax.destroy
    redirect_to back_office_taxes_path(@store)
  end

  def taxes_current
    @store.taxes.each do |t|
      t.update_attribute(:is_current_tax, 0)
    end
    @tax    = Tax.find(params[:id])
    @tax.update_attribute(:is_current_tax, 1)
    redirect_to back_office_taxes_path(@store)
  end


  ##============================================================##
  ## Categories
  ##============================================================##
  def categories
    @categories   = @store.categories
  end

  def categories_create
    @category = Category.create(categories_params)
    redirect_to back_office_categories_path(@store)
  end

  def categories_delete
    @category = Category.find(params[:id])
    @category.destroy
    redirect_to back_office_categories_path(@store)
  end

  def categories_update
    @category = Category.find(params[:id])
    @category.update_attributes(categories_params)
    redirect_to back_office_categories_path(@store)
  end

  def subcategories_create
    @subcategory = Subcategory.create(subcategories_params)
    redirect_to back_office_categories_path(@store)
  end

  def subcategories_delete
    @subcategory = Subcategory.find(params[:id])
    @subcategory.destroy
    redirect_to back_office_categories_path(@store)
  end

  def subcategories_update
    @subcategory = Subcategory.find(params[:id])
    @subcategory.update_attributes(subcategories_params)
    redirect_to back_office_categories_path(@store)
  end

  def categories_sort
    params[:my_order].each_with_index do |id, index|
      category = Category.find(id)
      category.update_columns(:position =>index+1)
      category.save
    end
    render nothing: true
  end


  ##============================================================##
  ## Shippings
  ##============================================================##
  def shippings
    @shippings_mode   = @store.shipping_modes
  end

  def shippings_mode_create
    shippingMode = ShippingMode.new(shipping_mode_params)
    shippingMode.save
    redirect_to back_office_shippings_path(@store.id)
  end

  def shippings_mode_delete
    shippingMode  = ShippingMode.find(params[:id])
    shippingMode.destroy
    redirect_to back_office_shippings_path(@store)
  end

  def shippings_create
    @shipping   = Shipping.new(shippings_params)
    @shipping.save
    redirect_to back_office_shippings_path(@store)
  end

  def shippings_edit
    @shipping = Shipping.find(params[:id])
  end

  def shippings_update
    @shipping = Shipping.find(params[:id])
    @shipping.update_attributes(shippings_params)
    redirect_to back_office_shippings_path(@store)
  end

  def shippings_delete
    @shipping = Shipping.find(params[:id])
    @shipping.destroy
    redirect_to back_office_shippings_path(@store)
  end



  ##============================================================##
  ## Checkouts
  ##============================================================##
  def checkouts
    @checkout_units = CheckoutUnit.where(:store_id =>params[:store_id])
  end

  def checkout_units_update
    @checkout_unit = CheckoutUnit.where(:id => params[:id]).first_or_create
    @checkout_unit.update_attributes(checkout_units_params)
    redirect_to(:back)
  end

  def checkout_units_delete
    @checkout_unit = CheckoutUnit.find(params[:id])
    @checkout_unit.destroy
    redirect_to(:back)
  end


  ##============================================================##
  ## Products
  ##============================================================##
  def products
    @products = Product.where(:draft => 0,:store_id => @store.id).order(:slug)
  end

  def products_new
    @product = Product.where(:draft => 1,:store_id => @store.id, :is_printable => (params[:type].present? && params[:type] == "myprint")).first_or_create
    redirect_to back_office_products_edit_path(@store,@product.id)
  end

  def products_edit
    @product   = Product.find(params[:id])
    if @product.is_printable
      response    =  HTTParty.get("#{ENV['myprint_server']}/api/v2/templates/1")
      response2   = JSON.parse(response.body) if response.code == 200
      @templates  = response2["templates"] if response2["templates"].present?
    end
    @languages = @store.languages
  end

  ##============================================================##
  ## First, we find the difference between the associations BEFORE the form was filled out, and what is being submitted
  ## AFTER we update the page, destroy all the records that differ from before and after the form was submitted
  ##============================================================##
  def products_update
    @product  = Product.find(params[:id])


    deleted_ass_ids = @product.associations.collect(&:id) - params[:product][:left_association_ids].reject(&:blank?).map(&:to_i) if params[:product][:left_association_ids].present?
    if @product.update_attributes(products_params)
      deleted_ass_ids.each do |ass_id|
        LinkedProduct.where(:product_id => ass_id, :linked_product_id => @product.id).delete_all
      end if params[:product][:left_association_ids].present?
      flash[:success] = "Product saved"
      redirect_to back_office_products_path(@store)
    else
      @companies      = Company.where(:store_id => @store.id)
      @languages      = @store.languages
      flash[:danger]  = @product.errors.full_messages
      render 'products_edit'
    end
  end

  def products_delete
    @product = Product.find(params[:id])
    @product.destroy
    redirect_to back_office_products_path(@store)
  end

  def products_duplicate
    @product = Product.find(params[:id])

    @new_product = @product.deep_clone(:include => [:pricings, :product_users, :left_associations, :right_associations, {:pricings => :pricing_plan}, {:pricings => :pricing_checkouts}, {:pricings => :extras}, {:pricings => :sales}, {:pricings => {:variant_categories => :variant_combinations}}], :use_dictionary => true) do |original, kopy|

    end

    @new_product.company_id = nil
    @new_product.subcategory_id = nil
    @new_product.pricings.each{|p| p.checkout_unit_id = nil}

    # duplicate images
    @new_product.cover = open(@product.cover.url)
    @new_product.logo_app = open(@product.logo_app.url)
    @new_product.logo = open(@product.logo.url)
    @new_product.store_id = params[:new_store_id]

    if @new_product.save

      # duplicate images
      @product.product_assets.each do |asset|
        p = ProductAsset.new(:product_id => @new_product.id)
        p.image = open(asset.image.url)
        p.save
      end

      redirect_to back_office_products_path
    else
      flash[:danger] = @new_product.errors.full_messages
      redirect_to back_office_products_path
    end

  end


  ##============================================================##
  ## Pricings
  ##============================================================##
  def pricings
    @product  = Product.find(params[:product_id])
    @pricings = @product.pricings.where(:status => "active")
  end

  def pricings_new
    @pricing = Pricing.where(:draft => 1,:product_id => params[:product_id]).first_or_create
    PricingPlan.where(:pricing => @pricing.id).first_or_create     if params[:type] == "plan"
    PricingCheckout.where(:pricing => @pricing.id).first_or_create if params[:type] == "checkout"
    redirect_to back_office_pricings_edit_path(:id => @pricing.id)
  end

  def pricings_edit
    @pricing  = Pricing.find(params[:id])
  end

  def pricings_update
    @pricing = Pricing.find(params[:id])
    if @pricing.update_attributes(pricings_params)
      flash[:success] = "Pricing saved successfully"
      redirect_to back_office_pricing_prices_path(:pricing_id => @pricing.id)
    else
      render 'pricings_edit'
    end
  end

  def pricings_delete
    @pricing = Pricing.find(params[:id])
    @pricing.update_attribute(:status, "hidden")
    redirect_to :back
  end

  def pricings_duplicate
    @pricing = Pricing.find(params[:id])
    if @pricing.is_a_plan?
      @new_pricing = @pricing.deep_clone(:include => [:extras, :pricing_plan, {:variant_categories => :variant_combinations}]) do |original, kopy|
        kopy.product_id = original.product_id if kopy.respond_to?(:product_id)
      end
    else
      @new_pricing = @pricing.deep_clone(:include => [:extras, :pricing_checkouts, {:variant_categories => :variant_combinations}]) do |original, kopy|
        kopy.product_id = original.product_id if kopy.respond_to?(:product_id)
      end
    end
    if @new_pricing.save
      redirect_to back_office_pricings_path(:product_id => @pricing.product_id)
    else
      flash[:danger] = @new_pricing.errors.full_messages
      redirect_to back_office_pricings_path(:product_id => @pricing.product_id)
    end
  end

  def pricings_sort
    params[:my_order].each_with_index do |id, index|
      @pricing = Pricing.find(id)
      @pricing.update_columns(:position =>index+1)
      @pricing.save
    end
    render nothing: true
  end


  ##============================================================##
  ## Extras
  ##============================================================##
  def extras
    @pricing = Pricing.find(params[:pricing_id])
    @extras = @pricing.extras.where(:status => "active")
  end

  def extras_new
    @pricing = Pricing.find(params[:pricing_id])
    @extra   = Extra.new
  end

  def extras_create
    @extra = Extra.new(extras_params)
    @extra.save
    @pricing = @extra.pricing
    @extras = @pricing.extras.where(:status => "active")
  end

  def extras_edit
    @extra    = Extra.find(params[:id])
    @pricing  = @extra.pricing
    @extras = @pricing.extras.where(:status => "active")
    render 'extras_new'
  end

  def extras_update
    @extra    = Extra.find(params[:id])
    @extra.update_attributes(extras_params)
    @pricing = @extra.pricing
    @extras = @pricing.extras.where(:status => "active")
    render 'extras_create'
  end

  def extras_delete
    @extra    = Extra.find(params[:id])
    @pricing  = @extra.pricing
    @extras = @pricing.extras.where(:status => "active")
    @extra.unable!
  end

  ##============================================================##
  ## Combinations
  ##============================================================##
  def combinations
    @pricing = Pricing.find(params[:pricing_id])
    @combinations = VariantCombination.includes(:variants).where("variants.variant_category_id" => @pricing.variant_categories.pluck(:id))
  end

  def combinations_update
    @pricing = Pricing.find(params[:pricing_id])
    if params[:combinations].present?
      params[:combinations].each do |index, comb|
        combination = VariantCombination.find(comb[:id])
        combination.update_attributes(:sku => comb[:sku], :display => comb[:display], :price_percentage => comb[:price_percentage])
      end
    end
    redirect_to back_office_pricings_path(:product_id => @pricing.product_id, :store_id => @store.id)
  end

  ##============================================================##
  ## Variants
  ##============================================================##
  def variants
    @pricing = Pricing.find(params[:pricing_id])

    @combinations = VariantCombination.joins(:variant_categories).where(:variant_categories => {:id => @pricing.variant_categories.pluck(:id)}).uniq!
  end

  def variants_new
    @pricing  = Pricing.find(params[:pricing_id])
    @category = VariantCategory.new
    @api_key  = Digest::SHA2.new(512).hexdigest(current_user.id.to_s+"_"+current_user.email)
  end

  def variants_create
    @category = VariantCategory.new(variants_params)
    @category.save

    @category.destroy_combinations
    @category.generate_combinations

    @combinations = VariantCombination.joins(:variant_categories).where(:variant_categories => {:id => @category.pricing.variant_categories.pluck(:id)}).uniq!

    @pricing = @category.pricing
  end

  def variants_edit
    @category = VariantCategory.find(params[:id])
    @pricing = @category.pricing
    @api_key = Digest::SHA2.new(512).hexdigest(current_user.id.to_s+"_"+current_user.email)
    render 'variants_new'
  end

  def variants_update
    @category = VariantCategory.find(params[:id])
    old_values = @category.values
    @category.update_attributes(variants_params)

    @category.destroy_combinations
    @category.generate_combinations
    @combinations = VariantCombination.joins(:variant_categories).where(:variant_categories => {:id => @category.pricing.variant_categories.pluck(:id)}).uniq!
    @pricing = @category.pricing
    render 'variants_create'
  end

  def variants_delete
    @category = VariantCategory.find(params[:id])
    @pricing = @category.pricing
    @category.destroy_combinations
    @category.destroy
    if @pricing.variant_categories.first.present?
      @pricing.variant_categories.first.generate_combinations
    end
    @pricing.reload
    @combinations = VariantCombination.joins(:variant_categories).where(:variant_categories => {:id => @pricing.variant_categories.pluck(:id)}).uniq!
  end

  def variants_sort
    params[:my_order].each_with_index do |id, index|
      @variant = VariantCategory.find(id)
      @variant.update_columns(:position =>index+1)
      @variant.save
    end
    render nothing: true
  end


  ##============================================================##
  ## PricingPrice
  ##============================================================##
  def pricing_prices
    @pricing = Pricing.find(params[:pricing_id])
    ##============================================================##
    ## Checkout
    ##============================================================##
    if @pricing.is_a_checkout?
      @pricing.pricing_plan.destroy if @pricing.pricing_plan.present?
      @checkouts = @pricing.pricing_checkouts
      @pricing.pricing_checkouts.first_or_create if @checkouts.blank?
      @checkouts = @pricing.pricing_checkouts
    end
    ##============================================================##
    ## Plan
    ##============================================================##
    if @pricing.is_a_plan?
      @pricing.pricing_checkouts.destroy_all
      @plan = @pricing.pricing_plan
    end
  end


  def pricing_checkouts_new
    @pricing = Pricing.find(params[:pricing_id])
    @pricing_checkout   = PricingCheckout.new

  end

  def pricing_checkouts_create
    @pricing_checkout = PricingCheckout.new(pricing_checkouts_params)
    @pricing_checkout.save
    @pricing = @pricing_checkout.pricing
  end

  def pricing_checkouts_edit
    @pricing_checkout    = PricingCheckout.find(params[:id])
    @pricing  = @pricing_checkout.pricing
    render 'pricing_checkouts_new'
  end

  def pricing_checkouts_update
    @pricing_checkout    = PricingCheckout.find(params[:id])
    @pricing_checkout.update_attributes(pricing_checkouts_params)
    @pricing = @pricing_checkout.pricing
    render 'pricing_checkouts_create'
  end


  def pricing_checkouts_delete
    @pricing_checkout  = PricingCheckout.find(params[:id])
    @pricing  = @pricing_checkout.pricing
    @pricing_checkout.destroy
  end



  def pricing_plans_edit
    @pricing_plan = PricingPlan.find(params[:id])
    @pricing      = @pricing_plan.pricing
  end

  def pricing_plans_update
    @pricing_plan    = PricingPlan.find(params[:id])
    @pricing_plan.update_attributes(pricing_plans_params)
    @pricing = @pricing_plan.pricing
  end



  ##============================================================##
  ## Visibilities
  ##============================================================##
  def visibilities
    @visibilities = ProductUser.where(:product_id => params[:id])
    @product = Product.find(params[:id])
    @all_clients = @store.clients - @visibilities.map{|v| v.client}
  end

  def visibilities_add
    @visibility = ProductUser.new(visibilities_params)
    @visibility.product_id = params[:product_id]

    if @visibility.save
      redirect_to(:back)
    else
      flash[:danger] = @visibility.errors.full_messages
      redirect_to(:back)
    end
  end

  def visibilities_delete
    @visibility = ProductUser.find(params[:product_id])
    @visibility.destroy
    redirect_to(:back)
  end

  ##============================================================##
  ## Users
  ##============================================================##
  def users
    @users = User.all.order(:email)
    @minimum_password_length = 8
  end

  def users_create
    @user = User.new(users_params)
    if @user.save
      redirect_to back_office_users_path
    else
      flash[:danger] = @user.errors.full_messages
      redirect_to back_office_users_path
    end
  end

  def users_update
    @user = User.find(params[:id])
    if @user.update_attributes(users_params)
      redirect_to back_office_users_path
    else
      flash[:danger] = @user.errors.full_messages
      redirect_to back_office_users_path
    end
  end

  def users_delete
    @user = User.find(params[:id])
    @user.destroy
    redirect_to(:back)
  end

  ##============================================================##
  ## Sales
  ##============================================================##
  def sales
    @pricing  = Pricing.find(params[:pricing_id])
    @sales    = @pricing.sales
  end

  def sales_new
    @pricing  = Pricing.find(params[:pricing_id])
    @sale     = Sale.new
  end

  def sales_create
    @sale = Sale.new(sales_params)
    @sale.save
    @pricing  = @sale.pricing
  end

  def sales_edit
    @sale     = Sale.find(params[:id])
    @pricing  = @sale.pricing
    render "sales_new"
  end

  def sales_update
    @sale = Sale.find(params[:id])
    @sale.update_attributes(sales_params)
    @pricing  = @sale.pricing
    render 'sales_create'
  end

  def sales_delete
    @sale     = Sale.find(params[:id])
    @pricing  = @sale.pricing
    @sale.destroy
  end

  ##============================================================##
  ## Product Assets
  ##============================================================##
  def product_asset_create
    @product_asset = ProductAsset.create(product_assets_params)
    @product = @product_asset.product
  end

  def product_asset_delete
    @asset = ProductAsset.find(params[:id])
    @asset.destroy
  end

  def product_asset_sort
    params[:my_order].each_with_index do |id, index|
      @product = ProductAsset.find(id)
      @product.update_columns(:position =>index+1)
      @product.save
    end
    render nothing: true
  end


  ##============================================================##
  ## Companies
  ##============================================================##
  def companies
    @companies = Company.where(:store_id => params[:store_id]).order(:name)
  end

  def companies_update
    @company = Company.where(:id => params[:id]).first_or_create do |cie|
      # if it's a new record, create the Api keys
      cie.api_key = SecureRandom.urlsafe_base64(nil, false)
      cie.api_token = SecureRandom.urlsafe_base64(nil, false)
      cie.store_id = params[:store_id]
    end

    @company.update_attributes(companies_params)

    # come here if you want to regenerate the keys
    @company.update_attributes(:api_key => SecureRandom.urlsafe_base64(nil, false), :api_token => SecureRandom.urlsafe_base64(nil, false)) if params[:company][:regenerate].present? && params[:company][:regenerate] == "1"

    redirect_to(:back)
  end

  def companies_delete
    @company = Company.find(params[:id])
    @company.destroy
    redirect_to(:back)
  end



  ##============================================================##
  ## Orders
  ##============================================================##
  def bills
    @bills = Bill.includes(:client).where("clients.store_id = ?", @store.id).references(:clients).order(:date => "desc")
  end

  def bills_send
    @bill = Bill.find(params[:id])
    @bill.update_attribute(:has_been_shipped, true)
    return redirect_to back_office_bills_path
  end


  private

  def store_params
    params.require(:store).permit!
  end

  def taxes_params
    params.require(:tax).permit!
  end

  def categories_params
    params.require(:category).permit!
  end

  def subcategories_params
    params.require(:subcategory).permit!
  end

  def shippings_params
    params.require(:shipping).permit!
  end

  def products_params
    params.require(:product).permit!
  end

  def pricings_params
    params.require(:pricing).permit!
  end

  def checkout_units_params
    params.require(:checkout_unit).permit!
  end

  def visibilities_params
    params.require(:product_user_store).permit!
  end

  def users_params
    params.require(:user).permit!
  end

  def sales_params
    params.require(:sale).permit!
  end

  def product_assets_params
    params.require(:product_asset).permit!
  end

  def companies_params
    params.require(:company).permit!
  end

  def variants_params
    params.require(:variant_category).permit!
  end

  def extras_params
    params.require(:extra).permit!
  end

  def pricing_checkouts_params
    params.require(:pricing_checkout).permit!
  end

  def pricing_plans_params
    params.require(:pricing_plan).permit!
  end

  def translation_attributes
    params.require(:translation).permit!
  end

  def shipping_mode_params
    params.require(:shipping_mode).permit!
  end

end
