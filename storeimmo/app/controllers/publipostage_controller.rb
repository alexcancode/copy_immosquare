class PublipostageController < ApplicationController

  def find_routes
    @zipcode  = params["post_canada_publipostage"]["zipcode"][0..2].upcase
    routes    = PostCanadaPublipostage.where(:zipcode_rta => @zipcode)
    if routes.present?
      @roads    = Array.new
      @map      = "https://maps.canadapost.ca/RouteFinder/Maps/Current/CPC_SCP_#{routes[0].supervisor_province}_#{@zipcode}.pdf" if routes.present?
      routes.group_by(&:shipping_road_id).each do |road_id,routes|
        domiciles   = sum_my_array(routes.map(&:consumer_choice_domiciles))
        appartments = sum_my_array(routes.map(&:consumer_choice_appartments))
        shops       = sum_my_array(routes.map(&:consumer_choice_shops))
        farms       = sum_my_array(routes.map(&:consumer_choice_farms))
        @roads << {
          :road_id     => road_id,
          :domiciles   => domiciles,
          :appartments => appartments,
          :shops       => shops,
          :farms       => farms,
          :total       => domiciles+appartments+shops+farms
        }
      end
    end
  end





end
