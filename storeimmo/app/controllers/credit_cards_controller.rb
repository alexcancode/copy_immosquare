class CreditCardsController < ApplicationController

  include ClientAuthorized
  include ClientCart

  before_action :config_stripe


  def index
    @profile = current_client.profile
    begin
      if current_client.stripe_customer_id.present?
        customer      = Stripe::Customer.retrieve(current_client.stripe_customer_id)
        @cards        = customer.sources.all(:object => "card")
        @default_card = customer.default_source
      end
    rescue Stripe::StripeError => e
      flash[:danger] = e.json_body[:error][:message]
    rescue Exception => e
      JulesLogger.info e
      JulesLogger.info "toto"
      flash[:danger] = e.message
    end
  end


  def card_new
    begin
      if current_client.stripe_customer_id.blank?
        customer  = Stripe::Customer.create(:description =>params["credit_card"]["name"],:source => params["credit_card"]["stripeToken"])
        current_client.update_attribute(:stripe_customer_id, customer.id)
      else
        customer = Stripe::Customer.retrieve(current_client.stripe_customer_id)
        card     = customer.sources.create(:source => params["credit_card"]["stripeToken"])
        customer.default_source = card.id
        customer.save
      end
    rescue Stripe::StripeError => e
      flash[:danger] = e.json_body[:error][:message]
    rescue Exception => e
      flash[:danger] = e.message
    end
    redirect_to dashboard_cards_path
  end


  def card_default
    begin
      customer = Stripe::Customer.retrieve(current_client.stripe_customer_id)
      card     = customer.sources.retrieve(params[:stripe_id])
      customer.default_source = card.id
      customer.save
    rescue Stripe::StripeError => e
      flash[:danger] = e.json_body[:error][:message]
    rescue Exception => e
      flash[:danger] = e.message
    end
    redirect_to dashboard_cards_path
  end



  def card_delete
    begin
      customer     = Stripe::Customer.retrieve(current_client.stripe_customer_id)
      card         = customer.sources.retrieve(params[:stripe_id])
      default_card = customer.sources.retrieve(customer.default_source)
      raise "Default Card can't be deleted" if card.id == default_card.id
      card.delete
    rescue Stripe::StripeError => e
      flash[:danger] = e.json_body[:error][:message]
    rescue Exception => e
      flash[:danger] = e.message
    end
    redirect_to dashboard_cards_path
  end




end
