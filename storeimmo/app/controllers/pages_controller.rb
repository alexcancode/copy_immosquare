class PagesController < ApplicationController

  include ClientCart
  before_action :getProducts, :only => [:home,:category_show]

  def home
    @products_with_category     = @products.select {|p| p.subcategory_id.present?}
    @products_for_slider        = @products.reject {|p| p.is_favorite != 1}
  end

  ##============================================================##
  ## http://.../stylesheet.css
  ##============================================================##
  def stylesheet
  end

  ##============================================================##
  ##
  ##============================================================##
  def choose_store
    @stores = Store.all.order(:position)
  end

  def contact
  end

  def contact_create
    @contact = Contact.create(contact_params)
    flash[:success] = t('app.thanks_you')
    if @contact.pricing_id.present?
      pricing = Pricing.find(@contact.pricing_id)
      redirect_to product_show_path(pricing.product_id)
    else
      redirect_to contact_path
    end
  end

  def faq
  end

  def thanks
  end


  def category_show
    @category                   = Subcategory.find(params[:id])
    @products_with_category     = @products.select { |p| p.subcategory_id.present? && p.subcategory_id == @category.id }
    render 'home'
  end


  def product_show
    begin
      @product  = Product.find(params[:id])
      if client_signed_in? && @product.is_printable
        if params[:order].present?
          call          = HTTParty.get("#{ENV['myprint_server']}/api/v2/orders/#{params[:order]}")
          @print_call   = JSON.parse(call.body).deep_symbolize_keys
        elsif @product.print_template_id.present?
          @clients_ads  = JSON.parse(HTTParty.get("#{@store.shareimmo_api_url}/search/properties?user=#{current_client.shareimmo_user_id}&apiKey=#{@store.shareimmo_api_key}&apiToken=#{@store.shareimmo_api_token}").body)["properties"]
          @pdfs         = JSON.parse(HTTParty.get("#{ENV['myprint_server']}/api/v2/templates/#{@product.print_template_id}/pdfs").body)
          @selected_pdf_id = @pdfs["pdfs"].first["id"] if @pdfs["pdfs"].present? && @pdfs["pdfs"].first.present?
          @n = @pdfs["pdfs"].first["property_count"] if @pdfs["pdfs"].present? && @pdfs["pdfs"].first.present?
        end
      end
      @linked_products = @product.associations
    rescue Exception => e
      flash[:danger] = e.message
      redirect_to root_path
    end
  end



  private

  ##============================================================##
  ## 1/ Get all products for store
  ## 2/ Remove products reserved for specific client
  ##============================================================##
  def getProducts
    products            = Product.where(:draft => 0,:store_id => @store.id)
    customized_products = ProductUser.where(:product_id =>products.pluck(:id)).select(:client_id,:product_id)

    if client_signed_in?
      customized_products = customized_products.reject do |product|
        product.client_id == current_client.work_for_himself? ? current_client.id : current_client.working_as_client_id
      end
    end

    @products = products.reject do |product|
      customized_products.pluck(:product_id).include?(product.id)
    end
  end

  def contact_params
    params.require(:contact).permit!
  end

end
