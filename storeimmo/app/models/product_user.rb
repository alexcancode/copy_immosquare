class ProductUser < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :product
  belongs_to :client

end
