class Sale < ApplicationRecord


  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :client
  belongs_to :pricing

  has_many :cart_item_sales
  has_many :order_sales


  ##============================================================##
  ## Money
  ##============================================================##
  monetize :amount_cents, with_model_currency: :currency
  def currency
    self.pricing.currency if self.pricing.present?
  end





end
