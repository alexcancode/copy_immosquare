class CheckoutUnit < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :store


  translates :name
  globalize_accessors :attributes => [:name]
end
