class Contact < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :store
  belongs_to :client
end
