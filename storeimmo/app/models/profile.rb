class Profile < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :client

   Paperclip.interpolates('client_id_paperclip') do |attachment, style|
    attachment.instance.client_id.to_s.parameterize
  end

  #============================================================##
  ## Paperclip : Profil avatar
  ##============================================================##
  has_attached_file :avatar,
  # :path => "#{Rails.env}/users/:client_id_paperclip/avatar_:style.:extension",
  :styles => {
    :large    => ["450x450", :png]
  },
  :convert_options => {
    :large    => "-background transparent -gravity center -extent 450x450",
  },
  :default_url => "https://placehold.it/450x450"

  validates_attachment_content_type :avatar, :content_type => /\Aimage/
  validates_attachment_size :avatar, :less_than=>2.megabytes


  #============================================================##
  ## Paperclip : Logo
  ##============================================================##
  has_attached_file :logo,
  :path => "#{Rails.env}/users/:client_id_paperclip/logo_:style.:extension",
  :styles => {
    :large    => ["450x450", :png]
  },
  :convert_options => {
    :large    => "-background transparent -gravity center -extent 450x450",
  },
  :default_url => "https://placehold.it/450x450"

  validates_attachment_content_type :logo, :content_type => /\Aimage/
  validates_attachment_size :logo, :less_than=>2.megabytes




end
