class GatewayActivation < ApplicationRecord

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :gateway
  belongs_to :client
  belongs_to :store
  has_many :import_trackings, :dependent => :destroy


  ##============================================================##
  ## Validation
  ##============================================================##
  validate :check_username_passowrd



  def check_username_passowrd
    if gateway.name == "pagesimmo"
      soap = Savon.client(:wsdl => "http://wsimmosquare.pagesimmo.com/storeimmo.asmx?wsdl")
      call  = soap.call(:authentification, :message => {"_agence_Login" => username, "_agence_Pwd" => password, "_editeur_Key" => ENV['gercop_editor_key']})
      token = call.body[:authentification_response][:authentification_result]
      errors.add(:password,"Not match") if token.blank?
    end
  end

end
