class Bill < ApplicationRecord

  ##============================================================##
  ## Money
  ##============================================================##
  monetize :subtotal_cents,             with_model_currency: :currency
  monetize :tax1_cents,                 with_model_currency: :currency
  monetize :tax2_cents,                 with_model_currency: :currency
  monetize :shipping_unit_price_cents,  with_model_currency: :currency
  monetize :shipping_cents,             with_model_currency: :currency
  monetize :total_price_cents,          with_model_currency: :currency


  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :items_data, JSON
  serialize :sales_data, JSON
  serialize :payment_data, JSON

  ##============================================================##
  ## Callbacks
  ##============================================================##
  before_save :set_store


  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :client
  belongs_to :store
  belongs_to :product
  belongs_to :origin, :polymorphic => true
  has_one :authorization, :class_name => "Authorization", :foreign_key => "first_bill_id"

  ##============================================================##
  ## Bill number
  ##============================================================##
  acts_as_list :column => :number, scope: :store


  def set_store
    self.store_id = self.client.store_id
  end


  ##============================================================##
  ## This function is called in the checkouts controller when a
  ## user makes a purchase.
  ## We create and activate all the services in the order.
  ##============================================================##
  def activate_services
    now = DateTime.now

    self.items_data.each do |item|

      if item["is_a_service"]
        # We check if we already have that service activated (only for tokens type services)

        pricing = Pricing.find(item["pricing_id"].to_i)
        auths   = Authorization.where(:client_id => client_id, :pricing_id => item["pricing_id"], :status => "active", :mode => "tokens")

        if auths.any?
          # The service already exists, let's refill it (again, only for tokens)

          auth = auths.first
          auth.update_attributes(
            :pricing_id => item["pricing_id"],
            :status => "active",
            :last_bill_date => now,
            :next_bill_date => nil,
            :purchase_date => now,
            :limit_date => item["limit_date"],
            :tokens => auth.tokens.to_i + item["tokens"].to_i,
            :currency => self.currency
            )

          # add to history
          auth.add_to_history("refill", now, auth.tokens.to_i + item["tokens"].to_i)

        else
          # It's a new service, let's create it

          next_bill = case item["mode"]
          when "plan_month"
            now + 1.month
          when "plan_year"
            now + 12.month
          else nil
          end



          auth = Authorization.create(
            :client_id        => client_id,
            :company_id       => item["company_id"],
            :pricing_id       => item["pricing_id"],
            :first_bill_id    => self.id,
            :status           => "active",
            :product_sku      => pricing.product.sku,
            :last_bill_date   => now,
            :next_bill_date   => next_bill,
            :purchase_date    => now,
            :limit_date       => item["limit_date"],
            :tokens           => item["tokens"],
            :mode             => item["mode"],
            :history          => [{}],
            :currency         => self.currency,
            :item_data        => item
            )

          # add to history
          auth.add_to_history("activation", now, item["tokens"])

          item["extras"].each do |extra|
            AuthorizationsExtra.create(:authorization_id => auth.id, :extra_id => extra["id"])
          end
        end

      else

        pricing = Pricing.find(item["pricing_id"].to_i)
        auth = Authorization.create(
          :client_id          => self.client_id,
          :company_id         => item["company_id"],
          :pricing_id         => item["pricing_id"],
          :first_bill_id      => self.id,
          :status             => "active",
          :product_sku        => pricing.product.sku,
          :last_bill_date     => now,
          :purchase_date      => now,
          :limit_date         => item["limit_date"],
          :tokens             => item["tokens"],
          :mode               => item["mode"],
          :history            => [{}],
          :currency           => self.currency,
          :item_data          => item
          )

        # add to history
        auth.add_to_history("activation", now, item["tokens"])
      end
    end
  end

end
