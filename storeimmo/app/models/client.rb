class Client < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :store
  belongs_to :working_as_client, :class_name => Client , :foreign_key => "working_as_client_id"
  has_one :profile, dependent: :destroy
  has_one :team,    dependent: :destroy
  has_many :team_members, dependent: :destroy
  has_many :teams, through: :team_members
  has_many :authorizations
  has_many :gateway_activations
  has_many :bills



  ##============================================================##
  ## Fake
  ##============================================================##
  attr_accessor :invitation_template
  attr_accessor :invitation_team_member


  ##============================================================##
  ## We ave remove validatable module from devise
  ## to create our own validation with store scope.
  ##============================================================##
  validates_presence_of     :email
  validates_format_of       :email, with: Devise.email_regexp
  validates_uniqueness_of   :email,scope: :store_id
  validates_presence_of     :password
  validates_confirmation_of :password
  validates_length_of       :password, within: Devise.password_length


  ##============================================================##
  ## Devise :
  ## Include default devise modules. Others available are:
  ## :confirmable, :lockable, :timeoutable and :omniauthable
  ##============================================================##
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, authentication_keys: [:email,:store_id]


  ##============================================================##
  ## Devise overwrite to scope user by store
  ##============================================================##
  def self.find_for_authentication(warden_conditions)
    where(:email => warden_conditions[:email], :store_id => warden_conditions[:store_id]).first
  end




  ##============================================================##
  ## Define domain must be use for current_client
  ##============================================================##
  def domain
    self.store.store_domain.domain
  end

  ##============================================================##
  ## Return false is current_client is working as a team Member
  ##============================================================##
  def work_for_himself?
    self.working_as_client_id == self.id
  end




  ##============================================================##
  ## Must conform  to any password format validation rules of the application.
  ## This default fixes the most common scenarios: Passwords must contain
  ## lower + upper case, a digit and a symbol.
  ##============================================================##
  def self.random_password
    "aA1!" + Devise.friendly_token[0, 20]
  end

  ##============================================================##
  ## Name of the client
  ##============================================================##
  def name
    name = ""
    name += self.profile.first_name + " " if self.profile.present? && self.profile.first_name.present?
    name += self.profile.last_name if self.profile.present? && self.profile.last_name.present?
    return name.blank? ? self.email : name
  end





end
