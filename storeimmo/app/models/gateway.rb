class Gateway < ApplicationRecord

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :store
  has_many :gateway_activations
end
