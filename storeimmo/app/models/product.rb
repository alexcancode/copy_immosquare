class Product < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :subcategory
  belongs_to :store

  belongs_to :company

  has_many :product_assets, -> { order(position: :asc) },:dependent => :destroy
  has_many :product_users
  has_many :pricings, -> { order(position: :asc) },:dependent => :destroy
  has_one :pricing, -> { where :price_to_home => 1, :status => "active" }

  has_many :clients, :through => :product_users
  has_many :wysiwyg_assets, as: :imageable,:dependent => :destroy


  # everything is explained here : http://cobwwweb.com/bi-directional-has-and-belongs-to-many-on-a-single-model-in-rails
  has_many :left_product_associations, :foreign_key => :product_id, :class_name => 'LinkedProduct'
  has_many :left_associations, :through => :left_product_associations, :source => :linked_product
  has_many :right_product_associations, :foreign_key => :linked_product_id, :class_name => 'LinkedProduct'
  has_many :right_associations, :through => :right_product_associations, :source => :product

  ##============================================================##
  ## Translations
  ##============================================================##
  translates :name, :baseline, :summary, :description
  globalize_accessors



  ##============================================================##
  ## Methods
  ##============================================================##
  def category
    self.subcategory.category if self.subcategory.present?
  end

  def associations
    (left_associations + right_associations).flatten.uniq
  end


  ##============================================================##
  ## Paperclip : product logo
  ##============================================================##
  has_attached_file :logo,
  :path => "#{Rails.env}/products/:id/logo_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small        => ["100x100", :png],
    :medium       => ["200x200", :png],
    :large        => ["500x500", :png]
    },
    :convert_options => {
      :small          => "-background transparent -gravity center -extent 100x100 -quality 70",
      :medium         => "-background transparent -gravity center -extent 200x200 -quality 80",
      :large          => "-background transparent -gravity center -extent 500x500",
      },
      :default_url => "https://placehold.it/200x200"


      validates_attachment_content_type :logo, :content_type => /\Aimage/
      validates_attachment_size :logo, :less_than=>2.megabytes


  ##============================================================##
  ## Paperclip : product logo_app
  ##============================================================##
  has_attached_file :logo_app,
  :path => "#{Rails.env}/products/:id/logo_app_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small        => ["100x100", :png],
    :medium       => ["200x200", :png],
    :large        => ["500x500", :png]
    },
    :convert_options => {
      :small          => "-background transparent -gravity center -extent 100x100 -quality 70",
      :medium         => "-background transparent -gravity center -extent 200x200 -quality 80",
      :large          => "-background transparent -gravity center -extent 500x500",
      },
      :default_url => "https://placehold.it/200x200"


      validates_attachment_content_type :logo_app, :content_type => /\Aimage/
      validates_attachment_size :logo_app, :less_than=>2.megabytes


  ##============================================================##
  ## Paperclip : product cover
  ##============================================================##
  has_attached_file :cover,
  :path             => "#{Rails.env}/products/:id/cover_:style.:extension",
  :hash_secret      => "some_secret",
  :styles           => { :large  => ["945x500", :png]},
  :convert_options  => { :large  => "-background transparent -gravity center -extent 945x500"},
  :default_url      => "https://placehold.it/945x500"

  validates_attachment_content_type :cover, :content_type => /\Aimage/
  validates_attachment_size :cover, :less_than=>2.megabytes


  private


end
