class Country < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :shippings,            :dependent => :destroy
  has_many :stores,               :dependent => :nullify


end
