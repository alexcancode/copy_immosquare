class Team < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :client
  has_many :team_members, dependent: :destroy

end
