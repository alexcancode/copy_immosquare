class VariantCategory < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  has_and_belongs_to_many :variant_combinations

  belongs_to :pricing
  acts_as_list scope: :pricing

  ##============================================================##
  ## Validations
  ##============================================================##
  validates_presence_of :name, :unless => :new_record?

  ##============================================================##
  ## Combinations
  ##============================================================##
  def generate_combinations
    # generate all the combinations with the values of this category

    values = VariantCategory.where(:id => self.pricing.variant_categories.pluck(:id)).where.not(:id => self.id).pluck(:values).map {|val| val.split(",")}

    comb = self.values.split(",").product(*values).map{|a| a.join("|*|")}

    comb.each do |combination|
      vc = VariantCombination.create(:name => combination, :variant_categories => self.pricing.variant_categories)
    end
  end

  def regenerate_combinations(old_values)
    # add or delete the combinations which changed

    # if self.pricing.variant_categories.first.present?
    #   self.pricing.variant_categories.first.variants.each do |variant|
    #     variant.generate_combinations
    #   end
    # end
  end

  def destroy_combinations
    self.pricing.variant_categories.each do |cat|
      cat.variant_combinations.each do |c|
        c.destroy
      end
    end
  end


end
