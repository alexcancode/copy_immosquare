class TeamMember < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :team
  belongs_to :client

  ##============================================================##
  ## Fake Attributes
  ##============================================================##
  attr_accessor :emails
  attr_accessor :emails_to_add_to_team

end
