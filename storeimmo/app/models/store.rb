class Store < ApplicationRecord

  ##============================================================##
  ## CUSTOM CSS
  ##============================================================##
  COMPILED_FIELDS = [:color1,:color2]

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :country
  has_one :store_domain,      dependent: :destroy
  has_many :categories, -> { order(position: :asc) },   dependent: :destroy
  has_many :taxes,            dependent: :destroy
  has_many :shipping_modes,   dependent: :destroy
  has_many :products,         dependent: :destroy
  has_many :contacts,         dependent: :destroy
  has_many :checkout_units,   dependent: :destroy
  has_many :gateways,         dependent: :destroy
  has_many :bills
  has_many :clients

  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :languages, JSON


  ##============================================================##
  ## Translations
  ##============================================================##
  translates :baseline
  globalize_accessors

  ##============================================================##
  ## Usefull Attributes
  ##============================================================##
  def domain
    self.store_domain.domain
  end

  def tax
    self.taxes.where(:is_current_tax => 1).first || Tax.new(:store_id => self.id, :tax1_name =>nil,:tax1_number => nil,:tax1_value => 0,:tax2_name =>nil,:tax2_number => nil,:tax2_value => 0)
  end

  def locale
    self.languages.first if self.languages.present?
  end





  ##============================================================##
  ## Paperclip : product logo
  ##============================================================##
  has_attached_file :logo,
  :path => "#{Rails.env}/store/:id/logo_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small                => ["100x100", :png],
    :medium               => ["200x200", :png],
    :large                => ["500x500", :png],
    :small_transparent    => ["100x100", :png],
    :medium_transparent   => ["200x200", :png],
    :large_transparent    => ["500x500", :png]
    },
    :convert_options => {
      :small              => "-background transparent -quality 70",
      :medium             => "-background transparent -quality 80",
      :large              => "-background transparent -strip",
      :small_transparent  => "-background transparent -gravity center -extent 100x100 -quality 70",
      :medium_transparent => "-background transparent -gravity center -extent 200x200 -quality 80",
      :large_transparent  => "-background transparent -flatten +matte -quality 90 -strip"
      },
      :default_url => "https://placehold.it/300x300"
      validates_attachment_content_type :logo, :content_type => /\Aimage/
      validates_attachment_size :logo, :less_than=>2.megabytes

  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_logo
  after_save :compile, :if => :compiled_attributes_changed?

  def rename_logo
    extension = File.extname(logo_file_name).downcase
    self.logo.instance_write :file_name, "logo_#{Time.now.to_i.to_s}#{extension}"
  end



  def compile
    begin
      body            = ERB.new(File.read(File.join(Rails.root, 'app', 'assets', 'stylesheets', 'store.scss.erb'))).result(self.get_binding)
      compressed_body = ::Sass::Engine.new(body,{:syntax => :scss,:cache => false,:read_cache => false,:style => :compressed}).render
      self.update_attribute(:theme,compressed_body)
    rescue Exception => e
      JulesLogger.info e.message
    end
  end

  def get_binding
    binding
  end

  private

  def compiled_attributes_changed?
    changed_attributes.keys.map(&:to_sym).any? { |f| COMPILED_FIELDS.include?(f)}
  end

end
