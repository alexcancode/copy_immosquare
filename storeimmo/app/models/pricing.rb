class Pricing < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :product
  belongs_to :checkout_unit

  has_many :extras, :dependent => :destroy
  has_many :sales, :dependent => :destroy
  has_many :variant_categories, -> { order(position: :asc) }, :dependent => :destroy

  has_many :pricing_checkouts, :dependent => :destroy
  has_one :pricing_plan, :dependent => :destroy
  accepts_nested_attributes_for :pricing_plan

  ##============================================================##
  ## Position
  ##============================================================##
  acts_as_list scope: :product



  ##============================================================##
  ## Translations
  ##============================================================##
  translates :name, :description, :warning
  globalize_accessors

  ##============================================================##
  ## Before & After
  ##============================================================##
  before_save :undraft, :unless => :new_record?
  after_save  :delete_cart_items

  def currency
    self.product.store.country.currency if self.product.present?
  end


  def primary_sale
    self.sales.where("start < ?",Time.now).where("end > ?",Time.now).where(:client_id =>nil,:code =>"").first
  end

  def is_a_checkout?
    !self.pricing_plan.present?
  end

  def is_a_plan?
    self.pricing_plan.present?
  end

  def is_a_service?
    return self.is_a_plan? || (self.is_a_checkout? && self.quantity.present? && self.quantity > 0)
  end

  private
  def undraft
    self.draft = 0
  end


  def delete_cart_items
    if self.status == "hidden"
      CartItem.where(:pricing_id => self.id).select {|it| it.order.status == "cart"}.each do |item|
        item.destroy
      end
    end
  end

end
