class Category < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :store
  has_many :subcategories, dependent: :destroy

  ##============================================================##
  ## Position
  ##============================================================##
  acts_as_list :column => :position, scope: :store



  #============================================================##
  # Translations
  #============================================================##
  translates :name
  globalize_accessors :locales => I18n.available_locales, attributes: [:name]




end
