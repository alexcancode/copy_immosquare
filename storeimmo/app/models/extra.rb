class Extra < ApplicationRecord

  attr_accessor :qte

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :pricing

  ##============================================================##
  ## Position
  ##============================================================##
  acts_as_list scope: [:pricing]

  ##============================================================##
  ## Money
  ##============================================================##
  monetize :price_cents,    with_model_currency: :currency
  monetize :price2_cents,   with_model_currency: :currency, :allow_nil => true


  def currency
    self.pricing.currency if self.pricing.present?
  end

  ##============================================================##
  ## Validations
  ##============================================================##
  validates_presence_of     :name


  def unable!
    self.update_attribute(:status, "hidden")
  end

  def is_active?
    return self.status == "active"
  end


  private


end
