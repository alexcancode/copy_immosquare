class CartItem < ApplicationRecord

  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :pricing_data, JSON
  serialize :variant_combination_data, JSON
  serialize :extras_id, JSON
  serialize :extras_data, JSON

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :order
  belongs_to :bill
  belongs_to :pricing
  belongs_to :variant_combination
  has_many :cart_item_sales, :dependent => :destroy

  ##============================================================##
  ## Money
  ##============================================================##
  monetize :total_price_cents, with_model_currency: :currency
  monetize :price_before_sale_cents, with_model_currency: :currency

  monetize :unit_price_cents, with_model_currency: :currency

  def unit_price_cents
    (self.quantity.present? && self.quantity > 1) ? (self.total_price_cents.to_f / self.quantity.to_f).to_d.truncate(1).to_f.round(0) : self.total_price_cents.to_f.to_d.truncate(1).to_f.round(0)
  end


  ##============================================================##
  ## This function checks if the item has a similar plan in the
  ## current cart or in the activated authorizations
  ##============================================================##
  def is_already_used?(client, cart)
    # if it's not a plan, ok
    return false if self.pricing.is_a_checkout?

    # return true  if (pricing.is_a_plan? && self.pricing.product_id == pricing.product_id)

    # check in cart if we already have an item of the same product (but not on the same item, of course ...)
    if cart.present?
      cart.cart_items.each do |item|
        return true if self.pricing.product_id == item.pricing.product_id && item.pricing.is_a_plan? && self.id != item.id
      end
    end

    # If the user is connected, we check in his activated authorizations (with type plan)
    if client.present?
      auths = Authorization.where(:client_id => client.id, :status => "active").where.not(:mode => ["tokens", "purchase"])
      # And we check if there is a similar plan in them
      auths.each do |a|
        return true if self.pricing.product_id == a.pricing.product_id
      end
    end
    return false
  end



end
