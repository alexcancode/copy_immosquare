class Order < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :client
  has_many :cart_items,   :dependent => :destroy
  has_many :order_sales,  :dependent => :destroy

  has_many :bills, :as => :origin

  # this is used on the infos page for a non-connected user
  attr_accessor :email

  ##============================================================##
  ## Money
  ##============================================================##
  monetize :subtotal_cents,                   with_model_currency: :currency
  monetize :shipping_cents,                   with_model_currency: :currency
  monetize :tax1_cents,                       with_model_currency: :currency
  monetize :tax2_cents,                       with_model_currency: :currency
  monetize :total_cents,                      with_model_currency: :currency
  monetize :shipping_unit_price_cents,        with_model_currency: :currency

  ##============================================================##
  ## Util
  ##============================================================##
  def will_be_shipped?
    self.cart_items.each do |item|
      return true if item.pricing.shippable == 1
    end
    return false
  end

  def is_empty?
    return self.cart_items.length == 0
  end

  def clear
    self.cart_items.destroy_all
    self.order_sales.destroy_all
  end

  def multiple_plans_on_cart?
    products_ids = []
    self.cart_items.each do |item|
      products_ids << item.pricing.product_id if item.pricing.is_a_plan?
    end
    return products_ids.uniq.length != products_ids.length
  end

  def weight
    weight = 0
    self.cart_items.each do |item|
      weight += item.quantity * item.pricing.weight if item.pricing.shippable == 1 && item.pricing.weight.present?
    end
    return weight
  end

  ##============================================================##
  ## Prices
  ##============================================================##
  def update_subtotal
    price = 0
    self.cart_items.each do |item|
      price += item.total_price_cents
    end
    self.update_attribute(:subtotal_cents, price)
  end

  def calculate_shipping
    unless self.will_be_shipped?
      self.update_attribute(:shipping_cents, 0)
      return 0
    end

    weight = self.weight
    price = 0

    # find shipping and calculate price
    if self.shipping_id.present?
      country = Country.where(:code => self.shipping_address_country).first
      if country.present?
        # get sĥipping for current country, with corresponding mode and in the right weight range
        ship = Shipping.where(:shipping_mode_id => Shipping.find(self.shipping_id).shipping_mode_id, :country => country).where("max_weight >= ?", weight).order(:max_weight => :asc).first
      end
      price = ship.present? ? ship.price_cents : 0
    end

    # update database
    if ship.present?
      self.update_attributes(:shipping_cents => price, :shipping_id => ship.id, :shipping_name => ship.shipping_mode.name, :shipping_unit_price_cents => ship.price_cents)
    else
      self.update_attribute(:shipping_cents, price)
    end
    return 0
  end

  def calculate_taxes(tax)
    if tax.present?
      base = tax.store.calculate_taxes_with_shipping ? (self.subtotal_cents + self.shipping_cents) : subtotal_cents
      self.update_attributes(
        :tax1_name    => tax.tax1_name,
        :tax1_value   => tax.tax1_value,
        :tax1_number  => tax.tax1_number,
        :tax1_cents   => (tax.tax1_value.present? && tax.tax1_value > 0) ? Tax.calculate_tax(base, tax.tax1_value) : 0,
        :tax2_name    => tax.tax2_name,
        :tax2_value   => tax.tax2_value,
        :tax2_number  => tax.tax2_number,
        :tax2_cents   => (tax.tax2_value.present? && tax.tax2_value > 0) ? Tax.calculate_tax(base, tax.tax2_value) : 0,
        )
    end
  end

  def calculate_total
    total = self.subtotal_cents + self.tax1_cents + self.tax2_cents + self.shipping_cents
    # self.order_sales.each do |order_sale|
    #   total -= order_sale.amount_cents
    # end
    self.update_attribute(:total_cents, total)
  end

end
