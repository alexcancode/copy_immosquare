class Subcategory < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :category
  has_many :products, :dependent => :nullify


  ##============================================================##
  ## Translations
  ##============================================================##
  translates :name
  globalize_accessors :locales => I18n.available_locales, attributes: [:name]



end
