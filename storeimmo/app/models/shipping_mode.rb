class ShippingMode < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :store
  has_many :shippings, dependent: :destroy
  translates :name
  globalize_accessors :locales => I18n.available_locales, attributes: [:name]
end
