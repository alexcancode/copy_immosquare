class AuthorizationsExtra < ApplicationRecord
  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :authorization
  belongs_to :extra
end
