class User < ActiveRecord::Base


  ##============================================================##
  ## Associations
  ##============================================================##
  rolify
  belongs_to :company



  ##============================================================##
  ## Devise
  # Include default devise modules. Others available are:confirmable, :lockable, :timeoutable and :omniauthable
  ##============================================================##
  devise :database_authenticatable,:recoverable, :rememberable, :trackable, :validatable

end
