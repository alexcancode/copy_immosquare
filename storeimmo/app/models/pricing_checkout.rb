class PricingCheckout < ApplicationRecord

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :pricing

  ##============================================================##
  ## Money
  ##============================================================##
  monetize :price_cents, with_model_currency: :currency


  def currency
    self.pricing.currency   if self.pricing.present?
  end




end
