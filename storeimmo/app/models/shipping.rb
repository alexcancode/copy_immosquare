class Shipping < ApplicationRecord


  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :country
  belongs_to :shipping_mode

  ##============================================================##
  ## Callbacks
  ##============================================================##


  ##============================================================##
  ## Money
  ##============================================================##
  monetize :price_cents, :allow_nil => true , :with_model_currency => :currency


  def currency
    self.shipping_mode.store.country.currency if(self.shipping_mode.present? && self.shipping_mode.store.present?)
  end



end
