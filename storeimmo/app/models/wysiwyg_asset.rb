class WysiwygAsset < ApplicationRecord
  belongs_to :imageable, polymorphic: true

  ##============================================================##
  ## Paperclip Interpolates
  ##============================================================##
  Paperclip.interpolates('wysiwyg_pictures_type') do |attachment, style|
    attachment.instance.imageable_type.parameterize
  end

  Paperclip.interpolates('wysiwyg_pictures_id') do |attachment, style|
    attachment.instance.imageable_id.to_s.parameterize
  end

  #============================================================##
  ## Paperclip : Profil Picture
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/wysiwyg_pictures/:wysiwyg_pictures_type/:wysiwyg_pictures_id/:id_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :large    => ["600x600", :png]
  },
  :convert_options => {
    :large    => "-quality 75 -strip",
  },
  :default_url => "https://placehold.it/450x450"

  validates_attachment_content_type :picture, :content_type => /\Aimage/
  validates_attachment_size :picture, :less_than=>2.megabytes

  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_picture_post_process :rename_picture


  ##============================================================##
  ## Cette fonction renome les avatars avant de les uploaders
  ##============================================================##
  def rename_picture
    extension = File.extname(picture_file_name).downcase
    self.picture.instance_write :file_name, "picture_#{Time.now.to_i.to_s}#{extension}"
  end
end
