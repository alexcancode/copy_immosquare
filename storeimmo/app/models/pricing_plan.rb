class PricingPlan < ApplicationRecord

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :pricing


  ##============================================================##
  ## Money
  ##============================================================##
  monetize :price_month_cents,  with_model_currency: :currency
  monetize :price_year_cents,   with_model_currency: :currency

  def currency
    self.pricing.product.store.country.currency  if (self.pricing.present? && self.pricing.product.present?)
  end


end
