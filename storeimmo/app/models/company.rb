class Company < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :users
  has_many :products
  has_many :authorizations
  belongs_to :store
  has_many :pricings, :through => :products

  ##============================================================##
  ## indicates the need of regenerating the Api keys
  ##============================================================##
  attr_accessor :regenerate
end
