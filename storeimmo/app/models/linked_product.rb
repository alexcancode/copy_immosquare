class LinkedProduct < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :product, :class_name => "Product"
  belongs_to :linked_product, :class_name => "Product"

end
