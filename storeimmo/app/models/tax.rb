class Tax < ApplicationRecord

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :store

  def self.calculate_tax(base, tax)
    return (base * (tax / 100.0)).to_d.truncate(1).to_f.round(0)
  end

end
