class Authorization < ApplicationRecord

  ##============================================================##
  ## Serialize
  ##============================================================##
  serialize :history, JSON
  serialize :item_data, JSON

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :client
  belongs_to :pricing
  belongs_to :company
  belongs_to :first_bill, :class_name => "Bill", :foreign_key => "first_bill_id"
  has_many :authorizations_extras
  has_many :extras, :through => :authorizations_extras
  has_many :bills, :as => :origin

  def is_active?
    return self.status == "active"
  end


  def next_payment
    case self.mode
    when "plan_month"
      next_date = DateTime.new(DateTime.now.year, DateTime.now.month, self.purchase_date.day).to_date
      return next_date > DateTime.now.to_date ? next_date : next_date + 1.month
    when "plan_year"
      next_date = DateTime.new(DateTime.now.year, self.purchase_date.month, self.purchase_date.day).to_date
      return next_date > DateTime.now.to_date ? next_date : next_date + 1.year
    else
      return nil
    end
  end

  def add_to_history(action, date, tokens)
    history = self.history
    history << {
      :action => action,
      :date => date,
      :tokens => tokens
    }
    self.update_attribute(:history, history)
  end

  def debit_token(quantity)
    self.update_attribute(:tokens, self.tokens - quantity.to_i)
  end

end
