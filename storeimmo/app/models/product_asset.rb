class ProductAsset < ApplicationRecord

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :product


  ##============================================================##
  ## Position
  ##============================================================##
  acts_as_list scope: :product

  ##============================================================##
  ## Paperclip Interpolates
  ##============================================================##
  Paperclip.interpolates('product_id') do |attachment, style|
    attachment.instance.product_id
  end

  #============================================================##
  # Paperclip : product logo
  #============================================================##
  has_attached_file :image,
  :path => "#{Rails.env}/products/:product_id/assets/image_:id_:style.:extension",
  :hash_secret => "some_secret",
  :styles   => {
    :small  => ["150x150", :jpg],
    :medium => ["800x500", :jpg],
    :large  => ["1920x1080", :jpg]
    },
    :convert_options => {
      :small  => "-quality 70 -strip",
      :medium => "-quality 85 -strip",
      :large  => "-quality 85 -strip"
      },
      :default_url => "https://placehold.it/1920x1080"
      validates_attachment_content_type :image, :content_type => /\Aimage/
      validates_attachment_size :image, :less_than=>2.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_image


  def rename_image
    extension = File.extname(image_file_name).downcase
    self.image.instance_write :file_name, "product_image_#{Time.now.to_i.to_s}#{extension}"
  end
end
