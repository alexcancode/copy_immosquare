class PdfBillGenerator
  include Sidekiq::Worker

  require "mypdflib"


  ##============================================================##
  ## This function generates a pdf from a bill and upload it to AWS.
  ## If send_after_generated is set to true, the bill is also sent to the client by email.
  ##============================================================##
  def perform(bill_id, send_after_generated = false)
    begin
      ##============================================================##
      ## Step #1 : Create PDF
      ##============================================================##
      JulesLogger.info "start bill generation #{bill_id}"

      bill        = Bill.find(bill_id)
      store       = bill.client.store
      my_pdf_path = "#{Rails.root}/app/pdfs/result/result#{bill.id}.pdf"
      my_pdf_path2 = "#{Rails.root}/app/pdfs/result/result#{bill.id}_2.pdf"
      my_txt_path = "#{Rails.root}/app/pdfs/result/result#{bill.id}.txt"
      
      dictionary  = Mypdflib::PdfParser.new(:pdf => "#{Rails.root}/app/pdfs/template_facture.pdf").parse
      dictionary  = DictionaryPopulater.new(
        :dictionary   => dictionary,
        :bill         => bill,
        :store        => store).populate_dictionary
      pdf         = Mypdflib::PdfWritter.new(
        :original   => "#{Rails.root}/app/pdfs/template_facture.pdf",
        :result     => my_pdf_path,
        :dictionary => dictionary).produce


      ##============================================================##
      ## Step #2 : Create table
      ##============================================================##

      # Header
      headers = [
        {:content => I18n.t("billing.tab.description"), :alignment => "left", :size => 410},
        {:content => I18n.t("billing.tab.quantity"), :alignment => "right", :size => 100},
        {:content => I18n.t("billing.tab.price"), :alignment => "right", :size => 100},
        {:content => I18n.t("billing.tab.total"), :alignment => "right", :size => 100}
      ]

      # body
      items = []
      items << [
        {:content => "", :type => "flow", :alignment => "left", :size => 410},
        {:content => "", :type => "string", :alignment => "right", :size => 100},
        {:content => "", :type => "string", :alignment => "right", :size => 100},
        {:content => "", :type => "string", :alignment => "right", :size => 100}
      ]
      bill.items_data.each do |item|
        item = item.deep_symbolize_keys
        items << [
          {:content => "#{item[:product_name]} - #{item[:pricing_name]} #{item[:sales].present? ? "\n#{I18n.t("back_office.product.sale.on_sale")}" : nil}", :type => "flow", :alignment => "left", :size => 410},
          {:content => item[:quantity].to_s, :type => "string", :alignment => "right", :size => 100},
          {:content => item[:unit_price_formatted], :type => "string", :alignment => "right", :size => 100},
          {:content => item[:total_price_formatted], :type => "string", :alignment => "right", :size => 100}
        ]
      end

      # footer
      footers = []
      footers << [I18n.t("billing.subtotal_title").upcase, ActionController::Base.helpers.humanized_money_with_symbol(bill.subtotal)]
      
      if store.calculate_taxes_with_shipping
        footers << [I18n.t("billing.shipping_title").upcase, ActionController::Base.helpers.humanized_money_with_symbol(bill.shipping)] if bill.shipping_cents.present? && bill.shipping_cents > 0
      else
        footers << [bill.tax1_name.upcase, ActionController::Base.helpers.humanized_money_with_symbol(bill.tax1)] if bill.tax1_name.present?
      end

      footers << [bill.tax2_name.upcase, ActionController::Base.helpers.humanized_money_with_symbol(bill.tax2)] if bill.tax2_name.present? && bill.tax2_cents.present? && bill.tax2_cents > 0

      if store.calculate_taxes_with_shipping
        footers << [bill.tax1_name.upcase, ActionController::Base.helpers.humanized_money_with_symbol(bill.tax1)] if bill.tax1_name.present?
      else
        footers << [I18n.t("billing.shipping_title").upcase, ActionController::Base.helpers.humanized_money_with_symbol(bill.shipping)] if bill.shipping_cents.present? && bill.shipping_cents > 0
      end

      footers << [I18n.t("billing.grandtotal_title").upcase, ActionController::Base.helpers.humanized_money_with_symbol(bill.total_price)]

      # call pdflib
      JulesLogger.info "start bill invoice #{bill_id}"
      pdf         = Mypdflib::PdfInvoice.new(
        :original   => my_pdf_path,
        :result     => my_pdf_path2,
        :items => items,
        :headers => headers,
        :footers => footers,
        :regular_font => "Raleway-Light",
        :bold_font => "Raleway-Regular",
        :closing_text => I18n.t("billing.thanks_title")).produce
      JulesLogger.info "end bill invoice #{bill_id}"

      ##============================================================##
      ## Step #3 : Upload to S3
      ##============================================================##
      JulesLogger.info "Upload bill #{bill_id} to S3"

      s3          = Aws::S3::Resource.new
      bucket      = s3.bucket(ENV['aws_bucket'])
      bucket      = s3.create_bucket(ENV['aws_bucket']) unless bucket.exists?
      obj         = bucket.object("#{Rails.env}/bills/bill_#{bill.id}.pdf")
      obj.upload_file(my_pdf_path2)

      ##============================================================##
      ## Step #4 : Send
      ##============================================================##
      if send_after_generated
        NotificationMailer.send_bill(bill, store, my_pdf_path2).deliver_now
        JulesLogger.info "send bill via email #{bill_id}"
      end

      ##============================================================##
      ## Step #5 Clean
      ##============================================================##
      File.delete(my_pdf_path2) if File.exist?(my_pdf_path2) && obj.exists?
      File.delete(my_pdf_path) if File.exist?(my_pdf_path) && obj.exists?
      File.delete(my_txt_path) if File.exist?(my_txt_path) && obj.exists?
      JulesLogger.info "end bill generation #{bill_id}"

    rescue Exception => e
      JulesLogger.info e
    end

  end

end