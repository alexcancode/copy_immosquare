class TranslateManager


  def initialize(args)
    begin
      @file = args[:file]
    rescue Exception => e
      JulesLogger.info e.message
    end
  end



  def init_db
    begin
      raise "No file found" if !File.exist?(@file)
      ActiveRecord::Base.connection.execute("TRUNCATE translations")
      yml = YAML.load_file(File.open(@file))
      yml.keys.each do |locale|
        deep_parser(locale,yml[locale])
      end if yml.is_a?(Hash)
    rescue Exception => e
      JulesLogger.info e.message
    end
  end



  def deep_parser(locale,hash,key=nil)
    if hash.is_a?(Hash)
      hash.keys.each do |k|
        deep_parser(locale, hash[k],"#{key}#{key.present? ? "." : nil }#{k}")
      end
    else
      t = Translation.where(:key => key).first_or_create
      t[locale.to_sym] = hash
      t.save
    end
  end




end
