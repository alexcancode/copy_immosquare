namespace :authorization do

  task :update_status => :environment do
    now = DateTime.now.to_date
    authorizations = Authorization.where(:status => "active")

    authorizations.each do |a|
      a.update_attributes(:status => "inactive", :inactive_date => now) if (a.limit_date.present? && now > a.limit_date.to_date)
    end
  end

end