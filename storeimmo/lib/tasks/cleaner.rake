namespace :cleaner do

  task :eraseCarts => :environment do
    @session_carts = Order.where(:status => "cart").where(:client_id => nil).where("updated_at < ?", DateTime.now - (ENV["delete_session_carts_older_than_hours"].to_f/24.0))
    @real_carts = Order.where(:status => "cart").where.not(:client_id => nil).where("updated_at < ?", DateTime.now - (ENV["delete_client_carts_older_than_hours"].to_f/24.0))

    @session_carts.destroy_all
    @real_carts.destroy_all
  end


  task :eraseTmpImages => :environment do
    entries = []
    entries = Dir.entries("#{Rails.root}/tmp/bill_images") - [".", ".."] if Dir.exists?("#{Rails.root}/tmp/bill_images")
    entries.each do |f|
      File.delete("#{Rails.root}/tmp/bill_images/#{f}") if File.exist?("#{Rails.root}/tmp/bill_images/#{f}")
    end
  end


  task :reset_bill => :environment do
    ActiveRecord::Base.connection.execute("TRUNCATE authorizations")
    ActiveRecord::Base.connection.execute("TRUNCATE authorizations_extras")
    ActiveRecord::Base.connection.execute("TRUNCATE bills")
    ActiveRecord::Base.connection.execute("TRUNCATE cart_item_sales")
    ActiveRecord::Base.connection.execute("TRUNCATE cart_items")
    ActiveRecord::Base.connection.execute("TRUNCATE order_sales")
    ActiveRecord::Base.connection.execute("TRUNCATE orders")
  end

  task :reset => :environment do
    ##============================================================##
    ## Reset auto_increment
    ##============================================================##
    ActiveRecord::Base.connection.execute("TRUNCATE authorizations")
    ActiveRecord::Base.connection.execute("TRUNCATE authorizations_extras")
    ActiveRecord::Base.connection.execute("TRUNCATE bills")
    ActiveRecord::Base.connection.execute("TRUNCATE cart_item_sales")
    ActiveRecord::Base.connection.execute("TRUNCATE cart_items")
    ActiveRecord::Base.connection.execute("TRUNCATE clients")
    ActiveRecord::Base.connection.execute("TRUNCATE order_sales")
    ActiveRecord::Base.connection.execute("TRUNCATE orders")
    ActiveRecord::Base.connection.execute("TRUNCATE profiles")
    ActiveRecord::Base.connection.execute("TRUNCATE profiles")
    ActiveRecord::Base.connection.execute("TRUNCATE team_members")
    ActiveRecord::Base.connection.execute("TRUNCATE teams")
    ActiveRecord::Base.connection.execute("TRUNCATE oauth_access_grants")
    ActiveRecord::Base.connection.execute("TRUNCATE oauth_access_tokens")
  end



end
