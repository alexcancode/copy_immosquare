require 'ansi/progressbar'


namespace :publipostage do

  task :import_zipcode => :environment do

    filename   = "#{Rails.root}/tmp/import/HouseElite.txt"
    line_count = `wc -l #{filename}`.to_i
    puts "#{line_count} lines in file"

    ##============================================================##
    ## Delete all reccords + put autoindent to 1
    ##============================================================##
    ActiveRecord::Base.connection.execute("TRUNCATE post_canada_publipostages")

    pbar   = ANSI::Progressbar.new("Parsing Sectors", line_count) rescue nil
    pbar.__send__ :show if pbar

    File.open(filename, "r").each_line do |line|
      postCanadaZip = PostCanadaPublipostage.new
      postCanadaZip.sector_zipcode        = line[file_position(1,6)].strip()
      postCanadaZip.sector_name           = line[file_position(7,30)].strip()
      postCanadaZip.sector_type           = line[file_position(37,5)].strip()
      postCanadaZip.sector_qualification  = line[file_position(42,15)].strip()
      postCanadaZip.sector_short_name     = line[file_position(57,15)].strip()
      postCanadaZip.sector_state_code     = line[file_position(72,1)].strip()

      postCanadaZip.shipping_mode_type  = line[file_position(73,2)].strip()
      postCanadaZip.shipping_mode_id    = line[file_position(75,4)].strip()
      postCanadaZip.shipping_road_id    = "#{postCanadaZip.shipping_mode_type}#{postCanadaZip.shipping_mode_id.rjust(4, '0')}"


      postCanadaZip.spot_appartments  = line[file_position(79,5)].strip()
      postCanadaZip.spot_shops        = line[file_position(84,5)].strip()
      postCanadaZip.spot_domiciles    = line[file_position(89,5)].strip()
      postCanadaZip.spot_farms        = line[file_position(94,5)].strip()

      postCanadaZip.consumer_choice_appartments = line[file_position(99,5)].strip()
      postCanadaZip.consumer_choice_shops       = line[file_position(104,5)].strip()
      postCanadaZip.consumer_choice_domiciles   = line[file_position(109,5)].strip()
      postCanadaZip.consumer_choice_farms       = line[file_position(114,5)].strip()

      postCanadaZip.zipcode                             = line[file_position(119,6)].strip()
      postCanadaZip.zipcode_udl_type                    = line[file_position(125,2)].strip()
      postCanadaZip.zipcode_rta                         = line[file_position(267,3)].strip()
      postCanadaZip.itinerary_sollicitation_indicator   = line[file_position(270,2)].strip()


      postCanadaZip.supervisor_zipcode                = line[file_position(127,6)].strip()
      postCanadaZip.supervisor_name                   = line[file_position(133,30)].strip()
      postCanadaZip.supervisor_shipping_name          = line[file_position(163,30)].strip()
      postCanadaZip.supervisor_type                   = line[file_position(193,5)].strip()
      postCanadaZip.supervisor_qualification          = line[file_position(198,15)].strip()
      postCanadaZip.supervisor_street_number          = line[file_position(213,6)].strip()
      postCanadaZip.supervisor_street_number_suffix   = line[file_position(219,1)].strip()
      postCanadaZip.supervisor_unit_number            = line[file_position(220,6)].strip()
      postCanadaZip.supervisor_street_name            = line[file_position(226,30)].strip()
      postCanadaZip.supervisor_street_type            = line[file_position(256,6)].strip()
      postCanadaZip.supervisor_cardinal_point         = line[file_position(262,2)].strip()
      postCanadaZip.supervisor_province               = line[file_position(264,2)].strip()
      postCanadaZip.supervisor_state_code             = line[file_position(266,1)].strip()

      postCanadaZip.save
      pbar.inc(1)
    end

    pbar.finish if pbar
  end

  def file_position(start,lenght)
    (start-1)..((start-1)+(lenght-1))
  end

end
