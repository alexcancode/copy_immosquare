namespace :translator do


  task :init_db => :environment do
    TranslateManager.new(:file => "#{Rails.root}/config/locales/app.fr.yml").init_db
  end


end
