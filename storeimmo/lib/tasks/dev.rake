namespace :dev do

# This task updates all currencies on Remax to cad
  task :update_currency => :environment do
    Product.all.select {|p| p.stores.pluck(:id).include?(5) || p.visible_everywhere == 1}.each do |p|
      p.pricings.each do |pricing|
        if pricing.is_a_plan?
          pp = pricing.pricing_plan
          # puts pp.id
          pp.update_attributes(:currency => "cad")
        else
          pricing.pricing_checkouts.each do |pc|
            # puts pc.id
            pc.update_attributes(:price_currency => "cad")
          end
        end
      end
    end
  end

end
