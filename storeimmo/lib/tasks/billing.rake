namespace :billing do

  task :recurrence => :environment do
    now = DateTime.now
    now_date = now.to_date
    authorizations = Authorization.where(:mode => ["plan_month", "plan_year"]).where("CAST(next_bill_date AS DATE) = ?", [now_date])

    authorizations.each do |a|
      if a.limit_date.blank? || now < a.limit_date.to_date
        # make payment only if the limit date is later than today (if it's today, don't make payment either)
        
        Stripe.api_key    = a.client.store.payment_api_token

        customer = Stripe::Customer.retrieve(a.client.stripe_customer_id)
        tax = a.client.store.tax

        tax1_cents = (tax.tax1_value.present? && tax.tax1_value > 0) ? Tax.calculate_tax(a.item_data["total_price_cents"], tax.tax1_value) : 0
        tax2_cents = (tax.tax2_value.present? && tax.tax2_value > 0) ? Tax.calculate_tax(a.item_data["total_price_cents"], tax.tax2_value) : 0

        bill = Bill.create(
          :items_data => [a.item_data],
          :date => now,
          :stripe_card_id => customer.default_source,
          :currency => a.currency,
          :client_id => a.client_id,
          :shipping_address_country => nil,
          :subtotal_cents => a.item_data["total_price_cents"],
          :total_price_cents => a.item_data["total_price_cents"] + tax1_cents + tax2_cents,
          :tax1_name => tax.tax1_name,
          :tax1_cents => tax1_cents,
          :tax2_name => tax.tax2_name,
          :tax2_cents => tax2_cents,
          :shipping_id => nil,
          :shipping_unit_price_cents => 0,
          :shipping_cents => 0,
          :origin_type => "Authorization",
          :origin_id => a.id
          )

        charge = Stripe::Charge.create(
          :amount       => bill.total_price_cents,
          :currency     => :cad, #TODO  
          :source       => customer.default_source,
          :customer     => customer.id,
          :description  => "Pay bill ##{bill.id}"
          )

        bill.update_attributes(:payment_data => charge)

        a.add_to_history("recurrence", now, nil)
        a.update_attributes(:last_bill_date => now, :next_bill_date => a.mode == "plan_month" ? a.next_bill_date + 1.month : a.next_bill_date + 12.month)

        PdfBillGenerator.perform_async(bill.id, true)

      end
    end
  end

  task :regenerate => :environment do
    puts "starting regeneration"
    Bill.all.each do |bill|
      puts "regenerate bill ##{bill.id}"
      PdfBillGenerator.new.perform(bill.id)
    end
    puts "regeneration over"
  end

end