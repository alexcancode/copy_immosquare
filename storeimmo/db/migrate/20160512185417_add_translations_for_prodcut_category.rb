class AddTranslationsForProdcutCategory < ActiveRecord::Migration[5.0]

  def self.up
    ProductCategory.create_translation_table!({
      :name => :string,
    }, {
      :migrate_data => false
    })
  end

  def self.down
    ProductCategory.drop_translation_table! :migrate_data => false
  end


end



