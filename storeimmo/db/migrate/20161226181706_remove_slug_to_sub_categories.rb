class RemoveSlugToSubCategories < ActiveRecord::Migration[5.0]
  def change
    remove_column :subcategories, :slug, :string
  end
end
