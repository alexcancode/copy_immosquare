class AddPriceToVariantCombination < ActiveRecord::Migration[5.0]
  def change
    add_column :variant_combinations, :price_percentage, :float
    rename_column :variant_combinations, :SKU, :sku
  end
end
