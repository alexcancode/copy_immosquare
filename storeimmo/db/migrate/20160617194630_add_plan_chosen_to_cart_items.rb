class AddPlanChosenToCartItems < ActiveRecord::Migration[5.0]
  def change
    add_column :cart_items, :chosen_plan, :string, :after => :pricing_data
  end
end
