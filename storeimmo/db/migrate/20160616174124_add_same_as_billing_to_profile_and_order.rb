class AddSameAsBillingToProfileAndOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :shipping_same_as_billing, :integer, :default => 0, :after => :shipping_address_zipcode
    add_column :orders, :shipping_same_as_billing, :integer, :default => 0, :after => :shipping_address_zipcode

  end
end
