class RemoveCurrencyToPricingCheckouts < ActiveRecord::Migration[5.0]
  def change
    remove_column :pricing_checkouts, :price_currency, :string
  end
end
