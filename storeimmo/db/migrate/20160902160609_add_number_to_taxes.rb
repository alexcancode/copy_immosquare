class AddNumberToTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :taxes, :tax1_number, :string, :after => :tax1_name
    add_column :taxes, :tax2_number, :string, :after => :tax2_name
  end
end
