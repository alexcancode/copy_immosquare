class AddIsReadyToPricings < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :is_ready, :integer, :default => 1, :after => :position
  end
end
