class AddBaselineToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :baseline, :string
  end
end
