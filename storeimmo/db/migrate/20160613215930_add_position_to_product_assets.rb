class AddPositionToProductAssets < ActiveRecord::Migration[5.0]
  def change
    add_column :product_assets, :position, :integer, :after => :product_id
  end
end
