class UpdateCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :slug, :string, :after => :name
    add_column :companies, :link, :string, :after => :slug
    add_column :companies, :api_key, :string, :after => :link
    add_column :companies, :api_token, :string, :after => :api_key

    rename_column :products, :supplier_id, :company_id

    remove_column :pricings, :service_id
    drop_table :services

    remove_column :bills, :sales_data, :text
  end
end
