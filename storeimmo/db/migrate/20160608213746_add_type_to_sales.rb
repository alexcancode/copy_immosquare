class AddTypeToSales < ActiveRecord::Migration[5.0]
  def change
    add_column :sales, :sale_type, :string, :default => "percentage"
  end
end
