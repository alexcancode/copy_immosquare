class AddProductSkuToAuthorizations < ActiveRecord::Migration[5.0]
  def change
    add_column :authorizations, :product_sku, :string, :after => :status
  end
end
