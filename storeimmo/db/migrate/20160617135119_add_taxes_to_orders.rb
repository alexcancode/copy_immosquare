class AddTaxesToOrders < ActiveRecord::Migration[5.0]
  def change
    remove_money :orders, :tax, currency: { present: false }
    add_column :orders, :tax1_name, :string
    add_money :orders, :tax1, currency: { present: false }
    add_column :orders, :tax2_name, :string
    add_money :orders, :tax2, currency: { present: false }
  end
end
