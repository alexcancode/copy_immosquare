class DeleteLinkToCompanies < ActiveRecord::Migration[5.0]
  def change
    remove_column :companies, :link, :string
  end
end
