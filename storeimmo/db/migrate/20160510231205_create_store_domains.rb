class CreateStoreDomains < ActiveRecord::Migration[5.0]
  def change
    create_table :store_domains do |t|
      t.string :domain
      t.references :store, foreign_key: true
      t.timestamps
    end
  end
end
