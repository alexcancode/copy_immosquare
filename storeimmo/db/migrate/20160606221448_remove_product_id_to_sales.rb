class RemoveProductIdToSales < ActiveRecord::Migration[5.0]
  def change
    remove_reference :sales, :product, foreign_key: false
  end
end
