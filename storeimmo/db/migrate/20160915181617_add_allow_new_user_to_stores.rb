class AddAllowNewUserToStores < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :allow_user_creation, :integer, :default => 0
  end
end
