class ManageProductVisibility < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :visible_everywhere, :integer, :after => :draft, :default => 0

    drop_table :product_user_stores
    
    create_table :product_user_stores do |t|
      # it's the visibility of a product
      t.references :product, index: true
      t.references :client, index: true
      t.references :store, index: true
    end
  end
end
