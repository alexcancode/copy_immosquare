class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.references :client, foreign_key: true
      t.string :name
      t.string :company_name
      t.string :email
      t.string :phone
      t.timestamps
    end
  end
end
