class AddStoreIdToBills < ActiveRecord::Migration[5.0]
  def change
    add_reference :bills, :store, foreign_key: false, :after => :client_id
    add_reference :bills, :product, foreign_key: false, :after => :store_id
  end
end
