class RemoveDomainsToStores < ActiveRecord::Migration[5.0]
  def change
    remove_column :stores, :domains, :string
  end
end
