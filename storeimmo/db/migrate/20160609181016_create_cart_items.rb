class CreateCartItems < ActiveRecord::Migration[5.0]
  def change
    create_table :cart_items do |t|
      t.references :order, foreign_key: false, index:true
      t.references :pricing, foreign_key: false, index:true
      t.text :pricing_data, :limit => 4294967295
      t.integer :quantity
      t.references :variant_combination, foreign_key: false, index:true
      t.text :variant_combination_data, :limit => 4294967295
      t.references :sale, foreign_key: false, index:true
      t.text :sale_data, :limit => 4294967295
      t.text :extras_id
      t.text :extras_data, :limit => 4294967295

      t.timestamps
    end
  end
end
