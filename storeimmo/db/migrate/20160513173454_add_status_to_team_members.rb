class AddStatusToTeamMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :team_members, :status, :integer, :default => 0, :after => :member_role
  end
end
