class AddDescritpionAndShippableToPricings < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :description, :text, :after => :name
    add_column :pricings, :shippable, :integer, :after => :price_from
    add_column :pricings, :draft, :integer, :after => :checkout_unit_id, :default => 0
  end
end
