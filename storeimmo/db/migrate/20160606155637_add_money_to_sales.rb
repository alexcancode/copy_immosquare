class AddMoneyToSales < ActiveRecord::Migration[5.0]
  def change
    remove_column :sales, :amount
    add_money :sales, :amount
  end
end
