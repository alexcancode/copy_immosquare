class AddOauthLinkToCompagnies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :oauth_link, :string, :after => :link
  end
end
