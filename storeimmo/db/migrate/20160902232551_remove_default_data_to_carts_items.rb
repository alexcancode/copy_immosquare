class RemoveDefaultDataToCartsItems < ActiveRecord::Migration[5.0]
  def change
    change_column :cart_items, :currency, :string, :default => :nil
  end
end
