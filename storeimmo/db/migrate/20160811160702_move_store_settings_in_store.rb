class MoveStoreSettingsInStore < ActiveRecord::Migration[5.0]
  def change
    remove_column :stores, :store_setting_id, :integer
    remove_column :stores, :slug, :string

    add_column :stores, :currency, :string, :after => :baseline
    add_column :stores, :address, :text, :after => :currency

    add_column :stores, :tax_id, :integer, :after => :address
    add_column :stores, :tax1_number, :string, :after => :tax_id
    add_column :stores, :tax2_number, :string, :after => :tax1_number

    add_column :stores, :company_registration_number, :string, :after => :tax2_number
    add_column :stores, :contact_address, :string, :after => :company_registration_number
    add_column :stores, :contact_email, :string, :after => :contact_address
    add_column :stores, :contact_phone, :string, :after => :contact_email
    add_column :stores, :languages, :text, :after => :contact_phone

    change_table :stores do |t|
      t.attachment :logo
    end

    drop_table :store_settings
  end
end
