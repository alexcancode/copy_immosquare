class CreateProductUser < ActiveRecord::Migration[5.0]
  def change
    create_table :product_user_stores, :id => false do |t|
      # it's the visibility of a product
      t.references :product, index: true
      t.references :client, index: true
      t.references :store, index: true
    end
  end
end
