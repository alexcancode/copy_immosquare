class AddPositioNToPricings < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :position, :integer, :after => :draft
  end
end
