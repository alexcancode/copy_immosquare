class RemoveSlugToCategories < ActiveRecord::Migration[5.0]
  def change
    remove_column :categories, :slug, :string
    add_column :categories, :position, :integer, :after => :store_id
  end
end
