class RemoveCurrencyToOrders < ActiveRecord::Migration[5.0]
  def change
    remove_column :orders, :currency, :string
    add_column :orders, :currency, :string, :after => :subtotal_cents
  end
end
