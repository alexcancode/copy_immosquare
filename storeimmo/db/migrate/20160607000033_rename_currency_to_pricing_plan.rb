class RenameCurrencyToPricingPlan < ActiveRecord::Migration[5.0]
  def change
    rename_column :pricing_plans, :price_month_currency, :currency
  end
end
