class AddPriceBeforeSaleToCartItem < ActiveRecord::Migration[5.0]
  def change
    add_money :cart_items, :price_before_sale, currency: { present: false }

    add_money :order_sales, :amount
  end
end
