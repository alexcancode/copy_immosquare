class ChangeCheckouts < ActiveRecord::Migration[5.0]
  def change
    add_column :checkout_types, :slug, :string, :after => :id

    add_column :checkout_units, :slug, :string, :after => :id

    rename_table :assets, :product_assets
  end
end
