class UpdateBillManagement < ActiveRecord::Migration[5.0]
  def change
    remove_column :cart_items,  :bill_id, :integer

    rename_column :bills, :order_id, :authorization_id
    add_column :bills, :client_data, :text, :limit => 4294967295, :after => :stripe_card_id
    add_column :bills, :items_data, :text, :limit => 4294967295, :after => :stripe_card_id

    add_column :authorizations, :last_bill_id, :integer, :after => :pricing_id
    add_column :authorizations, :last_bill_date, :datetime, :after => :last_bill_id
    add_column :authorizations, :next_bill_date, :datetime, :after => :last_bill_date
    rename_column :authorizations, :price_currency, :currency
    add_column :authorizations, :price_before_sale_cents, :integer, :after => :price_cents
  end
end
