class UpdateStoreInfos < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :default_language, :string, :after => :languages
    add_column :stores, :payment_api_key, :string
    add_column :stores, :payment_api_token, :string
    add_column :stores, :payment_api_type, :string, :default => "stripe"
  end
end
