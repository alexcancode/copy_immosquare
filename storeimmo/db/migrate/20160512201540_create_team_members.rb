class CreateTeamMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :team_members do |t|
      t.references :team, foreign_key: true
      t.references :client, foreign_key: true
      t.integer :member_role, :default => 1
      t.timestamps
    end
  end
end
