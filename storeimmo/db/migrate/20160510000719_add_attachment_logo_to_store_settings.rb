class AddAttachmentLogoToStoreSettings < ActiveRecord::Migration
  def self.up
    change_table :store_settings do |t|
      t.attachment :logo
    end
  end

  def self.down
    remove_attachment :store_settings, :logo
  end
end
