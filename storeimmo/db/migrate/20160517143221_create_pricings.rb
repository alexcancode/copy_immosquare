class CreatePricings < ActiveRecord::Migration[5.0]
  def change
    create_table :pricings do |t|
      t.references :product, index: true, foreign_key: false
      t.references :checkout_type, index: true, foreign_key: false
      t.references :checkout_unit, index: true, foreign_key: false
      t.string :name
      t.integer :quantity
      t.float :weight
      t.float :price_unit
      t.float :price_month
      t.float :price_year
      t.integer :price_from

      t.timestamps
    end
  end
end
