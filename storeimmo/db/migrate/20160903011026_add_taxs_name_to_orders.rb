class AddTaxsNameToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :tax1_value,  :string, :after => :tax1_name
    add_column :orders, :tax1_number, :string, :after => :tax1_value
    add_column :orders, :tax2_value,  :string, :after => :tax2_name
    add_column :orders, :tax2_number, :string, :after => :tax2_value
  end
end
