class RemoveEnvToStoreDomains < ActiveRecord::Migration[5.0]
  def change
    remove_column :store_domains, :env, :string
  end
end
