class CreateGatewayActivations < ActiveRecord::Migration[5.0]
  def change
    create_table :gateway_activations do |t|
      t.references :client, foreign_key: false
      t.references :store, foreign_key: false
      t.references :gateway, foreign_key: false
      t.string :username
      t.string :password
      t.timestamps
    end
  end
end
