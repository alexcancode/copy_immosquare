class RemoveSkuToProducts < ActiveRecord::Migration[5.0]
  def change
    remove_column :products, :sku, :string
  end
end
