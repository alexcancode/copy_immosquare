class CreateStoreShippings < ActiveRecord::Migration[5.0]
  def change
    create_table :store_shippings do |t|
      t.string :name
      t.money :unit_price
      t.references :store_setting, foreign_key: false, index: true


      t.timestamps
    end

    add_column :orders, :store_shipping_id, :integer, :after => :stripe_card_id
    add_column :orders, :store_shipping_name, :string, :after => :store_shipping_id
    add_money :orders, :store_shipping_unit_price, :currency => {:present => false}, :after => :store_shipping_name
  end
end
