class AddWarningToPricing < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :warning, :string
  end
end
