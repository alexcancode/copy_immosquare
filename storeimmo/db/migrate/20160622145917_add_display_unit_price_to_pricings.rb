class AddDisplayUnitPriceToPricings < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :display_unit_price, :boolean, :default => 0
  end
end
