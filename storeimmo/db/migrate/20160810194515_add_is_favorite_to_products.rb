class AddIsFavoriteToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :is_favorite, :integer
  end
end
