class ChangePriceInShippings < ActiveRecord::Migration[5.0]
  def change

    remove_column :shippings, :unit_price_cents
    add_money :shippings, :price, amount: { null: true, default: 0 },currency: { present: false } , :after => :country_id
  end
end
