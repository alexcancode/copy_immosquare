class RemoveMaxQuantityToExtras < ActiveRecord::Migration[5.0]
  def change
    remove_column :extras, :max_quantity, :integer
  end
end
