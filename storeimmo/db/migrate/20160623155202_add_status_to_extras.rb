class AddStatusToExtras < ActiveRecord::Migration[5.0]
  def change
    add_column :extras, :status, :string, :after => :sku, :default => "active"

    add_column :pricings, :rechargeable, :boolean, :after => :shippable, :default => 0

    add_column :services, :sku, :string, :after => :name

    rename_column :authorizations, :type, :mode
    rename_column :authorizations, :service_id, :pricing_id

    add_money :authorizations, :price
  end
end
