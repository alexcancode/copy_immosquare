class ScopeClientWithStore < ActiveRecord::Migration[5.0]
  def change
    remove_index :clients, :email
    add_index :clients, [:email, :store_id], :unique => true
  end
end
