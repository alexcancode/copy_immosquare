class AddAuthorizationWithoutConnectionToStore < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :pay_without_connection, :boolean, :default => false
  end
end
