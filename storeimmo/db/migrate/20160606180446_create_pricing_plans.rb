class CreatePricingPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :pricing_plans do |t|
      t.references :pricing, foreign_key: false, :index => true
      t.money :price_month
      t.money :price_year, currency: { present: false }

      t.timestamps
    end
  end
end
