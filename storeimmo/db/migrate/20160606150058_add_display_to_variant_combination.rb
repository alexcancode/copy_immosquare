class AddDisplayToVariantCombination < ActiveRecord::Migration[5.0]
  def change
    add_column :variant_combinations, :display, :integer, :default => 1
  end
end
