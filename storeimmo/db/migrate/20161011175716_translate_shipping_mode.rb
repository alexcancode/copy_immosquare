class TranslateShippingMode < ActiveRecord::Migration[5.0]
 def up
   ShippingMode.create_translation_table! :name => :string
 end
 
 def down
   ShippingMode.drop_translation_table!
 end
end
