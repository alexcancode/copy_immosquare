class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.references :store, foreign_key: false
      t.references :client, foreign_key: false
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :company
      t.text :message

      t.timestamps
    end
  end
end
