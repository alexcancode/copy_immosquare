class RemoveCurrencyToAuthorizations < ActiveRecord::Migration[5.0]
  def change
    remove_column :authorizations, :currency, :string
    add_column :authorizations, :currency, :string, :after => :price_cents
  end
end
