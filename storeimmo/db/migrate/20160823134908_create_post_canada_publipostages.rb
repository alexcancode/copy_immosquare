class CreatePostCanadaPublipostages < ActiveRecord::Migration[5.0]
  def change
    create_table :post_canada_publipostages do |t|

      t.string :sector_zipcode,       :limit => 6
      t.string :sector_name,          :limit => 30
      t.string :sector_type,          :limit => 5
      t.string :sector_qualification, :limit => 15
      t.string :sector_short_name,    :limit => 15
      t.string :sector_state_code,    :limit => 1

      t.string :shipping_mode_type,   :limit => 2
      t.string :shipping_mode_id,     :limit => 4
      t.string :shipping_road_id

      t.string :spot_appartments, :limit => 5
      t.string :spot_shops,       :limit => 5
      t.string :spot_domiciles,   :limit => 5
      t.string :spot_farms,       :limit => 5

      t.string :consumer_choice_appartments, :limit => 5
      t.string :consumer_choice_shops,       :limit => 5
      t.string :consumer_choice_domiciles,   :limit => 5
      t.string :consumer_choice_farms,       :limit => 5

      t.string :zipcode,                            :limit => 6
      t.string :zipcode_rta,                        :limit => 3
      t.string :zipcode_udl_type,                   :limit => 2
      t.string :itinerary_sollicitation_indicator,  :limit => 2

      t.string :supervisor_zipcode,               :limit => 6
      t.string :supervisor_name,                  :limit => 30
      t.string :supervisor_shipping_name,         :limit => 30
      t.string :supervisor_type,                  :limit => 5
      t.string :supervisor_qualification,         :limit => 15
      t.string :supervisor_street_number,         :limit => 6
      t.string :supervisor_street_number_suffix,  :limit => 1
      t.string :supervisor_unit_number,           :limit => 6
      t.string :supervisor_street_name,           :limit => 30
      t.string :supervisor_street_type,           :limit => 6
      t.string :supervisor_cardinal_point,        :limit => 2
      t.string :supervisor_province,              :limit => 2
      t.string :supervisor_state_code,            :limit => 1



      t.timestamps
    end
    add_index :post_canada_publipostages,:zipcode_rta
  end
end


