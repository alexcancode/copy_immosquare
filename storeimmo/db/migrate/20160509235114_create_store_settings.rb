class CreateStoreSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :taxes do |t|
      t.string :name
      t.string :tax1_name
      t.float :tax1_value
      t.string :tax2_name
      t.float :tax2_value

      t.timestamps
    end


    create_table :store_settings do |t|
      t.string :name
      t.string :currency
      t.text :address
      t.string :billing_wording1
      t.string :billing_wording2
      t.string :billing_wording3
      t.string :billing_wording4
      t.references :tax, foreign_key: true
      t.text :contact_address
      t.string :contact_email
      t.string :contact_phone
      t.string :contact_wording1
      t.string :contact_wording2
      t.text :languages

      t.timestamps
    end
  end
end
