class AddReferencesToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :clients, :store, foreign_key: false, :after => :email
  end
end
