class UpdateOrdersTable < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :shipping_address_first_name, :string
    add_column :orders, :shipping_address_last_name, :string
    add_column :orders, :shipping_address_address1, :string
    add_column :orders, :shipping_address_address2, :string
    add_column :orders, :shipping_address_city, :string
    add_column :orders, :shipping_address_country, :string
    add_column :orders, :shipping_address_province, :string
    add_column :orders, :shipping_address_zipcode, :string
    add_column :orders, :billing_address_first_name, :string
    add_column :orders, :billing_address_last_name, :string
    add_column :orders, :billing_address_address1, :string
    add_column :orders, :billing_address_address2, :string
    add_column :orders, :billing_address_city, :string
    add_column :orders, :billing_address_country, :string
    add_column :orders, :billing_address_province, :string
    add_column :orders, :billing_address_zipcode, :string
    add_column :orders, :stripe_card_id, :string

    rename_column :orders, :subtotal_currency, :currency

    add_money :orders, :shipping, currency: { present: false }
    add_money :orders, :tax, currency: { present: false }
    add_money :orders, :total, currency: { present: false }

    create_table :order_sales do |t|
      t.references :order, foreign_key: false, :index => true
      t.references :sale, foreign_key: false, :index => true
      t.text :sale_data, :limit => 4294967295
    end

    create_table :cart_item_sales do |t|
      t.references :cart_item, foreign_key: false, :index => true
      t.references :sale, foreign_key: false, :index => true
      t.text :sale_data, :limit => 4294967295
    end

    remove_column :cart_items, :sale_id, :integer
    remove_column :cart_items, :sale_data, :text

  end
end
