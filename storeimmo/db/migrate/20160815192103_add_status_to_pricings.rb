class AddStatusToPricings < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :status, :string, :after => :draft, :default => "active"
  end
end
