class AddPriceToExtras < ActiveRecord::Migration[5.0]
  def change
    remove_column :extras, :price
    add_money :extras, :price
  end
end
