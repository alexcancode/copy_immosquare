class AddMultipleToExtras < ActiveRecord::Migration[5.0]
  def change
    add_column :extras, :max_quantity, :integer, :default => 1
  end
end
