class AddPrintTemplateIdToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :print_template_id, :integer
  end
end
