class UpdateCartItem < ActiveRecord::Migration[5.0]
  def change
    remove_column :orders, :session_id, :integer
    add_money :cart_items, :total_price
  end
end
