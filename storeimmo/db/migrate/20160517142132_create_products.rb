class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.references :product_category, index: true, foreign_key: false
      t.integer :supplier_id
      t.string :name
      t.string :SKU
      t.integer :validity_duration_month
      t.text :description
      t.string :video_url
      t.string :logo_url

      t.timestamps
    end
  end
end
