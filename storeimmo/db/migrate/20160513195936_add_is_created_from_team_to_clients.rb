class AddIsCreatedFromTeamToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :is_created_from_team, :integer, :after => :store_id
  end
end
