class AddTranslateFieldToProducts < ActiveRecord::Migration[5.0]
  def change
    Product.add_translation_fields! name: :string
  end
end
