class RemovePrinCentsToShippings < ActiveRecord::Migration[5.0]
  def change
    remove_column :shippings, :unit_price_currency, :string
  end
end
