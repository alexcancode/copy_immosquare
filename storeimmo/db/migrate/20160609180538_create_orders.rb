class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.references :client, foreign_key: false, index: true
      t.integer :session_id
      t.string :status
      t.money :subtotal

      t.timestamps
    end
  end
end
