class AddIsPublipostageToPricings < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :is_publipostage, :integer, :default => 0
  end
end
