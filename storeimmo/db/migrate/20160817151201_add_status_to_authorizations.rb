class AddStatusToAuthorizations < ActiveRecord::Migration[5.0]
  def change
    add_column :authorizations, :status, :string, :after => :pricing_id, :default => "active"
    add_column :authorizations, :inactive_date, :datetime, :after => :status
  end
end
