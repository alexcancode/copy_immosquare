class RemoveCheckoutType < ActiveRecord::Migration[5.0]
  def change
    drop_table :checkout_types
  end
end
