class AddStoreToProducts < ActiveRecord::Migration[5.0]
  def change
    add_reference :products, :store, foreign_key: false, :after => :id
  end
end
