class AddBaselineToStore < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :baseline, :string, :after => :theme
  end
end
