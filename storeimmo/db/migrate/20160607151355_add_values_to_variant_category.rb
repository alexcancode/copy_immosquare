class AddValuesToVariantCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :variant_categories, :values, :string, :after => :name
  end
end
