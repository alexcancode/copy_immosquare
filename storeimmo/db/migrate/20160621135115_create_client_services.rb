class CreateClientServices < ActiveRecord::Migration[5.0]
  def change
    create_table :client_services do |t|
      t.references :client, index: true, foreign_key: false
      t.references :service, index: true, foreign_key: false
      t.datetime :limit_date
      t.integer :tokens, :default => 0
      t.string :type

      t.timestamps
    end

    add_column :pricings, :service_id, :integer, :after => :product_id
  end
end
