class TranslatePricing < ActiveRecord::Migration[5.0]
  def change
    reversible do |dir|
      dir.up do
        Pricing.create_translation_table!({
          :name         => :string,
          :description  => :text,
          :warning      => :string
          }, {
            :migrate_data => true
            })
      end

      dir.down do
        Pricing.drop_translation_table! :migrate_data => true
      end
    end
  end
end
