class AddDateBuyToAuthorizations < ActiveRecord::Migration[5.0]
  def change
    add_column :authorizations, :purchase_date, :datetime, :after => :pricing_id
  end
end
