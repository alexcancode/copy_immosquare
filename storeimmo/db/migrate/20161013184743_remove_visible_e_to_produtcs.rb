class RemoveVisibleEToProdutcs < ActiveRecord::Migration[5.0]
  def change
    remove_column :products, :visible_everywhere, :integer
    remove_column :products, :display_in_store, :integer
  end
end
