class RemoveSlugToCheckoutUnits < ActiveRecord::Migration[5.0]
  def change
    remove_column :checkout_units, :slug, :string
  end
end
