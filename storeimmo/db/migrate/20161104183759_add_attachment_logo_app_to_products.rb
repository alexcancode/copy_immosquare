class AddAttachmentLogoAppToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :logo_app
    end
  end

  def self.down
    remove_attachment :products, :logo_app
  end
end
