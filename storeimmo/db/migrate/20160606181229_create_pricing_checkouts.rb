class CreatePricingCheckouts < ActiveRecord::Migration[5.0]
  def change
    create_table :pricing_checkouts do |t|
      t.references :pricing, foreign_key: false, :index => true
      t.integer :quantity_min, :default => 1
      t.integer :quantity_max
      t.money :price

      t.timestamps
    end

    remove_column :pricings, :price_month, :integer
    remove_column :pricings, :price_year, :integer
    remove_column :pricings, :price_unit, :integer

    remove_column :pricings, :price_month_cents, :integer
    remove_column :pricings, :price_year_cents, :integer
    remove_column :pricings, :price_unit_cents, :integer

    remove_column :pricings, :currency, :string
  end
end
