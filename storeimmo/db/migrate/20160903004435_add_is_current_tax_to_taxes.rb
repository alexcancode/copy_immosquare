class AddIsCurrentTaxToTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :taxes, :is_current_tax, :integer, :default => 0, :after => :store_id
  end
end
