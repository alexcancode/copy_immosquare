class CreateCheckoutUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :checkout_units do |t|
      t.string :name

      t.timestamps
    end
  end
end
