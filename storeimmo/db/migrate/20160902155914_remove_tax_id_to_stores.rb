class RemoveTaxIdToStores < ActiveRecord::Migration[5.0]
  def change
    remove_column :stores, :tax_id
    remove_column :stores, :tax1_number
    remove_column :stores, :tax2_number
  end
end
