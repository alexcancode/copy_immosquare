class ManageVariants < ActiveRecord::Migration[5.0]
  def change
    remove_column :variants, :default, :integer
    add_column :variants, :SKU, :string, :after => :name

    remove_column :variant_categories, :default, :integer
    add_column :variant_categories, :product_id, :integer, :after => :name
    drop_table :products_variants
  end
end
