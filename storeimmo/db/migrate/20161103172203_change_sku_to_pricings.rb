class ChangeSkuToPricings < ActiveRecord::Migration[5.0]
  def change
    change_column :pricings, :sku, :string, :after => :id
  end
end
