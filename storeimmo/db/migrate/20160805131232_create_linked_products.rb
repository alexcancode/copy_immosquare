class CreateLinkedProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :linked_products, :id => false do |t|
      t.integer :product_id, index: true
      t.integer :linked_product_id, index: true
    end

    rename_column :cart_items, :total_price_currency, :currency
  end
end
