class AddSlugToProductTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :product_types, :slug, :string, :after => :name
  end
end
