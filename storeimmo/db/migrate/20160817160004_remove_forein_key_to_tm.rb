class RemoveForeinKeyToTm < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key(:team_members, :column => :client_id)
    remove_foreign_key(:team_members, :column => :team_id)
  end
end
