class CreateAttachements < ActiveRecord::Migration[5.0]
  def change
    change_table :products do |t|
      t.remove :logo_url
      t.attachment :logo, :after => :video_url
      t.integer :display_in_store, :after => :description
      t.string :email_notification, :after => :display_in_store
      t.integer :notification_on_order, :after => :email_notification
      t.integer :notification_on_use, :after => :notification_on_order
      t.integer :draft, :after => :supplier_id
    end

    change_table :assets do |t|
      t.remove :url
      t.attachment :image, :after => :product_id
    end
  end
end
