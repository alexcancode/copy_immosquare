class AddStoreToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_reference :companies, :store, foreign_key: false, :after => :id
  end
end
