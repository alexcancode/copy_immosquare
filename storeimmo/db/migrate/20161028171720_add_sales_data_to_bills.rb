class AddSalesDataToBills < ActiveRecord::Migration[5.0]
  def change
    add_column :bills, :sales_data, :text, :limit => 4294967295, :after => :items_data
  end
end
