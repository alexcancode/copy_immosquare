class RenameProdcutsUserStore < ActiveRecord::Migration[5.0]
  def change
    remove_column :product_user_stores, :store_id, :integer
    rename_table :product_user_stores, :product_users
  end
end
