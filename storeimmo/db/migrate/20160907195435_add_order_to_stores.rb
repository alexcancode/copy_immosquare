class AddOrderToStores < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :position, :integer, :after => :country_id, :default => 1
  end
end
