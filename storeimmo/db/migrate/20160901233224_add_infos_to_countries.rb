class AddInfosToCountries < ActiveRecord::Migration[5.0]
  def change
    drop_table :store_countries
    remove_column :countries, :country_name, :string
    add_column :countries, :name, :string, :after => :id
    add_column :countries, :code, :string, :after => :name
    add_column :countries, :currency, :string, :after => :code
  end
end
