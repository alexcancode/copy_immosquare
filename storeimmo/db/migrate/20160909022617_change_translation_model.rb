class ChangeTranslationModel < ActiveRecord::Migration[5.0]
  def change
    remove_column :translations, :value
    remove_column :translations, :interpolations
    remove_column :translations, :is_proc
    remove_column :translations, :locale
    add_column    :translations, :fr    , :string , :after => :key
    add_column    :translations, :fr_ca , :string , :after => :fr
    add_column    :translations, :en    , :string , :after => :fr_ca
  end
end
