class AddStoreSettingsToTaxs < ActiveRecord::Migration[5.0]
  def change
    add_reference :taxes, :store_setting, foreign_key: false, :after => :id
  end
end
