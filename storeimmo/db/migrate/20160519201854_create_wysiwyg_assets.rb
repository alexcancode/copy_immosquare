class CreateWysiwygAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :wysiwyg_assets do |t|
      t.references :imageable, polymorphic: true

      t.timestamps
    end
  end
end
