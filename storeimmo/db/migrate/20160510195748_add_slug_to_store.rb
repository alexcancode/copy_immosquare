class AddSlugToStore < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :slug, :string, :after => :domains
  end
end
