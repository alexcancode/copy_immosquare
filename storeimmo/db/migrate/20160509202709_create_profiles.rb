class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :company_name
      t.string :company_email
      t.string :company_website

      t.timestamps
    end
  end
end
