class AddAddressToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :shipping_address_address1, :string
    add_column :profiles, :shipping_address_address2, :string
    add_column :profiles, :shipping_address_city, :string
    add_column :profiles, :shipping_address_country, :string
    add_column :profiles, :shipping_address_province, :string
    add_column :profiles, :shipping_address_zipcode, :string
    add_column :profiles, :billing_address_address1, :string
    add_column :profiles, :billing_address_address2, :string
    add_column :profiles, :billing_address_city, :string
    add_column :profiles, :billing_address_country, :string
    add_column :profiles, :billing_address_province, :string
    add_column :profiles, :billing_address_zipcode, :string
  end
end
