class AddIsLinkedToPricingToExtras < ActiveRecord::Migration[5.0]
  def change
    add_column :extras, :is_linked_to_pricing_quantity, :boolean, :default => true
  end
end
