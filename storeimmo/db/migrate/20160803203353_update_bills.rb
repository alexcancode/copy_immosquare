class UpdateBills < ActiveRecord::Migration[5.0]
  def change
    add_column :bills, :date, :datetime, :after => :order_id
    add_column :bills, :currency, :string, :after => :date

    add_column :bills, :subtotal_cents, :integer, :after => :currency, :default => 0

    add_column :bills, :tax1_name, :string, :after => :subtotal_cents
    add_column :bills, :tax1_cents, :integer, :after => :tax1_name, :default => 0

    add_column :bills, :tax2_name, :string, :after => :tax1_cents
    add_column :bills, :tax2_cents, :integer, :after => :tax2_name, :default => 0

    add_column :bills, :shipping_id, :integer, :after => :tax2_cents
    add_column :bills, :shipping_unit_price_cents, :integer, :after => :shipping_id
    add_column :bills, :shipping_cents, :integer, :after => :shipping_unit_price_cents, :default => 0

    add_column :bills, :total_price_cents, :integer, :after => :shipping_cents, :default => 0

    add_column :bills, :stripe_card_id, :string, :after => :total_price_cents
    remove_column :orders, :stripe_card_id, :string
  end
end
