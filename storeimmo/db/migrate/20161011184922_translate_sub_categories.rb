class TranslateSubCategories < ActiveRecord::Migration[5.0]
 def up
   Subcategory.create_translation_table! :name => :string
 end
 
 def down
   Subcategory.drop_translation_table!
 end
end
