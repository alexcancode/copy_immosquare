class ChangeCombinations < ActiveRecord::Migration[5.0]
  def change
    rename_column :variant_combinations_variants, :variant_id, :variant_category_id

    rename_table :variant_combinations_variants, :variant_categories_combinations
  end
end
