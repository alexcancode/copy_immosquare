class AddNameToSales < ActiveRecord::Migration[5.0]
  def change
    add_column :sales, :name, :string, :after => :code
  end
end
