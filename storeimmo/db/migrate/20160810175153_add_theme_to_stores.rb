class AddThemeToStores < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :theme, :text, :after => :shareimmo_api_token
  end
end
