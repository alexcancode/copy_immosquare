class UpdateStoreShipping < ActiveRecord::Migration[5.0]
  def change
    rename_table :store_shippings, :shippings

    change_table :shippings do |t|
      t.remove :name
      t.remove :store_setting_id

      t.references :country, :after => :id
      t.float :max_weight, :after => :country_id
    end
  end
end
