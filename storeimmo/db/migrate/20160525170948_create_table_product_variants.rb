class CreateTableProductVariants < ActiveRecord::Migration[5.0]
  def change
    create_table :products_variants do |t|
      t.references :product, index: true
      t.references :variant, index: true
    end
  end
end
