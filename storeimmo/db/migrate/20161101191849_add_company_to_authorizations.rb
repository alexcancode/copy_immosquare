class AddCompanyToAuthorizations < ActiveRecord::Migration[5.0]
  def change
    add_column :authorizations, :company_id, :integer, :after => :client_id
  end
end
