class RemoveCurrencyToExtras < ActiveRecord::Migration[5.0]
  def change
    remove_column :extras, :currency, :string
  end
end
