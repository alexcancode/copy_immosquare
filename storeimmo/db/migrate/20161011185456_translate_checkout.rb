class TranslateCheckout < ActiveRecord::Migration[5.0]
 def up
   CheckoutUnit.create_translation_table! :name => :string
 end
 
 def down
   CheckoutUnit.drop_translation_table!
 end
end
