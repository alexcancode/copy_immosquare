class FixBugOnExtraPrice2 < ActiveRecord::Migration[5.0]
  def change
    change_column :extras, :price2_cents, :integer, :null => true
  end
end
