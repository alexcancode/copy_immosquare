class CreateImportTrackings < ActiveRecord::Migration[5.0]
  def change
    create_table :import_trackings do |t|
      t.integer :gateway_activation_id
      t.integer :nb_success
      t.integer :nb_error
      t.datetime :date

      t.timestamps
    end
  end
end
