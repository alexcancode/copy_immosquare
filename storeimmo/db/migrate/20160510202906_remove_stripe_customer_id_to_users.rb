class RemoveStripeCustomerIdToUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :stripe_customer_id, :string
  end
end
