class AddCodeToSales < ActiveRecord::Migration[5.0]
  def change
    add_column :sales, :pricing_id, :integer, :after => :product_id
    add_column :sales, :code, :string, :after => :pricing_id
  end
end
