class AddAttachmentPictureToWysiwygAssets < ActiveRecord::Migration
  def self.up
    change_table :wysiwyg_assets do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :wysiwyg_assets, :picture
  end
end
