class RemoveCurrencyToStores < ActiveRecord::Migration[5.0]
  def change
    remove_column :stores, :currency, :string
  end
end
