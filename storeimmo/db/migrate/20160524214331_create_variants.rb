class CreateVariants < ActiveRecord::Migration[5.0]
  def change
    create_table :variants do |t|
      t.string :name
      t.integer :default
      t.references :variant_category, index: true, foreign_key: false

      t.timestamps
    end
  end
end
