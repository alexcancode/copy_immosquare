class RemoveStoreFromSale < ActiveRecord::Migration[5.0]
  def change
    remove_column :sales, :store_id, :integer
  end
end
