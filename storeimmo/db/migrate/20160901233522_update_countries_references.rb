class UpdateCountriesReferences < ActiveRecord::Migration[5.0]
  def change
    remove_reference :stores, :store_country
    remove_reference :pricings, :store_country
    add_reference :stores, :country, foreign_key: false, :after => :id
    add_reference :pricings, :country, foreign_key: false, :after => :id
  end
end
