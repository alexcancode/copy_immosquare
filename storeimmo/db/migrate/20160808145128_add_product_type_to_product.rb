class AddProductTypeToProduct < ActiveRecord::Migration[5.0]
  def change
    add_reference :products, :product_type, foreign_key: false, :default => 1
  end
end
