class CreateGateways < ActiveRecord::Migration[5.0]
  def change
    create_table :gateways do |t|
      t.string :name
      t.references :store, foreign_key: false
      t.string :url
      t.timestamps
    end
  end
end
