class AddStoreIdToShippings < ActiveRecord::Migration[5.0]
  def change
    add_reference :shippings, :store, foreign_key: false, :after =>:id
  end
end
