class AddQuantityEditableToPricings < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :quantity_editable, :boolean, :default => 0
  end
end
