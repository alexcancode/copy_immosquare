class RenameCountry < ActiveRecord::Migration[5.0]
  def change
    rename_column :countries, :name, :country_name
  end
end
