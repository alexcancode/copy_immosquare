class RemoveCurrencyToCartsItems < ActiveRecord::Migration[5.0]
  def change
    remove_column :cart_items, :currency, :string
    add_column :cart_items, :currency, :string, :after => :total_price_cents
  end
end
