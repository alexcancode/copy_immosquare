class AddAdressToBills < ActiveRecord::Migration[5.0]
  def change
    add_column :bills, :has_been_shipped, :boolean, :after => :items_data, :default => false
    add_column :bills, :shippable, :boolean, :after => :tax2_cents, :default => false
    add_column :bills, :shipping_address_first_name, :string, :after => :shipping_id, :default => ""
    add_column :bills, :shipping_address_last_name, :string, :after => :shipping_address_first_name, :default => ""
    add_column :bills, :shipping_address_address1, :string, :after => :shipping_address_last_name, :default => ""
    add_column :bills, :shipping_address_address2, :string, :after => :shipping_address_address1, :default => ""
    add_column :bills, :shipping_address_zipcode, :string, :after => :shipping_address_address2, :default => ""
    add_column :bills, :shipping_address_city, :string, :after => :shipping_address_zipcode, :default => ""
  end
end
