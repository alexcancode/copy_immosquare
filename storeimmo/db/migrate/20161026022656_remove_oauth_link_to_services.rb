class RemoveOauthLinkToServices < ActiveRecord::Migration[5.0]
  def change
    remove_column :services, :oauth_link, :string
    remove_column :services, :sku, :string
    add_column :services, :link, :string, :after => :name
  end
end
