class RemoveIsCreatedFromTeamToClients < ActiveRecord::Migration[5.0]
  def change
    remove_column :clients, :is_created_from_team, :integer
  end
end
