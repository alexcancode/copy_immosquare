class CreateBills < ActiveRecord::Migration[5.0]
  def change
    create_table :bills do |t|
      t.references :order, index: true, foreign_key: false

      t.timestamps
    end

    add_column :cart_items, :bill_id, :integer, :after => :order_id
  end
end
