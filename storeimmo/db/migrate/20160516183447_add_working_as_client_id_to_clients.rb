class AddWorkingAsClientIdToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :working_as_client_id, :integer, :after => :id
    add_index :clients, :working_as_client_id
  end
end
