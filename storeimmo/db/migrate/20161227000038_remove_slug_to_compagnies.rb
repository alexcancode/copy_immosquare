class RemoveSlugToCompagnies < ActiveRecord::Migration[5.0]
  def change
    remove_column :companies, :slug, :string
  end
end
