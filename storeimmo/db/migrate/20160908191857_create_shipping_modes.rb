class CreateShippingModes < ActiveRecord::Migration[5.0]
  def change
    create_table :shipping_modes do |t|
      t.references :store, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
