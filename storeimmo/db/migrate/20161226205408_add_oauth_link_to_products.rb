class AddOauthLinkToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :oauth_link, :string
    remove_column :companies, :oauth_link, :string
  end
end
