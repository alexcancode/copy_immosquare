class AddClientToBill < ActiveRecord::Migration[5.0]
  def change
    remove_column :bills, :client_data, :text, :limit => 4294967295, :after => :stripe_card_id
    add_column :bills, :client_id, :integer, :after => :id
    add_column :bills, :shipping_address_country, :string, :after => :shipping_id
  end
end
