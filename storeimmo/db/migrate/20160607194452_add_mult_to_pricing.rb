class AddMultToPricing < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :display_range_price, :boolean
  end
end
