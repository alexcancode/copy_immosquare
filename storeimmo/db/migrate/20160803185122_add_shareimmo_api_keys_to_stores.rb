class AddShareimmoApiKeysToStores < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :shareimmo_api_url, :string,:after =>:color2
    add_column :stores, :shareimmo_api_key, :string,:after => :shareimmo_api_url
    add_column :stores, :shareimmo_api_token, :string,:after => :shareimmo_api_key

  end
end
