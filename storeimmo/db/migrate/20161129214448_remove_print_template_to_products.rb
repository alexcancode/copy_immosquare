class RemovePrintTemplateToProducts < ActiveRecord::Migration[5.0]
  def change
   remove_column :products, :print_template_ids, :string, :after => :is_printable
   add_column :products, :print_template_id, :integer, :after => :is_printable, :default => 1
  end
end
