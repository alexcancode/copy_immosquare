class AddPositionToExtras < ActiveRecord::Migration[5.0]
  def change
    add_column :extras, :position, :integer, :after => :pricing_id
  end
end
