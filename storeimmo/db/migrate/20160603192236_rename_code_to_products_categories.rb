class RenameCodeToProductsCategories < ActiveRecord::Migration[5.0]
  def change
    rename_column :product_categories, :code, :slug
  end
end
