class DeleteCheckoutTypeIdFromPricings < ActiveRecord::Migration[5.0]
  def change
    remove_column :pricings, :checkout_type_id, :integer
  end
end
