class TranslateCategories < ActiveRecord::Migration[5.0]
  def change
    rename_column :category_translations, :product_category_id, :category_id
  end
end
