class RemoveForGKeyToTeams < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key(:teams, :column => :client_id)
  end
end
