class AddPriceToHomeToPricing < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :price_to_home, :boolean
  end
end
