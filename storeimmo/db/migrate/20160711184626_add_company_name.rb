class AddCompanyName < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :billing_address_company_name, :string, :after => :shipping_same_as_billing
  end
end
