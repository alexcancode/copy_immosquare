class AddEnvToStoreDomains < ActiveRecord::Migration[5.0]
  def change
    add_column :store_domains, :env, :string, :after => :store_id
  end
end
