class AddShippingIdToShippings < ActiveRecord::Migration[5.0]
  def change
    remove_reference :shippings, :store
    add_reference :shippings, :shipping_mode, foreign_key: false, :after => :id
  end
end
