class RemoveProductFromBill < ActiveRecord::Migration[5.0]
  def change
    remove_column :bills, :product_id, :integer
  end
end
