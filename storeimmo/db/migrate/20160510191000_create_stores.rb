class CreateStores < ActiveRecord::Migration[5.0]
  def change
    create_table :stores do |t|
      t.string :name
      t.string :domains
      t.string :color1
      t.string :color2

      t.timestamps
    end
  end
end
