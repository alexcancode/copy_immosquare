class CreateExtras < ActiveRecord::Migration[5.0]
  def change
    create_table :extras do |t|
      t.references :pricing, foreign_key: false, index: true
      t.string :name
      t.float :price
      t.string :SKU

      t.timestamps
    end
  end
end
