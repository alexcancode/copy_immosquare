class UpdateAuthorizations < ActiveRecord::Migration[5.0]
  def change
    remove_column :bills, :authorization_id
    add_column :authorizations, :item_data, :text, :limit => 4294967295
    remove_column :authorizations, :last_bill_id
    remove_column :authorizations, :price_cents
    remove_column :authorizations, :price_before_sale_cents

    add_column :authorizations, :first_bill_id, :integer, :after => :client_id

    add_column :bills, :origin_type, :string, :after => :client_id
    add_column :bills, :origin_id, :integer, :after => :origin_type
  end
end
