class AdDPriceToExtras < ActiveRecord::Migration[5.0]
  def change
    rename_column :extras, :price_currency , :currency
    add_money :extras, :price2,  currency: { present: false }
  end
end
