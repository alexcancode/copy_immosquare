class AddStripeCustomerIdToClients < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :profiles, :users
    remove_reference :profiles, :user
    add_reference :profiles, :client
    add_column :clients, :stripe_customer_id, :string, :after => :id

  end
end
