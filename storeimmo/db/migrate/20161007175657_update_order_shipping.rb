class UpdateOrderShipping < ActiveRecord::Migration[5.0]
  def change
   rename_column :orders, :store_shipping_id, :shipping_id
   rename_column :orders, :store_shipping_name, :shipping_name
   rename_column :orders, :store_shipping_unit_price_cents, :shipping_unit_price_cents
 end
end
