class AddCalculateShippingInTaxesToStore < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :calculate_taxes_with_shipping, :boolean, :after => :pay_without_connection, :default => true
  end
end
