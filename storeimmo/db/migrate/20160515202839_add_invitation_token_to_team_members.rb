class AddInvitationTokenToTeamMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :team_members, :invitation_token, :string
  end
end
