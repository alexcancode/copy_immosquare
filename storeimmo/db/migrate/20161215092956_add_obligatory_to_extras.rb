class AddObligatoryToExtras < ActiveRecord::Migration[5.0]
  def change
    add_column :extras, :obligatory, :boolean
  end
end
