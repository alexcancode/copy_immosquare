class RemoveCurrencyToPricings < ActiveRecord::Migration[5.0]
  def change
    remove_column :pricings, :currency, :string
  end
end
