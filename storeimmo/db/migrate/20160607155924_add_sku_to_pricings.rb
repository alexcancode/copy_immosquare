class AddSkuToPricings < ActiveRecord::Migration[5.0]
  def change
    add_column :pricings, :sku, :string
  end
end
