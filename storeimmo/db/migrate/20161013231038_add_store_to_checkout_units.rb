class AddStoreToCheckoutUnits < ActiveRecord::Migration[5.0]
  def change
    add_reference :checkout_units, :store, foreign_key: false, :after => :id
  end
end
