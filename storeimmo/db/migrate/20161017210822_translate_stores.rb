class TranslateStores < ActiveRecord::Migration[5.0]
  def change
    reversible do |dir|
      dir.up do
        Store.create_translation_table!({
          :baseline => :string,
        }, {
          :migrate_data => true
        })
      end

      dir.down do
        Store.drop_translation_table! :migrate_data => true
      end
    end
  end
end
