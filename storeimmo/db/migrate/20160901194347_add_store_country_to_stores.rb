class AddStoreCountryToStores < ActiveRecord::Migration[5.0]
  def change
    add_reference :stores, :store_country, foreign_key: false, :after => :id
  end
end
