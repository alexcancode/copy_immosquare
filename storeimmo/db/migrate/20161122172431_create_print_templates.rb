class CreatePrintTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :print_templates do |t|
      t.integer :uid
      t.string :name
      t.integer :properties_number

      t.timestamps
    end

    remove_column :products, :print_template_id, :integer

    create_table :products_print_templates, :id => false do |t|
      t.references :product, index: true, :foreign_key => false
      t.references :print_template, index: true, :foreign_key => false
    end

    reversible do |dir|
      dir.up do
        PrintTemplate.create_translation_table!({
          :name => :string,
        }, {
          :migrate_data => true
        })
      end

      dir.down do
        PrintTemplate.drop_translation_table! :migrate_data => true
      end
    end
  end
end
