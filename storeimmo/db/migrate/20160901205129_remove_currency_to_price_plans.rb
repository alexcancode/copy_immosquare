class RemoveCurrencyToPricePlans < ActiveRecord::Migration[5.0]
  def change
    remove_column :pricing_plans, :currency, :string
  end
end
