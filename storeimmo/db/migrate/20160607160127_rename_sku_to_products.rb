class RenameSkuToProducts < ActiveRecord::Migration[5.0]
  def change
    rename_column :products, :SKU, :sku
    rename_column :extras, :SKU, :sku
  end
end
