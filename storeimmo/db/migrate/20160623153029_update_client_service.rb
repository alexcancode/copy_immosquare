class UpdateClientService < ActiveRecord::Migration[5.0]
  def change
    rename_table :client_services, :authorizations

    change_table :authorizations do |t|
      t.text :history, :after => :type, :limit => 4294967295
    end

    create_table :authorizations_extras do |t|
      t.references :authorization, index: true, foreign_key: false
      t.references :extra, index: true, foreign_key: false

      t.timestamps
    end
  end
end
