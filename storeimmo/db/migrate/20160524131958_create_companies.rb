class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name

      t.timestamps
    end

    add_column :users, :company_id, :integer, :after => :id
    remove_column :products, :email_notification
  end
end
