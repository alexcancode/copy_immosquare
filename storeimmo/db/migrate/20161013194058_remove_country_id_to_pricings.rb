class RemoveCountryIdToPricings < ActiveRecord::Migration[5.0]
  def change
    remove_reference :pricings, :country, foreign_key: false
  end
end
