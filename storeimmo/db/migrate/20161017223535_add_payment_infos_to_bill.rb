class AddPaymentInfosToBill < ActiveRecord::Migration[5.0]
  def change
    add_column :bills, :payment_data, :text, :limit => 4294967295, :after => :stripe_card_id
  end
end
