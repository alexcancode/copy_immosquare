class CreateVariantCombinations < ActiveRecord::Migration[5.0]
  def change
    create_table :variant_combinations do |t|
      t.string :name
      t.string :SKU

      t.timestamps
    end

    create_table :variant_combinations_variants, :id => false do |t|
      t.references :variant, index: true
      t.references :variant_combination, index: true
    end

    remove_column :variants, :SKU, :string

    remove_column :variant_categories, :product_id, :integer
    add_column :variant_categories, :pricing_id, :integer, :after => :name
    add_column :variant_categories, :position, :integer, :after => :name
  end
end
