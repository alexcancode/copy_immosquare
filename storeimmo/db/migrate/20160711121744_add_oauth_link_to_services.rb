class AddOauthLinkToServices < ActiveRecord::Migration[5.0]
  def change
    add_column :services, :oauth_link, :string
  end
end
