class CreateVariantCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :variant_categories do |t|
      t.string :name
      t.integer :default

      t.timestamps
    end
  end
end
