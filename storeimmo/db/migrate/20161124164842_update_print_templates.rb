class UpdatePrintTemplates < ActiveRecord::Migration[5.0]
  def change

    drop_table :products_print_templates
    drop_table :print_templates
    drop_table :print_template_translations

    add_column :products, :print_template_ids, :string, :after => :is_printable
  end
end
