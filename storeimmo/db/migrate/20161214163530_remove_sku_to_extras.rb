class RemoveSkuToExtras < ActiveRecord::Migration[5.0]
  def change
    remove_column :extras, :sku, :string
    add_column :extras, :price_quantity, :integer, :default => 1 , :after  => :price_cents
  end
end
