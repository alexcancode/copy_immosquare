class AddStoreidToTaxes < ActiveRecord::Migration[5.0]
  def change
    remove_column :taxes, :store_setting_id
    add_reference :taxes, :store, foreign_key: false, :after => :id
  end
end
