class AddPricingToContacts < ActiveRecord::Migration[5.0]
  def change
    add_reference :contacts, :pricing, foreign_key: false, :after => :client_id
  end
end
