class CreateAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :assets do |t|
      t.references :product, index: true, foreign_key: false
      t.string :url

      t.timestamps
    end
  end
end
