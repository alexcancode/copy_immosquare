class AddShareimmoUserIdToClient < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :shareimmo_user_id, :integer, :after => :working_as_client_id
  end
end
