class UpdateCategories < ActiveRecord::Migration[5.0]
  def change
    rename_table :product_categories, :categories
    rename_table :product_category_translations, :category_translations

    add_column :categories, :store_id, :integer, :after => :id

    create_table :subcategories do |t|
      t.integer :category_id
      t.string :name
      t.string :slug
    end

    rename_column :products, :product_category_id, :subcategory_id
  end
end
