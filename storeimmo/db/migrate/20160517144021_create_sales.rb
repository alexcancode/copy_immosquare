class CreateSales < ActiveRecord::Migration[5.0]
  def change
    create_table :sales do |t|
      t.references :store, index: true, foreign_key: false
      t.references :client, index: true, foreign_key: false
      t.references :product, index: true, foreign_key: false
      t.float :percentage
      t.float :amount
      t.datetime :start
      t.datetime :end

      t.timestamps
    end
  end
end
