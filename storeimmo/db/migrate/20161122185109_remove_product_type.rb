class RemoveProductType < ActiveRecord::Migration[5.0]
  def change
    drop_table :product_types

    remove_column :products, :product_type_id, :integer
    add_column :products, :is_printable, :boolean, :after => :slug
  end
end
