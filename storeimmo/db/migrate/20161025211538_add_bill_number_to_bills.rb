class AddBillNumberToBills < ActiveRecord::Migration[5.0]
  def change
    add_column :bills, :number, :integer, :after => :id
  end
end
