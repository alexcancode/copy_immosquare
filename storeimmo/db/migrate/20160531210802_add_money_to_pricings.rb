class AddMoneyToPricings < ActiveRecord::Migration[5.0]
  def change

    change_table :pricings do |t|
      t.string :currency, :after => :weight
      t.money :price_unit, :after => :currency, currency: { present: false }
      t.money :price_month, :after => :price_unit_cents, currency: { present: false }
      t.money :price_year, :after => :price_year_cents, currency: { present: false }
    end
  end
end
