# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161227232222) do

  create_table "authorizations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.integer  "company_id"
    t.integer  "first_bill_id"
    t.string   "status",                            default: "active"
    t.string   "product_sku"
    t.datetime "inactive_date"
    t.datetime "last_bill_date"
    t.datetime "next_bill_date"
    t.datetime "purchase_date"
    t.datetime "limit_date"
    t.integer  "tokens",                            default: 0
    t.string   "mode"
    t.text     "history",        limit: 4294967295
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "currency"
    t.text     "item_data",      limit: 4294967295
    t.integer  "pricing_id"
    t.index ["client_id"], name: "index_authorizations_on_client_id", using: :btree
  end

  create_table "authorizations_extras", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "authorization_id"
    t.integer  "extra_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["authorization_id"], name: "index_authorizations_extras_on_authorization_id", using: :btree
    t.index ["extra_id"], name: "index_authorizations_extras_on_extra_id", using: :btree
  end

  create_table "bills", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "number"
    t.integer  "client_id"
    t.integer  "store_id"
    t.string   "origin_type"
    t.integer  "origin_id"
    t.datetime "date"
    t.string   "currency"
    t.integer  "subtotal_cents",                                 default: 0
    t.string   "tax1_name"
    t.integer  "tax1_cents",                                     default: 0
    t.string   "tax2_name"
    t.integer  "tax2_cents",                                     default: 0
    t.boolean  "shippable",                                      default: false
    t.integer  "shipping_id"
    t.string   "shipping_address_first_name",                    default: ""
    t.string   "shipping_address_last_name",                     default: ""
    t.string   "shipping_address_address1",                      default: ""
    t.string   "shipping_address_address2",                      default: ""
    t.string   "shipping_address_zipcode",                       default: ""
    t.string   "shipping_address_city",                          default: ""
    t.string   "shipping_address_country"
    t.integer  "shipping_unit_price_cents"
    t.integer  "shipping_cents",                                 default: 0
    t.integer  "total_price_cents",                              default: 0
    t.string   "stripe_card_id"
    t.text     "payment_data",                limit: 4294967295
    t.text     "items_data",                  limit: 4294967295
    t.boolean  "has_been_shipped",                               default: false
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.index ["store_id"], name: "index_bills_on_store_id", using: :btree
  end

  create_table "cart_item_sales", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "cart_item_id"
    t.integer "sale_id"
    t.text    "sale_data",    limit: 4294967295
    t.index ["cart_item_id"], name: "index_cart_item_sales_on_cart_item_id", using: :btree
    t.index ["sale_id"], name: "index_cart_item_sales_on_sale_id", using: :btree
  end

  create_table "cart_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "order_id"
    t.integer  "pricing_id"
    t.text     "pricing_data",             limit: 4294967295
    t.string   "chosen_plan"
    t.integer  "quantity"
    t.integer  "variant_combination_id"
    t.text     "variant_combination_data", limit: 4294967295
    t.text     "extras_id",                limit: 65535
    t.text     "extras_data",              limit: 4294967295
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.integer  "total_price_cents",                           default: 0, null: false
    t.string   "currency"
    t.integer  "price_before_sale_cents",                     default: 0, null: false
    t.string   "title"
    t.index ["order_id"], name: "index_cart_items_on_order_id", using: :btree
    t.index ["pricing_id"], name: "index_cart_items_on_pricing_id", using: :btree
    t.index ["variant_combination_id"], name: "index_cart_items_on_variant_combination_id", using: :btree
  end

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "position"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_translations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "category_id", null: false
    t.string   "locale",      null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "name"
    t.index ["category_id"], name: "index_category_translations_on_category_id", using: :btree
    t.index ["locale"], name: "index_category_translations_on_locale", using: :btree
  end

  create_table "checkout_unit_translations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "checkout_unit_id", null: false
    t.string   "locale",           null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "name"
    t.index ["checkout_unit_id"], name: "index_checkout_unit_translations_on_checkout_unit_id", using: :btree
    t.index ["locale"], name: "index_checkout_unit_translations_on_locale", using: :btree
  end

  create_table "checkout_units", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_checkout_units_on_store_id", using: :btree
  end

  create_table "clients", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "working_as_client_id"
    t.integer  "shareimmo_user_id"
    t.string   "stripe_customer_id"
    t.string   "email",                       default: "", null: false
    t.integer  "store_id"
    t.integer  "passowrd_generated_randomly", default: 0
    t.string   "encrypted_password",          default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",               default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.string   "invited_by_type"
    t.integer  "invited_by_id"
    t.integer  "invitations_count",           default: 0
    t.index ["email", "store_id"], name: "index_clients_on_email_and_store_id", unique: true, using: :btree
    t.index ["invitation_token"], name: "index_clients_on_invitation_token", unique: true, using: :btree
    t.index ["invitations_count"], name: "index_clients_on_invitations_count", using: :btree
    t.index ["invited_by_id"], name: "index_clients_on_invited_by_id", using: :btree
    t.index ["reset_password_token"], name: "index_clients_on_reset_password_token", unique: true, using: :btree
    t.index ["store_id"], name: "index_clients_on_store_id", using: :btree
    t.index ["working_as_client_id"], name: "index_clients_on_working_as_client_id", using: :btree
  end

  create_table "companies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.string   "name"
    t.string   "api_key"
    t.string   "api_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_companies_on_store_id", using: :btree
  end

  create_table "contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "client_id"
    t.integer  "pricing_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.string   "company"
    t.text     "message",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["client_id"], name: "index_contacts_on_client_id", using: :btree
    t.index ["pricing_id"], name: "index_contacts_on_pricing_id", using: :btree
    t.index ["store_id"], name: "index_contacts_on_store_id", using: :btree
  end

  create_table "countries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "code"
    t.string   "currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "extras", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "pricing_id"
    t.integer  "position"
    t.string   "name"
    t.string   "status",                        default: "active"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "price_cents",                   default: 0,        null: false
    t.integer  "price_quantity",                default: 1
    t.integer  "price2_cents",                  default: 0
    t.boolean  "is_linked_to_pricing_quantity", default: true
    t.boolean  "obligatory"
    t.index ["pricing_id"], name: "index_extras_on_pricing_id", using: :btree
  end

  create_table "gateway_activations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.integer  "store_id"
    t.integer  "gateway_id"
    t.string   "username"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_gateway_activations_on_client_id", using: :btree
    t.index ["gateway_id"], name: "index_gateway_activations_on_gateway_id", using: :btree
    t.index ["store_id"], name: "index_gateway_activations_on_store_id", using: :btree
  end

  create_table "gateways", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "store_id"
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_gateways_on_store_id", using: :btree
  end

  create_table "import_trackings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "gateway_activation_id"
    t.integer  "nb_success"
    t.integer  "nb_error"
    t.datetime "date"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "linked_products", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "product_id"
    t.integer "linked_product_id"
    t.index ["linked_product_id"], name: "index_linked_products_on_linked_product_id", using: :btree
    t.index ["product_id"], name: "index_linked_products_on_product_id", using: :btree
  end

  create_table "oauth_access_grants", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "resource_owner_id",               null: false
    t.integer  "application_id",                  null: false
    t.string   "token",                           null: false
    t.integer  "expires_in",                      null: false
    t.text     "redirect_uri",      limit: 65535, null: false
    t.datetime "created_at",                      null: false
    t.datetime "revoked_at"
    t.string   "scopes"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree
  end

  create_table "oauth_access_tokens", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",             null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",        null: false
    t.string   "scopes"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree
  end

  create_table "oauth_applications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                                    null: false
    t.string   "uid",                                     null: false
    t.string   "secret",                                  null: false
    t.text     "redirect_uri", limit: 65535,              null: false
    t.string   "scopes",                     default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree
  end

  create_table "order_sales", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "order_id"
    t.integer "sale_id"
    t.text    "sale_data",       limit: 4294967295
    t.integer "amount_cents",                       default: 0,     null: false
    t.string  "amount_currency",                    default: "EUR", null: false
    t.index ["order_id"], name: "index_order_sales_on_order_id", using: :btree
    t.index ["sale_id"], name: "index_order_sales_on_sale_id", using: :btree
  end

  create_table "orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.string   "status"
    t.integer  "subtotal_cents",               default: 0, null: false
    t.string   "currency"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "shipping_address_first_name"
    t.string   "shipping_address_last_name"
    t.string   "shipping_address_address1"
    t.string   "shipping_address_address2"
    t.string   "shipping_address_city"
    t.string   "shipping_address_country"
    t.string   "shipping_address_province"
    t.string   "shipping_address_zipcode"
    t.integer  "shipping_same_as_billing",     default: 0
    t.string   "billing_address_company_name"
    t.string   "billing_address_first_name"
    t.string   "billing_address_last_name"
    t.string   "billing_address_address1"
    t.string   "billing_address_address2"
    t.string   "billing_address_city"
    t.string   "billing_address_country"
    t.string   "billing_address_province"
    t.string   "billing_address_zipcode"
    t.integer  "shipping_id"
    t.string   "shipping_name"
    t.integer  "shipping_cents",               default: 0, null: false
    t.integer  "total_cents",                  default: 0, null: false
    t.string   "tax1_name"
    t.string   "tax1_value"
    t.string   "tax1_number"
    t.integer  "tax1_cents",                   default: 0, null: false
    t.string   "tax2_name"
    t.string   "tax2_value"
    t.string   "tax2_number"
    t.integer  "tax2_cents",                   default: 0, null: false
    t.integer  "shipping_unit_price_cents",    default: 0, null: false
    t.index ["client_id"], name: "index_orders_on_client_id", using: :btree
  end

  create_table "post_canada_publipostages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "sector_zipcode",                    limit: 6
    t.string   "sector_name",                       limit: 30
    t.string   "sector_type",                       limit: 5
    t.string   "sector_qualification",              limit: 15
    t.string   "sector_short_name",                 limit: 15
    t.string   "sector_state_code",                 limit: 1
    t.string   "shipping_mode_type",                limit: 2
    t.string   "shipping_mode_id",                  limit: 4
    t.string   "shipping_road_id"
    t.string   "spot_appartments",                  limit: 5
    t.string   "spot_shops",                        limit: 5
    t.string   "spot_domiciles",                    limit: 5
    t.string   "spot_farms",                        limit: 5
    t.string   "consumer_choice_appartments",       limit: 5
    t.string   "consumer_choice_shops",             limit: 5
    t.string   "consumer_choice_domiciles",         limit: 5
    t.string   "consumer_choice_farms",             limit: 5
    t.string   "zipcode",                           limit: 6
    t.string   "zipcode_rta",                       limit: 3
    t.string   "zipcode_udl_type",                  limit: 2
    t.string   "itinerary_sollicitation_indicator", limit: 2
    t.string   "supervisor_zipcode",                limit: 6
    t.string   "supervisor_name",                   limit: 30
    t.string   "supervisor_shipping_name",          limit: 30
    t.string   "supervisor_type",                   limit: 5
    t.string   "supervisor_qualification",          limit: 15
    t.string   "supervisor_street_number",          limit: 6
    t.string   "supervisor_street_number_suffix",   limit: 1
    t.string   "supervisor_unit_number",            limit: 6
    t.string   "supervisor_street_name",            limit: 30
    t.string   "supervisor_street_type",            limit: 6
    t.string   "supervisor_cardinal_point",         limit: 2
    t.string   "supervisor_province",               limit: 2
    t.string   "supervisor_state_code",             limit: 1
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.index ["zipcode_rta"], name: "index_post_canada_publipostages_on_zipcode_rta", using: :btree
  end

  create_table "pricing_checkouts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "pricing_id"
    t.integer  "quantity_min", default: 1
    t.integer  "quantity_max"
    t.integer  "price_cents",  default: 0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["pricing_id"], name: "index_pricing_checkouts_on_pricing_id", using: :btree
  end

  create_table "pricing_plans", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "pricing_id"
    t.integer  "price_month_cents", default: 0, null: false
    t.integer  "price_year_cents",  default: 0, null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["pricing_id"], name: "index_pricing_plans_on_pricing_id", using: :btree
  end

  create_table "pricing_translations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "pricing_id",                null: false
    t.string   "locale",                    null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "name"
    t.text     "description", limit: 65535
    t.string   "warning"
    t.index ["locale"], name: "index_pricing_translations_on_locale", using: :btree
    t.index ["pricing_id"], name: "index_pricing_translations_on_pricing_id", using: :btree
  end

  create_table "pricings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "sku"
    t.integer  "product_id"
    t.integer  "checkout_unit_id"
    t.integer  "draft",                             default: 0
    t.string   "status",                            default: "active"
    t.integer  "position"
    t.integer  "is_ready",                          default: 1
    t.string   "name"
    t.text     "description",         limit: 65535
    t.integer  "quantity"
    t.float    "weight",              limit: 24
    t.integer  "price_from"
    t.integer  "shippable"
    t.boolean  "rechargeable",                      default: false
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "warning"
    t.boolean  "quantity_editable",                 default: true
    t.boolean  "price_to_home"
    t.boolean  "display_range_price"
    t.boolean  "display_unit_price",                default: false
    t.integer  "is_publipostage",                   default: 0
    t.index ["checkout_unit_id"], name: "index_pricings_on_checkout_unit_id", using: :btree
    t.index ["product_id"], name: "index_pricings_on_product_id", using: :btree
  end

  create_table "product_assets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "product_id"
    t.integer  "position"
    t.datetime "image_updated_at"
    t.integer  "image_file_size"
    t.string   "image_content_type"
    t.string   "image_file_name"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["product_id"], name: "index_product_assets_on_product_id", using: :btree
  end

  create_table "product_translations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "product_id",                null: false
    t.string   "locale",                    null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "baseline"
    t.text     "summary",     limit: 65535
    t.text     "description", limit: 65535
    t.string   "name"
    t.index ["locale"], name: "index_product_translations_on_locale", using: :btree
    t.index ["product_id"], name: "index_product_translations_on_product_id", using: :btree
  end

  create_table "product_users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "product_id"
    t.integer "client_id"
    t.index ["client_id"], name: "index_product_users_on_client_id", using: :btree
    t.index ["product_id"], name: "index_product_users_on_product_id", using: :btree
  end

  create_table "products", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "subcategory_id"
    t.integer  "company_id"
    t.integer  "draft"
    t.string   "sku"
    t.string   "name"
    t.string   "slug"
    t.boolean  "is_printable"
    t.integer  "print_template_id",                     default: 1
    t.integer  "validity_duration_month"
    t.text     "description",             limit: 65535
    t.integer  "notification_on_order"
    t.integer  "notification_on_use"
    t.string   "video_url"
    t.datetime "logo_updated_at"
    t.integer  "logo_file_size"
    t.string   "logo_content_type"
    t.string   "logo_file_name"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.text     "summary",                 limit: 65535
    t.string   "baseline"
    t.integer  "is_favorite"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.string   "logo_app_file_name"
    t.string   "logo_app_content_type"
    t.integer  "logo_app_file_size"
    t.datetime "logo_app_updated_at"
    t.string   "oauth_link"
    t.index ["store_id"], name: "index_products_on_store_id", using: :btree
    t.index ["subcategory_id"], name: "index_products_on_subcategory_id", using: :btree
  end

  create_table "profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.string   "company_name"
    t.string   "company_email"
    t.string   "company_website"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "shipping_address_address1"
    t.string   "shipping_address_address2"
    t.string   "shipping_address_city"
    t.string   "shipping_address_country"
    t.string   "shipping_address_province"
    t.string   "shipping_address_zipcode"
    t.integer  "shipping_same_as_billing",  default: 0
    t.string   "billing_address_address1"
    t.string   "billing_address_address2"
    t.string   "billing_address_city"
    t.string   "billing_address_country"
    t.string   "billing_address_province"
    t.string   "billing_address_zipcode"
    t.index ["client_id"], name: "index_profiles_on_client_id", using: :btree
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "sales", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.integer  "pricing_id"
    t.string   "code"
    t.string   "name"
    t.float    "percentage",      limit: 24
    t.datetime "start"
    t.datetime "end"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "amount_cents",               default: 0,            null: false
    t.string   "amount_currency",            default: "EUR",        null: false
    t.string   "sale_type",                  default: "percentage"
    t.index ["client_id"], name: "index_sales_on_client_id", using: :btree
  end

  create_table "shipping_mode_translations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "shipping_mode_id", null: false
    t.string   "locale",           null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "name"
    t.index ["locale"], name: "index_shipping_mode_translations_on_locale", using: :btree
    t.index ["shipping_mode_id"], name: "index_shipping_mode_translations_on_shipping_mode_id", using: :btree
  end

  create_table "shipping_modes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_shipping_modes_on_store_id", using: :btree
  end

  create_table "shippings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "shipping_mode_id"
    t.integer  "country_id"
    t.float    "max_weight",       limit: 24
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "price_cents",                 default: 0
    t.index ["country_id"], name: "index_shippings_on_country_id", using: :btree
    t.index ["shipping_mode_id"], name: "index_shippings_on_shipping_mode_id", using: :btree
  end

  create_table "store_domains", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "domain"
    t.integer  "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_store_domains_on_store_id", using: :btree
  end

  create_table "store_translations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id",   null: false
    t.string   "locale",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "baseline"
    t.index ["locale"], name: "index_store_translations_on_locale", using: :btree
    t.index ["store_id"], name: "index_store_translations_on_store_id", using: :btree
  end

  create_table "stores", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "country_id"
    t.integer  "position",                                    default: 1
    t.string   "name"
    t.string   "slug"
    t.string   "color1"
    t.string   "color2"
    t.string   "shareimmo_api_url"
    t.string   "shareimmo_api_key"
    t.string   "shareimmo_api_token"
    t.text     "theme",                         limit: 65535
    t.string   "baseline"
    t.text     "address",                       limit: 65535
    t.string   "company_registration_number"
    t.string   "contact_address"
    t.string   "contact_email"
    t.string   "contact_phone"
    t.text     "languages",                     limit: 65535
    t.string   "default_language"
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.integer  "allow_user_creation",                         default: 0
    t.boolean  "pay_without_connection",                      default: false
    t.boolean  "calculate_taxes_with_shipping",               default: true
    t.string   "payment_api_key"
    t.string   "payment_api_token"
    t.string   "payment_api_type",                            default: "stripe"
    t.index ["country_id"], name: "index_stores_on_country_id", using: :btree
  end

  create_table "subcategories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "category_id"
    t.string  "name"
  end

  create_table "subcategory_translations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "subcategory_id", null: false
    t.string   "locale",         null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "name"
    t.index ["locale"], name: "index_subcategory_translations_on_locale", using: :btree
    t.index ["subcategory_id"], name: "index_subcategory_translations_on_subcategory_id", using: :btree
  end

  create_table "taxes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "is_current_tax",            default: 0
    t.string   "name"
    t.string   "tax1_name"
    t.string   "tax1_number"
    t.float    "tax1_value",     limit: 24
    t.string   "tax2_name"
    t.string   "tax2_number"
    t.float    "tax2_value",     limit: 24
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["store_id"], name: "index_taxes_on_store_id", using: :btree
  end

  create_table "team_members", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "team_id"
    t.integer  "client_id"
    t.integer  "member_role",      default: 1
    t.integer  "status",           default: 0
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "invitation_token"
    t.index ["client_id"], name: "index_team_members_on_client_id", using: :btree
    t.index ["team_id"], name: "index_team_members_on_team_id", using: :btree
  end

  create_table "teams", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.string   "name"
    t.string   "company_name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["client_id"], name: "index_teams_on_client_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "company_id"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  create_table "variant_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "values"
    t.integer  "position"
    t.integer  "pricing_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "variant_categories_combinations", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "variant_category_id"
    t.integer "variant_combination_id"
    t.index ["variant_category_id"], name: "index_variant_categories_combinations_on_variant_category_id", using: :btree
    t.index ["variant_combination_id"], name: "index_variant_categories_combinations_on_variant_combination_id", using: :btree
  end

  create_table "variant_combinations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "sku"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "display",                     default: 1
    t.float    "price_percentage", limit: 24
  end

  create_table "wysiwyg_assets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "imageable_type"
    t.integer  "imageable_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.index ["imageable_type", "imageable_id"], name: "index_wysiwyg_assets_on_imageable_type_and_imageable_id", using: :btree
  end

  add_foreign_key "shipping_modes", "stores"
  add_foreign_key "store_domains", "stores"
end
