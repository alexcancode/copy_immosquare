Mypdflib.setup do |config|
  config.license        = (OS.mac? or OS.windows?) ?  nil : ENV['pdflib_license']
  config.stuff_folder   = "#{Rails.root}/app/fonts"
  config.ppi            = 250
end
