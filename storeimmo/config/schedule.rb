every 1.day, :at => '1:00 am' do
  rake "billing:recurrence"
end

every 1.day, :at => '3:00 am' do
  rake "cleaner:eraseCarts"
  rake "authorization:update_status"
end

every 1.week, :at => '3:00 am' do
  rake "cleaner:eraseTmpImages"
end