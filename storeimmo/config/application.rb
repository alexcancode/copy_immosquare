require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"


Bundler.require(*Rails.groups)

module ImmosquareStore
  class Application < Rails::Application

    ##============================================================##
    ## Date
    ##============================================================##
    config.time_zone = 'Eastern Time (US & Canada)'

    ##============================================================##
    ## The default locale is :en and all translations
    ## from config/locales/*.rb,yml are auto loaded.
    ##============================================================##
    config.i18n.default_locale = 'fr'
    config.i18n.available_locales = ['fr','en','fr_ca','es']
    config.i18n.fallbacks         = {
      "fr_ca".to_sym  => "fr",
      :fr             => :en,
      :es             => :en,
      :en             => :fr
    }

    ##============================================================##
    ## Setup Money Translations
    ##============================================================##
    Money.use_i18n = false


    ##============================================================##
    ## Load libs
    ##============================================================##
    config.autoload_paths << Rails.root.join('lib')

    ##============================================================##
    ## Config action_mailer
    ##============================================================##
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :address        => "smtp-relay.sendinblue.com",
      :port           => 587,
      :authentication => :plain,
      :user_name      => ENV['sendinblue_username'],
      :password       => ENV['sendingblue_key']
    }


    ##============================================================##
    ## Config paperclip S3_storage
    ##============================================================##
    config.paperclip_defaults = {
      :storage => :s3,
      :s3_protocol => :https,
      :s3_credentials => {
        :bucket             => ENV['aws_bucket'],
        :access_key_id      => ENV['aws_access_key_id'],
        :secret_access_key  => ENV['aws_secret_acces_key']
        },
        :s3_region => ENV["aws_region"]
      }


    ##============================================================##
    ## DoorKeeper Layout
    ##============================================================##
    config.to_prepare do
      Doorkeeper::ApplicationsController.layout           "back_office"
      Doorkeeper::AuthorizationsController.layout         "application"
      Doorkeeper::AuthorizedApplicationsController.layout "back_office"
    end


  end
end
