require 'sidekiq/web'

Rails.application.routes.draw do


  ##============================================================##
  ## DoorKeeper
  ##============================================================##
  use_doorkeeper

  ##============================================================##
  ## sidekiq
  ##============================================================##
  mount Sidekiq::Web => '/sidekiq'

  ##============================================================##
  ## API
  ##============================================================##
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      ##============================================================##
      ## oAuth
      ##============================================================##
      match 'me'              => "oauth#me",          :via => :get

      ##============================================================##
      ## Gateways
      ##============================================================##
      match '/gateways/:name'                   => "gateways#show",     :via => :get
      match '/gateways/track/:activation_id'    => "gateways#track",    :via => :post

      ##============================================================##
      ## Properites
      ##============================================================##
      match 'properties'      => "properties#index",  :via => :get
      match 'properties/:id'  => "properties#show",   :via => :get

      ##============================================================##
      ## Services
      ##============================================================##
      match 'services'                        => "services#index",    :via => :get
      match 'services/menu'                   => "services#menu",     :via => :get
      match 'services/debit/:id/:quantity'    => "services#debit",    :via => :post

      ##============================================================##
      ## Cart
      ##============================================================##
      scope 'cart' do
        match 'update_shipping'         => 'checkouts#update_shipping',  :via => :get
      end

      ##============================================================##
      ## Variants
      ##============================================================##
      scope 'variants' do
        match "/new/:variant_category_id"       => "variants#new",         :via => [:get]
        match "/:variant_category_id"           => "variants#delete",      :via => [:delete]
      end
    end
  end

  ##============================================================##
  ## All Apps Routes
  ##============================================================##
  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do
    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users
    devise_for :clients, :controllers => {
      :sessions       => "clients/sessions",
      :registrations  => "clients/registrations",
      :passwords      => "clients/passwords",
    }


    ##============================================================##
    ## HomePage
    ##============================================================##
    root "pages#home"
    match 'stylesheet'        => 'pages#stylesheet',        :via => [:get],   :format => [:css]
    match 'choose-store'      => 'pages#choose_store',      :via => [:get],   :as => :choose_your_store
    match "product/:id"       => "pages#product_show",      :via => [:get],   :as => :product_show
    match "category/(:id)"    => "pages#category_show",     :via => [:get],   :as => :category_show
    match "contact"           => "pages#contact",           :via => [:get],   :as => :contact
    match "contact"           => "pages#contact_create",    :via => [:post],  :as => :contact_create
    match "faq"               => "pages#faq",               :via => [:get],   :as => :faq
    match "thanks"            => "pages#thanks",            :via => [:get],   :as => :thanks


    ##============================================================##
    ## Team Invitation
    ##============================================================##
    match "invitation"      => "invitations#invitation",          :via => [:get],    :as => :invitation
    match "invitation"      => "invitations#invitation_accept",   :via => [:post],   :as => :invitation_accept

    ##============================================================##
    ## Wysiwyg
    ##============================================================##
    match 'wysiwyg/pictures'        => 'application#wysiwyg_picture_manager',via: [:post] ,:as => 'wysiwyg_picture_manager', :format => "json"
    match 'wysiwyg/pictures/d'      => 'application#wysiwyg_picture_delete',via: [:post] ,:as => 'wysiwyg_picture_delete'

    ##============================================================##
    ## Publipostage
    ##============================================================##
    match 'publipostage'            => "publipostage#find_routes", :via => [:post], :as => "publipostage"

    ##============================================================##
    ## Dashboard
    ##============================================================##
    scope "dashboard" do
      match "flow"                => "dashboard#flow",                :via => [:get],   :as => :dashboard_flow
      ##============================================================##
      ## Authorizations
      ##============================================================##
      match "/"                   => "dashboard#index",               :via => [:get],     :as => :dashboard
      match "plans"               => "dashboard#plans",               :via => [:get],     :as => :dashboard_plans
      match "tokens"              => "dashboard#tokens",              :via => [:get],     :as => :dashboard_tokens
      match "purchases"           => "dashboard#purchases",           :via => [:get],     :as => :dashboard_purchases
      match "olds"                => "dashboard#old_authorizations",  :via => [:get],     :as => :dashboard_old_authorizations
      ##============================================================##
      ##
      ##============================================================##
      match "checkout/edit/:pricing_id"  => "dashboard#checkout_edit",           :via => [:get],     :as => :dashboard_checkout_edit
      match "service/:id/cancel"         => "dashboard#cancel",              :via => [:delete],  :as => :dashboard_cancel
      ##============================================================##
      ## Shareimmo
      ##============================================================##
      match "shareimmo"           => "dashboard#shareimmo_signin",    :via => [:get],   :as => :dashboard_shareimmo
      ##============================================================##
      ## Billing
      ##============================================================##
      match "billing"             => "dashboard#billing",             :via => [:get],   :as => :dashboard_billing
      match "billing/:id"         => "dashboard#billing_view",        :via => [:get],   :as => :dashboard_billing_view
      match "bill/:id"            => "dashboard#redirect_to_bill",    :via => :get,     :as => :dashboard_redirect_to_bill
      match "bill/regenerate/:id" => "dashboard#regenerate_bill",     :via => :get,     :as => :dashboard_regenerate_bill
      ##============================================================##
      ## Team
      ##============================================================##
      match "team"                => "dashboard#team",                   :via => [:get],      :as => :dashboard_team
      match "team-member/new"     => "dashboard#team_member_create",     :via => [:post],     :as => :dashboard_team_member_create
      match "team-member/:id"     => "dashboard#team_member_resend",     :via => [:get],      :as => :dashboard_team_member_resend
      match "team-member/:id"     => "dashboard#team_member_delete",     :via => [:delete],   :as => :dashboard_team_member_delete
      match "team/switch/:id"     => "dashboard#change_my_team",         :via => [:post],     :as => :dashboard_change_my_team
      ##============================================================##
      ## Profile
      ##============================================================##
      match "profile"       => "dashboard#profile",         :via => [:get],     :as => :dashboard_profile
      match "profile/:id"   => "dashboard#profile_update",  :via => [:patch],   :as => :dashboard_profile_update
      ##============================================================##
      ## Credit cards
      ##============================================================##
      match "cards"             => "credit_cards#index",            :via => [:get],    :as => :dashboard_cards
      match "card"              => "credit_cards#card_new",         :via => [:post],   :as => :dashboard_card_new
      match "card/:stripe_id"   => "credit_cards#card_default",     :via => [:post],   :as => :dashboard_card_default
      match "card/:stripe_id"   => "credit_cards#card_delete",      :via => [:delete], :as => :dashboard_card_delete
      ##============================================================##
      ## Gateway
      ##============================================================##
      match "gateway"             => "dashboard#gateway",            :via => [:get],    :as => :dashboard_gateway
      match "gateway"             => "dashboard#gateway_create",     :via => [:post],   :as => :dashboard_gateway_create
      match "gateway/:id"         => "dashboard#gateway_update",     :via => [:patch],  :as => :dashboard_gateway_update
      match "gateway/:id"         => "dashboard#gateway_delete",     :via => [:delete], :as => :dashboard_gateway_delete
      match "gateway/:id"         => "dashboard#gateway_sync",       :via => [:get],    :as => :dashboard_gateway_sync

    end


    ##============================================================##
    ## Monitoring
    ##============================================================##
    scope "monitoring" do
      match ""         => "back_office#index",           :via => [:get],   :as => :back_office
      scope "store/:store_id" do
        ##============================================================##
        ## Store
        ##============================================================##
        match ""     => "back_office#store_edit",      :via => [:get],   :as => :back_office_stores_edit
        match ""     => "back_office#store_update",    :via => [:patch], :as => :back_office_stores_update
        ##============================================================##
        ## Bills
        ##============================================================##
        match "bills"              => "back_office#bills",         :via => [:get],     :as => :back_office_bills
        match "bills/send/:id"     => "back_office#bills_send",    :via => [:get],     :as => :back_office_bills_send
        ##============================================================##
        ## Taxes
        ##============================================================##
        match "taxes"       => "back_office#taxes",         :via => [:get],     :as => :back_office_taxes
        match "taxes"       => "back_office#taxes_new",     :via => [:post],    :as => :back_office_taxes_new
        match "taxes/:id"   => "back_office#taxes_update",  :via => [:patch],   :as => :back_office_taxes_update
        match "taxes/:id"   => "back_office#taxes_delete",  :via => [:delete],  :as => :back_office_taxes_delete
        match "taxes/:id"   => "back_office#taxes_current", :via => [:get],     :as => :back_office_taxes_current
        ##============================================================##
        ## Categories
        ##============================================================##
        match "categories"          => "back_office#categories",            :via => [:get],     :as => :back_office_categories
        match "categories"          => "back_office#categories_create",     :via => [:post],    :as => :back_office_categories_create
        match "categories/sort"     => "back_office#categories_sort",       :via => [:post],    :as => :back_office_categories_sort
        match "categories/:id"      => "back_office#categories_update",     :via => [:patch],   :as => :back_office_categories_update
        match "categories/:id"      => "back_office#categories_delete",     :via => [:delete],  :as => :back_office_categories_delete
        match "subcategories"       => "back_office#subcategories_create",  :via => [:post],    :as => :back_office_subcategories_create
        match "subcategories/:id"   => "back_office#subcategories_update",  :via => [:patch],   :as => :back_office_subcategories_update
        match "subcategories/:id"   => "back_office#subcategories_delete",  :via => [:delete],  :as => :back_office_subcategories_delete
        ##============================================================##
        ## Companies
        ##============================================================##
        match "companies"            => "back_office#companies",             :via => [:get],            :as => :back_office_companies
        match "companies/(:id)"      => "back_office#companies_update",      :via => [:patch, :post],   :as => :back_office_companies_update
        match "companies/:id"        => "back_office#companies_delete",      :via => [:delete],         :as => :back_office_companies_delete
        ##============================================================##
        ## Shippings
        ##============================================================##
        match "shippings"             => "back_office#shippings",               :via => [:get],       :as => :back_office_shippings
        match "shippings"             => "back_office#shippings_create",        :via => [:post],      :as => :back_office_shippings_create
        match "shippings/:id/edit"    => "back_office#shippings_edit",          :via => [:get],       :as => :back_office_shippings_edit
        match "shippings/:id"         => "back_office#shippings_update",        :via => [:patch],     :as => :back_office_shippings_update
        match "shippings/:id"         => "back_office#shippings_delete",        :via => [:delete],    :as => :back_office_shippings_delete
        match "shippings-mode"        => "back_office#shippings_mode_create",   :via => [:post],      :as => :back_office_shippings_mode_create
        match "shippings-mode/:id"    => "back_office#shippings_mode_delete",   :via => [:delete],    :as => :back_office_shippings_mode_delete
        ##============================================================##
        ## Checkouts
        ##============================================================##
        match "checkouts"                 => "back_office#checkouts",                  :via => [:get],            :as => :back_office_checkouts
        match "checkout_units/(:id)"      => "back_office#checkout_units_update",      :via => [:patch, :post],   :as => :back_office_checkout_units_update
        match "checkout_units/:id"        => "back_office#checkout_units_delete",      :via => [:delete],         :as => :back_office_checkout_units_delete

        scope "products" do
          ##============================================================##
          ## Products
          ##============================================================##
          match ""                  => "back_office#products",            :via => [:get],           :as => :back_office_products
          match "new/(:type)"       => "back_office#products_new",        :via => [:get],           :as => :back_office_products_new
          match ":id/edit"          => "back_office#products_edit",       :via => [:get],           :as => :back_office_products_edit
          match ":id"               => "back_office#products_update",     :via => [:patch],         :as => :back_office_products_update
          match ":id/duplicate/:new_store_id"  => "back_office#products_duplicate",  :via => [:get],           :as => :back_office_products_duplicate
          match ":id"               => "back_office#products_delete",     :via => [:delete],        :as => :back_office_products_delete
          ##============================================================##
          ## Pricings
          ##============================================================##
          match ":product_id/pricings"                    => "back_office#pricings",              :via => [:get],          :as => :back_office_pricings
          match ":product_id/pricings/new"                => "back_office#pricings_new",          :via => [:get],          :as => :back_office_pricings_new
          match ":product_id/pricings/sort"               => "back_office#pricings_sort",         :via => [:post],         :as => :back_office_pricings_sort
          match ":product_id/pricings"                    => "back_office#pricings_create",       :via => [:post],         :as => :back_office_pricings_create
          match ":product_id/pricings/:id"                => "back_office#pricings_edit",         :via => [:get],          :as => :back_office_pricings_edit
          match ":product_id/pricings/:id"                => "back_office#pricings_update",       :via => [:patch],        :as => :back_office_pricings_update
          match ":product_id/pricings/:id/duplicate"      => "back_office#pricings_duplicate",    :via => [:get],          :as => :back_office_pricings_duplicate
          match ":product_id/pricings/:id"                => "back_office#pricings_delete",       :via => [:delete],       :as => :back_office_pricings_delete
          scope ":product_id/pricings/:pricing_id" do
            ##============================================================##
            ## Pricing Price
            ##============================================================##
            match "prices"                     => "back_office#pricing_prices",                :via => [:get],         :as => :back_office_pricing_prices
            match "prices/checkouts/new"       => "back_office#pricing_checkouts_new",         :via => [:get],         :as => :back_office_pricing_checkouts_new
            match "prices/checkouts"           => "back_office#pricing_checkouts_create",      :via => [:post],        :as => :back_office_pricing_checkouts_create
            match "prices/checkouts/:id"       => "back_office#pricing_checkouts_edit",        :via => [:get],         :as => :back_office_pricing_checkouts_edit
            match "prices/checkouts/:id"       => "back_office#pricing_checkouts_update",      :via => [:patch],       :as => :back_office_pricing_checkouts_update
            match "prices/checkouts/:id"       => "back_office#pricing_checkouts_delete",      :via => [:delete],      :as => :back_office_pricing_checkouts_delete
            match "pricing_plans/:id"          => "back_office#pricing_plans_edit",            :via => [:get],         :as => :back_office_pricing_plans_edit
            match "pricing_plans/:id"          => "back_office#pricing_plans_update",          :via => [:patch],       :as => :back_office_pricing_plans_update
            ##============================================================##
            ## Extras
            ##============================================================##
            match "extras"           => "back_office#extras",              :via => [:get],         :as => :back_office_extras
            match "extras/new"       => "back_office#extras_new",          :via => [:get],         :as => :back_office_extras_new
            match "extras"           => "back_office#extras_create",       :via => [:post],        :as => :back_office_extras_create
            match "extras/:id"       => "back_office#extras_edit",         :via => [:get],         :as => :back_office_extras_edit
            match "extras/:id"       => "back_office#extras_update",       :via => [:patch],       :as => :back_office_extras_update
            match "extras/:id"       => "back_office#extras_delete",       :via => [:delete],      :as => :back_office_extras_delete
            ##============================================================##
            ## Sales
            ##============================================================##
            match "sales"            => "back_office#sales",          :via => [:get],          :as => :back_office_sales
            match "sales/new"        => "back_office#sales_new",      :via => [:get],          :as => :back_office_sales_new
            match "sales"            => "back_office#sales_create",   :via => [:post],         :as => :back_office_sales_create
            match "sales/:id"        => "back_office#sales_edit",     :via => [:get],          :as => :back_office_sales_edit
            match "sales/:id"        => "back_office#sales_update",   :via => [:patch],        :as => :back_office_sales_update
            match "sales/:id"        => "back_office#sales_delete",   :via => [:delete],       :as => :back_office_sales_delete
            ##============================================================##
            ## Variants
            ##============================================================##
            match "variants"          => "back_office#variants",              :via => [:get],         :as => :back_office_variants
            match "variants/new"      => "back_office#variants_new",          :via => [:get],         :as => :back_office_variants_new
            match "variants/sort"     => "back_office#variants_sort",         :via => [:post],        :as => :back_office_variants_sort
            match "variants"          => "back_office#variants_create",       :via => [:post],        :as => :back_office_variants_create
            match "variants/:id"      => "back_office#variants_edit",         :via => [:get],         :as => :back_office_variants_edit
            match "variants/:id"      => "back_office#variants_update",       :via => [:patch],       :as => :back_office_variants_update
            match "variants/:id"      => "back_office#variants_delete",       :via => [:delete],      :as => :back_office_variants_delete
          end
        end
      end

      ##============================================================##
      ## Combinations
      ##============================================================##
      match "combinations/:pricing_id"         => "back_office#combinations",             :via => [:get],          :as => :back_office_combinations
      match "combinations/:pricing_id"         => "back_office#combinations_update",      :via => [:patch],        :as => :back_office_combinations_update
      ##============================================================##
      ## Visibilities
      ##============================================================##
      match "/product_:id/visibilities"       => "back_office#visibilities",          :via => [:get],         :as => :back_office_visibilities
      match "visibilities/:product_id"        => "back_office#visibilities_add",      :via => [:post],        :as => :back_office_visibilities_add
      match "visibilities/:product_id"        => "back_office#visibilities_delete",   :via => [:delete],      :as => :back_office_visibilities_delete
      ##============================================================##
      ## Users
      ##============================================================##
      match "users"              => "back_office#users",          :via => [:get],           :as => :back_office_users
      match "users"              => "back_office#users_create",   :via => [:post],          :as => :back_office_users_create
      match "users/:id"          => "back_office#users_update",   :via => [:patch],         :as => :back_office_users_update
      match "users/:id"          => "back_office#users_delete",   :via => [:delete],        :as => :back_office_users_delete
      ##============================================================##
      ## Product Assets
      ##============================================================##
      match 'products/:id/assets'        => 'back_office#product_asset_create',  via: [:post],     :as => :back_office_product_asset_create
      match 'products/:id/assets'        => 'back_office#product_asset_delete',  via: [:delete],   :as => :back_office_product_asset_delete
      match "products/:id/sort"          => "back_office#product_asset_sort",    :via => [:post],  :as => :back_office_product_asset_sort
    end

    ##============================================================##
    ## Order
    ##============================================================##
    scope "order" do
      match "add_sale/:cart_item_id"              => "orders#add_sale",        :via => :post,   :as => :cart_add_sale
      match "add_or_update/(:cart_item_id)"       => "orders#add_or_update",  :via => :post,    :as => :cart_add_or_update
      match "remove/:cart_item_id"                => "orders#remove",         :via => :delete,  :as => :cart_remove
      match "display_update/:cart_item_id"        => "orders#display_update", :via => :get,     :as => :cart_display_update
      match "empty"                               => "orders#empty", :via => :get, :as => :cart_empty
    end

    ##============================================================##
    ## Cart
    ##============================================================##
    scope "cart" do
      match "" => "checkouts#view_cart", :via => :get, :as => :checkout_cart
      match "infos" => "checkouts#infos", :via => :get, :as => :checkout_infos
      match "checkout" => "checkouts#checkout", :via => :patch, :as => :checkout_checkout
    end

    ##============================================================##
    ## Users
    ##============================================================##
    scope "users" do
      match "edit_password"      => "users#edit_password",          :via => [:get],          :as => :edit_password
      match "update_password"    => "users#update_password",        :via => [:patch],        :as => :update_password
    end
  end


end
