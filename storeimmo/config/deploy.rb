set :application, "storeimmo"
set :repo_url, "git@bitbucket.org:wgroupe/storeimmo.git"
set :bundle_roles, :all
set :linked_dirs, %w{log bin tmp/pids tmp/sockets}
set :linked_files, %w{config/database.yml}
set :keep_releases, 6
set :whenever_identifier, ->{ "storeimmo_#{fetch(:stage)}" }
