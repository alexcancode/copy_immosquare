# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160527201200) do

  create_table "ad_support_details", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.integer  "ad_support_id",        limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "draft",                limit: 4,   default: 0
    t.string   "picture_file_name",    limit: 255
    t.string   "picture_content_type", limit: 255
    t.integer  "picture_file_size",    limit: 4
    t.datetime "picture_updated_at"
  end

  add_index "ad_support_details", ["ad_support_id"], name: "index_ad_support_details_on_ad_support_id", using: :btree

  create_table "ad_supports", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "draft",              limit: 4,   default: 0
    t.integer  "ad_support_type_id", limit: 4
  end

  add_index "ad_supports", ["ad_support_type_id"], name: "index_ad_supports_on_ad_support_type_id", using: :btree

  create_table "ads", force: :cascade do |t|
    t.integer  "project_id",           limit: 4
    t.integer  "ad_support_id",        limit: 4
    t.integer  "ad_support_detail_id", limit: 4
    t.integer  "ad_season",            limit: 4
    t.date     "date_from"
    t.date     "date_to"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "dimensions",           limit: 255
    t.string   "event_name",           limit: 255
    t.integer  "price_cents",          limit: 4,   default: 0,     null: false
    t.string   "price_currency",       limit: 255, default: "EUR", null: false
  end

  add_index "ads", ["ad_support_detail_id"], name: "index_ads_on_ad_support_detail_id", using: :btree
  add_index "ads", ["ad_support_id"], name: "index_ads_on_ad_support_id", using: :btree
  add_index "ads", ["project_id"], name: "index_ads_on_project_id", using: :btree

  create_table "api_providers", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "authentifications", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "project_id",      limit: 4
    t.integer  "api_provider_id", limit: 4
    t.string   "uid",             limit: 255
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255
    t.string   "first_name",      limit: 255
    t.string   "last_name",       limit: 255
    t.string   "nickname",        limit: 255
    t.string   "image",           limit: 255
    t.string   "token",           limit: 255
    t.string   "token_secret",    limit: 255
    t.string   "refresh_token",   limit: 255
    t.string   "expires_at",      limit: 255
    t.integer  "expires",         limit: 4
    t.string   "location",        limit: 255
    t.string   "link",            limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "authentifications", ["api_provider_id"], name: "index_authentifications_on_api_provider_id", using: :btree
  add_index "authentifications", ["project_id"], name: "index_authentifications_on_project_id", using: :btree
  add_index "authentifications", ["user_id"], name: "index_authentifications_on_user_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "full_name",  limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.text     "content",    limit: 65535
    t.integer  "newsletter", limit: 4,     default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "genders", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "status",               limit: 4
    t.integer  "draft",                limit: 4,     default: 0
    t.string   "title",                limit: 255
    t.string   "slug",                 limit: 255
    t.text     "summary",              limit: 65535
    t.text     "content",              limit: 65535
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "picture_file_name",    limit: 255
    t.string   "picture_content_type", limit: 255
    t.integer  "picture_file_size",    limit: 4
    t.datetime "picture_updated_at"
  end

  create_table "project_assets", force: :cascade do |t|
    t.integer  "project_id",         limit: 4
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "project_assets", ["project_id"], name: "index_project_assets_on_project_id", using: :btree

  create_table "project_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "projects", force: :cascade do |t|
    t.integer  "sport_id",                    limit: 4
    t.integer  "user_id",                     limit: 4
    t.string   "secure_id",                   limit: 255
    t.integer  "project_type_id",             limit: 4
    t.integer  "gender_id",                   limit: 4,     default: 1
    t.string   "title",                       limit: 255
    t.text     "summary",                     limit: 65535
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.integer  "sport_category_id",           limit: 4
    t.integer  "project_type",                limit: 4,     default: 1
    t.integer  "draft",                       limit: 4,     default: 0
    t.string   "picture_file_name",           limit: 255
    t.string   "picture_content_type",        limit: 255
    t.integer  "picture_file_size",           limit: 4
    t.datetime "picture_updated_at"
    t.text     "palmares",                    limit: 65535
    t.string   "facebook",                    limit: 255
    t.string   "twitter",                     limit: 255
    t.string   "youtube",                     limit: 255
    t.integer  "sport_level_id",              limit: 4
    t.string   "address",                     limit: 255
    t.float    "latitude",                    limit: 24
    t.float    "longitude",                   limit: 24
    t.string   "google_place_id",             limit: 255
    t.string   "zipcode",                     limit: 255
    t.string   "street_number",               limit: 255
    t.string   "street_name",                 limit: 255
    t.string   "sublocality",                 limit: 255
    t.string   "locality",                    limit: 255
    t.string   "administrative_area_level_2", limit: 255
    t.string   "administrative_area_level_1", limit: 255
    t.string   "country",                     limit: 255
    t.string   "website",                     limit: 255
  end

  add_index "projects", ["gender_id"], name: "index_projects_on_gender_id", using: :btree
  add_index "projects", ["project_type_id"], name: "index_projects_on_project_type_id", using: :btree
  add_index "projects", ["sport_category_id"], name: "index_projects_on_sport_category_id", using: :btree
  add_index "projects", ["sport_id"], name: "index_projects_on_sport_id", using: :btree
  add_index "projects", ["sport_level_id"], name: "index_projects_on_sport_level_id", using: :btree
  add_index "projects", ["user_id"], name: "index_projects_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "sport_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "draft",      limit: 4,   default: 0
  end

  create_table "sport_levels", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "draft",      limit: 4,   default: 0
  end

  create_table "sport_univers", force: :cascade do |t|
    t.integer  "draft",                limit: 4,   default: 0
    t.string   "name",                 limit: 255
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "picture_file_name",    limit: 255
    t.string   "picture_content_type", limit: 255
    t.integer  "picture_file_size",    limit: 4
    t.datetime "picture_updated_at"
  end

  create_table "sports", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "draft",           limit: 4,   default: 0
    t.integer  "sport_univer_id", limit: 4
  end

  add_index "sports", ["sport_univer_id"], name: "index_sports_on_sport_univer_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "wysiwyg_assets", force: :cascade do |t|
    t.integer  "imageable_id",         limit: 4
    t.string   "imageable_type",       limit: 255
    t.string   "picture_file_name",    limit: 255
    t.string   "picture_content_type", limit: 255
    t.integer  "picture_file_size",    limit: 4
    t.datetime "picture_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_foreign_key "projects", "sports"
  add_foreign_key "projects", "users"
end
