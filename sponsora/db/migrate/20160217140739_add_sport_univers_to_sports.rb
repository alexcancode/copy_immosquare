class AddSportUniversToSports < ActiveRecord::Migration
  def change
    add_reference :sports, :sport_univer, index: true, foreign_key: false
  end
end
