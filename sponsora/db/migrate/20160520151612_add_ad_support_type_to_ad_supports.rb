class AddAdSupportTypeToAdSupports < ActiveRecord::Migration
  def change
    add_reference :ad_supports, :ad_support_type, index: true, foreign_key: false
  end
end
