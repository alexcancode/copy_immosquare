class AddMoneyToAds < ActiveRecord::Migration
  def change
    remove_column :ads, :amount
    add_money :ads, :price
  end
end
