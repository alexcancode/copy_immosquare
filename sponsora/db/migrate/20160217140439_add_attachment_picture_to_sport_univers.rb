class AddAttachmentPictureToSportUnivers < ActiveRecord::Migration
  def self.up
    change_table :sport_univers do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :sport_univers, :picture
  end
end
