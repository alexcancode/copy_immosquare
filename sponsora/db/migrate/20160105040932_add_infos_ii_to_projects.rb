class AddInfosIiToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :palmares, :text
    add_column :projects, :facebook, :string
    add_column :projects, :twitter, :string
    add_column :projects, :youtube, :string
  end
end
