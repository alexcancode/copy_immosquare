class CreateProjets < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.references :sport, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :secure_id
      t.string :title
      t.text :summary

      t.timestamps null: false
    end
  end
end
