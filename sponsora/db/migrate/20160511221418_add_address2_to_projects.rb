class AddAddress2ToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :latitude, :float
    add_column :projects, :longitude, :float
    add_column :projects, :google_place_id, :string
    add_column :projects, :zipcode, :string
    add_column :projects, :street_number, :string
    add_column :projects, :street_name, :string
    add_column :projects, :sublocality, :string
    add_column :projects, :locality, :string
    add_column :projects, :administrative_area_level_2, :string
    add_column :projects, :administrative_area_level_1, :string
    add_column :projects, :country, :string
  end
end
