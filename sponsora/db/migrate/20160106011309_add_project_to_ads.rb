class AddProjectToAds < ActiveRecord::Migration
  def change
    add_reference :ads, :project, index: true, foreign_key: false, :after => :id
  end
end
