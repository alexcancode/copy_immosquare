class AddAttachmentPictureToAdSupportDetails < ActiveRecord::Migration
  def self.up
    change_table :ad_support_details do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :ad_support_details, :picture
  end
end
