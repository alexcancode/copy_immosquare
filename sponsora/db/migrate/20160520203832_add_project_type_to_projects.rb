class AddProjectTypeToProjects < ActiveRecord::Migration
  def change
    add_reference :projects, :project_type, index: true, foreign_key: false, :after => :secure_id
  end
end
