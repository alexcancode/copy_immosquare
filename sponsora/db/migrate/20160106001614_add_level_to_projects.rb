class AddLevelToProjects < ActiveRecord::Migration
  def change
    add_reference :projects, :sport_level, index: true, foreign_key: false
  end
end
