class CreateAdSupportDetails < ActiveRecord::Migration
  def change
    create_table :ad_support_details do |t|
      t.string :name
      t.references :ad_support, index: true, foreign_key: false

      t.timestamps null: false
    end
  end
end
