class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :full_name
      t.string :email
      t.string :phone
      t.text :content
      t.integer :newsletter, :default => 0
      t.timestamps null: false
    end
  end
end
