class AddDraftToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :draft, :integer, :default => 0
  end
end
