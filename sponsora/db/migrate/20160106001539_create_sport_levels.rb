class CreateSportLevels < ActiveRecord::Migration
  def change
    create_table :sport_levels do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
