class AddGenderToProjects < ActiveRecord::Migration
  def change
    add_reference :projects, :gender, index: true, foreign_key: false, :default => 1, :after => :secure_id
  end
end
