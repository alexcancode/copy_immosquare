class CreateAuthentifications < ActiveRecord::Migration
  def change
    create_table :authentifications do |t|
      t.references :user,  index: true, foreign_key: false
      t.references :project, index: true, foreign_key: false
      t.references :api_provider, index: true, foreign_key: false
      t.string :uid
      t.string :name
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :nickname
      t.string :image
      t.string :token
      t.string :token_secret
      t.string :refresh_token
      t.string :expires_at
      t.integer :expires
      t.string :location
      t.string :link

      t.timestamps null: false
    end
  end
end
