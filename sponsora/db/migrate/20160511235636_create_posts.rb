class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :status
      t.integer :draft, :default => 0
      t.string :title
      t.string :slug
      t.text :summary
      t.text :content

      t.timestamps null: false
    end
  end
end
