class CreateProjectAssets < ActiveRecord::Migration
  def change
    create_table :project_assets do |t|
      t.references :project, index: true, foreign_key: false
      t.attachment :image

      t.timestamps
    end
  end
end
