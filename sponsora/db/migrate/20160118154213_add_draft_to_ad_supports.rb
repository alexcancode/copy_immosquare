class AddDraftToAdSupports < ActiveRecord::Migration
  def change
    add_column :ad_supports, :draft, :integer, :default => 0
    add_column :ad_support_details, :draft, :integer, :default => 0
    add_column :sports, :draft, :integer, :default => 0
    add_column :sport_levels, :draft, :integer, :default => 0
    add_column :sport_categories, :draft, :integer, :default => 0
  end
end
