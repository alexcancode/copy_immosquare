class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.references :ad_support, index: true, foreign_key: false
      t.references :ad_support_detail, index: true, foreign_key: false
      t.integer :ad_season
      t.date :date_from
      t.date :date_to
      t.integer :amount

      t.timestamps null: false
    end
  end
end
