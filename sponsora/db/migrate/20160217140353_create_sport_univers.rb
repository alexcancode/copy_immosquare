class CreateSportUnivers < ActiveRecord::Migration
  def change
    create_table :sport_univers do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
