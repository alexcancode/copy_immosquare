class AddInfosToProjects < ActiveRecord::Migration
  def change
    add_reference :projects, :sport_category, index: true, foreign_key: false
    add_column :projects, :project_type, :integer, :default => 1
  end
end
