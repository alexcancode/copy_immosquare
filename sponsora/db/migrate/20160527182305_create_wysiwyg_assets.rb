class CreateWysiwygAssets < ActiveRecord::Migration
  def change
    create_table :wysiwyg_assets do |t|
      t.references :imageable, polymorphic: true

      t.attachment :picture

      t.timestamps
    end
  end
end
