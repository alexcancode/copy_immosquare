Rails.application.config.middleware.use OmniAuth::Builder do

  ##============================================================##
  ## Facebook
  ##============================================================##
  provider :facebook, ENV['facebook_app_id'], ENV['facebook_app_secret'],
  {
    :scope=>'email,public_profile,user_friends,publish_pages,manage_pages',
    :display => 'page',
    :image_size=> 'square',
    :secure_image_url => true
  }


  ##============================================================##
  ## Twitter
  ##============================================================##
  provider :twitter, ENV['twitter_api_key'], ENV['twitter_api_key_secret'],
  {
    :secure_image_url => true,
    :image_size => 'bigger',
  }


  ##============================================================##
  ## Google/Youtube
  ##============================================================##
  provider :google_oauth2, ENV['google_app_id_key'], ENV['google_app_secret_key'],
  {
    :scope => "email,profile,youtube",
    :access_type => "offline",
    :image_aspect_ratio => "square",
    :image_size => 50,
    :prompt => "consent",
    :include_granted_scopes => true,
    :grant_type => 'refresh_token'
  }


  ##============================================================##
  ## Instagram
  ##============================================================##
  provider :instagram, ENV['instagram_client_id'], ENV['instagram_client_secret'],
  {
    :scope => "follower_list public_content comments relationships likes"
  }




end


OmniAuth.config.on_failure do |env|
  error_type = env['omniauth.error.type']
  new_path = "#{env['SCRIPT_NAME']}#{OmniAuth.config.path_prefix}/failure?message=#{error_type}"
  [301, {'Location' => new_path, 'Content-Type' => 'text/html'}, []]
end
