Yt.configure do |config|
  config.client_id      = ENV['google_app_id_key']
  config.client_secret  = ENV['google_app_secret_key']
  config.log_level      = :debug
end
