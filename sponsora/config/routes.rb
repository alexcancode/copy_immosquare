Rails.application.routes.draw do

  ##============================================================##
  ## OAuth
  ##============================================================##
  match 'auth/:provider/callback'     => 'authentifications#create',    :via => [:get]
  match 'auth/failure'                => 'authentifications#fail',      :via => [:get]
  match 'auth/provider/:id'           => "authentifications#delete",    :via => :delete,  :as => :dashboard_delete_provider



  ##============================================================##
  ## Api
  ##============================================================##
  namespace :api, defaults: {format: 'json'} do
    ##============================================================##
    ## API V1
    ##============================================================##
    namespace :v1 do
      ##============================================================##
      ## Search
      ##============================================================##
      match 'search/projects'   => 'searchs#projects', :via => :get
    end
  end

  ##============================================================##
  ## Wysiwyg
  ##============================================================##
  match 'wysiwyg/pictures'        => 'application#wysiwyg_picture_manager',via: [:post] ,:as => 'wysiwyg_picture_manager', :format => "json"
  match 'wysiwyg/pictures/d'      => 'application#wysiwyg_picture_delete',via: [:post] ,:as => 'wysiwyg_picture_delete'



  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do

    ##============================================================##
    ## Devise
    ##============================================================##
    devise_for :users, :controllers => {
      :registrations => 'registrations'
    }

    root 'pages#home'
    ##============================================================##
    ## Statics Pags
    ##============================================================##
    match "about"         => "pages#about",           :via => :get,   :as => "about"
    match "explore"       => "pages#explore",         :via => :get,   :as => "explore"
    match "explore/:id"   => "pages#explore_show",    :via => :get,   :as => "explore_show"
    match 'contact'       => 'pages#contact_create',  :via => :post,  :as => "create_contact"
    match 'blog'          => "pages#blog",            :via => :get,   :as =>:blog
    match 'blog/:id'      => "pages#blog_show",       :via => :get,   :as =>:blog_show
    match 'terms'         => "pages#terms",           :via => :get,   :as =>:terms
    match 'policy'        => "pages#policy",          :via => :get,   :as =>:policy


    ##============================================================##
    ## Admin
    ##============================================================##
    scope 'admin' do
      match ''                        => "admin#index",                 :via => :get,   :as => :admin
      match 'ad_supports'             => "admin#ad_supports",           :via => :get,   :as => :admin_ad_supports
      match 'ad_support_details'      => "admin#ad_support_details",    :via => :get,   :as => :admin_ad_support_details
      match 'sports_univers'          => "admin#sport_univers",         :via => :get,   :as => :admin_sport_univers
      match 'sports'                  => "admin#sports",                :via => :get,   :as => :admin_sports
      match 'sports/levels'           => "admin#sport_levels",          :via => :get,   :as => :admin_sport_levels
      match 'sports/categories'       => "admin#sport_categories",      :via => :get,   :as => :admin_sport_categories
      match 'posts'                   => "admin#posts",                 :via => :get,   :as => :admin_posts
      ##============================================================##
      ## Ad Supports
      ##============================================================##
      match 'ad_support/new' => "admin#ad_support_new",               :via => :get,     :as => :ad_support_new
      match 'ad_support/:id' => "admin#ad_support_edit",              :via => :get,     :as => :ad_support_edit
      match 'ad_support/:id' => "admin#ad_support_delete",            :via => :delete,  :as => :ad_support_delete
      match 'ad_support/:id' => "admin#ad_support_update",            :via => :patch,   :as => :ad_support_update
      ##============================================================##
      ## Ad Support Details
      ##============================================================##
      match 'ad_support_detail/new' => "admin#ad_support_detail_new",               :via => :get,     :as => :ad_support_detail_new
      match 'ad_support_detail/:id' => "admin#ad_support_detail_edit",              :via => :get,     :as => :ad_support_detail_edit
      match 'ad_support_detail/:id' => "admin#ad_support_detail_delete",            :via => :delete,  :as => :ad_support_detail_delete
      match 'ad_support_detail/:id' => "admin#ad_support_detail_update",            :via => :patch,   :as => :ad_support_detail_update
      ##============================================================##
      ## Sports
      ##============================================================##
      match 'sport/new' => "admin#sport_new",               :via => :get,     :as => :sport_new
      match 'sport/:id' => "admin#sport_edit",              :via => :get,     :as => :sport_edit
      match 'sport/:id' => "admin#sport_delete",            :via => :delete,  :as => :sport_delete
      match 'sport/:id' => "admin#sport_update",            :via => :patch,   :as => :sport_update
      ##============================================================##
      ## Sport Level
      ##============================================================##
      match 'sport_level/new' => "admin#sport_level_new",               :via => :get,     :as => :sport_level_new
      match 'sport_level/:id' => "admin#sport_level_edit",              :via => :get,     :as => :sport_level_edit
      match 'sport_level/:id' => "admin#sport_level_delete",            :via => :delete,  :as => :sport_level_delete
      match 'sport_level/:id' => "admin#sport_level_update",            :via => :patch,   :as => :sport_level_update
      ##============================================================##
      ## Sport Unviers
      ##============================================================##
      match 'sport_univer/new' => "admin#sport_univer_new",               :via => :get,     :as => :sport_univer_new
      match 'sport_univer/:id' => "admin#sport_univer_edit",              :via => :get,     :as => :sport_univer_edit
      match 'sport_univer/:id' => "admin#sport_univer_delete",            :via => :delete,  :as => :sport_univer_delete
      match 'sport_univer/:id' => "admin#sport_univer_update",            :via => :patch,   :as => :sport_univer_update
      ##============================================================##
      ## Sport Level
      ##============================================================##
      match 'sport_category/new' => "admin#sport_category_new",               :via => :get,     :as => :sport_category_new
      match 'sport_category/:id' => "admin#sport_category_edit",              :via => :get,     :as => :sport_category_edit
      match 'sport_category/:id' => "admin#sport_category_delete",            :via => :delete,  :as => :sport_category_delete
      match 'sport_category/:id' => "admin#sport_category_update",            :via => :patch,   :as => :sport_category_update
      ##============================================================##
      ## Posts
      ##============================================================##
      match 'post/new' => "admin#post_new",               :via => :get,     :as => :post_new
      match 'post/:id' => "admin#post_edit",              :via => :get,     :as => :post_edit
      match 'post/:id' => "admin#post_delete",            :via => :delete,  :as => :post_delete
      match 'post/:id' => "admin#post_update",            :via => :patch,   :as => :post_update
    end


    ##============================================================##
    ## Project
    ##============================================================##
    match "project"                     => "back_office#project_new",                 :via => :get,    :as => "project_new"
    match "project/:id/c"               => "back_office#project_create",              :via => :patch,  :as => "project_create"
    match "project/:id/e"               => "back_office#project_edit",                :via => :get,    :as => "project_edit"
    match "project/:id/v"               => "back_office#project_visibility",          :via => :get,    :as => "project_visibility"
    match "project/:id/v_d"             => "back_office#project_visibility_digital",  :via => :get,    :as => "project_visibility_digital"
    match "project/:id/u"               => "back_office#project_update",              :via => :patch,  :as => "project_update"
    match "project/:id"                 => "back_office#project_destroy",             :via => :delete, :as => "project_delete"
    match "project/:id/ad"              => "back_office#ad_create",                   :via => :post,   :as => "ad_create"
    match "project/:id/ad/:ad_id"       => "back_office#ad_delete",                   :via => :delete, :as => "ad_delete"

    ##============================================================##
    ## project Assets
    ##============================================================##
    match 'projects/:id/assets'        => 'back_office#project_asset_create',  via: [:post],   :as => :back_office_project_asset_create
    match 'projects/:id/assets'        => 'back_office#project_asset_delete',  via: [:delete], :as => :back_office_project_asset_delete
    


    ##============================================================##
    ## BackOffice
    ##============================================================##
    match 'dashboard/'                      => "back_office#dashboard",         :via => :get,     :as => :dashboard
    match 'dashboard/profile'               => "back_office#profile",           :via => :get,     :as => :dashboard_profile
    match 'dashboard/profile/:id'           => "back_office#profile_update",    :via => :patch,   :as => :dashboard_profile_update

  end
end
