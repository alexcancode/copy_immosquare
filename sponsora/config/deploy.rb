set :application, 'sponsora'
set :rvm_ruby_version, "2.3.2@sponsora"
set :repo_url, 'git@bitbucket.org:wgroupe/sponsora.git'
set :deploy_to, "/srv/apps/sponsora"
set :bundle_roles, :all
set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets}
set :keep_releases, 6
set :keep_assets, 2
