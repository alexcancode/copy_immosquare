require File.expand_path('../boot', __FILE__)

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Sponsora
  class Application < Rails::Application

    config.time_zone = 'Eastern Time (US & Canada)'

    # add app/assets/fonts to the asset path
    config.assets.paths << Rails.root.join("app", "assets", "fonts")



    ##============================================================##
    ## Load libs
    ##============================================================##
    config.autoload_paths << Rails.root.join('lib')

    ##============================================================##
    ## The default locale is :en and all translations
    ## from config/locales/*.rb,yml are auto loaded.
    ##============================================================##
    ##============================================================##
    config.i18n.default_locale = 'fr'
    config.i18n.available_locales = ['fr','en']
    config.i18n.fallbacks = false


    ##============================================================##
    ## Config action_mailer
    ##============================================================##
    config.action_mailer.default_url_options = { :host => ENV["HOST"] }
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :address        => "smtp-relay.sendinblue.com",
      :port           => 587,
      :authentication => :plain,
      :user_name      => ENV["SENDINBLUE_USERNAME"],
      :password       => ENV["SENDINBLUE_KEY"]
    }


    ##============================================================##
    ## Config paperclip S3_storage
    ##============================================================##
    config.paperclip_defaults = {
      :storage => :s3,
      :s3_protocol => :https,
      :s3_credentials => {
        :bucket => "sponsora",
        :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
        :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
      }
    }


    config.active_record.raise_in_transactional_callbacks = true
  end
end
