$(document).ready ->

  ##============================================================##
  ## Disable form submit with enter
  ##============================================================##
  $("form").keydown (event) ->
      e = e or event
      txtArea = /textarea/i.test((e.target or e.srcElement).tagName)
      txtArea or (e.keyCode or e.which or e.charCode or 0) != 13


  ##============================================================##
  ## SearBar
  ##============================================================##
  $('#search-bar li').on 'click', (event) ->
    event.stopPropagation()
    $('#search_what').val($(@).data('category') + ' ')
    $('#search-bar .categories').removeClass 'active'
    $('#search-bar #search_what').focus()

  ##============================================================##
  ## Datepicker
  ##============================================================##
  $('.datepicker').pickadate()

  ##============================================================##
  ##  Chosen
  ##============================================================##
  $('.chosen-select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'


  ##============================================================##
  ## Déploiement de la navigation utilisateur
  ##============================================================##
  $(".user-infos").on "click", ->
    $(".user-nav").toggleClass 'active'
  $('body').click (e) ->
    if e.target.id == ".user-nav"
      return;
    if $(e.target).closest('.user-nav').length
      return;
    if $(".user-nav").hasClass "active"
      $(".user-nav").removeClass 'active'





  ##============================================================##
  ## Setup de l'éditeur Froala...
  ##============================================================##
  if typeof froala_init isnt 'undefined' and froala_init is true
    $('.froala').froalaEditor(
      theme: 'gray'
      height: '300'
      scrollableContainer: '.fr-box.fr-top'
      toolbarButtons: [
        'fullscreen'
        'bold'
        'italic'
        'underline'
        'strikeThrough'
        'subscript'
        'superscript'
        'fontFamily'
        'fontSize'
        '|'
        'color'
        'emoticons'
        'inlineStyle'
        'paragraphStyle'
        '|'
        'paragraphFormat'
        'align'
        'formatOL'
        'formatUL'
        'outdent'
        'indent'
        '-'
        'insertLink'
        'insertImage' if picture_module == true
        'insertVideo' if picture_module == true
        'insertTable'
        '|'
        'quote'
        'insertHR'
        'undo'
        'redo'
        'clearFormatting'
        'selectAll'
      ]
      imageUploadURL: froala_upload_url
      imageMaxSize: 1 * 1024 * 1024
      imageAllowedTypes: [
        'jpeg'
        'jpg'
        'png'
      ]
      inlineMode: false
      toolbarFixed: false).on 'froalaEditor.image.removed', (e, editor, $img) ->
      $.ajax(
        method: 'POST'
        url: frola_delete_url
        data: src: $img.attr('src')).done((data) ->
        console.log 'image was deleted'
        return
      ).fail ->
        console.log 'image delete problem'
        return
      return