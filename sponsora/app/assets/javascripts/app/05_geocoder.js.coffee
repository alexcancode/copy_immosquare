$(document).ready ->

  if google? and $('#search_where').length != 0
    autocomplete = new google.maps.places.Autocomplete $('#search_where')[0],
        types: ['geocode']
    google.maps.event.addListener autocomplete, 'place_changed', ->
      place = autocomplete.getPlace()
      console.log place
      if !place.geometry
        alert("Autocomplete's returned place contains no geometry")
      if (place.geometry.viewport)
        $('#search_bounds').val(place.geometry.viewport.toUrlValue())
      else
        $('#search_bounds').val(place.geometry.location.toUrlValue())
        $('#search_radius').val(100.00/place.address_components.length)
