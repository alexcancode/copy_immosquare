$('body.back_office.project_visibility').ready ->

  ##============================================================##
  ##
  ##
  ##============================================================##
  support_type = $('#ad_ad_support_detail_id').html()
  $('#ad_ad_support_id').change ->
    selected_text = $('#ad_ad_support_id :selected').text()
    escaped_selected = selected_text.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = "<option value=''>--</option>"
    options += $(support_type).filter("optgroup[label='#{escaped_selected}']").html()
    if options
      $('#ad_ad_support_detail_id').html(options)

  $('#ad_ad_support_id').change ->
    $('#preview').html('')


  $('#ad_ad_support_detail_id').change ->
    $('#preview').html('<img class="img-responsive">')
    id = parseInt($(@).val())
    _.each supports_details, (s) ->
      if s.id == id
        $('#preview img').attr('src',s.picture)



  ##============================================================##
  ##
  ##============================================================##
  $('.ad_event_name').hide()
  $('#ad_ad_season').change ->
    if parseInt($(@).val()) is 2
      $('.ad_event_name').show()
    else
      $('.ad_event_name').hide()


$('body.back_office.project_edit').ready ->

  ##============================================================##
  ## Image Upload for Logo
  ##============================================================##

  # Customize choose image button
  $('#input_file_button_logo').on 'click', (e) ->
    $('#input-file-logo').trigger "click"

  # FileUpload Pictures
  $("form.simple_form.edit_project").fileupload
    dataType: 'script'
    dropZone: $('#dropzone_logo')
    add: (e, data) ->
      types = /(\.|\/)(gif|jpe?g|png|mov|mpeg|mpeg4|avi)$/i
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        data.context = $("#upload_progess_bar_logo")
        data.context.find('#progress_bar_logo').text(file.name)
        $("#upload_progess_bar_logo").show()
        data.submit()
      else
        swal("Error","#{file.name} is not a gif, jpg or png image file",'error')
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('#progress_bar_logo').css('width', progress + '%')
    done: (e,data) ->
      $("#upload_progess_bar_logo").hide()
      data.context.find('#progress_bar_logo').css('width', '0%')

  # Effect on dragover
  $(document).bind 'dragover', (e) ->
    dropZone = $('#dropzone_logo')
    timeout = window.dropZoneTimeout
    if !timeout
      dropZone.addClass 'in'
    else
      clearTimeout timeout
    found = false
    node = e.target
    loop
      if node == dropZone[0]
        found = true
        break
      node = node.parentNode
      unless node != null
        break
    if found
      dropZone.addClass 'hover'
    else
      dropZone.removeClass 'hover'
    window.dropZoneTimeout = setTimeout((->
      window.dropZoneTimeout = null
      dropZone.removeClass 'in hover'
      return
    ), 100)
    return




  ##============================================================##
  ## Image Upload for Project Assets
  ##============================================================##

  # Customize le bouton choisir image
  $('#input_file_button').on 'click', (e) ->
    $('#input-file').trigger "click"

  # FileUpload Pictures
  $("#new_project_asset").fileupload
    dataType: 'script'
    dropZone: $('#dropzone')
    add: (e, data) ->
      types = /(\.|\/)(gif|jpe?g|png|mov|mpeg|mpeg4|avi)$/i
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        data.context = $("#upload_progess_bar")
        data.context.append('<div class="progress"><div id="progress_bar_'+file.lastModified+'" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div></div>')
        data.context.find('#progress_bar_'+file.lastModified).text(file.name)
        data.submit()
      else
        swal("Error","#{file.name} is not a gif, jpg or png image file",'error')
    progress: (e, data) ->
      if data.context
        file = data.files[0]
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('#progress_bar_'+file.lastModified).css('width', progress + '%')
    done: (e,data) ->
      file = data.files[0]
      data.context.find('#progress_bar_'+file.lastModified).css('width', '0%').parent().remove()

  # Effect on dragover
  $(document).bind 'dragover', (e) ->
    dropZone = $('#dropzone')
    timeout = window.dropZoneTimeout
    if !timeout
      dropZone.addClass 'in'
    else
      clearTimeout timeout
    found = false
    node = e.target
    loop
      if node == dropZone[0]
        found = true
        break
      node = node.parentNode
      unless node != null
        break
    if found
      dropZone.addClass 'hover'
    else
      dropZone.removeClass 'hover'
    window.dropZoneTimeout = setTimeout((->
      window.dropZoneTimeout = null
      dropZone.removeClass 'in hover'
      return
    ), 100)
    return

