$("body.pages.home, body.explore_show, body.explore").ready ->


  ## Sticky search bar
  searchBar = $("#search-bar")
  searchBarPos = searchBar.position().top
  lastScroll = 0
  $(window).scroll ->
    scroll = $(@).scrollTop()
    if(scroll > searchBarPos + $("#top-header").outerHeight())
      searchBar.addClass 'fixed'
    else if(scroll < searchBarPos)
      searchBar.removeClass 'fixed'

    if (scroll > lastScroll)
      searchBar.removeClass 'visible'
    else
      searchBar.addClass 'visible'
    lastScroll = scroll;

  ## Déploiement du dropdown categorie (search bar)
  $("#search-bar .categories .selected").on "click", ->
    $("#search-bar .categories").toggleClass 'active'
  $('body').click (e) ->
    if e.target.id == "#search-bar .categories"
      return;
    if $(e.target).closest('#search-bar .categories').length
      return;
    if $("#search-bar .categories").hasClass "active"
      $("#search-bar .categories").removeClass 'active'

  ## Déploiement du dropdown categorie (search bar)
  filter_clicked = null
  $("#search-bar-filters .filter .title").on "click", ->
    $("#search-bar-filters .filter").removeClass 'active'
    if filter_clicked != null
      if filter_clicked.is($(@))
        $(@).parent('.filter').removeClass 'active'
        filter_clicked = null
        return false;
      else
        $(@).parent('.filter').toggleClass 'active'
        filter_clicked = $(@)
        return false;
    $(@).parent('.filter').toggleClass 'active'
    filter_clicked = $(@)
  $('body').click (e) ->
    if e.target.id == "#search-bar-filters .filter"
      return;
    if $(e.target).closest('#search-bar-filters .filter').length
      return;
    if $("#search-bar-filters .filter").hasClass "active"
      $("#search-bar-filters .filter").removeClass 'active'
      filter_clicked = null
