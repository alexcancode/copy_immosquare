$("body.project_edit").ready ->

  autocomplete = new google.maps.places.Autocomplete(document.getElementById('gmaps-input-address'))

  google.maps.event.addListener autocomplete, 'place_changed', ->
    performGeocoding(autocomplete.getPlace())

  performGeocoding = (place) ->
    $("#mission_google_place_id, #mission_street_number, #mission_street_name, #mission_sublocality, #mission_locality, #mission_administrative_area_level_1, #mission_administrative_area_level_2, #mission_country, #mission_zipcode, #mission_latitude, #mission_longitude").val("")

    $("#gmaps-input-address").val place.formatted_address

    if !place.geometry
      swal "Autocomplete's returned place contains no geometry"
      return

    $("#project_latitude").val(place.geometry.location.lat())
    $("#project_longitude").val(place.geometry.location.lng())
    $("#project_google_place_id").val(place.place_id)

    componentForm =
      street_number:
        long_or_short:'short_name'
        my_input_id: 'street_number'
      route:
        long_or_short: 'long_name'
        my_input_id: 'street_name'
      neighborhood:
        long_or_short: 'long_name'
        my_input_id: 'sublocality'
      locality:
        long_or_short: 'long_name'
        my_input_id: 'locality'
      administrative_area_level_2:
        long_or_short: 'long_name'
        my_input_id: 'administrative_area_level_2'
      administrative_area_level_1:
        long_or_short: 'long_name'
        my_input_id: 'administrative_area_level_1'
      country:
        long_or_short: 'short_name'
        my_input_id: 'country'
      postal_code:
        long_or_short: 'short_name'
        my_input_id: 'zipcode'

    i = 0
    while i < place.address_components.length
      addressType = place.address_components[i].types[0]
      if componentForm[addressType]
        val = place.address_components[i][componentForm[addressType].long_or_short]
        console.log "#{addressType} => #{val}"
        $("#project_#{componentForm[addressType].my_input_id}").val(val).trigger('change')
      i++
