//============================================================
//  JQUERY
//============================================================
//= require jquery
//= require jquery_ujs



//============================================================
//  OHTER LIB
//============================================================
//= require bootstrap-sprockets
//= require underscore
//= require chosen-jquery
//= require sweetalert
//= require sweet-alert-confirm
//= require pickadate/picker
//= require pickadate/picker.date
//= require lighter



//============================================================
//  FROALA
//============================================================
//= require froala_editor.min.js
//= require plugins/align.min.js
//= require plugins/char_counter.min.js
//= require plugins/colors.min.js
//= require plugins/entities.min.js
//= require plugins/file.min.js
//= require plugins/font_family.min.js
//= require plugins/font_size.min.js
//= require plugins/fullscreen.min.js
//= require plugins/image.min.js
//= require plugins/inline_style.min.js
//= require plugins/line_breaker.min.js
//= require plugins/link.min.js
//= require plugins/lists.min.js
//= require plugins/paragraph_format.min.js
//= require plugins/paragraph_style.min.js
//= require plugins/quote.min.js
//= require plugins/table.min.js
//= require plugins/url.min.js
//= require plugins/video.min.js


//============================================================
//  My plugins
//============================================================
//= require plugins/jquery.readySelector

//============================================================
//  IMAGE UPLOAD
//============================================================
//= require jquery.remotipart
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl


//============================================================
//  APPS
//============================================================
//= require app/00_for_all_pages
//= require app/01_geocoding
//= require app/02_home
//= require app/03_project_edit
//= require app/04_social_sharing
//= require app/05_geocoder
