class Post < ActiveRecord::Base



  ##============================================================##
  ## Slug
  ##============================================================##
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  def should_generate_new_friendly_id?
    title_changed?
  end




  ##============================================================##
  ## Paperclip : Post picture
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/posts/:id/picture_:style.:extension",
  :styles => {
    :small => ["630x315#", :jpg],
    :medium => ["630x630#", :jpg],
    :large => ["1920x784#", :jpg],
  },
  :convert_options => {
    :small => "-background white -gravity center",
    :medium => "-background white -gravity center",
    :large => "-background white -gravity center"
  },
  :default_url => "https://placehold.it/1920x784"
  validates_attachment_content_type :picture, :content_type => /\Aimage/
  validates_attachment_size :picture, :less_than=>2.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_picture

  private

  def rename_picture
    extension = File.extname(picture_file_name).downcase
    self.picture.instance_write :file_name, "sponsora_#{Time.now.to_i.to_s}#{extension}"
  end


end
