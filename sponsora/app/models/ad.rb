class Ad < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :project
  belongs_to :ad_support
  belongs_to :ad_support_detail

  ##============================================================##
  ## Money
  ##============================================================##
  monetize :price_cents

end
