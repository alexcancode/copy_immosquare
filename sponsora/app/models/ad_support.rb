class AdSupport < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  has_many :ad_support_details
end
