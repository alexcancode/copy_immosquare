class SportUniver < ActiveRecord::Base

  ##============================================================##
  ## Association
  ##============================================================##
  has_many :sports
  has_many :projects, through: :sports

  ##============================================================##
  ## Paperclip : picture
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/sport_univers/:id/picture_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small => ["50x50#", :png],
    :medium => ["100x100#", :png]
    },
    :convert_options => {
      :small => "-background white -flatten +matte -quality 90 -strip",
      :medium => "-background white -flatten +matte -quality 90 -strip"
      },
      :default_url => "http://placehold.it/100x100"
      validates_attachment_content_type :picture, :content_type => /\Aimage/
      validates_attachment_size :picture, :less_than=>2.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_picture


  def rename_picture
    extension = File.extname(picture_file_name).downcase
    self.picture.instance_write :file_name, "sponsora_#{Time.now.to_i.to_s}#{extension}"
  end


end
