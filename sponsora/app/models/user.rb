class User < ActiveRecord::Base

  ##============================================================##
  ## Include Helper
  ##============================================================##
  require "#{Rails.root}/app/helpers/gravatar_helper"
  include GravatarHelper


  ##============================================================##
  ## Association
  ##============================================================##
  rolify
  has_many :projects, dependent: :destroy
  has_many :authentifications, dependent: :destroy



  ##============================================================##
  ## Validation
  ##============================================================##
  validates_presence_of  :first_name
  validates_presence_of  :last_name

  ##============================================================##
  ## Devise
  ##============================================================##
  devise :database_authenticatable, :registerable,:recoverable, :rememberable, :trackable, :validatable



  ##============================================================##
  ## Paperclip : picture
  ##============================================================##
  has_attached_file :avatar,
  :path => "#{Rails.env}/users/:id/avatar_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :medium => ["500x500#", :jpg]
    },
    :convert_options => {
    :medium => "-background white -flatten +matte -quality 90 -strip"
  },
  :default_url => "http://placehold.it/500x500"
  validates_attachment_content_type :avatar, :content_type => /\Aimage/
  validates_attachment_size :avatar, :less_than=>2.megabytes



  def name_to_display
    (self.first_name.present? or self.last_name.present?) ?  "#{self.first_name.present? ? self.first_name.upcase.initial : nil} #{self.last_name.present? ? self.last_name.upcase.initial : nil}" : self.email
  end


  def name_to_display_full
    (self.first_name.present? or self.last_name.present?) ?  "#{self.first_name.present? ? self.first_name.capitalize : nil} #{self.last_name.present? ? self.last_name.capitalize : nil}" : self.email
  end



  def image_to_display
    self.avatar? ?  self.avatar.url(:medium) : gravatar_url(self.email,400)
  end



end
