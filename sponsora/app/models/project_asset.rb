class ProjectAsset < ActiveRecord::Base
  belongs_to :project

  ##============================================================##
  ## Paperclip Interpolates
  ##============================================================##
  Paperclip.interpolates('project_id') do |attachment, style|
    attachment.instance.project_id
  end

  #============================================================##
  # Paperclip : project assets
  #============================================================##
  has_attached_file :image,
  :path => "#{Rails.env}/projects/:project_id/assets/image_:id_:style.:extension",
  :hash_secret => "some_secret",
  :styles => {
    :small => ["150x150", :jpg],
    :large => ["500x500#", :jpg]
    },
    :convert_options => {
      :small => "-quality 70 -strip",
      :large => "-quality 85 -strip"
      },
      :default_url => "http://placehold.it/500x500"
      validates_attachment_content_type :image, :content_type => /\Aimage/
      validates_attachment_size :image, :less_than=>2.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_image


  def rename_image
    extension = File.extname(image_file_name).downcase
    self.image.instance_write :file_name, "project_image_#{Time.now.to_i.to_s}#{extension}"
  end
end
