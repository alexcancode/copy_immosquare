class Sport < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  has_many :projects
  belongs_to :sport_univer

end
