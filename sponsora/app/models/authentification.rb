class Authentification < ActiveRecord::Base

  ##============================================================##
  ## Associations
  ##============================================================##
  belongs_to :user
  belongs_to :api_provider
  belongs_to :project
end
