class Project < ActiveRecord::Base

  ##============================================================##
  ## includes router helper
  ##============================================================##
  include Rails.application.routes.url_helpers

  ##============================================================##
  ## Association
  ##============================================================##
  belongs_to :sport
  belongs_to :user
  belongs_to :sport_category
  belongs_to :sport_level
  belongs_to :gender
  belongs_to :project_type
  has_many :ads, dependent: :destroy
  has_many :authentifications, dependent: :destroy
  has_many :wysiwyg_assets, as: :imageable,:dependent => :destroy
  has_many :project_assets


  ##============================================================##
  ## Validations
  ##============================================================##
  validates :title, presence: true, if: "draft ==  0"

  ##============================================================##
  ## Paperclip : Post picture
  ##============================================================##
  has_attached_file :picture,
  :path => "#{Rails.env}/projects/:id/picture_:style.:extension",
  :styles => {
    :small => ["630x315#", :jpg],
    :medium => ["630x630#", :jpg],
    :large => ["1920x784#", :jpg],
  },
  :convert_options => {
    :small => "-background white -gravity center",
    :medium => "-background white -gravity center",
    :large => "-background white -gravity center"
  },
  :default_url => "https://placehold.it/1920x784"
  validates_attachment_content_type :picture, :content_type => /\Aimage/
  validates_attachment_size :picture, :less_than=>20.megabytes


  ##============================================================##
  ## PaperClip Pre & Post Proccess
  ## Dois impérativement se trouver arpès has_attached_file!
  ##============================================================##
  before_post_process :rename_picture




  ##============================================================##
  ## Elasticseach
  ##============================================================##
  include Elasticsearch::Model
  index_name "#{Rails.application.class.parent_name.underscore}_#{Rails.env}_projects"
  document_type "project"

  after_save     { ElasticIndexer.new(:index, self.class.name, self.id)}
  after_destroy  { ElasticIndexer.new(:delete,self.class.name, self.id)}


  ##============================================================##
  ## Pour définir manuellement les champs présent dans les
  ## sources
  ## Geohash ne sert pas pour elasticsearch, mais pour grouper
  ## les résultats par la suite dans la réponse de l'api
  ##============================================================##
  def as_indexed_json(options={})
    my_infos = Jbuilder.encode do |json|
      json.id                           self.id
      json.draft                        self.draft
      json.title                        self.title
      json.summary                      self.summary
      json.palmares                     self.palmares
      json.address                      self.address
      json.cover_url                    self.picture.url(:medium,:timestamp => false)
      json.link                         explore_show_path(self.id,:locale => "fr")
      json.updated_at                   I18n.localize self.updated_at.to_time,:format =>("%Y-%m-%d")
      json.sublocality                  self.sublocality
      json.locality                     self.locality
      json.administrative_area_level_2  self.administrative_area_level_2
      json.administrative_area_level_1  self.administrative_area_level_1
      json.min_max                      self.ads.pluck(:price_cents).minmax
      json.ads_count                    self.ads.count
      json.ads_count_available          self.ads.count
      json.sport do
        if self.sport.present?
          json.id self.sport_id
          json.fr self.sport.name
        end
      end
      json.sport_univer do
        if self.sport.present? and self.sport.sport_univer.present?
          json.id  self.sport.sport_univer_id
          json.fr  self.sport.sport_univer.name
          json.url self.sport.sport_univer.picture.url(:small,:timestamp => false)
        end
      end
      json.sport_category do
        if self.sport_category.present?
          json.id self.sport_category_id
          json.fr self.sport_category.name
        end
      end
      json.sport_level do
        if self.sport_level.present?
          json.id self.sport_level_id
          json.fr self.sport_level.name
        end
      end
      json.gender do
        if self.gender.present?
          json.id self.gender_id
          json.fr self.gender.name
        end
      end
      json.project_type do
        if self.project_type.present?
          json.id self.project_type_id
          json.fr self.project_type.name
        end
      end
      if (self.latitude.present? and self.longitude.present?)
        json.location do
          json.lat   self.latitude
          json.lon   self.longitude
        end
      end
    end
    return JSON.parse(my_infos)
  end


  ##============================================================##
  ## Pour définir l'indexation de chaque champs....
  ## Si dynamic == true le bloc sert à overwrite et le reste est automatique
  ## Si dynamic == false, on définie quels sont à indexer manuellement
  ##============================================================##
  settings index: {
    number_of_shards: 5,
    number_of_replicas: 0,
    analysis: {
      tokenizer: {
        nGram: {
          type: "nGram",
          min_gram: "2",
          max_gram: "20"
        }
      },
      filter: {
        french_stopwords: {
          type: 'stop',
          stopwords: '_french_',
          ignore_case: true
        },
        french_snowball: {
          type: "snowball",
          language: "French"
        },
        french_elision:{
          type:  "elision",
          articles: %w{l m t qu n s j d},

        },
        worddelimiter:{
          type: 'word_delimiter'
        }
      },
      analyzer: {
        my_french_custom_analyzer: {
          type: "custom",
          tokenizer: "nGram",
          filter: ["french_stopwords", "asciifolding", "lowercase", "french_snowball", "french_elision", "worddelimiter"],
          char_filter: ["html_strip"]
        },
        my_french_search_analyzer: {
          type: "custom",
          tokenizer: "standard",
          filter: ["french_stopwords", "asciifolding", "lowercase", "french_snowball", "french_elision", "worddelimiter"],
          char_filter: ["html_strip"]
        }
      }
    }
  } do
    mappings dynamic: true do
      indexes :location,  :type  => "geo_point"
      indexes :title,     :boost => 8,   :analyzer => "my_french_custom_analyzer", :search_analyzer => 'my_french_search_analyzer'
      indexes :locality,  :boost => 6,   :analyzer => "my_french_custom_analyzer", :search_analyzer => 'my_french_search_analyzer'
      indexes :summary,   :boost => 4,   :analyzer => "my_french_custom_analyzer"
      indexes :palmares,  :boost => 3,   :analyzer => "my_french_custom_analyzer"
      indexes :sport do
        indexes :fr,              :boost => 8,   :analyzer => "my_french_custom_analyzer", :search_analyzer => 'my_french_search_analyzer'
      end
      indexes :sport_category do
        indexes :fr,              :boost => 8,   :analyzer => "my_french_custom_analyzer", :search_analyzer => 'my_french_search_analyzer'
      end
      indexes :sport_level do
        indexes :fr,              :boost => 8,   :analyzer => "my_french_custom_analyzer", :search_analyzer => 'my_french_search_analyzer'
      end
    end
  end


  ##============================================================##
  ## ~ : recherche partielle sur le mot
  ## must     : The clause (query) must appear in matching documents and will contribute to the score.
  ## filter   : The clause (query) must appear in matching documents. However unlike must the score of the query will be ignored.
  ## should   : The clause (query) should appear in the matching document. In a boolean query with no must clauses, one or more should clauses must match a document. The minimum number of should clauses to match can be set using the minimum_should_match parameter.
  ## must_not : The clause (query) must not appear in the matching documents.
  ##============================================================##
  def self.my_search_elastic(params)
    must   = []
    filter = []
    should = []

    ##============================================================##
    ## Default Setting
    ##============================================================##
    filter << {not: {term: { draft: 1 }}}
    must << {"exists": { "field":  "sport.id" }}
    must << {"exists": { "field":  "sport_univer.id" }}
    must << {"exists": { "field":  "project_type.id" }}



    ##============================================================##
    ## Filter
    ##============================================================##
    filter << {      terms: { "sport.id":  params[:sport].split(',') }}     if params[:sport].present?
    filter << {not:{ terms: { "sport.id":  params[:sport!].split(',') }}}   if params[:sport!].present?

    filter << {      terms: { "sport_univer.id":  params[:sport_univer].split(',') }}     if params[:sport_univer].present?
    filter << {not:{ terms: { "sport_univer.id":  params[:sport_univer!].split(',') }}}   if params[:sport_univer!].present?

    filter << {      terms: { "project_type.id":  params[:project_type].split(',') }}     if params[:project_type].present?
    filter << {not:{ terms: { "project_type.id":  params[:project_type!].split(',') }}}   if params[:project_type!].present?

    ##============================================================##
    ## sport_category,sport_level, gender n'existe pas pour les clubs
    ##============================================================##
    filter << {      terms: { "sport_category.id":  params[:sport_category].split(',') }}     if params[:sport_category].present?
    filter << {not:{ terms: { "sport_category.id":  params[:sport_category!].split(',') }}}   if params[:sport_category!].present?

    filter << {      terms: { "sport_level.id":  params[:sport_level].split(',') }}     if params[:sport_level].present?
    filter << {not:{ terms: { "sport_level.id":  params[:sport_level!].split(',') }}}   if params[:sport_level!].present?

    filter << {      terms: { "gender.id":  params[:gender].split(',') }}     if params[:gender].present?
    filter << {not:{ terms: { "gender.id":  params[:gender!].split(',') }}}   if params[:gender!].present?



    ##============================================================##
    ## Doit impérativement (must) contenier params['what']
    ## ~ Fuziness pour dire qu'il peut manquer des lettres
    ##============================================================##
    must << {
      "query_string":{
        query: "#{params['what']}~",
      }
    } if params['what'].present?

    #============================================================##
    # Devrait contenir (should) params['what']...Augement le score
    # si le document réponds aux différents critères
    #============================================================##
    should << {
      "query_string": {
        query: "#{params['what']}~",
        fields: ["name"],
        "boost": 2
      },
      "query_string": {
        query: "#{params['what']}~",
        fields: ["category.fr"],
        "boost": 5
      },
    } if params['what'].present?

    should << {
      "query_string": {
        query: "#{params['where']}~",
        fields: ["sublocality,locality,administrative_area_level_2,administrative_area_level_1"],
        "boost": 10
      },
      "query_string": {
        query: "#{params['where']}~",
        "boost": 10
      }
    } if params['where'].present?

    #============================================================##
    # Geoloc
    # bounds = lat_sw,lng_sw,lat_ne,lng_ne
    #============================================================##
    if params[:bounds].present?
      bounds = params[:bounds].split(',')
      if bounds.size == 4 and params[:radius].blank?
        filter << { geo_bounding_box: {location: { bottom_left: { lat: bounds[0], lon: bounds[1]},top_right:{ lat: bounds[2], lon: bounds[3]}}}}
      end
      if bounds.size == 2 and params[:radius].blank?
        filter << {geo_distance: {location: {lat: bounds[0], lon: bounds[1]},:distance =>params[:radius]}}
      end
    end

    ##============================================================##
    ## Setup search
    ##============================================================##
    @response = Project.__elasticsearch__.search(
    {
      "query": {
        "bool": {
          "filter": filter,
          "should": should,
          "must":   must,
        }
      }
    }
    ).paginate(:page => params[:page], :per_page => params[:per_page])
  end

  ##============================================================##
  ## Fin Elasticsearh
  ##============================================================##









  def is_a_club?
    self.project_type_id == 1
  end





  private

  def rename_picture
    extension = File.extname(picture_file_name).downcase
    self.picture.instance_write :file_name, "sponsora_#{Time.now.to_i.to_s}#{extension}"
  end



end
