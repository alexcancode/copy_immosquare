class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  ##============================================================##
  ## BEFORE AND AFTER
  ##============================================================##
  before_filter :set_locale

  skip_before_filter :verify_authenticity_token, :only => [:wysiwyg_picture_manager]



  def after_sign_in_path_for(resource)
    dashboard_path
  end

  ##============================================================##
  ## On définie par défault la locale dans toutes les ulrs
  ## générées par l'application.
  ##============================================================##
  def default_url_options(options = {})
    {locale: I18n.locale}
  end

  ##============================================================##
  ## WYSIWYG picture
  ##============================================================##
  def wysiwyg_picture_manager
    @my_type = Project.find(params[:id]) if params[:asset_type] == "project"
    @asset = @my_type.wysiwyg_assets.new(:picture=>params[:file])
    @asset.save
  end

  def wysiwyg_picture_delete
    @asset = WysiwygAsset.find(File.basename(params[:src]).split('_').first)
    @asset.destroy
    render :nothing => true
  end


  private

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end




end
