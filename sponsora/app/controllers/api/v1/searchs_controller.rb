module Api
  module V1
    class SearchsController < Api::V1::ApplicationController

      ##============================================================##
      ## Before & After
      ##============================================================##
      skip_before_action  :verify_authenticity_token

      def projects
        begin
          @response = Project.my_search_elastic(params)
        rescue Exception => e
          return render :json =>{:errors=>e.message}, :status=> 402
        end
      end



    end
  end
end
