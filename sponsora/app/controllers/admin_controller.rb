class AdminController < ApplicationController


  before_filter :authenticate_user!
  before_filter :check_ability

  def index
  end

  ##============================================================##
  ## Ad_supports Management
  ##============================================================##
  def ad_supports
    @ad_supports = AdSupport.where(:draft =>0)
  end

  def ad_support
    @ad_supports = AdSupport.where(:draft =>0)
  end

  def ad_support_new
    @ad_support = AdSupport.where(:draft => 1).first_or_create
    redirect_to ad_support_edit_path(@ad_support.id)
  end


  def ad_support_edit
    @ad_support = AdSupport.find(params[:id])
  end

  def ad_support_update
    @ad_support = AdSupport.find(params[:id])
    @ad_support.update_attributes(ad_supports_params)
    redirect_to admin_ad_supports_path
  end

  def ad_support_delete
    @ad_support = AdSupport.find(params[:id])
    @ad_support.destroy
    redirect_to admin_ad_supports_path
  end



  ##============================================================##
  ## Ad_support Details Management
  ##============================================================##
  def ad_support_details
    @ad_support_details = AdSupportDetail.where(:draft =>0)
  end

  def ad_support_detail_new
    @ad_support_detail = AdSupportDetail.where(:draft => 1).first_or_create
    redirect_to ad_support_detail_edit_path(@ad_support_detail.id)
  end


  def ad_support_detail_edit
    @ad_support_detail = AdSupportDetail.find(params[:id])
  end

  def ad_support_detail_update
    @ad_support_detail = AdSupportDetail.find(params[:id])
    @ad_support_detail.update_attributes(ad_support_details_params)
    redirect_to admin_ad_support_details_path
  end

  def ad_support_detail_delete
    @ad_support_detail = AdSupportDetail.find(params[:id])
    @ad_support_detail.destroy
    redirect_to admin_ad_support_details_path
  end



  ##============================================================##
  ## Sports Management
  ##============================================================##
  def sports
    @sports = Sport.where(:draft =>0)
  end

  def sport_new
    @sport = Sport.where(:draft => 1).first_or_create
    redirect_to sport_edit_path(@sport.id)
  end


  def sport_edit
    @sport = Sport.find(params[:id])
  end

  def sport_update
    @sport = Sport.find(params[:id])
    @sport.update_attributes(sports_params)
    redirect_to admin_sports_path
  end

  def sport_delete
    @sport = Sport.find(params[:id])
    @sport.destroy
    redirect_to admin_sports_path
  end



  ##============================================================##
  ## Sports Level Management
  ##============================================================##
  def sport_univers
    @sport_univers = SportUniver.where(:draft =>0)
  end

  def sport_univer_new
    @sport_univer = SportUniver.where(:draft => 1).first_or_create
    redirect_to sport_univer_edit_path(@sport_univer.id)
  end


  def sport_univer_edit
    @sport_univer = SportUniver.find(params[:id])
  end

  def sport_univer_update
    @sport_univer = SportUniver.find(params[:id])
    @sport_univer.update_attributes(sport_univers_params)
    redirect_to admin_sport_univers_path
  end

  def sport_univer_delete
    @sport_univer = SportUniver.find(params[:id])
    @sport_univer.destroy
    redirect_to admin_sport_univers_path
  end


  ##============================================================##
  ## Sports Level Management
  ##============================================================##
  def sport_levels
    @sport_levels = SportLevel.where(:draft =>0)
  end

  def sport_level_new
    @sport_level = SportLevel.where(:draft => 1).first_or_create
    redirect_to sport_level_edit_path(@sport_level.id)
  end


  def sport_level_edit
    @sport_level = SportLevel.find(params[:id])
  end

  def sport_level_update
    @sport_level = SportLevel.find(params[:id])
    @sport_level.update_attributes(sport_levels_params)
    redirect_to admin_sport_levels_path
  end

  def sport_level_delete
    @sport_level = SportLevel.find(params[:id])
    @sport_level.destroy
    redirect_to admin_sport_levels_path
  end


  ##============================================================##
  ## Sports Categories Management
  ##============================================================##
  def sport_categories
    @sport_categories = SportCategory.where(:draft =>0)
  end

  def sport_category_new
    @sport_category = SportCategory.where(:draft => 1).first_or_create
    redirect_to sport_category_edit_path(@sport_category.id)
  end


  def sport_category_edit
    @sport_category = SportCategory.find(params[:id])
  end

  def sport_category_update
    @sport_category = SportCategory.find(params[:id])
    @sport_category.update_attributes(sport_categorys_params)
    redirect_to admin_sport_categories_path
  end

  def sport_category_delete
    @sport_category = SportCategory.find(params[:id])
    @sport_category.destroy
    redirect_to admin_sport_categories_path
  end



  ##============================================================##
  ## Blog Management
  ##============================================================##
  def posts
    @posts = Post.where(:draft =>0)
  end

  def post_new
    @post = Post.where(:draft => 1).first_or_create
    redirect_to post_edit_path(@post.id)
  end


  def post_edit
    @post = Post.find(params[:id])
  end

  def post_update
    @post = Post.find(params[:id])
    @post.update_attributes(posts_params)
    redirect_to admin_posts_path
  end

  def post_delete
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to admin_posts_path
  end











  private

  def check_ability
    unless current_user.has_any_role? :admin
      redirect_to root_path
    end
  end

  def ad_supports_params
    params.require(:ad_support).permit!
  end

  def ad_support_details_params
    params.require(:ad_support_detail).permit!
  end

  def sports_params
    params.require(:sport).permit!
  end

  def sport_levels_params
    params.require(:sport_level).permit!
  end

  def sport_categorys_params
    params.require(:sport_category).permit!
  end

  def sport_univers_params
    params.require(:sport_univer).permit!
  end

  def posts_params
    params.require(:post).permit!
  end

end
