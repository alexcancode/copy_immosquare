class BackOfficeController < ApplicationController

  before_filter :authenticate_user!

  ##============================================================##
  ## Dashboard
  ##============================================================##
  def dashboard
    @projects = current_user.projects.where(:draft => 0)
  end

  ##============================================================##
  ## Profile
  ##============================================================##
  def profile
    @profile = current_user
  end

  def profile_update
    @profile = User.find(params[:id])
    if @profile.update_attributes(profile_params)
      redirect_to dashboard_path
    else
      render 'profile'
    end
  end


  ##============================================================##
  ## Project
  ##============================================================##
  def project_new
    @project = Project.where(:user_id =>current_user.id,:draft=>1).first_or_create
  end

  def project_create
    @project = Project.find(params[:id])
    if @project.update_attributes(project_params)
      redirect_to project_edit_path(@project)
    else
      render :project_new
    end
  end


  def project_edit
    @project = Project.find(params[:id])
  end



  def project_visibility
    @project = Project.find(params[:id])
    @supports_details = Array.new
    AdSupportDetail.all.each do |s|
      @supports_details <<{:id => s.id, :picture => s.picture.url(:original,:timestamp=>false)}
    end
  end


  def project_visibility_digital
    @project = Project.find(params[:id])
    ##============================================================##
    ## Facebook
    ##============================================================##
    @facebook  = Authentification.joins(:api_provider).where(:user_id => @project.user_id, :project_id => @project.id,:api_providers => { :name =>'facebook'}).last
    if @facebook.present?
      graph = Koala::Facebook::API.new(@facebook.token)
      @facebook_count = graph.api("me/friends")["summary"]["total_count"]
      pages_ids = Array.new
      graph.get_connections("me","accounts").each do |page|
        pages_ids << page["id"]
      end
      @facebook_pages = graph.get_objects(pages_ids,:fields =>['name','fan_count','picture'])
    end

    ##============================================================##
    ## Twitter
    ##============================================================##
    @twitter  = Authentification.joins(:api_provider).where(:user_id => @project.user_id, :project_id => @project.id,:api_providers => { :name =>'twitter'}).last
    if @twitter.present?
      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV['twitter_api_key']
        config.consumer_secret     = ENV['twitter_api_key_secret']
        config.access_token        = @twitter.token
        config.access_token_secret = @twitter.token_secret
      end
      @twitter_user = client.user(@twitter.nickname)
    end

    ##============================================================##
    ## Instagram
    ##============================================================##
    @instagram  = Authentification.joins(:api_provider).where(:user_id => @project.user_id, :project_id => @project.id,:api_providers => { :name =>'instagram'}).last
    if @instagram.present?
      @instagram_user = JSON.parse(HTTParty.get("https://api.instagram.com/v1/users/#{@instagram.uid}/?access_token=#{@instagram.token}").body)
    end

    ##============================================================##
    ## Youtube
    ##============================================================##
    @google = Authentification.joins(:api_provider).where(:user_id => @project.user_id, :project_id => @project.id,:api_providers => { :name =>'google_oauth2'}).last
    if @google.present?
      account       = Yt::Account.new(:refresh_token=> @google.refresh_token)
      @channel = account.channel
    end
  end



  def project_update
    @project = Project.find(params[:id])
    if @project.update_attributes(project_params)
      respond_to do |format|
        format.html { redirect_to dashboard_path }
        format.js {}
      end
    else
      render :project_edit
    end
  end

  def project_destroy
    @project = Project.find(params[:id])
    @project.destroy
    redirect_to dashboard_path
  end

    ##============================================================##
  ## Project Assets
  ##============================================================##
  def project_asset_create
    @project_asset = ProjectAsset.create(project_assets_params)
    @project = @project_asset.project
  end

  def project_asset_delete
    @asset = ProjectAsset.find(params[:id])
    @asset.destroy
  end


  ##============================================================##
  ## Ads
  ##============================================================##
  def ad_create
    @ad = Ad.new(ad_params)
    @ad.save
    redirect_to project_visibility_path(params[:id])
  end

  def ad_delete
    @ad = Ad.find(params[:ad_id])
    @ad.destroy
    redirect_to project_visibility_path(params[:id])
  end




  private

  def project_params
    params.require(:project).permit!
  end

  def ad_params
    params.require(:ad).permit!
  end

  def profile_params
    params.require(:user).permit(:first_name,:last_name,:avatar,:email)
  end

  def project_assets_params
    params.require(:project_asset).permit!
  end


end
