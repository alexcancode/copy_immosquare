class PagesController < ApplicationController

  def home
    response = HTTParty.get("#{request.base_url}/api/v1/search/projects?#{params.to_param}")
    @elastic = JSON.parse(response.body)
  end

  def about
  end

  def contact_create
    @contact = Contact.new(contact_params)
    if @contact.save
      ContactMailer.contact(@contact).deliver_now
    end
  end

  def explore
    response = HTTParty.get("#{request.base_url}/api/v1/search/projects?#{params.to_param}")
    @elastic = JSON.parse(response.body)
    univer   = SportUniver.find(params[:sport_univer])  if params[:sport_univer].present?
    sport    = Sport.find(params[:sport])               if params[:sport].present?
    @what    = "#{univer.present? ? univer.name : nil} - #{sport.present? ? sport.name : nil} #{params['what']}"
  end

  def explore_show
    @project        = Project.find(params[:id])
    @ad_price_range = @project.ads.pluck(:price_cents)
    @ad_price_range = @ad_price_range.minmax

    ##============================================================##
    ## Facebook
    ##============================================================##
    @facebook  = Authentification.joins(:api_provider).where(:user_id => @project.user_id, :project_id => @project.id,:api_providers => { :name =>'facebook'}).last
    if @facebook.present?
      graph = Koala::Facebook::API.new(@facebook.token)
      @facebook_count = graph.api("me/friends")["summary"]["total_count"]
      @facebook_link  = "https://www.facebook.com/#{@facebook.uid}"
      if @project.facebook.present?
        facebook_page   = graph.get_objects(@project.facebook,:fields =>['name','fan_count','picture'])
        @facebook_count = facebook_page[@project.facebook]['fan_count'] if facebook_page.present?
        @facebook_link  = "https://www.facebook.com/#{@project.facebook}"
      end
    end

    ##============================================================##
    ## Instagram
    ##============================================================##
    @instagram  = Authentification.joins(:api_provider).where(:user_id => @project.user_id, :project_id => @project.id,:api_providers => { :name =>'instagram'}).last
    if @instagram.present?
      @instagram_user = JSON.parse(HTTParty.get("https://api.instagram.com/v1/users/#{@instagram.uid}/?access_token=#{@instagram.token}").body)
      @instagram_link = "https://www.instagram.com/#{@instagram.nickname}"
    end


    ##============================================================##
    ## Twitter
    ##============================================================##
    @twitter  = Authentification.joins(:api_provider).where(:user_id => @project.user_id, :project_id => @project.id,:api_providers => { :name =>'twitter'}).last
    if @twitter.present?
      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV['twitter_api_key']
        config.consumer_secret     = ENV['twitter_api_key_secret']
        config.access_token        = @twitter.token
        config.access_token_secret = @twitter.token_secret
      end
      @twitter_user = client.user(@twitter.nickname)
      @twitter_link = "https://twitter.com/#{@twitter.nickname}"
    end



    ##============================================================##
    ## Youtube
    ##============================================================##
    @google = Authentification.joins(:api_provider).where(:user_id => @project.user_id, :project_id => @project.id,:api_providers => { :name =>'google_oauth2'}).last
    if @google.present?
      account       = Yt::Account.new(:refresh_token=> @google.refresh_token)
      @youtube_channel = account.channel
      @youtube_link    = "https://www.youtube.com/channel/#{account.channel.id}"
    end


  end



  def blog
    @blogs = Post.where(:draft => 0,:status => 1)
  end

  def blog_show
    @blog = Post.find(params[:id])
    @blogs = Post.where(:draft => 0,:status => 1)
  end


  def policy
  end

  def terms
  end


  private
  def contact_params
    params.require(:contact).permit(:email,:first_name,:last_name,:phone,:content)
  end

end
