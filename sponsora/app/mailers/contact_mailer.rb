class ContactMailer < ApplicationMailer

  default :template_path => "mailers/contact_mailer"


  def contact(contact)
    @contact = contact
    mail(
     :to => "fabien@sponsora.io",
     :from => set_my_from('no-reply'),
     :subject => set_my_subject("Formulaire contact")
     )
  end

end
