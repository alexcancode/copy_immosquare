class ApplicationMailer < ActionMailer::Base

  before_filter :set_website
  layout 'mailer'
  include MailerHelper
  add_template_helper(MailerHelper)


  def set_website
    @my_provider = {:name=>"sponsora", :host => "sponsora.io",:link_host=>Rails.env.production? ? "sponsora.io" : "lvh.me:3000",:color=>"#eb6e2a"}
  end


  def default_url_options(options = {})
    {
      :locale => I18n.locale,
      :host => @my_provider[:link_host]
    }
  end



end

