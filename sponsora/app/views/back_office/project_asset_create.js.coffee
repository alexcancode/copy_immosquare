<% if @project_asset.errors.any? %>
swal("Failed to upload picture: <%= j @project_asset.errors.full_messages.join(', ').html_safe %>");
<% else %>
$('#new_project_asset')[0].reset()
$('#uploaded_pictures').append('<%= j render :partial => "back_office/partials/project_asset", locals: {project_asset: @project_asset} %>')
<% end %>
