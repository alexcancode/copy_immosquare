$('form#new_contact').find("div.form-group").each ->
  $(@).removeClass "has-error has-success"
  $(@).children('p.hint').remove()


<% if @contact.errors.any? %>

errors = jQuery.parseJSON('<%= raw(escape_javascript(@contact.errors.to_hash.to_json)) %>')
$.each errors, (key, value) ->
  $(".contact_#{key}")
    .addClass("has-error")
    .append("<p class='hint'>#{value[0]}</p>")


<% else %>
$('form#new_contact')[0].reset()
$('#flashMessage').remove()
$('body').prepend(
  '<div id="flashMessage">\
    <div class="alert alert-success" role="alert">\
      <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>\
        Merci\
    </div>\
  </div>'
  )
<% end %>

