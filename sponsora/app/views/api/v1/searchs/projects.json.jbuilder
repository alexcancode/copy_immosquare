if @response
  json.took         @response.took
  json.pagination do
    from = @response.offset+1
    to   = @response.offset+@response.length >= @response.results.total ? @response.results.total : @response.offset+@response.length
    json.total          @response.results.total
    json.current_page   @response.current_page
    json.previous_page  @response.previous_page
    json.next_page      @response.next_page
    json.length         @response.length
    json.offset         @response.offset
    json.from           from
    json.to             to
    json.formated      "#{from}-#{to} sur #{@response.results.total}"
  end
  json.results    @response.results
else
  json.results []
end

