namespace :import do

  require "csv"

  task :sports => :environment do
    puts "start"
    file = "#{Rails.root}/docs/01_sports.csv"
    if File.exists?(file) == true
      begin
        CSV.foreach(file) do |z|
          Sport.where(:name => z).first_or_create
        end
      rescue Exception => e
        JulesLogger.info e
      end
    end
  end
end
