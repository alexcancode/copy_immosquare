class ElasticIndexer

  Client = Elasticsearch::Model.client

  def initialize(operation,klass,id)
    begin
      klass = eval(klass.to_s)
      JulesLogger.info "#{operation},#{klass},#{id}"
      case operation.to_s
      when /index/
        record = klass.find(id)
        Client.index index: klass.index_name, type: klass.document_type, id:id, body: record.as_indexed_json
      when /delete/
        Client.delete index: klass.index_name , type:klass.document_type, id:id
      else
        raise ArgumentError, "Unknown operation '#{operation}'"
      end
      JulesLogger.info "#{operation},#{klass},#{id} finished"
    rescue Exception => e
      JulesLogger.info "error #{e}"
    end
  end
end
